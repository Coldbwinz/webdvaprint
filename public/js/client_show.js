function fastEdit(nameBlock)
{
    $('.' + nameBlock + '-block').hide();
    $('.' + nameBlock + '-block-edit').show();    
}

function fastSave(field, url, nameBlock)
{
  var _token = $('input[name="_token"]').val();
  var value  = $('.' + field).val();
  
  $.ajax({
   type: "POST",
   url: '/' + url,
   data: field + "=" + value + "&_token=" + _token + "&name_filed=" + field,
   success: function(msg){
       $('.' + nameBlock + '-block-text').html(value);
       console.log('Save field');
       fastEnd(nameBlock);
   }
 });
}

function fastEnd(nameBlock)
{
    $('.' + nameBlock + '-block-edit').hide();
    $('.' + nameBlock + '-block').show();    
}