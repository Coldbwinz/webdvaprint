$(document).ready(function () {
    $('#table_history').on('click', '.showCalculation', '', function (e) {
        var id = parseInt(e.currentTarget.attributes['data-id'].nodeValue);
        page = parseInt('<?php if (isset($_GET["page"])) echo $_GET["page"]; ?>');
        if (page > 0) {
            window.location.href = '/orders/history/calculation/0/0/' + id + '?page=' + page;
        } else {
            window.location.href = '/orders/history/calculation/0/0/' + id;
        }
        return false;
    });

    $('#table_history').on('click', '.downloadBill', '', function (e) {
        var id = parseInt(e.currentTarget.attributes['data-id'].nodeValue);
        page = parseInt('<?php if (isset($_GET["page"])) echo $_GET["page"]; ?>');
        if (page > 0) {
            window.location.href = '/orders/calculation_pdf/' + id + '/1?page=' + page;
        } else {
            window.location.href = '/orders/calculation_pdf/' + id + '/1';
        }
        return false;
    });

    var count_timout = 0;
    $('input[name="search"]').keyup(function () {
        count_timout++;
        setTimeout(function () {
            count_timout--;
            if (count_timout == 0) {
                $('input[name="change"]').val(1);
                filterActivate();
            }
        }, 1000);
    });

    $('input[name = "created_at"]').on('input change keyup', function () {
        $('input[name="change"]').val(1);
        filterActivate();
    });

    $('.top_search_box select').on('change', function() {
        $('input[name="change"]').val(1);
        filterActivate();
    });

    $('.btn-clear-search').click( function() {
        $('input[name="search"]').val('');
        $('input[name="created_at"]').val('');
        $.each($('.top_search_box select'), function(i, item) {
            $(item).val($(item).find('option').eq(0).val());
        });
        $('input[name="change"]').val(1);
        filterActivate();
    });
    
    $('#table_history').on('click', '.btn-send-invoice', '', function (e) {
        var order_id = parseInt(e.currentTarget.attributes['data-order-id'].nodeValue);
        var url = '/order/only_invoice_send/' + order_id;
        $.ajax({
            url: url,
            method: "GET",
        });
    });

    $('#table_history').on('click', '.checkGuarantee', '', function (e) {
        getGuarantee($(this).data('order-id'));
        return false;
    });

    $('#div_orders_history_pagination').on('click', '.pagination a', function(e) {
        e.preventDefault();
        var url = new URL($(this).attr('href'));
        var page = url.searchParams.get("page");
        $('input[name="page"]').val(page);
        $('input[name="change"]').val(0);
        filterActivate();
        return false;
    })
});

function getGuarantee(order_id) {
    var url = '/group_order/check_guarantee/' + order_id;
    $.ajax({
        url: url,
        method: "GET",
        success: function (data) {
            $('#no_money_' + order_id).removeAttr('onclick').attr('href', '/invoicenomoney/add/' + order_id);
            window.open(data, '_blank');
        },
    });
}

function backToPage() {
    page = parseInt('<?php if (isset($_GET["page"])) echo $_GET["page"]; ?>');
    if (page > 0) {
        window.location.href = '/orders/history?page=' + page;
    } else {
        window.location.href = '/orders/history/';
    }
}

var filterIsActive = false;
function filterActivate() {
    if (!filterIsActive) {
        var frm = $('.top_search_box form');
        filterIsActive = true;
        startLoadingAnimation();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: frm.serialize(),
            method: "POST",
            url: "/orders/history",
            error: function (xhr, status, error) {
                filterIsActive = false;
                stopLoadingAnimation();
            },
            success: function (data) {
                filterIsActive = false;
                stopLoadingAnimation();
                $('#div_orders_history_pagination').html(data.pagination);
                $('input[name="temp_count"]').val(data.temp_count);
                $('#orders_history_table_data').html(data.html);
            }
        });
    }
}


