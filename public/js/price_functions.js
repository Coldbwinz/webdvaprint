$(document).ready( function() {
    $('.box-body').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    });
    $('.box-body').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    });

    $('.box-body').on('input keydown', 'input[type=number]', function (e) {
        if ((e.keyCode === 38) || (e.keyCode === 40)) {
            e.preventDefault();
        }
        if (/^0/.test(this.value)) {
            this.value = this.value.replace(/^0/, "")
        }
    });

    $('.ddm-prepress').on('click', 'a', function () {
        selected_prepress = $(this).attr('data-value');
        $('#group_prepress').html($(this).html());
        setDivLabels();
    } );
    
    $('.ddm-addresses').on('click', 'a', function () {
        selected_address = $(this).attr('data-value');
        $('#group_address').val($(this).html());
    });
});

function setDivLabels() {
    if (selected_prepress > 0) {
        var dws = $('#divWorkStart');
        var label1 = dws.find('label').eq(0);
        var label2 = dws.find('label').eq(1);
        var input1 = '<input type="radio" name="start" value="0">';
        var input2 = '<input type="radio" name="start" value="1">';
        if (((selected_prepress == 3) && (complects_count > 1)) || (selected_prepress == 4)) {
            input2 = input2.replace('>', ' checked>')
            label1.html(input1 + ' разные задания для каждого комплекта');
            label2.html(input2 + ' общее задание для всех комплектов');
        } else {
            input1 = input1.replace('>', ' checked>')
            label1.html(input1 + ' после оформления заказа');
            label2.html(input2 + ' по нажатию Продолжить');
        }
        if (((selected_prepress != 3) && (complects_count > 1)) || (selected_prepress == 4)) {
            $('#divWorkStart').hide();
        } else {
            $('#divWorkStart').show();
        }
    }
}

function make_print_structure (width, height) {
    var result = {};
    var countPages = 0;
    /*  "singlesheet", //однолистовое изделие
     "brochurebifold", //Буклет с одним изгибом
     "brochuretrifold", //Евробуклет - Буклет с 2-мя изгибами
     "bookletsaddlestitch", //брошюра на скобе
     "bookletwiredcoil"]; //брошюра на пружине*/
    var body_p_name;
    if (printler_type == 1) {
        body_p_name = 'singlesheet';
    }
    if (printler_type == 2) {
        body_p_name = (selected_type == 'один сгиб') ? 'brochurebifold' : 'brochuretrifold';
    }
    if (printler_type == 3) {
        if (selected_type == 'на пружине') {
            body_p_name = 'bookletwiredcoil';
            if (selected_cover_chromacity != 'без печати') {
                countPages = ((selected_cover_chromacity[4] == 0) ? 1 : 2);
                result.cover = {p_name: 'singlesheet', type : 'cover', count : countPages, chromacity_front: selected_cover_chromacity[0], chromacity_back : selected_cover_chromacity[4], width: width, height: height};
            }
            if (selected_pad_chromacity != 'без печати') {
                countPages = ((selected_pad_chromacity[4] == 0) ? 1 : 2);
                result.pad = {p_name: 'singlesheet', type : 'pad', count : countPages , chromacity_front: selected_pad_chromacity[0], chromacity_back : selected_pad_chromacity[4], width: width, height: height};
            }
        } else {
            body_p_name = 'bookletwiredcoil';
            if (selected_cover_chromacity != 'без печати') {
                countPages = ((selected_cover_chromacity[4] == 0) ? 2 : 4);
                result.cover = {p_name: 'brochurebifold', type : 'cover', count :  countPages, chromacity_front: selected_cover_chromacity[0], chromacity_back : selected_cover_chromacity[4], width: (width * 2), height: height};
            }
        }
        if (selected_chromacity != 'без печати') {
            if ($('#inpSamePages').is(':checked')) { //Если все листы одинаковые, то превращаем многостраничку в листовое изделие или изделие со сгибом
                body_p_name = (selected_type == 'на пружине') ? 'singlesheet' : 'brochurebifold';
                countPages = ((selected_chromacity[4] == 0) ? 1 : 2);
            } else {
                countPages = ((selected_chromacity[4] == 0) ? parseInt(selected_product) / 2 : parseInt(selected_product));
            }
            var side_type = 'byWidth';
            if ($('#divByTight').is(':visible')) {
                if ($('#inpByTight').is(':checked')) {
                    side_type = 'byHeight';
                }
            }
            var samePages;
            if ($('#spanSamePages').is(':visible')) {
                samePages = ($('#spanSamePages').html() == 'одинаковых стр.') ? 1 : 0;
            } else {
                samePages = ($('#inpSamePages').is(':checked')) ? 1 : 0;
            }
            result.body = {p_name: body_p_name, type : 'body', count : countPages, chromacity_front: selected_chromacity[0],
                chromacity_back : selected_chromacity[4], width: width, height: height, side_type: side_type, same_pages: samePages};
        }
    } else {
        if (selected_chromacity != 'без печати') {
            countPages = ((selected_chromacity[4] == 0) ? 1 : 2);
            result.body = {p_name: body_p_name, type : 'body', count : countPages, chromacity_front: selected_chromacity[0], chromacity_back : selected_chromacity[4], width: width, height: height};
        }
    }
    return result;
}

function calculateTemplateCost(base, countBodyPages) {
    sides = 0;
    if (printler_type == 3) {
        if ((selected_chromacity != 'без печати') && ((selected_chromacity != 'Выбрать'))) {
            var samePages;
            if ($('#spanSamePages').is(':visible')) {
                samePages = ($('#spanSamePages').html() == 'одинаковых стр.') ? 1 : 0;
            } else {
                samePages = ($('#inpSamePages').is(':checked')) ? 1 : 0;
            }
            if (samePages == 1) {
                if (selected_chromacity[4] == 0) {
                    sides += 1;
                } else {
                    sides += 2;
                }
            } else {
                sides = countBodyPages;
                if (selected_chromacity[4] == 0) {
                    sides = parseInt(sides / 2);
                }
            }
        }
        if (selected_type == 'на скрепке') {
            if (selected_cover != 'Без обложки') {
                if (selected_cover_chromacity != 'без печати') {
                    if (selected_cover_chromacity[4] == 0) {
                        sides += 2;
                    } else {
                        sides += 4;
                    }
                }
            }
        } else if (selected_type == 'на пружине') {
            if (selected_cover != 'Без обложки') {
                if (selected_cover_chromacity != 'без печати') {
                    if (selected_cover_chromacity[4] == 0) {
                        sides += 1;
                    } else {
                        sides += 2;
                    }
                }
            }
            if (selected_pad != 'Без подложки') {
                if (selected_pad_chromacity != 'без печати') {
                    if (selected_pad_chromacity[4] == 0) {
                        sides += 1;
                    } else {
                        sides += 2;
                    }
                }
            }
        }
        base = base * sides;
    } else {
        if ((selected_chromacity != 'без печати') && (selected_chromacity != 'Выбрать')) {
            if (selected_chromacity[4] == 0) {
                sides = 1;
            } else {
                sides = 2;
            }
        }
    }
    return base;
}

function checkPrepress() {
    if (selected_chromacity != 'Выбрать' && (selected_chromacity.length > 0) && (selected_size.length > 0) && client_id) {
        var tempWidth = parseInt(selected_size.split('x')[0]);
        var tempHeight = parseInt(selected_size.split('x')[1]);
        var two_side = (selected_chromacity[4] == 0) ? 0 : 1;
        var folds = 0;
        if (printler_type == 2) {
            folds = (selected_type == 'один сгиб') ? 1 : 2;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url: '/api/templates-count?product_type_id=' + type_id + '&folds=' + folds + '&size=' + tempWidth + 'x' + tempHeight + '&two_side=' + two_side + '&client_id=' + client_id,
            type: 'get',
            success: function (response) {
                if (response.templates_count > 0) {
                    if ($('.ddm-prepress li a[data-value="5"]').length == 0) {
                        $('.ddm-prepress li a[data-value="4"]').parent().after(
                            '<li><a data-value="5">Использовать макет из архива</a></li>');
                    }
                } else {
                    toDelete = $('.ddm-prepress li a[data-value="5"]');
                    if ($('#group_prepress').html() == toDelete.html()) {
                        $('#group_prepress').html('Выбрать');
                        selected_prepress = 0;
                    }
                    toDelete.parent().remove();
                }
            },
            failed: function (data) {
                console.log("ajax error");
            }
        });
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url: '/api/templates-count?product_type_id=' + type_id + '&folds=' + folds + '&size=' + tempWidth + 'x' + tempHeight + '&two_side=' + two_side,
            type: 'get',
            success: function (response) {
                if (response.templates_count > 0) {
                    if ($('.ddm-prepress li a[data-value="1"]').length == 0) {
                        $('.ddm-prepress li a[data-value="2"]').parent().before(
                            '<li><a data-value="1">Создать макет по шаблону</a></li>');
                    }
                } else {
                    toDelete = $('.ddm-prepress li a[data-value="1"]');
                    if ($('#group_prepress').html() == toDelete.html()) {
                        $('#group_prepress').html('Выбрать');
                        selected_prepress = 0;
                    }
                    toDelete.parent().remove();
                }
            },
            failed: function (data) {
                console.log("ajax error");
            }
        });

    }
}

function loadAddresses() {
    var addresses;
    if (selected_delivery_type.name == 'Самовывоз') {
        $('#group_address').prop('readonly', true);
        addresses = issuing_addresses;
        $('#group_address').val(issuing_addresses[0]);
    } else {
        $('#group_address').prop('readonly', false);
        addresses = client_addresses;
        $('#group_address').val('');
    }
    var html_addresses = '';
    $.each(addresses, function (i, item) {
        html_addresses += '<li><a style="cursor:pointer">' + item + '</a></li>';
    });

    if ((selected_delivery_type.name == 'Самовывоз') && (addresses.length == 1)) {
        $('#divChoiseIssuingPoint').hide();
    } else {
        $('#divChoiseIssuingPoint').show();
    }
    $('.ddm-addresses').html(html_addresses);
}

function checkPaymentType () {
    if (selected_payment_type.name == 'Отсрочка платежа') {
        $('#divDaysForWaitingPayment').show();
    } else {
        $('#divDaysForWaitingPayment').hide();
    }
}