var clientController = {
    
    contact: {
        fields: ["name", "email", "phone", "position"],
        data: {},
        comment: null,
        
        editBtn: '#contact-edit-btn',
        saveBtn: '#contact-save-btn',
        
        commentEditBtn: '#edit-user-comment',
        commentSaveBtn: '#save-user-comment',
        commentField: '#comment',
        commentText: '#comment-text',
        
        edit: function(){
            for(var key in this.fields){
                $("#" + this.fields[key] + "-placeholder span").hide();
                $("#" + this.fields[key]).show();
            }
            $(this.editBtn).hide();
            $(this.saveBtn).show();
        },
        
        editComment: function(){
            $(this.commentText).hide();
            $(this.commentEditBtn).hide();
            $(this.commentField).show();
            $(this.commentSaveBtn).show();
        },
        
        saveComment: function(){
            var $this = this;
            $(this.commentText).html($(this.commentField).val());
            $(this.commentField).hide();
            $(this.commentSaveBtn).hide();
            $(this.commentText).show();
            $(this.commentEditBtn).show();
        }
    },
    
    company: {
        commentEditBtn: '#edit-company-comment',
        commentSaveBtn: '#save-company-comment',
        commentField: '#company-comment',
        commentText: '#company-comment-text',
        
        editComment: function(){
            $(this.commentText).hide();
            $(this.commentEditBtn).hide();
            $(this.commentField).show();
            $(this.commentSaveBtn).show();
        },
        
        saveComment: function(){
            var $this = this;
            $(this.commentText).html($(this.commentField).val());
            $(this.commentField).hide();
            $(this.commentSaveBtn).hide();
            $(this.commentText).show();
            $(this.commentEditBtn).show();
        }
    },
    
    init: function(){
        var $this = this;
        $(this.contact.editBtn).on("click", function(){
            $this.contact.edit();
            return false;
        });
        $(this.contact.commentEditBtn).on("click", function(){
            $this.contact.editComment();
            return false;
        });
        $(this.contact.commentSaveBtn).on("click", function(){
            $this.contact.saveComment();
            return false;
        });
        $(this.company.commentEditBtn).on("click", function(){
            $this.company.editComment();
            return false;
        });
        $(this.company.commentSaveBtn).on("click", function(){
            $this.company.saveComment();
            return false;
        });
    }
};
$(document).ready(function(){
    clientController.init();
});
