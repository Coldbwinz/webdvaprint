var clientController = {
    messages: [
        'Извините - ваш браузер не поддерживает FileReader API'
    ],
    contact: {
        id: null,
        fields: ["name", "email", "phone", "position", "client_type", "client_status"],
        data: {},
        
        editBtn: '#contact-edit-btn',
        saveBtn: '#contact-save-btn',
        
        commentEditBtn: '#edit-user-comment',
        commentSaveBtn: '#save-user-comment',
        commentField: '#comment',
        commentText: '#comment-text',
        
        avatarInput: '#upload',
        avatarInputTrigger: '#change-photo',
        base64Input: '#imagebase64',
        $uploadCrop: null,
        avatar: '#contact-avatar',

        edit: function(){
            for(var key in this.fields){
                $("#" + this.fields[key] + "-placeholder span").hide();
                $("#" + this.fields[key]).show();
            }
            $(this.editBtn).hide();
            $(this.saveBtn).show();
        },

        save: function(){
            for(var key in this.fields){
                this.data[this.fields[key]] = $("#" + this.fields[key]).val();
                $("#" + this.fields[key] + "-placeholder span").html($("#" + this.fields[key]).val());
                $("#" + this.fields[key]).hide();
                $("#" + this.fields[key] + "-placeholder span").show();
            }
            $(this.saveBtn).hide();
            $(this.editBtn).show();
            this.pushSave();
        },
        
        pushSave: function(){
            var $this = this;
            $.ajax({
                url: '/api/contact/' + $this.id + '/avatar',
                type: 'post',
                dataType: 'json',
                data: $this.data,
                success: function(response){
                    console.log(response);
                }
            })
                .error(function (error) {
                    if (error.status === 422) {
                        $($this.avatarInputTrigger).addClass('error');
                        alert(error.responseJSON.avatar[0]);
                        $($this.avatar).trigger('error');
                    }
                });
        },
        
        editComment: function(){
            $(this.commentText).hide();
            $(this.commentEditBtn).hide();
            $(this.commentField).show();
            $(this.commentSaveBtn).show();
        },
        
        saveComment: function(){
            var $this = this;
            $(this.commentText).html($(this.commentField).val());
            $(this.commentField).hide();
            $(this.commentSaveBtn).hide();
            $(this.commentText).show();
            $(this.commentEditBtn).show();
            this.data['comment'] = $(this.commentField).val();
            this.pushSave();
        },
        
        saveAvatar: function(val){
            this.data['avatar'] = val;
            this.pushSave();
        }
    },
    
    client: {
        id: null,
        changed: [],
        data: {},
        commentEditBtn: '#edit-company-comment',
        commentSaveBtn: '#save-company-comment',
        commentField: '#company-comment',
        commentText: '#company-comment-text',

        clientForm: '#client-form',
        detailsForm: '#details-form',
        saveBtn: '#save-company',
        
        
        editComment: function(){
            $(this.commentText).hide();
            $(this.commentEditBtn).hide();
            $(this.commentField).show();
            $(this.commentSaveBtn).show();
        },
        
        saveComment: function(){
            $(this.commentText).html($(this.commentField).val());
            $(this.commentField).hide();
            $(this.commentSaveBtn).hide();
            $(this.commentText).show();
            $(this.commentEditBtn).show();
            this.data['comment'] = $(this.commentField).val();
            this.saveClient();
        },
        
        onChange: function(el){
            this.changed[this.changed.length] = '#' + $(el).attr("id");
            this.data[$(el).attr("name")] = $(el).val();
        },
        
        saveClient: function(){
            var $this = this;
            $($this.clientForm + ' input').each(function(){
                $this.data[$(this).attr("name")] = $(this).val();
            });
            $($this.clientForm + ' select').each(function(){
                $this.data[$(this).attr("name")] = $(this).val();
            });
            $this.pushSave();
        },

        pushSave: function(){
            var $this = this;
            $.ajax({
                url: '/client/saveClient/' + $this.id,
                type: 'post',
                dataType: 'json',
                data: $this.data,
                success: function(response){
                    if(response != undefined && response.status == 'ok'){
                        $this.showSaveSuccess();
                    }
                },
                failed: function(data) {
                    console.log(data);
                }
            });
        },

        showSaveSuccess: function(){
            var $this = this;
            $('#saved-company').show();
            setTimeout(function(){
                $('#saved-company').hide();
                $this.changed = [];
            }, 1000);
        },
        
        getClient: function(id){
            var $this = this;
            $.ajax({
                url: '/client/getClient/' + id,
                type: 'post',
                dataType: 'json',
                data: {_token: $("input[name='_token']").val()},
                success: function(response){
                    if(response != undefined && response.status == 'ok'){
                        $this.id = id;
                        for(var key in response.data){
                            $this.data[key] = response.data[key];
                            $($this.clientForm + " input[name='" + key + "']").val(response.data[key]);
                        }
                        $($this.clientForm + " #client-id").val(id);
                        $($this.commentText).html(response.data.comment);
                        $($this.commentField).val(response.data.comment);
                        $(document).scrollTop(0);
                    }
                }
            });
        },
        
        deleteFile: function(id){
            var $this = this;
            $.ajax({
                url: '/client/deleteFile/' + id,
                type: 'post',
                dataType: 'json',
                data: {_token: $("input[name='_token']").val()},
                success: function(response){
                    if(response != undefined && response.status == 'ok'){
                        $("#doc-" + id).fadeOut(function(){
                            $("#doc-" + id).remove();
                        });
                    }
                }
            });
        }
    },

    $modal: {
        $el: '#avatar-crop-modal',
        $cropArea: '#uploaded-image',
        $saveBtn: '#btn-croppie',

        show: function(){
            $(this.$el).show();
        },
        
        hide: function(){
            $(this.$el).hide();
        }
    },

    init: function(){
        var $this = this;
        //contact
        $this.contact.id = $("#contact_id").val();
        $this.client.id = $("#client-id").val();
        for(var key in $this.contact.fields){
            $this.contact.data[$this.contact.fields[key]] = $("#" + $this.contact.fields[key]).val();
        }
        $(this.contact.editBtn).on("click", function(){
            $this.contact.edit();
            return false;
        });
        $(this.contact.saveBtn).on("click", function(){
            $this.contact.save();
            return false;
        });
        $(this.contact.commentEditBtn).on("click", function(){
            $this.contact.editComment();
            return false;
        });
        $(this.contact.commentSaveBtn).on("click", function(){
            $this.contact.saveComment();
            return false;
        });
        $(this.contact.avatarInputTrigger).on("click", function(){
            $($this.contact.avatarInput).click();
            return false;
        });
        $(this.contact.avatarInput).on("change", function(){
            $this.readFile(this);
            $this.$modal.show();
            return false;
        });
        //client
        $(this.client.commentEditBtn).on("click", function(){
            $this.client.editComment();
            return false;
        });
        $(this.client.commentSaveBtn).on("click", function(){
            $this.client.saveComment();
            return false;
        });
        $(this.client.saveBtn).on("click", function(){
            $this.client.saveClient();
            return false;
        });
        $(".with-informer input").on("change", function(){
            $this.client.onChange(this);
        });
        $(".related-client-item").on("click", function(){
            $this.client.getClient($(this).attr("data-id"));
            return false;
        });
        //crop
        if ($(this.$modal.$cropArea + ' .cr-boundary').length){
            $(this.$modal.$cropArea + ' .cr-boundary').css('max-width', 450);
            $(this.$modal.$cropArea + ' .cr-boundary').css('width', 'auto');
        }
        $(this.$modal.$saveBtn).on("click", function(){
            $this.$uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                $($this.contact.base64Input).val(resp);
                $($this.contact.avatar).attr("src", resp);
                $this.$modal.hide();
                $this.contact.saveAvatar(resp);
            });
            return false;
        });
        $(this.$modal.$el + ' .close').on("click", function(){
            $this.$modal.hide();
            return false;
        });
        //documents upload
        $("#file-upload-trigger").on("click", function(){
            $("#file-upload").click();
            return false;
        });
        $("#file-upload").on("change", function(){
            $(this).closest("form").submit();
        });
        $(".remove-file").on("click", function(){
            $this.client.deleteFile($(this).attr("data-id"));
            return false;
        });
    },
    
    readFile: function(input) {
        var $this = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $this.$uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $($this.$modal.$cropArea).show();
                $($this.$modal.$cropArea).addClass('ready');
            };
            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert($this.messages[0]);
        }
    }
};
$(document).ready(function(){
    clientController.init();
});

/**
 * Uploading picture for client
 */
;(function () {
    var $clientPictureBox = $('#client-picture-box'),
        $pictureFilePicker = $('#picture-file-picker'),
        uploadUrl = $clientPictureBox.find('[data-picture-selector-btn]').data('url');

    var PictureUploader = function (uploadUrl, files) {
        this.files = files;
        this.uploadUrl = uploadUrl;
    };

    /**
     * Make form data with picture for ajax sending to server
     *
     * @param files
     * @returns FormData
     */
    PictureUploader.prototype.makeFormData = function (files) {
        var formData = new FormData();

        formData.append('picture', files[0]);

        return formData;
    };

    /**
     * Send ajax request to the server
     *
     * @param formData
     * @returns {*|c}
     */
    PictureUploader.prototype.sendAjaxRequest = function (formData) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: this.uploadUrl,
                data: formData,
                method: 'post',
                processData: false,
                contentType: false,
                success: function (response) {
                    resolve(response);
                }
            })
                .error(function (error) {
                    reject(error);
                });
        }.bind(this));
    };

    PictureUploader.prototype.upload = function () {
        var response = this.sendAjaxRequest(
            this.makeFormData(this.files)
        );

        return response;
    };

    var pictureUploadHandler = function(event) {
        var filePicker = event.target,
            files = filePicker.files;

        if (files.length === 0) {
            event.preventDefault();
        }

        var uploader = new PictureUploader(uploadUrl, files);

        uploader.upload()
            .then(function (response) {
                if (response.status === 'success') {
                    return $clientPictureBox.find('img').attr('src', response.picture);
                }

                return alert(response.message);
            })
            .catch(function (error) {
                alert('Ошибка при загрузке файла: ' + error.statusText);
            })
    };

    $clientPictureBox.on('click', '[data-picture-selector-btn]', function (event) {
        $pictureFilePicker.trigger('click');
        event.preventDefault();
    });

    $pictureFilePicker.on('change', pictureUploadHandler);
})();

$('[data-quick-edit]')
    .on('change', function (event) {
        var $input = $(this);

        var data = {
            name: $input.data('name') || $input.attr('name'),
            value: $input.val()
        };

        $input.parent().removeClass('has-error');
        $input.prop('disabled', true);

        $.ajax({
            url: $input.data('url'),
            data: data,
            method: 'post',
            success: function (response) {
                if (response.status === 'fail') {
                    $input.parent().addClass('has-error');
                    if (response.message) {
                        alert(response.message);
                    }
                }
            }
        })
            .always(function () {
                $input.prop('disabled', false);
            });
    });

$('[details-quick-edit]')
    .on('change', function (event) {
        var $input = $(this);
        var data = {
            name: $input.attr('name'),
            value: $input.val()
        };

        $input.parent().removeClass('has-error');
        $input.prop('disabled', true);

        $.ajax({
                url: $input.data('url'),
                data: data,
                method: 'post',
                success: function (response) {
                    if (response.status === 'fail') {
                        $input.parent().addClass('has-error');
                        if (response.message) {
                            alert(response.message);
                        }
                    }
                }
            })
            .always(function () {
                $input.prop('disabled', false);
            });
    });


//Дублируем работы с полями, может потом удалить двойники и снести это
$('#company-ownership-type, #legal_name').on('change, input', function(e) {
   $('#c-name').val($('#company-ownership-type').val() + ' ' + $('#legal_name').val()).change();
});

$('#c-name').on('change, input', function(e) {
    var value = e.currentTarget.value;
    var indexSpace = value.indexOf(' ');
    $('#company-ownership-type').val(value.substr(0, indexSpace));
    $('#legal_name').val(value.substr(indexSpace + 2));
});

$('input[name="address"]').on('change, input', function(e) {
    $('#c-post-address').val(e.currentTarget.value);
});

$('#c-post-address').on('change, input', function(e) {
    $('input[name="address"]').val(e.currentTarget.value);
});
//END

$('#company-client-type, #ltd-client-type').on('change', function (event) {
    var $this = $(this);

    $('#company-client-type, #ltd-client-type').val($this.val());

    var data = {
        name: 'is_company',
        value: 0
    };

    $this.parent().removeClass('has-error');

    $('#card > .overlay').removeClass('hidden');

    if ($this.val() === 'company') {
        data.value = 1;
    } else {
        data.value = 0;
    }

    $.ajax({
        url: $this.data('url'),
        data: data,
        method: 'post',
        success: function (response) {
            if (response.status === 'fail') {
                $this.parent().addClass('has-error');
            }

            if (response.status === 'success') {
                if ($this.val() === 'company') {
                    $('#private-person-wrapper').addClass('hidden');
                    $('#company-info-wrapper').removeClass('hidden');
                } else {
                    $('#private-person-wrapper').removeClass('hidden');
                    $('#company-info-wrapper').addClass('hidden');
                }
            }
        }
    })
        .always(function () {
            $('#card > .overlay').addClass('hidden');
        });
});

$('.connection-add-btn').on('click', '.dropdown-menu a', function (event) {
    var $this = $(this),
        $button = $this.closest('.connection-item-primary'),
        $wrapper = $this.closest('.connections-field'),
        name = $this.data('name');

    var $wrapper = $('<div>');
    $wrapper.addClass('col-sm-3 connection-item connection-additional');

    var $group = $('<div class="form-group"></div>');
    var $inputGroup = $('<div class="input-group"></div>');

    var $close = '<span class="input-group-btn">' +
                    '<button class="btn btn-default" type="button" data-close>' +
                        '<i class="fa fa-close"></i>' +
                    '</button>' +
                 '</span>';

    var $input = $('<input/>');
    $input.attr('name', name);
    $input.addClass('form-control');
    $input.attr('placeholder', $this.text());
    $input.attr('data-name', $this.data('name'));
    $input.attr('data-title', $this.text());

    $wrapper.append($group);
    $group.append($inputGroup);
    $inputGroup.append($close);
    $inputGroup.append($input);

    $button.before($wrapper);

    event.preventDefault();
});

var updateConnections = function ($inputs) {
    var values = [];

    $inputs.prop('disabled', true);

    $inputs.each(function (index, input) {
        values.push(
            {
                name: $(input).data('name'),
                value: $(input).val(),
                title: $(input).data('title')
            }
        );
    });

    $.ajax({
        url: '/api/client/' + CLIENT_ID + '/contacts',
        method: 'post',
        data: {
            connections: JSON.stringify(values)
        }
    })
        .always(function () {
            $inputs.prop('disabled', false);
        })
};

$('.connections-field').on('click', '[data-close]', function () {

    var $wrapper = $(this).closest('.connections-field');
    var $item = $(this).closest('.connection-additional');

    $item.remove();

    $items = $wrapper.find('.connection-additional input.form-control');

    updateConnections($items);
});

$('.connections-field').on('change', '.connection-additional input.form-control', function () {
    updateConnections(
        $(this).closest('.connections-field').find('.connection-additional input.form-control')
    );
});
