Vue.component('colorpicker', {
    template: '<input :id="identificator" :name="name" :placeholder="placeholder" :class="classes" :value="value"/>',

    props: ['name', 'id', 'placeholder', 'classes', 'value'],

    computed: {
        identificator: function () {
            return this.id || this.name + '-colorpicker-id';
        }
    },

    mounted: function () {
        $('#' + this.identificator).colorpicker();
    }
});