Vue.component('address-items', {
    template: '#addressComponentTemplate',

    data: function () {
        return {
            newAddress: '',
            addresses: []
        };
    },

    props: {
        placeholder: {
            type: String
        },

        name: {
            type: String,
            required: true
        },

        title: {
            type: String,
            required: true
        },

        items: {
            type: Array
        },

        errors: {
            type: String
        }
    },

    watch: {
        items: function () {
            this.addresses = this.items;
        }
    },

    computed: {
        hasErrors: function () {
            return this.errors;
        }
    },

    methods: {
        addAddress: function () {
            if (this.newAddress) {
                this.addresses.push(
                    this.newAddress
                );
            }

            this.newAddress = '';
        },

        deleteAddress: function (index) {
            this.addresses.splice(index, 1);
        }
    }
});
