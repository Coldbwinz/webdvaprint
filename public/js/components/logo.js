/**
 * Компонент для выбора, обрезки и загрузки изображений на сервер.
 *
 * <logo name="avatar"
 *       src="/path/to/current/user/avatar"
 *       hadler="/path/to/handler"
 *       width="400"
 *       height="400"
 *       class-list="img-responsive img-rounded"
 * ></logo>
 *
 * name - Уникальное имя компонента.
 * src - путь к текущему изображению пользователя, если есть
 * handler - путь к обработчику изображения на сервере
 * width - необходимая ширина изображения
 * height - необхожимая высота изображения
 * default - путь к изображению по умолчанию
 * class-list - классы, добавляемые к изображению
 */

Vue.component('logo', {
    template: '#logoComponentTempalate',

    props: {
        name: { required: true },
        handler: { required: true },
        width: { required: true },
        height: { required: true },
        src: String,
        default: String,
        classList: {
            default: ''
        }
    },

    computed: {
        identificator: function () {
            return this.name + 'Id';
        },

        modalId: function () {
            return this.identificator + 'Modal';
        },

        cropperAreaId: function () {
            return this.identificator + 'CropperArea';
        },

        hasImage: function () {
            return this.imageSrc;
        },

        onErrorImage: function () {
            return "this.src='" + this.default + "'";
        }
    },

    data: function () {
        return {
            file: null,
            imageSrc: null,
            base64: null,
            cropper: null,
            isBusy: false,
            dragMode: 'move'
        };
    },

    watch: {
        dragMode: function (newMode) {
            this.cropper.setDragMode(newMode);
        }
    },

    methods: {
        selectFileTrigger: function () {
            document.getElementById(this.identificator).click();
        },

        selectLogo: function (event) {
            if (event.target.files.length === 0) {
               return false;
            }

            var file = event.target.files[0];

            if (! this.isImage(file)) {
                alert('Неверное изображение. Выберите другое изображение в формате: jpeg, jpg, png, gif');
                return false;
            }

            this.file = file;
            this.showCropper();
        },

        isImage: function (file) {
            return ["image/jpeg", "image/png", "image/jpg", "image/gif"].indexOf(file.type) !== -1;
        },

        showCropper: function () {
            $('#' + this.modalId).modal('show');

            var reader = new FileReader();
            var vue = this;

            reader.onload = function (event) {
                vue.cropper.replace(event.target.result);
            }.bind(this);

            reader.readAsDataURL(this.file);
        },

        cropImage: function () {
            var vue = this;

            this.isBusy = true;

            this.imageSrc = this.cropper.getCroppedCanvas().toDataURL();

            // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
            this.cropper.getCroppedCanvas().toBlob(function (blob) {
                var formData = new FormData();

                formData.append(vue.name, blob);

                vue.uploadToServer(blob)
                    .then(function () {
                        vue.isBusy = false;
                        $('#' + vue.modalId).modal('hide');
                    });
            });
        },

        uploadToServer: function (blob) {
            var vue = this;

            return new Promise(function (resolve, reject) {
                var formData = new FormData();
                formData.append(vue.name, blob);
                formData.append('width', vue.width);
                formData.append('height', vue.height);

                $.ajax({
                    url: vue.handler,
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        resolve(response);
                    }
                })
                    .error(function (error) {
                        reject(error);
                    });
            });
        },

        zoom: function (value) {
            this.cropper.zoom(value);
        }
    },

    mounted: function () {
        this.imageSrc = this.src;

        var canvas = document.getElementById(this.cropperAreaId);
        this.cropper = new Cropper(canvas, {
            aspectRatio: this.width / this.height,
            minContainerHeight: 500
        });
    }
});