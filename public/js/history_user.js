var user_count_timout = 0;
$(document).ready( function() {
    $('#div_pagination_user').on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url = new URL($(this).attr('href'));
        var page = url.searchParams.get("page");
        $('input[name="page"]').val(page);
        $('input[name="change"]').val(0);
        userFilterActivate();
        return false;
    });

    $('input[name="user_search"]').keyup(function () {
        user_count_timout++;
        setTimeout(function () {
            user_count_timout--;
            if (user_count_timout == 0) {
                $('input[name="change"]').val(1);
                userFilterActivate();
            }
        }, 1000);
    });

    var filterActivated = false;
    function userFilterActivate() {
        if (!filterActivated) {
            var frm = $('.top_search_box form');
            filterActivated = true;
            startLoadingAnimation();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: frm.serialize(),
                method: "POST",
                url: $('input[name="href"]').val(),
                error: function (xhr, status, error) {
                    filterActivated = false;
                    stopLoadingAnimation();
                },
                success: function (data) {
                    filterActivated = false;
                    stopLoadingAnimation();
                    $('#table_history_block').html(data.html);
                    $('#div_pagination_user').html(data.pagination);
                }
            });
        }
    }
});