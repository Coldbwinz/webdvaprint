"use strict";

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.url = function(link) {
    return window.Laravel.baseUrl + '/' + link;
};

function confirmDelete() {

 if (confirm("Вы подтверждаете удаление?")) {
  return true;
 } else {
  event.preventDefault();
  return false;
 }
}
$(document).ready(function() {
    $('.select2').select2();

    $('.select2-min').select2({
        minimumResultsForSearch: Infinity
    });
});

// Скрывает элемент, указанный в дата атрибуте data-target=".selector"
$('[data-action="hide-form"]').on('click', function () {
    var target = $(this).data('target');
    $(target).hide();
});
