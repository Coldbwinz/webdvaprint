$(function () {
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('#uploaded-image').show();
                $('#uploaded-image').addClass('ready');
            };

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Извините - ваш браузер не поддерживает FileReader API");
        }
    }

    $uploadCrop = $('#uploaded-image').croppie({
        enableExif: true,
        viewport: {
            width: 150,
            height: 150,
            type: 'circle'
        },
        boundary: {
            width: 450,
            height: 150
        }
    });

    if ($('#uploaded-image .cr-boundary').length) {
        $('#uploaded-image .cr-boundary').css('max-width', 450);
        $('#uploaded-image .cr-boundary').css('width', 'auto');
    }

    $('#upload').on('change', function () {
        readFile(this);
    });

    $('#btn-croppie').on('click', function (ev) {
        console.log($uploadCrop);
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            $('.modal').hide();
        });
    });
});
