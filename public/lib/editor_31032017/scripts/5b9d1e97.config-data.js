define('config-data', function () {
    'use strict';

    /* jshint ignore:start */
    return {
        'version': '2017.2.1',
        'saveReport': '/lib/frx/save.php?=#{id}',
        'makePreview': '/lib/editor/save.php?=#{id}',
        'getReport': '/lib/frx/show.php?uuid=#{id}&v=#{rand_hash}',
        'getFunctions': '../FastReport.Export.axd?getFunctions=#{id}',
        'cookieName': 'ARRAffinity',


        'locales-path':'/lib/editor//' +'locales/#{locale}.js',
        'templates-path': 'views/#{name}.tmpl.html',
        'images-path':'/lib/editor//' +'images/#{image}',

        'scale-mobile': 0.6,
        'scale': 1,

        'grid': 9.45, // 9.45 px === 0.25 cm

        'sticky-grid': false,

        'hotkeyProhibited': false,

        'dasharrays': {
            'DashDot': '9, 2, 2, 2',
            'Dot': '2, 2',
            'Solid': '',
            'Dash': '9, 3',
            'DashDotDot': '9, 2, 2, 2, 2, 2'
        },

        'font-names': [
            'Calibri', 'Calibri Light', 'Comic Sans MS', 'Consolas', 'Constantia', 'Courier New',
            'Georgia', 'Impact',
            'Tahoma', 'Times New Roman',
            'Trebuchet MS', 'Verdana',
            'Droid Sans Mono', 'Ubuntu Mono',
            /* WebFont which could not be loaded are listed below */
            'Microsoft Sans Serif', 'Palatino Linotype', 'Lucida Console', 'Lucida Sans Unicode', 'Segoe Print', 'Segoe Script', 'Segoe UI', 'Segoe UI Symbol', 'Sylfaen', 'Symbol', 'Webdings', 'Wingbings', 'Cambria', 'Arial', 'Candara', 'Corbel', 'Franklin Gothic', 'Gabriola', 'Cambria Math'
        ],
        'default-font-name': 'Arial',
        'preload-fonts': ['Tahoma', 'Arial', 'Calibri', 'Calibri Light', 'Verdana'],
        'prefetch-fonts': [],

        'brackets': '[,]',

        'band-indent-top': 9.448,
        'band-indent-opacity': 0.3,

        'minComponentWidthForResizingElements': 40,
        'minComponentHeightForResizingElements': 40,

        'rectButtonWidth': 15,
        'rectButtonHeight': 15,
        'rectButtonFill': '#c25853',

        'circleButtonWidth': 6,
        'circleButtonHeight': 6,
        'circleButtonRadius': 5,
        'circleButtonWidth-mobile': 12,
        'circleButtonHeight-mobile': 12,
        'circleButtonRadius-mobile': 10,

        'resizingBandBlockWidth': 100,
        'resizingBandBlockHeight': 15,

        'polylineStroke': '#c25853',
        'polylineStrokeWidth': '1px',
        'selectedPolylineStrokeWidth': '2.5px',
        'polylineFill': 'none',
        'polylineWidth': 4,

        'lineMovingScope': 30,

        'guides': true,

        'colors': {
            'button-circle': '#c25853',
            'angle-slider': '#c25853',
            'default-band-separator': '#C0C0C0',
            'selected-band-separator': '#2B579A'
        },

        'show-band-title': false,
        'add-bands': false,
        'resize-bands': false,
        'movable-components': true,
        'resizable-components': true,

        'customization': {
            'preview': {
                'enable': true,
                'shown': true,
                'button': false,
                'header': false,
                'background': 'initial',
                'container': 'horz',
                'width': 100,
            },
            'component-viewer': {
                'enable': true,
                'shown': true,
                'button': false,
                'header': true,
                'hasBorder': true,
                'background': 'initial',
            },
        },

        'fadeout': 150,

        // home, view, components, bands, page
        // for classic theme
        'default-tab-menu': 'home',

        // default, small, large
        'show-saving-progress': 'default',

        // default, html5, false,
        'notifications': 'default',
        'notifications-mobile': 'default',

        "colors":{"toolbar":"#fafafa","bottom":"#fafafa","decor-border":"#3c8dbc","button-circle":"#c25853","angle-slider":"#c25853"},"image_prefix":"/lib/frx/","_token":"4p3k3ZSz5QzkLHTbxPeyRghFIc1yIwv7psfWrUY2","images_path":"/lib/frx/#{uuid}/#{filename}","upload_path":"/api/images-add","convert_to_pdf_text":"Идет процесс конвертации pdf","save_buttons":"2"
    };
    /* jshint ignore:end */
});
