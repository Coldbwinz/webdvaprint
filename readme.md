# w2p

## Вход на сайт

```bash
php artisan db:seed --class=UsersTableSeeder
```

Эта команда добавит нового пользователя в группу "root". Логин и пароль для входа:

```
email: admin@w2p.ru
password: admin
```

Дополнительные данные можно посмотреть/изменить в файле `database/seeds/UsersTableSeeder.php`