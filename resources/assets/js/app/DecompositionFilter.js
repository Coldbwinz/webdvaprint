class DecompositionFilter {
    constructor(values = {}) {
        /**
         * Раскладка
         */
        this.layout = values.layout || 1;

        /**
         * Печатный лист
         */
        this.sheet = values.sheet || null;

        /**
         * Формат печатного листа
         *
         * @type {null}
         */
        this.printSheetFormat = values.printSheetFormat || { width: null, height: null };

        /**
         * Цветность
         */
        this.chromacity = values.chromacity || ['1 + 1'];

        /**
         * Вариант спуска
         */
        this.descent = values.descent || 'picking';
    }
}

export default DecompositionFilter;
