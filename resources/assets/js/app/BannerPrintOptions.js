import PrintColor from "./PrintColor";

class BannerPrintOptions {
    constructor() {
        this.maxPrintSheetFormat = { width: null, height: null };
        this.technologicalFields = null;
        this.valve = { 'value': 0, 'side': 'wide' };
        this.chromaticity = new PrintColor();
    }
}

export default BannerPrintOptions;
