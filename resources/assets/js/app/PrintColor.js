class PrintColor {
    constructor() {
        this.chromaticity = 'fullColor';
        this.click_price = { value: 0, currency: 'RUB' };
        this.performance = null;
        this.rebuilding = null;
        this.fitting_1 = 0;
        this.fitting_2 = 0;
        this.defect_norm = 0;
    }
};

export default PrintColor;
