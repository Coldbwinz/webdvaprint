window._ = require('lodash');

/**
 * Axios initialization
 */
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'baseURL': window.Laravel.baseUrl,
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

/**
 * Routes for ajax requests
 */
import Route from './routes';
window.Route = Route;

import helpers from './core/functions';
window.helpers = helpers;

require("./core/download");
