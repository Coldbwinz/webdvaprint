require('./bootstrap');

//Vue.component('queue-prepress-table', require('./components/tables/queue-prepress-table'));
Vue.component('queue-print-table', require('./components/tables/queue-print-table'));
Vue.component('queue-postpress-table', require('./components/tables/queue-postpress-table'));
Vue.component('queue-design-table', require('./components/tables/design-table'));

Vue.component('prepress-table', require('./components/tables/prepress-table'));

Vue.component('thumbnail', require('./components/html/Thumbnail.vue'));
Vue.component('alert', require('./components/html/Alert.vue'));
Vue.component('box', require('./components/html/Box.vue'));
Vue.component('panel', require('./components/html/Panel.vue'));
Vue.component('tr-panel', require('./components/html/TrPanel.vue'));

Vue.component('create-material-form', require('./components/dictionary/material/CreateMaterialForm.vue'));
Vue.component('edit-material-form', require('./components/dictionary/material/EditMaterialForm.vue'));
Vue.component('create-equipment-form', require('./components/dictionary/equipment/CreateEquipmentForm.vue'));
Vue.component('edit-equipment-form', require('./components/dictionary/equipment/EditEquipmentForm.vue'));

Vue.component('sheet-format-select', require('./components/form/select/SheetFormatSelect.vue'));
Vue.component('editable-select', require('./components/form/select/EditableSelect.vue'));
Vue.component('material-type-select', require('./components/form/select/MaterialTypeSelect.vue'));
Vue.component('cover-type-select', require('./components/form/select/CoverTypeSelect.vue'));
Vue.component('price-input', require('./components/form/PriceInput.vue'));

Vue.component('dashboard', require('./components/dashboard/Dashboard.vue'));
Vue.component('v-select', require('vue-select'));
Vue.component('overlay', require('./components/html/Overlay.vue'));

Vue.component('edit-motivation-form', require('./components/employee/EditMotivationForm.vue'));
Vue.component('input-group', require('./components/form/InputGroup.vue'));
Vue.component('input-dropdown', require('./components/form/InputDropdown.vue'));
Vue.component('employee-add-bonus-form', require('./components/employee/AddBonusForm.vue'));
Vue.component('salary-filter-form', require('./components/employee/SalaryFilterForm.vue'));

/**
 * Convert unit timestamp to format
 */
Vue.filter('unixDate', (value, format) => {
    if (value) {
        return moment.unix(value).format(format);
    }

    return null;
});
/**
 * Get full path to the order preview
 */
Vue.filter('imageFullPath', url => {
    return '/lib/frx/' + url;
});

/**
 * Date format
 *
 * @link https://momentjs.com/docs/#/displaying/format/
 */
Vue.filter('date_format', (value, format) => {
    if (_.isObject(value)) {
        return moment(value.date).format(format);
    }

    return moment(value).format(format);
});

/**
 * Format object to string
 */
Vue.filter('formatToString', (format) => {
    if (format.width && format.height) {
        return format.width + 'x' + format.height;
    }

    return 'Выберите формат';
});

new Vue({
    el: '#root'
});
