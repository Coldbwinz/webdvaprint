/**
 * Класс для обработки ошибок валидации Laravel
 */
class ValidationError {
    constructor(errorObject = {}) {
        this.errors = errorObject;
    }

    /**
     * Make from laravel response
     *
     * @param response
     * @returns {ValidationError}
     */
    static fromResponse(response) {
        if (response.status === 422) {
            return new ValidationError(response.data);
        }

        return new ValidationError();
    }

    /**
     * Возвращает весь объект с ошибками
     *
     * @returns {*}
     */
    all() {
        return this.errorObject;
    }

    /**
     * Преобразует объект в массив
     *
     * @returns {Array}
     */
    toArray() {
        return Object.keys(this.errors).map((objectKey, index) => {
            return this.errors[objectKey][0];
        });
    }

    /**
     * Get the validation error for the givven key
     *
     * @param key
     * @returns {*}
     */
    first(key) {
        if (this.errors[key]) {
            return this.errors[key][0];
        }

        return null;
    }

    /**
     * Check for errors for the givven key
     *
     * @param key
     * @returns {boolean}
     */
    has(key) {
        return this.errors[key] !== undefined;
    }
}

export default ValidationError;
