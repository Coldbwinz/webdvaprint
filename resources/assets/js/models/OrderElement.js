class OrderElement {

    /**
     * Get all order elements
     *
     * @param params
     */
    static all(params) {
        return axios.get('/api/order-elements', {
            params: params
        });
    }
}

export default OrderElement;
