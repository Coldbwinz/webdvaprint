class GroupOrder {

    /**
     * Get all perform orders
     */
    static all(filter = {}) {
        return axios.get(Route.group_order, {
            params: filter
        });
    }

    /**
     * Group Orders for design queue table
     *
     * @param filter
     */
    static designQueue(filter = {}) {
        return axios.get('/api/design/group-order', {
            params: filter
        });
    }

}

export default GroupOrder;
