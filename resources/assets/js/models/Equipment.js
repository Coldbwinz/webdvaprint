class Equipment {
    constructor(data = {}) {
        this.print_type_id = data.print_type_id || null;
        this.equipment_name = data.equipment_name || null;
        this.max_print_sheet_format = data.max_print_sheet_format || { width: null, height: null };
        this.technological_fields = data.technological_fields || null;
        this.valve = data.valve || { value: null, side: 'wide' };
        this.chromaticity = data.chromaticity || {
                chromaticity: 'fullColor',
                clickPrice: { value: null, currency: 'RUB' },
                performance: null,
                rebuilding: null,
                fitting_1: null,
                fitting_2: null,
                defectNorm: null
            };
        this.banner_print = data.banner_print || null;
    }

    /**
     * Find equipment by Id
     *
     * @param equipmentId
     * @returns {AxiosPromise}
     */
    static find(equipmentId) {
        return axios.get(`/api/equipments/${equipmentId}`);
    }

    /**
     * Store new equipment
     *
     * @param equipmentData
     * @returns {*|AxiosPromise}
     */
    static store(equipmentData) {
        return axios.post(`/api/equipments`, equipmentData);
    }

    /**
     * Update the equipment
     *
     * @param equipmentId
     * @param data
     * @returns {AxiosPromise}
     */
    static update(equipmentId, data) {
        return axios.patch(`/api/equipments/${equipmentId}`, data);
    }
}

export default Equipment;
