class PrintSheetFormat {

    /**
     * Get all print sheet formatsa
     */
    static all(params = null) {
        return axios.get('/api/sheet-format', {
            params: params
        });
    }

    /**
     * Save new format to the database
     *
     * @param data
     * @returns {*|AxiosPromise}
     */
    static make(params) {
        return axios.post('/api/sheet-format', params);
    }
}

export default PrintSheetFormat;
