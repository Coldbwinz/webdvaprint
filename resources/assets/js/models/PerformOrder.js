class PerformOrder {
    /**
     * Get all perform orders
     */
    static all(filter = {}) {
        return axios.get(Route.perform_order, {
            params: filter
        });
    }

    /**
     * Store new perform order
     *
     * @param data
     * @returns {*|AxiosPromise}
     */
    static store(data) {
        return axios.post(Route.store_perform_order, data);
    }

    /**
     * Get next index number
     */
    static getPerformOrderNextNumber() {
        return axios.get(Route.perform_order_next_number);
    }

    /**
     * download descent
     *
     * @param performOrder
     */
    static downloadDescent(performOrder) {
        return axios.get(`/api/perform-order/${performOrder.id}/download-descent`);
    }

    /**
     * Send to print
     *
     * @param performOrder
     */
    static sendToPrint(performOrder) {
        return axios.get(`/api/perform-order/${performOrder.id}/printed`);
    }

    /**
     * @param performOrder
     * @returns {AxiosPromise}
     */
    static downloadTemplates(performOrder) {
        return axios.patch(`/api/perform-order/${performOrder.id}/download-templates`);
    }

    /**
     * Set perform order ready
     *
     * @param performOrder
     * @returns {AxiosPromise}
     */
    static setReady(performOrder) {
        return axios.patch(`/api/perform-order/${performOrder.id}/set-ready`);
    }

    /**
     * Upload descent file and link to the server
     * 
     * @param performOrderId
     * @param data
     * @returns {*|AxiosPromise}
     */
    static uploadDescent(performOrderId, data) {
        let url = `/api/perform-order/${performOrderId}/upload-descent`;

        let formData = new FormData();

        for (let index in data) {
            formData.append(index, data[index]);
        }

        return axios.post(url, formData);
    }

    /**
     * Pause perform order
     *
     * @param performOrderId
     * @param reason
     * @returns {AxiosPromise}
     */
    static pause(performOrderId, reason) {
        return axios.patch(`/api/perform-order/${performOrderId}/pause`, { reason });
    }

    /**
     * Resume perform order
     *
     * @param performOrderId
     * @param reason
     * @returns {AxiosPromise}
     */
    static resume(performOrderId, reason) {
        return axios.patch(`/api/perform-order/${performOrderId}/resume`, { reason });
    }

    /**
     * Change perform order processing status
     *
     * @param performOrderId
     * @param pause
     * @param reason
     * @returns {AxiosPromise}
     */
    static changeProccessingStatus(performOrderId, pause, reason) {
        if (pause) {
            return PerformOrder.pause(performOrderId, reason);
        }

        return PerformOrder.resume(performOrderId, reason);
    }
}

export default PerformOrder;
