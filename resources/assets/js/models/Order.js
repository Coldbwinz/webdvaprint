class Order {
    /**
     * Get all orders with filter
     *
     * @param filters
     */
    static all(filters = {}) {
        let params = Object.assign({
            page: this.currentPage
        }, filters);

        return axios.get('/api/order', {
            params: params
        });
    }

    /**
     * Get technical task for order
     *
     * @param order
     */
    static technicalTask(order) {
        return axios.get(`/api/design/order/${order.id}/technical-task`);
    }

    /**
     * Upload template for approval
     *
     * @param order
     * @param file
     * @returns {*|AxiosPromise}
     */
    static uploadForApproval(order, file) {
        let formData = new FormData();
        formData.append('template', file);

        return axios.post(`/api/design/order/${order.id}/upload-for-approval`, formData);
    }

    /**
     * Upload approved template
     *
     * @param order
     * @param file
     * @returns {*|AxiosPromise}
     */
    static uploadApproved(order, file) {
        let formData = new FormData();
        formData.append('template', file);

        return axios.post(`/api/design/order/${order.id}/upload-approved`, formData);
    }
}

export default Order;
