class Material {

    constructor(data = {}) {
        this.material_type_id = data.material_type_id || null;
        this.cover_type_id = data.cover_type_id || null;
        this.name = data.name || null;
        this.price = data.price || { value: 0, currency: 'RUB' };
        this.initial_sheet_format = data.initial_sheet_format || { width: null, height: null };
        this.print_sheet_format = data.print_sheet_format || { width: null, height: null };
        this.print_sheets_number = null;
        this.lamination = data.lamination || false;
        this.density_items = data.density_items || [];
    }

    /**
     * Find material by id
     *
     * @param materialId
     * @returns {AxiosPromise}
     */
    static find(materialId) {
        return axios.get(`/api/materials/${materialId}`)
    }

    /**
     * Store new materials
     *
     * @param data
     * @returns {AxiosPromise}
     */
    static store(data) {
        return axios.post('/api/materials', data);
    }

    /**
     * Update the material
     *
     * @param materialId
     * @param data
     * @returns {AxiosPromise}
     */
    static update(materialId, data) {
        return axios.patch(`/api/materials/${materialId}`, data);
    }
}

export default Material;
