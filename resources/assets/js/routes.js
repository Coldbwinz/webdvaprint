/**
 * Содержит в себе объект URLов и их алиасов
 */
export default {

    perform_order: '/api/perform-order',

    store_perform_order: '/api/perform-order',

    perform_order_next_number: '/api/perform-order/get-next-index',

    group_order: '/api/group-order'

};