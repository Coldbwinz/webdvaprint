class MotivationScheme {
    constructor(data = {}) {
        this.percent = data.persent || 0;
        this.salary = data.salary || 'standart';
        this.premium = data.premium || 'standart';
        this.bonus = data.bonus || 'standart';
        this.bonus_coefficient = data.bonus_coefficient || 1;
    }
}

export default MotivationScheme;
