class QueueFilter {

    constructor(values = {}) {
        /**
         * Дата готовности заказа
         */
        this.range = values.range || null;

        /**
         * Печатный лист
         */
        this.sheet = values.sheet || null;

        /**
         * Формат изделия
         */
        this.format = values.format || null;

        /**
         * Формат печатного листа
         *
         * @type {null}
         */
        this.printSheetFormat = values.printSheetFormat || { width: null, height: null };

        /**
         * Раскладка
         */
        this.layout = values.layout || 1;

        /**
         * Цветность
         */
        this.chromacity = values.chromacity || [];

        /**
         * Постпресс
         */
        this.postpress = values.postpress || [];
    }
}

export default QueueFilter;
