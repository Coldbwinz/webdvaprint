import { Doughnut } from 'vue-chartjs'

export default Doughnut.extend({
    data () {
        return {
            datacollection: null
        }
    },

    mounted () {
        axios.get('api/statistic-orders')
            .then(response => {
                this.renderChart(response.data, {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    animation: {
                        duration: 2000,
                        easing: 'easeOutBounce'
                    }
                });
            });
    }
})
