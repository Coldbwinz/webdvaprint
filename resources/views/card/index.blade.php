@extends('app')

@section('content')
    <script src="/js/clientController.js"></script>
    <div class="row" id="card">
        <div class="col-lg-4">
            <div class="box box-primary">
                <form role="form" class="form-horizontal" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="user-avatar">
                                <a href="#" id="change-photo">
                                    <img src="/images/nophoto.png" />
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="users-list-name" id="name-placeholder">
                                <div class="col-sm-12">
                                    <span>Богдан Александрович Одарченко</span>
                                    <input type="text" class="form-control" style="display: none;" id="name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3">Почта:</label>
                            <div class="col-sm-9" id="email-placeholder">
                                <span>web@2-up.ru</span>
                                <input type="email" class="form-control" style="display:none;" id="email" placeholder="Введите E-Mail" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-3">Телефон:</label>
                            <div class="col-sm-9" id="phone-placeholder">
                                <span>8-888-888-88-88</span>
                                <input type="text" class="form-control" style="display:none;" id="phone" placeholder="8-888-888-88-88" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="position" class="col-sm-3">Должность: </label>
                            <div class="col-sm-9" id="position-placeholder">
                                <span>Директор</span>
                                <input type="text" class="form-control" style="display:none;" id="position" placeholder="Директор" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-default btn-full-width" id="contact-edit-btn">Редактировать</button>
                                <button type="submit" class="btn btn-primary btn-full-width" id="contact-save-btn" style="display:none;">Сохранить</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-danger btn-full-width">Новый заказ</button>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <div class="col-sm-12" id="comment-placeholder">
                                <div class="form-group-title">
                                    Комментарий 
                                    <button class="btn btn-default btn-flat pull-right" id="edit-user-comment" title="Редактировать"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-default btn-flat pull-right" id="save-user-comment" title="Сохранить" style="display: none;"><i class="fa fa-save"></i></button>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="text-muted" id="comment-text">Текст комментария</p>
                                <textarea class="form-control" id="comment" style="display: none;"></textarea>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group-title">
                                    Дополнительные контакты 
                                    <button class="btn btn-default btn-flat pull-right" id="add-new-contact" title="Добавить контакт"><i class="fa fa-plus"></i></button>
                                    <div class="clearfix"></div>
                                </div>
                                <ul class="contacts-list">
                                    <li><a href="#">Росляков Алексей</a></li>
                                    <li><a href="#">Андрей Наумов</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
        </div>
        <div class="col-lg-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">О клиенте</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Заказы</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Реквизиты</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Документы</a></li>
                    <li class="has-debt"><button class="btn btn-warning"><i class="fa fa-warning"></i>Ожидает оплаты</button></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="c-name" class="col-sm-2 control-label">Название</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-name" placeholder="2UP" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="c-email" class="col-sm-2 control-label">Почта</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="c-email" placeholder="web@2-up.ru" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="c-phone" class="col-sm-2 control-label">Телефон</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-phone" placeholder="8-888-888-88-88" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="c-sector" class="col-sm-2 control-label">Отрасль</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-sector" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 comapny-controls">
                                        <a href="#" class="btn btn-default pull-right" id="add-company">Добавить компанию</a>
                                        <a href="#" class="btn btn-primary pull-right" id="save-company">Сохранить</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="col-sm-12" id="company-comment-placeholder">
                                        <div class="form-group-title">
                                            Комментарий 
                                            <button class="btn btn-default btn-flat pull-right" id="edit-company-comment" title="Редактировать"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-default btn-flat pull-right" id="save-company-comment" title="Сохранить" style="display: none;"><i class="fa fa-save"></i></button>
                                            <div class="clearfix"></div>
                                        </div>
                                        <p class="text-muted" id="company-comment-text">Текст комментария</p>
                                        <textarea class="form-control" id="company-comment" style="display: none;"></textarea>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="form-group-title">
                                            Привязанные компании
                                        </div>
                                        <ul class="contacts-list">
                                            <li><a href="#">МфСейл</a></li>
                                            <li><a href="#">В2В</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="box-header">
                            <div class="box-tools">
                                <ul class="pagination pagination-sm no-margin pull-left">
                                    <li><a href="#">&laquo;</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">&raquo;</a></li>
                                </ul>
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover orders-list">
                                <tr>
                                    <th>ID</th>
                                    <th>Дата</th>
                                    <th>Время</th>
                                    <th>Статус</th>
                                    <th class="actions">Действие</th>
                                </tr>
                                <tr>
                                    <td><a href="#">#1</a></td>
                                    <td>01.01.2016</td>
                                    <td>17:48</td>
                                    <td><span class="label label-success">Готово</span></td>
                                    <td class="actions">
                                        <button class="btn btn-flat btn-danger" title="Удалить"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#2</a></td>
                                    <td>01.01.2016</td>
                                    <td>17:48</td>
                                    <td><span class="label label-danger">Отменен</span></td>
                                    <td class="actions">
                                        <button class="btn btn-flat btn-danger" title="Удалить"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#3</a></td>
                                    <td>01.01.2016</td>
                                    <td>17:48</td>
                                    <td><span class="label label-warning">Ожидает</span></td>
                                    <td class="actions">
                                        <button class="btn btn-flat btn-danger" title="Удалить"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="c-inn" class="col-sm-2 control-label">ИНН</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-inn" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="c-ogrn" class="col-sm-2 control-label">ОГРН</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-ogrn" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="c-bank" class="col-sm-2 control-label">Банк</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="c-bank" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tab_4">
                        <form>
                            <div class="form-group">
                                <button class="btn btn-flat btn-primary pull-right" id="file-upload-trigger">Загрузить файл</button>
                                <input type="file" id="file-upload" style="display: none;" />
                                <div class="clearfix"></div>
                            </div>
                        </form>
                        <table class="table">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Файл</th>
                                <th class="actions text-right">Действие</th>
                            </tr>
                            <tr>
                                <td><a href="#">#1</a></td>
                                <td><a href="#">Документ 1</a></td>
                                <td class="actions text-right">
                                    <button class="btn btn-flat btn-danger" title="Удалить"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">#2</a></td>
                                <td><a href="#">Документ 2</a></td>
                                <td class="actions text-right">
                                    <button class="btn btn-flat btn-danger" title="Удалить"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop