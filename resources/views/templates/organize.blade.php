@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="box-title">Шаблоны</h3>
                        </div>
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    {!! Form::open(['method' => 'patch']) !!}

                    <table id="parser-files-table" class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Тематики</th>
                            <th>Цвета</th>
                            <th>Коллекция</th>
                            <th>Тип</th>
                            <th>Миниатюра</th>
                        </tr>
                        @foreach ($templates as $template)
                            <tr class="{{ $template->activity ? '' : 'danger' }}">
                                {{-- ID --}}
                                <td>{{ $template->id }}</td>

                                {{-- Тематики --}}
                                <td>
                                    <div class="form-group">
                                        <div class="themes-selector">
                                            @foreach($themes as $theme)
                                                <label for="theme-{{ $template->id }}-{{ $theme->id }}"
                                                       title="{{ $theme->tags->implode('name', ', ') }}"
                                                >
                                                    <input id="theme-{{ $template->id }}-{{ $theme->id }}"
                                                           type="checkbox"
                                                           name="templates[{{ $template->collection }}][themes][]"
                                                           value="{{ $theme->id }}"
                                                           {{ in_array($theme->id, $template->themes->lists('id')->toArray()) ? ' checked' : '' }}
                                                    />
                                                    {{ $theme->name }}
                                                </label><br>
                                            @endforeach
                                        </div>
                                    </div>
                                </td>

                                {{-- Цвета --}}
                                <td>
                                    @foreach(\App\ThemeColor::all() as $themeColor)
                                        <label for="color-{{ $template->id }}-{{ $themeColor->id }}">
                                            <input id="color-{{ $template->id }}-{{ $themeColor->id }}"
                                                   type="checkbox"
                                                   name="templates[{{ $template->collection }}][colors][]"
                                                   value="{{ $themeColor->id }}"
                                                    {{ in_array($themeColor->id, $template->colors->lists('id')->toArray()) ? ' checked' : '' }}
                                            />
                                            {{ $themeColor->color }}
                                        </label><br>
                                    @endforeach
                                </td>

                                {{-- Коллекция --}}
                                <td>
                                    <p>{{ $template->collection }}</p>

                                    <button type="button"
                                            class="btn btn-xs{{ $template->activity ? ' btn-danger' : ' btn-success' }}"
                                            data-collection="{{ $template->collection }}"
                                            data-action="{{ $template->activity ? 'disable' : 'enable' }}"
                                    >
                                        @if ($template->activity)
                                            <i class="fa fa-ban"></i> Отключить коллекцию
                                        @else
                                            <i class="fa fa-check"></i> Включить коллекцию
                                        @endif
                                    </button>
                                </td>

                                {{-- Тип шаблона --}}
                                <td>{{ $template->type_key }}</td>

                                {{-- Миниатюра --}}
                                <td>
                                    <div class="text-center">
                                        <a href="{{ url($template->url) }}" target="_blank">
                                            <img src="{{ url($template->url) }}" alt="{{ $template->collection }}" height="100">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Сохранить
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

            {!! $templates->appends(Request::all())->render() !!}
            </nav>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('[data-collection]').on('click', function (event) {
            var $button = $(this),
                data = {
                    collection: $button.data('collection'),
                    action: $button.data('action')
                };

            var message = 'Это действие отключит ВСЕ шаблоны в коллекции. Продолжить?';
            if (data.action == 'enable') {
                message = 'Это действие активирует ВСЕ шаблоны в коллекции. Продолжить?';
            }

            if (! confirm(message)) {
                return false;
            }

            $button.button('loading');

            $.post('{{ url('templates/organize/activity') }}', data, function (response) {
                if (data.action == 'enable') {
                    $button.closest('tr').removeClass('danger');
                } else {
                    $button.closest('tr').addClass('danger');
                }

                $button.remove();
            })
                .error(function () {
                    alert('Возникла ошибка');
                    $button.button('reset');
                });

            event.preventDefault();
        });
    </script>
@endsection
