@extends('app')

@section('content')
<?php /*
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default box-solid collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Добавить новый шаблон в формате w2p</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <form method="POST" action="/template/new" autocomplete="off" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body" style="display: none">


            <div class="form-group">
              <label>Название</label>
               <input class="form-control" type="text" name="name" value="{{ old('name') }}">
            </div>

            <div class="form-group">
              <label>Описание</label>
               <input class="form-control" type="text" name="description" value="{{ old('position') }}">
            </div>

            <div class="form-group">
              <label>Типы продукции</label>
              <select class="form-control" name="type">
                @foreach ($product_types as $value)
                  <option  value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach   
              </select>
            </div>

            <div class="form-group">
              <label>Превью</label>
              <input name="photo" type="file" />
            </div>

            <div class="form-group">
              <label>Шаблон W2P</label>
              <input name="w2p" type="file" />
            </div>        
            

          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Добавить</button>
          </div>
        </form>
      </div><!-- /.box -->
    </div>    
  </div>
*/?>

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body table-responsive no-padding">        
          <table class="table table-hover">
            
            <tbody><tr>
              <th width="70">Номер</th>
              <th width="240">Превью</th>
              <th>Клиент</th>               
              <th width="200">Тип продукции</th>              
              <th width="140">Включен</th>
              <th width="250">Действия</th>              
            </tr>

           @foreach ($templates as $value)
            <tr id="template-id-{{ $value->id }}">
              <td>{{ $value->id }}</td>
              <td><center><img src="{{ url($value->url) }}" alt="" style="max-height: 100px"></center></td>
              <td>@if ($value->client) 
                  <a href="/orders/new-step1/{{ $value->client->id }}">{{ $value->client->name }}</a>
                  @endif
              </td>               
              <td>{{ $value->type_name }}</td>
              <td>
                @if ($value->activity == 1) 
                <a href="/template/off/{{ $value->id }}"><button class="btn btn-danger btn-xs">выключить</button></a>
                @else 
                <a href="/template/on/{{ $value->id }}"><button class="btn btn-success btn-xs">включить</button></a>
                @endif
              </td>
              <td>                
<!--
                <a href="/template/edit/{{ $value->id }}"><button class="btn btn-success btn-xs">редактировать</button></a>
                <a href="/designer/edit/{{ $value->id }}"><button class="btn btn-success btn-xs">Designer</button></a>
-->
                <button class="btn btn-danger btn-xs delete-button" data-id="{{ $value->id }}">удалить</button>                
              </td>
            </tr>
           @endforeach   
          </tbody></table>
            
            {!! (new Landish\Pagination\Pagination($templates))->render() !!}
            
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>

<script>
$(document).ready(function(){
    $('.delete-button').click(function(){
        if (confirm('Вы уверены что хотите удалить шаблон?')) {
            var id = $(this).data('id');
            $.ajax({
                url: '/template/delete/'+id, 
                dtataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $('#template-id-'+id).hide(200);
                    }
                },
            });
        }
    });
})
</script>
@stop