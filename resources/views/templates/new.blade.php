@extends('app')

@section('content')

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body no-padding">

            <form method="POST" action="/template/store/{{ $client_id }}/{{ $type_id }}" autocomplete="off" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="choise_side" value="1" />
            <div class="box-body">

              <div class="btn-group pb20">
                  <button type="button" class="btn btn-default choise-side" data-choise="1">1 сторона</button>
                  <button type="button" class="btn btn-default choise-side" data-choise="2">2 стороны</button>
              </div>

              <div class="form-group">
                <label>Шаблон PSD</label>
                <input name="psd" type="file" accept=".psd" />
              </div>

              @if (old('choise_side') == 2)
                <div class="form-group" id="div_psd2">
                  <label>Шаблон PSD, 2 сторона</label>
                  <input name="psd2" type="file" accept=".psd"/>
                </div>
              @else
                <div class="form-group" style="display:none" id="div_psd2">
                  <label>Шаблон PSD, 2 сторона</label>
                </div>
              @endif

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Добавить</button>
              <a href="/orders/new-step2/{{$client_id}}/{{ $type_id }}" class="btn btn-default">Назад</a>
            </div>
          </form>

            @if ($errors->has())
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
              @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
              @endforeach
            </div>
          @endif

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>

<script>
$(document).ready(function() {

    $('.choise-side').click(function(){
        if (!$(this).hasClass('active')) {
            var choise = $(this).data('choise');
            if (choise == 2) {
                $('#div_psd2').append('<input name="psd2" type="file" accept=".psd" />');
                $('#div_psd2').show();
            }
            else {
                $('#div_psd2').find('input').remove();
                $('#div_psd2').hide();
            }
            $('input[name=choise_side]').val(choise);
        }
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
    });

    @if (old('choise_side') == 2)
        $('.choise-side[data-choise=2]').addClass('active');
    @else
        $('.choise-side[data-choise=1]').addClass('active');
    @endif;
});
</script>
@stop
