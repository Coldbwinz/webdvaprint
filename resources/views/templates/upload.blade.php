@extends('app')

@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header">
            <div class="box-header with-border">
              <i class="fa fa-hourglass-2"></i>

              <h3 class="box-title">Загрузка</h3>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="alert alert-info">
                <div class="row flex">
                    <div class="col-md-1">
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                    </div>
                    <div class="col-md-11" style="padding-top: 7px;">
                        @foreach ($templates as $template)
                        <h4>{{ $template->psd }}</h4>
                        @endforeach
                    </div>
                </div>
            </div>

            <div id="error-alert-box" style="display:none">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-ban"></i>
                    <span id="error_message"></span>
                </div>

                <button type="button" class="btn btn-default" id="start-parse-btn">Повторить</button>
                <a href="/template/new/{{ session('client_id') }}/{{ session('type_id') }}" class="btn btn-default" id="start-parse-btn">Назад</a>
            </div>

            <div id="success-alert-box" style="display:none">
                <div class="alert alert-success alert-dismissible">
                    <i class="icon fa fa-thumbs-o-up"></i>
                    Готово.
                </div>
                <a href="/template/new/{{ session('client_id') }}/{{ session('type_id') }}" class="btn btn-default" id="start-parse-btn">Назад</a>
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>




<script>
$(function(){
    function parse() {
        $.ajax({
            url: '/template/parse/{{ $template_id }}',
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    $('.fa-refresh').removeClass('fa-spin');
                    $('#success-alert-box').show(100);
                }
                else {
                    $('.fa-refresh').removeClass('fa-spin');
                    $('#error_message').text(data.error);
                    $('#error-alert-box').show(100);
                }
            },
            error: function(xHr, status, err) {
                console.log(err);
                $('.fa-refresh').removeClass('fa-spin');
                $('#error_message').text(err);
                $('#error-alert-box').show(100);
            }
        });
    }

    $('#start-parse-btn').click(function() {
        $('#error-alert-box').hide(100);
        $('.fa-refresh').addClass('fa-spin');
        parse();
    });

    parse();
});
</script>
@stop
