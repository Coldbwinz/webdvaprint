@if (session()->has('message.warning'))
    <div class="alert alert-warning">
        <i class="fa fa-warning"></i>
        {{ session()->get('message.warning') }}
    </div>
@endif

@if (session()->has('message.success'))
    <div class="alert alert-success">
        <i class="fa fa-check-o"></i>
        {{ session()->get('message.success') }}
    </div>
@endif

@if (session()->has('message.information'))
    <div class="alert alert-info">
        <i class="fa fa-info"></i>
        {{ session()->get('message.information') }}
    </div>
@endif

@if (session()->has('message.danger'))
    <div class="alert alert-danger">
        <i class="fa fa-ban"></i>
        {{ session()->get('message.danger') }}
    </div>
@endif
