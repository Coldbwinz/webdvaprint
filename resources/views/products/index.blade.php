@extends('app')

@section('content')
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>

    <div class="row">
        <div class="col-md-6">
            <div class="box" style="border-top: none;">
                @if (Auth::user()->hasNotGroup(\App\UserGroup::PARTNER))
                    <button type="button" class="btn btn-lg collapsed"
                            style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                            data-toggle="collapse"
                            data-target="#collapsed_container">Добавить новый продукт
                    </button>
                    <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>
                        <form method="POST" action="/products/new" autocomplete="off" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                @if ($errors->has())
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                        @foreach ($errors->all() as $error)
                                            <div>{{ $error }}</div>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Название</label>
                                    <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                                </div>

                                <div class="form-group">
                                    <label>Тип продукции</label>
                                    <select class="form-control" name="printler_type">
                                        <option @if (old('printler_type') == 1) selected="selected" @endif
                                        value="1">Листовое изделие
                                        </option>
                                        <option @if ( old('printler_type') == 2) selected="selected" @endif
                                        value="2">Изделие со сгибом
                                        </option>
                                        <option @if ( old('printler_type') == 3) selected="selected" @endif
                                        value="3">Многостраничное изделие
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Изображение для иконки</label>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="2097152"/>
                                    <input name="photo" type="file"/>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th width="150">Название</th>
                            <th width="160">Изображение</th>
                            <th width="140">Стоимость</th>

                            @if (Auth::user()->isNotPartner())

                                <th width="140">Включен</th>
                                <th width="140">Действия</th>
                            @endif
                        </tr>

                        @foreach ($product_types as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>
                                    @if ($value->url)
                                        <img src="/upload/products/{{ $value->url }}"
                                             style="vertical-align: top;max-width: 90px">
                                    @endif
                                </td>

                                <td>
                                    @if ($value->services)
                                        <a href="/products/price/{{ $value->id }}">
                                            <button class="btn btn-primary btn-xs">Настроить прайс</button>
                                        </a>
                                    @else
                                        <a href="/products/price/{{ $value->id }}">
                                            <button class="btn btn-warning btn-xs">Создать прайс</button>
                                        </a>
                                    @endif

                                    @if (settings('1c_sync'))
                                        <a href="{{ route('sync1s.edit', $value->id) }}" class="btn btn-default btn-xs">
                                            Настройка 1С
                                        </a>

                                        @if (is_null($value->product_1c_key))
                                            <span class="text-danger" title="Код продукта для 1С не установлен.">
                                                <i class="fa fa-warning"></i>
                                            </span>
                                        @endif
                                    @endif
                                </td>

                                @if (Auth::user()->isNotPartner())
                                    <td>
                                        @if ($value->activity == 1)
                                            Да
                                        @else
                                            Нет
                                        @endif
                                    </td>

                                    <td>
                                        <a href="/products/edit/{{ $value->id }}">
                                            <button class="btn btn-success btn-xs">редактировать</button>
                                        </a><br>
                                        @if ($value->activity == 1)
                                            <a href="/products/off/{{ $value->id }}">
                                                <button class="btn btn-danger btn-xs">выключить</button>
                                            </a>
                                        @else
                                            <a href="/products/on/{{ $value->id }}">
                                                <button class="btn btn-success btn-xs">включить</button>
                                            </a>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {!! (new Landish\Pagination\Pagination($product_types))->render() !!}

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop
