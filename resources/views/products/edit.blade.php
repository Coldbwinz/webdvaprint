@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать продукт</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="POST" action="/products/edit/{{ $product_info->id }}" enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @if ($errors->has())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" type="text" name="name" value="{{ $product_info->name }}">
                        </div>

                        <div class="form-group">
                            <label>Тип</label>
                            <select class="form-control" name="printler_type">
                                <option
                                        @if ((old('printler_type') && (old('printler_type') == 1)) || ($product_info->printler_type == 1))
                                        selected="selected"
                                        @endif
                                        value="1">Листовое изделие
                                </option>
                                <option
                                        @if ((old('printler_type') && (old('printler_type') == 2)) || ($product_info->printler_type == 2))
                                        selected="selected"
                                        @endif
                                        value="2">Изделие со сгибом
                                </option>
                                <option
                                        @if ((old('printler_type') && (old('printler_type') == 3)) || ($product_info->printler_type == 3))
                                        selected="selected"
                                        @endif
                                        value="3">Многостраничное изделие
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Изображение</label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="300000"/>
                            <input name="photo" type="file"/>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    </section><!-- /.content -->
@stop