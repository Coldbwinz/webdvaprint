@extends('app')

@section('content')
    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }

        .simple-button-padding {
            padding: 1px !important;
            margin: 2px !important;
        }

        .no-padding {
            padding: 0 !important;
        }

        .no-margin {
            margin: 0 !important;
        }

        .table-grey-grid {
            margin: 1%;
            width: 98%;
        }

        .table-grey-grid td, th {
            border: 1px solid #ddd !important;
        }

        #add_count, #add_product, #add_cover, #add_pad, #add_option, #share_options {
            width: 230px;
            height: 34px;
            padding: 5px;
            background-color: #d2d6de;
        }

        #add_product, #add_cover, #add_pad, #add_option {
            margin: 8px 8px;
        }

        input[type='number'] {
            -moz-appearance: textfield;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

    </style>

    <script>
        var standardSizes = {
            '50x90': null, '54x86': null, '90x90': null, '70x100': null,
            '98x210': null, '74x105': null, '105x148': null,
            '148x210': null, '210x297': null, '297x420': null
        };

        var prices = <?php if (strlen($product_services) > 0) {
            echo $product_services;
        } else {
            echo '{}';
        } ?>;
        var urgency_table = JSON.parse('<?php echo $urgency_table; ?>');
        var product_id = '<?php echo $product_id; ?>';
        var printler_type = '<?php echo $printler_type; ?>';
        var selected = JSON.parse('<?php echo $selected; ?>');
        var other_service_urgency = JSON.parse('<?php echo (isset($other_service_urgency)) ? $other_service_urgency : '{}'; ?>');
        var materials = JSON.parse('{}');
        var counts = 0;
        var products = 0;
        var selectedSize = '';
        var selectedType = '';
        var selectedMaterial = '';
        var selectedChromacity = '';
        var change = 0;
        var current_price = null;
        var template_chromacity = '<?php echo $template_chromacity ?>';
        var template_paper_clip_chromacity = '<?php echo $template_paper_clip_chromacity ?>';
        var tempCoversPrices = {};
        var coversPriceIndex = '';
        var tempPadsPrices = {};
        var padsPriceIndex = '';
        $(document).ready(function () {
            loadPrice();
            if (!selected) {
                loadUrgency();
                startLoading();
                checkType();
            }

            setCoverChromacity();

            //Конвертирует ассоциативный массив а объект для понимания его JSON.stringfy
            function convertArrToObj(array) {
                var thisEleObj = new Object();
                if (typeof array == "object") {
                    for (var i in array) {
                        var thisEle = convertArrToObj(array[i]);
                        thisEleObj[i] = thisEle;
                    }
                } else {
                    thisEleObj = array;
                }
                return thisEleObj;
            }

            function setCoverChromacity() {
                if (selectedType == 'на пружине') {
                    $('.ddm-chromacity').html(template_chromacity);
                } else if (selectedType == 'на скрепке') {
                    $('.ddm-chromacity').html(template_paper_clip_chromacity);
                    if (($('#group_chromacity').html() == '4 + 0') || ($('#group_chromacity').html() == '1 + 0')) {
                        $('#group_chromacity').html('Выбрать');
                    }
                }
            }

            function startLoading() {
                $.each(prices, function (i, item) {
                    if (i != 'counts') {
                        selectSize(item.size);
                        $('#group_chromacity').html(item.chromacity);
                        if (printler_type >= 2) {
                            $('#group_type').html(item.type);
                        }
                        if (printler_type == 3) {
                            $('#group_material').val(item.material);
                        }
                        selectPrice(item.size, item.type, item.chromacity, item.material);
                        return false;
                    }
                });
                if (typeof(prices.counts) == 'undefined') {
                    prices.counts = [];
                }
            }

            $('.box-body').on('focus', 'input[type=number]', function (e) {
                $(this).on('mousewheel.disableScroll', function (e) {
                    e.preventDefault()
                })
            });
            $('.box-body').on('blur', 'input[type=number]', function (e) {
                $(this).off('mousewheel.disableScroll')
            });

            $('#add_product, #add_cover, #add_pad, #add_option').on("click", function (e) {
                if (checkSizeCounts()) {
                    var name_page = '';
                    if ((printler_type == 3) && (e.currentTarget.id == 'add_product')) {
                        var last_value = parseInt($('input[name="products[]"]:eq(-1)').val());
                        if (!(last_value > 0)) {
                            last_value = 0;
                        }
                        name_page = (selectedType == 'на пружине') ? parseInt((last_value + 2) / 2) * 2 : parseInt((last_value + 4) / 4) * 4;
                        if (name_page < 8) {
                            name_page = 8;
                        }
                    }
                    addRows('table_' + e.currentTarget.id.substr(4) + 's', name_page);
                    if ((printler_type == 3) && (e.currentTarget.id == 'add_product')) {
                        checkDeleteForProducts();
                    }

                    var table = null;
                    if (e.currentTarget.id == 'add_product') {
                        table = $('#table_products');
                    }
                    if (e.currentTarget.id == 'add_pad') {
                        table = $('#table_pads');
                    }
                    if (e.currentTarget.id == 'add_cover') {
                        table = $('#table_covers');
                    }
                    if (table) {
                        var last_tr = table.find('tr:last');
                        last_tr.find('td:first input').click();
                        disableFreeLine(last_tr,'');
                    }
                }
                return false;
            });

            $('#share_options').click(function () {
                var options = [];
                var options_prices = [];
                $('#table_options').find('input[name="options[]"]:visible').each(function (i, item) {
                    options.push(item.value);
                });
                $('#table_options').find('input[name="options_prices[]"]').each(function (i, item) {
                    options_prices.push(item.value);
                });
                $.each(prices, function (i, price) {
                    if (i != 'counts') {
                        $.each(options, function (j, option) {
                            var arr_index = $.inArray(option, price.options);
                            if (arr_index == -1) {
                                prices[i].options.push(option);
                                prices[i].options_prices = prices[i].options_prices.concat(options_prices.slice(j * counts, (j + 1) * counts));
                            }
                        });
                    }
                });
                $('#share_options').hide();
            });

            $('#table_options').click(function () {
                $('#share_options').show();
            });

            function checkSizeCounts() {
                var good = true;
                if (selectedSize.length < 2) {
                    $('#product_size').css('border', not_correct);
                    good = false;
                }
                if (counts == 0) {
                    $('#add_count').css('border', not_correct);
                    good = false;
                }
                return good;
            }

            $('#priceBody').on('focusin', 'input[name="products_prices[]"], input[name="options_prices[]"]', function () {
                var this_material = $(this).closest('tr').find('td:first input:first');
                if (printler_type == 3) {
                    if ((this_material.val() != 'Выбрать материал') && (this_material.val().length > 0)) {
                        if ($('#selectMaterial').is(':not(:visible)')) {
                            $('#selectMaterial').show();
                        }
                        if (($('#group_material').val().trim().length < 2) || ($('#group_material').val() == 'Выбрать материал')) {
                            $('#selectMaterial').css('border', not_correct);
                            $(this).blur();
                            return false;
                        } else {
                            checkChromacity(this);
                        }
                    } else {
                        this_material.css('border', not_correct).focus();
                    }
                } else {
                    if ((this_material.val() != 'Выбрать материал') && (this_material.val().length > 0)) {
                        checkChromacity(this);
                    } else {
                        this_material.css('border', not_correct).focus();
                    }
                }
            });

            $('#priceBody').on('focusin', 'input[name="covers_prices[]"]', function () {
                if (printler_type == 3) {
                    var this_material = $(this).closest('tr').find('td:first input');
                    if ((this_material.val() != 'Выбрать материал') && (this_material.val().length > 0)) {
                        if ($('#select_covers_chromacity').is(':not(:visible)')) {
                            $('#select_covers_chromacity').show();
                        }
                        if (($('#covers_chromacity').html().trim().length < 2) || ($('#covers_chromacity').html().trim() == 'Выбрать цветность')) {
                            $('#select_covers_chromacity').css('border', not_correct);
                            $(this).blur();
                            return false;
                        }
                    } else {
                        this_material.css('border', not_correct).focus();
                    }
                }
            });

            $('#priceBody').on('focusin', 'input[name="pads_prices[]"]', function () {
                if (printler_type == 3) {
                    var this_material = $(this).closest('tr').find('td:first input');
                    if ((this_material.val() != 'Выбрать материал') && (this_material.val().length > 0)) {
                        if ($('#select_pads_chromacity').is(':not(:visible)')) {
                            $('#select_pads_chromacity').show();
                        }
                        if (($('#pads_chromacity').html().trim().length < 2) || ($('#pads_chromacity').html().trim() == 'Выбрать цветность')) {
                            $('#select_pads_chromacity').css('border', not_correct);
                            $(this).blur();
                            return false;
                        }
                    } else {
                        this_material.css('border', not_correct).focus();
                    }
                }
            });

            function checkChromacity(obj) {
                if (selectedChromacity.length < 2) {
                    if ($('#selectChromacity').is(':not(:visible)')) {
                        $('#selectChromacity').show();
                    }
                    $('#selectChromacity').css('border', not_correct);
                    $(obj).blur();
                    return false;
                }
                return true;
            }

            function escapeHtml(unsafe) {
                if (typeof(unsafe) != 'undefined') {
                    return unsafe
                            .toString()
                            .replace(/&/g, "&amp;")
                            .replace(/</g, "&lt;")
                            .replace(/>/g, "&gt;")
                            .replace(/"/g, "&quot;")
                            .replace(/'/g, "&#039;");
                } else {
                    return false;
                }
            }

            function addRows(selectedTable, serviceName, arrValues) {
                var namesValue = '';
                var pricesName = '';
                var deleteName = '';
                var addString = '';
                var input_type = 'text';
                var readonly = '';
                var tdType = 'number';
                serviceName = escapeHtml(serviceName);
                if (selectedTable == "table_covers") {
                    namesValue = "covers[]";
                    pricesName = "covers_prices[]";
                    deleteName = "delete_cover[]";
                } else if (selectedTable == "table_pads") {
                    namesValue = "pads[]";
                    pricesName = "pads_prices[]";
                    deleteName = "delete_pads[]";
                } else if (selectedTable == "table_products") {
                    namesValue = "products[]";
                    pricesName = "products_prices[]";
                    deleteName = "delete_product[]";
                    if (printler_type == 3) {
                        readonly = ' tabindex="-1" ';
                    }
                    products++;
                } else if (selectedTable == "table_options") {
                    namesValue = "options[]";
                    pricesName = "options_prices[]";
                    deleteName = "delete_option[]";
                } else if (selectedTable == "table_urgency") {
                    addString = '<input style="display:none" readonly class="form-control" type="text" tabindex="-1" name="' + namesValue + '" value="' + ((serviceName) ? serviceName : "") + '"/>';
                    namesValue = "urgency_name[]";
                    pricesName = "urgency_deploy_time[]";
                    deleteName = "urgency_active[]";
                    readonly = ' readonly tabindex="-1" ';
                } else if (selectedTable == "table_urgency_deadline") {
                    addString = '<input style="display:none" readonly class="form-control" type="text" tabindex="-1" name="' + namesValue + '" value="' + ((serviceName) ? serviceName : "") + '"/>';
                    namesValue = "urgency_deadline[]";
                    pricesName = "urgency_deadline_time[]";
                    deleteName = "urgency_deadline_active[]";
                }
                if (printler_type == 3) {
                    if (namesValue == 'products[]') {
                        input_type = 'number';
                    }
                    if (selectedTable == "table_products") {
                        addString = '<input style="display:none" class="form-control" type="number" name="' + namesValue + '" value="' + ((serviceName) ? serviceName : "") + '"/>';
                    }
                }
                if ((namesValue == 'products[]' && printler_type != 3) || (namesValue == "covers[]") || (namesValue == "pads[]")) {
                    rowString = '<tr><td class="col-md-2" style="width:245px;">' +
                            '<div class="input-group dropdown products-ddm-group">' +
                            '<div class="input-group dropdown">' +
                            '<input type="text" class="form-control dropdown-toggle" name="' + namesValue + '" ' +
                            'style="width: 197px; font-size: 16px; text-align: left; height:34px;"' +
                            'data-toggle="dropdown" value="' + ((serviceName) ? serviceName : "Выбрать материал") + '"/>' +
                            '<ul class="ddm-materials dropdown-menu"></ul>' +
                            '<span class="input-group-btn"><button ' +
                            'name="' + deleteName + '" class="btn" tabindex="-1">X</button></span></div></div></td>' +
                            gettd(counts, pricesName, tdType) + '</tr>';
                } else if (deleteName.length != 0) {
                    rowString = '<tr><td class="col-md-2" style="width:245px;">' + addString + '<div class="input-group">' +
                            '<input ' + readonly + ' class="form-control" type="' + input_type + '" name="' + namesValue + '" value="' +
                            ((serviceName) ? serviceName : "") + '"/><span class="input-group-btn"><button ' +
                            'name="' + deleteName + '" class="btn" tabindex="-1">X</button></span></div></td>' +
                            gettd(counts, pricesName, tdType) + '</tr>';
                } else {
                    rowString = '<tr><td class="col-md-2 text-left">' +
                            ((serviceName) ? serviceName : "") + '</td>' + gettd(counts, pricesName, tdType) + '</tr>';
                }
                var trEQmin1 = $("#" + selectedTable + " tr").eq(-1);
                if (trEQmin1.length > 0) {
                    trEQmin1.after(rowString);
                } else {
                    $('#' + selectedTable).html(rowString);
                }
                if ((namesValue == 'products[]' && printler_type != 3) || (namesValue == "covers[]") || (namesValue == "pads[]")) {
                    addMaterials('.ddm-materials');
                }
            }

            $('#table_products').on('focusout', 'input[name="products[]"]', function (e) {
                if (printler_type == 3) {
                    var delta = 2;
                    if (selectedType == 'на скрепке') {
                        delta = 4;
                    }
                    var value = parseInt($(e.currentTarget).val() / delta) * delta;
                    var closeTD = $(e.currentTarget).closest('td');
                    $.each($(closeTD).find('input[name="products[]"]'), function (i, item) {
                        $(item).val(value);
                    });
                } else {
                    if (e.currentTarget.value.length == 0) {
                        e.currentTarget.value = 'Выбрать материал';
                    }
                }
            });

            $('#table_products').on('focusin', 'input[name="products[]"]', function (e) {
                if (printler_type != 3) {
                    if (e.currentTarget.value == 'Выбрать материал') {
                        e.currentTarget.value = '';
                    }
                } else {
                    $(e.currentTarget).css('border', '1px solid #D2D6DE');
                }
            });

            $('#table_pads, #table_covers').on('focusin', 'input[name="pads[]"], input[name="covers[]"]', function (e) {
                if (e.currentTarget.value == 'Выбрать материал') {
                    e.currentTarget.value = '';
                }
            });


            $('#add_count').on("click", function () {
                $(this).css('border', 'none')
                addCount();
            });

            function checkDeleteForProducts() {
                if (printler_type == 3) {
                    var tds = $('#table_products').find('tr td:first-child');
                    $.each(tds, function (i, item) {
                        if ((i != 0) && (i < (tds.length - 1))) {
                            $(item.childNodes[0]).show();
                            $(item.childNodes[1]).hide();
                        } else {
                            $(item.childNodes[0]).hide();
                            $(item.childNodes[1]).show();
                        }
                    });
                }
            }

            function checkType() {
                if (printler_type == 3) {
                    if (selectedType == 'на скрепке') {
                        $('#add_pad').hide();
                        $('#select_pads_chromacity').css('border', 'none').hide();
                    } else if (selectedType == 'на пружине') {
                        $('#add_pad').show();
                        if (current_price && current_price.pads_prices && (current_price.pads_prices.length > 0)) {
                            $('#select_pads_chromacity').show();
                        }
                    }
                }
            }

            function addCount(countValue) {
                $("#table_counts").find("tr td:eq(-1)").after(
                        '<td>' +
                        '<div class="input-group">' +
                        '<input class="form-control" type="number" name="counts[]" value="' +
                        ((countValue) ? countValue : "") + '"/>' +
                        '<span class="input-group-btn">' +
                        '<button tabindex="-1" name="delete_count" class="btn">X</button>' +
                        '</span>' +
                        '</div>' +
                        '</td>'
                );


                if (printler_type == 3) {
                    $.each($('#table_covers').find('tr'), function (i, item) {
                        $(item).find('td:last').after(gettd(1, 'covers_prices[]', 'number'));
                    });
                    disableFreeLines('#table_covers');
                    if (selectedType == 'на пружине') {
                        $.each($('#table_pads').find('tr'), function (i, item) {
                            $(item).find('td:last').after(gettd(1, 'pads_prices[]', 'number'));
                        });
                        disableFreeLines('#table_pads');
                    }
                }

                $.each($('#table_products').find('tr'), function (i, item) {
                    $(item).find('td:last').after(gettd(1, 'products_prices[]', 'number'));
                });
                disableFreeLines('#table_products');

                $.each($('#table_options').find('tr'), function (i, item) {
                    $(item).find('td:last').after(gettd(1, 'options_prices[]', 'number'));
                });

                $('#div_urgency').show();

                var new_urgency_deadline = gettd(1, 'urgency_deadline_time[]', 'number');
                $('#table_urgency_deadline').find('td:last').after(new_urgency_deadline);
                $.each($('#table_urgency').find('tr'), function (i, item) {
                    var new_urgency_deploy_time = gettd(1, 'urgency_deploy_time[]', 'number');
                    $(item).find('td:last').after(new_urgency_deploy_time);
                    var prop_disabled = $(item).find('td:first input').prop('disabled');
                    $('#table_urgency').find('tr').eq(i).find('td:last input').attr('disabled', prop_disabled);

                    var id = urgency_table[i].id;
                    if (current_price == null && other_service_urgency[id] && other_service_urgency[id][0]) {
                        var deploy_time = '';
                        var dead_line = '';
                        if (typeof(other_service_urgency[id][counts]) != 'undefined') {
                            deploy_time = other_service_urgency[id][counts].deploy_time;
                            dead_line = other_service_urgency[id][counts].dead_line
                        } else {
                            deploy_time = other_service_urgency[id][other_service_urgency[1].length - 1].deploy_time;
                            dead_line = other_service_urgency[id][other_service_urgency[1].length - 1].dead_line
                        }
                        $(item).find('td:last input').val(deploy_time);
                        $('#table_urgency_deadline').find('td:last input').val(dead_line);
                    }
                });

                if (typeof(countValue) == 'undefined') {
                    prices.counts.push('');
                    $.each(prices, function (i, item) {
                        if (i != 'counts') {
                            item.options_prices = addByIndexInVector(item.options_prices, counts);
                            item.products_prices = addByIndexInVector(item.products_prices, counts);

                            if (printler_type == 3) {
                                item.covers_prices = addByIndexInVector(item.covers_prices, counts);
                                if (item.type == 'на пружине') {
                                    item.pads_prices = addByIndexInVector(item.pads_prices, counts);
                                }
                            }
                            counter = 0;
                            $.each(item.urgencies, function (j, jtem) {
                                if (jtem != null) {
                                    var newUrgency = JSON.parse('{}');
                                    newUrgency.id = urgency_table[counter].id;
                                    newUrgency.name = urgency_table[counter].name;
                                    newUrgency.deploy_time = '';
                                    newUrgency.dead_line = '';
                                    newUrgency.active = urgency_table[counter].active;
                                    item.urgencies[urgency_table[counter].id].push(newUrgency);
                                    counter++;
                                }
                            });
                        }
                    });
                    $('input[name="counts[]"]:last').val();
                }
                counts++;
            }

            function gettd(count, tag, type, value) {
                var tdstring = "";
                if (type == null) {
                    type = 'text';
                }
                if (value == null) {
                    value = '';
                }
                for (var i = 0; i < count; i++) {
                    tdstring = tdstring +
                            '<td><input class="form-control" type="' + type + '" name="' + tag + '" value="' + value + '"/></td>';
                }
                return tdstring;
            }

            $('#table_covers').on("click", 'button[name="delete_cover[]"]', function (e) {
                $(this).closest('tr').remove();
                if ($('#table_covers').find('tr').length == 0) {
                    $('#select_covers_chromacity').hide();
                }
                return false;
            });

            $('#table_pads').on("click", 'button[name="delete_pads[]"]', function (e) {
                $(this).closest('tr').remove();
                if ($('#table_pads').find('tr').length == 0) {
                    $('#select_pads_chromacity').css('border', 'none').hide();
                }
                return false;
            });

            $('#table_urgency').on("click", 'button[name="urgency_active[]"]', function (e) {
                var input = $(this).closest('tr').find('input');
                if (input.attr('disabled')) {
                    input.attr('disabled', false);
                    $(this).html('X');
                } else {
                    input.attr('disabled', true);
                    $(this).html('V');
                    $(this).closest('tr').find('td:not(:first-child) input').css('border', '1px solid #D2D6DE');
                    checkType();
                }
            });

            $('#table_urgency_deadline').on('input', 'input[name="urgency_deadline_time[]"]', function (e) {
                setNumberVal(e.currentTarget, 0, 24);
            });

            $('#table_urgency').on('input', 'input[name="urgency_deploy_time[]"]', function (e) {
                setNumberVal(e.currentTarget, 0, 99);
            });

            $('#complects_discount_percent').on('input', function (e) {
                setNumberVal(e.currentTarget, 0, 100);
            });

            $('input[name="complectsCalculationType"]').on('change', function() {
               if ($(this).val() == 2) {
                   $('#div_complects_discount_percent').show();
               } else {
                   $('#div_complects_discount_percent').hide();
               }
            });

            function setNumberVal(target, min, max) {
                var val = target.value;
                if (val.length > 0) {
                    if (val.length == 0) {
                        val = min;
                    } else {
                        if ((parseFloat(val) == 0) && (val.length > 1)) {
                            val = 0;
                        } else {
                            if (val > max) {
                                val = max;
                            }
                            if (val < min) {
                                val = min;
                            }
                        }
                    }
                    $(target).val(val);
                }
            }

            $('#product_size').mousedown(function () {
                $('#product_size').css('border', '');
            });

            $('#selectMaterial').mousedown(function () {
                $('#selectMaterial').css('border', '');
            });

            $('#selectChromacity').mousedown(function () {
                $('#selectChromacity').css('border', '');
            });

            $('#table_products').on("click", 'button[name="delete_product[]"]', function (e) {
                $(this).closest('tr').remove();
                products--;
                if (printler_type == 3) {
                    checkDeleteForProducts();
                }
                return false;
            });

            $('#table_options').on("click", 'button[name="delete_option[]"]', function (e) {
                $(this).closest('tr').remove();
                return false;
            });

            $('#table_counts').on("click", 'button[name="delete_count"]', function (e) {
                var colIndex = $(this).closest('td').index();
                $(this).closest('td').remove();

                if (printler_type == 3) {
                    $.each($('#table_covers').find('tr'), function (i, item) {
                        $(item).find('td').eq(colIndex).remove();
                    });
                    for (key in tempCoversPrices) {
                        tempCoversPrices[key] = deleteByIndexInVector(tempCoversPrices[key], colIndex - 1);
                    }
                    if (selectedType == 'на пружине') {
                        $.each($('#table_pads').find('tr'), function (i, item) {
                            $(item).find('td').eq(colIndex).remove();
                        });
                        for (key in tempPadsPrices) {
                            tempPadsPrices[key] = deleteByIndexInVector(tempPadsPrices[key], colIndex - 1);
                        }
                    }
                }
                $.each($('#table_products').find('tr'), function (i, item) {
                    $(item).find('td').eq(colIndex).remove();
                });
                $.each($('#table_options').find('tr'), function (i, item) {
                    $(item).find('td').eq(colIndex).remove();
                });

                $.each($('#table_urgency').find('tr'), function (i, item) {
                    $(item).find('td').eq(colIndex).remove();
                });

                $('#table_urgency_deadline tr td').eq(colIndex).remove();

                colIndex--;
                $.each(prices, function (i, item) {
                    if (i != 'counts') {
                        item.options_prices = deleteByIndexInVector(item.options_prices, colIndex);
                        item.products_prices = deleteByIndexInVector(item.products_prices, colIndex);

                        if (i != (selectedSize + '_' + selectedType + '_' + selectedChromacity + '_' + selectedMaterial)) {
                            if (printler_type == 3) {
                                for (key in item.covers_prices) {
                                    item.covers_prices[key].covers_prices = deleteByIndexInVector(item.covers_prices[key], colIndex);
                                }
                                if (item.type == 'на пружине') {
                                    for (key in item.pads_prices) {
                                        item.pads_prices[key] = deleteByIndexInVector(item.pads_prices[key], colIndex);
                                    }
                                }
                            }
                        }
                        $.each(item.urgencies, function (j, jtem) {
                            if (jtem != null) {
                                item.urgencies[j].splice(colIndex, 1);
                            }
                        })
                    }
                });
                prices.counts.splice(colIndex, 1);

                counts--;
                return false;
            });

            function deleteByIndexInVector(arr, colIndex) {
                var tempIndex = colIndex;
                while (arr[tempIndex]) {
                    if (typeof(arr) == 'array') {
                        arr.splice(tempIndex, 1);
                        tempIndex += counts - 1;
                    } else {
                        delete arr[tempIndex];
                        tempIndex += counts;
                    }
                }
                if (typeof(arr) == 'object') {
                    tempIndex = 0;
                    var needCount = Object.keys(arr).length;
                    for (key in arr) {
                        arr[tempIndex] = arr[key];
                        tempIndex++;
                    }
                    while (Object.keys(arr).length > needCount) {
                        var k = Object.keys(arr);
                        delete arr[k[k.length - 1]];
                    }
                }
                return arr;
            }

            function addByIndexInVector(arr, colIndex) {
                var tempIndex = colIndex;
                while (tempIndex <= arr.length) {
                    arr.splice(tempIndex, 0, '');
                    tempIndex += counts + 1;
                    if (colIndex == 0) {
                        break;
                    }
                }
                return arr;
            }

            $('.ddm-product-sizes').on('mousedown', 'a', function () {
                change = 1;
                selectSize($(this).html().trim())
                $('#size_width').focus();
                selectPrice($(this).html().trim(), selectedType, selectedChromacity, selectedMaterial, true);
            });

            function selectSize(size) {
                $('.product-size').hide();
                $('#size_width').val(size.split('x')[0]);
                $('#size_height').val(size.split('x')[1]);
                $('#add_new_size').show();
            }

            $('#size_width, #size_height').focusout(function () {
                if ($('#size_width').val() <= 1) {
                    $('#size_width').val(1);
                }
                if ($('#size_height').val() <= 1) {
                    $('#size_height').val(1);
                }
                selectPrice($('#size_width').val() + 'x' + $('#size_height').val(), selectedType, selectedChromacity, selectedMaterial, true);
            });

            $('#priceBody').on('focusout', 'input[name="counts[]"]', function (e) {
                if (e.currentTarget.value <= 1) {
                    e.currentTarget.value = 1;
                }
            });

            $('.product-size').click(function () {
                $(this).val('');
                $(this).focus();
            });

            $('.product-size').keypress(function () {
                $('.product-size').val('');
                $('.product-size').hide();
                $('#add_new_size').show();
                $('#size_width').focus();
            });

            $('.product-size').focusout(function () {
                if (($('#size_width').val() == 0) || ($('#size_height').val() == 0)) {
                    if ($('.product-size').val() != 'Выбрать') {
                        $('.product-size').val('Выбрать');
                    }
                }
            });

            $('#span_size_select').click(function (e) {
                $('.product-size').val('');
                $('.product-size').focus();
            });

            $('#span_material_select').click(function (e) {
                $('#group_material').val('');
                $('#group_material').focus();
            });

            $('.dropdown-menu-type').on('click', 'a', function () {
                change = 2;
                $('#group_type').html($(this).html().trim());
                selectPrice(selectedSize, $(this).html(), selectedChromacity, selectedMaterial, true);
                checkType();
                setCoverChromacity();
            });

            $('.ddm-chromacity').on('click', 'a', function () {
                change = 3;
                $('#group_chromacity').html($(this).html().trim());
                selectPrice(selectedSize, selectedType, $(this).html(), selectedMaterial, true)
            });

            $('.ddm-covers-chromacity, .ddm-pads-chromacity').on('click', 'a', function (e) {
                var covers_type_name = $(this).closest('ul')[0].className.substr(4, 6);
                if (covers_type_name != 'covers') {
                    covers_type_name = 'pads';
                }
                var value = $(this).html().trim();
                $('#' + covers_type_name + '_chromacity').html(value);
                fillPadsCoverChromacityPrices(covers_type_name, value);
            });

            function fillPadsCoverChromacityPrices(cover_type, value) {
                var newParameters = [];
                if ((cover_type == 'covers') && tempCoversPrices) {
                    if (coversPriceIndex.length > 0) {
                        $.each($('input[name="covers_prices[]"]'), function (i, item) {
                            newParameters.push($(item).val());
                            $(item).val('');
                        });
                        tempCoversPrices[coversPriceIndex] = newParameters;
                    }
                    coversPriceIndex = value;
                    if (tempCoversPrices[coversPriceIndex]) {
                        $.each(tempCoversPrices[coversPriceIndex], function (i, item) {
                            $('input[name="covers_prices[]"]').eq(i).val(item);
                        });
                    }
                    $('#table_covers').find('input[name="covers_prices[]"]').each(function (i, item) {
                        $(item).css('border', '1px solid #D2D6DE');
                    });
                    disableFreeLines('#table_covers');
                }
                if ((cover_type == 'pads') && tempPadsPrices) {
                    if (padsPriceIndex.length > 0) {
                        $.each($('input[name="pads_prices[]"]'), function (i, item) {
                            newParameters.push($(item).val());
                            $(item).val('');
                        });
                        tempPadsPrices[padsPriceIndex] = newParameters;
                    }
                    padsPriceIndex = value;
                    if (tempPadsPrices[padsPriceIndex]) {
                        $.each(tempPadsPrices[padsPriceIndex], function (i, item) {
                            $('input[name="pads_prices[]"]').eq(i).val(item);
                        });
                    }
                    $('#table_pads').find('input[name="pads_prices[]"]').each(function (i, item) {
                        $(item).css('border', '1px solid #D2D6DE');
                    });
                    disableFreeLines('#table_pads');
                }
            }

            $('#selectMaterial').on('click', '.ddm-materials a', function (e) {
                if (printler_type == 3) {
                    change = 4;
                    $(e.currentTarget).closest('div').find('input').val(e.currentTarget.text.trim());
                    selectPrice(selectedSize, selectedType, selectedChromacity, e.currentTarget.text.trim(), true);
                }
            });

            $('#priceBody').on('click', '.ddm-materials a', function (e) {
                $(e.currentTarget).closest('div').find('input').val(e.currentTarget.text.trim());
            });

            $('#group_material').focusout(function (e) {
                if (e.currentTarget.value.length == 0) {
                    e.currentTarget.value = 'Выбрать материал';
                } else {
                    addToMaterials(e.currentTarget.value);
                    if (selectedMaterial != e.currentTarget.value.trim()) {
                        if (selectedMaterial.length == 0) {
                            selectedMaterial = e.currentTarget.value.trim();
                        }
                        if (selectedChromacity.length < 2) {
                            if ($('#selectChromacity').is(':not(:visible)')) {
                                $('#selectChromacity').show();
                            }
                            $('#selectChromacity').css('border', not_correct);
                        } else {
                            selectPrice(selectedSize, selectedType, selectedChromacity, e.currentTarget.value.trim(), true);
                        }
                    }
                }
            });

            function addToMaterials(value) {
                if ((value.length > 0) && (value != 'Выбрать материал')) {
                    materials[value] = null;
                    addMaterials('.ddm-materials');
                }
            }

            $('#group_material').focusin(function () {
                if ($(this).val() == 'Выбрать материал') {
                    $(this).val('');
                }
            });

            $('.ddm-designer-remote').on('click', 'a', function () {
                $('#days_designer_remote').html($(this).html().trim());
                $('#days_designer_remote').attr('data-value', $(this).attr('data-value').trim())
            });

            function selectPrice(size, type, chromacity, material, save) {
                if (save) {
                    saveCurrentPrice(selectedSize, selectedType, selectedChromacity, selectedMaterial);
                }
                current_price = null;
                var newIndex = size + "_" + type + "_" + chromacity + "_" + material;
                if (typeof(prices[newIndex]) == "object") {
                    current_price = prices[newIndex];
                    clearTable(1);
                } else {
                    clearTable();
                }

                if (current_price) {
                    $('#share_options').hide();
                    $('#div_urgency').show();
                    if (prices.counts) {
                        $.each(prices.counts, function (i, item) {
                            addCount(item);
                        });
                    }

                    $('input[name="weekendsSign"][value="' + current_price.weekends_sign + '"]').attr('checked', 'checked');
                    $('input[name="complectsCalculationType"][value="' + current_price.complects_calculation_type + '"]').attr('checked', 'checked');
                    if (typeof(current_price.complects_discount_percent) != 'undefined') {
                        $('#complects_discount_percent').val(current_price.complects_discount_percent);
                    }
                    if (current_price.complects_calculation_type != 2) {
                        $('#div_complects_discount_percent').hide();
                    }

                    if (printler_type == 3) {
                        $.each(current_price.covers, function (i, item) {
                            addRows('table_covers', item);
                        });
                        if (Object.keys(current_price.covers_prices).length > 0) {
                            tempCoversPrices = current_price.covers_prices;
                            coversPriceIndex = Object.keys(tempCoversPrices)[0];
                            $('#covers_chromacity').html(coversPriceIndex);
                            $.each(tempCoversPrices[coversPriceIndex], function (i, item) {
                                $('input[name="covers_prices[]"]').eq(i).val(item);
                            });
                        }
                        disableFreeLines('#table_covers');
                        if (type == 'на пружине') {
                            $.each(current_price.pads, function (i, item) {
                                addRows('table_pads', item);
                            });

                            if (Object.keys(current_price.pads_prices).length > 0) {
                                tempPadsPrices = current_price.pads_prices;
                                padsPriceIndex = Object.keys(tempPadsPrices)[0];
                                $('#pads_chromacity').html(padsPriceIndex);
                                $.each(tempPadsPrices[padsPriceIndex], function (i, item) {
                                    $('input[name="pads_prices[]"]').eq(i).val(item);
                                });
                            }
                            $('#select_pads_chromacity').toggle(current_price.pads && (current_price.pads.length > 0));
                            disableFreeLines('#table_pads');
                        }
                        $('#select_covers_chromacity').toggle(current_price.covers && (current_price.covers.length > 0));
                    }

                    $.each(current_price.products, function (i, item) {
                        addRows('table_products', item);
                    });

                    if ($('#table_urgency').html().trim().length == 0) {
                        loadUrgency();
                    }

                    if (printler_type == 3) {
                        checkDeleteForProducts();
                    }

                    $.each(current_price.products_prices, function (i, item) {
                        $('input[name="products_prices[]"]').eq(i).val(item);
                    });
                    disableFreeLines('#table_products');

                    $.each(current_price.options, function (i, item) {
                        addRows('table_options', item);
                    });
                    $.each(current_price.options_prices, function (i, item) {
                        $('input[name="options_prices[]"]').eq(i).val(item);
                    });
                    if (current_price.urgencies && current_price.urgencies.length > 0) {
                        if (typeof(current_price.urgencies[1][0]) != 'undefined') {
                            fillUrgencies(current_price.urgencies);
                        }
                    }
                    $('#price_template_employee').val(current_price.price_template_employee);
                    $('#price_template_self').val(current_price.price_template_self);
                    $('#price_customer_template_employee').val(current_price.price_customer_template_employee);
                    $('#price_customer_template_auto').val(current_price.price_customer_template_auto);
                    $('#price_designer_together').val(current_price.price_designer_together);
                    $('#price_designer_remote').val(current_price.price_designer_remote);
                    $('input[name="designer_together"]').eq(current_price.price_designer_together_selected).prop('checked', true);
                    $('#days_designer_remote').attr('data-value', current_price.days_designer_remote);
                    $('#days_designer_remote').html($('.ddm-designer-remote li a').eq(current_price.days_designer_remote - 1).html());
                    if ($('#selectMaterial').is(':not(:visible)')) {
                        $('#selectMaterial').show();
                    }
                    if ($('#selectChromacity').is(':not(:visible)')) {
                        $('#selectChromacity').show();
                    }
                    if (current_price.covers && (current_price.covers.length > 0)) {
                        if ($('#select_covers_chromacity').is(':not(:visible)')) {
                            $('#select_covers_chromacity').show();
                        }
                    }
                    if (current_price.pads && (current_price.pads.length > 0)) {
                        if ($('#select_pads_chromacity').is(':not(:visible)')) {
                            $('#select_pads_chromacity').show();
                        }
                    }
                } else {
                    tempCoversPrices = [];
                    $.each($('input[name="covers_prices[]"]'), function (i, item) {
                        $(item).val('');
                    });
                    tempPadsPrices = [];
                    $.each($('input[name="pads_prices[]"]'), function (i, item) {
                        $(item).val('');
                    });
                }
                selectedSize = size;
                selectedType = type;
                selectedMaterial = material;
                selectedChromacity = chromacity;
                setCoverChromacity();
            }

            function fillUrgencies(arr) {
                $('#table_urgency').find('tr').each(function (j, urgencyTR) {
                    var urgency_inputs = $(urgencyTR).find('input[name="urgency_deploy_time[]"]');
                    urgency = urgency_table[j];
                    if (!arr[urgency.id][0].active) {
                        $(urgencyTR).find('input').attr('disabled', true);
                        $(urgencyTR).find('button').html('V');
                    } else {
                        $(urgencyTR).find('button').html('X');
                    }
                    $.each(urgency_inputs, function (k, urgencyTD) {
                        $(urgencyTD).val(arr[urgency.id][k].deploy_time);
                    });
                });
                var urgency_deadline_inputs = $('#table_urgency_deadline').find('input[name="urgency_deadline_time[]"]');
                $.each(urgency_deadline_inputs, function (k, urgencyTD) {
                    $(urgencyTD).val(arr[urgency.id][k].dead_line);
                });
            }

            function loadUrgency() {
                $.each(urgency_table, function (i, item) {
                    addRows('table_urgency', item.name);
                });
                addRows('table_urgency_deadline', 'Дэдлайн');
                var urgency_td = $('#table_urgency tr:first-child td');
                urgency_td.find('div').hide();
                urgency_td.find('input:first-child').show();
                var urgency_deadline_td = $('#table_urgency_deadline tr:first-child td');
                urgency_deadline_td.find('div').hide();
                urgency_deadline_td.find('input:first-child').show();
            }

            function clearTable(fullclear) {
                if (fullclear) {
                    counts = 0;
                    products = 0;
                    $('#table_counts').find('tr td:not(:first-child)').remove();
                    $('#table_covers').html('');
                    $('#table_pads').html('');
                    $('#table_products').html('');
                    $('#table_options').html('');
                    $('#table_urgency').html('');
                    $('#table_urgency_deadline').html('');
                } else {
                    if (change == 3 || change == 4) {
                        $('input[name="products_prices[]"]').val('');
                    }
                    if (change == 2) {
                        if (printler_type == 3) {
                            products = 0;
                            $('#table_products').html('');
                            $('input[name="covers_prices[]"]').val('');
                            $('#table_pads').html('');
                        } else {
                            $('input[name="products_prices[]"]').val('');
                        }
                    }
                    if (change == 1) {
                        $('input[name="products_prices[]"]').val('');
                        $('input[name="covers_prices[]"]').val('');
                        $('input[name="pads_prices[]"]').val('');
                        $('input[name="options_prices[]"]').val('');
                    }
                }
            }

            function loadPrice() {
                if (Object.keys(prices).length > 0) {
                    $.each(prices, function (i, item) {
                        if (i != 'counts') {
                            standardSizes[item.size] = item.size;
                            if (printler_type == 3) {
                                materials[item.material] = null;
                                if (item.pads) {
                                    $.each(item.pads, function (j, pad) {
                                        materials[pad] = null;
                                    });
                                }
                                if (item.covers) {
                                    $.each(item.covers, function (j, cover) {
                                        materials[cover] = null;
                                    });
                                }
                            } else {
                                $.each(item.products, function (j, product) {
                                    materials[product] = null;
                                });
                            }
                        }
                    });
                }
                addParameters('.ddm-product-sizes', standardSizes);

                if (printler_type == 3) {
                    addMaterials('.ddm-materials');
                }

                if (selected && selected.size) {
                    selectedSize = selected.size;
                    selectedType = selected.type;
                    selectedChromacity = selected.chromacity;
                    selectedMaterial = selected.material;

                    $('#group_size').val(selectedSize);
                    $('#group_chromacity').html(selectedChromacity);
                    if (printler_type >= 2) {
                        $('#group_type').html(selectedType);
                    }
                    if (printler_type == 3) {
                        $('#group_material').val(selectedMaterial);
                    }
                } else {
                    if (printler_type != 1) {
                        selectedType = $('.dropdown-menu-type li a:eq(0)').html();
                    } else {
                        selectedType = '###';
                    }

                    if (printler_type != 3) {
                        selectedMaterial = '###';
                    }
                }
                selectPrice(selectedSize, selectedType, selectedChromacity, selectedMaterial);
            }

            function addParameters(ddm, arr) {
                var html = '';
                $.each(arr, function (i, item) {
                    html += '<li><a>' + i + '</a></li>';
                });
                $(ddm).html(html);
            }

            $('#priceBody').on('mousedown', 'input[name="counts[]"], ' +
                    'input[name="products[]"]:visible, input[name="products_prices[]"], ' +
                    'input[name="options[]"]:visible, input[name="options_prices[]"], ' +
                    'input[name="covers[]"]:visible, input[name="covers_prices[]"], ' +
                    'input[name="pads[]"]:visible, input[name="pads_prices[]"], ' +
                    'input[name="urgency_deploy_time[]"], input[name="urgency_deadline_time[]"], ' +
                    '#select_covers_chromacity, #select_pads_chromacity, #block_days_designer_remote', function (e) {
                $(e.currentTarget).css('border', '1px solid #D2D6DE');
            });

            $('#price_designer_together').focusout(function () {
                if (typeof($('input[name="designer_together"]:checked').val()) == 'undefined') {
                    $('#block_designer_together').css('border', not_correct);
                }
            });

            $('#price_designer_remote').focusout(function () {
                if ($('#days_designer_remote').html().trim() == 'Выбрать') {
                    $('#block_days_designer_remote').css('border', not_correct);
                }
            });

            $('#priceBody').on('focusout', 'input[name="products[]"]', function (e) {
                if (printler_type != 3) {
                    if ((e.currentTarget.value.length > 0) && (e.currentTarget.value != 'Выбрать материал')) {
                        $(this).css('border', '1px solid #D2D6DE');
                        addToMaterials(e.currentTarget.value);
                    }
                }
            });

            $('#priceBody').on('focusout', 'input[name="covers[]"], input[name="pads[]"]', function (e) {
                if (e.currentTarget.value.length > 0) {
                    $(this).css('border', '1px solid #D2D6DE');
                    addToMaterials(e.currentTarget.value);
                } else {
                    e.currentTarget.value = 'Выбрать материал';
                }
            });

            $('#price_template_employee, #price_template_self, #price_customer_template_employee, ' +
                    '#price_customer_template_auto, #price_designer_together, #price_designer_remote'
            ).mousedown(function (e) {
                $(e.currentTarget).css('border', '1px solid #A9A9A9');
            });

            $('#block_designer_together').mousedown(function () {
                $('#block_designer_together').css('border', 'none');
            });

            function saveCurrentPrice(size, type, chromacity, material) {
                if ((size.length > 1) && (type.length > 1) && (chromacity.length > 1) && (material.length > 1)) {
                    var tempPrice = JSON.parse('{}');
                    size = size.replace('х', 'x'); //русская буква заменяется на английскую
                    tempPrice.size = size;
                    tempPrice.type = type;
                    tempPrice.material = material;
                    tempPrice.chromacity = chromacity;
                    tempPrice.width = size.split('x')[0];
                    tempPrice.height = size.split('x')[1];
                    tempPrice.products = [];
                    tempPrice.options = [];
                    tempPrice.products_prices = [];
                    tempPrice.options_prices = [];
                    tempPrice.urgencies = [];
                    tempPrice.price_template_employee = $('#price_template_employee').val();

                    var productsOk = 0;
                    var productsPricesOk;
                    var coversPricesOk = 0;
                    var padsPricesOk = 0;
                    var optionsOk = 0;
                    var optionsPricesOk = 0;
                    var urgenciesOK = 0;
                    var countInOne = 0;
                    var countGoodLines = 0;
                    var countInOneGood = 0;
                    var arr_index;
                    var need_edit_to_save = false;
                    if (tempPrice.price_template_employee.length == 0) {
                        $('#price_template_employee').css('border', not_correct);
                    }
                    tempPrice.price_template_self = $('#price_template_self').val();
                    tempPrice.price_customer_template_employee = $('#price_customer_template_employee').val();
                    tempPrice.price_customer_template_auto = $('#price_customer_template_auto').val();
                    tempPrice.price_designer_together = $('#price_designer_together').val();
                    tempPrice.price_designer_together_selected = $('input[name="designer_together"]:checked').val();
                    tempPrice.price_designer_remote = $('#price_designer_remote').val();
                    tempPrice.days_designer_remote = $('#days_designer_remote').attr('data-value');
                    tempPrice.weekends_sign = $('input[name="weekendsSign"]:checked').val();
                    tempPrice.complects_calculation_type = $('input[name="complectsCalculationType"]:checked').val();
                    tempPrice.complects_discount_percent = (tempPrice.complects_calculation_type == 2) ? $('#complects_discount_percent').val() : 0;

                    prices.counts = [];
                    $('#table_counts').find('input[name="counts[]"]').each(function (i, item) {
                        prices.counts.push(item.value);
                    });
                    if (printler_type == 3) {

                        tempPrice.covers = [];
                        $('#table_covers').find('input[name="covers[]"]:visible').each(function (i, item) {
                            tempPrice.covers.push(item.value);
                        });
                        tempCoversPrices[coversPriceIndex] = [];

                        var price_covers_ok = 0;
                        $('#table_covers').find('tr').each(function (i, item) {
                            var price_covers_exist = 0;
                            $(item).find('input[type="number"]').each(function(j, jtem) {
                                tempCoversPrices[coversPriceIndex].push(jtem.value);
                                if (jtem.value.length != '') {
                                    price_covers_exist++;
                                }
                            });
                            if ((price_covers_exist == counts) || (price_covers_exist == 0)) {
                                price_covers_ok++;
                            }
                        });
                        tempPrice.covers_prices = convertArrToObj(tempCoversPrices);
                        coversPricesOk = price_covers_ok * counts;

                        for (key in tempCoversPrices) {
                            countInOne = 0
                            countInOneGood = 0;
                            for (keyIn in tempCoversPrices[key]) {
                                countInOne++;
                                if (tempCoversPrices[key][keyIn] > 0) {
                                    countInOneGood++;
                                }
                                if ((((parseInt(keyIn) + 1) % counts) == 0) && (keyIn > 0)) {
                                    if ((countInOneGood > 0) && (countInOneGood < counts)) {
                                        need_edit_to_save = true;
                                        if (coversPriceIndex != key) {
                                            $('#covers_chromacity').html(key);
                                            fillPadsCoverChromacityPrices('covers', key);
                                            showUncorrect();
                                        }
                                    }
                                    countInOneGood = 0;
                                }
                            }
                        }

                        if (type == 'на пружине') {
                            tempPrice.pads = [];
                            $('#table_pads').find('input[name="pads[]"]:visible').each(function (i, item) {
                                tempPrice.pads.push(item.value);
                            });
                            tempPadsPrices[padsPriceIndex] = [];

                            var price_pads_ok = 0;
                            $('#table_pads').find('tr').each(function (i, item) {
                                var price_pads_exist = 0;
                                $(item).find('input[type="number"]').each(function(j, jtem) {
                                    tempPadsPrices[padsPriceIndex].push(jtem.value);
                                    if (jtem.value.length != '') {
                                        price_pads_exist++;
                                    }
                                });
                                if ((price_pads_exist == counts) || (price_pads_exist == 0)) {
                                    price_pads_ok++;
                                }
                            });
                            tempPrice.pads_prices = convertArrToObj(tempPadsPrices);
                            padsPricesOk = price_pads_ok * counts;

                            for (key in tempPadsPrices) {
                                countInOne = 0
                                countInOneGood = 0;
                                for (keyIn in tempPadsPrices[key]) {
                                    countInOne++;
                                    if (tempPadsPrices[key][keyIn] > 0) {
                                        countInOneGood++;
                                    }
                                    if ((((parseInt(keyIn) + 1) % counts) == 0) && (keyIn > 0)) {
                                        if ((countInOneGood > 0) && (countInOneGood < counts)) {
                                            need_edit_to_save = true;
                                            if (padsPriceIndex != key) {
                                                $('#pads_chromacity').html(key);
                                                fillPadsCoverChromacityPrices('pads', key);
                                                showUncorrect();
                                            }
                                        }
                                        countInOneGood = 0;
                                    }
                                }
                            }
                        }
                    }

                    $('#table_products').find('input[name="products[]"]:visible').each(function (i, item) {
                        if (item.value.length > 0) {
                            productsOk++;
                        }
                        tempPrice.products.push(item.value);
                    });

                    var price_lines_ok = 0;
                    $('#table_products').find('tr').each(function (i, item) {
                        var price_exist = 0;
                        $(item).find('input[name="products_prices[]"]').each(function(j, jtem) {
                            tempPrice.products_prices.push(jtem.value);
                            if (jtem.value.length != '') {
                                price_exist++;
                            }
                        });
                        if ((price_exist == counts) || (price_exist == 0)) {
                            price_lines_ok++;
                        }
                    });
                    productsPricesOk = price_lines_ok * counts;

                    $('#table_options').find('input[name="options[]"]:visible').each(function (i, item) {
                        if (item.value.length > 0) {
                            optionsOk++;
                        }
                        tempPrice.options.push(item.value);
                    });
                    $('#table_options').find('input[name="options_prices[]"]').each(function (i, item) {
                        if (item.value.length > 0) {
                            optionsPricesOk++;
                        }
                        tempPrice.options_prices.push(item.value);
                    });
                    var table_urgency = $('#table_urgency').find('tr');
                    var table_deadline = $('#table_urgency_deadline').find('input[name="urgency_deadline_time[]"]');
                    $.each(table_urgency, function (tr, itemTR) {
                        var table_urgency_line = $(itemTR).find('input[name="urgency_deploy_time[]"]');
                        $.each(table_urgency_line, function (td, itemTD) {
                            if (typeof (tempPrice.urgencies[urgency_table[tr].id]) == 'undefined') {
                                tempPrice.urgencies[urgency_table[tr].id] = [];
                            }
                            var set_active = (typeof($($(itemTR).find('td input')[0]).attr('disabled')) == 'undefined');
                            if ((itemTD.value.length > 0) && (table_deadline[td].value.length > 0) || !set_active) {
                                urgenciesOK++;
                            }
                            tempPrice.urgencies[urgency_table[tr].id].push({
                                id: urgency_table[tr].id,
                                name: urgency_table[tr].name,
                                deploy_time: itemTD.value,
                                dead_line: table_deadline[td].value,
                                active: set_active
                            });
                        });
                    });

                    var countsGood = true;
                    $('input[name="counts[]"]').each(function () {
                        if (this.value.length == 0) {
                            countsGood = false;
                            return false;
                        }
                    });
                    if (countsGood && ($('#table_products').find('input[name="products[]"]:visible').length == productsOk) &&
                        ($('#table_products').find('input[name="products_prices[]"]').length == productsPricesOk) &&
                        ($('#table_options').find('input[name="options[]"]:visible').length == optionsOk) &&
                        ($('#table_options').find('input[name="options_prices[]"]').length == optionsPricesOk) &&
                        ($('#table_covers').find('input[name="covers_prices[]"]').length == coversPricesOk) &&
                        ($('#table_pads').find('input[name="pads_prices[]"]').length == padsPricesOk) &&
                        (urgenciesOK == $('input[name="urgency_deploy_time[]"]').length) &&
                        (tempPrice.price_template_employee.length > 0) &&
                        (tempPrice.price_template_self.length > 0) &&
                        (tempPrice.price_customer_template_employee.length > 0) &&
                        (tempPrice.price_customer_template_auto.length > 0) &&
                        (tempPrice.price_designer_together.length > 0) &&
                        (tempPrice.price_designer_together_selected.length > 0) &&
                        (tempPrice.price_designer_remote.length > 0) &&
                        (tempPrice.days_designer_remote && tempPrice.days_designer_remote.length > 0) &&
                        !need_edit_to_save) {
                        tempPrice.active = 1;
                    } else {
                        tempPrice.active = 0;
                    }
                    prices[size + "_" + type + '_' + chromacity + '_' + material] = tempPrice;
                    return tempPrice.active;
                }
                return null;
            }

            function showUncorrect() {
                if ($('#price_template_employee').val().length == 0) {
                    $('#price_template_employee').css('border', not_correct);
                }
                if ($('#price_template_self').val().length == 0) {
                    $('#price_template_self').css('border', not_correct);
                }
                if ($('#price_customer_template_employee').val().length == 0) {
                    $('#price_customer_template_employee').css('border', not_correct);
                }
                if ($('#price_customer_template_auto').val().length == 0) {
                    $('#price_customer_template_auto').css('border', not_correct);
                }
                if ($('#price_designer_together').val().length == 0) {
                    $('#price_designer_together').css('border', not_correct);
                }
                if ($('input[name="designer_together"]:checked').length == 0) {
                    $('#block_designer_together').css('border', not_correct);
                }
                if ($('#price_designer_remote').val().length == 0) {
                    $('#price_designer_remote').css('border', not_correct);
                }
                if ($('#days_designer_remote').html().trim() == 'Выбрать') {
                    $('#block_days_designer_remote').css('border', not_correct);
                }
                $('#table_counts').find('input[name="counts[]"]').each(function (i, item) {
                    if (item.value.length == 0) {
                        $(item).css('border', not_correct);
                    }
                });
                if (printler_type == 3) {
                    if ($('#covers_chromacity').html().trim() == 'Выбрать цветность') {
                        $('#select_covers_chromacity').css('border', not_correct);
                    }
                    $('#table_covers').find('input[name="covers[]"]:visible').each(function (i, item) {
                        if (item.value.length == 0) {
                            $(item).css('border', not_correct);
                        }
                    });
                    $('#table_covers').find('tr').each(function (i, item) {
                        checkCorrectForTR(item);
                    });
                    if (selectedType == 'на пружине') {
                        if ($('#pads_chromacity').html().trim() == 'Выбрать цветность') {
                            $('#select_pads_chromacity').css('border', not_correct);
                        }
                        $('#table_pads').find('input[name="pads[]"]:visible').each(function (i, item) {
                            if (item.value.length == 0) {
                                $(item).css('border', not_correct);
                            }
                        });
                        $('#table_pads').find('tr').each(function (i, item) {
                            checkCorrectForTR(item);
                        });
                    }
                }

                $('#table_products').find('input[name="products[]"]:visible').each(function (i, item) {
                    if ((item.value.length == 0) || (item.value == 'Выбрать цветность')) {
                        $(item).css('border', not_correct);
                    }
                });

                $('#table_products').find('tr').each(function (i, item) {
                    checkCorrectForTR(item);
                });

                $('#table_options').find('input[name="options[]"]:visible').each(function (i, item) {
                    if (item.value.length == 0) {
                        $(item).css('border', not_correct);
                    }
                });
                $('#table_options').find('input[name="options_prices[]"]').each(function (i, item) {
                    if (item.value.length == 0) {
                        $(item).css('border', not_correct);
                    }
                });
                var table_urgency = $('#table_urgency').find('tr');
                var table_deadline = $('#table_urgency_deadline').find('input[name="urgency_deadline_time[]"]');
                $.each(table_urgency, function (tr, itemTR) {
                    var table_urgency_line = $(itemTR).find('input[name="urgency_deploy_time[]"]');
                    $.each(table_urgency_line, function (td, itemTD) {
                        var set_active = (typeof($($(itemTR).find('td input')[0]).attr('disabled')) == 'undefined');
                        if ((itemTD.value.length > 0) && (table_deadline[td].value.length > 0) || !set_active) {
                            //urgenciesOK++;
                        } else {
                            if (itemTD.value.length == 0 && set_active) {
                                $(itemTD).css('border', not_correct);
                            }
                            if (table_deadline[td].value.length == 0) {
                                $(table_deadline[td]).css('border', not_correct);
                            }
                        }
                    });
                });
            }

            $('#table_products').on('focusout', 'input[name="products_prices[]"], input[name="pads_prices[]"], input[name="covers_prices[]"]', function() {
                disableFreeLine($(this).closest('tr'), this.style.backgroundColor);
            });

            $('#table_covers').on('focusout', 'input[name="covers_prices[]"]', function() {
                disableFreeLine($(this).closest('tr'), this.style.backgroundColor);
            });

            $('#table_pads').on('focusout', 'input[name="pads_prices[]"]', function() {
                disableFreeLine($(this).closest('tr'), this.style.backgroundColor);
            });

            $('#btnSubmit').click(function () {
                showUncorrect();
                if (saveCurrentPrice(selectedSize, selectedType, selectedChromacity, selectedMaterial)) {
                    $('#product_id').val(product_id);
                    $('#prices').val(JSON.stringify(prices));
                    $('#btn_save_price').click();
                }
                return false;
            });

            function addMaterials(objName) {
                $(objName).html('');
                $.each(materials, function (i, item) {
                    $(objName).append('<li><a>' + i + '</a></li>');
                });
            }
        });

        function disableFreeLine(tr, start_color) {
            var price_exist = 0;
            var current_background = start_color;
            var start_position = ((printler_type == 3) && ($(tr).closest('#table_products').length > 0)) ? 2 : 0;
            $(tr).find('input[type="number"]').each(function (i, item) {
                if (i >= start_position) {
                    if (item.value.length != 0) {
                        price_exist++;
                    }
                }
            });
            $(tr).find('input[type="number"]').each(function (i, item) {
                if (i >= start_position) {
                    item.style.backgroundColor = (price_exist == 0) ? '#EEE' : '';
                    item.style.border = '1px solid #D2D6DE';
                }
            });
        }

        function disableFreeLines(table_name) {
            $(table_name).find('tr').each(function (i, item) {
                disableFreeLine(item, '');
            });
        }

        function checkCorrectForTR(tr) {
            var price_exist = 0;
            var start_position = ((printler_type == 3) && ($(tr).closest('#table_products').length > 0)) ? 2 : 0;
            $(tr).find('input[type="number"]').each(function(j, jtem) {
                if (j >= start_position) {
                    if ((jtem.value.length != '') && (jtem.value != 0)) {
                        price_exist++;
                    }
                }
            });
            if ((price_exist != counts) && (price_exist > 0)) {
                $(tr).find('input[type="number"]').each(function (j, jtem) {
                    if (j >= start_position) {
                        if ((jtem.value.length == 0) || (jtem.value == 0)) {
                            $(jtem).css('border', not_correct);
                        }
                    }
                });
            }
        }

    </script>

    <div class="row" style="display: none;">
        <form style="display: none;" method="post" action="/products/price_save" id="form_save_price"
              enctype="multipart/form-data" autocomplete="off">
            <input type="text" name="_token" value="{{ csrf_token() }}">
            <input class="form-control" type="text" id="product_id" name="product_id" value=""/>
            <input class="form-control" type="text" id="prices" name="prices" value=""/>
            <button id="btn_save_price" type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                {!! csrf_field() !!}
                <div class="box-body">
                    <row>
                        <div class="col-xs-12" style="display:block; padding-left:8px; padding-bottom: 8px;">
                            <div class="no-padding" style="display:inline-block; width:230px;">
                                <span class="input-group-addon"
                                      style="background: none; border:none; font-size: 12px;">
                                    @if ($printler_type == 1)
                                        Обрезной размер (мм)
                                    @elseif ($printler_type == 2)
                                        Размер в развернутом виде (мм)
                                    @elseif ($printler_type == 3)
                                        Размер в сложенном виде (мм)
                                    @endif
                                </span>
                                <div id="product_size" class="input-group dropdown">
                                    <input type="text" class="form-control dropdown-toggle product-size"
                                           style="font-size: 16px; text-align: left;"
                                           data-toggle="dropdown" value="Выбрать"/>
                                    <div id="add_new_size" class="col-xs-12 no-padding"
                                         style="display:none; border: 1px solid #d2d6de; height:34px;">
                                        <input id="size_width" class="form-control pull-left" style="border:none;
                                            display: inline-block; width:42%; height:32px; font-size: 16px;"
                                               type="number" value=""/>
                                        <span style="cursor:default; display: inline-block; width:16%;
                                            height:32px; text-align:center; padding-top:2px; font-size:20px;
                                            border-left: 1px solid #d2d6de; border-right: 1px solid #d2d6de;"
                                              class="pull-left">X</span>
                                        <input id="size_height" class="form-control" style="border: none;
                                            display: inline-block; width:42%; height:32px; font-size: 16px;"
                                               type="number" value=""/>
                                    </div>
                                    <ul class="ddm-product-sizes dropdown-menu">
                                    </ul>
                                    <span id="span_size_select" role="button" class="input-group-addon dropdown-toggle"
                                          style="border-left: none; background-color: #DDDDDD"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                                </div>
                            </div>
                            @if ($printler_type != 1)
                                <div class="no-padding" style="display:inline-block; width:230px; margin-left:13px;">
                                <span class="input-group-addon"
                                      style="background: none; border:none; font-size: 12px;">
                                    @if ($printler_type == 2) Количество сгибов @endif
                                    @if ($printler_type == 3) Тип переплета @endif
                                </span>
                                    <div class="input-group dropdown">
                                        <button id="group_type" class="btn btn-default btn-block dropdown-toggle"
                                                style="font-size: 16px; height:34px; background-color: #FFFFFF;"
                                                type="button" data-toggle="dropdown">@if ($printler_type == 2) один
                                            сгиб @elseif ($printler_type == 3) на скрепке @endif</button>
                                        <ul class="dropdown-menu-type dropdown-menu">@if ($printler_type == 2)
                                                <li><a>один сгиб</a></li>
                                                <li><a>два сгиба</a>
                                                </li>@elseif ($printler_type == 3)
                                                <li><a>на скрепке</a></li>
                                                <li><a>на пружине</a></li>@endif</ul>
                                    <span role="button" class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span></span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </row>
                    <div id="priceBody">
                        <table id="table_counts" class="table table-striped no-margin">
                            <tr>
                                <td style="width: 245px;">
                                    <div id="add_count" class="btn btn-lg pull-left">
                                        Добавить тираж
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="col-md-12" style="display: inline-block; padding-left:0;">
                            <div id="add_product" class="btn btn-lg" style="width:230px; display:inline-block;">
                                {{ ($printler_type != 3) ? 'Добавить материал' : 'Добавить страницы' }}
                            </div>
                            @if ($printler_type == 3)
                                <div class="btnSelectMaterial no-padding"
                                     style="display:inline-block; width:230px; margin-left:5px; vertical-align: middle;">
                                    <div id="selectMaterial" class="input-group dropdown"
                                         style="display:none; width:100%;">
                                        <div class="input-group dropdown">
                                            <input id="group_material" type="text" class="form-control
                                                dropdown-toggle"
                                                   style="font-size: 16px; text-align: left; height:34px;"
                                                   data-toggle="dropdown" value="Выбрать материал"/>
                                            <ul class="ddm-materials dropdown-menu">
                                            </ul>
                                            <span role="button" class="input-group-addon dropdown-toggle"
                                                  style="border-left: none; background-color: #DDDDDD"
                                                  data-toggle="dropdown" id="span_material_select"
                                                  aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="no-padding"
                                 style="display:inline-block; width:230px; margin-left: @if ($printler_type == 3) 15px @else 5px @endif; vertical-align: middle;">
                                <div id="selectChromacity" class="input-group dropdown" style="display:none;">
                                    <button id="group_chromacity" class="btn btn-default btn-block dropdown-toggle"
                                            style="font-size: 16px; height:34px; background-color: #FFFFFF;"
                                            type="button" data-toggle="dropdown">Выбрать цветность
                                    </button>
                                    <ul class="ddm-chromacity dropdown-menu">
                                        @include('templates.chromacity')
                                    </ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span></span>
                                </div>
                            </div>
                        </div>
                        <table id="table_products" class="table table-striped no-margin">
                        </table>
                        @if ($printler_type == 3)
                            <div id="add_cover" class="btn btn-lg">
                                Добавить обложку
                            </div>
                            <div id="block_covers_chromacity" class="no-padding"
                                 style="display:inline-block; width:230px; margin-left:5px; vertical-align: middle;">
                                <div id="select_covers_chromacity" class="input-group dropdown" style="display:none;">
                                    <button id="covers_chromacity" class="btn btn-default btn-block dropdown-toggle"
                                            style="font-size: 16px; height:34px; background-color: #FFFFFF;"
                                            type="button" data-toggle="dropdown">Выбрать цветность
                                    </button>
                                    <ul class="ddm-covers-chromacity dropdown-menu">
                                        @include('templates.chromacity')
                                    </ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span></span>
                                </div>
                            </div>
                            <table id="table_covers" class="table table-striped no-margin">
                            </table>
                            <div id="add_pad" class="btn btn-lg">
                                Добавить подложку
                            </div>
                            <div id="block_pads_chromacity" class="no-padding"
                                 style="display:inline-block; width:230px; margin-left:5px; vertical-align: middle;">
                                <div id="select_pads_chromacity" class="input-group dropdown" style="display:none;">
                                    <button id="pads_chromacity" class="btn btn-default btn-block dropdown-toggle"
                                            style="font-size: 16px; height:34px; background-color: #FFFFFF;"
                                            type="button" data-toggle="dropdown">Выбрать цветность
                                    </button>
                                    <ul class="ddm-pads-chromacity dropdown-menu">
                                        @include('templates.chromacity')
                                    </ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span></span>
                                </div>
                            </div>
                            <table id="table_pads" class="table table-striped no-margin">
                            </table>
                        @endif
                        <div id="add_option" class="btn btn-lg">
                            Добавить опцию
                        </div>
                        <div id="share_options" class="btn btn-lg" style="display:none; width:230px; margin-left:5px;">
                            Применить ко всем
                        </div>
                        <table id="table_options" class="table table-striped no-margin">
                        </table>
                        <div id="div_urgency">
                            <row class="container no-padding pull-left" style="width: 950px; display: inline-block;">
                                <div style="margin-left:7px; line-height: 35px; text-align: left;">
                                    Настроить срок изготовления (срочность). <strong>Указывается в днях</strong>. Для
                                    заказов с выдачей в день оформления - установить "0"
                                </div>
                            </row>
                            <table id="table_urgency" class="table table-striped no-margin">
                            </table>
                            <div style="margin-left:7px; line-height: 35px; text-align: left;">
                                Настроить время суток (дэдлайн), после которого заказ переносится на следующую дату.
                                <strong>Указывается в часах</strong>
                            </div>
                            <table id="table_urgency_deadline" class="table table-striped no-margin">
                            </table>
                            <table class="table">
                                <tr>
                                    <td class="col-md-2" style="width: 230px;">
                                        <input style="width: 230px;" readonly class="form-control" type="text"
                                               tabindex="-1"
                                               name="" value="Учёт выходных дней:">
                                    </td>
                                    <td class="col-md-10">
                                        <div class="pull-left">
                                            <input type="radio" name="weekendsSign" value="1"
                                                   style="margin-top: -3px; vertical-align: middle;" checked/>
                                            <label>Учитывать</label>
                                            <input type="radio" name="weekendsSign" value="0"
                                                   style="margin-left: 15px; margin-top: -3px; vertical-align: middle;"/>
                                            <label>Не учитывать</label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table class="table">
                                <tr>
                                    <td class="col-md-2" style="width: 230px;">
                                        <input style="width: 230px;" readonly class="form-control" type="text"
                                               tabindex="-1"
                                               name="" value="Коррекция цены на комплекты:">
                                    </td>
                                    <td class="col-md-10">
                                        <div class="pull-left" style="margin-top:6px;">
                                            <input type="radio" name="complectsCalculationType" value="0"
                                                   style="margin-top: -3px; vertical-align: middle;" checked/>
                                            <label>Оставить расчётную цену</label>
                                            <input type="radio" name="complectsCalculationType" value="1"
                                                style="margin-left: 15px; margin-top: -3px; vertical-align: middle;"/>
                                            <label>Считать по общему тиражу</label>
                                            <input type="radio" name="complectsCalculationType" value="2"
                                                style="margin-left: 15px; margin-top: -3px; vertical-align: middle;"/>
                                            <label>Сделать скидку на комплекты</label>
                                        </div>
                                        <div id="div_complects_discount_percent" class="input-group nopadding" style="width: 120px; padding-left:15px;">
                                            <input id="complects_discount_percent" type="number"
                                                   class="form-control" value="0">
                                            <span class="input-group-addon">
                                                %
                                            </span>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            <?
                            ($printler_type != 3) ? $price_type = 'рублей' : $price_type = 'руб/стр.';
                            ($printler_type != 3) ? $page_type = 'за экз.' : $page_type = 'за стр.';
                            ?>
                            <div id="templates_prices" class="container col-md-12"
                                 style="width:1100px; margin-top:20px;">
                                <row>
                                    <div class="container pull-left" style="width:300px;">
                                        <div><strong>Разработка макета по шаблону</strong></div>
                                        <div class="container"
                                             style="width:300px; display: inline-block; padding-left:0;">
                                            <div style="width:120px; text-align: right; display: inline-block;">
                                                сотрудником -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_template_employee"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:60px; display: inline-block;">{{$price_type}}</div>
                                        </div>
                                        <div class="container"
                                             style="width:300px; display: inline-block; padding-top:3px; padding-left:0;">
                                            <div style="width:120px; text-align: right; display: inline-block;">
                                                самостоятельно
                                                -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_template_self"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:60px; display: inline-block;">{{$price_type}}</div>
                                        </div>
                                    </div>
                                    <div class="container pull-left" style="width:300px;">
                                        <div><strong>Проверка готового макета заказчика</strong></div>
                                        <div class="container"
                                             style="width:300px; display: inline-block; padding-left:0;">
                                            <div style="width:120px; text-align: right; display: inline-block;">
                                                сотрудником -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_customer_template_employee"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:60px; display: inline-block;">{{$price_type}}</div>
                                        </div>
                                        <div class="container"
                                             style="width:300px; display: inline-block; padding-top:3px; padding-left:0;">
                                            <div style="width:120px; text-align: right; display: inline-block;">
                                                автоматически
                                                -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_customer_template_auto"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:60px; display: inline-block;">{{$price_type}}</div>
                                        </div>
                                    </div>
                                    <div class="container pull-left" style="width:390px;">
                                        <div><strong>Разработка индивидуального макета дизайнером</strong></div>
                                        <div class="container"
                                             style="width:500px; display: inline-block; padding-left:0;">
                                            <div style="width:160px; text-align: right; display: inline-block;">
                                                совместная работа -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_designer_together"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:50px; display: inline-block;">рублей</div>
                                            <div style="display: inline-block;" id="block_designer_together">
                                                <input type="radio" name="designer_together" value="0"
                                                       style="margin-top: -3px; vertical-align: middle;"> {{$page_type}}</input>
                                                <input type="radio" name="designer_together" value="1"
                                                       style="margin-top: -3px; vertical-align: middle;"> за час</input>
                                            </div>
                                        </div>
                                        <div class="container"
                                             style="width:500px; display: inline-block; padding-top:3px; padding-left:0;">
                                            <div style="width:160px; text-align: right; display: inline-block;">
                                                удаленно по ТЗ
                                                -
                                            </div>
                                            <div style="width:80px; display: inline-block;"><input type="number"
                                                                                                   id="price_designer_remote"
                                                                                                   value=""
                                                                                                   style="width:80px; padding-left: 5px;"/>
                                            </div>
                                            <div style="width:50px; display: inline-block;">рублей</div>
                                            <div id="block_days_designer_remote"
                                                 style="display: inline-block; width: 120px; vertical-align: top;">
                                                <div class="input-group dropdown" style="height:24px; ">
                                                    <button id="days_designer_remote"
                                                            class="btn btn-default btn-block dropdown-toggle"
                                                            style="font-size: 14px; padding: 2px 12px; background-color: #FFFFFF;"
                                                            type="button" data-value="" data-toggle="dropdown">Выбрать
                                                    </button>
                                                    <ul style="top:-145px" class="ddm-designer-remote dropdown-menu">
                                                        <li><a data-value="1">1 день</a></li>
                                                        <li><a data-value="2">2 дня</a></li>
                                                        <li><a data-value="3">3 дня</a></li>
                                                        <li><a data-value="4">4 дня</a></li>
                                                        <li><a data-value="5">5 дней</a></li>
                                                    </ul>
                                                     <span role="button" class="input-group-addon dropdown-toggle"
                                                           style="background-color: #DDDDDD; height: 24px; padding: 4px 8px"
                                                           data-toggle="dropdown"
                                                           aria-haspopup="true" aria-expanded="false"><span
                                                                 class="caret"></span></span>
                                                </div>
                                            </div>
                                            <div style="width:50px; display: inline-block;">срок</div>
                                        </div>
                                    </div>
                                </row>
                                <row class="col-md-12">
                                    <button id="btnSubmit" class="btn btn-primary pull-left" style="margin-top:20px;">
                                        Сохранить
                                    </button>
                                </row>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@stop