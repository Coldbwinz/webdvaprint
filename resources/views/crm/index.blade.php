@extends('app')

@section('content')
<div class="row">
    <div class="col-md-7">
      <div class="box box-default box-solid collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Добавить нового клиента</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <form method="POST" action="/client/new" autocomplete="off" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body" style="display: none">
            <div class="form-group">
              <label for="exampleInputEmail1">Электронная почта</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input class="form-control" type="email" name="email" value="">
              </div>
            </div>


            <div class="form-group">
              <label>Имя</label>
               <input class="form-control" type="text" name="name" value="">
            </div>

            <div class="form-group">
              <label>Компания</label>
               <input class="form-control" type="text" name="company" value="">
            </div>

            <div class="form-group">
              <label>Телефон</label>
               <input class="form-control" type="text" name="phone" value="">
            </div>


            

          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Добавить</button>
          </div>
        </form>
      </div><!-- /.box -->
    </div>
  </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                  
<div class="box-header" style="
    height: 40px;
">
                    
                    <div class="box-tools" style="
    width: 98%;
    /* margin-left: 20px; */
">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Поиск">
                            <div class="input-group-btn">
                                <button id="btnSearch" class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                  
                  
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    
                    <tr>
                      <th>ID</th>
                      <th>Имя</th>
                      <th>Контакты</th>
                      <th>E-mail</th>
                      <th>Телефон</th>
                      <th>Долг</th>
                      <th>Заказов</th>
      
                    </tr>
                    @foreach ($clients as $value)
                    <tr>
                      <td>{{ $value->id }}</td>
                      <td><a href="/client/show/{{ $value->id }}">{{ $value->name }}</a></td>
                      <td></td>
                      <td>{{ $value->email }}</td>
                      <td>{{ $value->phone }}</td>
                      <td>0</td>
                      <td>{{ $value->count }}</td>
                    
                    </tr>

                    @endforeach
                                 

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
@stop