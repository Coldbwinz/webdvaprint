@extends('app')

@section('content')
          <div class="row">
            <div class="col-xs-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Новый клиент</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
 <form method="POST" action="/client/new" autocomplete="off" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Электронная почта</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input class="form-control" type="email" name="email" value="">
              </div>
            </div>


            <div class="form-group">
              <label>Имя</label>
               <input class="form-control" type="text" name="name" value="">
            </div>

            <div class="form-group">
              <label>Компания</label>
               <input class="form-control" type="text" name="company" value="">
            </div>

            <div class="form-group">
              <label>Телефон</label>
               <input class="form-control" type="text" name="phone" value="">
            </div>


            

          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Добавить</button>
          </div>
        </form>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
@stop