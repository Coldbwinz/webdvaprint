@extends('app')

@section('scripts')
    <link rel="stylesheet" href="/plugins/croppie/croppie.css"/>
    <script src="/plugins/croppie/croppie.js"></script>
    <script src="/plugins/croppie/croppie_code.js"></script>
@endsection

@section('content')
          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="/upload/users/0.jpg" alt="User profile picture">
                  <h3 class="profile-username text-center">{{ $client->name }}</h3>
<!--
                  <p class="text-muted text-center"><a href="/client/showcompany/1">ООО "Дабл АП"</a> - Директор</p>
                  <p class="text-muted text-center"><a href="/client/showcompany/1">ООО "МФсейл"</a> - Директор</p>
-->
                   
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Заказы</b> <a href="/orders/history/s/0/u/{{ $client->id }}" class="pull-right">{{ $status['all'] }}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Черновик</b> <a href="/orders/history/s/1/u/{{ $client->id }}" class="pull-right">{{ $status['new'] }}</a>
                    </li>
                    <li class="list-group-item">
                      <b>На согласование</b> <a class="pull-right"><a href="/orders/history/s/3/u/{{ $client->id }}" class="pull-right">{{ $status['waiting'] }}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Ждет действия</b> <a href="/orders/history/s/4/u/{{ $client->id }}" class="pull-right">{{ $status['work'] }}</a>
                    </li>
                    <li class="list-group-item">
                      <b>В работе</b> <a class="pull-right">0</a>
                    </li>
                    <li class="list-group-item">
                      <b>Выдан</b> <a class="pull-right">0</a>
                    </li>
                  </ul>

                  <a href="/orders/new-step1/{{ $client->id }}" class="btn btn-primary btn-block"><b>Новый заказ</b></a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Комментарий</h3>
                  
           
                </div><!-- /.box-header -->
                <div class="box-body">  
                    <div class="form-group comments-block">
                     <pre class="pre-no-style">{{ $client->description }}</pre>
                     <p style="margin-top: 10px;margin-bottom: 0px;"><a href="javascript: void(0);" onclick="fastEdit('comments');" class="btn btn-danger btn-xs">редактировать</a></p>
                    </div>
                    <div class="form-group comments-block-edit" style="display: none">
                        <form method="POST" action="/client/show/{{ $client->id }}" enctype="multipart/form-data" autocomplete="off">
                            {!! csrf_field() !!}
                            <textarea class="form-control description-field" style="height: 200px;padding: 5px" value="">{{ $client->description }}</textarea>
                            <p style="margin-top: 10px;margin-bottom: 0px;"><a onclick="fastSave('description-field','client/save/{{ $client->id }}', 'comments')" class="btn btn-danger btn-xs">Сохранить</a></p>
                        </form>
                    </div>        
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">О клиенте</a></li>
                    <li><a href="#timeline" data-toggle="tab">Заказы</a></li>                    
                    <li><a href="#requisites" data-toggle="tab">Реквизиты</a></li>
                    <li><a href="#docs" data-toggle="tab">Документы</a></li>
                </ul>
                <div class="tab-content">
                    <!-- tab-pane-1 settings -->
                    <div class="tab-pane active" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group">
                              <label for="inputName" class="col-sm-2 control-label">Имя</label>
                              <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="inputName" value="{{ $client->name }}" placeholder="Name">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputEmail" class="col-sm-2 control-label">Почта</label>
                              <div class="col-sm-10">    
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                  <input class="form-control" type="email" name="email" id="inputEmail" value="{{ $client->email }}">
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputCompany" class="col-sm-2 control-label">Компания</label>
                              <div class="col-sm-10">
                                <input type="text" name="company_name" class="form-control" id="inputCompany" value="{{ $client->company_name }}" placeholder="Компания">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="inputPhone" class="col-sm-2 control-label">Телефон</label>
                              <div class="col-sm-10">
                                <input type="text" name="phone" class="form-control" id="inputPhone" value="{{ $client->phone }}" placeholder="Телефон">
                              </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="upload" class="col-sm-2 control-label">Фото</label>                                
                                <div class="col-sm-10">                                    
                                    <div id="uploaded-image"></div>
                                    <div style="padding-top:5px;">                                        
                                        <input id="upload" name="photo" type="file" value=""/>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="8192000"/>
                                        <input type="hidden" id="imagebase64" name="imagebase64">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Сохранить</button>
                              </div>
                            </div>
                        </form>
                        
                    </div>
                    <!-- end tab-pane-1 settings -->
                    
                    <!-- tab-pane-2 activity -->
                    <div class="tab-pane" id="activity">
                        <!-- Post -->
                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="/upload/users/{{ Auth::user()->photo }}" alt="user image">
                            <span class="username">
                              <a href="#">Андрей Наумов</a>
                              <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                            </span>
                            <span class="description">Отправил на согласования - 29.11.15</span>
                          </div><!-- /.user-block -->
                          <p>
                            Добрый день!<br>

                          В приложение макет на согласование. 

                          </p>
                          <ul class="list-inline">
                           <!--  <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li> -->
                            <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Заметки (0)</a></li>
                          </ul>

                          <input class="form-control input-sm" type="text" placeholder="Type a comment">
                        </div><!-- /.post -->
                    </div>
                    <!-- end tab-pane-2 activity -->
                    
                    <!-- tab-pane-3 timeline -->
                    <div class="tab-pane" id="timeline">
                      <!-- The timeline -->
                      <ul class="timeline timeline-inverse">
                        <?php $buffer_date = ''; ?>

                        @foreach ($order_logs as $value) 
                            @if ($buffer_date != date('d.m.Y', strtotime($value->created_at)))                            
                            <li class="time-label">
                              <span class="bg-red">
                                {{ date('d.m.Y', strtotime($value->created_at)) }}
                              </span>
                            </li>                            
                            @endif
                            
                            <!-- timeline item -->
                            <li>
                              <i class="fa fa-file-text-o bg-blue"></i>
                              <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {{ date('H:i', strtotime($value->created_at)) }}</span>
                                <h3 class="timeline-header"><a href="#">Заказ №{{ $value->order_id }}</a> сменил статус</h3>
                                <div class="timeline-body">
                                  Статус: <b>
                                            @if ($value->status == 1)
                                            Черновик
                                            @endif

                                            @if ($value->status == 2)
                                            Ждет
                                            @endif

                                            @if ($value->status == 3)
                                            Отправлен
                                            @endif

                                            @if ($value->status == 4)
                                            Макет утвержден
                                            @endif

                                            @if ($value->status == 5)
                                            Ожидает оплаты
                                            @endif

                                            @if ($value->status == 6)
                                            Полностью оплачен
                                            @endif

                                            @if ($value->status == 7)
                                            Частично оплачен
                                            @endif

                                            @if ($value->status == 8)
                                            Печать без предоплаты
                                            @endif

                                            @if ($value->status == 9)
                                            Запущен в работу
                                            @endif

                                            @if ($value->status == 10)
                                            Пошел в печать
                                            @endif

                                            @if ($value->status == 11)
                                            Готов
                                            @endif

                                            @if ($value->status == 12)
                                            На выдаче
                                            @endif
                                          </b>
                                </div>
                                <div class="timeline-footer">
      <!--                            <a class="btn btn-primary btn-xs">Read more</a>-->
                                  @if ($value->status == 1)<a class="btn btn-danger btn-xs">Удалить</a>@endif
                                </div>
                              </div>
                            </li>
                            <!-- END timeline item -->        
                            <?php $buffer_date = date('d.m.Y', strtotime($value->created_at)); ?> 
                        @endforeach

                            <li>
                              <i class="fa fa-clock-o bg-gray"></i>
                            </li>
                      </ul>
                    </div>
                    <!-- end tab-pane-3 timeline -->
                  
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div>
@stop