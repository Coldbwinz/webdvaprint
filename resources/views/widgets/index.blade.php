@extends('app')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header">
                <h3 class="box-title">Виджет регистрации</h3>
            </div>
            <div class="box-body">
                <div>
                    <a href="#" data-toggle="modal" data-target="#widgetPreview">
                        <img src="{{ asset('data-widgets/register.png') }}" height="50">
                    </a>
                </div>
                <div>
                    <textarea class="form-control widget-code"><a href="{{ route('auth.register') }}"><img src=" {{ asset('data-widgets/register.png') }}"></a></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header">
                <h3 class="box-title">Настройки виджета</h3>
            </div>
            <div class="box-body" style="height:290px">
                <p>Выберите менеджера по умолчанию, к которому будут прикреплены новые клиенты, зарегистрированные через виджет:</p>

                <div class="widget-registration-save-panel">
                    {!! Form::open(['route' => 'widget.registration.save']) !!}
                        <div class="form-group">
                            {!! Form::select('manager_id', $widgets['registration']['managerList'], null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group text-right">
                            {!! Form::submit('Сохранить', ['class'=>'btn btn-flat btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="widgetPreview" tabindex="-1" role="dialog" aria-labelledby="widgetPreviewLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="widgetPreviewLabel">Просмотр виджета</h4>
            </div>
            <div class="modal-body">
                @include('auth.register-form')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Закрыть
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
