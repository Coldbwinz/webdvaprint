<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WEB2PRINT</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- seletc2 -->
    <link rel="stylesheet" href="/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/dist/css/app.css">
    <link rel="stylesheet" href="/css/app.css">
    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini <? /*if (isset($mini_menu)){?>sidebar-collapse<? } */?>">
        <section class="content">
            
            <section class="content-header">
                <h1>
                    {{$page['breadcrumbs'][0] }}
                    <small>{{ $page['breadcrumbs'][1] }}</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> {{ $page['breadcrumbs'][0] }}</a></li>
                    <li class="active">{{ $page['breadcrumbs'][1] }}</li>
                </ol>
            </section>

            <section class="content">
                @yield('content')
            </section>    
        </section><!-- /.content -->

    <!-- Bootstrap 3.3.5 -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="/plugins/chartjs/Chart.min.js"></script>
    <!-- select2 -->
    <script src="/plugins/select2/select2.min.js"></script>


    <!-- InputMask -->
    <script src="/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>    

    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>

    <script src="/js/app.js"></script>

    <script type="text/javascript">
        
        
    @if (isset($uuid))
        var uuid = '<?=$uuid?>';
    @endif
  
    @if (isset($company_id))
        var company_id = {{ $company_id }};
    @endif    
    
    console.log(uuid);
    console.log(company_id);
    
    $(document).ready(function () {

        
    //Date picker
    $('.datepicker').datepicker({
      autoclose: true,
        format: 'dd.mm.yy'
    });
        

    $("[data-mask]").inputmask();
       
        $("iframe").on("load", function () {

        console.log('load the iframe');   
            /*
            $(this).contents().find(".fr-designer").after('<br><br><a href="#" style="position: absolute;top: 0px;z-index: 10000;" onclick="require(\'app\').openReportByUUID(\'b3e22f54a851c00b62c3d28a7e8d42b9.frx\', true);">Кнопка1</a><a href="#" style="position: absolute;top: 20px;z-index: 10000;" onclick="require(\'app\').openReportByUUID(\'5e233953a45e5d0733ba9fdbe35c6b0e.frx\', true);">Кнопка2</a><br><br>');
            */
            
            $(this).contents().find(".change_page").click(function () {
            console.log('change_page has been clicked');    
                if (uuid) {
                    @if (isset($status))
                        @if ($status > 1)
                            @if (isset($order_id))
                                var order_id = {{ $order_id }};
                            @endif
                            location.href = '/imodule/status/' + order_id + '/4'; 
                        @else
                            location.href = '/imodule/review/' + uuid + '?sand=yes';    
                        @endif
                    @else
                        location.href = '/imodule/review/' + uuid + '?sand=yes';                        
                    @endif
                }
            });

            $(this).contents().find(".save_page").click(function () {
                if (uuid) {
                    location.href = '/imodule/review/send/' + uuid;
                    console.log('save page has been clicked');
                }
            });

            $(this).contents().find(".show_block_div").click(function () {
                console.log('show block div has been clicked');
                $('.bloc_div_overlay').show();
            });

        })
    });
    </script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    @yield('scripts')

    <script src="/js/client_show.js"></script>

</body>
</html>
