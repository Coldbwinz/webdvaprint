@extends('imodule.index')

@section('content')

    <div class="callout callout-info">
        <h4 style="margin-bottom: 0px">Полиграфия</h4>
    </div>             

    <div class="row">
    @foreach ($product_types as $value)
        @if ($value->activity == 1)
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua" style="background-color: #fff !important"><img
                                src="/upload/products/{{ $value->url }}" style="vertical-align: top"/></span>
                    <div class="info-box-content">
                        <span class="info-box-text">{{ $value->name }}</span>
                        <a href="/imodule/second/{{ $company_id }}/{{ $value->id }}">
                            <button class="btn btn-block btn-info btn-xs" style="margin-top: 5px;">Выбрать</button>
                        </a>
                        <!--<button class="btn btn-block btn-success btn-xs showCalculation" data="{{ $value->id }}"
                                style="margin-top: 5px;">Расчет
                        </button>-->

                        <div style="display: none"
                             class="invoice_{{ $value->id }}">{!! nl2br($value->services_id) !!}</div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
    </div>

    <div class="callout callout-info">
        <h4 style="margin-bottom: 0px">Календарная продукция</h4>
    </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua" style="background-color: #fff !important;"><img
                                src="http://printolet.ru/content/images/icons/blue/cal_karm.png"
                                style="vertical-align: top"/></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Календарики</span>
                        <a href="/imodule/second?type=10&company_id={{ $company_id }}">
                            <button class="btn btn-block btn-info btn-xs" style="margin-top: 5px;">Заказать</button>
                        </a>
                        <!--<button class="btn btn-block btn-success btn-xs" style="margin-top: 5px;">Расчет</button>-->
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua" style="background-color: #fff !important;"><img
                                src="http://printolet.ru/content/images/icons/blue/cal_per.png"
                                style="vertical-align: top"/></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Перекидные календари</span>
                        <a href="/imodule/second?type=11&company_id={{ $company_id }}">
                            <button class="btn btn-block btn-info btn-xs" style="margin-top: 5px;">Заказать</button>
                        </a>
                        <!--<button class="btn btn-block btn-success btn-xs" style="margin-top: 5px;">Расчет</button>-->
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua" style="background-color: #fff !important;"><img
                                src="http://printolet.ru/content/images/icons/blue/cal_kvart.png"
                                style="vertical-align: top"/></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Квартальные календари</span>
                        <a href="/imodule/second?type=12&company_id={{ $company_id }}">
                            <button class="btn btn-block btn-info btn-xs" style="margin-top: 5px;">Заказать</button>
                        </a>
                        <!--<button class="btn btn-block btn-success btn-xs" style="margin-top: 5px;">Расчет</button>-->
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->
        </div>

@stop