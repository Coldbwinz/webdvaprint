@extends('imodule.index')

@section('content')

    <div class="row">
        <div class="col-md-3">
            <a href="/template/new/{{ $company_id }}/{{ $type_id }}" class="btn btn-primary btn-block margin-bottom">Добавить
                макет</a>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Тематики</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding" style="display: block;">
                    <ul class="nav nav-pills nav-stacked">

                        <li class="active"><a href="/imodule/second/{{ $company_id }}/{{ $type_id }}"> Все <span
                                        class="label label-warning pull-right">{{ $count_all }}</span></a></li>
                        <li><a href="/imodule/second/{{ $company_id }}/{{ $type_id }}/0"> Макеты клиента <span
                                        class="label label-warning pull-right">{{ $count_my }}</span></a></li>

                        @foreach ($themes as $value)
                            <li>
                                <a href="/imodule/second/{{ $company_id }}/{{ $type_id }}/{{ $value->id }}"> {{ $value->name }}
                                    <span class="label label-primary pull-right">{{ $value->count ? $value->count : 0 }}</span></a>
                            </li>
                        @endforeach
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
        <div class="col-md-9">
            <style>
                .btn.btn-lg:after {
                    font-family: "Glyphicons Halflings";
                    content: "\e114";
                    float: right;
                    margin-left: 15px;
                }

                .btn.btn-lg.collapsed:after {
                    content: "\e080";
                    margin-top: -26px;
                }

                .table-grey-grid {
                    margin: 1%;
                    width: 98%;
                }

                .table-grey-grid td, th {
                    border: 1px solid #ddd !important;
                }

                .box-footer {
                    border: 0px;
                }
                #collapsed_container
                {
                    display: block;
                }
            </style>

            <script>
                $(document).ready(function () {
                    var namesCount = parseInt('<? echo count($price_table->serviceCounts) ?>');
                    var price = {};
                    price.serviceCounts = [];
                    price.servicePrices = [];

                    $('#table').on('click', ".countsDiv", '', function (e) {
                        $('#user_count').val(e.currentTarget.textContent);
                    });

                    $('#table').on('click', ".pricesDiv", '', function (e) {
                        $('#user_count').val(price.serviceCounts[$(e.currentTarget.parentNode).index() - 1].count);
                        $(e.currentTarget).closest('tr')[0].childNodes[1].childNodes[1].childNodes[1].childNodes[1].checked = true;
                        calculateAndShow();
                    });

                    $("input[name ='counts[]']").each(function (i, item) {
                        price.serviceCounts[i] = {};
                        price.serviceCounts[i].count = parseInt(item.value);
                    });

                    $("input[name ='prices[]']").each(function (i, item) {
                        price.servicePrices[i] = {};
                        price.servicePrices[i].price = parseInt(item.value);
                    });

                    $("#user_count").keyup(function () {
                        calculateAndShow();
                    });

                    $("input[name='namesChecked[]']").change(function () {
                        calculateAndShow();
                    });

                    function calculateAndShow() {
                        var user_count = parseInt($("#user_count").val());
                        if (user_count > 0) {
                            var selectedPriceIndex;
                            for (var i = 1; i <= price.serviceCounts.length; i++) {
                                if ((i == price.serviceCounts.length) || (user_count < price.serviceCounts[i].count) ) {
                                    selectedPriceIndex = i - 1;
                                    break;
                                }
                            }
                            var summ = 0;
                            $("input[name='namesChecked[]']").each(function (i, item) {
                                if (item.checked) {
                                    var need_approx = (user_count > price.serviceCounts[0].count) && (user_count < price.serviceCounts[price.serviceCounts.length - 1].count);
                                    var approxPrice = 0;
                                    if (need_approx) {
                                        var price1 = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex].price;
                                        var price2 = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex + 1].price;
                                        var count1 = price.serviceCounts[selectedPriceIndex].count;
                                        var count2 = price.serviceCounts[selectedPriceIndex + 1].count;
                                        approxPrice = price1 + (user_count - count1) / (count2 - count1) * (price2 - price1);
                                    } else {
                                        approxPrice = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex].price;
                                    }
                                    summ += approxPrice * user_count;
                                }
                            });
                            summ = Math.ceil(summ);
                            $("#total_price").val(summ);
                        }
                    }

                    $('.gallery-image').on('click', '.selectTemplate', '', function (e) {
                       console.log(e.currentTarget.href);
                        var sendObj = {};
                        var i = 0;
                        sendObj.name = "";
                        sendObj.options = [];
                        $("input[name='namesChecked[]']").each(function (i, item) {
                            if (item.checked) {
                                if (i < namesCount) {
                                    sendObj.name = item.value;
                                } else {
                                    sendObj.options.push(item.value);
                                };
                            };
                            i++;
                        });

                        sendObj.price = $("#total_price").val();
                        sendObj.draw = $("#user_count").val();

                        var new_href = e.currentTarget.href + '/' + JSON.stringify(sendObj);
                        window.location.href = new_href;
                        return false;
                    });
                });
            </script>

            @include('imodule.priceForm')
            
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Шаблоны: &nbsp; {{ $category_title }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="gallery">
                        @foreach ($templates as $value)
                            <li>
                                <div class="gallery-image">
                                    @if (isset($value->client_id))
                                        <a class="selectTemplate" href="/imodule/third/{{ $company_id }}/{{ $type_id }}/{{ $value->id }}/nolib">
                                            <div><img src="{{ $value->url }}" alt=""
                                                      style="max-height: 100%;max-width: 180px;">
                                            </div>
                                        </a>
                                    @else
                                        <a class="selectTemplate" href="/imodule/third/{{ $company_id }}/{{ $type_id }}/{{ $value->id }}/lib">
                                            <div><img src="{{ $value->url }}" alt=""
                                                      style="max-height: 100%;max-width: 180px;">
                                            </div>
                                        </a>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
        </div><!-- /.col -->
    </div>

@stop