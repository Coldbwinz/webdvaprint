@extends('imodule.index')

@section('content')
    <script>
        $(document).ready(function () {
            var namesCount = parseInt('<? echo count($price_table->serviceCounts) ?>');
            var price = {};
            price.serviceCounts = [];
            price.servicePrices = [];

            $('#table').on('click', ".countsDiv", '', function (e) {
                $('#user_count').val(e.currentTarget.textContent);
            });

            $('#table').on('click', ".pricesDiv", '', function (e) {
                $('#user_count').val(price.serviceCounts[$(e.currentTarget.parentNode).index() - 1].count);
                $(e.currentTarget).closest('tr')[0].childNodes[1].childNodes[1].childNodes[1].childNodes[1].checked = true;
                calculateAndShow();
            });

            $("input[name ='counts[]']").each(function (i, item) {
                price.serviceCounts[i] = {};
                price.serviceCounts[i].count = parseInt(item.value);
            });

            $("input[name ='prices[]']").each(function (i, item) {
                price.servicePrices[i] = {};
                price.servicePrices[i].price = parseInt(item.value);
            });

            $("#user_count").keyup(function () {
                calculateAndShow();
            });

            $("input[name='namesChecked[]']").change(function () {
                calculateAndShow();
            });

            function calculateAndShow() {
                var user_count = parseInt($("#user_count").val());
                if (user_count > 0) {
                    var selectedPriceIndex;
                    for (var i = 1; i <= price.serviceCounts.length; i++) {
                        if ((i == price.serviceCounts.length) || (user_count < price.serviceCounts[i].count) ) {
                            selectedPriceIndex = i - 1;
                            break;
                        }
                    }
                    var summ = 0;
                    $("input[name='namesChecked[]']").each(function (i, item) {
                        if (item.checked) {
                            var need_approx = (user_count > price.serviceCounts[0].count) && (user_count < price.serviceCounts[price.serviceCounts.length - 1].count);
                            var approxPrice = 0;
                            if (need_approx) {
                                var price1 = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex].price;
                                var price2 = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex + 1].price;
                                var count1 = price.serviceCounts[selectedPriceIndex].count;
                                var count2 = price.serviceCounts[selectedPriceIndex + 1].count;
                                approxPrice = price1 + (user_count - count1) / (count2 - count1) * (price2 - price1);
                            } else {
                                approxPrice = price.servicePrices[i * price.serviceCounts.length + selectedPriceIndex].price;
                            }
                            summ += approxPrice * user_count;
                        }
                    });
                    summ = Math.ceil(summ);
                    $("#total_price").val(summ);
                }
            }

            $('#btnAgree').click(function () {
                if (checkFields()) {
                    window.location.href = "/order/status/" + "<?php echo $order->id ?>" + "/4/" + JSON.stringify(getCalculationData());
                } else {
                    if ($('#collapsed_container').css('display') == 'none') {
                        $('#alertPlsSelectCalculation').show();
                    } 
                }
            });

            $("#btnCalculation, #btnSave").click(function() {
                $('#alertPlsSelectCalculation').hide();
            });
        });
    </script>

    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Важно</h4>
            Это страница на которой вы можете согласовать результат просто распечатав ее или отправить клиенту на
            согласование.
        </div>
    </div>
    <section class="invoice">
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">

                <div class="pull-left">
                    @include('orders.priceForm_new')
                    <div class="alert alert-error" id="alertPlsSelectCalculation" style="display:none;">
                        <span>
                            <p>Пожайлуйста выберите услугу и объём заказа!</p>
                        </span>
                    </div>
                    {{ $order->type_name }}@if ($order->price_details['name'] or $order->price_details['name'] or $order->draw)
                        ,@endif

                    @if ($order->price_details['name'])
                        материал {{ $order->price_details['name'] }},
                    @endif

                    @if ($order->draw)
                        тираж {{ $order->draw }} шт.
                    @endif

                    @if ($order->price_details['options'])
                        <strong>Дополнительно:</strong>
                        @foreach ($order->price_details['options'] as $option)
                            {{ $option }};
                        @endforeach

                    @endif
                </div>

                <button id="btnAgree" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Согласовать
                </button>

                @if ($order->status == 2)
                    <? if (!isset($_GET['sand'])) { ?>
                    <a href="/order/reviwer/send/{{ $order->id }}">
                        <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i
                                    class="fa fa-download"></i> Отправить клиенту
                        </button>
                    </a>
                    <? } ?>
                @endif


                <a href="/orders/edit/{{ $order->id }}">
                    <button style="border-color: #000;background-color: #fff;color: #000;margin-right: 5px;"
                            class="btn btn-success pull-right"><i class="fa fa-edit"></i> Внести правки
                    </button>
                </a>


            </div>
        </div>
        <br/>


        <div class="row no-print">
            <div class="col-xs-12" style="text-align: center;">
                <!--		<img src="/lib/frx/{{ $order->img }}" /><br/><br/>-->
                <iframe src="/lib/see/index.html?uuid={{ $order->file }}" width="100%"
                        style="width: 100%;height: 86vh;border: 0px;">
                    Ваш браузер не поддерживает плавающие фреймы!
                </iframe>
            </div>
        </div>


    </section>
@stop