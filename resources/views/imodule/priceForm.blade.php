<style type="text/css">
  
  .col-md-8
  {
    clear: both;
    padding-top: 20px;
  }

</style>

<div class="row">
    <div class="col-md-12">
        <div class="box" style="border-top: none;">
            <button type="button" class="btn btn-lg collapsed"
                    style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                    data-toggle="collapse"
                    data-target="#collapsed_container">Расчёт
            </button>
            <div id="collapsed_container" class="collapse">

                    <div class="box-body no-padding">
                         <form method="POST" action="/imodule/second/{{$company_id}}/{{$type_id}}"
                      enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                        <table id="table" class="table table-striped table-grey-grid">
                            <tbody>
                            <tr>
                                <th class="col-md-2 text-left">
                                    <p style="text-align: center;font-size: 18px;font-weight: bold;margin-bottom: 0px;">
                                        Тираж</p>
                                </th>
                                @foreach ($counts as $count)
                                    <th style="vertical-align: middle;text-align: center;font-size: 18px;">
                                        <div>
                                            <input class="form-control" type="hidden" disabled="true"
                                                   name="counts[]"
                                                   value="{{$count->count}}"/>
                                            <div class="countsDiv" style="cursor: pointer"> {{$count->count}} </div>
                                        </div>
                                    </th>
                                @endforeach
                            </tr>
                            <?php $i = 0 ?>
                            @foreach ($services as $service)
                                <tr>
                                    <td>
                                        <div class="input-group">
                                            <span>
                                                <input type="radio" name="namesChecked[]"
                                                       <?php
                                                       $set_selected = false;
                                                       if ($name_selected == $service->name) {
                                                           $set_selected = true;
                                                       }
                                                       ?>
                                                       @if ($set_selected)
                                                       checked
                                                       @endif
                                                       value="{{ $service->name }}"/>
                                            </span>
                                            <input class="form-control" type="hidden" disabled="true"
                                                   name="names[]"
                                                   value="{{ $service->name }}"/>
                                            &nbsp;&nbsp;{{ $service->name }}

                                        </div>
                                    </td>
                                    <?php
                                    $j = 0;
                                    $min = ($i * count($counts));
                                    $max = ($i + 1) * count($counts);
                                    ?>

                                    @foreach ($prices as $price)
                                        @if (($j >= $min) && ($j < $max))
                                            <td style="vertical-align: middle;text-align: center;font-size: 14px;">

                                                <input class="form-control" disabled="true" type="hidden"
                                                       name="prices[]"
                                                       value="{{$price->price}}"/>
                                                <div class="pricesDiv" style="cursor: pointer"> {{$price->price}} руб. </div>
                                            </td>
                                        @endif
                                        <?php $j++; ?>
                                    @endforeach
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <div class="col-md-4">

                            <div class="input-group" style="margin-top: 5px;">
                                 <span class="input-group-addon">
                                    Тираж
                                </span>
                                <input id="user_count" class="form-control" type="text" name="user_count"
                                       value="{{ $price_count }}"/>
                                 <span class="input-group-addon">
                                    шт.
                                </span>
                            </div>
                            <br>
                            <div class="input-group">
                                     <span class="input-group-addon">
                                        Итого
                                    </span>
                                <input id="total_price" class="form-control" readonly="readonly"
                                       type="number" name="total_price"
                                       value="{{ $price_price }}"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p style="font-weight: bold;margin-bottom: 5px;">Дополнительные опции</p>
                            <?php $i = 0 ?>
                            @foreach ($services_opt as $service)
                                <div>
                                    <input type="checkbox" name="namesChecked[]"
                                           <?php
                                           $set_selected = false;
                                           foreach ($options_selected as $option) {
                                               if ($option == $service->name) {
                                                   $set_selected = true;
                                               }
                                           }
                                           ?>
                                           @if ($set_selected)
                                           checked
                                           @endif
                                           value="{{ $service->name }}"/>

                                    <input class="form-control" type="hidden" disabled="true"
                                           name="names[]"
                                           value="{{ $service->name }}"/>
                                    &nbsp;&nbsp;{{ $service->name }}

                                </div>
                                <?php
                                $j = 0;
                                $min = ($i * count($counts));
                                $max = ($i + 1) * count($counts);
                                ?>
                            <?php /*<pre>
                                {{ var_dump($prices) }}
                            </pre> */?>
                                @foreach ($prices as $price)
                                    @if (($j >= $min) && ($j < $max))
                                        <input class="form-control" disabled="true" type="hidden"
                                               name="prices[]" value="{{$price->price}}"/>
                                    @endif
                                    <?php $j++; ?>
                                @endforeach

                                <?php $i++; ?>
                            @endforeach
                        </div>
                    </div>
                    <div class="box-footer">



                      </form>  
                        <button class="btn btn-info collapsed" data-toggle="collapse"
                    data-target="#collapsed_container" style="margin-left: 15px;">Дальше</button>
                    </div>

            </div>
        </div>
    </div>
</div>