@extends('imodule.index')

@section('content')
	 
	 <script>
        $(document).ready(function () {
            var namesCount = parseInt('<? echo count($price_table->serviceCounts) ?>');
            var counts = [];
            var prices = [];

			 $('#btnAgree').click(function () {
		                var drawVal = $("#user_count").val();
		                var priceVal = $("#total_price").val();
		                var nameVal = '';
		                $("input[name='namesChecked[]']").each(function (i, item) {
		                    if (item.checked) {
		                        if (i < namesCount) {
		                            nameVal = item.value;
		                            return 1;
		                        }
		                    }
		                });

		                if ((drawVal > 0) && (priceVal > 0) && (nameVal.length > 0)) {
		                    var sendObj = {};
		                    sendObj.name = "";
		                    sendObj.options = [];
		                    $("input[name='namesChecked[]']").each(function (i, item) {
		                        if (item.checked) {
		                            if (i < namesCount) {
		                                sendObj.name = item.value;
		                            } else {
		                                sendObj.options.push(item.value);
		                            }
		                        }
		                    });

		                    sendObj.price = priceVal;
		                    sendObj.draw = drawVal;

		                    window.location.href = "/imodule/status/" + "<?php echo $order->id ?>" + "/4/" + JSON.stringify(sendObj);
		                } else {
		                    $('#alertPlsSelectCalculation').show();
		                }
		            });
	</script>

	<div style="margin-top: 15px">
	    <button id="btnAgree" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Согласовать
	    </button>

	    @if ($order->status == 2)
	        <? if (!isset($_GET['sand'])) { ?>
	        <a href="/order/reviwer/send/{{ $order->id }}">
	            <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i
	                        class="fa fa-download"></i> Отправить клиенту
	            </button>
	        </a>
	        <? } ?>
	    @endif


	    <a href="/orders/edit/{{ $order->id }}">
	        <button style="border-color: #000;background-color: #fff;color: #000;margin-right: 5px;"
	                class="btn btn-success pull-right"><i class="fa fa-edit"></i> Внести правки
	        </button>
	    </a>
    </div>  

    <div class="row no-print">
            <div class="col-xs-12" style="text-align: center;">
                <!--    <img src="/lib/frx/{{ $order->img }}" /><br/><br/>-->
            <iframe src="/lib/see/index.html?uuid={{ $order->file }}" width="100%"
                        style="width: 100%;height: 86vh;border: 0px;">
                    Ваш браузер не поддерживает плавающие фреймы!
            </iframe>
        </div>
    </div>

@stop