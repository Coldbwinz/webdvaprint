<div class="modal fade" id="cardPaymentModal" tabindex="-1" role="dialog" aria-labelledby="cardPaymentModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="cardPaymentModalLabel">Оплата картой</h4>
            </div>
            <div class="modal-body">
                <img src="{{ asset('images/yandex-kassa.jpg') }}" alt="Yandex Kassa" class="img-responsive"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Выход</button>
            </div>
        </div>
    </div>
</div>
