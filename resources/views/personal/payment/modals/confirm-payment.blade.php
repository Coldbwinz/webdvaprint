<div class="modal fade" id="confirmPaymentModal" tabindex="-1" role="dialog" aria-labelledby="confirmPaymentModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmPaymentModalLabel">
                    Загрузите платежное поручение
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="file"
                           name="image"
                           class="form-control"
                           @change="confirmPaymentImageSelected"
                    />

                    <div class="help-block">Поддерживаемые форматы файлов: jpg, png, pdf.</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Выход</button>
                <button type="button"
                        class="btn btn-primary"
                        @click="uploadConfirmPaymentImage"
                        :disabled="! confirmPaymentImage"
                >
                    Отправить
                </button>
            </div>
        </div>
    </div>
</div>
