@extends('app')

@section('content')
    @if (Auth::user()->accessLevel() == 1)
        @include("admin.user.dashboard_panel")
    @endif
    <script src="/js/history_user.js"></script>
    <div id="payments" class="box box-default personal-payment">
        <div class="box-header with-border top_search_box">
            <form>
                <input class="form-control" name="page" style="display:none" value="{{ $current_page }}"/>
                <input class="form-control" name="change" style="display:none" value="0"/>
                <input class="form-control" name="href" style="display:none" value="/orders/payments"/>
                <input type="text" class="form-control" name="user_search" placeholder="Введите слово для поиска..."/>
            </form>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-payment" id="table_history_block">
                @include('personal._table.orders_payment_table')
            </table>

            <div id="div_pagination_user">
                {!! $pagination !!}
            </div>
        </div>

        @include('orders.waiting_for_server')
        @include('personal.payment.modals.card-payment')
        @include('personal.payment.modals.confirm-payment')
        @include('personal.payment.modals.guarantee-payment')

        <div class="box-footer">

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function show_order_list(id) {
            if ($('.box_' + id).hasClass('active_box')) {
                $('.top_group_box').show();
                $('.box_group').removeClass('active_box');
                $('.orders_list_item').hide();
                $('.invoice_form').hide();
            } else {
                //alert($('.not_invoce_' + id).is(":visible"));
                //alert(id);
                $('.top_group_box').hide();
                $('.box_group').removeClass('active_box');
                $('.box_' + id).addClass('active_box');
                $('.active_row').removeClass('active_row');
                $('.orders_list_' + id).addClass('active_row');
                $('.orders_list_item').hide();
                $('.orders_list_' + id).slideToggle();
            }
        }

        function show_invoice(id) {
            if (!$('.box_' + id).hasClass('active_box')) {
                $('.top_group_box').hide();
                $('.box_group').removeClass('active_box');
                $('.box_' + id).addClass('active_box');
                $('.active_row').removeClass('active_row');
                $('.orders_list_' + id).addClass('active_row');
                $('.orders_list_item').hide();
                $('.orders_list_' + id).slideToggle();
            }

            $('.invoice_form_full').hide();
            $('.invoice_form').hide();
            $('.not_invoce_' + id).slideDown();
        }

        function show_invoice_full(id) {
            if (!$('.box_' + id).hasClass('active_box')) {
                $('.top_group_box').hide();
                $('.box_group').removeClass('active_box');
                $('.box_' + id).addClass('active_box');
                $('.active_row').removeClass('active_row');
                $('.orders_list_' + id).addClass('active_row');
                $('.orders_list_item').hide();
                $('.orders_list_' + id).slideToggle();
            }

            $('.invoice_form').hide();
            $('.invoice_form_full').hide();
            $('.not_invoce_full' + id).slideDown();
        }

        $('.group_item').click(function () {
            //$('.group_item').toggle();
            $('.inwork_form').hide();
            $(this).toggleClass('active');

            //return false;
        })

        // Vue
        var payments = new Vue({
            'el': '#payments',
            data: {
                confirmPaymentImage: null,
                guaranteePaymentImage: null,
                imageUploadUrl: null,
                groupOrderId: null
            },

            methods: {
                confirmPaymentImageSelected: function (event) {
                    if (event.target.files.length === 1) {
                        var file = event.target.files[0];

                        if (! this.validateFile(file)) {
                            alert('Неверный формат файла! Необходимо выбрать изображение в формате jpg, png, pdf');

                            return false;
                        }

                        this.confirmPaymentImage = file;
                    }
                },

                guaranteePaymentImageSelected: function (event) {
                    if (event.target.files.length === 1) {
                        var file = event.target.files[0];

                        if (! this.validateFile(file)) {
                            alert('Неверный формат файла! Необходимо выбрать изображение в формате jpg, png, pdf');

                            return false;
                        }

                        this.guaranteePaymentImage = file;
                    }
                },

                validateFile: function (file) {
                    return ["image/jpeg", "image/png", "image/jpg", "application/pdf"].indexOf(file.type) !== -1;
                },

                attachImageToOrderId(file, orderId) {
                    return new Promise(function (resolve, reject) {
                        var formData = new FormData();
                        formData.append('image', file);

                        $.ajax({
                            url: this.imageUploadUrl,
                            method: 'post',
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                resolve(response);
                            }
                        })
                            .error(function (error) {
                                reject(error);
                            })
                            .always(function () {
                                this.confirmPaymentImage = null;
                            }.bind(this));
                    }.bind(this));
                },

                uploadConfirmPaymentImage: function (event) {
                    this.imageUploadUrl = '/orders/' + this.groupOrderId + '/confirm';
                    this.attachImageToOrderId(this.confirmPaymentImage, this.groupOrderId)
                        .then(function (response) {
                            if (response.status === 'success') {
                                alert('Файл загружен.');
                                $('#confirmPaymentModal').modal('hide');
                                window.location.reload();
                            }
                        });
                },

                uploadGuaranteePaymentImage: function (event) {
                    this.imageUploadUrl = '/orders/' + this.groupOrderId + '/guarantee';
                    this.attachImageToOrderId(this.guaranteePaymentImage, this.groupOrderId)
                        .then(function (response) {
                            if (response.status === 'success') {
                                alert('Файл загружен.');
                                $('#guaranteePaymentModal').modal('hide');
                                window.location.reload();
                            }
                        });
                },

                showConfirmModal: function (event) {
                    this.groupOrderId = $(event.target).data('group-order-id');
                    $('#confirmPaymentModal').modal('show');
                },

                showGuaranteeModal: function (event) {
                    this.groupOrderId = $(event.target).data('group-order-id');
                    $('#guaranteePaymentModal').modal('show');
                }
            },

            mounted: function () {
                $('[data-action="hide-form"]').on('click', function () {
                    var target = $(this).data('target');
                    $(target).hide();
                });
            }
        });
    </script>
@endsection
