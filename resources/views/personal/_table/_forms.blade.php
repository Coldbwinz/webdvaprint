@if (in_array($order->status, [2, 12, 25, 30, 102, 112, 1000]))
    <tr class="stopped_form_{{ $order->id }} stopped_form border-left border-right"
        style="display: none;">
        <td colspan="9">
            <div class="panel box box-success">
                <div class="box-header with-border stopped-form-commentary-title">
                    <h4 class="box-title">Комментарий</h4>
                </div>

                <div class="panel-collapse collapse in">
                    <div class="box-body">
                        <form method="POST" action="/order/stopped/{{ $order->id }}/1"
                              onsubmit="return validate_form_reason ( this );"
                              enctype="multipart/form-data" autocomplete="off">
                            {!! csrf_field() !!}

                            {{-- stopped_state --}}
                            <input type="hidden"
                                   class="form-control"
                                   id="stopped_state_{{ $order->id }}"
                                   name="stopped_state_{{ $order->id }}"
                                   value=""
                                   placeholder=""
                                   required="required"
                            >

                            {{-- stopped reason --}}
                            @if ($order->status == 1000)
                                <div class="pay-message form-group">
                                    {{ $order->totalExpense() }}
                                </div>
                            @endif
                            <div class="form-group stopped-form-commentary">
                                <input type="text" class="form-control" name="stopped_reason" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="accept-message btn btn-primary">
                                    Подтвердить
                                </button>

                                <button type="button"
                                        class="cancel-message btn btn-default"
                                        data-action="hide-form"
                                        data-target=".stopped_form_{{ $order->id }}"
                                >
                                    Отменить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </td>
    </tr>

@endif

@if (($order->group->subdomain && $order->status == 53) ||
     ($order->status == 52 && (!$order->group->statusInfo || ($order->group->statusInfo && $order->group->statusInfo->id != 100))))
    <form method="POST" action="/order/status_defect_return/{{ $order->id }}"
          onsubmit="return validate_form_reason ( this );"
          enctype="multipart/form-data" autocomplete="off">
        {!! csrf_field() !!}
        <tr class="defect_return_form_{{ $order->id }} defect_return_form" style="display: none;">
            <td colspan="9">
                <div class="panel box box-success">
                    <div class="box-header with-border">
                        <h4 class="box-title">Комментарий</h4>
                    </div>

                    <div class="panel-collapse collapse in">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="text" class="form-control" name="defect_return_reason" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Подтвердить
                                </button>

                                <button type="button"
                                        class="btn btn-default"
                                        data-action="hide-form"
                                        data-target=".defect_return_form_{{ $order->id }}"
                                >
                                    Отменить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </form>
@endif
