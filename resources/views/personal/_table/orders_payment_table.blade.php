<thead>
<tr>
    <th width="60">ID</th>
    <th width="180">Счёт</th>
    <th width="220">Куратор заказа</th>
    <th width="250">Контакты</th>
    <th width="200">Дата оформления</th>
    <th width="180">Сумма заказа</th>
    <th width="180">Статус</th>
    <th width="140">Действия</th>
</tr>
</thead>

<tbody>
@foreach($groupOrders as $groupOrder)
    <tr class="box_{{ $groupOrder->id }} box_group @if (($groupOrder->id == $selected_group_order_id) || $opened) active_box @endif">
        {{-- ID --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">{{ $groupOrder->id }}</td>

        {{-- Счёт --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">
            <a href="{{ $groupOrder->getInvoiceLink() }}" target="_blank">
                Счёт №{{ $groupOrder->invoice_number }}
            </a>
        </td>

        {{-- Куратор заказа --}}
        <?php $manager = \Auth::user()->client->manager; ?>
        <td onclick="show_order_list({{ $groupOrder->id }})">
            @if ($manager && $manager->contact)
                {{ $manager->contact->full_name }}
            @endif
        </td>

        {{-- Контакты --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">
            @if ($manager && $manager->contact)
                {{ $manager->contact->email }} <br>
                {{ $manager->contact->phone }}
            @endif
        </td>

        {{-- Дата оформления --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">
            {{ $groupOrder->created_at->format('d.m.Y') }} <br>
            {{ $groupOrder->created_at->format('H:i:s') }}
        </td>

        {{-- Сумма заказа --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">
            <p class="text-muted">{{ $groupOrder->price }} руб.</p>

            @if ($groupOrder->statusInfo->id == 7)
                <span style="font-size: 14px">к оплате {{ $groupOrder->price - $groupOrder->pay }} руб.
                    @if (strlen($groupOrder->close_date) > 0)
                        <br>до {{ $groupOrder->close_date }}
                    @else
                        <br>до ##.##.####
                    @endif
                                    </span>
            @endif
        </td>

        {{-- Статус --}}
        <td onclick="show_order_list({{ $groupOrder->id }})">
                                <span class="label label-warning"
                                      style="background-color: {{ "#".$groupOrder->statusInfo->color }} !important;">
                                    {{ $groupOrder->statusInfo->name }}
                                </span>
            @if (in_array($groupOrder->status, [1, 8]) && $groupOrder->payment_status)
                <br>
                <a href="{{ $groupOrder->confirm_image ? $groupOrder->confirm_image : $groupOrder->guarantee_image }}" class="label label-default" target="_blank">
                    {{ $groupOrder->payment_status }}
                </a>
            @endif

            <div class="logs" style="font-size: 12px;margin-top:10px;">
                @foreach ($groupOrder->logs as $record)
                    @if (in_array($record->status, [6, 7]))
                        <div class="text-muted">{{ $record->comments }}</div>
                    @endif
                @endforeach
            </div>
        </td>

        {{-- Действия --}}
        <td>
            <button class="btn btn-block btn-default btn-xs"
                    data-toggle="modal"
                    data-target="#cardPaymentModal"
            >
                Оплатить картой
            </button>

            <button class="btn btn-block btn-default btn-xs"
                    data-group-order-id="{{ $groupOrder->id }}"
            @click="showConfirmModal"
            >
            Подтвердить оплату
            </button>

            <button class="btn btn-block btn-default btn-xs"
                    data-group-order-id="{{ $groupOrder->id }}"
            @click="showGuaranteeModal"
            >
            Гарантировать оплату
            </button>
        </td>
    </tr>

    {{-- Позиции заказа --}}
    <tr class="orders_list_{{ $groupOrder->id }} orders_list_item active_head
                            @if ($groupOrder->id == $selected_group_order_id) || $opened) active_row @endif
            ">
        <td class="left-border"><b>#</b></td>
        <td><b>Наименование</b></td>
        <td><b>Тираж</b></td>
        <td><b>Превью</b></td>
        <td><b>Дата выдачи</b></td>
        <td><b>Цена</b></td>
        <td><b>Статус</b></td>
        <td class="right-border">&nbsp</td>
    </tr>
    @foreach ($groupOrder->orders as $index => $order)
        <tr class="orders_list_{{ $groupOrder->id }} orders_list_item
                                @if ($groupOrder->id == $selected_group_order_id) || $opened) active_row @endif
        @if ($index == count($groupOrder->orders) - 1) active_row_last @endif
                ">

            {{-- ID --}}
            <td class="left-border">
                {{ $order->group_id }}.{{ $order->position }}
            </td>

            <?php
            if (isset($order->productType)) {
                $prinler_type = $order->productType->printler_type;
                $product_name = $order->productType->name;
                $product_chromacity = $order->price_details['selected_chromacity'];
            } else {
                $prinler_type = $order->price_details['printler_type'];
                $product_name = $order->price_details['selected_product_type_name'];
                $product_chromacity = $order->price_details['selected_chromacity'];
            }
            ?>
            <td>{{ $product_name . ' ' . $order->price_details['selected_size'] }}
                @if ($prinler_type == 3),
                <br>{{ $order->price_details['selected_product'] . ' стр.'}}@if ($order->price_details['selected_cover'] != 'Без обложки')
                    +обложка @else Без обложки @endif
                @elseif ($prinler_type == 1),
                <br>{{ $product_chromacity }}
                @elseif ($prinler_type == 2),
                <br>({{ $order->price_details['selected_type'] }})
                @endif</td>
            <td>{{ $order->draw }} шт.</td>
            <td>
                @if ($order->status == 10)
                    <p style="text-align: left">Макет еще не загружен</p>
                @else
                    @if ($order->img)
                        @if (filter_var($order->img, FILTER_VALIDATE_URL))
                            <img src="{{ $order->img }}" class="max-preview-img-size">
                        @else
                            @if (file_exists('lib/frx/'.$order->img))
                                <img style="text-align: left;" class="max-preview-img-size"
                                     src="/lib/frx/{{ $order->img }}" alt="Превью"/>
                            @else
                                <i class="fa fa-refresh fa-spin"></i>
                            @endif
                        @endif
                    @endif
                @endif
            </td>
            <td>{{ $order->price_details['close_date'] }}</td>
            <td>{{ $order->price }} руб.</td>
            <td>
                @if (($order->status >= 25) && ($order->status <= 40) && ($order->sort_order > 55))
                    <span class="label label-warning"
                          style="background-color: #{{ \App\OrderStatuses::find(56)->color }} !important;">
                                            {{ \App\OrderStatuses::find(56)->name }}
                                        </span>
                    <br>
                @endif

                @if ($order->statusInfo)
                    <span class="label label-warning"
                          style="background-color: {{ "#".$order->statusInfo->color }} !important;">{{ $order->statusInfo->name }}</span>
                @else
                    <span class="label label-warning"
                          style="background-color: #{{ \App\OrderStatuses::find(999)->color }} !important;">{{ \App\OrderStatuses::find(999)->name }}</span>
                @endif

                {{ $order->payment_status }}
            </td>
            <td class="right-border">

                @include('personal._table._actions', ['order' => $order])

            </td>
        </tr>

        @include('personal._table._forms', ['order' => $order])

    @endforeach
@endforeach
</tbody>