@if ($order->status == 10)
    <a href="/loading/{{random_int(1000, 9999).$order->id.random_int(1000, 9999)}}">
        <button style="margin-bottom: 5px"
                class="btn btn-block btn-info btn-xs">
            Загрузить
        </button>
    </a>
@endif

@if (in_array($order->status, [2000, 2010]))
    <a href="/orders/post-step2/{{$order->id}}">
        <button style="margin-bottom: 5px"
                class="btn btn-block btn-info btn-xs">
            @if ($order->price_details['selected_prepress'] == 1)
                Выбрать шаблон
            @elseif ($order->price_details['selected_prepress'] == 5)
                Выбрать из архива
            @endif
        </button>
    </a>
@endif

@if (in_array($order->status, [4, 5, 9]))
    @if ($order->file)
        <a href="/order/reviwer/0/{{ $order->id }}">
            <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">
                Утвердить
            </button>
        </a>
    @else
        <a href="/order/reviwer/0/{{ $order->id }}?editable=false"
            <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">
                Утвердить
            </button>
        </a>
    @endif
@endif

@if (!in_array($order->status, [5, 9, 10, 1000]) &&  (($order->status <= 25) || in_array($order->status, [30, 102, 112])))
    <button style="margin-bottom: 5px;"
            class="pause-btn btn btn-block btn-danger btn-xs"
            data-orderid="{{ $order->id }}"
            data-loading-text="Подождите..."
    >Приостановить</button>
@endif

@if ($page['submenu'] != 'need_attention')
    @if ((!in_array($order->status, [5, 9, 10]) &&  (($order->status <= 25) || in_array($order->status, [102, 112, 1000])) && (($order->status_last == 0) || in_array($order->status_last,[25,30]))) ||
    // Если позиция приостановлена в Истории, то в ЛК вернуть статус Приостановлен и кнопку Изменить макет
    ($order->status == status('paused', 'id') && $order->stopped_user_id != Auth::user()->id && $order->stopped_from == \App\Orders::STOPPED_FROM_HISTORY) ||
    // Если заказ у подрядчика, то дать возможность изменить макет до скачивания подрядчиком
    ($order->with_contractor && !in_array($order->status, [30]))
)
        @if ($order->status != 1010)
            <a href="/order/change_psd/{{ $order->id }}">
                <button style="margin-bottom: 5px"
                        class="btn btn-block btn-info btn-xs">
                    {{ ($order->template_library_id == null) ?
                        'Заменить макет' :
                        'Изменить макет' }}
                </button>
            </a>
        @endif
    @endif
@endif

@if ($order->status == 1000)
    @if (($order->stopped_user_id && ($order->stopped_user_id == auth()->user()->id))
        || ($order->template_changed_user_id == auth()->user()->id))
        <button style="margin-bottom: 5px;" class="btn btn-block btn-success btn-xs do_restore" data-id="{{ $order->id }}" data-orderid="{{ $order->id }}">
            Возобновить
        </button>
        <button style="margin-bottom: 5px" class="btn btn-block btn-info btn-xs do_cancel" data-id="{{ $order->id }}" data-orderid="{{ $order->id }}">
            Отменить
        </button>
    @endif
@elseif ($order->status == status('exclusive_design', 'id'))
    <a href="{{ route('order.specification.create', $order->id) }}">
        <button class="btn btn-block btn-info btn-xs" style="margin-bottom: 5px">
            {{ $order->use_specification ? 'Редактировать ТЗ' : 'Сформировать ТЗ' }}
        </button>
    </a>
@endif

@if (($order->type_product != 999999) && in_array($order->status, status(['issued', 'received', 'closed_money_back', 'closed_with_discount'], 'id')->toArray()))
    <a href="/order/reorder/{{ $order->id }}">
        <button style="margin-bottom: 5px" class="btn btn-block btn-info btn-xs">
            Повторить заказ
        </button>
    </a>
@endif

@if (($order->status == 52 || $order->status == 53) && ($order->group->status != 100))
    <a href="javascript: void(0);"
       onclick="$('.defect_return_form').hide(); $('.defect_return_form_{{ $order->id }}').slideDown();">
        <button style="margin-bottom: 5px" class="btn btn-block btn-danger btn-xs">
            Заявить о браке
        </button>
    </a>
@endif
