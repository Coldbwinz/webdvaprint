<table id="table_history" class="table table-hover table-personal-orders">
    <tr>
        <td><b>#</b></td>
        <td><b>Наименование</b></td>
        <td><b>Тираж</b></td>
        <td><b>Превью</b></td>
        <td><b>Дата выдачи</b></td>
        <td><b>Цена</b></td>
        <td><b>Статус</b></td>
        <td><b>Действия</b></td>
    </tr>
    <?php $i = 0;?>
    @foreach ($orders as $order)
        <?php $i++;?>
        <tr>
            {{-- ID --}}
            <td>{{ $order->group_id }}.{{ $order->position }}</td>
            <?php
                if (isset($order->productType)) {
                    $productTypeName = $order->productType->name;
                } else {
                    $productTypeName = $order->price_details['selected_product_type_name'];
                }
            ?>

            {{-- Наименование --}}
            <td>{{ $productTypeName }}
                @if (isset($order->price_details['selected_product']))
                    <br>{{ $order->price_details['selected_product'] }}
                @endif
            </td>

            {{-- Тираж --}}
            <td>{{ $order->draw }} шт.</td>

            {{-- Превью --}}
            <td>
                @if (in_array($order->status, [2000, 2010]))
                    <p style="text-align: left">Макет еще не выбран</p>
                @elseif ($order->status == 10)
                    <p style="text-align: left">Макет еще не загружен</p>
                @else
                    @if ($order->img)
                        @if (filter_var($order->img, FILTER_VALIDATE_URL))
                            <img src="{{ $order->img }}" class="max-preview-img-size">
                        @else
                            @if (file_exists('lib/frx/'.$order->img))
                                <img style="text-align: left;" class="max-preview-img-size"
                                     src="/lib/frx/{{ $order->img }}" alt="Превью"/>
                            @else
                                <i class="fa fa-refresh fa-spin"></i>
                            @endif
                        @endif
                    @endif
                @endif
            </td>

            {{-- Дата выдачи --}}
            <td>{{ isset($order->price_details['close_date']) ? $order->price_details['close_date'] : '' }}</td>
            <td>{{ $order->price }} руб.</td>

            @if (! $order->statusInfo)
                {{-- Статус --}}
                <td>
                    <span class="label label-warning" style="background-color: #{{ $errorStatus->color }} !important;">
                        {{ $errorStatus->name }}
                    </span>
                </td>

                {{-- Действия --}}
                <td></td>
            @else
                {{-- Статус --}}
                @if ($order->group->subdomain)
                    <td>
                        @if (isset($order->rework_status_info))
                            <span class="label label-warning" style="background-color: {{ "#".$order->rework_status_info->color }} !important;">
                                {{ $order->rework_status_info->name }}
                            </span><br>
                        @endif

                        @if (in_array($order->status, [45, 52]))
                            <span class="label label-warning" style="background-color: #{{ $order->statusInfo->color }} !important;">В постпрессе</span>
                        @elseif ($order->status == 51)
                            <div class="label label-success">Готов</div>
                        @else
                            <span class="label label-warning" style="background-color: #{{ $order->statusInfo->color }} !important;">{{ $order->statusInfo->name }}</span>
                        @endif
                    </td>
                @else
                    <td>
                        @if (($order->status >= 25) && $order->status <= 40)
                            @if ($order->sort_order > 55)
                                <span class="label label-warning" style="background-color: {{ "#".status('on_rework', 'color') }} !important;">
                                    {{ status('on_rework', 'name') }}
                                </span><br>
                            @endif
                        @endif

                        @if ($order->reworked)
                            <span class="label label-info">Переделан</span><br>
                        @endif

                            {{-- Показывать доп статус "готов" --}}
                            @if (in_array($order->status, status(['sent_to_issue_point', 'on_issue'], 'id')->toArray()))
                                <span class="label label-success">Готов</span><br>
                            @endif

                        {{--
                            Если заказ приостановлен в производстве, не выводить статус Приостановлен в ЛК.
                            Приостановленная позиция не должна менять статус в ЛК.
                         --}}
                        @if ($order->status == 1000 && $order->stopped_user_id != Auth::user()->id)
                            {{-- Если позиция приостановлена в Истории, то в ЛК вернуть статус Приостановлен --}}
                            @if ($order->stopped_from == \App\Orders::STOPPED_FROM_HISTORY)
                                <span class="label label-warning" style="background-color: #{{ $order->statusInfo->color }} !important;">
                                    {{ $order->statusInfo->name }}
                                </span>
                            @else
                                <span class="label label-warning" style="background-color: #{{ $order->statusLastInfo->color }} !important;">
                                    {{ $order->statusLastInfo->name }}
                                </span>
                            @endif

                        @else
                            <span class="label label-warning" style="background-color: #{{ $order->statusInfo->color }} !important;">
                                @if ($order->status == 45)
                                    В постпрессе
                                @else
                                    @if ($order->status == 52)
                                        Получен
                                    @else
                                        {{ $order->statusInfo->name }}
                                    @endif
                                @endif
                            </span>
                        @endif
                        {{-- Показывать доп статус "оформлен самостоятельно" --}}
                        @if ($order->client->email == $order->manager->email)
                            <br><span class="label label-warning" style="background-color: {{ "#".status('self_created', 'color') }} !important;">
                                {{ status('self_created', 'name') }}
                            </span>
                        @endif

                        {{-- Показывать доп статус "готов" --}}
                        @if (in_array($order->status, status(['sent_to_issue_point', 'on_issue'], 'id')->toArray()))
                            <div class="text-muted text-small" style="margin-top: 10px;">
                                {{ $order->selected_issue_address }}
                            </div>
                        @endif
                    </td>
                @endif
                {{-- Действия --}}
                <td>
                    @include('personal._table._actions', ['order' => $order])
                </td>
            @endif
        </tr>
        @include('personal._table._forms', ['order' => $order])
    @endforeach
</table>

@include('orders.waiting_for_server')

<script type="application/javascript">

    var canPress = true;
    function validate_form_pay_type(obj) {
        if ((obj[1].value > 0) && (obj[3].checked || obj[4].checked || obj[5].checked)) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Укажите сумму и выберите способ оплаты'.");
        }
        return false;
    }

    function validate_form_reason(obj) {
        if (obj[1].value.length > 0) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Пожалуйста, заполните поле Комментарий");
        }
        return false;
    }

    function checkCanPress() {
        if (canPress) {
            canPress = false;
            return true;
        }
        return false;
    }

</script>

{{-- Pause order --}}
<script>
    @if (isset($order))
        $('.pause-btn').on('click', function (event) {

            event.preventDefault();

            let $button = $(this),
                orderId = $button.data('orderid'),
                self = this;

            $button.button('loading');

            $.get('/api/order-can-pause/' + orderId, function (response) {
                if (response.can_pause) {
                    $('.stopped_form').hide(); $('#stopped_state_' + orderId).val(1000); $('.stopped_form_' + orderId).slideDown();
                } else {
                    alert("Заказ №" + response.group_order_id + '.' + response.position + " уже запущен в работу и приостановить его невозможно.");
                }
            })
            .always(function () {
                $button.button('reset');
            });
        });
    @endif
</script>