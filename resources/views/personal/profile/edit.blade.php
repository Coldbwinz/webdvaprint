@extends('app')

@section('scripts')
    <link rel="stylesheet" href="/plugins/croppie/croppie.css"/>
    <script src="/plugins/croppie/croppie.js"></script>
    <script src="/plugins/croppie/croppie_code.js"></script>
@endsection

@section('content')

    @if ($errors->has())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование профиля</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form id="userForm" method="POST" action="{{ route('personal.profile.edit') }}" enctype="multipart/form-data" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">

                        <div class="form-group">
                            <label>Фото</label>
                            <div>
                                @if ($user->photo || old('photo'))
                                    <img src="/upload/users/{{ $user->photo }}"
                                         style="vertical-align: top; max-width: 90px">
                                @endif
                            </div>
                            @include('users.croppie')
                        </div>
                        <div class="form-group">
                            <label>Имя</label>
                            <input class="form-control" type="text" name="name"
                              required     value="{{ ( old('name') ? old('name') : $user->name) }}">
                        </div>

                        <div class="form-group">
                            <label>Фамилия</label>
                            <input class="form-control" type="text" name="lastname"
                               required    value="{{ ( old('lastname') ? old('lastname') : $user->lastname) }}">
                        </div>

                        <div class="form-group">
                            <label>Должность</label>
                            <input class="form-control" type="text" name="position"
                              required     value="{{ ( old('position') ? old('position') : $user->position) }}">
                        </div>

                        <div class="form-group">
                            <label>Эл. почта</label>
                            <input class="form-control" type="email" name="email"
                                required   value="{{ ( old('email') ? old('email') : $user->email) }}">
                        </div>

                        <div class="form-group">
                            <label>Телефон</label>
                            <input class="form-control" type="name" name="phone"
                                required   value="{{ ( old('phone') ? old('phone') : $user->phone) }}"/>
                        </div>

                        <div class="form-group">
                            <label>Пароль <span style="font-weight: 500;font-style: italic;">(минимум 6 символов)</span></label>
                            <input class="form-control" type="password" minlength="6"  name="password" value="">
                        </div>

                        <div class="form-group">
                            <label>Повтор пароля</label>
                            <input class="form-control" type="password" minlength="6"  name="password_confirmation" value="">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    </section><!-- /.content -->
@stop
