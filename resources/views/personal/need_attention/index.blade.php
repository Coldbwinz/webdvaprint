@extends('app')

@section('content')
    @if (Auth::user()->accessLevel() == 1)
        @include("admin.user.dashboard_panel")
    @endif

    <script src="/js/history.js"></script>
    <script src="/js/history_user.js"></script>
    <script>
        $(document).ready ( function() {
            @if ($message)
                $('#modalMessage').modal('show');
            @endif
            var selectedOrderId = 0;
            $(document).on('click', '.do_restore', function(e) {
                showStoppedForm(this, e, 0);
            });

            $(document).on('click', '.do_cancel', function(e) {
                showStoppedForm(this, e, 1010);
            });

            $('.cancel-message').click( function() {
                if ($('#stopped_state_' + selectedOrderId).val() == 1010) {
                    $('#stopped_state_' + selectedOrderId).val(0);
                    $('.stopped_form_' + selectedOrderId).submit();
                }
            });

            function showStoppedForm(_this, e, stoppedState) {
                var id = $(_this).data('id');
                selectedOrderId = $(_this).data('orderid');
                var form = $('.stopped_form_' + selectedOrderId);
                if (stoppedState != 1010) {
                    form.find('.stopped-form-commentary-title').show();
                    form.find('.stopped-form-commentary').show();
                    form.find('.stopped-form-commentary input').prop('required', true);
                    form.find('.pay-message').hide();
                    form.find('.accept-message').html('Подтвердить');
                    form.find('.cancel-message').html('Отмена');
                    form.find('.cancel-message').attr('type','button');
                } else {
                    if (form.find('.pay-message').html().trim().length > 10) {
                        form.find('.pay-message').show();
                        form.find('.accept-message').html('Готов погасить расходы');
                        form.find('.stopped-form-commentary input').prop('required', false);
                        form.find('.stopped-form-commentary-title').hide();
                        form.find('.stopped-form-commentary').hide();
                    } else {
                        form.find('.accept-message').html('Подтвердить');
                        form.find('.stopped-form-commentary input').prop('required', true);
                        form.find('.stopped-form-commentary-title').show();
                        form.find('.stopped-form-commentary').show();
                    }
                    form.find('.cancel-message').html('Продолжить выполнение заказа');
                    form.find('.cancel-message').attr('type','submit');
                }
                $('#stopped_state_' + selectedOrderId).val(stoppedState);
                form.show();
            }
        });
    </script>
    <div class="row">
        <div class="col-xs-12">
            @if ($message)
                <div id="modalMessage" class="modal">
                    <div class="modal-dialog" style="margin-top: 25%;">
                        <div class="modal-content" style="padding: 10px 2px;">
                            <div class="modal-header">
                                <h3 class="modal-title text-center">Выбранный вами заказ уже утверждён</h3>
                            </div>
                            <div class="modal-body" style="height:70px; padding-left: 0; line-height: 25px;">
                                <div class="col-md-12 text-left" style="font-size: 14px;">
                                    Вы находитесь в разделе - требуют внимания.
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" style="width: 100px;"
                                        data-dismiss="modal" aria-hidden="true" id="btnNext">Продолжить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="box">
                <div class="box-header with-border top_search_box">
                    <form>
                        <input class="form-control" name="page" style="display:none" value="{{ $current_page }}"/>
                        <input class="form-control" name="change" style="display:none" value="0"/>
                        <input class="form-control" name="href" style="display:none" value="/orders/need_attention"/>
                        <input type="text" class="form-control" name="user_search" placeholder="Введите слово для поиска..."/>
                    </form>
                </div>
                <div class="box-body table-responsive no-padding">
                    <div id="table_history_block">
                        @include('personal._table.orders_table')
                    </div>
                    <div id="div_pagination_user">
                        {!! $pagination !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop
