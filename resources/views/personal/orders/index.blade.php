@extends('app')

@section('content')
    @if (Auth::user()->accessLevel() == 1)
        @include("admin.user.dashboard_panel")
    @endif

    <script src="/js/history.js"></script>
    <script src="/js/history_user.js"></script>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border top_search_box">
                    <form>
                        <input class="form-control" name="page" style="display:none" value="{{ $current_page }}"/>
                        <input class="form-control" name="change" style="display:none" value="0"/>
                        <input class="form-control" name="href" style="display:none" value="/orders/user_orders"/>
                        <input type="text" class="form-control" name="user_search" placeholder="Введите слово для поиска..."/>
                    </form>
                </div>
                <div class="box-body table-responsive no-padding">
                    <div id="table_history_block">
                        @include('personal._table.orders_table')
                    </div>
                    <div id="div_pagination_user">
                        {!! $pagination !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop
