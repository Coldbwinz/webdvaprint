@extends('app')

@section('content')
    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th width="150">Название</th>
                        </tr>
                        <?php $i = 0;?>
                        @foreach ($client_statuses as $value)
                            <?php $i++?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $value->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! (new Landish\Pagination\Pagination($client_statuses))->render() !!}

                </div>
            </div>
        </div>
    </div>
    </section>
@stop