@extends('app')

@section('content')
    <div class="row">
        <!-- Массовое добавление -->
        <div class="col-md-6">
            <div class="box box-default box-solid collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Перенести в библиотеку</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                </div>
                
                <div class="box-body" style="display: none">                
                    <div class="form-group">
                        Выберите файлы и укажите параметры добавления
                    </div>                   

                    <div class="form-group">
                        <label>Типы продукции</label>
                        <select class="form-control" name="type" id="input-type-all" value="">
                          @foreach ($product_types as $product_type)
                            <option  value="{{ $product_type->id }}">{{ $product_type->name }}</option>
                          @endforeach   
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Тематика</label>
                        <select class="form-control select2" multiple name="themes[]" id="input-themes-all" data-placeholder="Выберите" style="width: 100%;">                  
                          @foreach ($themes as $theme)
                            <option  value="{{ $theme->id }}">{{ $theme->name }}</option>
                          @endforeach   
                        </select>
                    </div>
                    
                    <!-- error alert -->
                    <div class="col-md-12">
                        <div id="error-alert-box-all" style="display:none">
                            <div class="alert alert-danger alert-dismissible">                                        
                                <div id="error-message-all"></div>        
                            </div>
                        </div>

                        <div id="success-alert-box-all" style="display:none">
                            <div class="alert alert-success alert-dismissible">                    
                                <i class="icon fa fa-thumbs-o-up"></i>
                                Готово.
                            </div>                                    
                        </div>
                    </div><!-- error alert -->    
                    
                    <button class="btn btn-primary" id="add-to-library-mass">Добавить в библиотеку</button>
                </div>
            </div> 
        </div>
        
        <!-- PSD templates -->
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                      <h3 class="box-title">Psd-файлы</h3>
                      <div class="box-tools">
                          <div class="input-group" style="width: 150px;">
                              <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Поиск">
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                              </div>
                          </div>
                      </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
              
              
              
                    <table class="table table-hover">
                        <tr>
                            <th width="30"><input type="checkbox" class="input-check-all" id="input-check-all"></th>
                            <th width="45">ID</th>
                            <th width="120">Файл</th>                
                            <th width="280">Изображение</th>
                            <th>Папка</th>
                            <th width="120">Добавлен</th>
                            <th width="120">Изменен</th>
                        </tr>
                        
                        @foreach ($files as $value)
                            @include('parser.partials.file-item')
                        @endforeach

                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        
        </div>
    </div>

    @include('parser.partials.modal-merge')
    
    <div class="modal" id="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">                                    
                <div class="modal-body text-center">
                    <h3 style="color:#049804">Шаблоны объединены и добавлены в библиотеку</h3>                        
                </div>
                <div class="modal-footer">                                                                                
                    <button type="success" class="btn btn-primary" data-dismiss="modal">ОК</button>
                </div>                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
   

<script>
    var changeSide = false;
    var changeSideFirstId;
    var changeSideSecondId;
    
    $(document).ready(function(){
        //send to lib
        $('.add-to-library').click(function(){
            var id = $(this).data('id');            
            var sType = $('#input-type-'+id).val();
            var sThemes = $('#input-themes-'+id).val();            
            
            $.ajax({
                url: '/parser/library/move/'+id,
                method: 'post',
                dataType: 'json',
                data: {                    
                    type: sType,
                    themes: sThemes,
                },
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                },
                success: function(data) {
                    if (data.success == true) {             
                        $('.table-row-'+id).hide(300);                        
                    }
                    else {
                        var errors = '';
                        for (i=0; i<data.errors.length; i++) {
                            errors += '<div>' + data.errors[i] + '</div>';
                        }
                        $('#error-message-'+id).html(errors);
                        $('#error-alert-box-'+id).show();
                        $('#success-alert-box-'+id).hide();
                    }
                },
                error: function(xhr, status, err) {
                    $('#error-message-'+id).text(err);
                    $('#error-alert-box-'+id).show();
                    $('#success-alert-box-'+id).hide();
                }                
            });
        });
        
        // send to lib (mass)
        $('#add-to-library-mass').click(function() {            
            var checkboxes = $('.input-check:checked');
            
            if (checkboxes.length) {                                
                var sType = $('#input-type-all').val();
                var sThemes = $('#input-themes-all').val(); 
                
                var ids = [];
                for (i=0; i<checkboxes.length; i++) {
                    ids[i] = checkboxes.eq(i).data('id');
                }                
                
                $.ajax({
                    url: '/parser/library/massmove',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        ids: ids,
                        type: sType,
                        themes: sThemes,
                    },
                    headers: {
                        'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                    },
                    success: function(data) {
                        if (data.success == true) {                         
                            //$('#success-alert-box-all').show();
                            $('#error-alert-box-all').hide();
                            
                            // hide table row
                            for (i=0; i<checkboxes.length; i++) {
                                var id = checkboxes.eq(i).data('id');
                                $('.table-row-'+id).hide(500);                                 
                            }
                        }
                        else {
                            var errors = '';
                            for (i=0; i<data.errors.length; i++) {
                                errors += '<div>' + data.errors[i] + '</div>';
                            }                            
                            $('#error-message-all').html(errors);
                            $('#error-alert-box-all').show();
                            $('#success-alert-box-all').hide();
                            
                            // hide table row
                            if (data.error_code == 2) {                                
                                for (i=0; i<checkboxes.length; i++) {
                                    var id = checkboxes.eq(i).data('id');
                                    $('.table-row-'+id).hide(500);                                 
                                }
                            }
                        }
                    },
                    error: function(xhr, status, err) {
                        $('#error-message-all').text(err);
                        $('#error-alert-box-all').show();
                        $('#success-alert-box-all').hide();
                    } 
                })
            }
            else {
                alert('Выберите шаблоны');
            }
        });
        
        // check all
        $('#input-check-all').click(function() {
            var checkboxes = $('.input-check');
            checkboxes.prop('checked', this.checked);
        });
        
        // hide-button
        $('.hide-button').click(function() {
            var id = $(this).data('id');
            $.ajax({
                url: '/parser/hide/'+id,
                method: 'post',
                dataType: 'json',                
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                },
                success: function(data) {
                    if (data.success == true) {                        
                        $('.table-row-'+data.id).hide(500);
                    }
                }
            });
        });
        
        // change side
        $('.change-side').click(function() {            
            var id = $(this).data('id');
            var two = $('.change-side').not('.change-side-button-'+id);                        
            
            if (!changeSide) { 
                // start
                changeSide = true;
                changeSideFirstId = id;
                two.text('2 сторона');
                two.toggleClass('btn-primary');
                two.toggleClass('btn-success');
                $(this).attr('selected', 'selected');                
            }
            else {
                changeSideSecondId = id;
                if (id == changeSideFirstId) {
                    // cancel
                    changeSide = false;
                    two.text('1 сторона');
                    two.toggleClass('btn-primary');
                    two.toggleClass('btn-success');
                    $(this).attr('selected', false);
                }
                else {
                    // modal
                    modalValues(changeSideFirstId, changeSideSecondId);
                    showModal('#modal-merge');
                }
            }
            $(this).attr('selected', 'selected');                    
        });  
        
        function showModal(selector)
        {
            $(selector).modal('show');                    
            var top = (screen.height / 2) - ($(selector+' .modal-content').height() / 2);                    
            $(selector+' .modal-dialog').css('top', top - 100); 
        }
        
        function modalValues(id1, id2)
        {
            var html1 = '<td>' + id1 + '</td>';
            html1 += '<td>' + $('#file-name-'+id1).text() + '</td>';
            html1 += '<td><img src="' + $('#file-image-'+id1+' img').attr('src') + '" height="100"></td>';

            var html2 = '<td>' + id2 + '</td>';
            html2 += '<td>' + $('#file-name-'+id2).text() + '</td>';
            html2 += '<td><img src="' + $('#file-image-'+id2+' img').attr('src') + '" height="100"></td>';

            $('#modal-first-template').html(html1);
            $('#modal-second-template').html(html2);

            // form fields
            $('#mergeForm input[name=first_id]').val(id1);
            $('#mergeForm input[name=second_id]').val(id2);
        }
        
        @if (session('merge_success'))
            showModal('#modal-success');
        @elseif ($errors->has())
            showModal('#modal-merge');
        @endif
        
        @if (Request::old('_token'))
            modalValues({!! Request::old('first_id') !!}, {!! Request::old('second_id') !!})
        @endif

    });
</script>
@stop