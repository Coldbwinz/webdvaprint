@extends('app')

@section('content')

    <div class="progress">
        <div class="progress-bar progress-bar-success" style="width: {{ $parsedCount * 100 / $total }}%" title="Распарсено">
            <span>{{ round($parsedCount * 100 / $total, 1) }}% Complete</span>
        </div>
        <div class="progress-bar progress-bar-warning" style="width: {{ $newCount * 100 / $total }}%" title="Ожидают парсинга">
            <span>{{ round($newCount * 100 / $total, 1) }}% new</span>
        </div>
        <div class="progress-bar progress-bar-danger" style="width: {{ $errorCount * 100 / $total }}%" title="Ошибки парсинга">
            <span>{{ round($errorCount * 100 / $total, 1) }}% Errors</span>
        </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
              <div class="row">
                  <div class="col-md-3">
                      <h3 class="box-title">Psd-файлы</h3>
                  </div>

                  <div class="col-md-9">
                      <div class="box-tools">
                          @include('parser.partials.filters')
                      </div>
                  </div>
              </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table id="parser-files-table" class="table table-hover">

              <tr>
                <th>ID</th>
                <th>Файл</th>
                <th>Статус</th>
                <th>Полный путь</th>
                <th>Автор</th>
                <th>Номер</th>
                <th>Тип</th>
                <th>Сторона</th>
                <th>Миниатюра</th>
                <th>Добавлен</th>
                <th>Изменен</th>
              </tr>
              @foreach ($files as $value)
              <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->filename }}</td>
                <td>
                    <a href="#"
                       data-editable
                       data-pk="{{ $value->id }}"
                       data-name="status"
                       data-url="{{ route('parser.update', $value->id) }}"
                       data-type="select"
                       data-default-value="{{ $value->status }}"
                       data-source="['new', 'parse', 'move', 'error']"
                    >
                        {{ $value->status }}
                    </a>

                    @if ($value->status === 'error')
                        <a href="#errorsModal" class="btn btn-default btn-xs" title="Показать ошибки" data-id="{{ $value->id }}">
                            <i class="fa fa-bars"></i>
                        </a>
                    @endif
                </td>
                <td>{{ $value->path }}</td>
                <td>
                    <a href="#"
                       data-editable
                       data-pk="{{ $value->id }}"
                       data-name="vendor"
                       data-url="{{ route('parser.update', $value->id) }}"
                    >
                        {{ $value->vendor }}
                    </a>
                </td>
                <td>
                    <a href="#"
                       data-editable
                       data-pk="{{ $value->id }}"
                       data-name="number"
                       data-url="{{ route('parser.update', $value->id) }}"
                    >
                        {{ $value->number }}
                    </a>
                </td>
                <td>
                    <a href="#"
                       data-editable
                       data-pk="{{ $value->id }}"
                       data-name="type"
                       data-url="{{ route('parser.update', $value->id) }}"
                    >
                        {{ $value->type }}
                    </a>
                </td>
                <td>
                    <a href="#"
                       data-editable
                       data-pk="{{ $value->id }}"
                       data-name="side"
                       data-url="{{ route('parser.update', $value->id) }}"
                       data-type="select"
                       data-default-value="{{ $value->side }}"
                       data-source="['front', 'back']"
                    >
                        {{ $value->side }}
                    </a>
                </td>
                  <td>
                      @if ($value->status === 'move')
                          <div class="text-center">
                              <a href="{{ url($value->preview) }}" target="_blank">
                                  <img src="{{ url($value->preview) }}" alt="{{ $value->name }}" height="100">
                              </a>
                          </div>
                      @else
                        <p>Нет миниатюры</p>
                      @endif
                  </td>
                <td>{{ date('d.m.Y H:i', strtotime($value->created_at)) }}</td>
                <td>{{ date('d.m.Y H:i', strtotime($value->updated_at)) }}</td>
              </tr>
              @endforeach

            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        
        {!! $files->appends(Request::all())->render() !!}
        
      </nav>
      </div>
    </div>
  </section><!-- /.content -->

    <div class="modal fade" id="errorsModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="errorsModalLabel">Ошибки парсинга файла</h4>
                </div>
                <div class="modal-body">
                    <pre id="parser-errors"></pre>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('#parser-files-table').on('click', '[href="#errorsModal"]', function (event) {
            var $this = $(this),
                $modal = $('#errorsModal'),
                $parserErrors = $('#parser-errors');

            $modal.modal('show');
            $parserErrors.html('Загрузка...');

            $.get('/parser/' + $this.data('id') + '/errors', function (response) {
                $parserErrors.html(response);
            });

            event.preventDefault();
        });
    </script>
@endsection
