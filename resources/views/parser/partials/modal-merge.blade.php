<div class="modal" id="modal-merge">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="mergeForm" action="/parser/library/merge">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Объединить шаблоны</h4>
                </div>
                <div class="modal-body">

                    <div class="panel">
                        <div class="form-group">
                            <label>Типы продукции</label>
                            <select class="form-control" name="type">
                                @foreach ($product_types as $product_type)
                                    <option  value="{{ $product_type->id }}"<?php if (old('type') == $product_type->id) echo ' selected'; ?>>{{ $product_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Тематика</label>
                            <select class="form-control select2" multiple name="themes[]" data-placeholder="Выберите" style="width: 100%;">
                                @foreach ($themes as $theme)
                                    <option  value="{{ $theme->id }}"<?php if (old('themes') && in_array($theme->id, old('themes'))) echo ' selected'; ?>>{{ $theme->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if ($errors->has())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    <table class="table">
                        <tr>
                            <th width="45">ID</th>
                            <th>Файл</th>
                            <th width="160">Изображение</th>
                        </tr>
                        <tr id="modal-first-template">
                            <td>{{ session('first.id') }}</td>
                            <td></td>
                        </tr>
                        <tr id="modal-second-template"></tr>
                    </table>
                </div>
                <div class="modal-footer">
                    {!! csrf_field() !!}
                    <input type="hidden" name="first_id" value="">
                    <input type="hidden" name="second_id" value="">
                    <input type="hidden" name="page" value="1">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="success" class="btn btn-primary">Объединить</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->