<form action="{{ route('parser.index') }}" method="get" class="form-inline">
    <div class="form-group">
        <label for="status" class="form-label">Статус</label>
        <select name="status" id="status" class="form-control">
            <option value=""{{ Request::input('status') === '' ? ' selected' : '' }}>Любой</option>
            <option value="new"{{ Request::input('status') === 'new' ? ' selected' : '' }}>new</option>
            <option value="parse"{{ Request::input('status') === 'parse' ? ' selected' : '' }}>parse</option>
            <option value="move"{{ Request::input('status') === 'move' ? ' selected' : '' }}>move</option>
            <option value="error"{{ Request::input('status') === 'error' ? ' selected' : '' }}>error</option>
        </select>
    </div>

    <div class="form-group">
        <label for="vendor" class="form-label">Автор</label>
        <input type="text" id="vendor" name="vendor" class="form-control" value="{{ Request::input('vendor') }}"/>
    </div>

    <div class="form-group">
        <label for="number" class="form-label">Номер</label>
        <input type="text" id="number" name="number" class="form-control" value="{{ Request::input('number') }}"/>
    </div>

    <div class="form-group">
        <label for="type" class="form-label">Тип</label>
        <input type="text" id="type" name="type" class="form-control" value="{{ Request::input('type') }}"/>
    </div>

    <div class="form-group">
        <label for="side" class="form-label">Сторона</label>
        <select name="side" id="side" class="form-control">
            <option value=""{{ Request::input('side') === '' ? ' selected' : '' }}>Любой</option>
            <option value="front"{{ Request::input('side') === 'front' ? ' selected' : '' }}>front</option>
            <option value="back"{{ Request::input('side') === 'back' ? ' selected' : '' }}>back</option>
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-sm">
            Применить
        </button>
    </div>
</form>
