<tr class="table-row-{{ $value->id }}" height="37">
    <td rowspan="2"><input type="checkbox" class="input-check" data-id="{{ $value->id }}" title="Выбрать"></td>
    <td rowspan="2">{{ $value->id }}</td>
    <td id="file-name-{{ $value->id }}">{{ $value->filename }}</td>
    <td id="file-image-{{ $value->id }}" rowspan="2"><img src="{{ url($value->preview) }}" class="preview-image" style="border:solid 1px #d0d0d0"></td>
    <td>{{ $value->upload_folder }}</td>
    <td>{{ date('d.m.Y H:i', strtotime($value->created_at)) }}</td>
    <td>{{ date('d.m.Y H:i', strtotime($value->updated_at)) }}</td>
</tr>
<tr class="table-row-{{ $value->id }}">
    <td>
        <button class="btn btn-primary change-side change-side-button-{{ $value->id }}" data-id="{{ $value->id }}">1 сторона</button>
        <button class="btn btn-danger hide-button mt10 btn-xs" data-id="{{ $value->id }}">Скрыть</button>
    </td>
    <td colspan="3">
        <div class="box box-default box-solid collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Перенести в библиотеку</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->

            <div class="box-body" style="display: none">

                <div class="form-group">
                    <label>Типы продукции</label>
                    <select class="form-control" name="type" id="input-type-{{ $value->id }}" value="">
                        @foreach ($product_types as $product_type)
                            <option  value="{{ $product_type->id }}">{{ $product_type->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Тематика</label>
                    <select class="form-control select2" multiple name="themes[]" id="input-themes-{{ $value->id }}" data-placeholder="Выберите" style="width: 100%;">
                        @foreach ($themes as $theme)
                            <option  value="{{ $theme->id }}">{{ $theme->name }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- error alert -->
                <div class="col-md-12">
                    <div id="error-alert-box-{{ $value->id }}" style="display:none">
                        <div class="alert alert-danger alert-dismissible">
                            <div id="error-message-{{ $value->id }}"></div>
                        </div>
                    </div>

                    <div id="success-alert-box-{{ $value->id }}" style="display:none">
                        <div class="alert alert-success alert-dismissible">
                            <i class="icon fa fa-thumbs-o-up"></i>
                            Готово.
                        </div>
                    </div>
                </div><!-- error alert -->

                <div class="box-footer">
                    <button class="btn btn-primary add-to-library" data-id="{{ $value->id }}">Добавить</button>
                </div>
            </div><!-- /.box-body -->
        </div>
    </td>
</tr>
