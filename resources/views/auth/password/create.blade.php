@extends('auth')

@section('content')
    <style>
        .login-msg-title {
            text-align: center;
            margin-top: 4px;
        }
    </style>

    <h4 class="login-msg-title">Здравствуйте, {{ $user->contact->name }} {{ $user->contact->lastname }}!</h4>
    <p class="login-box-msg">Укажите Ваш пароль для входа</p>
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
        <ul class="alert alert-danger text-center">{!! $error !!}</ul>
        @endforeach
    @endif

    {!! Form::open([
            'url' => route('auth.password.new', $token),
            'method' => 'post'
        ]) !!}

        {!! Form::token() !!}
        <div class="form-group has-feedback">
            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Пароль']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            {!! Form::password('confirm_password', ['class'=>'form-control', 'placeholder'=>'Повтор пароля']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
        <div class="col-xs-12 pull-right">
            {!! Form::submit('Установить пароль', ['class' => 'btn btn-primary btn-block btn-flat']) !!}
        </div>
        </div>
    {!! Form::close() !!}
@stop
