@extends('auth')

@section('content')
    <p class="login-box-msg">Введите почту, чтобы восстановить доступ</p>
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
      <ul class="alert alert-danger text-center">{!! $error !!}</ul>
      @endforeach
    @endif

    {!! Form::open([
            'route' => 'auth.password.restore',
            'method' => 'post'
        ]) !!}

        {!! Form::token() !!}
        <div class="form-group has-feedback">
            {!! Form::email('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Электронная почта']) !!}
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <a href="{{ route('home') }}">
                    {!! Form::button('Назад', ['class' => 'btn btn-default btn-block btn-flat', 'style'=>"width:100%"]) !!}
                </a>
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Восстановить', ['class' => 'btn btn-primary btn-block btn-flat']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@stop
