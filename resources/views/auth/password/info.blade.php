@extends('auth')

@section('content')
    <style>
        .login-msg-title,
        .login-msg-success {
            text-align: center;
            margin-top: 4px;
        }
    </style>

    <h4 class="login-msg-title">Здравствуйте, {{ $user->contact->name }} {{ $user->contact->lastname }}!</h4>
    <p class="login-msg-success">Информация по восстановлению пароля отправлена на Ваш почтовый адрес.</p>
    <div class="row">
        <div class="col-xs-12 pull-right">
            <a href="{{ route('home') }}">
                {!! Form::button('Назад', ['class' => 'btn btn-default btn-block btn-flat', 'style'=>"width:100%"]) !!}
            </a>
        </div>
    </div>
@stop
