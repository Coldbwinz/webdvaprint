<div class="register-area">
    <div class="row">
        <div class="col-md-12">
            <div class="register-logo">
                <a href="/"><img style="border-radius: 150px;width: 150px;" src="{{ asset($logo) }}" alt="Logo"/></a>
            </div><!-- /.login-logo -->

            <h2>Добро пожаловать <br>в раздел самообслуживания</h2>
            <p class="register-box-msg">
                Тут можно самостоятельно рассчитать заказ и узнать дату его исполнения,<br>
                автоматически проверить свой макет или создать новый,<br>
                внеся свои данные в один из тысячи шаблонов.<br>
                Также вы сможете отслеживать<br>
                продвижение своего заказа на производстве<br>
                и вовремя узнать о его готовности.<br>
                <strong>
                    Для начала работы<br>
                    вам нужно только зарегистрироваться.
                </strong>
            </p>

            <div class="register-box-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger text-center">
                        @foreach ($errors->all() as $error)
                            <p>{!! $error !!}</p>
                        @endforeach
                    </div>
                @endif

                {!! Form::open([
                        'route' => 'auth.register',
                        'method' => 'post',
                        'id' => 'registerForm'
                    ]) !!}
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 pr0">
                            <div class="form-group has-feedback">
                                {!! Form::text('last_name', null, ['class'=>'form-control', 'required' => true, 'placeholder'=>'Фамилия']) !!}

                            </div>
                        </div>
                        <div class="col-md-8" style="padding-left:15px">
                            <div class="form-group has-feedback">
                                {!! Form::text('name', null, ['class'=>'form-control', 'required' => true, 'placeholder'=>'Имя (Отчество)']) !!}
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Электронная почта (логин в системе)',  'pattern'=>"[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+"]) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Телефон (для SMS уведомлений)']) !!}
                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                    </div>
                    <div class="form-group">
                        <select class="select2 form-control" name="is_company">
                            <option value="0" @if (old('is_company') != 1) selected @endif>Частное лицо</option>
                            <option value="1" @if (old('is_company') == 1) selected @endif>Юридическое лицо</option>
                        </select>
                    </div>
                    <div class="row @if (old('is_company') != 1) display-none @endif" id="is_company_panel">
                        <div class="col-md-8 pr0">
                            <div class="form-group has-feedback">
                                {!! Form::text('company_name', null, ['class'=>'form-control', 'placeholder'=>'Компания']) !!}
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left:15px">
                            <div class="form-group has-feedback">
                                {!! Form::text('position', null, ['class'=>'form-control', 'placeholder'=>'Должность']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mlr0 pt20">
                        <div class="col-md-2 plr0"></div>
                        <div class="col-md-4 plr0">
                            <button type="button" data-type="1" role="client_type" class="btn @if(old('client_type') != 3) btn-primary @else btn-default @endif btn-block btn-flat">Я - Заказчик</button>
                        </div>
                        <div class="col-md-4 plr0">
                            <button type="button" data-type="3" role="client_type" class="btn @if(old('client_type') == 3) btn-primary @else btn-default @endif btn-block btn-flat">Я - Посредник</button>
                        </div>
                        <div class="col-md-2 plr0"></div>
                        {!! Form::hidden('client_type', old('client_type')) !!}
                    </div>
                    <div class="display-none">
                        <input type="submit">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
