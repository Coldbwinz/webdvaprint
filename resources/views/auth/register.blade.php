@extends('register')

@section('content')
    @include('auth.register-form')
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $('button[role=client_type]').click(function(e){
        //$('button[role=client_type]').removeClass('btn-primary').addClass('btn-default');
        //$(this).removeClass('btn-default').addClass('bg-primary');
        $('input[name=client_type]').val($(this).data('type'));
        $('input[type=submit]').click();
    });

    $('select[name=is_company]').change(function(e){
        var is_company = $(this).val();
        if (is_company == 1) {
            $('#is_company_panel').show();
            $('input[name=company_name]').prop('required', true);
        } else {
            $('#is_company_panel').hide();
            $('input[name=company_name]').prop('required', false);
        }
    });

});
</script>
@endsection
