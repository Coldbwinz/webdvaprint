@extends('auth')

@section('content')
    <p class="login-box-msg">Войдите в систему чтобы продолжить</p>
    @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
      <ul class="alert alert-danger text-center">{!! $error !!}</ul>
      @endforeach
    @endif

    {!! Form::open([
            'route' => 'auth.login',
            'method' => 'post'
        ]) !!}

      {!! Form::token() !!}
      <div class="form-group has-feedback">
        <!--input type="email" name="email" value="@if (isset($email)) {{$email}} @else {{ old('email') }} @endif" class="form-control" placeholder="Электронная почта"-->
        {!! Form::email('email', (isset($email) ? $email : old('email')), ['class'=>'form-control', 'placeholder'=>'Электронная почта']) !!}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Пароль']) !!}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" style="display:none"> Запомнить меня
            </label>
          </div>
        </div>
        <div class="col-xs-4">
            {!! Form::submit('Войти', ['class' => 'btn btn-primary btn-block btn-flat']) !!}
        </div>
      </div>
    {!! Form::close() !!}

    <br/><br/>
    <a href="{{ route('auth.password.restore') }}">Я забыл свой пароль</a><br>
@stop
