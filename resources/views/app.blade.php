<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ settings()->get('meta_title') }}</title>
    <meta name="description" content="{{ settings()->get('meta_description') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- seletc2 -->
    <link rel="stylesheet" href="/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap3-editable/css/bootstrap-editable.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/croppie/croppie.css') }}"/>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/dist/css/app.css">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="{{ mix('css/main.css') }}">

    @yield('styles')

    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        window.Laravel = {
            baseUrl: '{{ url() }}',
            csrfToken: '{{ csrf_token() }}'
        };
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });
    </script>

    @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini <? if (isset($mini_menu)){?>sidebar-collapse<? } ?>">
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-79391842-1', 'auto');
    ga('send', 'pageview');
</script>
<div class="wrapper">

    @include("admin.header")

    @if (Auth::user()->isClient())
        @include("admin.menu.user")

    @elseif (Auth::user()->isManager())
        @include("admin.menu.manager")

    @elseif (Auth::user()->isAdmin())
        @include("admin.menu.admin")

    @elseif (Auth::user()->isRoot())
        @include("admin.menu.root")

    @elseif (Auth::user()->isPartner())
        @include("admin.menu.partner")

    @elseif (Auth::user()->isDirector())
        @include("admin.menu.director")

    @elseif (Auth::user()->isDesigner())
        @include("admin.menu.designer")
    @endif

            <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if (isset($page_title))
                    {{ $page_title }}
                @else
                    {{$page['breadcrumbs'][0] }}
                    <small>{{ $page['breadcrumbs'][1] }}</small>
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> {{ $page['breadcrumbs'][0] }}</a></li>
                <li class="active">{{ $page['breadcrumbs'][1] }}</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            @include('partials.messages')

            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

</div><!-- ./wrapper -->

<!-- Bootstrap 3.3.5 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.js"></script>
<!-- Sparkline -->
<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- select2 -->
<script src="/plugins/select2/select2.min.js"></script>
<script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>

<script src="https://vuejs.org/js/vue.js"></script>

<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/ru.js"></script>
<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="{{ asset('plugins/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script src="/js/app.js"></script>

<script type="text/javascript">

    var status_last = 0;
    var new_invoice = 1;
    @if (isset($order_id))
        var order_id = '<? echo $order_id; ?>';
        @if (isset($status_last)) status_last = {{ $status_last }}; @endif
        @if (isset($new_invoice)) new_invoice = {{ $new_invoice }}; @endif
    @endif
    var not_correct = '1px solid {{$colorSecondary}}';
    $(document).ready(function () {

        $('.datepicker').datepicker({
            language: 'ru',
            startDate: '0d',
            format: 'dd.mm.yyyy',
            autoclose: true
        });

        $("[data-mask]").inputmask();

        @if (isset($order_id))
        $("iframe").on("load", function () {
            $(this).contents().find(".change_page").click(function () {
                if (order_id) {
                    if (status_last == 0) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                            },
                            url: '/order/set_status_id/' + order_id + '/4',
                            type: 'get',
                            success: function (response) {
                                window.location.href = '/order/reviwer/' + new_invoice + '/' + order_id + '?sand=yes';
                            },
                            failed: function (data) {
                                console.log("ajax error");
                            }
                        });
                    } else {
                        @if (\Auth::user()->accessLevel() == 1)
                            window.location.href = '/orders/need_attention';
                        @else
                            window.location.href = '/orders/history';
                        @endif
                    }
                }
            });

            $(this).contents().find(".save_page").click(function () {
                if (order_id) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                        },
                        url: '/order/set_status_id/' + order_id + '/5',
                        type: 'get',
                        success: function(response){
                            if (response != 0) {
                                $('#modalContinueOrder').show();
                            } else {
                                @if (\Auth::user()->accessLevel() == 1)
                                        window.location.href = '/orders/need_attention';
                                @else
                                        window.location.href = '/orders/history';
                                @endif
                            }
                        },
                        failed: function(data) {
                            console.log("ajax error");
                        }
                    });
                }
            });

            $(this).contents().find(".show_block_div").click(function () {
                $('.bloc_div_overlay').show();
            });
        });
        @endif
    });
</script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

@yield('scripts')

<script src="/js/client_show.js"></script>
<script src="/js/prostiezvonki.js"></script>
<script src="/js/noty/jquery.noty.js"></script>
<script src="/js/noty/layouts/bottomRight.js"></script>
<script src="/js/noty/themes/default.js"></script>
<style>
    .select2-dropdown {
        z-index:10000001;
    }
</style>

<script>
    var callNumber = '';
    $(document).ready( function() {
        pzConnect();

        $(document).on('click', '.unknow_client', function(e) {
            $('<form action="/orders/new" method="POST">' +
                    '<input type="hidden" name="_token" value="{{ csrf_token() }}" />' +
                    '<input type="hidden" name="phone" value="'+ $(e.currentTarget).find('a').data('phone') +'">' +
                    '</form>').appendTo($(document.body)).submit();
        });

        $(document).on('click', '.select_client', function(e) {
            $(e.currentTarget).closest('.noty_text').find('.client_container').show();
        });


        $(document).on("select2:select", function (e) {
            if ($(e.target).closest('.select2-client-show').length > 0) {
                $selected = $(e.target).select2().find(':selected');
                $('<form action="/contact/show/' + $selected.data('href') + '" method="POST">' +
                        '<input type="hidden" name="_token" value="{{ csrf_token() }}" />' +
                        '<input type="hidden" name="phone" value="' + $selected.data('phone') + '">' +
                        '</form>').appendTo($(document.body)).submit();
            }
        });

        $(document).on('click','.noty_close', function() {
            $(this).closest('li').remove();
        });

        $(document).on('click', '.contact-phone', function() {
            callNumber = $(this).text();
            if (!pz.isConnected()) {
                pzConnect()
            }
            setTimeout(function() {
                if (pz.isConnected()) {
                    pz.call(callNumber);
                }
            }, 500);
        });
    });

    function pzConnect() {
        pz.connect({
            user_phone: {{ substr(\Auth::user()->phone, -3, 3) }},  // Номер менеджера
            host: "wss://mobile.prostiezvonki.ru:443", // Адрес сервера
            client_id: '860003',  // Пароль
            client_type: 'jsapi'    // Тип приложения
        });
    }

    function sanitizePhone(phone)
    {
        return phone.replace(/\D/g, '').slice(-10);
    }

    pz.onEvent(function (event) {
        switch (true) {
            case event.isIncoming():
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                    },
                    url: '/api/client/check_who_call',
                    data: {'phone_number' : sanitizePhone(event.from)},
                    type: 'POST',
                    success: function(response){
                        response = JSON.parse(response);
                        response.phone = event.from;
                        response.callID = event.callID;
                        showCard(response);
                    },
                    failed: function(data) {
                        console.log("ajax error");
                    }
                });
                break;
            case event.isIncomingAnswer():
                $('div[select_id="' + event.callID + '"]').attr('answered', true);
                break;
            case event.isHistory():
                var noty_evented = $('div[select_id="' + event.callID + '"]');
                if (noty_evented.length > 0) {
                    if (noty_evented.attr('answered')) {
                        noty_evented.html(noty_evented.html().replace('Входящий звонок', 'Принятый вызов'));
                        noty_evented.css('background-color', 'rgba(0, 166, 90, 0.2)')
                    } else {
                        noty_evented.html(noty_evented.html().replace('Входящий звонок', 'Пропущенный звонок'));
                        noty_evented.css('background-color', 'rgba(221, 75, 57, 0.2)')
                    }
                }
                break;
        }
    });

    function showCard(response) {
        var new_noty = noty({
            layout: 'bottomRight',
            closeWith: ['button'],
            text: getNotyCall(response)
        });
        new_noty.$message.attr('select_id', response.callID);
        $(".select2").select2();
    }

    function getNotyCall(response) {
        var date = new Date;
        var hh = date.getHours().toString();
        var mm = date.getMinutes().toString();
        var day = date.getDate().toString();
        var month = date.getMonth().toString();
        var date ='<b>' + hh + ':' +
                ((mm.length == 2) ? mm : '0' + mm) + '</b> ' +
                ((day.length == 2) ? day : '0' + day) + '.' +
                ((month.length == 2) ? month : '0' + month) + '.' + date.getFullYear();
        var text = '<span class="pz_noty_title" style="font-size:16px;">' + date + '<br>Входящий звонок &nbsp&nbsp<font class="contact-phone" style="font-size:20px;">' + response.phone + '</font></span><br>';

        if (response.contact) {
            text += '<span class="pz_noty_contact"><a href="/contact/show/' +
                    ((response.contact.contact_id) ? response.contact.contact_id : response.clients[0].pivot.contact_id) + '/' +
                    ((response.contact.client_id) ? response.contact.client_id : response.clients[0].pivot.client_id) + '" style="cursor: pointer" >' +
                    response.contact.name + ' ' + response.contact.last_name +'</a></span><br>';
        }
        if (response.clients && (response.clients.length > 0)) {
            $.each(response.clients, function (i, item) {
                text += '<span class="pz_noty_contact"><a href="/contact/show/' +
                        ((item.pivot) ? item.pivot.contact_id : item.contact_id) + '/' + ((item.pivot) ? item.pivot.client_id : item.client_id) + '" style="cursor: pointer" >' +
                        ((item.tm_name) ? ' компания: ' + item.tm_name : ('частное лицо : ' + item.name + ' ' + item.last_name)) + '</a></span>';
                if (i < response.clients.length) {
                    text += '<br>';
                }
            });
        }
        if(response.clients_list && (response.clients_list.length > 0)) {
            text += '<div class="row"><div class="col-md-12 container" style="width: 100%; margin-top:10px; padding: 0 15px;"><div class="btn btn-default pz_noty_contact unknow_client" style="width:47%; padding: 5px 6px;"><a data-phone="' +
                    response.phone +  '" style="cursor: pointer" >' +
                    'Добавить клиента</a></div>';
            text += '<div class="btn btn-default pz_noty_contact select_client" style="width:47%; margin-left:6%; padding: 6px 6px;">Выбрать клиента</div></div></div>'
            text += '<div class="row client_container" style="display:none; width:100%; margin-left:0; margin-right:0;"><br><select class="form-control select2 select2-client-show" style="width: 100%;" tabindex="-1">';
            $.each(response.clients_list, function (i, item) {
                text += '<option data-href="' + item.contact_id + '/' + item.client_id + '" data-phone="' +
                        response.phone + '">' + item.name + ((item.tm_name) ? (' ( ' + item.tm_name + ' )' ) : ' ( частное лицо )') + '</option>';
            });
            text += '</select></div>';
        }
        return text;
    }
</script>
</body>
</html>
