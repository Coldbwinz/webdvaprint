@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать тип доставки</h3>
                </div>
                <form method="POST" action="/delivery_type/edit/{{ $delivery_type->id }}" enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @if ($errors->has())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Вариант доставки</label>
                            <input class="form-control" type="text" name="name" value="{{ ( old('name') ? old('name') : $delivery_type->name) }}"/>
                        </div>
                        <div class="form-group">
                            <label>Стоимость</label>
                            <input class="form-control" type="text" name="cost" value="{{ ( old('cost') ? old('cost') : $delivery_type->cost) }}"/>
                        </div>
                        <div class="form-group">
                            <label>Срок</label>
                            <input class="form-control" type="text" name="days" value="{{ ( old('days') ? old('days') : $delivery_type->days) }}"/>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
@stop