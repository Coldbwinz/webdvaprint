@extends('app')

@section('content')
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>

    <div class="row">
        <div class="col-md-6">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg collapsed"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse"
                        data-target="#collapsed_container">Добавить новый тип доставки
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>
                    <form method="POST" action="/delivery_type/new" autocomplete="off" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            @if ($errors->has())
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Вариант доставки</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}"/>
                            </div>
                            <div class="form-group">
                                <label>Стоимость</label>
                                <input class="form-control" type="text" name="cost" value="{{ old('cost') }}"/>
                            </div>
                            <div class="form-group">
                                <label>Срок</label>
                                <input class="form-control" type="text" name="days" value="{{ old('days') }}"/>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th class="col-md-2">Номер</th>
                            <th class="col-md-4">Вариант доставки</th>
                            <th class="col-md-2">Стоимость</th>
                            <th class="col-md-2">Срок</th>
                            <th class="col-md-2">Действия</th>
                        </tr>
                        <?php $i = 0;?>
                        @foreach ($delivery_types_table as $value)
                            <?php $i++?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->cost }}</td>
                                <td>{{ $value->days }}</td>
                                <td>
                                    <a href="/delivery_type/edit/{{ $value->id }}">
                                        <button class="btn btn-success btn-xs">редактировать</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! (new Landish\Pagination\Pagination($delivery_types_table))->render() !!}

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop
