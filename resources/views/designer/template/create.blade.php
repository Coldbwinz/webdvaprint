@extends('app')

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::open(['route' => 'template.store', 'method' => 'post', 'files' => true]) !!}
                {!! csrf_field() !!}

                {{-- Psd файл --}}
                <div class="form-group{{ $errors->has('psd_file') ? ' has-error' : '' }}">
                    {!! Form::label('psd_file', 'Psd файл') !!}
                    {!! Form::file('psd_file', ['class' => 'form-control']) !!}
                    @if ($errors->has('psd_file'))
                        <div class="help-block">{{ $errors->first('psd_file') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Продукт</label>
                    {!! Form::select('product_type_id', $products->pluck('name', 'id'), null, ['class' => 'form-control']) !!}
                </div>

                {{-- Тематики --}}
                <div class="form-group">
                    <label>Тематики</label>

                    <div class="themes-selector">
                        @foreach($themes as $theme)
                            <label>
                                <input type="checkbox" name="themes[]" value="{{ $theme->id }}"> {{ $theme->name }}
                            </label><br>
                        @endforeach
                    </div>
                </div>

                {{-- Цвета --}}
                <div class="form-group">
                    <label>Цвета</label>

                    <div>
                        @foreach($themeColors as $color)
                            <label>
                                <input type="checkbox" name="colors[]" value="{{ $color->id }}"> {{ $color->color }}
                            </label><br>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Загрузить
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
