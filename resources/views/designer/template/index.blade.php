@extends('app')

@section('content')
    <div class="box">
        <div class="box-header">
            <a href="{{ route('template.create') }}" class="btn btn-primary">
                <i class="fa fa-upload"></i> Загрузить шаблон
            </a>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Статус</th>
                        <th>Миниатюра</th>
                        <th>Тематики</th>
                        <th>Цвета</th>
                        <th>Коллекция</th>
                        <th>Дата создания</th>
                        <th>Действия</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($templates as $template)
                        <tr>
                            <td>{{ $template->id }}</td>
                            <td>{{ \App\TemplateLibrary::getStatusReadableName($template->status) }}</td>
                            <td>
                                @if ($template->url)
                                    <a href="{{ $template->url }}" target="_blank">
                                        <img src="{{ $template->url }}" height="80"/>
                                    </a>
                                @else
                                    <i class="fa fa-refresh fa-spin fa-fw"></i>
                                    <span class="sr-only">Loading...</span>
                                @endif
                            </td>
                            <td>
                                @foreach($template->themes as $theme)
                                    <span class="label label-default">{{ $theme->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($template->colors as $color)
                                    <span class="label label-default">{{ $color->color }}</span>
                                @endforeach
                            </td>

                            <td>{{ $template->collection }}</td>

                            <td>{{ $template->created_at }}</td>

                            <td>
                                @if ($template->activity == false)
                                    <a href="{{ route('template.preview', $template->id) }}" class="btn btn-primary btn-block btn-xs">
                                        <i class="fa fa-check"></i> Утвердить
                                    </a>
                                @endif

                                <div class="btn-block">
                                    {!! Form::open(['method' => 'delete', 'route' => ['template.delete', $template->id]]) !!}
                                    <button type="submit" class="btn btn-danger btn-xs btn-block">
                                        <i class="fa fa-trash"></i> Удалить
                                    </button>
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
