@extends('app')

@section('content')
    <div class="box">
        <div class="box-header">
            {!! Form::open(['method' => 'patch', 'route' => ['template.approve', $template->id]]) !!}
                <button class="btn btn-primary">Утвердить</button>
            {!! Form::close() !!}
        </div>
        <div class="box-body">
            <iframe src="/lib/see/index.html?uuid={{ $template->w2p }}" frameborder="0" width="100%" style="height: 80vh;"></iframe>
        </div>
    </div>
@endsection
