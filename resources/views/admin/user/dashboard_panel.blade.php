<?php
    $dashboardPanelInfo = \App\Classes\Library\DashboardPanelInfo::userCabinetParams();
?>
<style>
    .small-box h3 {
        margin-bottom: 43px;
    }

    .small-box {
        padding-bottom: 26px;
        position: relative;
    }
    .small-box .small-box-footer {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
    }
    .small-box.active {
        color: white;
    }

    .box-attention {
        background: rgba(221, 75, 57, 0.2);
        color: #bc3f30;
    }
    .box-attention .ion {
        color: #bc3f30;
    }

    .box-attention .small-box-footer {
        background: #dd4b39;
    }
    .box-attention.active,
    .box-attention:hover {
        background: #dd4b39;
    }
    .box-attention.active .small-box-footer {
        display: none;
    }

    .box-payments {
        background: rgba(0, 166, 90, 0.2);
        color: #008d4c;
    }
    .box-payments .ion {
        color: #008d4c;
    }
    .box-payments .small-box-footer {
        background: #00a65a;
    }
    .box-payments.active,
    .box-payments:hover {
        background: #00a65a;
    }
    .box-payments.active .small-box-footer {
        display: none;
    }

    .box-my_orders {
        background: rgba(243, 156, 18, 0.2);
        color: #ce840f;
    }
    .box-my_orders .ion {
        color: #ce840f;
    }
    .box-my_orders .small-box-footer {
        background: #f39c12;
    }
    .box-my_orders.active,
    .box-my_orders:hover {
        background: #f39c12;
    }
    .box-my_orders.active .small-box-footer {
        display: none;
    }

    .box-new-order {
        background: rgba(0, 192, 239, 0.21);
        color: #00a3cb;
    }
    .box-new-order .ion {
        color: #00a3cb;
    }
    .box-new-order .small-box-footer {
        background: #00c0ef;
    }
    .box-new-order.active,
    .box-new-order:hover {
        background: #00c0ef;
    }
    .box-new-order.active .small-box-footer {
        display: none;
    }
</style>
<div class="row" style="">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-attention {{ $page['submenu'] === 'need_attention' ? ' active' : '' }}">
            <div class="inner">
                <h3>{{ $dashboardPanelInfo['need_attention'] | 0 }}</h3>
                <p>Требуют внимания!</p>
            </div>
            <div class="icon">
                <i class="ion ion-fireball"></i>
            </div>
            <a href="/orders/need_attention" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-payments {{ $page['submenu'] === 'payments' ? 'active' : '' }}">
            <div class="inner">
                <h3>{{ $dashboardPanelInfo['payments'] }}</h3>
                <p>Ожидают оплаты ({{ $dashboardPanelInfo['waiting_payment_total'] }} руб.)</p>
            </div>
            <div class="icon">
                <i class="ion ion-cash"></i>
            </div>
            <a href="/orders/payments" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-my_orders {{ $page['menu'] == 'order' && $page['submenu'] === 'my_orders' ? 'active' : '' }}">
            <div class="inner">
                <h3>{{ $dashboardPanelInfo['user_orders'] | 0 }}</h3>
                <p>Мои заказы</p>
            </div>
            <div class="icon">
                <i class="ion ion-clock"></i>
            </div>
            <a href="/orders/user_orders" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-new-order {{ $page['menu'] == 'order' && $page['submenu'] === 'new' ? 'active' : '' }}">
            <div class="inner">
                <h3>&nbsp;</h3>
                <p>Новый заказ</p>
            </div>
            <div class="icon">
                <i class="ion ion-android-cart"></i>
            </div>
            <a href="/orders/new-step1/{{Auth::user()->client->id}}" class="small-box-footer">Заказать <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
