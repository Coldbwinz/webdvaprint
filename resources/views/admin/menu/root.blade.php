<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><h4 style="color:#DDD;">Меню</h4></li>
            <li <?if($page['menu'] == "new-dashboard"){?>class="active"<?}?>>
                <a href="/new-dashboard">
                    <i class="fa fa-th"></i> <span>Панель инструментов</span>

                    <span class="pull-right-container">
                      <small class="label pull-right bg-red">New</small>
                    </span>
                </a>
            </li>
            @include('admin.menu.items.orders_history')
            @include('admin.menu.items.new_order')
            @include('admin.menu.items.turn_orders')
            @include('admin.menu.items.contractors')
            @include('admin.menu.items.settings')
            @include('admin.menu.items.spravochnik')
            @include('admin.menu.items.controls')
            @include('admin.menu.items.queue')

            <li>
                <a href="{{ route('employee.motivation') }}">
                    <span>Мотивация</span>
                </a>
            </li>

            <li>
                <a href="{{ route('templates') }}">
                    Загрузить шаблон
                </a>
            </li>
        </ul>
    </section>
</aside>
