<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><h4 style="color:#DDD;">Меню</h4></li>
            <li <?if($page['submenu'] == "history"){?>class="active"<?}?>>
                <a href="/orders/history">
                    <i class="fa fa-navicon"></i> <span>История заказов</span>
                </a>
            </li>
            <li <?if($page['submenu'] == "new"){?>class="active"<?}?>>
                <a href="/orders/new">
                    <i class="fa fa-file-text-o"></i> <span>Новый заказ</span>
                </a>
            </li>
            @if ($ordersHistoryCount > 0)
                <li <?if($page['submenu'] == "my_orders"){?>class="active"<?}?>>
                    <a href="{{ route('orders.archive') }}">
                        <i class="fa fa-navicon"></i> <span>Архив заказов</span>
                    </a>
                </li>
            @endif
            <li{!! $page['menu'] === 'contractors' ? ' class="active"' : '' !!}>
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span>Контрагенты</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li{!! $page['submenu'] === 'clients' ? ' class="active"' : '' !!}>
                        <a href="{{ route('contractors.clients') }}">
                            <i class="fa fa-smile-o"></i> Клиенты
                        </a>
                    </li>

                    <li{!! $page['submenu'] === 'employees' ? ' class="active"' : '' !!}>
                        <a href="{{ route('contractors.employees') }}">
                            <i class="fa fa-user"></i> Сотрудники
                        </a>
                    </li>
                </ul>
            </li>
            <li <?if($page['menu'] == "options"){?>class="active" <?}?> class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Настройки</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li <?if($page['submenu'] == "parameters"){?>class="active"<?}?>>
                        <a href="/options/parameters"><i class="fa fa-gears"></i>Параметры компании</a>
                    </li>
                    <li <?if($page['submenu'] == "client_types"){?>class="active"<?}?>>
                        <a href="/client_types"><i class="fa fa-sort-amount-asc"></i>Типы клиентов</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
