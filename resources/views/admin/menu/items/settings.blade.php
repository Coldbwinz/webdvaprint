<li <?if($page['menu'] == "options"){?>class="active" <?}?> class="treeview">
    <a href="#">
        <i class="fa fa-gear"></i>
        <span>Настройки</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>

    <ul class="treeview-menu">
        <li <?if($page['submenu'] == "parameters"){?>class="active"<?}?>>
            <a href="/options/parameters"><i class="fa fa-gears"></i>Параметры компании</a>
        </li>
        <li <?if($page['submenu'] == "index"){?>class="active"<?}?>>
            <a href="/products"><i class="fa fa-industry"></i>Типы продукции</a>
        </li>
        <li <?if($page['submenu'] == "client_types"){?>class="active"<?}?>>
            <a href="/client_types"><i class="fa fa-sort-amount-asc"></i>Типы клиентов</a>
        </li>
        <li <?if($page['submenu'] == "urgency"){?>class="active"<?}?>>
            <a href="/urgency"><i class="fa fa-forward"></i>Типы срочности</a>
        </li>
        <li <?if($page['submenu'] == "payment_types"){?>class="active"<?}?>>
            <a href="/payment_types"><i class="fa fa-money"></i>Типы оплаты</a>
        </li>
        <li <?if($page['submenu'] == "delivery_types"){?>class="active"<?}?>>
            <a href="/delivery_types"><i class="fa fa-suitcase"></i>Типы доставки</a>
        </li>
        @if (auth()->user()->isRoot())
        <li <?if($page['submenu'] == "client_statuses"){?>class="active"<?}?>>
            <a href="/client_statuses"><i class="fa fa-check"></i>Типы допуска в работу</a>
        </li>
        <li <?if($page['submenu'] == "order_statuses"){?>class="active"<?}?>>
            <a href="/order_statuses"><i class="fa fa-check"></i>Статусы заказов</a>
        </li>
        <li <?if($page['submenu'] == "options"){?>class="active"<?}?>>
            <a href="{{ route('options') }}"><i class="fa fa-institution"></i>Опции</a>
        </li>
        @endif

        @if (auth()->user()->isAdmin() || auth()->user()->isRoot())
            <li <?if($page['submenu'] == "widgets"){?>class="active"<?}?>>
                <a href="{{ route('widgets') }}"><i class="fa fa-podcast"></i>Виджеты</a>
            </li>
        @endif
    </ul>
</li>
