<li <?if($page['menu'] == "control"){?>class="active" <?}?>  class="treeview">
    <a href="#"><i class="fa fa-gear"></i> <span>Управление</span> <i
                class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li <?if($page['submenu'] == "code"){?>class="active"<?}?>><a href="/control/code"><i
                        class="fa fa-institution"></i>Статусы заказов</a>
        </li>

        <li <?if($page['submenu'] == "parser"){?>class="active"<?}?>>
            <a href="/parser">
                <i class="fa fa-gears"></i> <span>Парсер</span>
            </a>
        </li>

        <li <?if($page['submenu'] == "themes"){?>class="active"<?}?>>
            <a href="{{ route('templates.organize') }}">
                <i class="fa fa-television"></i> <span>По тематикам</span>
            </a>
        </li>
    </ul>
</li>
