<li <?if($page['menu'] == "catalogs"){?>class="active" <?}?>  class="treeview">
    <a href="#"><i class="fa fa-book"></i> <span>Справочники</span> <i
                class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li <?if($page['submenu'] == "template"){?>class="active"<?}?>>
            <a href="/template"><i class="fa fa-newspaper-o"></i>Шаблоны</a>
        </li>
        <li <?if($page['submenu'] == "types"){?>class="active"<?}?>>
            <a href="/products/types"><i class="fa fa-clone"></i>Тематики</a>
        </li>

        @if (auth()->user()->isRoot())
            <li <?if($page['submenu'] == "materials"){?>class="active"<?}?>>
                <a href="{{ route('dictionary.material') }}">
                    Материалы <small class="label pull-right bg-red">root</small>
                </a>
            </li>

            <li <?if($page['submenu'] == "equipment"){?>class="active"<?}?>>
                <a href="{{ route('dictionary.equipment') }}">
                    Оборудование <small class="label pull-right bg-red">root</small>
                </a>
            </li>
        @endif
    </ul>
</li>
