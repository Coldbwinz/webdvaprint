@if (Auth::user()->isAdmin() || Auth::user()->isRoot() || Auth::user()->isDesigner())
    <li <?if($page['menu'] == "design"){?>class="active"<?}?>>
        <a href="{{ route('queue.design') }}">
            <span>Очередь дизайна</span>
        </a>
    </li>

    <li <?if($page['menu'] == "prepress"){?>class="active"<?}?>>
        <a href="{{ route('queue.prepress') }}">
            <span>Препресс - очередь </span>
        </a>
    </li>
@endif

@if (Auth::user()->isAdmin() || Auth::user()->isRoot() || Auth::user()->isDirector())
    <li <?if($page['menu'] == "print"){?>class="active"<?}?>>
        <a href="{{ route('queue.print') }}">
            <span>Очередь печати</span>
        </a>
    </li>

    <li <?if($page['menu'] == "postpress"){?>class="active"<?}?>>
        <a href="{{ route('queue.postpress') }}">
            <span>Постпресс - очередь</span>
        </a>
    </li>
@endif
