<li{!! $page['menu'] === 'contractors' ? ' class="active"' : '' !!}>
    <a href="{{ url('users') }}">
        <i class="fa fa-group"></i>
        <span>Контрагенты</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>

    <ul class="treeview-menu">
        <li{!! $page['submenu'] === 'clients' ? ' class="active"' : '' !!}>
            <a href="{{ route('contractors.clients') }}">
                <i class="fa fa-smile-o"></i> Клиенты
            </a>
        </li>

        <li{!! $page['submenu'] === 'employees' ? ' class="active"' : '' !!}>
            <a href="{{ route('contractors.employees') }}">
                <i class="fa fa-user"></i> Сотрудники
            </a>
        </li>

        <li{!! $page['submenu'] === 'contractors' ? ' class="active"' : '' !!}>
            <a href="{{ route('contractors.contractors.index') }}">
                <i class="fa fa-black-tie"></i> Подрядчики
            </a>
        </li>

        <li{!! $page['submenu'] === 'partners' ? ' class="active"' : '' !!}>
            <a href="{{ route('partner.index') }}">
                <i class="fa fa-cubes"></i> Партнёры
            </a>
        </li>
    </ul>
</li>
