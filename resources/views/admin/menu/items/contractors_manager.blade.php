<li{!! in_array($page['submenu'], ['clients', 'contractors']) ? ' class="active"' : '' !!}>
    <a href="#">
        <i class="fa fa-group"></i>
        <span>Контрагенты</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>

    <ul class="treeview-menu">
        <li{!! $page['submenu'] === 'clients' ? ' class="active"' : '' !!}>
            <a href="{{ route('contractors.clients') }}">
                <i class="fa fa-smile-o"></i> Клиенты
            </a>
        </li>

        <li{!! $page['submenu'] === 'contractors' ? ' class="active"' : '' !!}>
            <a href="{{ route('contractors.contractors.index') }}">
                <i class="fa fa-black-tie"></i> Подрядчики
            </a>
        </li>
    </ul>
</li>
