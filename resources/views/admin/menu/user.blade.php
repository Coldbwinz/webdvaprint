<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><h4 style="color:#DDD;">Меню</h4></li>
            <li <?if($page['submenu'] == "new"){?>class="active"<?}?>>
                <a href="/orders/new-step1/{{ Auth::user()->client->id }}">
                    <i class="fa fa-file-text-o"></i> <span>Новый заказ</span>
                </a>
            </li>
            <li <?if($page['submenu'] == "need_attention"){?>class="active"<?}?>>
                <a href="{{ route('personal.orders.need_attention') }}">
                    <i class="fa fa-list-ol"></i> <span>Требуют внимания</span>
                </a>
            </li>
            <li <?if($page['submenu'] == "payments"){?>class="active"<?}?>>
                <a href="{{ route('personal.orders.payments') }}">
                    <i class="fa fa-list-ol"></i> <span>Ожидают оплаты</span>
                </a>
            </li>
            <li <?if($page['submenu'] == "my_orders"){?>class="active"<?}?>>
                <a href="{{ route('personal.orders.user_orders') }}">
                    <i class="fa fa-navicon"></i> <span>Мои заказы</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
