<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><h4 style="color:#DDD;">Меню</h4></li>
            @include('admin.menu.items.orders_history')
            @include('admin.menu.items.new_order')
            @include('admin.menu.items.turn_orders')
            @include('admin.menu.items.contractors')
            @include('admin.menu.items.settings')
            @include('admin.menu.items.queue')
        </ul>
    </section>
</aside>
