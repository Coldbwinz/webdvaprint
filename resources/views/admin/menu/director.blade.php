<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><h4 style="color:#DDD;">Меню</h4></li>
            <li <?if($page['submenu'] == "index"){?>class="active"<?}?>>
                <a href="/products">
                    <i class="fa fa-industry"></i> Типы продукции
                </a>
            </li>
            @include('admin.menu.items.queue')
        </ul>
    </section>
</aside>
