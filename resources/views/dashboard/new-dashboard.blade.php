@extends('app')

@section('content')
    <div id="root">
        <dashboard></dashboard>
    </div>
@stop

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
