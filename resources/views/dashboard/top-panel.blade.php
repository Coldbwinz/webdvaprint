<?php
    if (empty($active)) {
        $active = null;
    }
?>

<div class="row dashboard-top-buttons">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-approval{{ $active == 2 ? ' active' : '' }}">
            <div class="inner">
                <h3>{{ $status['approval'] }}</h3>
                <p>На согласовании</p>
            </div>
            <div class="icon">
                <i class="ion ion-clock"></i>
            </div>

            @if ($active == 2)
                <a href="/orders/history" class="small-box-footer">
                    <i class="fa fa-arrow-left"></i> Вернуться в историю
                </a>
            @else
                <a href="/orders/history/2" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
            @endif
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-need_attention{{ $active == 1 ? ' active' : '' }}">
            <div class="inner">
                <h3>{{ $status['need_attention'] }}</h3>
                <p>Требуют внимания!</p>
            </div>
            <div class="icon">
                <i class="ion ion-fireball"></i>
            </div>

            @if ($active == 1)
                <a href="/orders/history" class="small-box-footer">
                    <i class="fa fa-arrow-left"></i> Вернуться в историю
                </a>
            @else
                <a href="/orders/history/1" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
            @endif
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-ready{{ $active == 6 ? ' active' : '' }}">
            <div class="inner">
                <h3>{{ $status['ready'] }}</h3>
                <p>Готовы к выдаче</p>
            </div>
            <div class="icon">
                <i class="ion ion-android-cart"></i>
            </div>

            @if ($active == 6)
                <a href="/orders/history" class="small-box-footer">
                    <i class="fa fa-arrow-left"></i> Вернуться в историю
                </a>
            @else
                <a href="/orders/history/6" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
            @endif
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box box-waiting_payment{{ $active == 3 ? ' active' : '' }}">
            <div class="inner">
                <h3>{{ $status['waiting_payment'] }}</h3>
                <p>Ожидают оплаты ({{ $status['waiting_payment_total'] }} руб.)</p>
            </div>
            <div class="icon">
                <i class="ion ion-cash"></i>
            </div>

            @if ($active == 3)
                <a href="/orders/history" class="small-box-footer">
                    <i class="fa fa-arrow-left"></i> Вернуться в историю
                </a>
            @else
                <a href="/orders/history/3" class="small-box-footer">Смотреть <i class="fa fa-arrow-circle-right"></i></a>
            @endif
        </div>
    </div>
</div>
