@extends('app')

    @section('scripts')
    <script src="/dist/js/pages/dashboard2.js"></script>
    @endsection

    @section('content')
        @include('dashboard.top-panel')

    <div class="row" style="display: none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Заказы и оплаты</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <div class="btn-group">
                            <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i
                                        class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-center">
                                <strong>Продажи и заказы: 1 января 2016 - 1 мая 2016</strong>
                            </p>
                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div><!-- /.chart-responsive -->
                        </div><!-- /.col -->
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Заказы</strong>
                            </p>
                            <div class="progress-group">
                                <span class="progress-text">Черновики</span>
                                <span class="progress-number"><b>160</b>/200</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Согласование</span>
                                <span class="progress-number"><b>310</b>/400</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Ждет действия</span>
                                <span class="progress-number"><b>480</b>/800</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Готово</span>
                                <span class="progress-number"><b>250</b>/500</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-green"><i
                                            class="fa fa-caret-up"></i> 17%</span>
                                <h5 class="description-header">35,210.43 руб.</h5>
                                <span class="description-text">Общий доход</span>
                            </div><!-- /.description-block -->
                        </div><!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-yellow"><i
                                            class="fa fa-caret-left"></i> 0%</span>
                                <h5 class="description-header">10,390.90 руб.</h5>
                                <span class="description-text">Общая стоимость</span>
                            </div><!-- /.description-block -->
                        </div><!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <span class="description-percentage text-green"><i
                                            class="fa fa-caret-up"></i> 20%</span>
                                <h5 class="description-header">24,813.53 руб.</h5>
                                <span class="description-text">Общая прибыль</span>
                            </div><!-- /.description-block -->
                        </div><!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block">
                                <span class="description-percentage text-red"><i
                                            class="fa fa-caret-down"></i> 18%</span>
                                <h5 class="description-header">1200</h5>
                                <span class="description-text">Выполнено заказов</span>
                            </div><!-- /.description-block -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div class="row" style="">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Статистика недоступна, мало данных</h3>
                </div>
            </div>
        </div>
    </div>

    <!-- Main row -->
    <div class="row" style="display: none;">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Последние заказы</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Дата</th>
                                <th>Контакт</th>
                                <th>Тип продукции</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($orders as $value)
                                <tr>
                                    <td><a href="/order/info/{{ $value->id }}">{{ $value->id }}</a></td>
                                    <td>{{ date('d.m.Y H:i', strtotime($value->created_at)) }}</td>
                                    <td>
                                        <a href="/client/show/<? if (isset($value->client_info->id)) { ?>{{ $value->client_info->id }}<? } ?> "><? if (isset($value->client_info->name)) { ?>{{ mb_substr($value->client_info->name,0,20,'UTF-8') }}<? } ?> </a>
                                    </td>
                                    <td>{{ $value->type_name }}</td>

                                    @if ($value->status == 1)
                                        <td><span class="label label-info">Черновик</span></td>
                                    @endif

                                    @if ($value->status == 2)
                                        <td><span class="label label-warning">Ждет</span></td>
                                    @endif

                                    @if ($value->status == 3)
                                        <td><span class="label label-info">На согласовании</span></td>
                                    @endif

                                    @if ($value->status == 4)
                                        <td><span class="label label-info">Макет утвержден</span></td>
                                    @endif

                                    @if ($value->status == 5)
                                        <td><span class="label label-info">Ожидает оплаты</span></td>
                                    @endif

                                    @if ($value->status == 6)
                                        <td><span class="label label-info">Полностью оплачен</span></td>
                                    @endif

                                    @if ($value->status == 7)
                                        <td><span class="label label-info">Частично оплачен</span></td>
                                    @endif

                                    @if ($value->status == 8)
                                        <td><span class="label label-info">Печать без предоплаты</span></td>
                                    @endif

                                    @if ($value->status == 9)
                                        <td><span class="label label-info">Запущен в работу</span></td>
                                    @endif

                                    @if ($value->status == 10)
                                        <td><span class="label label-info">Пошел в печать</span></td>
                                    @endif

                                    @if ($value->status == 11)
                                        <td><span class="label label-info">Готов</span></td>
                                    @endif

                                    @if ($value->status == 12)
                                        <td><span class="label label-info">На выдаче</span></td>
                                    @endif

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="/orders/new" class="btn btn-sm btn-info btn-flat pull-left">Новый заказ</a>
                    <a href="/orders/history" class="btn btn-sm btn-default btn-flat pull-right">Все заказы</a>
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-4" style="display: none">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Inventory</span>
                    <span class="info-box-number">5,200</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Mentions</span>
                    <span class="info-box-number">92,050</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                  <span class="progress-description">
                    20% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Downloads</span>
                    <span class="info-box-number">114,381</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Direct Messages</span>
                    <span class="info-box-number">163,921</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 40%"></div>
                    </div>
                  <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@stop
