@extends('out_uploading')

@section('scripts_bottom')
    @parent
@stop

@section('content')
    <div class="edit-content" id="files">
        <h4 class="empty" @if(count($attachments) > 0) style="display:none" @endif>Вы пока ничего не загрузили</h4>
        @foreach ($attachments as $file)
            @include('upload.specification._file', ['file' => $file])
        @endforeach
    </div>
    <div class="aside-right">

        <div class="upload">
            <p class="btn btn-primary upload-btn">
                <span>Выберите файлы</span>
                <input type="file" name="file" id="fileupload" multiple>
            </p>
            <p class="big-blue-text">или</p>
            <p class="big-blue-text">перетащите их сюда</p>
            <p>размер файла до <b>100mb</b></p>
        </div>

        <div class="form-group link-group" id="linkGroup">
            @foreach ($links as $link)
                @include('upload.specification._link', ['link' => $link])
            @endforeach
            @if (count($links) < 7)
                @include('upload.specification._link', ['link' => null])
            @endif
        </div>

        <div class="form-group spec-group">
            <textarea id="specification" name="description" class="form-control specification" rows="5" placeholder="Опишите требования к дизайну...">{{ $description }}</textarea>
        </div>

        <div class="form-group bottom-btn-panel">
            <a href="{{ url('/orders/history') }}" class="btn btn-default mr10">Отмена</a>
            <button id="makeTechnicalTaskBtn" class="btn btn-primary">{{ (isset($action) && $action == 'edit') ? 'Редактировать ТЗ' : 'Сформировать ТЗ' }}</button>
        </div>
    </div>
@stop

@section('scripts')
<script>
    var orderId = {{ $order->id }};
    var specification = {{ isset($order->specification) ? 1 : 0 }};
    var complects_count = {{ (isset($complects_count)) ? $complects_count : 0 }};
    var links = [];
    var attachments = [];
    var data;

    $(document).ready(function() {
        var maxUploadedFileSize = 100 * 1024 * 1024;
        var uploadedFiles = [],
            attachedLinks = [];

        // User pick file
        $('#linkGroup').on('click', '.addLinkBtn', function (e) {
            var group = $(this).closest('.input-group');
            var input = group.find('input');
            var number = input.data('number');
            var inputs = $('#linkGroup input');

            if (input.val()) {
                var clone = group.clone();
                var btn = group.find('.addLinkBtn');
                input.data('is-end', 0);
                btn.removeClass('addLinkBtn').removeClass('btn-primary').addClass('btn-static').addClass('btn-static-success');
                btn.find('i').removeClass('fa-plus').addClass('fa-check');

                if (inputs.size() < 5) {
                    clone.find('input')
                            .data('is-end', 1)
                            .val('');
                    $('#linkGroup').append(clone);
                    attachedLinks.push(input.val());
                }

                $('#linkGroup').find('input[data-is-end=1]').focus();
            }
            else {
                input.focus();
            }
        });

        $('#linkGroup').on('keydown', '.addLink', function (e) {
            var code = e.keyCode || e.which;
            if(code == 13 && $(this).data('is-end') == 1) {
                $('.addLinkBtn').trigger('click');
            }
        });

        /**
         * Создание технического задания
         */
        $('#makeTechnicalTaskBtn').on('click', function () {
            if ($('#specification').val().length == 0) {
                $('#specification').focus();
                return false;
            }
            $('input[name="link[]"]').each(function(index) {
                if ($(this).val()) {
                    links.push($(this).val());
                }
            });

            $('.upload-item').each(function(index) {
                attachments.push($(this).data('id'));
            });

            data = {
                uploaded_files: attachments,
                description: $('#specification').val(),
                links: links,
            };

            // Make technical task
            if ((specification == 0) || (complects_count <= 1)) {
                sendChangesOfTZ((complects_count > 1) ? 0 : 1);
            } else {
                $('#modalAssignToAllComplects').show();
            }
        });

        //delete file
        $('#files').on('click', '.delete a', function() {
            var url = $(this).attr('href');
            var context = $(this).closest('.upload-item');

            $.post(url, {}, function(data) {
                if (data.success) {
                    context.fadeOut(1000).remove();
                }
            })
            .error(function() {
                var preview = context.find('.upload-preview');
                preview.append('<span class="text-danger">Ошибка</span>');
                context.addClass('upload-error');
            });
            return false;
        });

        // upload file
        $('#fileupload').fileupload({
            url: '/order/' + orderId + '/specification/files/upload',
            type: 'post',
            dataType: 'json',
            //autoUpload: false,
            minFileSize: 1,
            maxFileSize: maxUploadedFileSize,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            //previewMaxWidth: 100,
            //previewMaxHeight: 100,
            //previewCrop: true
        })
        .on('fileuploadadd', function (e, data) {
            var file = data.files[0];
            if (file.size <= maxUploadedFileSize) {
                $('h4.empty').hide();
                data.context = $('<div class="upload-item" title="'+file.name+'"/>').appendTo('#files');
                data.context.append('<div class="upload-preview" />');
                var displayName = file.name
                if (file.name.length >= 10) {
                    var extension = file.name.split('.').pop();
                    var basename = file.name.substr(0, file.name.length - extension.length - 1);
                    displayName = basename.substr(0, 10) + '...' + extension;
                }
                data.context.append('<p class="title"><span title="'+file.name+'">' + displayName + '</span><p>');
            }
        })
        .on('fileuploadprogress', function (e, data) {
            if (data.total > 1024 * 512) {
                var preview = data.context.find('.upload-preview');
                if (!preview.hasClass('in-progress')) {
                    preview.addClass('in-progress');
                    preview.append('<div class="progressbar"/>');
                }

                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find('.progressbar').css('height', progress + '%');
            }
        })
        .on('fileuploaddone', function (e, data) {
            var preview = data.context.find('.upload-preview');
            preview.removeClass('in-progress');
            data.context.find('.progressbar').remove();

            $.each(data.result.files, function (index, file) {
                data.context.data('id', file.id);
                data.context.find('progressbar').remove();
                data.context.append('<p class="delete"><a href="'+file.delete_url+'" class="text-danger">Удалить</a></p>');
                if (file.url) {
                    var node = file.thumbnail_url.length > 0
                                ? '<img src="'+file.thumbnail_url+'"/>'
                                : '<p>Готово</p>';

                    data.context.find('.upload-preview').append(node).fadeIn(500);

                    // Add to uploaded files
                    uploadedFiles.push(file.id);
                }
                else if (file.error) {
                    data.context.addClass('upload-error');
                    data.context.find('.upload-preview').append('<span class="text-danger">Ошибка</span>');
                }
            });
        })
        .on('fileuploadfail', function (e, data) {
            var file = data.files[0];
            var preview = data.context.find('.upload-preview');
            preview.append('<span class="text-danger">Ошибка</span>');
            data.context.addClass('upload-error');

            preview.removeClass('in-progress');
            data.context.find('.progressbar').remove();

            data.context.append('<p class="delete"><a href="'+file.delete_url+'" class="text-danger">Удалить</a></p>');
        })
        .prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
    function sendChangesOfTZ(one) {
        $.post('/order/' + orderId + '/specification/store/' + one, data, function (data) {
            if (data == 1) {
                $('#modalContinueOrder').show();
            } else if (data.redirect) {
                window.location.href = data.redirect;
            }
        });
    }
</script>
@include('upload.specification.modals.assign_to_all_complects')
@include('orders.continue_order_for_group')
@endsection
