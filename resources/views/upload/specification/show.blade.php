@extends('out_uploading')

@section('scripts_bottom')
    @parent
@stop

@section('content')
    <div class="edit-content" id="files">
        <h4 class="empty" @if(count($attachments) > 0) style="display:none" @endif>Вы пока ничего не загрузили</h4>
        @foreach ($attachments as $file)
            @include('upload.specification._file', ['file' => $file, 'show' => true])
        @endforeach
    </div>
    <div class="aside-right">

        <div class="mb20">
            @include('upload.specification.order-information')
        </div>

        <h4>Описание технического задания</h4>
        <div class="form-group">
            <textarea class="form-control specification showed" rows="5" disabled>{{ $description }}</textarea>
        </div>

        @if (!empty($links))
            <h4>Ссылки</h4>
            <div>
            @foreach ($links as $link)
                <p><a href="{{ $link }}">{{ $link }}</a></p>
            @endforeach
            </div>
        @endif

        <div class="form-group bottom-btn-panel">
            <a href="{{ URL::previous() }}"><button class="btn btn-primary">Назад</button></a>
        </div>
    </div>
@endsection
