<script>
    var assignButtonPressed = false;
    $(document).ready(function () {
        $('#btnForAll').on("click", function () {
            if (!assignButtonPressed) {
                assignButtonPressed = true;
                sendChangesOfTZ(0);
            }
        });

        $('#btnForOne').on("click", function () {
            if (!assignButtonPressed) {
                assignButtonPressed = true;
                sendChangesOfTZ(1);
            }
        });
    });
</script>

<div id="modalAssignToAllComplects" class="modal">
    <div class="modal-dialog" style="margin-top: 25%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Сделать изменения для всех комплектов?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 30%; " id="btnForAll">Для всех комплектов</button>
                <button type="button" class="btn btn-primary" style="width: 35%; margin-right: 18%;" id="btnForOne">Изменить только это ТЗ</button>
            </div>
        </div>
    </div>
</div>
