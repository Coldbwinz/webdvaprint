@if ($link)
    <div class="input-group add-link">
        <input type="text" name="link[]" class="form-control addLink" data-is-end="0" placeholder="Добавьте ссылку..." value="{{ $link }}">
        <span class="input-group-btn">
            <button class="addLinkBtn btn btn-static btn-static-success" type="button"><i class="fa fa-check"></i></button>
        </span>
    </div>
@else
    <div class="input-group add-link">
        <input type="text" name="link[]" class="form-control addLink" data-is-end="1" placeholder="Добавьте ссылку...">
        <span class="input-group-btn">
            <button class="addLinkBtn btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
        </span>
    </div>
@endif
