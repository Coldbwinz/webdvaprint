<h3>Информация о заказе</h3>

<div class="row">
    <div class="col-sm-4"><label>No:</label></div>
    <div class="col-sm-8">{{ $order->group_id }}.{{ $order->position }}</div>
</div>
<?php
if ($order->type_product != 999999) {
    $productName = $order->productType->name;
    $printlerType = $order->productType->printler_type;
} else {
    $productName = $order->price_details['selected_product_type_name'];
    $printlerType = $order->price_details['printler_type'];
}
?>

<div class="row">
    <div class="col-sm-4"><label>Продукт:</label></div>
    <div class="col-sm-8">
        {{ $productName }}

        @if ($order->price_details['selected_type'] !== '###')
            {{ $order->price_details['selected_type'] }}
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-4"><label>Тираж:</label></div>
    <div class="col-sm-8">
        {{ $order->draw }}
    </div>
</div>

@if ($complects_count)
<div class="row">
    <div class="col-sm-4" style="padding-right:0"><label>Кол-во комплектов</label></div>
    <div class="col-sm-8">
        {{ $complects_count }}
    </div>
</div>
@endif

<div class="row">
    <div class="col-sm-4"><label>Параметры:</label></div>
    <div class="col-sm-8">
        {{ !empty($order->price_details) ? $order->price_details['selected_size'] : '' }},
        {{ !empty($order->price_details) ? $order->price_details['selected_product'] : '' }}
        {{ $order->price_details['printler_type'] == 3 ? ' стр.' : '' }}
        @if ($order->price_details['printler_type'] != 3)
            , {{ !empty($order->price_details) ? $order->price_details['selected_chromacity'] : '' }}
        @endif
    </div>
</div>

@if ($order->price_details['printler_type'] == 3)
    <?php
        $size = $order->price_details['selected_size'];
        if ($order->price_details['selected_type'] === 'на скрепке') {
            $size = explode('x', $size);
            if ($size[0] > $size[1]) {
                $size[1] *= 2;
            } else {
                $size[0] *= 2;
            }
            $size = implode('x', $size);
        }
    ?>
    <div class="row">
        <div class="col-sm-4"><label>Вн. блок:</label></div>
        <div class="col-sm-8">
            {{ $size }},
            {{ $order->price_details['selected_material'] }},
            {{ $order->price_details['selected_chromacity'] }}
        </div>
    </div>

    @if (isset($order->price_details['selected_cover']))
        <div class="row">
            <div class="col-sm-4"><label>Обложка:</label></div>
            <div class="col-sm-8">
                {{ $size }},
                {{ $order->price_details['selected_cover'] }},
                {{ $order->price_details['selected_cover_chromacity'] }}
            </div>
        </div>
    @endif

    @if (isset($order->price_details['selected_pad']))
        <div class="row">
            <div class="col-sm-4"><label>Подложка:</label></div>
            <div class="col-sm-8">
                {{ $size }},
                {{ $order->price_details['selected_pad'] }},
                {{ $order->price_details['selected_pad_chromacity'] }}
            </div>
        </div>
    @endif
@endif
