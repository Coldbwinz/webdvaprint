@extends('out_uploading')

@section('content')
    <div class="edit-content upload-ready-products" id="files">
        <h4 class="empty" @if(count($readyProducts) > 0) style="display:none" @endif>
            <?php if ($order->productType) echo $order->productType->name; ?><br>
            Загрузка <?php
                if ($type == 'cover') echo 'обложки';
                if ($type == 'pad') echo 'подложки';
                if ($type == 'body') echo 'страниц';
            ?><br>
            <span class="count">{{ $filesCount }}
                @if ($filesCount == 1)
                    страница
                @elseif (in_array($filesCount % 10, [2,3,4]))
                    страницы
                @else
                    страниц
                @endif</span>
        </h4>
        @foreach ($readyProducts as $file)
            @include('upload.ready_layout._file', ['file' => $file])
        @endforeach
    </div>
    <div class="aside-right">
        <!-- step0 -->
        <div class="prepare{{ $order->price_details['selected_type'] === 'на скрепке' ? ' ' : ' hidden' }}">
            <h1>Выберите вариант загрузки</h1>
        </div>

        <!-- step1 -->
        <div class="step1">
            <div class="select-buttons{{ $order->price_details['selected_type'] === 'на скрепке' ? ' ' : ' 1hidden' }}">
                <div class="radiocheck">
                    <input type="radio" value="1" name="print_style" id="ps2" class="minimal" checked><label for="ps2" class="pl10">Постранично</label>
                </div>
                <div class="radiocheck">
                    <input type="radio" value="2" name="print_style" id="ps1" class="minimal"><label for="ps1" class="pl10">Разворотами</label>
                </div>
            </div>
        </div>

        <!-- step2 -->
        <div class="step2 display-none">
            <div class="select-buttons-step2 form-group{{ $order->price_details['selected_type'] === 'на скрепке' ? ' ' : ' hidden' }}"></div>

            <div id="uploadPanel" class="upload-outer">
                <div class="upload">
                    <p class="btn btn-primary upload-btn">
                        <span>Выберите файлы</span>
                        <input type="file" name="file" id="fileupload" accept="{{ $acceptedFileTypes }}" multiple>
                    </p>
                    <p class="big-blue-text">или</p>
                    <p class="big-blue-text">перетащите их сюда</p>
                    <p>типы файлов: <strong>{{ $acceptedFileTypes }}</strong></p>
                    <p>размер файла до <b>100mb</b></p>
                </div>
            </div>

            <div class="form-group">
                <div class="text-danger" id="message"></div>
            </div>
        </div>

        <div id="errorMessage" class="count-error-message"></div>

        <!-- step3 -->
        <div class="step3 display-none">
            <h1>Проверьте последовательность вывода страниц</h1>
            <p class="question-title text-center">При необходимости переместите позиции файлов курсором.</p>
        </div>

        <!-- continue -->
        <div id="continue" class="form-group bottom-btn-panel">
            <button id="continueBtn" class="btn btn-primary display-none">Продолжить</button>
        </div>
    </div>
@stop

@section('scripts')
<script>
$(document).ready(function() {
    var orderId = {{ $order->id }};
    var type = '{{ $type }}';
    var maxUploadedFiles = {{ $filesCount }};
    var pageCount = maxUploadedFiles;
    var maxUploadedFileSize = 100 * 1024 * 1024;
    var step = 1;

    var uploadedFiles = [],
        attachedLinks = [];

    $('.minimal').iCheck({
        radioClass: 'iradio_minimal-blue',
    });

    function createCountName(value) {
        if (value == 1) {
            return 'файл';
        } else if ($.inArray(value % 10, [2,3,4]) >= 0) {
            return 'файла';
        } else {
            return 'файлов';
        }
    }

    // check continue
    function checkContinue(withError) {
        withError = (typeof withError !== 'undefined') ? withError : true;
        var itemCount = $('.upload-item').not('.upload-error').size();
        var diff = itemCount - pageCount;

        if (diff == 0) {
            $('#continueBtn').show();
            $('#errorMessage').hide();
            $('#uploadPanel').find('.upload').hide();
        } else {
            if (withError) {
                if (diff > 0) {
                    $('#errorMessage').html('<div class="red-color">Вы загрузили слишком много файлов<br>Удалите лишние<br><div style="font-size:82px; padding-top:30px; display: block">' + diff + '<div></div>');
                    $('#uploadPanel').find('.upload').hide();
                } else if (diff < 0) {
                    diff = diff * (-1);
                    $('#errorMessage').html('<div class="green-color">Загруженных файлов не хватает<br>Добавьте недостающие<br><div class="big-number">' + diff + '<div></div>');
                    $('#uploadPanel').find('.upload').show();
                }
            }
            $('#continueBtn').hide();
            $('#errorMessage').show();
        }
    }

    // step2
    function step2() {
        step = 2;
        $('.step1').hide();
        $('.step2').show();
        $('.select-buttons').appendTo('.select-buttons-step2');
    }

    // step3
    function step3() {
        step = 3;
        $('.prepare').hide();
        $('.step2').hide();
        $('.step3').show();
        $('.upload-item .delete').hide();
    }

    // sort position
    function sortPosition() {
        var sort = [];
        $('.upload-item').each(function(index) {
            if (!$(this).hasClass('upload-error')) {
                sort.push($(this).data('id'));
            }
        });
        return sort;
    }

    // recheck position
    function recheckPosition() {
        var sort = sortPosition();
        $.each(sort, function(index, id) {
            $('.upload-item[data-id='+id+']').find('.position').text(index + 1);
        });
        return sort;
    }

    $('input').on('ifChanged', function (event) { $(event.target).trigger('change'); });

    // radio check
    $('input[name=print_style]').change(function() {
        var memStep = step;
        if (step == 1) {
            step2();
        }
        var printStyleFactor = $('input[name=print_style]:checked').val();
        pageCount = maxUploadedFiles / printStyleFactor;
        $('#filesCount').text(pageCount + ' ' + createCountName(pageCount));

        checkContinue();
    }).trigger('change');

    // continue
    $('#continueBtn').on('click', function () {
        if (step == 3 || pageCount == 1) {
            $('#continueBtn').prop('disabled', true);

            // Проверить, что макеты загружены для всех элементов
            $.get('/order/'+orderId+'/upload/layout/check-ready', function (data) {
                if (data.status === 'error') {
                    alert(data.message);
                    window.location.href = '/order/'+data.complect_id+'/upload/layout/start';
                    return;
                }

                // Если все макеты загружены
                $.post('/order/'+orderId+'/upload/layout/finish', function (data) {
                    if (data.redirect) {
                        window.location.href = data.redirect;
                        return;
                    }

                    alert('Возникла ошибка при сохранении.');
                })
                    .always(function () {
                        $('#continueBtn').prop('disabled', false);
                    });
            });
        } else if (step == 2) {
            step3();
            checkContinue();
        }
    });

    // sortable items
    $("#files").sortable({
        connectWith: '.upload-item',
        handle: ".upload-preview",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder",
        update: function(e, ui) {
            sort = recheckPosition();
            $.post('/order/' + orderId + '/upload/layout/type/'+type+'/files/sort', {sort: sort});
        },
        start: function (e, ui) {
            $(ui.item[0]).find('.upload-preview').addClass('portlet-wrapper');
            $(ui.item[0]).find('.upload-bottom').hide();
        },
        stop: function(e, ui) {
            $(ui.item[0]).find('.upload-preview').removeClass('portlet-wrapper');
            $(ui.item[0]).find('.upload-bottom').show();
        },
    });
    $("#files").disableSelection();

    //delete file
    $('#files').on('click', '.delete a', function() {
        var url = $(this).attr('linkhref');
        var context = $(this).closest('.upload-item');

        if (context.hasClass('upload-error')) {
            context.fadeOut(1000).remove();
        }
        else {
            $.post(url, {}, function(data) {
                if (data.success) {
                    context.fadeOut(1000).remove();
                    if (step > 1) {
                        checkContinue();
                    }
                    recheckPosition();
                }
            })
            .error(function() {
                var preview = context.find('.upload-preview');
                preview.append('<span class="text-danger">Ошибка</span>');
                context.addClass('upload-error');
            });
        }

        return false;
    });

    // add Dots to filename (ex. return 'file_numbe...jpg');
    function cropDisplayName(filename, size) {
        var displayName = filename;
        if (filename.length >= size) {
            var extension = filename.split('.').pop();
            var basename = filename.substr(0, filename.length - extension.length - 1);
            displayName = basename.substr(0, size) + '...' + extension;
        }
        return displayName;
    }

    // make context DOM for new upload file
    function makeFileUploadContextDOM(filename) {
        var uploadItem = $('<div class="upload-item"/>');
        uploadItem.append('<div class="upload-preview" />');
        uploadItem.append('<div class="upload-bottom"><p class="title"><span>' + cropDisplayName(filename, 10) + '</span><p></div>');
        uploadItem.appendTo('#files');
        return uploadItem;
    }

    // upload file
    $('#fileupload').fileupload({
        url: '/order/' + orderId + '/upload/layout/type/'+type+'/files/add',
        type: 'post',
        dataType: 'json',
        //autoUpload: false,
        minFileSize: 1,
        maxFileSize: maxUploadedFileSize,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
    })
    .on('fileuploadadd', function (e, data) {
        var file = data.files[0];
        if (file.size <= maxUploadedFileSize) {
            $('h4.empty').hide();
            data.context = makeFileUploadContextDOM(file.name);
        }
    })
    .on('fileuploadprogress', function (e, data) {
        console.log(data.total);
        if (data.total > 1024 * 1024) {
            var preview = data.context.find('.upload-preview');
            if (!preview.hasClass('in-progress-circle')) {
                preview.addClass('in-progress-circle');
                preview.append('<div class="progressbar"><div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></div>');
            }

            //var progress = parseInt(data.loaded / data.total * 100, 10);
            //data.context.find('.progressbar').css('height', progress + '%');
        }
    })
    .on('fileuploaddone', function (e, data) {
        var preview = data.context.find('.upload-preview');
        preview.removeClass('in-progress-circle');
        data.context.find('.progressbar').remove();

        if (data.result.error) {
            data.context.remove();
            alert(data.result.error);
            return;
        }

        // set context to first item
        var context = data.context;

        $.each(data.result.files, function (index, file) {
            if (index > 0) {
                context = makeFileUploadContextDOM(file.name);
            }
            context.attr('data-id', file.id);
            context.prepend('<div class="position"/>');
            context.find('.upload-bottom').append('<p class="delete"><a linkhref="'+file.delete_url+'" class="text-danger">Удалить</a></p>');
            if (file.url) {
                var node = file.thumbnail_url.length > 0
                            ? '<img src="'+file.thumbnail_url+'"/>'
                            : '<p>Готово</p>';

                context.find('.upload-preview').append(node).fadeIn(500);

                // Add to uploaded files
                uploadedFiles.push(file.id);

                firstUploadFileExtetion = file.type;

                recheckPosition();
            }
            else if (file.error) {
                context.addClass('upload-error');
                context.find('.upload-preview').append('<span class="text-danger">Ошибка</span>');
            }
        });
        checkContinue();
    })
    .on('fileuploadfail', function (e, data) {
        var file = data.files[0];
        var preview = data.context.find('.upload-preview');
        preview.append('<span class="text-danger">Ошибка</span>');
        data.context.addClass('upload-error');

        preview.removeClass('in-progress-circle');
        data.context.find('.progressbar').remove();

        data.context.find('.upload-bottom').append('<p class="delete"><a linkhref="'+file.delete_url+'" class="text-danger">Удалить</a></p>');
    })
    .prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

});
</script>
@endsection
