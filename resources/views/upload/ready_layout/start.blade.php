@extends('out_loading')

@section('scripts_bottom')
    @parent
@stop

@section('content')
    <br><br><br><br><br>

    @foreach($complect as $order)
        <div class="text-center">
            <h3>Заказ №{{ $order->group_id }}.{{ $order->position }}</h3>
        </div>
        @foreach ($order->price_details['print_structure'] as $type => $value)
            @if ($value['chromacity_front'] !== 'б')
                <div style="text-align: center;">
                    <a href="{{ route('order.upload.layout.type', ['order_id' => $order->id, 'type' => $type]) }}" style="font-size: 20px" class="btn red">
                        <? $founded = false; ?>
                        @foreach($order->readyProducts as $ready)
                            @if ($ready->type == $value['type'])
                                <? $founded = true; ?>
                            @endif
                        @endforeach
                        @if ($founded)
                            @if ( $value['type'] == 'body')
                                <span>
                                    @if ($order->price_details['printler_type'] == 1)
                                        Макет загружен
                                    @else
                                        Страницы загружены
                                    @endif
                                    @if ($order->readyProducts->where('type', $value['type'])->count() > $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Уберите лишние файлы)
                                    </span>
                                    @elseif ($order->readyProducts->where('type', $value['type'])->count() < $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Добавьте недостающие файлы)
                                    </span>
                                    @endif
                            </span>
                            @elseif ( $value['type'] == 'cover' )
                                <span>
                                Обложка загружена
                                    @if ($order->readyProducts->where('type', $value['type'])->count() > $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Уберите лишние файлы)
                                    </span>
                                    @elseif ($order->readyProducts->where('type', $value['type'])->count() < $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Добавьте недостающие файлы)
                                    </span>
                                    @endif
                            </span>
                            @elseif ( $value['type'] == 'pad' )
                                <span>
                                Подложка загружена
                                    @if ($order->readyProducts->where('type', $value['type'])->count() > $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Уберите лишние файлы)
                                    </span>
                                    @elseif ($order->readyProducts->where('type', $value['type'])->count() < $value['count'])
                                        <span class="{{ $order->readyProducts->where('type', $value['type'])->count() == $value['count'] ? 'text-success' : 'text-danger' }}">
                                        (Добавьте недостающие файлы)
                                    </span>
                                    @endif
                            </span>
                            @endif
                        @else
                            @if ( $value['type'] == 'body')
                                @if ($order->price_details['printler_type'] == 1)
                                    Загрузить макет
                                @else
                                    Загрузить страницы
                                @endif
                            @elseif ( $value['type'] == 'cover' )
                                Загрузить обложку
                            @elseif ( $value['type'] == 'pad' )
                                Загрузить подложку
                            @endif
                        @endif
                    </a>
                </div>
            @endif
        @endforeach
    @endforeach
@stop
