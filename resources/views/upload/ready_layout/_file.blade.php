<?php
    $displayedName = pathinfo($file->original_name, PATHINFO_BASENAME);
    if (strlen($displayedName) > 11) {
        $extension = pathinfo($file->original_name, PATHINFO_EXTENSION);
        $filename = pathinfo($file->original_name, PATHINFO_FILENAME);
        $displayedName = substr($filename, 0, 11).'...'.$extension;
    }
?>
<div class="upload-item" data-id="{{ $file->id }}">
    <div class="position">{{ $file->position + 1 }}</div>
    <div class="upload-preview">
        <img src="{{ asset($file->preview) }}">
    </div>
    <div class="upload-bottom">
        <p class="title">{{ $displayedName }}</p>
        @if (isset($show) && $show)
        <p class="download"><a href="{{ $file->url }}" download>Скачать</a></p>
        @else
        <p class="delete"><a linkhref="{{ route('order.upload.layout.files.delete', ['order_id' => $order->id, 'type' => $type, 'id' => $file->id]) }}" class="text-danger">Удалить</a></p>
        @endif
    </div>
</div>
