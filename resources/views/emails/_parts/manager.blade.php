<b>{{ $manager->contact ? $manager->contact->name : $manager->name }} {{ $manager->contact ? $manager->contact->lastname : $manager->lastname }}</b>
<br>
@if ($position = ($manager->contact ? $manager->contact->position : $manager->position))
    {{ $position }}@if ($tm_name = ($manager->client ? $manager->client->tm_name : '')),@endif
@endif
@if ($tm_name = ($manager->client ? $manager->client->tm_name : ''))
    {{ $tm_name }}
@endif
<br>
@if ($phone = ($manager->contact ? $manager->contact->phone : $manager->phone))
    {{ $phone }}@if ($email = ($manager->contact ? $manager->contact->email : $manager->email)),@endif
@endif
@if ($email = ($manager->contact ? $manager->contact->email : $manager->email))
    {{ $email }}
@endif
