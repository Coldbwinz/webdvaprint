Здравствуйте, {{ $client->contacts()->first()->name }}!<br>
По Вашей заявке сформирован счет (во вложении).<br>
Чтобы ускорить запуск заказа в работу, можно подтвердить его оплату в
<a href="{{ url("auth/login_token/{$to}/{$login_token}/1") }}">личном кабинете</a>.<br>
<br>
Ваш персональный менеджер {{ $manager->contact ? ($manager->contact->name.' '.$manager->contact->lastname) : ($manager->name.' '.$manager->lastname) }}<br>

@if ($phone = ($manager->contact ? $manager->contact->phone : $manager->phone))
телефон {{ $phone }}<br/>
@endif

@if ($email = ($manager->contact ? $manager->contact->email : $manager->email))
e-mail: {{ $email }}<br>
@endif
