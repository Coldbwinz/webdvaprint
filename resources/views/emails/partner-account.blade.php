<p>Здравствуйте, {{ $contact->name }}.</p>

<p>
    <strong>Логин:</strong> {{ $client->account->email }} <br>
    <strong>Пароль:</strong> {{ $password }}
</p>

<p>Ссылка для входа в личный кабинет: <a href="{{ $link }}" target="_blank">{{ $link }}</a></p>