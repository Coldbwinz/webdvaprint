Здравствуйте, {{ $contractor->name }}. <br>
<br>
Просим немедленно проиостановить все работы по заказу № <a href="{{ route('contractor.order', ['token' => $token->token]) }}">{{ $order->group->id.'.'.$order->position }}</a><br>
в связи с пожеланиями клиента "{{ $order->stopped_reasons['1000'] }}"<br>
О дальнейших действиях сообщим позднее.<br><br>

@include('emails._parts.manager', compact('manager'))