Здравствуйте, {{ $contractor->name }}. <br>
<br>
Отправляем вам заказ № {{ $order->group->id.'.'.$order->position }} на исполнение<br>
Для просмотра ТЗ перейдите по  <a href="{{ route('contractor.order', ['token' => $token->token]) }}">ссылке</a>
<br><br>

@include ('emails._parts.manager', compact('manager'))
