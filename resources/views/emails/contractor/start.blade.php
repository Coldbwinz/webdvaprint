Здравствуйте, {{ $contractor->name }}. <br>
<br>
Просим продолжить выполнение работ по заказу № <a href="{{ route('contractor.order', ['token' => $token->token]) }}">{{ $order->group->id.'.'.$order->position }}</a><br>
в связи с пожеланиями клиента "{{ $order->stopped_reasons['0'] }}"<br>
Спасибо за понимание.<br><br>

@include('emails._parts.manager', compact('manager'))
