<p>Здравствуйте, <strong>{{ $account->fullName }}</strong>. Вы зарегистрированы как "{{ $account->group->name }}".</p>
<p>
    Для входа в систему, перейдите по адресу:
    <a href="{{ $account->passwordResetLink() }}" target="_blank">
        {{ parse_url(url(), PHP_URL_HOST) }}
    </a>.
</p>
<p>Ваш логин: {{ $account->email }}</p>
<p>Это письмо отправлено роботом, отвечать на него не нужно.</p>
