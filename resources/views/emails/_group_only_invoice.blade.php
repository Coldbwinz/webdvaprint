Здравствуйте, <b>{{ $name }}</b><br/><br/>
Необходимо согласовать макет. Это можно сделать в личном кабинете <a href="http://{{ $_SERVER['SERVER_NAME'] }}/auth/login_token/{{$to}}/{{$login_token}}">ссылке</a><br/><br/>
Во вложении счёт на оплату заказа.
<br/>

@if (isset($manager))
Ваш персональный менеджер <b> @if ($manager->name) {{$manager->name }} @endif @if ($manager->lastname) {{ $manager->lastname }} @endif </b><br/>
@if ($manager->phone) {{ $manager->phone }}<br/> @endif
@endif

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>