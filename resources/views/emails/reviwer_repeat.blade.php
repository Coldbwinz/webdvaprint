Здравствуйте, <b>{{ $name }}</b><br/><br/>
Возможно, что-то пошло не так, и Вы не получили<br/>
на согласование макет, разработанный по Вашей заявке.<br/>
Для согласования перейдите по <a href="http://{{ $_SERVER['SERVER_NAME'] }}/auth/login_token/{{$to}}/{{$login_token}}/approve/{{$order_id}}">ссылке</a><br/>
<br>
Ваш персональный менеджер <b>{{ $manager->contact ? $manager->contact->name : $manager->name }} {{ $manager->contact ? $manager->contact->lastname : $manager->lastname }}</b><br>
@if ($phone = ($manager->contact ? $manager->contact->phone : $manager->phone))
    телефон {{ $phone }}<br/>
@endif
@if ($email = ($manager->contact ? $manager->contact->email : $manager->email))
    e-mail: {{ $email }}<br>
@endif

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>
