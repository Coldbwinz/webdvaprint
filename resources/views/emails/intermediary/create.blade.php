<p>Здравствуйте, {{ $intermediary->contact->fullName }}.</p>

<p>
    <strong>Логин:</strong> {{ $user->email }} <br>
    <strong>Пароль:</strong> {{ $password }}
</p>

<p>Ссылка для входа в личный кабинет: <a href="{{ $link }}" target="_blank">{{ $link }}</a></p>