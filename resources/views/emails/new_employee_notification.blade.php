<p>Здравствуйте, {{ $employee->contact->name }}.</p>

<p>Администратором сайта w2p.me для Вас был создан новый аккаунт.</p>

<p>Данные для входа:</p>

<ul>
    <li><strong>Логин:</strong> {{ $employee->email }}</li>
    <li><strong>Пароль:</strong> {{ $password }}</li>
</ul>

Ссылка для входа: <a href="{{ url('/auth/login') }}">{{ url('/login') }}</a>.

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>
