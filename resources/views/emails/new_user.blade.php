Здравствуйте, <b>' . {{ $name }}. '</b><br/><br/>
Благодарим Вас за регистрацию<br/>
Ваш логин - ' . {{ $to }} . '<br/>
Ваш пароль - ' . {{ $password }} . '<br/>
Для входа в систему перейдите по <a href="http://{{ $_SERVER['SERVER_NAME'] }}/auth/login_token/{{$to}}/{{$login_token}}">ссылке</a><br/><br/>
Ваш персональный менеджер <b> {{ $manager_name }} {{ $manager_lastname }} </b><br/>
@if ($manager_phone) {{ $manager_phone }}<br/> @endif

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>