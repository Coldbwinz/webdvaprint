Здравствуйте, <b>{{ $name }}</b><br/><br/>

Макет Вы можете загрузить по <a href="http://{{ $_SERVER['SERVER_NAME'] }}/loading/{{$url}}">ссылке</a>.<br/><br/>
<br/><br/>
Ваш персональный менеджер <b> {{$manager->name }} {{ $manager->lastname }} </b><br/>
@if ($manager->phone) {{ $manager->phone }}<br/> @endif

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>