Здравствуйте, <b>{{ $name }}</b><br/><br/>
Вы утвердили макет и оформили заказ.<br/>
Уведомляем Вас о его получении.<br/>
Счет на оплату в приложении.<br/>
<br/><br/>
Ваш персональный менеджер <b> {{ $manager->name }} {{ $manager->lastname }}</b><br/>
@if ($manager->phone) {{ $manager->phone }}<br/> @endif
