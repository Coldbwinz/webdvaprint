<b>{{ $name }}</b>, вы зарегистрированы в качестве менеджера<br/>
Для входа в систему, перейдите по ссылке <a href="http://{{ $_SERVER['SERVER_NAME'] }}/auth/login_token/{{$to}}/{{$login_token}}">ссылке</a><br/><br/>
Логин - {{ $to }}, временный пароль {{ $password }}<br/>
Сменить пароль Вы можете в настройках профиля<br/><br/>

Это письмо отправлено роботом, отвечать на него не нужно