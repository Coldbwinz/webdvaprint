Здравствуйте, <b>{{ $name }}</b><br/><br/>
По Вашей заявке, мы разработали макет<br/>
Для согласования перейдите по <a href="http://{{ $_SERVER['SERVER_NAME'] }}/user/reviwer/see/{{ $url }}">ссылке</a>.<br/>
<br>
Ваш персональный менеджер <b>{{ $manager->contact ? $manager->contact->name : $manager->name }} {{ $manager->contact ? $manager->contact->lastname : $manager->lastname }}</b><br>
@if ($phone = ($manager->contact ? $manager->contact->phone : $manager->phone))
    телефон {{ $phone }}<br/>
@endif
@if ($email = ($manager->contact ? $manager->contact->email : $manager->email))
    e-mail: {{ $email }}<br>
@endif

<i>Это письмо отправлено роботом. Отвечать на него не нужно.</i>
