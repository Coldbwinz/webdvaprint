Здравствуйте, <b>{{ $name }}</b><br/>
<br/>
По вашей заявке мы сформировали заказ № {{ $group_order->id }} <br>
<br>
@if ($agree_template_id)
    Согласовать макет можно по <a href="{{ url('auth/login_token/'.$to.'/'.$login_token) }}/approve/{{$agree_template_id}}">ссылке</a>. <br>
@endif
@if ($choise_template_id)
    Выбрать шаблон можно по <a href="{{ url('auth/login_token/'.$to.'/'.$login_token) }}/approve/{{$choise_template_id}}">ссылке</a>. <br>
@endif
@if ($printler_id)
    Загрузить макет можно по <a href="{{ url('auth/login_token/'.$to.'/'.$login_token) }}/approve/{{$printler_id}}">ссылке</a>. <br>
@endif
@if ($designer_id)
    Предоставить ТЗ дизайнеру можно по <a href="{{ url('auth/login_token/'.$to.'/'.$login_token) }}/approve/{{$designer_id}}">ссылке</a>. <br>
@endif
<br>
Ваш персональный менеджер {{ $manager_contacts->name }} {{ $manager_contacts->lastname }}<br>
@if ($manager_contacts->phone) телефон {{ $manager_contacts->phone }}<br/> @endif
@if ($manager_contacts->email) e-mail: {{ $manager_contacts->email }}<br>@endif
