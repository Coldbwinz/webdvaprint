@extends('app')

@section('content')
    <div id="root">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <button type="button" class="btn btn-lg"
                            style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                            data-toggle="collapse"
                            data-target="#collapsed_container">Добавить печатное оборудование
                    </button>

                    <div id="collapsed_container" class="collapse">
                        <div class="box-body" style="padding: 30px;">
                            <create-equipment-form></create-equipment-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Название</th>
                            <th>Тип печати</th>
                            <th>Макс. формат</th>
                            <th>Область печати</th>
                            <th>Скорость</th>
                            <th>Себестоимость</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($equipments as $equipment)
                            @foreach($equipment->printParameters as $index => $parameter)
                                <tr>
                                    <td>{{ $index === 0 ? $equipment->id : '' }}</td>
                                    <td>{{ $index === 0 ? $equipment->name : '' }}</td>
                                    <td>{{ $parameter->print_type }}</td>
                                    <td>{{ $parameter->maxPrintFormat }}</td>
                                    <td>{{ $parameter->printFormat }}</td>
                                    <td>{{ $parameter->properties->performance }} отп/мин.</td>
                                    <td>{{ $parameter->cost_price }} руб/клик</td>
                                    <td>
                                        @if ($index === 0)
                                            <a href="{{ route('dictionary.equipment.edit', $equipment) }}" class="btn btn-block btn-xs btn-success">
                                                Редактировать
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
