@extends('app')

@section('content')
    <div id="root">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <edit-material-form :material-id="{{ $material->id }}"></edit-material-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
