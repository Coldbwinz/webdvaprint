@extends('app')

@section('content')
    <div id="root">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <button type="button" class="btn btn-lg"
                            style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                            data-toggle="collapse"
                            data-target="#collapsed_container">Добавить листовой материал
                    </button>

                    <div id="collapsed_container" class="collapse">
                        <div class="box-body" style="padding: 30px;">
                            <create-material-form></create-material-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Наименование позиции</th>
                            <th>Формат (мм)</th>
                            <th>Цена (руб/лст)</th>
                            <th>Наличие (лст)</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($materials as $material)
                            <tr>
                                <td>{{ $material->id }}</td>
                                <td>{{ $material->title }}</td>
                                <td>{{ $material->format }}</td>
                                <td>{{ $material->sheet_price }}</td>
                                <td>{{ $material->stock_balance }}</td>
                                <td>
                                    <a href="{{ route('dictionary.material.edit', $material) }}"
                                       class="btn btn-success btn-block btn-xs"
                                    >
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
