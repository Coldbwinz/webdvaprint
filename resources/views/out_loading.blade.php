<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ settings('company_name') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <? if ($_SERVER['HTTP_HOST'] == 'dev.w2p.me') { ?>
                        <a href="http://web2print.pro/" class="navbar-brand">{{ settings('company_name') }}</a>
                    <? } else { ?>
                        <a href="#" class="navbar-brand">{{ settings('company_name') }}</a>
                    <? } ?>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">

                </div><!-- /.navbar-collapse -->

                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <div style="padding-top:5px; padding-bottom: 0px;color: #fff;">
                                <!-- The user image in the navbar-->
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                @if (isset($manager) && Auth::user()->isClient())
                                <p style="float: left;text-align: right;">
                                    Ваш персональный менеджер:&nbsp
                                    <b>@if ($manager->name) {{ $manager->name }} @if ($manager->lastname)  {{ $manager->lastname }} @endif
                                        &nbsp @endif</b>

                                    <br>

                                    @if ($manager->phone)<b> {{ $manager->phone }}</b>, &nbsp @endif

                                    @if ($manager->email) {{ $manager->email }} &nbsp @endif

                                </p>
                                @if ($manager->photo)
                                    &nbsp<img src="/upload/users/{{ $manager->photo }}" class="img-circle"
                                              alt="User Image" style="margin-left: 10px;float: right;width:40px;">
                                @endif
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="content-wrapper" style="background-color: #fff;">
        <div class="container">
            <section class="content-header" style="padding-bottom: 15px">


            @yield('content')

        </div><!-- /.container -->
    </div><!-- /.content-wrapper -->

</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script type="text/javascript">
    @if (isset($order_id))
        var $order_id = '<? echo $order_id?>';
    @endif

    $(document).ready(function () {
        $('.datepicker').datepicker({
            language: 'ru',
            startDate: '0d',
            format: 'dd.mm.yyyy',
            autoclose: true
        });
    });

    $(document).ready(function () {
        $("iframe").on("load", function () {

            $(this).contents().find(".change_page").click(function () {
                if ($order_id) {
                    location.href = '/user/reviwer/see/' + $order_id;
                }
            });

            $(this).contents().find(".show_block_div").click(function () {
                $('.bloc_div_overlay').show();
            });

        })
    });
</script>

@yield('scripts_bottom')

</body>
</html>
