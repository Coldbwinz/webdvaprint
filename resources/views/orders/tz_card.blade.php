@extends('app')

@section('content')
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <div>
                        <a href="{{ route('order-group.show', $order->group->id) }}">
                            Карточка заказа #{{ $order->group->id }}.{{ $order->position }}
                        </a>

                        <div class="pull-right" style="font-size: 12px; color: gray;">
                            Дата оформления: {{ $order->created_at->format('d.m.Y H:i') }}
                        </div>
                    </div>

                </h2>
            </div><!-- /.col -->
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    @if (! isset($from_queue))
                        <strong>Клиент</strong><br>
                        {{ $order->client_info->name }}<br>
                        @if ($order->client_info->company_name)
                            компания: {{ $order->client_info->company_name }}<br>
                        @endif
                        телефон: {{ $order->client_info->phone }}<br>
                        почта: <a href="mailto:{{ $order->client_info->email }}">{{ $order->client_info->email }}</a>
                    @endif

                    <br><br><strong>Менеджер</strong><br>
                    {{ $order->maneger_info->name }} {{ $order->maneger_info->lastname }}
                    - {{ $order->maneger_info->position }}
                </address>
            </div><!-- /.col -->

            <div class="col-sm-4 invoice-col">
                @if ($order->status == 10)
                    <p style="text-align: left">Макет еще не загружен</p>
                @else
                    @if ($order->img)
                        @if (file_exists('lib/frx/'.$order->img))
                            <img class="img-responsive" src="/lib/frx/{{ $order->img }}" alt="Превью"/>
                        @else
                            <i class="fa fa-refresh fa-spin"></i>
                        @endif
                    @endif
                @endif

            </div><!-- /.col -->

            <div class="col-sm-4 invoice-col">
                <div class="well well-sm">
                    <div>
                        <strong>Наименование:</strong>

                        <?php
                        if ($order->type_product != 999999) {
                            $productName = $order->productType->name;
                            $printlerType = $order->productType->printler_type;
                        } else {
                            $productName = $order->price_details['selected_product_type_name'];
                            $printlerType = $order->price_details['printler_type'];
                        }
                        ?>

                        {{ $productName . ' ' . $order->price_details['selected_size'] }}

                        @if ($printlerType == 3),
                        {{ $order->price_details['selected_product'] . ' стр.'}}@if ($order->price_details['selected_cover'] != 'Без обложки')
                            +обложка @else Без обложки @endif
                        @elseif ($printlerType == 1),
                        {{ $order->price_details['selected_chromacity'] }}
                        @elseif ($printlerType == 2),
                        ({{ $order->price_details['selected_type'] }})
                        @endif
                    </div>

                    <div><strong>Тираж:</strong> {{ $order->draw }} шт.</div>
                    <div>
                        <strong>Параметры:</strong>
                        @if ($order->price_details['printler_type'] == 3)
                            Вн. блок: <br> {{ $order->price_details['selected_material']}}
                            , {{ $order->price_details['selected_chromacity'] }} <br>
                            Обложка: <br> {{ $order->price_details['selected_cover'] }}

                            @if ($order->price_details['selected_type'] == 'на пужине')
                                <br>Подложка: <br> {{ $order->price_details['selected_pad'] }}
                            @endif
                        @else
                            {{ $order->price_details['selected_product'] }}, {{ $order->price_details['selected_chromacity'] }}
                        @endif
                    </div>

                    <div>
                        <strong>Постпресс:</strong>
                        @if ($order->price_details['printler_type'] == 3)
                            Переплёт {{ $order->price_details['selected_type'] }}, <br> @endif
                        @foreach ($order->price_details['selected_options'] as $i => $option)
                            {{ $option }}<br>
                        @endforeach
                        @if ($order->price_details['printler_type'] == 2) {{ $order->price_details['selected_type'] }},
                        <br> @endif
                    </div>

                    <div><strong>Дата выдачи:</strong>
                        @if (isset($order->close_date))
                            <span style="color: black">
                           <strong>{{ \Carbon\Carbon::createFromTimestampUTC($order->close_date)->format('d.m.Y') }}</strong>
                       </span>
                        @endif
                        @if (isset($speed_type)) {
                        @if (isset($order->close_date))
                            <br>
                        @endif
                        {{ $speed_type }}
                        @endif</div>
                </div>
            </div><!-- /.col -->
        </div>
        <!-- info row -->

        <br><br>
        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="width: 20%">Дата</th>
                        <th>Статус</th>
                        <th>Комментарий</th>
                        <th>Значение</th>
                        <th>Ответственный</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($logs as $value)
                        <tr>
                            <td>{{ $value->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $value->status_name }}</td>
                            <td>{!! $value->comments !!}</td>
                            <td>{!! $value->value !!}</td>
                            <td>
                            @if ($order->with_contractor && in_array($value->status_name, ['Принят в работу', 'Макет скачан']))
                                    {{ $order->contractor()->first()->contacts()->first()->name }} (Подрядчик)
                                @else
                                    @if ($value->user->contact)
                                        {{ $value->user->contact->getFullNameAttribute() }}
                                    @else
                                        {{ $value->user->getFullNameAttribute() }}
                                    @endif
                                    @if ($value->who)
                                        ({{ ($value->who->group->name == 'Пользователь') ? 'Заказчик' : $value->who->group->name }})
                                    @else
                                        менеджер не найден
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->

    </section>
@stop
