<style>
    .btn.btn-lg:after {
        font-family: "Glyphicons Halflings";
        content: "\e114";
        float: right;
        margin-left: 15px;
    }

    .btn.btn-lg.collapsed:after {
        content: "\e080";
    }

    .table-grey-grid {
        margin: 1%;
        width: 98%;
    }

    .table-grey-grid td, th {
        border: 1px solid #ddd !important;
    }

    .box-footer {
        border: 0px;
    }

    .nopadding {
        padding: 0;
    }

    .el-padding {
        width: 290px;
        margin-right: 20px;
    }

    input[type='number'] {
        -moz-appearance: textfield;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .dropdown-menu a {
        cursor: pointer;
    }

</style>
<script src="/js/price_functions.js"></script>
<script>
    @if (isset($step1))
    var product_type_names = <?php echo json_encode($product_type_names); ?>;
    var product_names = <?php echo json_encode($product_names); ?>;
    var product_options = <?php echo json_encode($product_options); ?>;
    var product_sizes = <?php echo json_encode($product_sizes); ?>;
    var product_materials = <?php echo json_encode($product_materials); ?>;

    var selected_product_type_name = '<?php echo $selected_product_type_name; ?>';
    var selected_product = '<?php echo $selected_product; ?>';
    var selected_type = '<?php echo $selected_type; ?>';
    var selected_material = '<?php echo $selected_material; ?>';
    var selected_cover = 'Без обложки';
    var selected_cover_chromacity = 'без печати';
    var selected_pad = 'Без подложки';
    var selected_pad_chromacity = 'без печати';
    var selected_options = JSON.parse('<?php echo json_encode($selected_options); ?>');
    var selected_options_prices = JSON.parse('<?php echo json_encode($selected_options_prices); ?>');
    var delivery_types = JSON.parse('<?php echo $delivery_types; ?>');
    var urgency = '<? echo $urgency?>';
    var price_count = '<?php echo $draw; ?>';
    var price = '<?php echo $price; ?>';
    var close_date = '<?php echo $close_date; ?>';
    var template_cost = 0;
    var complects_count = 1;
    var type_rate = 2;
    @endif
    var printler_type = '<?php echo $selected_printler_type; ?>';
    var selected_chromacity = 'Выбрать';
    var selected_delivery_type = <?php echo $selected_delivery_type; ?>;
    if (typeof(selected_delivery_type.name) == 'undefined') {
        selected_delivery_type = {
            name: delivery_types[0].name, id: delivery_types[0].id,
            cost: delivery_types[0].cost, days: delivery_types[0].days
        };
    }
    var payment_types = <?php echo $payment_types; ?>;
    var selected_payment_type = <?php echo $selected_payment_type ?>;
    var prepress_in_one_line = <?php echo $prepress_in_one_line ?>;
    if (typeof(selected_payment_type.id) == 'undefined') {
        selected_payment_type = payment_types[0];
    }
    var client_addresses = <?php echo $client_addresses; ?>;
    var issuing_addresses = <?php echo json_encode(settings('issuingAddresses')); ?>;
    var selected_address = '<?php echo $selected_address; ?>';
    var selected_prepress = '<?php echo $selected_prepress; ?>';
    var selected_size = '<?php echo $selected_size; ?>';
    var selected_type = '';
    var errorMessage = 'Не указаны: ';

    var standardSizes =
    {
        '50x90': null, '54x86': null, '90x90': null, '70x100': null,
        '98x210': null, '74x105': null, '105x148': null,
        '148x210': null, '210x297': null, '297x420': null
    };

    var template_chromacity = '<?php echo $template_chromacity ?>';
    var template_paper_clip_chromacity = '<?php echo $template_paper_clip_chromacity ?>';

    function setBodyChromacity() {
        if (printler_type == 3) {
            if (selected_type == 'на пружине') {
                $('.ddm-chromacities').html(template_chromacity);
            } else if (selected_type == 'на скрепке') {
                $('.ddm-chromacities').html(template_paper_clip_chromacity);
                if (($('.product-chromacity').html() == '4 + 0') || ($('.product-chromacity').html() == '1 + 0')) {
                    $('.product-chromacity').html('Выбрать');
                }
            }
        } else {
            $('.ddm-chromacities').html(template_chromacity);
            $('.product-chromacity').html('Выбрать');
        }
    }

    function getCalculationData() {
        var sendObj = {};
        sendObj.complects_count = $('#complects_count').val();
        sendObj.selected_address = $('#group_address').val();
        sendObj.price_index = 'manual';
        sendObj.real_price_index = selected_size + '_' + selected_type + '_' + selected_chromacity + '_' + selected_material;
        sendObj.printler_type = printler_type;
        sendObj.selected_product_type_name = selected_product_type_name;
        sendObj.selected_size = selected_size;
        sendObj.width = parseInt(selected_size.split('x')[0]);
        sendObj.height = parseInt(selected_size.split('x')[1]);
        sendObj.print_structure = make_print_structure(sendObj.width, sendObj.height);
        if (printler_type == 2) {
            (selected_type == 'один сгиб') ? sendObj.folds = 1 : sendObj.folds = 2;
        } else {
            sendObj.folds = 0;
        }

        if (printler_type != 3) {
            if (parseInt(selected_chromacity[4]) == 0) {
                sendObj.sides = 1
            } else if (parseInt(selected_chromacity[4]) > 0) {
                sendObj.sides = 2
            }
        } else if (printler_type == 3) {
            sendObj.sides = parseInt(selected_product);
            sendObj.selected_cover = selected_cover;
            sendObj.selected_cover_chromacity = $('.product-cover-chromacity').html();
            if (selected_type == 'на скрепке') {
                if (selected_cover != 'Без обложки') {
                    sendObj.sides += 4;
                }
            }
            if (selected_type == 'на пружине') {
                sendObj.selected_pad = selected_pad;
                sendObj.selected_pad_chromacity = $('.product-pad-chromacity').html();
                if (selected_cover != 'Без обложки') {
                    sendObj.sides += 2;
                }
                if (selected_pad != 'Без подложки') {
                    sendObj.sides += 2;
                }
            }
        }

        sendObj.selected_product = selected_product;
        sendObj.selected_type = selected_type;
        sendObj.selected_material = selected_material;
        sendObj.selected_chromacity = selected_chromacity;
        sendObj.selected_options = [];
        sendObj.selected_options_prices = [];
        $(".option-name").each(function (i, item) {
            if (item.checked) {
                sendObj.selected_options.push(item.value);
                sendObj.selected_options_prices.push($($(item).parent().next().children()).val());
            }
        });
        sendObj.selected_prepress = selected_prepress;
        sendObj.selected_delivery_type = selected_delivery_type;
        selected_payment_type.days_for_wait = 0;
        if (selected_payment_type.name == 'Отсрочка платежа') {
            if ($('#group_days_for_waiting_payment').val() > 0) {
                selected_payment_type.days_for_wait = $('#group_days_for_waiting_payment').val().trim();
            }
        }
        sendObj.selected_payment_type = selected_payment_type;
        sendObj.price = price;
        sendObj.template_cost = template_cost;
        sendObj.selected_urgency = urgency;
        sendObj.draw = $("#user_count").val();
        sendObj.close_date = $('#close_date').val();
        sendObj.start = $('input[name="start"]:checked').val();
        sendObj.prepress_in_one_line = ($('#inpPrepressInOneLine').length > 0) ? (($('#inpPrepressInOneLine:checked').length > 0) ? 0 : 1) : prepress_in_one_line;
        return sendObj;
    }

    function checkFields() {
        redCloseDate();
        errorMessage = 'Не указаны: ';
        if (checkField(printler_type)) {
            addErrorMessage(', тип продукции');
        }
        if (checkField(selected_product_type_name)) {
            addErrorMessage(', наименование продукта');
        }
        if (checkField(selected_size)) {
            addErrorMessage(', размер');
        }
        if (checkField(selected_product)) {
            addErrorMessage(', материал');
        }

        if (printler_type == 3) {
            if (checkField(selected_material)) {
                addErrorMessage(', материал');
            }
        }

        if (checkField(selected_chromacity)) {
            addErrorMessage(', цветность');
        }
        if (printler_type > 1) {
            if (checkField(selected_type)) {
                if (printler_type == 2) {
                    addErrorMessage(', кол-во сгибов');
                }
            }
        }
        if (printler_type == 3) {
            if (checkField(selected_type)) {
                addErrorMessage(', тип переплёта');
            }
        }

        if ($("#user_count").val() == 0) {
            addErrorMessage(', тираж');
        }
        if (selected_prepress == 0) {
            addErrorMessage(', препресс');
        }
        if ($('.datepicker').val() == 0) {
            addErrorMessage(', дата');
        }
        if ($("#price").val() == 0) {
            addErrorMessage(', итоговая стоимость');
        }
        if ($("#group_address").val() == 0) {
            addErrorMessage(', адрес');
        }

        if (errorMessage.length > 12) {
            errorMessage += '.';
            $('#errorMessage').html(errorMessage);
            return showError();
        }
        return true;
    }

    function addErrorMessage(message) {
        if (errorMessage == 'Не указаны: ') {
            errorMessage += message.substr(2);
        } else {
            errorMessage += message;
        }
    }

    function showError() {
        $('#alertPlsSelectCalculation').show();
        return false;
    }

    function checkField(field) {
        return ((field.length == 0) || (field == 'Выбрать'));
    }

    function calculateTotal() {
        price = parseFloat($('#price').val());
        template_cost = 0;
        complects_count = parseInt($('#complects_count').val());
        if (($('#price').val().length > 0) && ($('#template_cost').val().length > 0)) {
            $('#total_price').val(Math.round((price + template_cost) * 100) / 100 * complects_count);
        }
    }

    $(document).ready(function () {
        loadAddresses();
        if (selected_address.length > 0) {
            $('#group_address').val(selected_address);
        }

        if (selected_payment_type.name) {
            $('#group_payment_type').html(selected_payment_type.name);
            checkPaymentType();
        }

        @if (isset($step1)) calculateTotal(); @endif
        checkPrepress();

        $('#price, #template_cost').on('change input', function () {
            calculateTotal();
        });

        $.each($('.ddm-prepress a'), function (i, item) {
            if ($(item).attr('data-value') == selected_prepress) {
                $('#group_prepress').html($(item).html());
                return false;
            }
        });

        $('.ddm-printler-types').on('click', 'li a', function () {
            $('#divByTight').css('display','none');
            $('#row_prepress').show();
            $('#row_delivery_types').show();
            var new_printler_type = $(this).attr('data-value');
            if (new_printler_type != printler_type) {
                $('#printler_type_not_selected').show();
                printler_type = new_printler_type;
                $('.printler_type').text($(this).text());

                addParameters('.ddm-product-type-names', product_type_names[printler_type]);
                $.each(standardSizes, function (i) {
                    if (product_sizes[printler_type].indexOf(i) == -1) {
                        product_sizes[printler_type].push(i);
                    }
                });
                addParameters('.ddm-product-sizes', product_sizes[printler_type]);
                $('#additionalOptions').html('<p><strong>Дополнительные опции</strong></p>');
                $.each(product_options[printler_type], function (i, item) {
                    addOption(item, '');
                });

                if (printler_type == 1) {
                    $('#product_size_label').html('Обрезной размер (мм)');
                    selected_type = '###';
                    selected_material = '###';
                    $('#div_type').hide();
                    $('#div_material').hide();
                    $('#block_parameters2').hide();
                } else if (printler_type == 2) {
                    $('#product_size_label').html('Размер в развернутом виде (мм)');
                    $('.ddm-product-types').html(
                            '<li><a>один сгиб</a></li><li><a>два сгиба</a></li>');
                    $('.product-types-title').html('Кол-во сгибов');
                    $('#div_type').show();
                    $('#div_material').hide();
                    $('#block_parameters2').show();
                } else if (printler_type == 3) {
                    $('#product_size_label').html('Размер в сложенном виде (мм)');
                    $('.ddm-product-types').html(
                            '<li><a>на скрепке</a></li><li><a>на пружине</a></li>');
                    $('.product-types-title').html('Тип переплета');
                    $('#div_type').show();
                    $('#div_material').show();
                    $('#block_parameters2').show();
                }

                if (printler_type != 3) {
                    $('#div_chromacity').find('p').html('Цветность (кол-во сторон)');
                    $('#div_material').insertBefore($('#div_product_type_3'));
                    addParameters('.ddm-product-names', product_names[printler_type]);
                    $('#div_product').show();
                    $('#div_product_type_3').hide();
                    $('#div_covers').hide();
                } else if (printler_type == 3) {
                    $('#div_chromacity').find('p').html('Цветность');
                    $('#div_material').insertBefore($('#div_chromacity'));
                    $('#div_product').hide();
                    $('#div_product_type_3').show();
                    $('#div_covers').show();
                }
                setBodyChromacity();

                addProductMaterials();

                $('.product-name-pages').val(0);
                $('.product-type-name').val('Выбрать');
                $('#size_width').val('');
                $('#size_height').val('');
                $('.product-name').val('Выбрать');
                $('.product-type').html('Выбрать');
                $('.product-material').val('Выбрать');
                $('.product-cover-material').val('Без обложки');
                $('.product-pad-material').val('Без подложки');
                selected_chromacity = 'Выбрать';
                selected_cover_chromacity = 'без печати';
                selected_pad_chromacity = 'без печати';
                $('.product-chromacity').html(selected_chromacity);
                $('.product-cover-chromacity').html(selected_cover_chromacity);
                $('.product-pad-chromacity').html(selected_cover_chromacity);
                $('.product-size').show();
                $('#add_new_size').hide();
                calculateTotal();
            }
        });

        var needToFocus = [
            'product-type-name',
            'product-size',
            'product-name',
            'product-material',
            'product-cover-material',
            'product-pad-material'
        ];
        $('.dropdown-toggle').click(function () {
            var input = $(this).closest('div').children('input')[0];
            if (typeof(input) != 'undefined') {
                var inputClassList = input.classList;
                $.each(inputClassList, function (i, item) {
                    if (needToFocus.indexOf(item) != -1) {
                        input.focus();
                        return false;
                    }
                });
            }
        });

        $('.ddm-product-type-names').on('click', 'a', function () {
            selected_product_type_name = $(this).html().trim();
            $('.product-type-name').val(selected_product_type_name);
        });
        $('.product-type-name').focusout(function () {
            var val = $(this).val();
            if (val.length > 0) {
                selected_product_type_name = val;
            } else {
                $(this).val('Выбрать');
            }
        });
        $('.product-type-name').focusin(function () {
            if ($(this).val() == 'Выбрать') {
                $(this).val('');
            }
        });

        $('.ddm-product-sizes').on('mousedown', 'a', function () {
            $('.product-size').hide();
            selected_size = $(this).html().trim();
            $('#size_width').val(selected_size.split('x')[0]);
            $('#size_height').val(selected_size.split('x')[1]);
            $('#add_new_size').show();
            $('#size_width').focus();
        });

        $('#product_size').click(function () {
            $('.product-size').val('');
            $('.product-size').focus();
        });

        $('#size_width, #size_height').focusout(function () {
            if (($('#size_width').val() > 0) && ($('#size_height').val() > 0)) {
                selected_size = $('#size_width').val() + 'x' + $('#size_height').val();
                checkPrepress();
            }
        });

        $('.ddm-product-names').on('click', 'a', function () {
            selected_product = $(this).html().trim();
            $('.product-name').val(selected_product);
        });

        $('.product-size').keypress(function () {
            $('.product-size').val('');
            $('.product-size').hide();
            $('#add_new_size').show();
            $('#size_width').focus();
        });

        $('.product-size').focusout(function () {
            if (($('#size_width').val() == 0) || ($('#size_height').val() == 0)) {
                $('.product-size').val('Выбрать');
            }
        });

        $('.product-name').focusout(function () {
            var val = $(this).val();
            if (val.length > 0) {
                selected_product = val;
            } else {
                $(this).val('Выбрать');
            }
        });
        $('.product-name').focusin(function () {
            if ($(this).val() == 'Выбрать') {
                $(this).val('');
            }
        });

        $('.ddm-product-types').on('click', 'a', function () {
            if ($(this).html().trim() != selected_type) {
                selected_type = $(this).html().trim();
                selected_chromacity = 'Выбрать';
                selected_cover_chromacity = 'без печати';
                selected_pad_chromacity = 'без печати';
                $('.product-chromacity').html(selected_chromacity);
                $('.product-cover-chromacity').html(selected_cover_chromacity);
                $('.product-pad-chromacity').html(selected_cover_chromacity);
                if (selected_type == 'на скрепке') {
                    $('#divByTight').css('display','none');
                    type_rate = 4;
                    $('#divPad').hide();
                    checkAndSetCorrectNumForSelectedType($('.product-name-pages').val());
                } else if (selected_type == 'на пружине') {
                    $('#divByTight').css('display','inline-block');
                    type_rate = 2;
                    $('#divPad').show();
                    checkAndSetCorrectNumForSelectedType($('.product-name-pages').val());
                }
                $('.product-type').html(selected_type);
                setBodyChromacity();
                calculateTotal();
                checkPrepress();
            }
        });

        $('.ddm-same-pages').on('click', 'a', function () {
            $('#spanSamePages').html($(this).html().trim());
        });

        $('.product-name-pages').focusout(function (e) {
            checkAndSetCorrectNumForSelectedType($(this).val());
            calculateTotal();
        });

        function checkAndSetCorrectNumForSelectedType(typed_value) {
            var add = 0;
            if ((typed_value % type_rate) > 0) {
                add = 1;
            }
            var value = (parseInt(typed_value / type_rate) + add) * type_rate;
            if (value < 8) {
                value = 8;
            }
            selected_product = value;
            $('.product-name-pages').val(selected_product);
        }

        $('.ddm-product-materials').on('click', 'a', function () {
            selected_material = $(this).html().trim();
            $('.product-material').val(selected_material);
        });
        $('.product-material').focusout(function () {
            var val = $(this).val();
            if (val.length > 0) {
                selected_material = $(this).val();
                addProductMaterials(selected_material);
            } else {
                $(this).val('Выбрать');
            }
        });
        $('.product-material').focusin(function () {
            if ($(this).val() == 'Выбрать') {
                $(this).val('');
            }
        });

        $('.ddm-chromacities').on('click', 'a', function () {
            selected_chromacity = $(this).html().trim();
            $('.product-chromacity').html(selected_chromacity);
            calculateTotal();
            checkPrepress();
        });

        //---------------------------------------------------------------
        $('#div_cover_material_block, #div_cover_chromacity_block, #div_pad_material_block, #div_pad_chromacity_block').click(function () {
            $(this).css('border', '#D2D6DE');
        });

        $('.ddm-product-cover-materials').on('click', 'a', function () {
            selected_cover = $(this).html().trim();
            $('.product-cover-material').val(selected_cover);
            if (selected_cover == 'Без обложки') {
                selected_cover_chromacity = 'без печати'
                $('.product-cover-chromacity').html(selected_cover_chromacity);
            }
            calculateTotal();
        });

        $('.ddm-product-pad-materials').on('click', 'a', function () {
            selected_pad = $(this).html().trim();
            $('.product-pad-material').val(selected_pad);
            if (selected_pad == 'Без подложки') {
                selected_pad_chromacity = 'без печати';
                $('.product-pad-chromacity').html(selected_pad_chromacity);
            }
            calculateTotal();
        });

        $('.product-cover-material').focusout(function () {
            var val = $(this).val();
            if (val.length > 0) {
                selected_cover = val;
                addProductMaterials(selected_cover);
            } else {
                $(this).val('Без обложки');
                selected_cover_chromacity = 'без печати';
                $('.product-cover-chromacity').html();
            }
            calculateTotal();
        });

        $('.product-pad-material').focusout(function () {
            var val = $(this).val();
            if (val.length > 0) {
                selected_pad = val;
                addProductMaterials(selected_pad);
                calculateTotal();
            } else {
                $(this).val('Без подложки');
                selected_pad_chromacity = 'без печати';
                $('.product-pad-chromacity').html(selected_pad_chromacity);
            }
        });

        $('.product-cover-material').focusin(function () {
            if ($(this).val() == 'Без обложки') {
                $(this).val('');
            }
        });

        $('.product-pad-material').focusin(function () {
            if ($(this).val() == 'Без подложки') {
                $(this).val('');
            }
        });

        $('.ddm-cover-chromacities').on('click', 'a', function () {
            selected_cover_chromacity = $(this).html().trim();
            if ($('.product-cover-material').val() == 'Без обложки') {
                $('.product-cover-chromacity').html(selected_cover_chromacity);
                $('.product-cover-material').val('Выбрать');
                $('#div_cover_material_block').css('border', not_correct);
            } else {
                $('.product-cover-chromacity').html(selected_cover_chromacity);
            }
            calculateTotal();
        });

        $('.ddm-pad-chromacities').on('click', 'a', function () {
            selected_pad_chromacity = $(this).html().trim();
            if ($('.product-pad-material').val() == 'Без подложки') {
                $('.product-pad-chromacity').html(selected_pad_chromacity);
                $('.product-pad-material').val('Выбрать');
                $('#div_pad_material_block').css('border', not_correct);
            } else {
                $('.product-pad-chromacity').html(selected_pad_chromacity);
            }
            calculateTotal();
        });
        //------------------------------------------------------------------

        $('.ddm-delivery_types').on('click', 'a', function () {
            selected_delivery_type = {
                name: $(this).html(), id: $(this).attr('data-id'),
                cost: $(this).attr('data-cost'), days: $(this).attr('data-days')
            };
            $('#group_delivery_type').html($(this).html());
            loadAddresses();
        });

        $('.ddm-payment_types').on('click', 'a', function () {
            selected_payment_type = payment_types[$(this).attr('data-id')];
            $('#group_payment_type').html($(this).html());
            checkPaymentType();
        });

        $('#close_date').datepicker({
            language: 'ru',
            startDate: '0d',
            format: 'dd.mm.yyyy',
        });

        $('#group_urgency').click(function (e) {
            $('#close_date').css('border-color', '#D2D6DE');
        });

        $('#table, #user_count, #total_price, #close_date, #group_prepress').click(function (e) {
            $('#alertPlsSelectCalculation').hide();
        });

        $('#user_count').on("input", function () {
            if (/^0/.test(this.value)) {
                this.value = this.value.replace(/^0/, "")
            }
        });

        $('#complects_count').on('input', function(e) {
            if (complects_count != $('#complects_count').val()) {
                complects_count = $('#complects_count').val();
                setDivLabels()
            }
        });

        $('#group_prepress, #prepress_caret_button').click(function (e) {
            $('#group_prepress').css('color', '');
            $('#group_prepress').css('border', '');
        });

        $('#add_option').click(function () {
            $('#addOptionBlock').toggle();
        });

        $('#btnAddOption').click(function () {
            addOption($('#addNameInput').val(), $('#addPriceInput').val());
            return false;
        });
    });

    function addProductMaterials(newValue) {
        if ((printler_type == 3) && (newValue != 'Без обложки') &&
                (newValue != 'Без подложки') && (newValue != 'Выбрать')) {
            if (newValue) {
                if ($.inArray(newValue, product_materials[printler_type]) == -1) {
                    product_materials[printler_type].push(newValue);
                }
            }
        }
        addParameters('.ddm-product-materials', product_materials[printler_type]);
        addParameters('.ddm-product-cover-materials', product_materials[printler_type]);
        addParameters('.ddm-product-pad-materials', product_materials[printler_type]);
    }

    function addParameters(ddm, arr) {
        if (arr) {
            var html = '';
            if (ddm == '.ddm-product-cover-materials') {
                html += '<li><a>Без обложки</a></li>';
            }
            if (ddm == '.ddm-product-pad-materials') {
                html += '<li><a>Без подложки</a></li>';
            }
            $.each(arr, function (i, item) {
                html += '<li><a>' + item + '</a></li>';
            });
            $(ddm).html(html);
        }
    }


    function addOption(name, price) {
        $('#additionalOptions').append(
                '<div class="container nopadding" style="margin-top: 5px; display: inline-block;">' +
                '<div style="width:150px; display: inline-block;"><input class="option-name" type="checkbox" value="' +
                name + '" style="vertical-align:top;"/>&nbsp&nbsp' + name + '</div></div>');
    }

    function redCloseDate() {
        if ($('#group_urgency').html() == 'Выбрать') {
            $('#group_urgency').css('border-color', 'red');
            return false;
        }
    }

</script>

<div id="calculation_table" class="box box-warning" style="@if (!isset($reviwer)) display:none @endif">
    <div clas="box" style="border-top: none;">
        <button id="btnCalculation" type="button" class="btn btn-lg collapsed"
                style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                data-toggle="collapse"
                data-target="#collapsed_container" @if (isset($reviwer)) disabled @endif>
            @if (!isset($reviwer))
                Выбор параметров заказа
            @else
                Изменение параметров заказа невозможно
            @endif
        </button>
        <div id="collapsed_container" @if (isset($reviwer)) class="collapse" @endif>
            <div class="box-body">
                <div class="errorMessagealert alert alert-error" id="alertPlsSelectCalculation" style="display:none;">
            <span>
                <p id="errorMessage" style="margin-bottom: 0px;">Пожалуйста, уточните параметры заказа и дату готовности заказа!</p>
            </span>
                </div>
                <row class="container col-md-12 nopadding">
                    <div class="col-md-3 nopadding" style="width:600px; margin-bottom:20px;">
                        <p style="font-size: 12px; margin-bottom: 2px;">Тип продукции</p>
                        <div class="input-group dropdown">
                            <div class="form-control dropdown-toggle printler_type"
                                 data-toggle="dropdown">Тип изделия
                            </div>
                            <ul class="ddm-printler-types dropdown-menu">
                                <li><a data-value="1">Листовое изделие</a></li>
                                <li><a data-value="2">Изделие со сгибом</a></li>
                                <li><a data-value="3">Многостраничное изделие</a></li>
                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span></span>
                        </div>
                    </div>
                </row>
                <div id="printler_type_not_selected"
                     style="display: none;  height:100%; margin-top: 15px; margin-bottom: 15px;">
                    <row class="container col-md-12 nopadding">
                        <div class="col-md-3 nopadding el-padding">
                            <p style="font-size: 12px; margin-bottom: 2px;">Наименование</p>
                            <div class="input-group dropdown">
                                <input type="text" class="form-control dropdown-toggle product-type-name"
                                       style="font-size: 16px; text-align: left;"
                                       data-toggle="dropdown" value="Выбрать"/>
                                <ul class="ddm-product-type-names dropdown-menu">
                                </ul>
                                <span role="button" class="input-group-addon dropdown-toggle caret"
                                      style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span></span>
                            </div>
                        </div>
                        <div class="col-md-3 nopadding el-padding">
                            <p id="product_size_label" style="font-size: 12px; margin-bottom: 2px;"></p>
                            <div id="product_size" class="input-group dropdown">
                                <input type="text" class="form-control dropdown-toggle product-size"
                                       style="font-size: 16px; text-align: left;"
                                       data-toggle="dropdown" value="Выбрать"/>
                                <div id="add_new_size" class="col-xs-12 no-padding"
                                     style="display:none; border: 1px solid #d2d6de; height:34px;">
                                    <input id="size_width"
                                           style="border:none; display: inline-block; width:42%; height:32px; font-size: 16px;"
                                           type="number" class="form-control pull-left" value=""/>
                                        <span style="cursor:default; display: inline-block; width:16%; height:32px; text-align:center; padding-top:2px; font-size:20px; border-left: 1px solid #d2d6de; border-right: 1px solid #d2d6de;"
                                              class="pull-left">X</span>
                                    <input id="size_height"
                                           style="border: none; display: inline-block; width:42%; height:32px; font-size: 16px;"
                                           type="number" class="form-control" value=""/>
                                </div>
                                <ul class="ddm-product-sizes dropdown-menu">
                                </ul>
                                <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span></span>
                            </div>
                        </div>
                    </row>
                    <row id="block_parameters2" class="container col-md-12 nopadding"
                         style="display:none; margin-top: 5px; margin-bottom: 5px;">
                        <div id="div_type" class="col-md-3 nopadding el-padding">
                            <p class="product-types-title" style="display:inline-block; font-size: 12px; margin-bottom: 2px;"></p>
                            <div id="divByTight" style="display:none; "><input id="inpByTight" type="checkbox" style="margin-top:0; vertical-align:middle;"><font style="font-size: 12px;"> по узкой стороне</font></input></div>
                            <div class="input-group dropdown">
                                <div class="form-control dropdown-toggle product-type"
                                     data-toggle="dropdown">Выбрать
                                </div>
                                <ul class="ddm-product-types dropdown-menu"></ul>
                                <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span></span>
                            </div>
                        </div>
                        <div id="div_material" class="col-md-3 nopadding el-padding">
                            <p style="font-size: 12px; margin-bottom: 2px;">Материал внутреннего блока</p>
                            <div class="input-group dropdown">
                                <input type="text" class="form-control dropdown-toggle product-material"
                                       style="font-size: 16px; text-align: left;"
                                       data-toggle="dropdown" value="Выбрать"/>
                                <ul class="ddm-product-materials dropdown-menu"></ul>
                                <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span></span>
                            </div>
                        </div>
                        <div id="div_product_type_3" class="col-md-3 nopadding el-padding" style="display: none;">
                            <p style="display:inline-block; font-size: 12px; margin-bottom: 2px;">Кол-во</p>
                            <div class="input-group" style="width:100%;">
                                <input class="form-control product-name-pages" type="number" value=""/>
                                <div id="divSamePages" class="input-group-addon nopadding" style="border-top:none; border-bottom:none;">
                                    <span role="button" class="input-group-addon dropdown-toggle" style="height:34px; border-left: none;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span id="spanSamePages">разных стр.&nbsp&nbsp</span><span class="caret"></span></span>
                                    <ul class="ddm-same-pages dropdown-menu" style="margin-left:130px;">
                                        <li><a>разных стр.&nbsp&nbsp</a></li>
                                        <li><a>одинаковых стр.&nbsp&nbsp</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </row>
                    <row id="block_parameters1" class="container col-md-12 nopadding"
                         style="margin-top: 5px; margin-bottom: 5px;">
                        <div id="div_product" class="col-md-3 nopadding el-padding">
                            <p style="font-size: 12px; margin-bottom: 2px;">Материал</p>
                            <div class="input-group dropdown">
                                <input type="text" class="form-control dropdown-toggle product-name"
                                       style="font-size: 16px; text-align: left;"
                                       data-toggle="dropdown" value="Выбрать"/>
                                <ul class="ddm-product-names dropdown-menu"></ul>
                                <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                        </div>
                        <div id="div_chromacity" class="col-md-3 nopadding el-padding">
                            <p style="font-size: 12px; margin-bottom: 2px;">Цветность</p>
                            <div class="input-group dropdown">
                                <div class="form-control dropdown-toggle product-chromacity"
                                     data-toggle="dropdown">Выбрать
                                </div>
                                <ul class="ddm-chromacities dropdown-menu"></ul>
                                <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                      data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span></span>
                            </div>
                        </div>
                    </row>
                    <div id="div_covers" style="display: none;">
                        <row class="col-md-12 nopadding" style="margin-top: 5px; margin-bottom: 5px;">
                            <div class="col-md-3 nopadding el-padding">
                                <p style="font-size: 12px; margin-bottom: 2px;">Материал обложки</p>
                                <div id="div_cover_material_block" class="input-group dropdown">
                                    <input type="text" class="form-control dropdown-toggle product-cover-material"
                                           style="font-size: 16px; text-align: left;"
                                           data-toggle="dropdown" value="Без обложки"/>
                                    <ul class="ddm-product-cover-materials dropdown-menu"></ul>
                                    <span role="button" class="input-group-addon dropdown-toggle"
                                          style="border-left: none;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span></span>
                                </div>
                            </div>
                            <div class="col-md-3 nopadding el-padding">
                                <p style="font-size: 12px; margin-bottom: 2px;">Цветность обложки</p>
                                <div id="div_cover_chromacity_block" class="input-group dropdown">
                                    <div class="form-control dropdown-toggle product-cover-chromacity"
                                         data-toggle="dropdown">Выбрать
                                    </div>
                                    <ul class="ddm-cover-chromacities  dropdown-menu">
                                        @include('templates.chromacity')
                                    </ul>
                                    <span role="button" class="input-group-addon dropdown-toggle"
                                          style="border-left: none;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span></span>
                                </div>
                            </div>
                        </row>
                        <div id="divPad" style="display: none;">
                            <row class="col-md-12 nopadding" style="margin-top: 5px; margin-bottom: 5px;">
                                <div class="col-md-3 nopadding el-padding">
                                    <p style="font-size: 12px; margin-bottom: 2px;">Материал подложки</p>
                                    <div id="div_pad_material_block" class="input-group dropdown">
                                        <input type="text" class="form-control dropdown-toggle product-pad-material"
                                               style="font-size: 16px; text-align: left;"
                                               data-toggle="dropdown" value="Без подложки"/>
                                        <ul class="ddm-product-pad-materials dropdown-menu">
                                        </ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="border-left: none;"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-3 nopadding el-padding">
                                    <p style="font-size: 12px; margin-bottom: 2px;">Цветность подложки</p>
                                    <div id="div_pad_chromacity_block" class="input-group dropdown">
                                        <div class="form-control dropdown-toggle product-pad-chromacity"
                                             data-toggle="dropdown">Выбрать
                                        </div>
                                        <ul class="ddm-pad-chromacities dropdown-menu">
                                            @include('templates.chromacity')
                                        </ul>
                            <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none;"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                                    </div>
                                </div>
                            </row>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 nopadding" style="margin-top: 34px; margin-bottom: 5px;">
                    <div class="container col-md-6 nopadding" style="width:350px; display: inline-block;">
                        @if (isset($step1))
                            <div style="margin-bottom:5px;">
                                <div class="container nopadding"  style="display:inline-block; width: 225px; margin-top: 5px; vertical-align: bottom;">
                                    <div class="input-group nopadding">
                                         <span class="input-group-addon">
                                            Тираж
                                        </span>
                                        <input id="user_count" class="form-control" type="number" value=""/>
                                         <span class="input-group-addon">
                                            шт.
                                        </span>
                                    </div>
                                </div>
                                <div class="container" style="width: 122px; display: inline-block; padding-left:10px; padding-right:0;">
                                    <p style="font-size: 12px; display: inline-block; margin-bottom: 5px;">Кол-во комплектов</p>
                                    <input id="complects_count" type="number"
                                           class="form-control" value="1">
                                </div>
                            </div>
                        @endif
                        <row id="row_prepress" class="container col-md-12 nopadding"
                             style="@if (isset($step1)) margin-top:5px; @endif margin-bottom: 5px; display:none;">
                            <div style="display:inline-block;">
                                <label><input id="inpPrepressInOneLine" type="checkbox" style="vertical-align:top;" checked/>
                                    <font style="font-size: 12px; font-weight: normal;"> выводить отдельной строкой</font></label>
                            </div>
                            <div class="input-group dropdown">
                                <span class="input-group-addon">
                                    Макет
                                </span>
                                <button id="group_prepress" class="btn btn-default btn-block dropdown-toggle"
                                        type="button"
                                        style="border-radius: 0; border: 1px solid {{$colorSecondary}}; color: {{$colorPrimary}}"
                                        data-toggle="dropdown">Выбрать
                                </button>
                                <ul class="ddm-prepress dropdown-menu" style="width:260px; margin-left:65px;">
                                    <li><a data-value="1">Создать макет по шаблону</a></li>
                                    <li><a data-value="2">Проверить макет заказчика</a></li>
                                    <li><a data-value="3">Сформировать ТЗ дизайнеру</a></li>
                                    <li><a data-value="4">Работа вместе с дизайнером</a></li>
                                    <li><a data-value="5">Использовать макет из архива</a></li>
                                </ul>
                                <span id="prepress_caret_button" role="button" class="input-group-addon dropdown-toggle"
                                      style="background-color: #DDDDDD; height:14px !important;" data-toggle="dropdown"
                                      aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </span>
                            </div>
                            <div id="divWorkStart" style="display:none; border:1px solid #d2d6de; border-top:none;">
                                <div style="padding-left: 15px;">
                                    <label style="font-weight: normal;"><input type="radio" name="start" value="0"
                                                                               checked>
                                        после оформления заказа</label>
                                    <label style="font-weight: normal;"><input type="radio" name="start" value="1">
                                        по нажатию Продолжить</label>
                                </div>
                            </div>
                        </row>
                        <div class="container nopadding" style="width:350px; margin-top:10px; margin-bottom:5px;">
                            <div class="container nopadding" style="display: inline-block; width: 225px; vertical-align:bottom;">
                                <div class="input-group nopadding">
                                    <span class="input-group-addon">
                                        <i class="fa fa-shopping-cart"></i>
                                    </span>
                                    <button id="group_delivery_type" class="btn btn-default btn-block dropdown-toggle"
                                            type="button" style="border-radius: 0;"
                                            data-toggle="dropdown">{{ $delivery_types[0]->name }}
                                    </button>
                                    <ul class="ddm-delivery_types dropdown-menu" style="width:100%;">
                                        @foreach($delivery_types as $item)
                                            <li><a data-id="{{ $item->id }}" data-cost="{{ $item->cost }}"
                                                   data-days="{{ $item->days }}">{{ $item->name }}</a></li>
                                        @endforeach
                                    </ul>
                                    <span role="button"
                                          class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD; height:14px !important;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="container" style="width: 122px; display: inline-block; padding-left:10px; padding-right:0;">
                                <p style="font-size: 12px; margin-top: 2px; margin-bottom: 5px;">Дата выдачи</p>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input id="close_date" type="text" class="form-control datepicker"
                                           value="{{$close_date}}">
                                </div>
                            </div>
                            <div id="divChoiseIssuingPoint" class="nopadding el-padding" style="width:100%; border:1px solid #d2d6de; border-top:none; border-bottom:none;">
                                <div class="input-group dropdown">
                                    <input id="group_address" type="text" class="form-control dropdown-toggle"
                                           style="font-size: 16px; text-align: left; border-left:none;"
                                           data-toggle="dropdown" placeholder="Выбрать адрес доставки"
                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Выбрать адрес доставки'"/>
                                    <ul class="ddm-addresses dropdown-menu"></ul>
                            <span role="button" class="input-group-addon dropdown-toggle" style="border-left: none; border-right:none; background-color: #DDDDDD;"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"
                                  onclick="$('#group_address').prop('placeholder','').focus();">
                                <span class="caret"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="container col-md-12 nopadding" style="width:350px; margin-top:10px;">
                            <div class="container nopadding" style="width: 350px; vertical-align:bottom;">
                                <div class="input-group nopadding">
                                    <span class="input-group-addon">
                                        <i class="fa fa-money"></i>
                                    </span>
                                    <button id="group_payment_type" class="btn btn-default btn-block dropdown-toggle"
                                            type="button" style="border-radius: 0;"
                                            data-toggle="dropdown">{{ $payment_types[0]->name }}
                                    </button>
                                    <ul class="ddm-payment_types dropdown-menu" style="width:100%;">
                                        <? $payment_types_index = 0; ?>
                                        @foreach($payment_types as $payment_type)
                                            <li><a data-id="{{ $payment_types_index }}" data-discount="{{ $payment_type->discount }}">{{ $payment_type->name }}</a></li>
                                            <? $payment_types_index++; ?>
                                        @endforeach
                                    </ul>
                                    <span role="button"
                                          class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD; height:14px !important;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id="divDaysForWaitingPayment" class="col-md-12 nopadding el-padding" style="display:none; width:350px; border:1px solid #d2d6de; border-top:none; border-bottom:none;">
                            <div class="input-group">
                                <input id="group_days_for_waiting_payment" type="number" class="form-control dropdown-toggle"
                                       style="font-size: 16px; text-align: left; border-left:none; border-top:none;"
                                       value="0"/>
                                <span class="input-group-addon" style="border-top:none; border-right:none;">дней после выдачи</span>
                            </div>
                        </div>
                        <div class="col-md-12 nopadding" style="margin-top: 15px;">
                            <div class="input-group">
                                <span class="input-group-addon">Итого</span>
                                <input id="price" class="form-control" type="number" value="{{ $price }}"/>
                                <span class="input-group-addon" style="padding: 6px 6px;">руб.</span>
                            </div>
                        </div>
                        <div style="display: none;">
                            <div class="col-md-12 nopadding" style="margin-top: 10px;">
                                <div class="input-group">
                                    <input id="template_cost" class="form-control" placeholder="Укажите стоимость"
                                           type="number" value=""/>
                                    <span class="input-group-addon" style="padding: 6px 6px;">руб.</span>
                                </div>
                            </div>
                            <div class="col-md-12 nopadding" style="margin-top: 10px;">
                                <div class="input-group">
                                    <span class="input-group-addon">Итого</span>
                                    <input id="total_price" class="form-control" type="number" disabled
                                           value=""/>
                                    <span class="input-group-addon" style="padding: 6px 6px;">руб.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container nopadding" style="width: 330px; margin-left: 20px; display: inline-block;">
                        <div id="additionalOptions">
                            <p><strong>Дополнительные опции</strong></p>
                        </div>
                        <row class="col-md-12 nopadding" style="margin-top: 5px;">
                            <a id="add_option" style="cursor: pointer; text-decoration: underline;">+Добавить опцию</a>
                        </row>
                        <row id="addOptionBlock" class="col-md-12 nopadding" style="margin-top: 20px; display: none;">
                            <div class="container nopadding" style="width:600px; display: inline-block;">
                                <div style="width:160px; display: inline-block;">
                                    <input id="addNameInput" type="text" value="" style="width: 100%;"/>
                                    <p style="font-size: 12px; font-style: italic; display:inline-block;">Введите
                                        название опции</p>
                                </div>
                                <div style="width:80px; margin-left:15px; display: inline-block; vertical-align: top;">
                                    <button id="btnAddOption" style="height: 26px !important;"
                                            class="btn btn-info btn-xs">Добавить
                                    </button>
                                </div>
                            </div>
                        </row>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <row>
                    <div class="form-group col-md-8" style="padding-left: 0; margin-left: 1%;">
                        <button id="btnNext" class="btn btn-info" style="background-color: #2B78E4">Продолжить</button>
                    </div>
                </row>
            </div>
        </div>
    </div>
</div>
