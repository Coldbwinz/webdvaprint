@foreach($clients as $client)
    <tr>
        {{-- ID --}}
        <td>{{ $client->id }}</td>

        {{-- Менеджер --}}
        <td>
            @if ($client->manager && $client->manager->contact)
                {{ $client->manager->contact->fullName }}
            @endif
        </td>

        {{-- Клиент --}}
        <td>
            @if ($client->isCompany())
                <span title="$client->tm_name">
                    {{ $client->tm_name ? str_limit($client->tm_name, 20) : 'Без названия' }}
                </span>
            @else
                Частное лицо
            @endif

            <br>

            @if ($client->type)
                <span class="label label-default">
                    {{ $client->type->name }}
                </span>
            @endif
        </td>

        {{-- Контактные лица клиента --}}
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <a href="{{ url('orders/new-step1/' . $client->id . '?contact_id=' . $contact->id) }}"
                       class="contact-link">
                        {{ $contact->full_name }}
                    </a>
                    <div></div>
                @endforeach
            @endif
        </td>

        {{-- Телефон --}}
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <div><a class="contact-phone">{{ $contact->phone }}</a></div>
                @endforeach
            @endif
        </td>

        {{-- Email --}}
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <div>{{ $contact->email }}</div>
                @endforeach
            @endif
        </td>

        {{-- Всего заказов --}}
        <td>
            @if ($client->hasGroupOrders())
                {{ $client->groupOrders->count() }} на сумму {{ $client->groupOrders->sum('price') }}
            @endif
        </td>
        <td>
            @if ($client->hasGroupOrders())
                @if ($client->groupOrdersActive)
                    @if ($client->groupOrdersActive->count() > 0)
                        {{ $client->groupOrdersActive->count() }} на
                        сумму {{ $client->groupOrdersActive->sum('price') }}
                    @else
                        Последний заказ {{ $client->orders->last()->created_at->format('d.m.Y H:i') }}
                    @endif
                @endif
            @endif
        </td>
        <td class="text-center">
            @if ($client->hasGroupOrders())
                <?php
                $total = $client->groupOrders->sum('price');
                $payed = $client->groupOrders->sum('pay');
                ?>

                @if ($total === $payed)
                    <span class="label label-success">
                        <i class="fa fa-check-circle"></i>
                    </span>
                @endif

                @if ($total < $payed)
                    <span class="label label-danger" title="Долг">
                        + {{ abs($total - $payed) }}
                    </span>
                @endif

                @if ($total > $payed)
                    <span class="label label-warning" title="Переплата">
                        - {{ abs($total - $payed) }}
                    </span>
                @endif
            @endif
        </td>
    </tr>
@endforeach