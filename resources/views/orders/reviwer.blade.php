@extends('app')

@section('content')
    <script>
        var client_id = '<?php echo $company_id; ?>';
        if (client_id.length == 0) {
            client_id = prices.client_id;
        }

        var new_invoice = '<? echo $new_invoice; ?>';
        var price_index = '<? echo $price_index; ?>';
        var pressedButton = false;
        var global_response = '';
        var templatesCount = 0;
        var indexForApprove = 0;
    </script>

    <script>
        var approveHandler = function () {
            pressedButton = true;
            var orderId = '{{ $order->id }}';
            var params = {
                id: orderId,
                status: 2,
                agree: 1
            };
            if (price_index != 'manual') {
                params.fill_calculation = JSON.stringify(getCalculationData());
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                },
                url: '/order/post_status',
                data: params,
                type: 'post',
                success: function (response) {
                    global_response = JSON.parse(response);
                    pressedButton = false;
                    if ((global_response.client_for_close == -1) && (global_response.count_not_closed_orders == 0) &&
                        (company_details > 0)) {
                        $('#modalGetCompanyData').show();
                    } else if (global_response.client_for_close > 0) {
                        $('#modalContinueOrder').show();
                    } else {
                        @if ($user->id_group == 1)
                            window.location.href = '/orders/need_attention';
                        @else
                            window.location.href = '/orders/history';
                        @endif
                    }
                },
                failed: function (data) {
                    pressedButton = false;
                    console.log("ajax error");
                }
            });
        };

        $(document).ready(function () {
            var iframe = document.querySelector('iframe');
            $('#btnAgree').click( function () {
                if ($('.preview-box').length) {
                    $('.preview-box').trigger('approve');
                } else {
                    if ((price_index == 'manual') || checkFields()) {
                        if (((printler_type < 3) && ($.inArray(selected_chromacity, ['1 + 0', '4 + 0']) > -1))) {
                            approveHandler();
                        } else {
                            if ($(this).data('editable') == true) {
                                iframe.contentWindow.postMessage("approve_next_page", "*");
                            }
                        }
                    } else {
                        if ($('#collapsed_container').css('display') == 'none') {
                            $('#btnCalculation').click();
                            $('#alertPlsSelectCalculation').show();
                        }
                        return false;
                    }
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#btnChange').click(function () {
                if ($(this).data('editable') == true) {
                    if (!pressedButton) {
                        pressedButton = true;
                        window.location.href = "/orders/edit/{{ $order->id }}@if ($order->replicated){{ '/1' }}@endif";
                    }
                } else {
                    $('#nonEditable').modal('show');
                }
            });

            $("#btnSave").click(function () {
                if (!pressedButton) {
                    if (checkFields()) {
                        pressedButton = true;
                        $('#new_invoice').val(new_invoice);
                        $('#order_id').val({{ $order->id }});
                        $('#fill_calculation').val(JSON.stringify(getCalculationData()));
                        $('#btn_save_price').click();
                    } else {
                        return false;
                    }
                }
            });

            $("#btnCalculation, #btnSave").click(function () {
                $('#alertPlsSelectCalculation').hide();
            });
        });
    </script>

    <script>
        window.addEventListener('message', function (e) {
            if (e.data === 'approved_successfully') {
                btnAgree.style.display = 'none';
                approveHandler();
            }
        });
    </script>

    @include("orders.modals.get_company_data_form")
    @include("orders.continue_order_for_group")

    <section class="invoice">
        <form style="display: none;" method="post" action="/order/reviwer/calculation" id="form_save_price"
              enctype="multipart/form-data" autocomplete="off">
            <input type="text" name="_token" value="{{ csrf_token() }}">
            <input class="form-control" type="text" id="new_invoice" name="new_invoice" value=""/>
            <input class="form-control" type="text" id="order_id" name="order_id" value=""/>
            <input class="form-control" type="text" id="fill_calculation" name="fill_calculation" value=""/>
            <button id="btn_save_price" type="submit" class="btn btn-primary">Добавить</button>
        </form>
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <div class="/*pull-left*/" style="font-size:15px; width: 100%">

                    Формат <b>{{ $order->price_details['selected_size'] }};</b>
                    цветность: <b>{{ $order->price_details['selected_chromacity'] }};</b>
                    материал: <b><span id="material_reviwer">{{ $order->price_details['selected_product'] }}</span>;</b>

                    <span id="draw_reviwer">
                    @if ($order->draw)
                        тираж: <b>{{ $order->draw }}</b> шт.;
                    @endif
                    </span>

                    <span id="options_reviwer">
                    @if ($order->price_details['selected_options'])
                        доп. опции:
                        @foreach ($order->price_details['selected_options'] as $option)
                            <b>{{ $option }};</b>
                        @endforeach
                    @endif
                    </span>

                    @if ($price_index != 'manual')
                        <span id="urgency_reviwer">
                        @if ($order->price_details['selected_urgency'])
                            срочность: <b>{{ $order->price_details['selected_urgency'] }}</b>;
                        @endif
                        </span>
                    @endif

                    <span id="close_date_reviwer">
                    @if ($order->price_details['close_date'])
                            дата выдачи: <b>{{ $order->price_details['close_date'] }}</b>;
                        @endif
                    </span>

                    <span id="price_reviwer">
                        стоимость: <b>{{ $order->price }}</b> руб.
                    </span>

                </div>
                @if ($price_index != 'manual')
                    @include('orders.priceForm_new')
                @else
                    @include('orders.priceForm_manual')
                @endif
                <div class="col-md-12 no-padding" style="display: table; margin-bottom: 5px;">
                    <div class="no-padding" style="display: table-cell; vertical-align: top;">
                        <span style="font-size: 15px; margin-left: 0px; margin-right: 0;">
                            При согласовании макета <b>ВНИМАТЕЛЬНО ПРОВЕРЯЙТЕ ИНФОРМАЦИЮ</b> на наличие <b>ОШИБОК</b> и
                            <b>ОПЕЧАТОК</b>.<br>
                            Найденная в готовом изделии ошибка, в случае, если макет утвержден, не является основанием
                            для претензий.
                        </span>
                    </div>
                    <div class="no-padding" style="display: table-cell; width: 240px; ">
                        <button id="btnAgree" data-editable="{{ $order->file ? 'true' : 'false' }}" class="btn btn-success pull-right">
                            Утвердить
                        </button>
                        @if (! Auth::user()->isClient())
                            @if ($order->status == 2)
                                <? if (!isset($_GET['sand'])) { ?>
                                <a href="/order/reviwer/send/{{ $order->id }}">
                                    <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i
                                                class="fa fa-download"></i> Отправить клиенту
                                    </button>
                                </a>
                                <? } ?>1
                            @endif
                        @endif

                        <button id="btnChange" style="border-color: #000;background-color: #fff;color: #000;margin-right: 5px;" data-editable="{{ $order->file ? 'true' : 'false' }}"
                                class="btn btn-success pull-right">Изменить макет
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row no-print">
            <div class="col-xs-12" style="text-align: center;">
                @if ($order->file)
                <!--		<img src="/lib/frx/{{ $order->img }}" /><br/><br/>-->
                <iframe src="/lib/see/index.html?uuid={{ $order->file }}" width="100%"
                        style="width: 100%;height: 86vh;border: 0px;">
                    Ваш браузер не поддерживает плавающие фреймы!
                </iframe>
                @else
                    <?php
                        $previews = [];
                        if ($order->readyProducts->count()) {
                            $previews = $order->readyProducts->map(function ($product) {
                                return $product->preview;
                            });
                        } elseif ($order->clientTemplate) {
                            $previews = $order->clientTemplate->previews;
                        }
                        if (!isset($previews)) {
                            $previews = [];
                        }
                    ?>

                    <div class="preview-box">
                        <div class="image-wrapper">
                            <img class="image"/>
                        </div>

                        <div class="previews">
                            @foreach($previews as $preview)
                                <div class="preview-item">
                                    <img src="{{ $preview }}" alt="Preview" class="img-thumbnail">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <script>
                        $(document).ready(function () {
                            var $previewBox = $('.preview-box'),
                                $previews = $('.previews').find('.preview-item'),
                                $image = $('.preview-box').find('.image'),
                                $firstElement = $previews.first();

                            $firstElement.addClass('current');
                            $image.attr('src', $firstElement.find('img').attr('src'));

                            $previewBox.on('click', '.preview-item', function () {
                                var $element = $(this);
                                $element.addClass('current').siblings().removeClass('current');
                                $image.attr('src', $element.find('img').attr('src'));
                            });

                            $previewBox.on('approve', function () {
                                var $currentElement = $(this).find('.preview-item.current');
                                $currentElement.removeClass('current').addClass('approved');

                                var $unapproved = $previews.not('.approved').first();

                                if ($unapproved.length) {
                                    $unapproved.addClass('current');
                                    $image.attr('src', $unapproved.find('img').attr('src'));
                                } else {
                                    $('#btnAgree').prop('disabled', true).html(' <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
                                    approveHandler();
                                }
                            });
                        });
                    </script>
                @endif
            </div>
        </div>
    </section>

    @include('orders.partials.nonEditableModal')
    <script>
        $('#printAsIsButton').click(function () {
            $('#nonEditable').modal('hide');
            $('#btnAgree').click();
        });

        $('#makeTemplate').click(function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                },
                url: "/specification/for-order/{{ $order->id }}",
                type: 'get',
                success: function (response) {
                    $('#nonEditable').modal('hide');
                    if (response == 1) {
                        $('#modalContinueOrder').show();
                    } else {
                        window.location.href = '/orders/history';
                    }
                },
                failed: function (data) {
                    console.log("ajax error");
                }
            });
        });
    </script>
@stop
