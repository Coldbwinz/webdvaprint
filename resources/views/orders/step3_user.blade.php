@extends('app')

@section('content')

  <div class="row">
   <div class="col-xs-12">
	<div class="box" style="border-top: 0px;background: none;">
            <iframe src="/lib/editor_user/index.html?uuid={{ $uuid }}.frx" width="100%" style="width: 100%;height: 86vh;border: 0px;"> 
	      Ваш браузер не поддерживает плавающие фреймы!
	    </iframe>
        <!--
            @if ($template)
                <div><strong>FRX:</strong> <span>{{ $template->w2p }}</span></div>
                <div><strong>PSD:</strong> <span>{{ $template->psd }}</span></div>
            @endif
        -->
            <div class="overlay bloc_div_overlay" style="display: none"><i class="fa fa-refresh fa-spin"></i></div>
        </div>
  </div>
</section><!-- /.content -->
@stop