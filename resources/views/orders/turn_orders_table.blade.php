<style>
    .max-preview-img-size {
        max-height: 50px;
        max-width: 100px;
    }
</style>
<script>
    var canPress = true;
    var canPressArrows = true;
    $(document).ready(function () {
        $('#table_turn_orders').on('click', '.btnChangeStatus', function () {
            if (canPress) {
                canPress = false;
                var closest_tr = $(this).closest('tr');
                var statusBlock = closest_tr.children()[7];
                var buttonBlock = closest_tr.children()[8];
                var order_id = $(this).closest('tr').attr('data-order-id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "GET",
                    url: '/order/set_status_id/' + order_id + '/' + $(buttonBlock).find('button')[0].getAttribute('data-status'),
                    success: function (result) {
                        result = JSON.parse(result);
                        if ((result.status == 45) || (result.status == 51)) {
                            window.location.href = '/orders/turn_orders';
                            return false;
                        }
                        if (["30","35","40"].indexOf(result.status) != -1) {
                            button_html = '<button style="margin-bottom: 5px" class="btn btn-block btn-info btn-xs btnChangeStatus" data-status="';
                            if (result.status == 30) {
                                button_html += '35">Отправить в печать</button>';
                            } else if (result.status == 35) {
                                button_html += '40">Отпечатан</button>';
                            } else if (result.status == 40) {
                                button_html += '45">Готов</button>';
                            }
                            button_html +=
                                    '<a href="#" data-order-id="' + order_id + '" class="btnStop">' +
                                    '<button style="margin-top: 5px;" class="btn btn-block btn-danger btn-xs" >Приостановить</button ></a>';
                        }
                        var needFirst;
                        if ($(statusBlock).find('span')[0].innerText == 'Запущен на переделку') {
                            needFirst = 1;
                        } else {
                            needFirst = 0;
                        }
                        while ($(statusBlock).find('span').length > needFirst) {
                            $(statusBlock).find('span:last').remove();
                        }
                        $(statusBlock).append('<span class="label label-warning" style="background-color: #' + result.status_color + '!important;">' + result.status_name + '</span>');
                        $(buttonBlock).html(button_html);
                        canPress = true;
                    },
                    error: function (xhr, status, error) {
                        canPress = true;
                    }
                });
            }
        });

        $('#table_turn_orders').on('click', '.btnStop', function () {
            $order_id = $(this).attr('data-order-id');
            $('.stopped_form').hide();
            $('#stopped_state_' + $order_id).val(1000);
            $('.stopped_form_' + $order_id).slideDown();
        });

        $('#table_turn_orders').on('click', '.btnRestart', function () {
            $order_id = $(this).attr('data-order-id');
            $('.stopped_form').hide();
            $('#stopped_state_' + $order_id).val(0);
            $('.stopped_form_' + $order_id).slideDown();
        });

        $('#table_turn_orders').on('click', '.btnCancel', function () {
            $order_id = $(this).attr('data-order-id');
            $('.stopped_form').hide();
            $('#stopped_state_' + $order_id).val(1010);
            $('.stopped_form_' + $order_id).slideDown();
        });

        $('.fa-angle-up').click(function () {
            sendResortPosition(this, -1);
        });

        $('.fa-angle-down').click(function () {
            sendResortPosition(this, 1);
        });

        function sendResortPosition(object, delta) {
            var order_id = $(object).closest('tr').attr('data-order-id');
            window.location.href = '/order/change_position/' + order_id + '/' + delta;
            /*if (canPressArrows) {
             var closest_tr = $(object).closest('tr');
             var order_id = closest_tr.children()[0].childNodes[0].text;
             $.ajax({
             headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             method: "GET",
             url: '/order/change_position/' + order_id + '/' + delta,
             success: function (result) {
             canPressArrows = true;
             },
             error: function (xhr, status, error) {
             canPressArrows = true;
             }
             });
             }*/
        }
    })
</script>

<table id="table_turn_orders" class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Наименование</th>
        <th>Тираж</th>
        <th>Параметры</th>
        <th>Постпресс</th>
        <th style="text-align: center;">Превью</th>
        <th>Дата выдачи</th>
        <th>Статус</th>
        <th style="text-align: center;">Действия</th>
        <th></th>
    </tr>
    @foreach ($turn_orders as $value)
        <tr data-order-id="{{ $value->id }}">
            <td>
                <a href="{{ route('order.show', $value->id) }}">
                    {{ $value->group_id }}.{{ $value->position }}
                </a>
            </td>

            <td>{{ $value->product_name }} {{ $value->selected_size }}
                @if ($value->printler_type == 3)
                    ,<br>

                    {{ $value->price_details['selected_product'] . ' стр.'}}

                    @if ($value->price_details['selected_cover'] != 'Без обложки')
                        +обложка
                    @else
                        Без обложки
                    @endif
                @elseif ($value->printler_type == 1),
                <br>{{ $value->sides_text }}
                @elseif ($value->printler_type == 2),
                <br>({{ $value->price_details['selected_type'] }})
                @endif</td>
            <td>{{ $value->draw }} шт.</td>
            <td>@if ($value->printler_type == 3)
                    Вн. блок: <br> {{ $value->price_details['selected_material']}}
                    , {{ $value->sides_text }} <br>
                    Обложка: <br> {{ $value->price_details['selected_cover'] }}

                    @if ($value->price_details['selected_type'] == 'на пужине')
                        <br>Подложка: <br> {{ $value->price_details['selected_pad'] }}
                    @endif
                @else
                    {{ $value->price_details['selected_product'] }}, {{ $value->selected_chromacity }}
                @endif
            </td>
            <td>
                @if ($value->printler_type == 3) Переплёт {{ $value->price_details['selected_type'] }}, <br> @endif
                    @foreach ($value->price_details['selected_options'] as $i => $option)
                        {{ $option }}, <br>
                    @endforeach
                @if ($value->printler_type == 2) {{ $value->price_details['selected_type'] }}, <br> @endif
                Порезка в размер
            </td>

            <td class="text-center">
                @if (strpos($value->img, 'http') !== false)
                    <img class="max-preview-img-size" src="{{ $value->img }}" alt="Превью"/>
                @else
                    <img class="max-preview-img-size" src="/lib/frx/{{ $value->img }}" alt="Превью"/>
                @endif
            </td>

            <td>
                @if (isset($value->close_date))
                    <span style="color: black"><strong>{{ date('d.m.Y', strtotime($value->close_date)) }}</strong><br>{{ date('h:i', strtotime($value->close_date)) }}</span>
                @endif
                @if (isset($speed_type)) {
                @if (isset($value->close_date))
                    <br>
                @endif
                {{ $speed_type }}
                @endif
            </td>

            <td>
                @if ($value->sort_order > 55)
                    <span class="label" style="background-color: {{"#".$value->order_status_rework_color }} !important;">{{ $value->order_status_rework_name }}</span>
                    <br>
                @endif
                @if ($value->reworked)
                    <span class="label label-info">Переделан</span>
                    <br>
                @endif
                @if ($value->status_last > 0)
                    <span class="label" style="background-color: {{"#".$value->order_status_last_color }} !important;">{{ $value->order_status_last_name }}</span>
                    <br>
                @endif
                @if (!(($value->sort_order == 56) && ($value->status == 25)))
                    <span class="label" style="background-color: {{"#".$value->order_status_color }} !important;">{{ $value->order_status_name }}</span>
                @endif
            </td>

            <td>
                @if ($value->status == 25)
                    @if ($value->pdf || $value->file)
                        <?php
                            if ($value->pdf && (is_null($value->designer_working))) {
                                $download_file = '/lib/frx/'.$value->pdf;
                            } elseif ($value->pdf && (!is_null($value->designer_working))) {
                                $download_file = $value->pdf;
                            } elseif ($value->file) {
                                $download_file = '/lib/frx/'.$value->file;
                            }
                        ?>
                        <a href="{{ $download_file }}" download="{{ $download_file }}">
                            <button style="margin-bottom: 5px"
                                    class="btn btn-block btn-info btn-xs btnChangeStatus" data-status="30"
                            >Скачать</button>
                        </a>
                    @else
                        <a href="#">
                            <button style="margin-bottom: 5px" class="btn btn-block btn-info btn-xs" onclick="alert('Заблокировано.')"
                            >Скачать</button>
                        </a>
                    @endif
                @elseif ($value->status == 30)
                    <button style="margin-bottom: 5px"
                            class="btn btn-block btn-info btn-xs btnChangeStatus" data-status="35">
                        Отправить в печать
                    </button>
                @elseif ($value->status == 35)
                    <button style="margin-bottom: 5px"
                            class="btn btn-block btn-info btn-xs btnChangeStatus" data-status="40">
                        Отпечатан
                    </button>
                @elseif ($value->status == 40)
                    <button style="margin-bottom: 5px"
                            class="btn btn-block btn-info btn-xs btnChangeStatus" data-status="45">
                        Готов
                    </button>
                @endif


                @if (in_array($value->status, [25,30,35,40]))
                    <a href="#" data-order-id="{{ $value->id }}" class="btnStop">
                        <button style="margin-top: 5px;" class="btn btn-block btn-danger btn-xs">
                            Приостановить
                        </button>
                    </a>
                @endif
                @if ($value->status == 1000)
                    @if ($value->stopped_user_id && ($value->stopped_user_id == auth()->user()->id))
                        <a href="#" data-order-id="{{ $value->id }}" class="btnRestart">
                            <button style="margin-bottom: 5px;" class="btn btn-block btn-success btn-xs">
                                Возобновить
                            </button>
                        </a>
                        <a href="#" data-order-id="{{ $value->id }}" class="btnCancel">
                            <button style="margin-bottom: 5px" class="btn btn-block btn-info btn-xs">
                                Отменить
                            </button>
                        </a>
                    @endif
                @endif
            </td>
            <td>
                @if (($value->main_sort_order == 0) && ($value->sort_order == 0))
                    <span class="fa fa-angle-up" style="cursor: pointer;"></span>
                    <span class="fa fa-angle-down" style="cursor: pointer;"></span>
                @endif
            </td>
        </tr>
        @if (in_array($value->status, [2, 12, 25, 30, 35, 40, 56, 102, 112, 1000, 1010]))
                <tr class="stopped_form_{{ $value->id }} stopped_form" style="display: none;">
                    <td colspan="10">
                        <div class="panel box box-success with-border">
                            <div class="panel-collapse collapse in">
                                <div class="box-header">
                                    <h3 class="box-title">Комментарий</h3>
                                </div>
                                <div class="box-body">
                                    <form method="POST" action="/order/stopped/{{ $value->id }}/0"
                                          onsubmit="return validate_form_reason ( this );"
                                          enctype="multipart/form-data" autocomplete="off">
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="stopped_reason">
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">
                                                        Подтвердить
                                                    </button>

                                                    <button type="button"
                                                            class="btn btn-default"
                                                            data-action="hide-form"
                                                            data-target=".stopped_form_{{ $value->id }}"
                                                    >
                                                        Отмена
                                                    </button>
                                                </div>
                                                <input type="hidden" class="form-control"
                                                       id="stopped_state_{{ $value->id }}"
                                                       name="stopped_state_{{ $value->id }}"
                                                       value=""
                                                       placeholder="">

                                                <input type="hidden"
                                                       name="stopped_from"
                                                       value="{{ \App\Orders::STOPPED_FROM_QUEUE }}"
                                                >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
        @endif
    @endforeach
</table>
<script>

    var canPress = true;
    function validate_form_reason(obj) {
        if (obj[1].value.length > 0) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Пожалуйста, заполните поле Комментарий");
        }
        return false;
    }

    function checkCanPress() {
        if (canPress) {
            canPress = false;
            return true;
        }
        return false;
    }
</script>
{!! (new Landish\Pagination\Pagination($turn_orders))->render() !!}
