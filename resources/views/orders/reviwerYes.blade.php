@extends('out')

@section('content')
<style>
    .poup {
        background-color: #fff;
        width: 400px;
        padding: 20px;
        position: absolute;
        z-index: 15;
        margin-left: -200px;
        left: 50%;
        border: 3px solid red;
        cursor: pointer;
        box-shadow: -2px 2px 4px 0 grey;
    }
</style>

    <div class="poup">
        <p><a href="/{{ $pdf_link }}" style="text-decoration: underline">Скачать предварительный счёт</a></p>
        <p style="font-weight: bold;margin-bottom: 0px;" onclick="$('.poup').hide();">Закрыть окно</p>
    </div>

    @include('orders.template_ok_login_or_register')
@stop