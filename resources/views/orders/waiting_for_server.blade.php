<style>
    #waitingForServerContent  {
        -webkit-border-radius: 25px !important;
        -moz-border-radius: 25px !important;
        border-radius: 25px !important;
    }
</style>
<script>
    function startLoadingAnimation() {
        $('#modalWaitingServer').show();
    }

    function stopLoadingAnimation() {
        $("#modalWaitingServer").hide();
    }
</script>

<div id="modalWaitingServer" class="modal">
    <div class="modal-dialog" style="margin-top: 25%;">
        <div class="modal-content">
            <div class="modal-header" style="display: inline-block;">
                <img id="loadImg" src="/images/ajax-loader.gif" style="margin-left:15px; margin-top:0; display: inline-block;"/>
                <h4 class="modal-title" style="display: inline-block; padding-left: 15px; line-height:0px;">Ожидаем ответ сервера...</h4>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>