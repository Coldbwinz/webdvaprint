@extends('app')

@section('content')
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <div>
                        Карточка заказа #{{ $groupOrder->id }} ({{ $groupOrder->orders->count() }})
                        <small class="pull-right">Дата оформления: {{ $groupOrder->created_at->format('d.m.Y H:i') }}</small>
                    </div>
                </h2>
            </div>
        </div>

        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    @if ($groupOrder->manager && $groupOrder->manager->contact)
                        <strong>Менеджер</strong><br>
                        {{ $groupOrder->manager->contact->fullName }}
                        - {{ $groupOrder->manager->contact->getInformationFieldForClient($groupOrder->client->id, 'position') }}
                    @endif

                    <h4>Клиент</h4>
                    {{ $groupOrder->client->contacts->first()->fullName }}<br>
                    @if ($groupOrder->client->company_name)
                        компания: {{ $groupOrder->client->company_name }}<br>
                    @endif
                    телефон: {{ $groupOrder->client->contacts->first()->phone }}<br>
                    почта: <a href="mailto:{{ $groupOrder->client->contacts->first()->email }}">
                        {{ $groupOrder->client->contacts->first()->email }}
                    </a>
                </address>
            </div>

            <div class="col-sm-8 invoice-col">
                <h4>Позиции заказа</h4>
                <table class="table table-stripped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Продукт</th>
                            <th>Статус</th>
                            <th>Цена</th>
                            <th>Превью</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($groupOrder->orders as $order)
                            <tr>
                                <td>
                                    <a href="{{ url('order/tz/' . $order->id) }}">
                                        {{ $order->group_id }}.{{ $order->position }}
                                    </a>
                                </td>
                                <td>
                                    @if ($order->productType)
                                        {{ $order->productType->name }}
                                    @else
                                        Произвольный заказ
                                    @endif

                                    <br>{{ $order->draw }} шт.
                                </td>
                                <td>
                                    <span class="label label-default" style="background-color: {{"#".$order->statusInfo->color }}; color: white;">
                                        {{ $order->statusInfo->name }}
                                    </span>
                                </td>
                                <td><strong>{{ $order->price }}</strong> <i class="fa fa-rub"></i></td>
                                <td>
                                    @if ($order->status == 10)
                                        <p style="text-align: left">Макет еще не загружен</p>
                                    @else
                                        @if ($order->img)
                                            @if (file_exists('lib/frx/'.$order->img))
                                                <img style="text-align: left;" class="max-preview-img-size"
                                                     src="/lib/frx/{{ $order->img }}" alt="Превью" height="50"/>
                                            @else
                                                <i class="fa fa-refresh fa-spin"></i>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 20%">Дата</th>
                            <th>Статус</th>
                            <th>Комментарий</th>
                            <th>Значение</th>
                            <th>Ответственный</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($groupOrder->logs as $item)
                            <tr>
                                <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                                <td>
                                    @if ($item->status == 1)
                                        @if ($item->comments != 'Выдан акт')
                                            <a href="<?php echo e($groupOrder->getInvoiceLink()); ?>" target="_blank">
                                                <i class="fa fa-download"></i> Выставлен счет
                                            </a>
                                        @else
                                            Выданы закрывающие документы
                                        @endif
                                    @elseif ($item->status)
                                        {{ $item->statusInfo->name }}
                                    @endif
                                </td>
                                <td>{!! $item->comments !!}</td>
                                <td>{!! $item->value !!}</td>
                                <td>
                                    {{ $item->user && $item->user->contact ? $item->user->contact->fullName : $item->user->getFullNameAttribute() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
