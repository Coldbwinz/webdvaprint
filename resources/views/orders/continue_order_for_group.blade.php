<script>
    var continueButtonPressed = false;
    var company_details = '<?php echo $company_details ?>';
    var reviwer = '<? if (isset($reviwer)) echo $reviwer; ?>';
    $(document).ready(function () {
        $('#btnInvoice').on("click", function () {
            if (!continueButtonPressed) {
                continueButtonPressed = true;
                if (('{{ $company_details }}' > 0) && (reviwer == 1) && (global_response.count_not_closed_orders == 0)) {
                    $('#modalContinueOrder').hide();
                    $('#modalGetCompanyData').show();
                } else {
                    window.location.href = '/order/close_group_order/{{ $company_id }}';
                }
            }
        });

        $('#btnNextGroupItem').on("click", function () {
            if (!continueButtonPressed) {
                continueButtonPressed = true;
                window.location.href = '/orders/new-step1/{{ $company_id }}';
            }
        });
    });
</script>

<div id="modalContinueOrder" class="modal">
    <div class="modal-dialog" style="margin-top: 25%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Сформировать заказ или продолжить оформление?</h4>
            </div>
            <div class="user-message modal-body" style="display:none">Работу с макетами вы сможете произвести после полного оформления заказа</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="width: 30%; " id="btnInvoice">Сформировать заказ</button>
                <button type="button" class="btn btn-primary" style="width: 35%; margin-right: 18%;" id="btnNextGroupItem">Продолжить оформление</button>
            </div>
        </div>
    </div>
</div>
