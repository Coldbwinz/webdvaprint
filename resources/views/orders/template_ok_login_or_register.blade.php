<div class="col-md-8 col-md-offset-2">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p style="text-align: center;font-size: 20px;">{{ $client->name }}, спасибо за Ваш заказ! <br>
        @if ($need_account)
            Все макеты будут доступны Вам в личном кабинете
        @else
            Все макеты хранятся в Вашем личном кабинете
        @endif
    </p>
    <br>
    @if ($need_account)
        <div style="border: none; -webkit-box-shadow: none;">
            <button type="button" class="btn btn-lg btn-primary btn-block collapsed"
                    data-toggle="collapse"
                    data-target="#collapsed_container" style="width:40%; margin-left: 30%;">Создать личный кабинет
            </button>
            <div id="collapsed_container" @if (count($errors) == 0) class="collapse" @endif style="width:60%; margin-left: 20%;">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert-danger text-center">{!! $error !!}</div>
                    @endforeach
                @endif
                <form method="POST" action="/user/reviwer/status/{{$client->id}}"
                      style="border: none; -webkit-box-shadow: none;">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Пароль </label>
                            <input class="pull-right" type="password" name="password" />
                        </div>
                        <div class="form-group">
                            <label>Подтверждение пароля </label>
                            <input class="pull-right" type="password" name="password_confirmation" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="manager_id" value="{{ $manager->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="manager_name" value="{{ $manager->name }}" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="manager_lastname" value="{{ $manager->lastname }}" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="manager_email" value="{{ $manager->email }}" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="manager_phone" value="{{ $manager->phone }}" />
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary center-block" type="submit">Создать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @else
        <a href="/auth/logout/{{$client->email}}"><button type="button" class="btn btn-lg btn-primary btn-block"
                                                          style="width:40%; margin-left: 30%;">Войти в личный кабинет
            </button></a>
    @endif
</div>