@extends('app')

@section('content')
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>

    <div class="row">
        <div class="col-md-7">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg collapsed"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse"
                        data-target="#collapsed_container">Добавить нового клиента
                </button>
                <div id="collapsed_container" @if (Request::input('phone') === null) class="collapse" @endif>
                    @include("contractors.clients.new_client_form")
                </div>
            </div>
        </div>
    </div>

    @if ($errors->has())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

    <style>
        th, td {
        / / text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>


    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="height: 40px;">
                    <div class="box-tools" style="width: 98%;">
                        <form id="seach_form" method="get">
                            <div class="input-group" style="width: 100%;">
                                <input type="text"
                                       name="search"
                                       class="form-control input-sm pull-right"
                                       placeholder="Поиск"
                                       value="{{ Request::input('search') }}"
                                />

                                <div class="input-group-btn">
                                    <button id="btnSearch" class="btn btn-sm btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover client_table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Менеджер</th>
                                <th>Клиент</th>
                                <th>Контактное лицо</th>
                                <th>Телефон</th>
                                <th>E-mail</th>
                                <th>Всего заказов</th>
                                <th>В работе</th>
                                <th>Балланс</th>
                            </tr>
                        </thead>

                        <tbody>
                            @include('orders.new_order_client_table')
                        </tbody>
                    </table>
                </div>
            </div>

            @if ($clients->hasPages())
                <div class="box-footer">
                    {!! $clients->render() !!}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var count_timout = 0;
        $(document).ready(function () {
            $('input[name = "search"]').keyup(function () {
                count_timout++;
                setTimeout(function() {
                    count_timout--;
                    if (count_timout == 0) {
                        filterActivate();
                    }
                }, 500);
            });

            $("#btnSearch").click(function () {
                filterActivate();
            });

            function filterActivate() {
                var frm = $('#seach_form');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: frm.serialize(),
                    method: "POST",
                    url: "/orders/new",
                    error: function (xhr, status, error) {
                        $('.future_field_msg').css('color', 'red').text("Error! Contact admin ...");
                    },
                    success: function (data) {
                        $('.client_table tbody').html(data);
                    }
                });
            }
        });
    </script>
@endsection

