@extends('app')

@section('content')
    @if (Auth::user()->accessLevel() == 1)
        @include("admin.user.dashboard_panel")
    @endif

    <style>
        .div_center {
            cursor: pointer;
            display: flex;
        / / width: 249 px;
        / / height: 150 px;
        }

        .div_center > img {
            max-width: 100%;
            max-height: 150px;
            margin: auto;
        }
    </style>
    <script>
        var type_id;
        var client_id = '<?php echo $company_id; ?>';
        if (client_id.length == 0) {
            client_id = prices.client_id;
        }

        var printler = null;
        $(document).ready(function () {
            var sended = false;
            var accessLevel = '<?php echo Auth::user()->accessLevel() ?>';
            $('#invoice_info').on('click', '#btnNext', function () {
                var start = $('input[name="start"]:checked').val();
                if (checkFields()) {
                    if ((selected_prepress == 1) || (selected_prepress == 5)) {
                        if (!sended) {
                            printler = true;
                            if (selected_prepress == 5) {
                                $('#theme_id').val(0);
                            }
                            if ((start == 0) || (complects_count > 1)) {
                                $('#type_id6').val(type_id);
                                $('#fill_calculation6').val(JSON.stringify(getCalculationData()));
                                var frm = $('#form_new_step6');
                                $.ajax({
                                    url: frm.attr('action'),
                                    type: frm.attr('method'),
                                    data: frm.serialize(),
                                    success: function (response) {
                                        stopLoadingAnimation();
                                        if (accessLevel == 1) {
                                            $('.user-message').show();
                                        }
                                        $('#modalContinueOrder').show();
                                        sended = false;
                                    },
                                    failed: function (data) {
                                        stopLoadingAnimation();
                                        sended = false;
                                    }
                                });
                            } else {
                                $('#type_id').val(type_id);
                                $('#fill_calculation').val(JSON.stringify(getCalculationData()));
                                $('#btn_form_new_step2').click();
                            }
                        }
                    } else if (selected_prepress == 2) {
                        printler = false;
                        sended = true;
                        startLoadingAnimation();
                        var frm = $('#form_new_step3');
                        $('#type_id3').val(type_id);
                        $('#fill_calculation3').val(JSON.stringify(getCalculationData()));
                        $.ajax({
                            url: frm.attr('action'),
                            type: frm.attr('method'),
                            data: frm.serialize(),
                            success: function (response) {
                                stopLoadingAnimation();
                                if (((accessLevel == 1) || (start == 1)) && (complects_count == 1)) {
                                    window.location.href = response;
                                } else {
                                    if (accessLevel == 1) {
                                        $('.user-message').show();
                                    }
                                    $('#modalContinueOrder').show();
                                }
                                sended = false;
                            },
                            failed: function (data) {
                                stopLoadingAnimation();
                                sended = false;
                            }
                        });
                    } else if ((selected_prepress == 3) || (selected_prepress == 4)){
                        printler = false;
                        sended = true;
                        startLoadingAnimation();
                        var frm = $('#form_new_step_designer');
                        $('#type_id4').val(type_id);
                        $('#fill_calculation4').val(JSON.stringify(getCalculationData()));
                        $('#sendInvoice4').val((selected_prepress == 3) ? 4 : 5);
                        $.ajax({
                            url: frm.attr('action'),
                            type: frm.attr('method'),
                            data: frm.serialize(),
                            success: function (response) {
                                stopLoadingAnimation();
                                if ((selected_prepress == 3) && (start == 1)) {
                                    window.location.href = response;
                                } else if ((accessLevel == 1) && (complects_count == 1)) {
                                    window.location.href = response;
                                } else {
                                    if (accessLevel == 1) {
                                        $('.user-message').show();
                                    }
                                    $('#modalContinueOrder').show();
                                }
                                sended = false;
                            },
                            failed: function (data) {
                                stopLoadingAnimation();
                                sended = false;
                            }
                        });

                    }
                }
                return false;
            });

            $('.selectProductType').click(function () {
                if (type_id != $(this).attr("data_product_type")) {
                    type_id = $(this).attr("data_product_type");
                    preset_type_name = $(this).closest('div').find('p').html();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                        },
                        type: "GET",
                        url: '/order/get_price/' + type_id + '/{{ $company_id }}/1',
                        success: function (data) {
                            $('#invoice_info').html($(data));
                            $('#calculation_table').show();
                            $('#btnCalculation').click();
                        }
                        ,
                        error: function (xhr, status, err) {
                            $('#calculation_table').hide();
                        }
                    });
                }
                return false;
            });
        });

    </script>

    @include('orders.waiting_for_server')
    @include('orders.continue_order_for_group')
    <form style="display: none;" method="post" action="/orders/post-step2" id="form_new_step2"
          enctype="multipart/form-data" autocomplete="off">
        <input type="text" name="_token" value="{{ csrf_token() }}">
        <input class="form-control" type="text" id="company_id" name="company_id" value="{{ $company_id }}"/>
        <input class="form-control" type="text" id="type_id" name="type_id" value=""/>
        <input class="form-control" type="text" id="theme_id" name="theme_id" value="-1"/>
        <input class="form-control" type="text" id="fill_calculation" name="fill_calculation" value=""/>
        <button id="btn_form_new_step2" type="submit" class="btn btn-primary">Добавить</button>
    </form>
    <form style="display: none;" method="POST" action="/orders/post-step3{{ request()->has('contact_id') ? '?contact_id=' . request('contact_id') : '' }}" id="form_new_step6"
          enctype="multipart/form-data" autocomplete="off">
        <input type="text" name="_token" value="{{ csrf_token() }}">
        <input class="form-control" type="text" id="company_id6" name="company_id" value="{{ $company_id }}"/>
        <input class="form-control" type="text" id="type_id6" name="type_id" value=""/>
        <input class="form-control" type="text" id="lib6" name="lib" value=""/>
        <input class="form-control" type="text" id="template_id6" name="template_id" value=""/>
        <input class="form-control" type="text" id="fill_calculation6" name="fill_calculation" value=""/>
        <input class="form-control" type="text" id="sendInvoice6" name="sendInvoice" value="6"/>
        <button id="btn_form_new_step6" type="submit" class="btn btn-primary">Добавить</button>
    </form>
    <form style="display: none;" method="post" action="/orders/post-step3{{ request()->has('contact_id') ? '?contact_id=' . request('contact_id') : '' }}" id="form_new_step3"
          enctype="multipart/form-data" autocomplete="off">
        <input type="text" name="_token" value="{{ csrf_token() }}">
        <input class="form-control" type="text" id="company_id3" name="company_id" value="{{ $company_id }}"/>
        <input class="form-control" type="text" id="type_id3" name="type_id" value=""/>
        <input class="form-control" type="text" id="lib3" name="lib" value="nolib"/>
        <input class="form-control" type="text" id="template_id3" name="template_id" value="-1"/>
        <input class="form-control" type="text" id="fill_calculation3" name="fill_calculation" value=""/>
        <input class="form-control" type="text" id="sendInvoice3" name="sendInvoice" value="3"/>
        <button id="btn_form_n4ew_step3" type="submit" class="btn btn-primary">Добавить</button>
    </form>
    <form style="display: none;" method="post" action="/orders/post-step3{{ request()->has('contact_id') ? '?contact_id=' . request('contact_id') : '' }}" id="form_new_step_designer"
          enctype="multipart/form-data" autocomplete="off">
        <input type="text" name="_token" value="{{ csrf_token() }}">
        <input class="form-control" type="text" id="company_id4" name="company_id" value="{{ $company_id }}"/>
        <input class="form-control" type="text" id="type_id4" name="type_id" value=""/>
        <input class="form-control" type="text" id="lib4" name="lib" value="nolib"/>
        <input class="form-control" type="text" id="template_id4" name="template_id" value="-1"/>
        <input class="form-control" type="text" id="fill_calculation4" name="fill_calculation" value=""/>
        <input class="form-control" type="text" id="sendInvoice4" name="sendInvoice" value="4"/>
    </form>
    <div id="invoice_info"></div>
    <div class="row">
        @if (Auth::user()->accessLevel() > 1)
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box" style="min-height: 220px;">
                    <div>
                        <p class="preset-type-name" style="padding-top: 10px;padding-left: 8px;padding-right: 8px">Произвольный заказ</p>
                        <a class="selectProductType" data_product_type="999999">
                            <div class="div_center">
                                <img src="/upload/products/tipografija.png"/>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        @endif
        @foreach ($product_types as $value)
            @if ($value->activity == 1)
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="info-box" style="min-height: 220px;">
                        <div>
                            <p class="preset-type-name" style="padding-top: 10px;padding-left: 8px;padding-right: 8px">{{ $value->name }}</p>
                            <a class="selectProductType" data_product_type="{{ $value->id }}">
                                <div class="div_center">
                                    <img src="/upload/products/{{ $value->url }}"/>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    </section><!-- /.content -->
@stop
