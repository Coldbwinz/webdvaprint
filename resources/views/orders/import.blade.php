@extends('app')

@section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Заказы</h3>
          <div class="box-tools">
            <div class="input-group" style="width: 150px;">
              <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Поиск">
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            
            <tbody><tr>
              <th>Название</th>
              <th>Превью</th>
              <th>Действия</th>              
            </tr>
            <? $i = 1; ?>
            @foreach ($imports as $value)
            <? 
            $dont_show = false;
            if (isset($value['templates'])) { 
              $dont_show = true;
            } 
            if (!$dont_show) {
            ?>
            <tr>
              <td>{{ $value['file'] }}</td>
              <td><img src="/lib/frx/default/{{ str_replace('.frx', '' ,$value['file']) }}.png" style="width: 200px;" /></td>
              <td><a href="javascript: void(0);" onclick="$('.box_{{ $i }}').slideDown();"><button class="btn btn-block btn-success btn-xs">Добавить</button></a></td>
            </tr>
   

            <tr class="box_{{ $i }}" style="display: none">
              <td colspan="3">


             <form method="POST" action="/import/add" autocomplete="off" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="box-body">

              <p><b>Тип продукции</b><br/><br/>
                <select name="product_types">
                  @foreach ($product_types as $pt)
                  <option value="{{ $pt->id }}">{{ $pt->name }}</option>
                  @endforeach
                </select>
              </p>
              <p><b>Тематика</b><br/><br/>

                  @foreach ($themes as $th)
                  <input type="checkbox" name="themes[]" value="{{ $th->id }}"> &nbsp; {{ $th->name }} &nbsp; &nbsp;
                  @endforeach
           
              </p>
            <input type="hidden" name="w2p" value="{{ $value['file'] }}" />
            <input type="hidden" name="url" value="{{ str_replace('.frx', '' ,$value['file']) }}.png" />
            <button type="submit" class="btn btn-primary">Добавить</button>
         
        </form>
              </td>
            </tr>
       
            <? } ?>
           <? $i++; ?>
           
           @endforeach

          </tbody></table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section><!-- /.content -->
@stop