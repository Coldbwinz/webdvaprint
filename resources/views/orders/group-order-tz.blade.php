@extends('app')

@section('content')
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <div>
                        Карточка заказа #{{ $groupOrder->id }}
                        <small class="pull-right">
                            Дата: {{ date('d.m.Y H:i', strtotime($groupOrder->updated_at)) }}
                        </small>
                    </div>

                </h2>
            </div>
        </div>

        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    <strong>Клиент</strong><br>

                    @if ($groupOrder->client->contacts->count())
                        <i class="fa fa-user"></i> {{ $groupOrder->client->contacts->first()->fullName }}<br>
                    @endif

                    @if ($groupOrder->client->is_company)
                        @if ($groupOrder->client->company_name)
                            компания: {{ $groupOrder->client->company_name }}<br>
                        @else
                            компания: {{ $groupOrder->client->tm_name }}<br>
                        @endif
                    @endif

                    @if ($groupOrder->client->contacts->count())
                        <i class="fa fa-phone"></i> телефон: {{ $groupOrder->client->contacts->first()->phone }}<br>
                        <i class="fa fa-envelope"></i> почта:
                        <a href="mailto:{{ $groupOrder->client->contacts->first()->email }}">
                            {{ $groupOrder->client->contacts->first()->email }}
                        </a>
                    @endif

                    @if ($groupOrder->manager && $groupOrder->manager->contact)
                        <br><br><strong>Менеджер</strong><br>
                        {{ $groupOrder->manager->contact->fullName }}
                        - {{ $groupOrder->manager->contact->getInformationFieldForClient($groupOrder->client->id, 'position') }}
                    @endif
                </address>
            </div>

            <div class="col-sm-4 invoice-col">
                // Preview
            </div>

            <div class="col-sm-4 invoice-col">
                <strong>Техническое задание</strong><br>
            </div>
        </div>

        <br><br>
        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Ответственный</th>
                            <th style="width: 20%">Дата</th>
                            <th>Статус</th>
                            <th>Значение</th>
                            <th>Комментарий</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($groupOrder->logs as $item)
                        <tr>
                            <td>
                                @if ($item->user && $item->user->contact)
                                    {{ $item->user->contact->fullName }}
                                @else
                                    <span class="label label-default">Пользователь не найден</span>
                                @endif
                            </td>
                            <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                @if ($item->statusInfo)
                                    {{ $item->statusInfo->name }}
                                @endif
                            </td>
                            <td>{{ $item->value }}</td>
                            <td>{!! $item->comments !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop
