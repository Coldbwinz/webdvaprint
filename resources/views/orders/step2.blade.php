@extends('app')
@section('content')
    <script>
        var pressed = false;
        $(document).ready(function() {
            $('#themes').on('click', 'a', function (e) {
                $('#theme_id2').val($(e.currentTarget).attr('data-href'));
                $('#btn_form_new_step2').click();
                return false;
            });
        });
    </script>
    <form style="display: none;" method="post" action="/orders/post-step2{{ request()->has('contact_id') ? '?contact_id=' . request('contact_id') : '' }}" id="form_new_step2" enctype="multipart/form-data" autocomplete="off">
        <input type="text" name="_token" value="{{ csrf_token() }}">
        <input class="form-control" type="text" id="order_id2" name="order_id" value="{{ $order_id }}"/>
        <input class="form-control" type="text" id="company_id2" name="company_id" value="{{ $company_id }}"/>
        <input class="form-control" type="text" id="type_id2" name="type_id" value="{{ $type_id }}"/>
        <input class="form-control" type="text" id="theme_id2" name="theme_id" value=""/>
        <input class="form-control" type="text" id="fill_calculation2" name="fill_calculation" value="{{ $fill_calculation }}"/>
        <button id="btn_form_new_step2" type="submit" class="btn btn-primary">Добавить</button>
    </form>
    <div class="row">
        <form style="display: none;" method="POST" action="/orders/post-step3{{ request()->has('contact_id') ? '?contact_id=' . request('contact_id') : '' }}" id="form_new_step3" enctype="multipart/form-data" autocomplete="off">
            <input type="text" name="_token" value="{{ csrf_token() }}">
            <input class="form-control" type="text" id="order_id" name="order_id" value="{{ $order_id }}"/>
            <input class="form-control" type="text" id="company_id" name="company_id" value="{{ $company_id }}"/>
            <input class="form-control" type="text" id="type_id" name="type_id" value="{{ $type_id }}"/>
            <input class="form-control" type="text" id="fill_calculation" name="fill_calculation" value="{{ $fill_calculation }}"/>
            <input class="form-control" type="text" id="lib" name="lib" value=""/>
            <input class="form-control" type="text" id="template_id" name="template_id" value=""/>
            <input class="form-control" type="text" id="action" name="action" value=""/>
            <button id="btn_form_new_step3" type="submit" class="btn btn-primary">Добавить</button>
        </form>
        <div class="col-md-3">
            <a href="/template/new/{{ $company_id }}/{{ $type_id }}" class="btn btn-primary btn-block margin-bottom">Добавить
                макет</a>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Тематики</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding" style="display: block;">
                    <ul id="themes" class="nav nav-pills nav-stacked">

                        <li class="active">
                            <a href="#" data-href="-1">Все</a>
                        </li>

                        <li>
                            <a href="#" data-href="0">
                                @if (Auth::user()->id_group == 1)
                                    Мои макеты
                                @else
                                    Макеты клиента
                                @endif
                                <span class="label label-warning pull-right">{{ $count_my }}</span>
                            </a>
                        </li>

                        @foreach ($themes as $theme)
                            <li>
                                <a href="#" data-href="{{ $theme->id }}">
                                    {{ $theme->name }}

                                    <span class="label label-primary pull-right">
                                        {{ $theme->templates->count() }}
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <script>
                $(document).ready(function () {
                    var prepress = {{ $selected_prepress }};
                    $('.gallery-image').on('click', '.selectTemplate', '', function (e) {
                        var $this = $(this);

                        if (prepress == 5) {

                        }

                        if ($this.data('editable') === false && prepress == 1) {
                            $('#nonEditable').modal('show');
                            $('#printAsIsButton').data('template-id', $this.data('template-id'));
                            $('#makeTemplate').data('template-id', $this.data('template-id'));
                            return false;
                        }

                        if (!pressed) {
                            pressed = true;
                            $('#lib').val($(e.currentTarget).attr('data-lib'));
                            $('#template_id').val($(e.currentTarget).attr('data-template-id'));
                            $('#btn_form_new_step3').click();
                            return false;
                        }
                    });
                });
            </script>
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Шаблоны: &nbsp; {{ $category_title }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="gallery">
                        @foreach ($templates as $value)
                            <li>
                                <div class="gallery-image">
                                    @if (isset($value->client_id))
                                        <a class="{{ $selected_prepress != 5 || auth()->user()->isClient() ? 'selectTemplate' : '' }}{{ $value->two_side ? ' has-two-side' : '' }}"
                                           data-lib="nolib"
                                           data-template-id="{{ $value->id }}"
                                           data-editable="{{ $value->editable ? 'true' : 'false' }}"
                                        >
                                            <div class="img-wrapper">
                                                <img src="{{ $value->url }}" class="gallery-preview is-first{{ strpos($selected_chromacity, '1 + 0') !== false ? ' is-grayscale' : '' }}">
                                                @if ($value->two_side)
                                                    <img src="{{ $value->second_preview }}" class="gallery-preview is-second{{ strpos($selected_chromacity, '4 + 1') !== false ? ' is-grayscale' : '' }}">
                                                @endif
                                            </div>
                                        </a>
                                    @else
                                        <a class="selectTemplate{{ $value->two_side ? ' has-two-side' : '' }}" data-lib="lib" data-template-id="{{ $value->id }}">
                                            <div class="img-wrapper">
                                                <img src="{{ $value->url }}" class="gallery-preview is-first{{ strpos($selected_chromacity, '1 + 0') !== false ? ' is-grayscale' : '' }}">
                                                @if ($value->two_side)
                                                    <img src="{{ $value->second_preview }}" class="gallery-preview is-second{{ strpos($selected_chromacity, '4 + 1') !== false ? ' is-grayscale' : '' }}">
                                                @endif
                                            </div>
                                        </a>
                                    @endif
                                </div>
                                @if (($theme_id != -1) && ($value->client_id && $selected_prepress == 5
                                || $selected_prepress == 1 && $value->editable === false) &&
                                ! auth()->user()->isClient())
                                    <div class="text-center">
                                        <div class="template-action-title text-primary"><strong>Утвердить</strong></div>
                                        <button class="agreeOnPlace btn btn-default btn-sm"
                                                data-template-id="{{ $value->id }}"
                                        >На месте</button>

                                        <button class="agreeByMail btn btn-primary btn-sm"
                                                data-template-id="{{ $value->id }}"
                                        >По почте</button>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('orders.partials.showTemplateToClient')
    @include('orders.partials.nonEditableModal')
    @include('orders.waiting_for_server')

    <div id="modalContinueOrder" class="modal">
        <div class="modal-dialog" style="margin-top: 25%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Сформировать заказ или продолжить оформление?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" style="width: 30%; " id="btnInvoice">Сформировать заказ</button>
                    <button type="button" class="btn btn-primary" style="width: 35%; margin-right: 18%;" id="btnNextGroupItem">Продолжить оформление</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.agreeOnPlace').click( function() {
            var $this = $(this);
            $('#lib').val('nolib');
            $('#action').val('agree_in_place');
            $('#template_id').val($this.data('template-id'));
            $('#btn_form_new_step3').click();
        });

        $('.agreeByMail').click( function() {
            var $this = $(this);
            $('#template_id').val($this.data('template-id'));
            $('#modalContinueOrder').show();
        });


        $('#btnPrintAsIsByMail').click( function() {
            $('#template_id').val(template_id);
            $('#modalContinueOrder').show();
        });

        var template_id;
        $('#printAsIsButton').on('click', function () {
            template_id = $(this).data('template-id');
            $('#nonEditable').modal('hide');
            $('#showTemplateToClient').modal('show');
        });

        $('#btnPrintAsIsOnPlace').click( function() {
            $('#lib').val('nolib');
            $('#action').val('print_as_is');
            $('#template_id').val(template_id);
            $('#btn_form_new_step3').click();
        });

        $('#makeTemplate').on('click', function () {
            var $this = $(this);
            $('#lib').val('nolib');
            $('#action').val('make_template');
            $('#template_id').val($this.data('template-id'));
            $('#btn_form_new_step3').click();
        });
    </script>

    <script>
        var continueButtonPressed = false;
        var company_details = '<?php echo $company_details ?>';
        var reviwer = '<? if (isset($reviwer)) echo $reviwer; ?>';

        $('#btnInvoice').on("click", function () {
            $('#lib').val('nolib');
            $('#action').val('btn_invoice');
            $('#btn_form_new_step3').click();
        });

        $('#btnNextGroupItem').on("click", function () {
            $('#lib').val('nolib');
            $('#action').val('btn_next');
            $('#btn_form_new_step3').click();
        });
    </script>
@stop