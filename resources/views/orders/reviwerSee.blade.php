@extends('out')

@section('scripts_bottom')
    @parent
    <script>
        $(document).ready(function () {
            var filename = "<?php echo $order->file; ?>";
            filename = filename.replace('.frx', '');
            $("#btnSave").click(function (e) {
                if (checkFields()) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                        },
                        url: '/order/status/' + '<?php echo $order->id ?>' + '/4/' + JSON.stringify(getCalculationData()) + '/1',
                        type: 'get',
                        success: function(response){
                            $('.modal').show();
                        },
                        failed: function(data) {
                            console.log("ajax error");
                        }
                    });
                } else {
                    return false;
                }
            });

            $('#btnAgree').click(function () {
                if (checkFields()) {
                    var href = "/user/reviwer/status/" + filename + "/4/" + JSON.stringify(getCalculationData()) + '/1';
                    window.location.href = href;
                } else {
                    if ($('#collapsed_container').css('display') == 'none') {
                        $('#btnCalculation').click();
                        $('#alertPlsSelectCalculation').show();
                    }
                    return false;
                }
            });
        });
    </script>
@stop

@section('content')
    <div class="pull-left" style="padding-bottom: 5px;">
        {{ $order->type_name }}@if($order->price_details['name'] or $order->price_details['name'] or $order->draw)
            ,@endif

        @if ($order->price_details['name'])
            материал {{ $order->price_details['name'] }},
        @endif

        @if ($order->draw)
            тираж {{ $order->draw }} шт.,
        @endif

        @if ($order->price)
            стоимость <span id="price_reviwer">{{ $order->price }}</span> руб.
        @endif

        @if ($order->price_details['options'])
            <br/><strong>Дополнительно:</strong>
            @foreach ($order->price_details['options'] as $option)
                {{ $option }};
            @endforeach

        @endif
    </div>
    <button id="btnAgree" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Утвердить</button>

    <a href="/user/reviwer/edit/{{ str_replace('.frx','', $order->file) }}">
        <button style="float: right;
    margin-right: 5px;" class="btn btn-info">Внести изменения
        </button>
    </a>
    <br><br>
    @if ($price_index != 'manual')
        @include('orders.priceForm_new')
    @else
        <div style="display:none;">
            @include('orders.priceForm_manual')
        </div>
    @endif
    <div class="alert alert-error" id="alertPlsSelectCalculation" style="display:none;">
        <span>
            <p>Пожайлуйста выберите услугу и объём заказа!</p>
        </span>
    </div>
    <p style="font-size: 16px;">
        При согласовании макета <b>ВНИМАТЕЛЬНО ПРОВЕРЯЙТЕ ИНФОРМАЦИЮ</b> на наличие <b>ОШИБОК</b> и <b>ОПЕЧАТОК</b>.<br> Найденная в готовом изделии ошибка, в случае, если макет утвержден, не является основанием для претензий к переделке заказа.
    </p>
    <div class="row no-print">
        <div class="col-xs-12" style="text-align: center;">
            <iframe src="/lib/see/index.html?uuid={{ $order->file }}" width="100%"
                    style="width: 100%;height: 80vh;border: 0px;">
                Ваш браузер не поддерживает плавающие фреймы!
            </iframe>
        </div>
    </div>
    <br/>
    <!--
      <a href="/user/reviwer/status/{{ str_replace('.frx','', $order->file) }}/4"><button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Утвердить</button></a>

      <a href="/user/reviwer/edit/{{ str_replace('.frx','', $order->file) }}"><button class="btn btn-info">Внести изменения</button></a>

      <br/><br/><br/>
-->

@stop
