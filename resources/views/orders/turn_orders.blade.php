@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <div id="table_turn_orders_block">
                        @include('orders.turn_orders_table')
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section><!-- /.content -->
@stop