@extends('app')

@section('content')
    @include('dashboard.top-panel', ['active' => ! empty($status) ? $status : null])

    <script src="/js/history.js"></script>
    <div class="row">
        <div id="modal_calculation" class="modal" style="top:25%;">
            <div class="modal-dialog" style="width:60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                onclick="backToPage();"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Расчёт</h4>
                    </div>
                    <div id="calculation" class="modal-body">
                        <p>Тело</p>
                    </div>
                    <div class="modal-footer">
                        <row>
                            <div class="form-group col-md-2">
                                <button type="button" class="btn pull-left" data-dismiss="modal"
                                        onclick="backToPage();">
                                    Закрыть
                                </button>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="close_date"
                                           class="form-control datepicker"
                                           style="width: 162px; float: left"
                                           value="">
                                </div>
                            </div>
                        </row>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <div id="table_history_block">
                        @include('orders.history_table_new')
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section><!-- /.content -->

    <script type="application/javascript">
        function invoice(id) {

            var draw = $('.draw_input_' + id);
            var price = $('.price_input_' + id);

            var error = false;

            $('.error_form').hide();

            if (!isNumeric(draw.val())) {
                $(draw).after('<span class="error_form" style="color:red">забыли заполнить это поле</span>');
                error = true;
            } else {
                if (draw.val() < 1) {
                    $(draw).after('<span class="error_form" style="color:red">забыли заполнить это поле</span>');
                    error = true;
                }
            }

            if (!isNumeric(price.val())) {
                $(price).after('<span class="error_form" style="color:red">забыли заполнить это поле</span>');
                error = true;
            } else {
                if (price.val() < 1) {
                    $(price).after('<span class="error_form" style="color:red">забыли заполнить это поле</span>');
                    error = true;
                }
            }

            if (error) {
                return false;
            }

            //return false;

        }

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    </script>

    @include('orders.modals.confirm-payment')
    @include('orders.waiting_for_server')
@stop

@section('scripts')
    <script>
        (function($) {
            var $confirmModal = $('#partnerConfirmPaymentModal');
            var $uploadSpecificationModal = $('#uploadSpecificationModal');

            var groupOrderId = null;
            var specOrderId = null;
            var file = null;

            // Show modal
            $('.confirmPaymentBtn').on('click', function () {
                $confirmModal.modal('show');
                groupOrderId = $(this).data('group-order-id');
            });

            // User pick file
            $confirmModal.on('change', '.confirmationFilePicker', function (event) {
                if (event.target.files.length === 0) {
                    file = null;
                    return false;
                }

                file = event.target.files[0];
            });

            // Send confirmation file to the server
            $confirmModal.on('click', '.sendConfirmationBtn', function () {
                if (! file) {
                    alert('Вы не выбрали файл.');
                    return false;
                }

                var formData = new FormData();
                formData.append('image', file);
                formData.append('partner', true);

                $.ajax({
                    url: '/orders/' + groupOrderId + '/confirm',
                    method: 'post',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if (response.status === 'success') {
                            alert('Файл успешно загружен');
                            $confirmModal.modal('hide');
                            window.location.reload();
                        }
                    }
                });
            });            
        })(jQuery);
    </script>

    <script>
        $('input[data-type="daterange"]').daterangepicker({
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
            ranges: {
                'За сегодня': [moment().startOf('day'), moment().endOf('day')],
                'За вчера': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'За неделю': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'За месяц': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'В этом месяце': [moment().startOf('month'), moment().endOf('month')],
                'В прошлом месяце': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                format: "DD.MM.YYYY HH:mm",
                separator: " - ",
                applyLabel: 'Выбрать',
                cancelLabel: 'Все периоды',
                customRangeLabel: 'Другой период'
            }
        })
        .on('cancel.daterangepicker', function (event, picker) {
            $(event.currentTarget).val('');
            filterActivate();
        })
        .on('apply.daterangepicker', function (event, picker) {
            $(event.currentTarget).val(
                picker.startDate.format(picker.locale.format) + picker.locale.separator + picker.endDate.format(picker.locale.format)
            );
            filterActivate();
        });

        var imageUploadUrl = null;
        $('.confirm_close_doc_image_selected').change(function (e) {
            if (e.target.files.length > 0) {
                $.each(e.target.files, function (i, item) {
                    if (!validateFile(item)) {
                        alert('Неверный формат файла! Необходимо выбрать изображение в формате jpg, png, pdf');
                        return false;
                    }
                });
            }
        });

        function validateFile (file) {
            return ["image/jpeg", "image/png", "image/jpg", "application/pdf"].indexOf(file.type) !== -1;
        }

        function attachImageToOrderId(files) {
            return new Promise(function (resolve, reject) {
                var formData = new FormData();
                jQuery.each(files, function(i, file) {
                    formData.append('images[]', file);
                });
                $.ajax({
                    url: imageUploadUrl,
                    method: 'post',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        resolve(response);
                    }
                })
                .error(function (error) {
                    reject(error);
                })
                .always(function () {
                    closeDocImages = null;
                });
            });
        }

        $('.upload_close_doc_image').click(function(e) {
            var closeDocImages = $(this).closest('.closing_group_documents_form').find('input[name="images[]"]');
            var closeDocFiles = [];
            $.each(closeDocImages, function(i ,item) {
                if ($(item).get(0).files[0]) {
                    closeDocFiles.push($(item).get(0).files[0]);
                }
            });
            if (closeDocFiles.length > 0) {
                imageUploadUrl = '/orders/close_doc_load/' + $(e.target).data('group-order-id');
                attachImageToOrderId(closeDocFiles)
                    .then(function (response) {
                        if (response.status === 'success') {
                            alert('Файлы загружены.');
                            window.location = '/orders/history';
                        }
                    });
            }
        });

        $('[data-action="hide-form"]').on('click', function () {
            var target = $(this).data('target');
            $(target).hide();
        });

    </script>
@endsection
