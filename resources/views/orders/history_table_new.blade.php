<table id="table_history" class="table table-hover">
    <tr class="top_search_box">
        <form>
            <input class="form-control" name="page" style="display:none" value="{{ $current_page }}"/>
            <input class="form-control" name="change" style="display:none" value="0"/>
            <th>
                <span class="btn btn-default btn-clear-search" style="width: 30px; height:30px; padding: 4px 6px">
                    <i class="fa fa-undo"></i>
                </span>
            </th>
            <th colspan="3">
                <div class="input-group" style="width:100%">
                    <input type="text" name="search" class="form-control input-sm pull-right" style="border-right:none"
                       placeholder="Поиск по клиенту, контактам..." value="{{ request('search') }}"/>
                    <span class="input-group-addon" style="border-left:none"><i class="fa fa-search"></i></span>
                </div>
            </th>
            <th>
                <div class="input-group">
                    <input type="text" name="created_at" class="form-control input-sm" style="border-right:none"
                        value="{{ request('created_at') }}" placeholder="Любой период" data-type="daterange"/>
                    <span class="input-group-addon" style="border-left:none; padding-right:4px;"><i class="fa fa-caret-down"></i></span>
                </div>
            </th>
            <th>
                <select name="status_pay_id" class="form-control input-sm">
                    <option value="0">Все статусы оплаты</option>
                    <option value="8">Без предоплаты</option>
                    <option value="7">Частично оплачен</option>
                    <option value="6">Полностью оплачен</option>
                    <option value="1">Выставлен счёт</option>
                    <option value="500">Заказ сформирован</option>
                    <option value="-1">Просрочен платёж</option>
                </select>
            </th>
            <th>
                <select name="status_id" class="form-control input-sm">
                    <option value="0">Все статусы заказов</option>
                    @foreach ($statuses as $value)
                        <option value="{{ $value->id }}" @if ($value->id == request('by_summ')) selected @endif>
                            {{ ($value->id == 45) ? 'Контроль качества' : $value->name }}
                        </option>
                    @endforeach
                </select>
            </th>
            <th>
                <select name="product_type_id" class="form-control input-sm">
                    <option value="0">Все продукты</option>
                    @foreach ($product_types as $value)
                        <option value="{{ $value->id }}" @if ($value->id == request('product_type_id')) selected @endif>
                            {{ $value->name }}
                        </option>
                    @endforeach
                </select>
            </th>
        </form>
    </tr>
    <tr class="top_group_box"
        style="@if (!empty($selected_group_order_id) || ($status && $status != 3)) display: none @endif">
        <th>ID</th>
        <th>Клиент</th>
        <th>Контактное лицо</th>
        <th>Контакты</th>
        <th>Дата оформления</th>
        <th>Сумма заказа</th>
        <th>Статус</th>
        <th>Действия</th>
    </tr>
    <tbody id="orders_history_table_data">
        @include('orders.history_table_data')
    </tbody>
</table>
<div id="div_orders_history_pagination">
    {!! $pagination !!}
</div>

<script type="application/javascript">

    var canPress = true;
    var issuingAddresses = <?php echo json_encode(settings('issuingAddresses')); ?>;
    var printingAddresses = <?php echo json_encode(settings('printingAddresses')); ?>;

    function validate_form_pay_type(obj) {
        if ((obj[1].value > 0) && (obj[3].checked || obj[4].checked || obj[5].checked)) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Укажите сумму и выберите способ оплаты.");
        }
        return false;
    }

    function validate_form_pay_type_full(obj) {
        if (obj[1].checked || obj[2].checked || obj[3].checked) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Пожалуйста Выберите способ оплаты.");
        }
        return false;
    }

    function validate_form_reason(obj) {
        if (obj[1].value.length > 0) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Пожалуйста, заполните поле Комментарий");
        }
        return false;
    }

    function validate_form_close_with_discount(obj) {
        if (obj[1].value.length > 0) {
            if (checkCanPress()) {
                return true;
            }
        }
        if (canPress) {
            alert("Пожалуйста, заполните поле Согласована скидка");
        }
        return false;
    }

    function checkCanPress() {
        if (canPress) {
            canPress = false;
            return true;
        }
        return false;
    }

    function showBoxGroup(id, target) {
        if ($('.box_' + id).hasClass('active_box')) {
            if (target.tagName != 'BUTTON') {
                $('#table_history tr').not('.box_group').hide();
                $('.top_group_box').show();
                $('.box_group').removeClass('active_box');
            }
            else {
                $('#table_history tr').not('.box_group').not('.orders_list_item').hide();
            }
        } else {
            $('#table_history tr').not('.box_group').hide();
            $('.box_group').removeClass('active_box');
            $('.box_' + id).addClass('active_box');
            $('.orders_list_' + id).slideToggle();
        }
    }
    var selectedOrderId = 0;
    function showStoppedForm(_this, e, stoppedState) {
        var id = $(_this).data('id');
        selectedOrderId = $(_this).data('orderid');
        var form = $('.stopped_form_' + selectedOrderId);
        if (stoppedState != 1010) {
            form.find('.stopped-form-commentary-title').show();
            form.find('.stopped-form-commentary').show();
            form.find('.stopped-form-commentary input').prop('required', true);
            form.find('.pay-message').hide();
            form.find('.accept-message').html('Подтвердить');
            form.find('.cancel-message').html('Отмена');
            form.find('.cancel-message').attr('type','button');
        } else {
            if (form.find('.pay-message').html().trim().length > 10) {
                form.find('.pay-message').show();
                form.find('.accept-message').html('Клиент готов погасить расходы');
                form.find('.stopped-form-commentary input').prop('required', false);
                form.find('.stopped-form-commentary-title').hide();
                form.find('.stopped-form-commentary').hide();
            } else {
                form.find('.accept-message').html('Подтвердить');
                form.find('.stopped-form-commentary input').prop('required', true);
                form.find('.stopped-form-commentary-title').show();
                form.find('.stopped-form-commentary').show();
            }
            form.find('.cancel-message').html('Продолжить выполнение заказа');
            form.find('.cancel-message').attr('type','submit');
        }
        $('#stopped_state_' + selectedOrderId).val(stoppedState);
        form.show();
    }

    // Stop nested crossing events
    $(document).on('click', '.box_group button', function(e){
        e.stopPropagation();
    });

    // box_group tr click
    $(document).on('click', '.box_group', function(e){
        var id = $(this).data('id');
        if (!$('.box_' + id).hasClass('active_box')) {
            $('#table_history tr').not('.box_group').not('.top_search_box').hide();
            $('.box_group').removeClass('active_box');
            $('.box_' + id).addClass('active_box');
            $('.orders_list_' + id).slideToggle();
        }
        e.stopPropagation();
    });

    $(document).on('click', '.do_show_invoice', function(e) {
        var id = $(this).data('id');
        if ($(this).data('clientstatus') != 2) {
            showBoxGroup(id, e.target);
            $('.not_invoce_' + id).slideDown();
        }
        else {
            alert('Для этого клиента недоступно');
        }
    });

    $(document).on('click', '.do_show_invoice_full', function(e) {
        var id = $(this).data('id');
        showBoxGroup(id, e.target);
        $('.not_invoce_full' + id).slideDown();
    });

    $(document).on('click', '.do_inwork', function(e) {
        var id = $(this).data('id');
        var orderid = $(this).data('orderid');
        showBoxGroup(id, e.target);
        $('.inwork_' + orderid).show();
    });

    $(document).on('click', '.do_stopped', function(e) {
        let $button = $(this),
            orderId = $button.data('orderid'),
            self = this;

        $button.button('loading');

        $.get('/api/order-can-pause/' + orderId, function (response) {
            if (response.can_pause) {
                showStoppedForm(self, e, 1000);
            } else {
                alert("Заказ №" + orderId + " уже запущен в работу и приостановить его невозможно.");
            }
        })
            .always(function () {
                $button.button('reset');
            });
    });

    $(document).on('click', '.do_restore', function(e) {
        showStoppedForm(this, e, 0);
    });

    $(document).on('click', '.do_cancel', function(e) {
        showStoppedForm(this, e, 1010);
    });

    $('.cancel-message').click( function() {
        if ($('#stopped_state_' + selectedOrderId).val() == 1010) {
            $('#stopped_state_' + selectedOrderId).val(0);
            $('.stopped_form_' + selectedOrderId).submit();
        }
    });

    $('input[name=hwodo]').click(function(){
        var orderid = $(this).data('orderid');
        var inwork_not_my = $('.inwork_not_my_' + orderid);

        if ($(this).val() == 1) {
            inwork_not_my.hide();
            inwork_not_my.find('input[name=close_date]').prop('required', false);
        }
        else {
            inwork_not_my.show();
            inwork_not_my.find('input[name=close_date]').prop('required', true);
        }
    });

    $('.btn_rework').click(function() {
        var orderid = $(this).data('orderid');
       $('.rework_' + orderid).toggle();
    });

    $('input[name="rework_type"]').click(function () {
        var obj = $(this);
        if (obj.val() == 3) {
            obj.closest('.rework_form').find('.rework_count').show();
            obj.closest('.rework_form').find('input[name="rework_reason_count"]').attr('required',true);
        } else {
            obj.closest('.rework_form').find('input[name="rework_reason_count"]').val('');
            obj.closest('.rework_form').find('.rework_count').hide();
            obj.closest('.rework_form').find('input[name="rework_reason_count"]').attr('required',false);
        }
    });

    $('input[name="rework_reason_count"]').change( function() {
        var obj = $(this);
        if (parseInt(obj.val()) < obj.attr('min')) {
            obj.val(obj.attr('min'));
        }
        if (parseInt(obj.val()) > obj.attr('max')) {
            obj.val(obj.attr('max'));
        }
    });

    $('input[name="issue_draw_to_print"]').change( function() {
        var obj = $(this);
        var thisForm = obj.closest('form');
        thisForm.find('input[name="issue_draw_discount"]').prop('required', !obj.prop('checked'));
        thisForm.find('input[name="issue_draw_discount"]').val('');
        thisForm.find('input[name="issue_discount_type"]').eq(0).prop('checked', true);
        thisForm.find('input[name="issue_draw_discount"]').parent().hide();
        thisForm.find('.div_issue_discount_type').toggle(!obj.prop('checked'));
    });

    var see_group_guarantee = 0;
    $('.btn_issue').click(function(e) {
        var groupOrderId = $(this).data('group-order-id');
        var groupOrderStatus = $(this).data('group-status');
        var guarantee = $(this).data('guarantee-image');
        if ((groupOrderStatus != 6) && (groupOrderId != see_group_guarantee)) {
            if (guarantee) {
                see_group_guarantee = $(this).data('group-order-id')
                alert('Оплата не поступила полностью. Вы можете выдать заказ только после получения оплаты или проверки гарантийного письма.');
                getGuarantee(groupOrderId);
            } else {
                alert('Оплата не поступила полностью. Вы можете выдать заказ только после получения оплаты или проверки гарантийного письма.');
            }
        } else {
            $('.issue_draw_' + $(this).data('order-id')).show();
        }
    });

    $('.issue_draw_form').on('input', 'input', function() {
        if (this.name == 'issue_draw') {
            var tempValue = $(this).data('temp-value');
            var newVal = ($(this).val() < tempValue);
            var thisForm = $(this).closest('form');
            thisForm.find('.issue_draw_trouble').toggle(newVal);
            thisForm.find('input[name="issue_draw_commentary"]').parent().toggle(newVal);
            thisForm.find('input[name="issue_draw_commentary"]').prop('required', newVal);
            thisForm.find('.div_issue_discount_type').toggle(newVal);
            if ($(this).val() >= tempValue) {
                thisForm.find('input[name="issue_draw_to_print"]').prop('checked', false);
                thisForm.find('input[name="issue_draw_discount"]').val('');
                thisForm.find('input[name="issue_discount_type"]').eq(0).prop('checked', true);
                thisForm.find('input[name="issue_draw_discount"]').parent().hide();
                thisForm.find('input[name="issue_draw_commentary"]').val('');
                thisForm.find('input[name="issue_draw_discount"]').prop('required', newVal);
            }
        }
    });

    $('input[name="issue_discount_type"]').change( function() {
        var thisForm = $(this).closest('form');
        var newValue = ($(this).val() == 1) ? true : false;
        thisForm.find('input[name="issue_draw_discount"]').parent().toggle(newValue);
        thisForm.find('input[name="issue_draw_discount"]').prop('required', newValue);
    });

    $(document).on('click', '.btnSetCheckControlQuality', function(e) {
        var formData = new FormData();
        formData.append('order_id', $(this).data('orderid'));
        var this_button = $(this);
        var td = this_button.closest('td');
        $.ajax({
            url: '/order/set_check_control_quality',
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response == 'ok') {
                    td.find('.btnSetCheckControlQuality').hide();
                    td.find('.send-rework').hide();
                    this_button.closest('tr').find('.control_quality').css('background-color', '#ef00d2').html('Контроль качества пройден');
                    $('.box_' + this_button.data('groupid')).find('.btnSendToIssue').show();
                }
            },
            error: function (e) {
                console.log('error ' + e.message);
            },
        });
    })

    $(document).on('click', '.btnSendToIssue', function(e) {
        var groupid = $(this).data('groupid');
        $('<form action="/group_order/send_to_issue" method="POST">' +
                '<input type="hidden" name="_token" value="{{ csrf_token() }}" />' +
                '<input type="hidden" name="group_id" value="'+groupid+'">' +
                '</form>').appendTo($(document.body)).submit();
        return false;
    });

    function sendIssueAddress(order_id, address) {
        $('<form action="/order/send_to_issue" method="POST">' +
                '<input type="hidden" name="_token" value="{{ csrf_token() }}" />' +
                '<input type="hidden" name="order_id" value="'+order_id+'">' +
                '<input type="hidden" name="address" value="'+address+'">' +
                '</form>').appendTo($(document.body)).submit();
    }

    $('.btnPrintClosingDocuments').click( function(e) {
        var group_order_id = $(this).data('group_order_id');
        var status = $(this).data('status');
        download_doc($('#get_invoice'));

        $.ajax({
            url: '/orders/close_doc_act_print/' + group_order_id,
            type: "get",
            success: function (response) {
                $('#get_act').attr('href', response);
                download_doc($('#get_act'));
                window.location.href = '/orders/history'
            },
            error: function (e) {
                console.log('error ' + e.message);
            },
        });
        return false;
    });

    function download_doc(obj) {
        if ($(obj).attr('href').length > 0) {
            var a = document.createElement('a');
            a.href = $(obj).attr('href');
            a.target = '_blank';
            document.body.appendChild(a);
            a.click();
        }
    }

    var location_href = '';
    $('.btnSelectIssuingAddress').click(function() {
        var orderid = $(this).data('orderid');
        var groupid = $(this).data('groupid');
        var address = $('.issuing_address_' + groupid).find('select[name=issuing_address_id]').find(':selected');

        sendIssueAddress(orderid, address.text());
        return false;
    });

    $("#fileupload").change(function(e) {
        var files = $(this).get(0).files;
        console.log($(this).get(0).files);

        $.each(files, function(index, file) {
            var formData = new FormData();
            formData.append('file', file);
            $.post('/order/' + orderId + '/specification/upload/', {}, function (response) {
                console.log('Upload completed');
                console.log(response);
            }).error(function (e) {
                console.log('error ' + e.message);
            });

            $.ajax({
                url: '/order/' + orderId + '/specification/upload/',
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                //Ajax events
                beforeSend: function (e) {
                    console.log('Are you sure you want to upload document.');
                },
                success: function (response) {
                    console.log('Upload completed');
                    console.log(response);
                },
                error: function (e) {
                    console.log('error ' + e.message);
                },
            });
        });
    });

    $('.addContractorBtn').click(function() {
        var order_id = $(this).data('order-id');
        var container = $('.inwork_' + order_id);
        var sumInput = container.find('input[name=summ_worker]');
        var dateInput = container.find('input[name=close_date]');

        if (sumInput.val() <= 0) {
            sumInput.focus();
            return false;
        }
        if(!dateInput.val()) {
            dateInput.focus();
            return false;
        }        
        window.location.href = "{{ route('contractors.contractors.index') }}?op=1&id=" + order_id + '&summ_worker=' + sumInput.val() + '&close_date=' + dateInput.val();
    });

</script>
