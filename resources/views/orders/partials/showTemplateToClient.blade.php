<div class="modal fade" id="showTemplateToClient" tabindex="-1" role="dialog" aria-labelledby="showTemplateToClientLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="Label">Показать макет клиенту</h4>
            </div>
            <div class="modal-footer">
                <button id="btnPrintAsIsOnPlace" type="button" class="btn btn-default" data-template-id="null">На месте</button>
                <button id="btnPrintAsIsByMail" type="button" class="btn btn-primary" data-template-id="null">По почте</button>
            </div>
        </div>
    </div>
</div>
