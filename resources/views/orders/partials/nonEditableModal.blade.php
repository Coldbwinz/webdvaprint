<?php
    $designer_days = '   ';
    $designer_price = '   ';
    $days = ['','1 день', '2 дня', '3 дня', '4 дня', '5 дней'];
    if (isset($order)) {
        if ($order->productType) {
            $designer_service = json_decode($order->productType->services)->$price_index;
        } else {
            $designer_service = \App\ProductType::productTypeSameByCalculation($order->price_details['fill_calculation']);
        }
    } else {
        $price_index = json_decode($fill_calculation)->price_index;
        if ($price_index != 'manual') {
            $designer_service = json_decode($product_type->services)->$price_index;
        } else {
            $designer_service = \App\ProductType::productTypeSameByCalculation($fill_calculation);
        }
    }
    if (isset($designer_service)) {
        $designer_days = $days[$designer_service->days_designer_remote];
        $designer_price = $designer_service->price_designer_remote;
    }
?>
<div class="modal fade" id="nonEditable" tabindex="-1" role="dialog" aria-labelledby="nonEditableLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="Label">Выбранный макет не может быть изменен</h4>
            </div>
            <div class="modal-body">
                Создание редактируемого шаблона по этому образцу займет {{ $designer_days }}, стоимость {{ $designer_price }} руб.
            </div>
            <div class="modal-footer">
                <button id="printAsIsButton" type="button" class="btn btn-default" data-template-id="null">
                    Печатать как есть
                </button>

                <button id="makeTemplate" type="button" class="btn btn-primary" data-template-id="null">Заказать
                    шаблон
                </button>
            </div>
        </div>
    </div>
</div>
