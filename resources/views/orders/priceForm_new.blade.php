<style>
    .btn.btn-lg:after {
        font-family: "Glyphicons Halflings";
        content: "\e114";
        float: right;
        margin-left: 15px;
    }

    .btn.btn-lg.collapsed:after {
        content: "\e080";
    }

    .table-grey-grid {
        margin: 1%;
        width: 98%;
    }

    .table-grey-grid td, th {
        border: 3px solid #fff !important;
        font-size: 16px;
    }

    .box-footer {
        border: 0px;
    }

    .nopadding {
        padding: 0 !important;
    }

    table tr td {
        cursor: pointer;
    }

    table tr th {
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        cursor: pointer;
    }

     input[type='number'] {
         -moz-appearance: textfield;
     }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .dropdown-menu a {
        cursor: pointer;
    }

</style>
<script src="/js/price_functions.js"></script>
<script>
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    var prices = <?php echo json_encode($prices) ?>;
    var printler_type = prices.printler_type;
    var selected_size = '<?php echo $selected_size; ?>';
    var selected_type = '<?php echo $selected_type; ?>';
    var selected_pad = '<?php echo $selected_pad; ?>';
    var selected_pad_index = -1;
    var selected_pad_chromacity = '<?php echo $selected_pad_chromacity; ?>';
    var selected_cover = '<?php echo $selected_cover; ?>';
    var selected_cover_index = -1;
    var selected_cover_chromacity = '<?php echo $selected_cover_chromacity; ?>';
    var selected_material = '<?php echo $selected_material; ?>';
    var selected_chromacity = '<?php echo $selected_chromacity; ?>';
    var selected_product = '<?php echo $selected_product; ?>';
    var selected_options = JSON.parse('<?php echo json_encode($selected_options); ?>');
    var selected_urgency = '<?php echo $selected_urgency; ?>';
    var selected_urgency_id;
    var selected_prepress = '<?php echo $selected_prepress; ?>';
    var selected_delivery_type = JSON.parse('<?php echo $selected_delivery_type; ?>');
    var payment_types = prices.payment_types;
    var selected_payment_type = <?php echo $selected_payment_type ?>;
    var prepress_in_one_line = <?php echo $prepress_in_one_line ?>;
    if (typeof(selected_payment_type.id) == 'undefined') {
        selected_payment_type = prices.payment_types[0];
    }
    var complects_calculation_type = '<?php echo $complects_calculation_type; ?>';
    var complects_discount_percent = '<?php echo $complects_discount_percent; ?>';
    var selected_address = '<?php echo $selected_address; ?>';
    if (typeof(selected_delivery_type.name) == 'undefined') {
        selected_delivery_type = {
            name: prices.delivery_types[0].name, id: prices.delivery_types[0].id,
            cost: prices.delivery_types[0].cost, days: prices.delivery_types[0].days
        };
    }
    var issuing_addresses = <?php echo json_encode(settings('issuingAddresses')); ?>;
    var client_addresses = prices.client_addresses;
    var urgency_koef = 0;
    var price_index = '<?php echo $price_index; ?>';
    var price_price = Math.round('<?php echo $price; ?>').toFixed(2);
    var draw = '<?php echo $draw; ?>';
    var totalDraw = parseInt(draw);
    var draw_index = '';
    var template_cost = '<?php echo $template_cost; ?>';
    var template_cost_hour;
    var complects_count = 1;
    var creator = '<?php if (isset($order)) {
        echo $order->creator;
    } else {
        echo null;
    } ?>';
    var order_replicated = '<?php if (isset($order)) {
        echo $order->replicated;
    } else {
        echo null;
    } ?>';
    var close_date = '<?php echo $close_date; ?>';
    var selected_index = '';
    var current_price = null;
    var counts = 0;
    var minimal_date = '';
    var accessLevel = '<?php echo Auth::user()->accessLevel() ?>';
    var step1 = '<?php if (isset($step1)) {
        echo $step1;
    } else {
        echo null;
    } ?>';
    var selectedTD;
    var selectedTR;
    var focusedTR;
    var focusedTD;
    var focusedTH;
    var priceChanged = false;
    var errorMessage = 'Не указаны: ';
    var firstLoad = true;
    var price_change_reason = '<?php if (isset($price_change_reason)) {
        echo $price_change_reason;
    } else {
        echo null;
    } ?>';
    var dead_line;
    var deploy_time;
    var sides = 0;
    var header_size = '';
    var header_type = '';
    var header_chromacity = '';
    var header_material = '';
    var header_cover = '';
    var header_cover_chromacity = '';
    var header_pad = '';
    var header_pad_chromacity = '';
    var reviwer = '<?php echo (isset($reviwer)) ? true : false ?>';

    $(document).ready(function () {
        loadAddresses();
        if (selected_address.length > 0) {
            $('#group_address').val(selected_address);
        }
        if (selected_delivery_type.name) {
            $('#group_delivery_type').html(selected_delivery_type.name);
        }

        if (selected_payment_type.name) {
            $('#group_payment_type').html(selected_payment_type.name);
            $('#group_days_for_waiting_payment').val(selected_payment_type.days_for_wait);
            checkPaymentType();
        }

        selectPrice();
        if (selected_options.length > 0) {
            $.each($("input[name='namesOptionsChecked[]']"), function (i, item) {
                $.each(selected_options, function (k, selected_option) {
                    if (item.value == selected_option) {
                        $(item).click();
                        namesOptionsClick(item);
                    }
                });
            });
        }

        $.each($('.ddm-prepress a'), function (i, item) {
            if ($(item).attr('data-value') == selected_prepress) {
                $('#group_prepress').html($(item).html());
                return false;
            }
        });

        $('#priceBody').on('click', ".countsDiv", function (e) {
            selectedTD = $(e.currentTarget.parentNode).find('th').index(e.currentTarget);
            $('#user_count').val(e.currentTarget.textContent).change();
            clearAndDrawBackGround();
            calculateAndShow();
        });

        $('#priceBody').on('mouseover', 'td, th', '', function (e) {
            focusedTR = $(e.currentTarget.parentNode).index();
            focusedTD = $(e.currentTarget.parentNode).find('td').index(e.currentTarget);
            if (focusedTD == -1) {
                focusedTH = $(e.currentTarget.parentNode).find('th').index(e.currentTarget);
            } else {
                focusedTH = -1;
            }
            clearAndDrawBackGround();
        });

        $('#priceBody').mouseout(function () {
            focusedTR = -1;
            focusedTD = -1;
            focusedTH = -1;
            clearAndDrawBackGround();
        });

        $('#priceBody').on('click', '.product_prices', '', function (e) {
            selectedTR = $(e.currentTarget.parentNode).index();
            selectedTD = $(e.currentTarget.parentNode).find('td').index(e.currentTarget);
            selected_product = current_price.products[selectedTR - 1];
            $('#material_reviwer').html(selected_product);
            $('#user_count').val(prices.counts[selectedTD - 1]).change();
            clearAndDrawBackGround();
            calculateAndShow();
        });

        $('#priceBody').on('click', '.tdProducts', function (e) {
            selectedTR = $(e.currentTarget.parentNode).index();
            selected_product = $(e.currentTarget)[0].innerText.trim();
            $('#material_reviwer').html(selected_product);
            clearAndDrawBackGround();
            calculateAndShow();
        });

        $('#priceOptions').on('change', "input[name='namesOptionsChecked[]']", function (e) {
            namesOptionsClick(e.currentTarget);
        });

        function namesOptionsClick(input_item) {
            var option_index = 0;
            $.each(current_price.options, function (i, option) {
                if (option == input_item.value) {
                    option_index = i;
                    return false;
                }
            });
            $.each($('.product_prices'), function (i, item) {
                var value = parseFloat($(item).html());
                var index = i % counts + option_index * counts;
                var addValue = current_price.options_prices[index];
                if (input_item.checked) {
                    $(item).html((value + addValue).toFixed(2));
                } else {
                    $(item).html((value - addValue).toFixed(2));
                }
            });
            calculateAndShow();
            var optionsString = '';
            $("input[name='namesOptionsChecked[]']").each(function (i, item) {
                if (item.checked) {
                    optionsString += item.value + '; ';
                }
            });
            if (optionsString.length > 0) {
                $('#options_reviwer').html('доп опции: <b>' + optionsString + '</b>');
            } else {
                $('#options_reviwer').html('');
            }
        }

        $('#close_date').datepicker({
            language: 'ru',
            startDate: '5d',
            format: 'dd.mm.yyyy',
        });

        $('#close_date').change( function() {
            if ($('#close_date').val().length > 0) {
                var selected_urgency = '';
                var selected_urgency_id = 0;
                var selected_date = $('#close_date').val();
                var current_date = $('#close_date').datepicker("getDate").valueOf();
                var min_date = new Date(Math.floor(new Date().valueOf() / 86400000) * 86400000 + (new Date()).getTimezoneOffset() * 60000);
                $.each(current_price.urgencies, function (i, item) {
                    if (item) {
                        var urgency_date = Math.floor(new Date().valueOf() / 86400000) * 86400000 + (new Date()).getTimezoneOffset() * 60000 + (parseInt(item[draw_index].deploy_time) + addDay) * 86400000;
                        if ((current_date >= urgency_date) && ((current_date - urgency_date) <= (current_date - min_date))) {
                            min_date = urgency_date;
                            selected_urgency = item[draw_index].name;
                            selected_urgency_id = item[draw_index].id - 1;
                        }
                    }
                });
                $('#group_urgency').html(selected_urgency);
                $('#urgency_reviwer').html('срочность: <b>' + selected_urgency + ';</b>');
                $('#close_date').css('border-color', '#D2D6DE');
                setUrgency(selected_urgency_id);
                $('#close_date').val(selected_date);
            }
        });

        var addDay = 0;
        $('#close_date').click( function() {
            var found = false;
            var minDate = 100;
            $.each(current_price.urgencies, function(i, item) {
                if (item && (item[draw_index].deploy_time < minDate)) {
                    found = true;
                    minDate = item[draw_index].deploy_time;
                    if ((new Date()).getHours() >= item[draw_index].dead_line ) {
                        addDay = 1;
                        minDate += addDay;
                    }
                }
            });
            if (!found) { minDate = 0; }
            var minDate = new Date().getTime() + minDate * 86400000;
            $('#close_date').datepicker('setStartDate', new Date(minDate));
        });

        $('#complects_count').on('input change', function(e) {
            if (complects_count != $('#complects_count').val()) {
                complects_count = parseInt($('#complects_count').val());
                setDivLabels();
                changeTotalDraw();
            }
        });

        $('#user_count').on("input change", function () {
            if (draw != parseInt($('#user_count').val())) {
                draw = parseInt($('#user_count').val())
                changeTotalDraw();
            }
        });

        function changeTotalDraw() {
            if (complects_calculation_type == 1) {
                totalDraw = draw * complects_count;
            } else {
                totalDraw = draw;
            }
            calculate_draw_index();
            $('#draw_reviwer').html('тираж: <b>' + draw + '</b> шт.;');
            selectTrueTD();
            clearAndDrawBackGround();
            calculateAndShow();
        }

        $('#group_urgency').click(function (e) {
            $('#group_urgency').css('border-color', '#D2D6DE');
        });

        $('#table, #user_count, #total_price, #close_date, #group_prepress').click(function (e) {
            $('#alertPlsSelectCalculation').hide();
        });

        $('#group_prepress, #prepress_caret_button').click(function (e) {
            $('#group_prepress').css('color', '');
            $('#group_prepress').css('border', '');
        });

        $('.ddm-sizes').on('click', 'a', function () {
            selected_size = $(this).attr('data-value').trim();
            $('#group_size').html(selected_size);
            checkPrepress();
            selectPrice();
            clearCountAndSum();
        });

        $('.ddm-urgencies').on('click', 'a', function () {
            selected_urgency = $(this).attr('data-value');
            $('#group_urgency').html(selected_urgency);
            $('#urgency_reviwer').html('срочность: <b>' + selected_urgency + ';</b>');
            $('#close_date').css('border-color', '#D2D6DE');
            var id = $(this).attr('data-id') - 1;
            setUrgency(id);
        });

        $('#inpSamePages').click(function() {
            calculateAndShow();
        });

        $('.ddm-prepress').on('click', 'a', function () {
            selected_prepress = $(this).attr('data-value');
            $('#group_prepress').html($(this).html());
            setDivLabels();
            calculateAndShow();
            setUrgencyParameters();
        });

        $('.ddm-delivery_types').on('click', 'a', function () {
            selected_delivery_type = {
                name: $(this).html(), id: $(this).attr('data-id'),
                cost: $(this).attr('data-cost'), days: $(this).attr('data-days')
            };
            $('#group_delivery_type').html($(this).html());
            calculateAndShow();
            setUrgencyParameters();
            loadAddresses();
        });

        $('.ddm-payment_types').on('click', 'a', function () {
            selected_payment_type = payment_types[$(this).attr('data-id')];
            $('#group_payment_type').html($(this).html());
            checkPaymentType();
            calculateAndShow();
        });

        $('.ddm-types').on('click', 'a', function () {
            selected_type = $(this).attr('data-value').trim();
            $('#group_type').html(selected_type);
            selectPrice();
            clearCountAndSum();
        });

        $('.ddm-chromacities').on('click', 'a', function () {
            selected_chromacity = $(this).attr('data-value').trim();
            $('#group_chromacity').html(selected_chromacity);
            selectPrice();
            clearCountAndSum();
        });

        $('.ddm-materials').on('click', 'a', function () {
            selected_material = $(this).attr('data-value').trim();
            $('#group_material').html(selected_material);
            selectPrice()
            clearCountAndSum();
        });

        $('.ddm-covers').on('click', 'a', function (e) {
            applyCoversPrices(-1);
            selected_cover = $(this).data('value');
            selected_cover_index = $(this).data('index');
            $('#group_cover').html(selected_cover);
            if (selected_cover == 'Без обложки') {
                selected_cover_chromacity = 'без печати';
                $('#group_cover_chromacity').html(selected_cover_chromacity);
                $('#div_group_covers_chromacity').hide();
            } else {
                var addCoversChromacities = '';
                $.each(Object.keys(current_price.covers_prices), function (i, item) {
                    if (checkMaterials(current_price.covers_prices[item], selected_cover_index)) {
                        addCoversChromacities += '<li><a data-value="' + item + '">' + item + '</a></li>';
                    }
                });
                $('.ddm-covers-chromacity').html(addCoversChromacities);
                selected_cover_chromacity = $('.ddm-covers-chromacity a:first-child').data('value');
                $('#group_cover_chromacity').html(selected_cover_chromacity);
                $('#div_group_covers_chromacity').show();
            }
            applyCoversPrices(1);
            extProductName();
        });

        $('.ddm-pads').on('click', 'a', function (e) {
            applyPadsPrices(-1);
            selected_pad = $(this).attr('data-value').trim();
            selected_pad_index = $(this).data('index');
            $('#group_pad').html(selected_pad);
            if (selected_pad == 'Без подложки') {
                selected_pad_chromacity = 'без печати';
                $('#group_pad_chromacity').html(selected_pad_chromacity);
                $('#div_group_pads_chromacity').hide();
            } else {
                var addPadsChromacities = '';
                $.each(Object.keys(current_price.pads_prices), function (i, item) {
                    if (checkMaterials(current_price.pads_prices[item], selected_pad_index)) {
                        addPadsChromacities += '<li><a data-value="' + item + '">' + item + '</a></li>';
                    }
                });
                $('.ddm-pads-chromacity').html(addPadsChromacities);
                selected_pad_chromacity = $('.ddm-pads-chromacity a:first-child').data('value');
                $('#group_pad_chromacity').html(selected_pad_chromacity);
                $('#div_group_pads_chromacity').show();
            }
            applyPadsPrices(1);
            extProductName();
        });

        $('.ddm-covers-chromacity').on('click', 'a', function () {
            applyCoversPrices(-1);
            selected_cover_chromacity = $(this).html();
            $('#group_cover_chromacity').html(selected_cover_chromacity);
            applyCoversPrices(1);
        });

        $('.ddm-pads-chromacity').on('click', 'a', function () {
            applyPadsPrices(-1);
            selected_pad_chromacity = $(this).html();
            $('#group_pad_chromacity').html(selected_pad_chromacity);
            applyPadsPrices(1);
        });

        @if (!\Auth::user()->isClient())
            if ((complects_calculation_type != 1) && (Math.round(getSumm()) != price_price)) {
                $('#total_price').val(price_price);
                $('#price_reviwer').val(price_price);
                $('#divChangePriceReason').show();
                $('#price_change_reason').val(price_change_reason);
                $('#total_price').removeAttr('disabled');
                priceChanged = true;
            }
        @endif
        firstLoad = false;
    });

    function extProductName() {
        if (printler_type == 3) {
            var addString = ' стр. ';
            var sc = (selected_cover == 'Без обложки');
            var sp = (selected_pad == 'Без подложки');
            if (selected_type == 'на скрепке') {
                if (sc) {
                    addString += 'без обложки';
                } else {
                    addString += '+обложка';
                }
            } else if (selected_type == 'на пружине') {
                if (sc && sp) {
                    addString += 'без обложки';
                } else if (!sc && sp) {
                    addString += '+обложка';
                } else if (sc && !sp) {
                    addString += '+подложка';
                } else if (!sc && !sp) {
                    addString += '+обложка';
                }
            }
            $.each(current_price.products, function (i, product) {
                $('#priceBody').find(".tdProducts").eq(i).html('<NOBR>&nbsp;&nbsp;' + product + addString + '</NOBR>');
            });
        }
    }

    function clearAndDrawBackGround() {
        var tableTR = $('#table tr');
        $('#table td').css('color', '#333');
        $('#table th').css('background-color', '#DDDDDD');
        tableTR.find('td:first-child').css('background-color', '#DDDDDD');
        tableTR.find('td:not(:first-child)').css('background-color', '#EEEEEE');

        var fTR = tableTR.eq(focusedTR);
        var ftdEQ = 'td:eq(' + focusedTD + ')';
        if (focusedTR > 0) {
            fTR.find('td').css('background-color', '{{ $colorSecondary }}');
        }
        if (focusedTD > 0) {
            tableTR.find(ftdEQ).css('background-color', '{{ $colorSecondary }}');
            tableTR.find('th').eq(focusedTD).css('background-color', '{{ $colorSecondary }}');
            fTR.find(ftdEQ).css('background-color', '{{ $colorPrimary }}');
            fTR.find(ftdEQ).css('color', '#FFF')
        }
        if (focusedTH > 0) {
            tableTR.find('td:eq(' + focusedTH + ')').css('background-color', '{{ $colorSecondary }}');
            tableTR.find('th').eq(focusedTH).css('background-color', '{{ $colorSecondary }}');
        }
        if ((focusedTD > 0) && (focusedTR > 0)) {
            $(focusedTR).find(ftdEQ).css('background-color', '{{ $colorPrimary }}');
            $(focusedTR).find(ftdEQ).css('color', '#FFF')
        }

        var sTR = $('#table tr:eq(' + selectedTR + ')');
        var stdEQ = 'td:eq(' + selectedTD + ')';
        if (selectedTR > 0) {
            sTR.find('td').css('background-color', '{{ $colorSecondary }}');
        }
        if (selectedTD > 0) {
            tableTR.find(stdEQ).css('background-color', '{{ $colorSecondary }}');
            tableTR.find('th').eq(selectedTD).css('background-color', '{{ $colorSecondary }}');
            sTR.find(stdEQ).css('background-color', '{{ $colorPrimary }}');
            sTR.find(stdEQ).css('color', '#FFF')
        }
        if ((selectedTD > 0) && (selectedTR > 0)) {
            sTR.find(stdEQ).css('background-color', '{{ $colorPrimary }}');
            sTR.find(stdEQ).css('color', '#FFF')
        }
    }

    function selectTrueTD() {
        selectedTD = null
        $.each($('.countsDiv'), function (i, item) {
            if (totalDraw == item.textContent) {
                selectedTD = i + 1;
            }
        });
    }

    function clearCountAndSum() {
        $('#user_count').val('').change();
        $('#total_price').val('');
    }

    function applyCoversPrices(delta) {
        if ((selected_cover != 'Без обложки') && (selected_cover_chromacity.length > 0) && (selected_cover_chromacity != 'без печати')) {
            var cover_index = -1;
            $.each(current_price.covers, function (i, item) {
                if (item == selected_cover) {
                    cover_index = i;
                    return false;
                }
            });
            $.each($('.product_prices'), function (i, item) {
                var value = parseFloat($(item).html());
                var addValue = parseFloat(current_price.covers_prices[selected_cover_chromacity][cover_index * counts + i % counts]);
                if (isNumber(addValue)) {
                    $(item).html((value + addValue * delta).toFixed(2));
                }
            });
            if (Object.keys(current_price.covers_prices).length > 1) {
                $('#div_group_covers_chromacity').show();
            } else {
                $('#div_group_covers_chromacity').hide();
            }
        }
        if ((selected_cover != 'Без обложки') && (Object.keys(current_price.covers_prices).length == 1)) {
            selected_cover_chromacity = Object.keys(current_price.covers_prices)[0];
            $('#group_cover').html(selected_cover + ', ' + selected_cover_chromacity);
        } else {
            $('#group_cover').html(selected_cover);
        }
        calculateAndShow();
    }

    function applyPadsPrices(delta) {
        if ((selected_pad != 'Без подложки') && (selected_pad_chromacity.length > 0) && (selected_pad_chromacity != 'без печати')) {
            var pad_index = -1;
            $.each(current_price.pads, function (i, item) {
                if (item == selected_pad) {
                    pad_index = i;
                    return false;
                }
            });
            $.each($('.product_prices'), function (i, item) {
                var value = parseFloat($(item).html());
                var addValue = parseFloat(current_price.pads_prices[selected_pad_chromacity][pad_index * counts + i % counts]);
                $(item).html((value + addValue * delta).toFixed(2));
            });
            if (Object.keys(current_price.pads_prices).length > 1) {
                $('#div_group_pads_chromacity').show();
            } else {
                $('#div_group_pads_chromacity').hide();
            }
        }
        if ((selected_pad != 'Без подложки') && Object.keys(current_price.pads_prices).length == 1) {
            selected_pad_chromacity = Object.keys(current_price.pads_prices)[0];
            $('#group_pad').html(selected_pad + ', ' + selected_pad_chromacity);
        } else {
            $('#group_pad').html(selected_pad);
        }
        calculateAndShow();
    }

    function redCloseDate() {
        if ($('#group_urgency').html() == 'Выбрать') {
            $('#group_urgency').css('border-color', 'red');
            return false;
        }
    }

    function getCalculationData() {
        var sendObj = {};
        sendObj.complects_count = $('#complects_count').val();
        sendObj.selected_address = $('#group_address').val();
        sendObj.selected_options = [];
        sendObj.selected_product = selected_product;
        $("input[name='namesOptionsChecked[]']").each(function (i, item) {
            if (item.checked) {
                sendObj.selected_options.push(item.value);
            }
        });

        selected_size = selected_size.replace('х', 'x');
        sendObj.selected_size = selected_size;
        sendObj.width = parseInt(selected_size.split('x')[0]);
        sendObj.height = parseInt(selected_size.split('x')[1]);
        sendObj.print_structure = make_print_structure(sendObj.width, sendObj.height);
        if (printler_type == 2) {
            (selected_type == 'один сгиб') ? sendObj.folds = 1 : sendObj.folds = 2;
        } else {
            sendObj.folds = 0;
        }
        sendObj.selected_type = selected_type;
        sendObj.selected_chromacity = selected_chromacity;
        sendObj.selected_material = selected_material;
        sendObj.price_index = selected_size + '_' + selected_type + '_' + selected_chromacity + '_' + selected_material;
        if (printler_type != 3) {
            if (selected_chromacity.split(' ')[2] == 0) {
                sendObj.sides = 1
            } else {
                sendObj.sides = 2
            }
        } else if (printler_type == 3) {
            sendObj.sides = parseInt(sendObj.selected_product);
            sendObj.selected_cover = selected_cover;
            sendObj.selected_cover_chromacity = selected_cover_chromacity;
            sendObj.sides = sides;
            if (selected_type == 'на пружине') {
                sendObj.selected_pad = selected_pad;
                sendObj.selected_pad_chromacity = selected_pad_chromacity;
            }
        }
        sendObj.draw = $('#user_count').val();


        var price = parseFloat($("#total_price").val()) - (template_cost * complects_count);
        sendObj.price = price / complects_count;
        sendObj.selected_prepress = selected_prepress;
        sendObj.selected_delivery_type = selected_delivery_type;
        selected_payment_type.days_for_wait = 0;
        if (selected_payment_type.name == 'Отсрочка платежа') {
            if ($('#group_days_for_waiting_payment').val() > 0) {
                selected_payment_type.days_for_wait = $('#group_days_for_waiting_payment').val();
            }
        }
        sendObj.selected_payment_type = selected_payment_type;
        sendObj.template_cost = template_cost;
        if ((selected_prepress == 4) && (template_cost_hour != -1)) {
            sendObj.template_cost_hour = template_cost_hour;
        }
        sendObj.close_date = $('#close_date').val();
        sendObj.selected_urgency = $('#group_urgency').html();
        sendObj.printler_type = printler_type;
        if (priceChanged) {
            sendObj.price_change_reason = $('#price_change_reason').val();
        }
        sendObj.start = ($('input[name="start"]').length > 0) ? $('input[name="start"]:checked').val() : 1;
        sendObj.prepress_in_one_line = ($('#inpPrepressInOneLine').length > 0) ? (($('#inpPrepressInOneLine:checked').length > 0) ? 0 : 1) : prepress_in_one_line;
        sendObj.complects_calculation_type =  complects_calculation_type;
        sendObj.complects_discount_percent = complects_discount_percent;
        return sendObj;
    }

    function checkFields() {
        redCloseDate();
        errorMessage = 'Не указаны: ';
        if (selected_product.length == 0) {
            addErrorMessage(', продукт');
        }
        if ($('#user_count').val() == 0) {
            addErrorMessage(', тираж');
        }
        if (selected_prepress == 0) {
            addErrorMessage(', макет');
            $('#group_prepress').css('border','1px solid #3c8dbc');
        }
        if ($('.datepicker').val() == 0) {
            addErrorMessage(', дата');
        }
        if ($("#total_price").val() == 0) {
            addErrorMessage(', цена');
        }

        if ($("#group_address").val() == 0) {
            addErrorMessage(', адрес');
        }
        if (priceChanged) {
            if ($('#price_change_reason').val().length == 0) {
                addErrorMessage(', причина изменения цены');
            }
        }
        if (errorMessage.length > 12) {
            errorMessage += '.';
            $('#errorMessage').html(errorMessage);
            return showError();
        }
        return true;
    }

    function addErrorMessage(message) {
        if (errorMessage == 'Не указаны: ') {
            errorMessage += message.substr(2);
        } else {
            errorMessage += message;
        }
    }

    function showError() {
        $('#alertPlsSelectCalculation').show();
        return false;
    }

    function loadPrice() {
        if (Object.keys(prices).length > 0) {
            var selectedItem = null;
            var sizes = '';
            var sizesArray = [];
            $.each(prices, function (i, item1) {
                if (i != 'counts') {
                    if (item1) {
                        if (typeof(item1) == 'object') {
                            if (item1.active == 1) {
                                item1.size = item1.size.replace('х', 'x'); //русская буква заменяется на английскую
                                if (!selectedItem) {
                                    selectedItem = item1;
                                }
                                if (!in_array(sizesArray, item1.size)) {
                                    sizesArray.push(item1.size);
                                    sizes += '<li><a data-value="' + item1.size + '">' + item1.size + '</a></li>';
                                }
                            }
                        }
                    }
                }
            });
            if (sizesArray.length > 1) {
                $('.ddm-sizes').html(sizes);
                $('#div_size_block').show();
            } else {
                header_size = sizesArray[0];
                if (printler_type > 1) {
                    header_size = ', ' + header_size;
                }
                $('#div_size_block').hide();
            }
            if (selected_size.length == 0) {
                selected_size = selectedItem.size;
            } else {
                selectedItem = prices[price_index];
            }
            $('#group_size').html(selected_size);
            $('#user_count').val(draw).change();
            $('#claculation_table').show();
        } else {
            $('#claculation_table').hide();
        }
    }

    function in_array(arr, item) {
        var found = false;
        $.each(arr, function (i, arr_item) {
            if (arr_item == item) {
                found = true;
                return false;
            }
        });
        return found;
    }

    function loadMenu() {
        var types = [];
        var typesString = '';
        var materials = [];
        var materialsString = '';
        var chromacity = [];
        var chromacityString = '';
        $.each(prices, function (i, item) {
            if (item) {
                if (typeof(item)) {
                    if (item.active == 1) {
                        if (item.size == selected_size) {
                            if (printler_type != 1) {
                                if (item.type != '###') {
                                    if (!in_array(types, item.type)) {
                                        types.push(item.type);
                                    }
                                }
                            }
                            if (printler_type == 3) {
                                if (!in_array(materials, item.material)) {
                                    materials.push(item.material);
                                }
                            }
                            if (!in_array(chromacity, item.chromacity)) {
                                if (checkMaterials(item.products_prices)) {
                                    chromacity.push(item.chromacity);
                                }
                            }
                        }
                    }
                }
            }
        });
        selected_type = (($.inArray(selected_type, types) != -1) && (selected_type.length > 0)) ? selected_type : types[0];
        if (types.length > 1) {
            $.each(types, function (i, item) {
                typesString += '<li><a data-value="' + item + '">' + item + '</a></li>';
            });
            $('.ddm-types').html(typesString);
            $('#group_type').html(selected_type);
            $('#div_type_block').show();
        } else if (types.length == 1) {
            header_type = types[0];
            $('#div_type_block').hide();
        }

        if (materials.length > 1) {
            $.each(materials, function (i, item) {
                materialsString += '<li><a data-value="' + item + '">' + item + '</a></li>';
            });
            $('.ddm-materials').html(materialsString);
            $('#group_material').html((selected_material.length > 0) ? selected_material : materials[0]);
            $('#div_body_material_block').show();
        } else if (materials.length == 1) {
            header_material = ', вн. блок: ' + materials[0];
            $('#div_body_material_block').hide();
        }

        if (chromacity.length > 1) {
            $.each(chromacity, function (i, item) {
                chromacityString += '<li><a data-value="' + item + '">' + item + '</a></li>';
            });
            $('.ddm-chromacities').html(chromacityString);
            if (selected_chromacity.length == 0) {
                selected_chromacity = chromacity[0];
            }
            $('#group_chromacity').html(selected_chromacity);
            $('#div_body_chromacity_block').show();
        } else if (chromacity.length == 1) {
            header_chromacity = ', ' + chromacity[0];
            $('#div_body_chromacity_block').hide();
        }
        if (printler_type == 1) {
            selected_type = '###'
        }
        if (printler_type == 3) {
            if (selected_material.length == 0) {
                selected_material = materials[0];
            }
        } else {
            selected_material = '###';
        }
        if (selected_chromacity.length == 0) {
            selected_chromacity = chromacity[0];
        }
    }

    function gettd(count, tag) {
        var tdstring = "";
        for (var i = 0; i < count; i++) {
            tdstring += '<td class="' + tag + ' text-center"></td>';
        }
        return tdstring;
    }

    function setSelectedIndex() {
        selected_index = selected_size + '_' + selected_type + '_' + selected_chromacity + '_' + selected_material;
    }

    function selectPrice() {
        header_size = '';
        header_type = '';
        header_material = '';
        header_chromacity = '';
        header_cover = '';
        header_pad = '';
        header_cover_chromacity = '';
        header_pad_chromacity = '';
        loadPrice();
        loadMenu();
        setSelectedIndex();
        current_price = prices[selected_index];
        $("#priceBody").html(
                '<table id="table" class="table table-striped table-grey-grid"><tr>' +
                '<th class="col-md-2 text-center" style="font-size: 18px;font-weight: bold;margin-bottom: 0; cursor:default;">Тираж' +
                '</th></tr></tbody></table>');
        $("#priceOptions").html('');
        if (current_price) {
            counts = prices.counts.length;
            var table = $('#table');
            $.each(prices.counts, function (i, countValue) {
                table.find("th").eq(-1).after(
                        '<th style="text-align: center;font-size: 18px;" class="countsDiv">' +
                        countValue + '</th>'
                );
            });

            $.each(current_price.products, function (i, product) {
                if (product == selected_product) {
                    selectedTR = i + 1;
                }
                table.find("tr").eq(-1).after(
                    '<tr><td class="tdProducts"><NOBR>&nbsp;&nbsp;' + product + '</NOBR></td></tr>'
                );
            });

            table.find('tr:not(:first-child)').append(gettd(counts, 'product_prices'));
            var value_exist = 0;
            $.each(table.find('.product_prices'), function (i, product_price_div) {
                product_price_div.innerText = Math.round(current_price.products_prices[i] * 100) / 100;
                if ((current_price.products_prices[i].length == 0) || (current_price.products_prices[i] != 0)) {
                    value_exist++;
                }
                if (((i + 1) % counts) == 0) {
                    if (value_exist == 0) {
                        $(product_price_div).closest('tr').css('display','none');
                    }
                    value_exist = 0;
                }
            });
            if (current_price.options.length > 0) {
                var addOptions = '<div class="col-md-5" style="padding-left:25px;">' +
                        '<p style="font-weight: bold;margin-bottom: 5px;">Дополнительные опции</p>';
                $.each(current_price.options, function (i, serviceName) {
                    addOptions += '<div><input type="checkbox" name="namesOptionsChecked[]"' +
                            ' value="' + serviceName + '" style="vertical-align:top;"/>&nbsp;&nbsp;' + serviceName + '</div>';
                });
                addOptions += '</div>';
                $('#priceOptions').html(addOptions);
            }

            if (printler_type == 3) {
                $('#divByTight').css('display','none');
                applyCoversPrices(-1);
                var addCovers = '<li><a data-value="Без обложки">Без обложки</a></li>';
                if (current_price.covers.length > 0) {
                    $.each(current_price.covers, function (i, item) {
                        addCovers += '<li><a data-index="' + i + '" data-value="' + item + '">' + item + '</a></li>';
                    });
                    $('.ddm-covers').html(addCovers);
                    $('#div_cover_material_block').show();
                } else {
                    $('#div_cover_material_block').hide()
                }
                if (selected_cover == '') {
                    selected_cover = 'Без обложки';
                } else {
                    var found = false;
                    $.each(current_price.covers, function (i, item) {
                        if (item == selected_cover) {
                            found = true;
                            return false;
                        }
                    });
                    if (!found) {
                        selected_cover = 'Без обложки';
                    }
                }
                if (current_price.covers.length == 0) {
                    header_cover = ', обложка: ' + selected_cover;
                }
                if (Object.keys(current_price.covers_prices).length == 0) {
                    selected_cover_chromacity = 'без печати';
                } else if (Object.keys(current_price.covers_prices).length > 1) {
                    selected_cover_chromacity = Object.keys(current_price.covers_prices)[0];
                    $('#group_cover_chromacity').html(selected_cover_chromacity);
                    var addCoversChromacities = '';
                    $.each(Object.keys(current_price.covers_prices), function (i, item) {
                        addCoversChromacities += '<li><a data-value="' + item + '">' + item + '</a></li>';
                    });
                    $('.ddm-covers-chromacity').html(addCoversChromacities);
                }
                applyCoversPrices(1);

                if (selected_type == 'на скрепке') {
                    $('#div_group_pads').hide();
                    $('#div_group_pads_chromacity').hide();
                } else {
                    $('#divByTight').css('display','inline-block');
                    applyPadsPrices(-1);
                    var addPads = '<li><a data-value="Без подложки">Без подложки</a></li>';
                    if (current_price.pads.length > 0) {
                        $.each(current_price.pads, function (i, item) {
                            addPads += '<li><a data-index="' + i + '" data-value="' + item + '">' + item + '</a></li>';
                        });
                        $('#div_pad_material_block').show();
                    } else {
                        $('#div_pad_material_block').hide();
                    }
                    if (selected_pad == '') {
                        selected_pad = 'Без подложки';
                    } else {
                        var found = false;
                        $.each(current_price.pads, function (i, item) {
                            if (item == selected_pad) {
                                found = true;
                                return false;
                            }
                        });
                        if (!found) {
                            selected_pad = 'Без подложки';
                        }
                    }
                    if (current_price.pads.length == 0) {
                        header_pad = ', подложка: ' + selected_pad;
                    } else if (current_price.pads.length > 1) {
                    }
                    if (Object.keys(current_price.pads_prices).length == 0) {
                        selected_pad_chromacity = 'без печати';
                    } else if (Object.keys(current_price.pads_prices).length > 1) {
                        selected_pad_chromacity = Object.keys(current_price.pads_prices)[0];
                        $('#group_pad_chromacity').html(selected_pad_chromacity);
                        var addPadsChromacities = '';
                        $.each(Object.keys(current_price.pads_prices), function (i, item) {
                            addPadsChromacities += '<li><a data-value="' + item + '">' + item + '</a></li>';
                        });
                        $('.ddm-pads-chromacity').html(addPadsChromacities);
                    }
                    $('.ddm-pads').html(addPads);
                    $('#div_group_pads').show();
                    applyPadsPrices(1);
                    extProductName();
                }
            }

            if (current_price.complects_calculation_type != null) {
                complects_calculation_type = current_price.complects_calculation_type;
            }
            if (current_price.complects_discount_percent != null) {
                complects_discount_percent = current_price.complects_discount_percent;
            }
            if (selected_urgency.length > 0) {
                $.each(prices.urgency_table, function (i, item) {
                    if (item.name == selected_urgency) {
                        $('#group_urgency').html($('.ddm-urgencies li a:eq(' + i + ')').attr('data-value'));
                        calculate_draw_index();
                        setUrgency(i);
                        return false;
                    }
                })
            } else {
                if (order_replicated) {
                    $('#group_urgency').html('Выбрать');
                } else {
                    $('#group_urgency').html($('.ddm-urgencies li a:eq(0)').attr('data-value'));
                    calculate_draw_index();
                    setUrgency(0);
                }
            }
            selectTrueTD();
            clearAndDrawBackGround();
            if (!reviwer) {
                var headerString = header_type + header_size + header_material + header_chromacity + header_cover + header_cover_chromacity + header_pad + header_pad_chromacity;
                headerString = (headerString.substr(0,2) == ', ') ? headerString.substr(2) : headerString;
                $('.content-header').html('<h1 style="display:inline-block"> ' + preset_type_name +
                        ' </h1><h4 style="display:inline-block">&nbsp&nbsp' + headerString + ' </h4>');
            }
            extProductName();
            @if (isset($step1)) checkPrepress(); @endif
        }
    }

    function checkMaterials(arrPrices, selected_material_index) {
        var value_exist = 0;
        var good_lines = 0;
        var line_number = 0;
        $.each(arrPrices, function (i, item) {
            if ((item.length == 0) || (item != 0)) {
                value_exist++;
            }
            if (((i + 1) % prices.counts.length) == 0) {
                if (value_exist == prices.counts.length) {
                    if (typeof(selected_material_index) != 'undefined') {
                        if (selected_material_index == line_number) {
                            good_lines++;
                        }
                    } else {
                        good_lines++;
                    }
                }
                value_exist = 0;
                line_number++;
            }
        });
        return good_lines > 0;
    }

    function setUrgency(id) {
        selected_urgency_id = prices.urgency_table[id].id;
        urgency_koef = prices.urgency_table[id].discount;
        calculate_draw_index();
        calculateAndShow();
    }

    function setUrgencyParameters() {
        if (selected_urgency_id >= 0) {
            var current_date = new Date();
            var koef_days = 0;
            if (typeof(current_price.urgencies[selected_urgency_id].dead_line) == 'undefined') {
                deploy_time = parseInt(current_price.urgencies[selected_urgency_id][draw_index].deploy_time);
                dead_line = current_price.urgencies[selected_urgency_id][draw_index].dead_line;
            } else {
                deploy_time = parseInt(current_price.urgencies[selected_urgency_id].deploy_time);
                dead_line = current_price.urgencies[selected_urgency_id].dead_line;
            }
            if (current_date.getHours() >= dead_line) {
                koef_days = 1;
            }

            var prepressTime = 0;
            if (selected_prepress == 3) {
                prepressTime = parseInt(prices[selected_index].days_designer_remote);
            }

            var total_days = deploy_time + koef_days + prepressTime + parseFloat(selected_delivery_type.days);
            if (current_price.weekends_sign == 1) {
                for (var i = 0; i < total_days; i++) {
                    var temp_date = new Date(current_date.getTime() + i * 86400000);
                    if ((temp_date.getDay() == 6) || (temp_date.getDay() == 0)) {
                        total_days = total_days + 1;
                    }
                }
            }
            var new_date = new Date(current_date.getTime() + total_days * 86400000);
            var days = new_date.getDate();
            if (days < 10) {
                days = '0' + days;
            }
            var months = new_date.getMonth() + 1;
            if (months < 10) {
                months = '0' + months;
            }
            if ((close_date.length > 0) && (close_date != (days + '.' + months + '.' + new_date.getFullYear()))) {
                $('#close_date').val(close_date);
                close_date = '';
            } else {
                $('#close_date').val(days + '.' + months + '.' + new_date.getFullYear());
            }

            $('#close_date_reviwer').html('дата выдачи: <b>' + $('#close_date').val() + ';</b>');
        }
    }

    function togglePriceChangeReason() {
        if ($('#divChangePriceReason').is(':visible')) {
            $('#divChangePriceReason').hide();
            $('#price_change_reason').val('');
            $('#total_price').attr('disabled', '');
            priceChanged = false;
            calculateAndShow();
        } else {
            $('#total_price').removeAttr('disabled').focus();
            $('#divChangePriceReason').show();
            $('#price_change_reason').focus();
            priceChanged = true;
        }
    }

    function calculateAndShow() {
        var summ = getSumm();
        if (summ > 0 && !firstLoad) {
            $("#total_price").val(summ);
            $("#price_reviwer").html('стоимость: <b>' + summ + '</b> руб.');
        }
    }

    function calculate_draw_index() {
        if (totalDraw > 0) {
            if (totalDraw <= prices.counts[0]) {
                draw_index = 0;
            } else if (totalDraw >= prices.counts[prices.counts.length - 1]) {
                draw_index = counts - 1;
            } else {
                $.each(prices.counts, function (i, item) {
                    if ((i == counts) || (totalDraw < item)) {
                        draw_index = i - 1;
                        return false;
                    }
                });
            }
        } else {
            draw_index = 0;
        }
        setUrgencyParameters();
    }

    function getSumm() {
        @if (isset($step1))
            complects_count = parseInt($('#complects_count').val());
        @endif
        if ((totalDraw > 0) && (selected_product.length > 0)) {
            var selectedPriceIndex;
            var approx_koef = 0;
            var need_approx = false;
            if (totalDraw <= prices.counts[0]) {
                totalDraw = prices.counts[0];
                selectedPriceIndex = 0;
            } else if (totalDraw >= prices.counts[counts - 1]) {
                selectedPriceIndex = counts - 1;
            } else {
                $.each(prices.counts, function (i, item) {
                    if ((i == counts) || (totalDraw < item)) {
                        selectedPriceIndex = i - 1;
                        need_approx = (totalDraw > prices.counts[0]) && (totalDraw < prices.counts[counts - 1]);
                        if (need_approx) {
                            var count1 = prices.counts[selectedPriceIndex];
                            var count2 = prices.counts[selectedPriceIndex + 1];
                            approx_koef = (totalDraw - count1) / (count2 - count1);
                        }
                        return false;
                    }
                });
            }
            var summ = 0;
            var approxPrice = 0;
            if (need_approx) {
                var price1 = current_price.products_prices[(selectedTR - 1) * counts + selectedPriceIndex];
                var price2 = current_price.products_prices[(selectedTR - 1) * counts + selectedPriceIndex + 1];
                approxPrice = price1 - (price1 - price2) * approx_koef;
            } else {
                approxPrice = current_price.products_prices[(selectedTR - 1) * counts + selectedPriceIndex];
            }
            summ += approxPrice * totalDraw;

            $('input[name="namesOptionsChecked[]"]').each(function (i, item) {
                if (item.checked) {
                    var approxPrice = 0;
                    if (need_approx) {
                        var price1 = current_price.options_prices[i * counts + selectedPriceIndex];
                        var price2 = current_price.options_prices[i * counts + selectedPriceIndex + 1];
                        approxPrice = price1 - (price1 - price2) * approx_koef;
                    } else {
                        approxPrice = current_price.options_prices[i * counts + selectedPriceIndex];
                    }
                    summ += approxPrice * totalDraw;
                }
            });
            if (printler_type == 3) {
                if (selected_cover != 'Без обложки') {
                    $.each(current_price.covers, function (i, item) {
                        if (item == selected_cover) {
                            var approxPrice = 0;
                            if (need_approx) {
                                var price1 = current_price.covers_prices[selected_cover_chromacity][i * counts + selectedPriceIndex];
                                var price2 = current_price.covers_prices[selected_cover_chromacity][i * counts + selectedPriceIndex + 1];
                                approxPrice = price1 - (price1 - price2) * approx_koef;
                            } else {
                                approxPrice = current_price.covers_prices[selected_cover_chromacity][i * counts + selectedPriceIndex];
                            }
                            summ += approxPrice * totalDraw;
                            return false;
                        }
                    });
                }
                if (selected_type == 'на пружине') {
                    if (selected_pad != 'Без подложки') {
                        $.each(current_price.pads, function (i, item) {
                            if (item == selected_pad) {
                                var approxPrice = 0;
                                if (need_approx) {
                                    var price1 = current_price.pads_prices[selected_pad_chromacity][i * counts + selectedPriceIndex];
                                    var price2 = current_price.pads_prices[selected_pad_chromacity][i * counts + selectedPriceIndex + 1];
                                    approxPrice = price1 - (price1 - price2) * approx_koef;
                                } else {
                                    approxPrice = current_price.pads_prices[selected_pad_chromacity][i * counts + selectedPriceIndex];
                                }
                                summ += approxPrice * totalDraw;
                                return false;
                            }
                        });
                    }
                }
            }

            selectTemplateCost();
            summ = summ * ((100 + parseFloat(selected_payment_type.discount)) / 100);
            summ += template_cost;
            summ += parseFloat(selected_delivery_type.cost);
            var complects_percent = (100 - (((complects_calculation_type == 2) && (complects_count > 1)) ? complects_discount_percent : 0)) / 100;
            summ = (Math.round((summ + summ * urgency_koef / 100) * 100) / 100 * complects_count * complects_percent).toFixed(2);
            return summ;
        }
        return 0;
    }

    function selectTemplateCost() {
        template_cost = 0;
        template_cost_hour = -1;
        $('#divDesignerTimeComment p').hide();
        if (selected_prepress == 1) {
            if (!creator) {
                if (accessLevel == 1) {
                    template_cost = prices[selected_index].price_template_self;
                } else {
                    template_cost = prices[selected_index].price_template_employee;
                }
            } else {
                if (creator == 1) {
                    template_cost = prices[selected_index].price_template_self;
                } else if (creator == 2) {
                    template_cost = prices[selected_index].price_template_employee;
                }
            }
        } else if (selected_prepress == 2) {
            template_cost = prices[selected_index].price_customer_template_auto;
        } else if (selected_prepress == 3) {
            template_cost = prices[selected_index].price_designer_remote;
        } else if (selected_prepress == 4) {
            if (prices[selected_index].price_designer_together_selected == 0) {
                template_cost = prices[selected_index].price_designer_together;
            } else {
                template_cost = 0;
                template_cost_hour = prices[selected_index].price_designer_together;
                $('#divDesignerTimeComment p').html('Дополнительно: работа дизайнера ' + prices[selected_index].price_designer_together + ' руб/час').show();
            }
        }
        template_cost = parseFloat(calculateTemplateCost(template_cost, parseInt(selected_product.split(" ")[0])));
    }
</script>

<div id="calculation_table" class="box box-warning" style="@if (!isset($reviwer)) display:none @endif">
    <div clas="box" style="border-top: none;">
        @if (!isset($step1))
            <?php $change_disable = (strlen($price_change_reason) != 0) ||
                    (isset($order->price_details['fill_calculation']['complects_count']) &&
                            ($order->price_details['fill_calculation']['complects_count'] > 1) && ($complects_calculation_type == 1))?>
            <button id="btnCalculation" type="button" class="btn btn-lg collapsed"
                    style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                    data-toggle="collapse"
                    data-target="#collapsed_container" @if ($change_disable) disabled @endif>
                @if ($change_disable)
                    Изменение параметров заказа невозможно
                @else
                    @if (isset($order))
                        @if ($order->draw > 0)
                            Измененить параметры заказа
                        @else
                            Выбор параметров заказа
                        @endif
                    @else
                        {{ $prices->type_name }}, Расчёт
                    @endif
                @endif
            </button>
        @endif
        <div id="collapsed_container" @if (!isset($step1)) class="collapse" @endif>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="errorMessagealert alert-error" id="alertPlsSelectCalculation" style="display:none;">
            <span>
                <p id="errorMessage" style="margin-bottom: 0;">Пожалуйста, уточните параметры заказа и дату готовности заказа!</p>
            </span>
                </div>

                <form method="POST" action="/orders/new-step2/{{$company_id}}/{{$type_id}}"
                      enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                    <row @if (!isset($step1)) style="display:none;" @endif>
                        <div class="col-xs-12" style="padding-bottom: 8px;">
                            @if ($prices->printler_type != 1)
                                <div id="div_type_block" class="col-xs-2 nopadding" style="margin: 0 4px;">
                                    <div style="text-align:center;">
                                    <p style="margin-bottom:5px; font-size: 12px; display:inline-block;">
                                        @if ($prices->printler_type == 2) Количество сгибов @endif
                                        @if ($prices->printler_type == 3) Тип переплета @endif
                                    </p>
                                    @if ($prices->printler_type == 3)
                                    <div id="divByTight" style="display:inline-block;"><input id="inpByTight" type="checkbox" style="margin-top:0; vertical-align:middle;"><font style="font-size: 12px;"> по узкой стороне</font></input></div>
                                    @endif
                                    </div>
                                    <div class="input-group dropdown col-xs-12">
                                        <button id="group_type" class="btn btn-default btn-block dropdown-toggle"
                                                type="button" data-toggle="dropdown"></button>
                                        <ul class="ddm-types dropdown-menu"></ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span></span>
                                    </div>
                                </div>
                            @endif
                            <div id="div_size_block" class="col-xs-2 nopadding" style="margin: 0 4px;">
                                    <span class="input-group-addon"
                                          style="background: none; border:none; font-size: 12px;">
                                        @if ($prices->printler_type == 1)
                                            Обрезной размер (мм)
                                        @elseif ($prices->printler_type == 2)
                                            Размер в развернутом виде (мм)
                                        @elseif ($prices->printler_type == 3)
                                            Размер в сложенном виде (мм)
                                        @endif
                                    </span>
                                <row>
                                    <div class="col-xs-12 no-padding">
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_size" class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-sizes dropdown-menu">
                                            </ul>
                                    <span id="caret_button" role="button"
                                          class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD; height:14px !important;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span>
                                    </span>
                                        </div>
                                    </div>
                                </row>
                            </div>
                            @if ($prices->printler_type == 3)
                                <div id="div_body_material_block" class="col-xs-2 nopadding" style="margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Внутренний блок</span>
                                    <div>
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_material"
                                                    class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-materials dropdown-menu">
                                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle"
                                  style="background-color: #DDDDDD"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span
                                        class="caret"></span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div id="div_body_chromacity_block" class="col-xs-2 nopadding" style="margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Цветность
                            @if ($prices->printler_type != 3) (Кол-во сторон) @endif
                            @if ($prices->printler_type == 3) внутреннего блока @endif
                        </span>
                                <div class="input-group dropdown col-xs-12">
                                    <button id="group_chromacity" class="btn btn-default btn-block dropdown-toggle"
                                            type="button" data-toggle="dropdown"></button>
                                    <ul class="ddm-chromacities dropdown-menu"></ul>
                                        <span role="button" class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span></span>
                                </div>
                            </div>
                            @if ($prices->printler_type == 3)
                                <div id="div_group_covers" class="col-xs-2 nopadding" style="margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Обложка</span>
                                    <div>
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_cover" class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-covers dropdown-menu">
                                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle"
                                  style="background-color: #DDDDDD"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span
                                        class="caret"></span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div id="div_group_covers_chromacity" class="col-xs-2 nopadding" style="display:none; margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Цветность обложки</span>
                                    <div>
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_cover_chromacity"
                                                    class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-covers-chromacity dropdown-menu">
                                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle"
                                  style="background-color: #DDDDDD"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span
                                        class="caret"></span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div id="div_group_pads" class="col-xs-2 nopadding" style="margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Подложка</span>
                                    <div>
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_pad" class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-pads dropdown-menu">
                                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle"
                                  style="background-color: #DDDDDD"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span
                                        class="caret"></span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div id="div_group_pads_chromacity" class="col-xs-2 nopadding" style="display:none; margin: 0 4px;">
                        <span class="input-group-addon"
                              style="background: none; border:none; font-size: 12px;">Цветность подложки</span>
                                    <div>
                                        <div class="input-group dropdown col-xs-12">
                                            <button id="group_pad_chromacity"
                                                    class="btn btn-default btn-block dropdown-toggle"
                                                    type="button" data-toggle="dropdown"></button>
                                            <ul class="ddm-pads-chromacity dropdown-menu">
                                            </ul>
                            <span role="button" class="input-group-addon dropdown-toggle"
                                  style="background-color: #DDDDDD"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"><span
                                        class="caret"></span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </row>
                    <div id="priceBody">
                    </div>
                    <div class="box-footer">
                        <div class="container col-md-3 nopadding" style="padding-left: 5px; width:350px;">
                            <div class="container nopadding"  style="display:inline-block; @if (isset($step1)) width:225px; @else width:350px; @endif margin-top: 5px; vertical-align: bottom;">
                                <div class="input-group nopadding">
                                     <span class="input-group-addon">
                                        Тираж
                                    </span>
                                    <input id="user_count" class="form-control" type="number" value=""/>
                                     <span class="input-group-addon">
                                        шт.
                                    </span>
                                </div>
                            </div>
                            @if (isset($step1))
                            <div class="container" style="width: 122px; display: inline-block; padding-right:0;">
                                <p style="font-size: 12px; display: inline-block; margin-bottom: 5px;">Кол-во комплектов</p>
                                <input id="complects_count" type="number"
                                       class="form-control" value="1">
                            </div>
                            @endif
                            @if (isset($step1))
                                <div style="display:block; margin-top:10px;">
                                    @if ($prices->printler_type == 3)
                                        <div id="divSamePages" style="display:inline-block;">
                                            <label><input id="inpSamePages" type="checkbox" style="vertical-align:top;"/>
                                            <font style="font-size: 12px; font-weight: normal;"> макеты страниц одинаковые</font></label>
                                        </div>
                                    @endif
                                    <div style="display:inline-block;">
                                        <label><input id="inpPrepressInOneLine" type="checkbox" style="vertical-align:top;" checked/>
                                        <font style="font-size: 12px; font-weight: normal;"> выводить отдельной строкой</font></label>
                                    </div>
                                </div>
                                <div class="input-group nopadding" style="width:100%;">
                                    <span class="input-group-addon">
                                        Макет
                                    </span>
                                    <div class="input-group dropdown">
                                        <button id="group_prepress" class="btn btn-default btn-block dropdown-toggle"
                                                type="button"
                                                style="border-radius: 0; border: 1px solid {{$colorSecondary}}; color: {{$colorPrimary}}"
                                                data-toggle="dropdown">Выбрать
                                        </button>
                                        <ul class="ddm-prepress dropdown-menu" style="width:260px;">
                                            <li><a data-value="1">Создать макет по шаблону</a></li>
                                            <li><a data-value="2">
                                                    @if (Auth::user()->accessLevel() == 1)
                                                        Проверить макет автоматически
                                                    @else
                                                        Проверить макет заказчика
                                                    @endif
                                                </a></li>

                                            @if (auth()->user()->isRoot())
                                                <li><a data-value="3">Сформировать ТЗ дизайнеру</a></li>
                                                @if (Auth::user()->accessLevel() > 1)
                                                    <li><a data-value="4">Работа вместе с дизайнером</a></li>
                                                @endif
                                            @endif
                                            <li><a data-value="5">Использовать макет из архива</a></li>
                                        </ul>
                                    <span id="prepress_caret_button" role="button"
                                          class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD; height:14px !important;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span>
                                    </span>
                                    </div>
                                </div>
                                @if (Auth::user()->accessLevel() != 1)
                                    <div id="divWorkStart"
                                         style="display:none; border:1px solid #d2d6de; border-top:none;">
                                        <div style="padding-left: 15px;">
                                            <label style="font-weight: normal;"><input type="radio" name="start" value="0"
                                               checked> после оформления заказа</label>
                                            <label style="font-weight: normal;"><input type="radio" name="start"
                                               value="1"> по нажатию Продолжить</label>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="container nopadding" style="width:350px; margin-top:15px;">
                                <div class="container nopadding" style="width: 350px; vertical-align:bottom;">
                                    <div class="input-group nopadding">
                                        <span class="input-group-addon">
                                            <i class="fa fa-shopping-cart"></i>
                                        </span>
                                        <button id="group_delivery_type" class="btn btn-default btn-block dropdown-toggle"
                                                type="button" style="border-radius: 0;"
                                                data-toggle="dropdown">{{ $prices->delivery_types[0]->name }}
                                        </button>
                                        <ul class="ddm-delivery_types dropdown-menu" style="width:100%;">
                                            @foreach($prices->delivery_types as $item)
                                                <li><a data-id="{{ $item->id }}" data-cost="{{ $item->cost }}"
                                                       data-days="{{ $item->days }}">{{ $item->name }}</a></li>
                                            @endforeach
                                        </ul>
                                        <span role="button"
                                              class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD; height:14px !important;"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div id="divChoiseIssuingPoint" class="col-md-12 nopadding el-padding" style="border:1px solid #d2d6de; border-top:none; border-bottom:none;">
                                <div class="input-group dropdown">
                                    <input id="group_address" type="text" class="form-control dropdown-toggle"
                                           style="font-size: 16px; text-align: left; border-left:none; border-top:none;"
                                           data-toggle="dropdown" placeholder="Выбрать адрес доставки"
                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Выбрать адрес доставки'"/>
                                    <ul class="ddm-addresses dropdown-menu"></ul>
                            <span role="button" class="input-group-addon dropdown-toggle" style="background-color: #DDDDDD; border-left: none; border-top:none; border-right:none;"
                                  data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false"
                                  onclick="$('#group_address').prop('placeholder','').focus();">
                                <span class="caret"></span></span>
                                </div>
                            </div>
                            <div class="container col-md-3 nopadding" style="width:350px; margin-top:5px;">
                                <div class="container col-md-6 nopadding" style="width:135px; display: inline-block;">
                                    <p style="font-size: 12px; display: inline-block; margin-bottom: 5px;">Дата
                                        выдачи</p>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input id="close_date" type="text"
                                               class="form-control datepicker"
                                               style="float: left" value="{{$close_date}}">
                                    </div>
                                </div>
                                <div class="container col-md-6 "
                                     style="width:215px; padding-right: 0px; display: inline-block;">
                                    <p style="font-size: 12px; display: inline-block; margin-bottom: 5px;">Тип
                                        срочности</p>
                                    <div class="input-group dropdown">
                                        <button id="group_urgency" class="btn btn-default btn-block dropdown-toggle"
                                                type="button" style="border-radius: 0;"
                                                data-toggle="dropdown"><? echo $prices->urgency_table[0]->name?></button>
                                        <ul class="ddm-urgencies dropdown-menu">
                                            @foreach ($prices->urgency_table as $value)
                                                <li><a data-id="{{$value->id}}"
                                                       data-value="{{$value->name}}">{{$value->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    <span id="caret_button" role="button"
                                          class="input-group-addon dropdown-toggle"
                                          style="background-color: #DDDDDD; height:14px !important;"
                                          data-toggle="dropdown"
                                          aria-haspopup="true" aria-expanded="false"><span
                                                class="caret"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="container col-md-12 nopadding" style="width:350px; margin-top:15px;">
                                <div class="container nopadding" style="width: 350px; vertical-align:bottom;">
                                    <div class="input-group nopadding">
                                        <span class="input-group-addon">
                                            <i class="fa fa-money"></i>
                                        </span>
                                        <button id="group_payment_type" class="btn btn-default btn-block dropdown-toggle"
                                                type="button" style="border-radius: 0;"
                                                data-toggle="dropdown">{{ $prices->payment_types[0]->name }}
                                        </button>
                                        <ul class="ddm-payment_types dropdown-menu" style="width:100%;">
                                            <? $payment_types_index = 0; ?>
                                            @foreach($prices->payment_types as $payment_type)
                                                <li><a data-id="{{ $payment_types_index }}" data-discount="{{ $payment_type->discount }}">{{ $payment_type->name }}</a></li>
                                                <? $payment_types_index++; ?>
                                            @endforeach
                                        </ul>
                                        <span role="button"
                                              class="input-group-addon dropdown-toggle"
                                              style="background-color: #DDDDDD; height:14px !important;"
                                              data-toggle="dropdown"
                                              aria-haspopup="true" aria-expanded="false"><span
                                                    class="caret"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div id="divDaysForWaitingPayment" class="col-md-12 nopadding el-padding" style="display:none; border:1px solid #d2d6de; border-top:none; border-bottom:none;">
                                <div class="input-group">
                                    <input id="group_days_for_waiting_payment" type="number" class="form-control dropdown-toggle"
                                           style="font-size: 16px; text-align: left; border-left:none; border-top:none;"
                                           value="0"/>
                                    <span class="input-group-addon" style="border-top:none; border-right:none;">дней после выдачи</span>
                                </div>
                            </div>

                            <div class="container col-md-12 nopadding">
                                <div class="container input-group nopadding" style="width:350px; margin-top:15px">
                                    <span class="input-group-addon">
                                        Итого
                                    </span>
                                    <input id="total_price" disabled class="form-control"
                                           @if (auth()->user()->isClient()) type="text" @else type="number" @endif
                                           style="background-color: #9FC5F8" value="{{ $price }}"/>
                                    @if (!auth()->user()->isClient())
                                        <div class="input-group-addon" style="cursor: pointer"
                                             onclick="togglePriceChangeReason()">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                                    @endif
                                </div>
                                @if (!auth()->user()->isClient())
                                    <div id="divChangePriceReason" class="collapsed col-md-12 nopadding"
                                         style="display: none;">
                                        <p style="padding-top:0; text-align: center; ;padding-bottom: 0; margin-top: 0; margin-bottom: 0;
                                        line-height: 34px; border-left: 1px solid #DDD; border-right: 1px solid #DDD;">
                                            Причина внесённых изменений</p>
                                        <div class="container input-group nopadding" style="width:350px;">
                                            <input id="price_change_reason" class="form-control">
                                            <i class="fa fa-delete"></i></input>
                                            <div class="input-group-addon" style="cursor: pointer"
                                                 onclick="togglePriceChangeReason()">
                                                <i class="fa fa-remove"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div id="divDesignerTimeComment" class="collapsed col-md-12 nopadding">
                                    <p style="text-align: left; padding-left: 10px; padding-top:0; padding-bottom: 0; margin-top: 0; margin-bottom: 0;
                                        line-height: 26px; border: 1px solid #DDD; border-top: none; display:none;">
                                        Дополнительно:</p>
                                </div>
                            </div>
                        </div>

                        <div id="priceOptions">
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
                @if (!isset($step1))
                    <row>
                        <div class="form-group col-md-6" style="padding-left: 0px;margin-left: 1%;">
                            <button id="btnSave" class="btn btn-info collapsed">Сохранить</button>
                            @if (isset($prices->send_invoice))
                                <button id="btnSendInvoice" class="btn btn-info collapsed"
                                        style="margin-left: 15px;">
                                    Выслать счет
                                </button>
                            @endif
                        </div>
                    </row>
                @else
                    <row>
                        <div class="form-group col-md-8" style="padding-left: 0; margin-left: 1%;">
                            <button id="btnNext" class="btn btn-info" style="background-color: #2B78E4">Продолжить
                            </button>
                        </div>
                    </row>
                @endif
            </div>
        </div>
    </div>
</div>
