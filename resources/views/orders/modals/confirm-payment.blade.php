<div class="modal fade" id="partnerConfirmPaymentModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    Загрузите платежное поручение
                </h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <input type="file"
                           name="file"
                           class="form-control confirmationFilePicker"
                    />

                    <div class="help-block">Поддерживаемые форматы файлов: jpg, png, pdf.</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Выход</button>
                <button type="button"
                        class="btn btn-primary sendConfirmationBtn"
                >
                    Отправить
                </button>
            </div>
        </div>
    </div>
</div>
