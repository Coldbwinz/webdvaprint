<div id="modalThanksForOrder" class="modal">
    <div class="modal-dialog" style="margin-top: 25%;">
        <form action="{{ route('orders.thanks_for_order') }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="modal-content" style="padding: 10px 2px;">
                <div class="modal-header">
                    <h3 class="modal-title text-center">Спасибо за заказ!</h3>
                </div>
                <div class="modal-body" style="height:140px; padding-left: 0; line-height: 25px;">
                    <div class="col-md-12 text-left" style="font-size: 14px;">
                        По Вашей заявке сформирован счёт № ### (Скачать его можно в личном кабинете)</br>
                        <font style="font-weight: bold;">Дубликат счёта отправлен на E-mail, оригинал будет передан вместе с заказом</font>
                    </div>
                    <div class="col-md-12 text-left" style="font-size: 13px; margin-top: 15px;">
                        Частное лицо может пересести деньги по счёту в любом отделении Сбербанка</br>
                        кроме того, Вы можете опратить заказа картой в Личном кабинете
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" style="width: 100px;"
                            id="btnNext">Продолжить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
