<script>
    $(document).ready(function () {
        $('#btnGetData').click(function () {
            pressedButton = true;
            var params = {
                inn: $('input[name="inn"]').val(),
                bik: $('input[name="bik"]').val(),
                rs: $('input[name="rs"]').val(),
                order_id: global_response.order_id,
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                },
                url: '/options/set_all_data/{{ $company_details }}',
                data: params,
                type: 'post',
                success: function (response) {
                    pressedButton = false;
                    var all_ok_info = JSON.parse(JSON.parse(response).all_ok_info);
                    var alertmsg = '';
                    $.each(all_ok_info, function(i, item) {
                       if (item != 1) {
                           alertmsg += item + '<br>';
                       }
                    });
                    if (alertmsg.length == 0) {
                        if (global_response.count_not_closed_orders == 0) {
                            window.location.href = '/order/close_group_order/{{ $company_id }}';
                        } else {
                            @if (\Auth::user() && (!\Auth::user()->isClient()))
                                    window.location.href = '/orders/history';
                            @else
                                    window.location.href = '/orders/need_attention';
                            @endif
                        }
                    } else {
                        $('.alert').show().find('div').html(alertmsg);
                    }
                },
                failed: function (data) {
                    pressedButton = false;
                    console.log("ajax error");
                }
            });
            return false;
        });
    });
</script>
<div id="modalGetCompanyData" class="modal">
    <div class="modal-dialog" style="margin-top: 25%;">
        <form>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="modal-content" style="padding: 15px 20px;">
                <div class="modal-header">
                    <h5 class="modal-title">Для оформления документов, нам необходимы реквизиты Вашей организации</h5>
                    <h4 class="modal-title">Заполните три поля, а остальное мы добавим самостоятельно</h4>
                </div>
                <div class="alert alert-danger alert-dismissible" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        <div></div>
                </div>
                <div class="modal-body" style="height:123px;">
                    <div class="form-group" style="height:30px; margin-bottom: 5px;">
                        <div class="col-md-5 text-right">ИНН</div>
                        <input class="col-md-7" name="inn" type="number" value="" required/>
                    </div>
                    <div class="form-group" style="height:30px; margin-bottom: 5px;">
                        <div class="col-md-5 text-right">Расчётный счёт</div>
                        <input class="col-md-7" name="rs" type="number" value="" required/>
                    </div>
                    <div class="form-group" style="height:30px;">
                        <div class="col-md-5 text-right">БИК</div>
                        <input class="col-md-7" name="bik" type="number" value="" required/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" style="width: 100px;"
                            id="btnGetData">Сохранить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
