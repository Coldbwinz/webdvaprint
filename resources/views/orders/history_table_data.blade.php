@foreach ($group_orders as $value)
    <?php
        // Если заказ партнёрский и выводится для типографии
        $partnerGroupOrderShowingForTypography = $value->subdomain && $value->subdomain->name !== SubdomainEnvironment::currentSubdomain();
    ?>

    <tr class="orders_list_{{ $value->id }} orders_list_item top_group_box_sub"
        style="@if (($value->id != $selected_group_order_id) && !$opened) display: none; @endif background-color: #dfe2e4;">
        <th class="td_id">ID</th>
        <th class="td_client">Клиент</th>
        <th class="td_contact_pers">Контактное лицо</th>
        <th class="td_contact">Контакты</th>
        <th class="td_data">Дата оформления</th>
        <th class="td_price">Сумма заказа</th>
        <th class="td_status">Статус</th>
        <th class="td_do">Действия</th>
    </tr>
    <tr class="box_{{ $value->id }} box_group @if (($value->id == $selected_group_order_id) || $opened) active_box @endif"
            data-id="{{ $value->id }}">
        {{-- ID --}}
        <td>
            <a href="{{ route('order-group.show', $value->id) }}">
                {{ $value->id }}({{ $value->orders_total_count }})
            </a>
        </td>
        {{-- Клиент --}}
        <td class="td_client">
            @if (! is_null($value->subdomain_id) && $value->subdomain->name != SubdomainEnvironment::currentSubdomain() && $value->subdomain->user->client->type_id == \App\ClientTypes::PARTNER)
                <a href="{{ route('client.show', $value->subdomain->user->client->id) }}">
                    {{ $value->subdomain->user->client->tm_name }}
                </a>
                <br>
                <span class="label label-default">
                        {{ $value->subdomain->user->client->type->name }}
                    </span>
            @else
                @if ($value->client)
                    <a href="{{ url("client/show/{$value->client_id}") }}">
                        {{ $value->client->isCompany() ? str_limit($value->client->tm_name, 20) : 'Частное лицо' }}
                    </a>

                    <br>

                    @if ($value->client->type)
                        <span class="label label-default">
                                {{ $value->client->type->name }}
                            </span>
                    @endif
                @endif
            @endif
        </td>

        {{-- Контактное лицо --}}
        <td class="td_contact_pers">
            @if (! is_null($value->subdomain_id) && $value->subdomain->name != SubdomainEnvironment::currentSubdomain() && $value->subdomain->user->client->type_id == \App\ClientTypes::PARTNER)
                <a href="{{ url("contact/show/{$value->client->manager->contact->id}/{$value->subdomain->user->client->id}") }}">
                    {{ $value->client->manager->contact->fullName }}
                </a>
            @else
                @if ($value->contact)
                    <a href="{{ url("contact/show/{$value->contact->id}/{$value->client->id}") }}"
                       title="{{ $value->contact->fullName }}"
                    >
                        {{ str_limit($value->contact->fullName, 20) }}
                    </a>
                @elseif ($value->client && $value->client->hasContacts())
                    <a href="{{ url("contact/show/{$value->client->contact->id}/{$value->client->id}") }}"
                       title="{{ $value->client->contact->fullName }}"
                    >
                        {{ str_limit($value->client->contact->fullName, 20) }}
                    </a>
                @endif
            @endif
        </td>

        {{-- Контакты --}}
        <td class="td_contact">
            @if (! is_null($value->subdomain_id) && $value->subdomain->name != SubdomainEnvironment::currentSubdomain() && $value->subdomain->user->client->type_id == \App\ClientTypes::PARTNER)
                <a href="mailto:{{ $value->client->manager->contact->email }}">
                    {{ $value->client->manager->contact->email }}
                </a>

                <br>

                {{ $value->client->manager->contact->phone }}
            @else
                @if ($value->contact)
                    {{-- Если к заказу привязан контакт выводит его данные --}}
                    <a href="mailto:{{ $value->contact->email }}">
                        {{ $value->contact->email }}
                    </a>

                    @if (isset($value->contact->phone))
                        <br><div><a class="contact-phone">{{ $value->contact->phone }}</a></div>
                    @endif
                @elseif ($value->client && $value->client->hasContacts())
                    {{-- Вывод данных первого контакта у клиента --}}
                    <a href="mailto:{{ $value->client->contact->email }}">
                        {{ $value->client->contact->email }}
                    </a>

                    @if (isset($value->client->contact->phone))
                        <br><div><a class="contact-phone">{{ $value->client->contact->phone }}</a></div>
                    @endif
                @endif
            @endif
        </td>

        {{-- Дата оформления --}}
        <td class="td_data">
                <span style="color: black">
                    <strong>{{ $value->created_at->format('d.m.Y') }}</strong>
                    <br>{{ $value->created_at->format('H:i:s') }}
                </span>
        </td>

        {{-- Сумма заказа --}}
        <td class="td_price">
            {{-- Счёт партнёру от типографии --}}
            @if (! is_null($value->subdomain) && $value->subdomain->name == SubdomainEnvironment::currentSubdomain())
                @if (in_array($value->status->id, [1, 6, 7, 8])&& $value->status_partner != 6)
                    <a href="{{ $value->getPartnerInvoiceLink() }}" class="label label-default production-invoice" target="_blank">
                        <i class="fa fa-file-text-o"></i> Счёт от производства
                    </a>
                @endif
            @endif

            <? $expenses = ($value->price_expenses < $value->price) ? true : false; ?>
            <div class="text-muted">@if ($expenses) <strike> @endif{{ $value->_price() }} руб. @if ($expenses) </strike> @endif
                @if ($expenses) <br> {{ $value->price_expenses }} руб. @endif</div>

            <br>

            @if ($partnerGroupOrderShowingForTypography)
                @if (in_array($value->status_partner, [6, 7, 8, 52, 1010]))
                    <span style="font-size: 14px">
                        @if ($value->pay_partner > $value->price_partner)
                            Переплата {{ abs($value->pay_partner - $value->price_partner) }} руб.
                        @else
                            к оплате {{ abs($value->pay_partner - $value->price_partner) }} руб.
                            @if (abs($value->pay_partner - $value->price_partner) > 0)
                                @if (strlen($value->close_date_partner) > 0)
                                    <br>до {{ $value->close_date_partner }}
                                @else
                                    <br>до ##.##.####
                                @endif
                            @endif
                        @endif
                        </span>
                @endif
            @else
                @if (in_array($value->status->id, [6, 7, 8, 52, 1010]))
                    <span style="font-size: 14px">
                        <? $price_expenses = ($value->price_expenses < $value->price) ? $value->price_expenses : $value->price; ?>
                        @if ($value->pay > $price_expenses)
                            Переплата {{ $value->pay - $price_expenses }} руб.
                        @elseif ($value->pay != $price_expenses)
                            к оплате {{ abs($value->pay - $price_expenses) }} руб.
                            @if (abs($value->pay - $price_expenses) > 0)
                                @if (strlen($value->close_date) > 0)
                                    <br>до {{ $value->close_date }}
                                @else
                                    <br>до ##.##.####
                                @endif
                            @endif
                        @endif
                        </span>

                @endif
            @endif
        </td>

        {{-- Статус --}}
        <td class="td_status">
            @if ($value->subdomain)
                {{-- Партнёрский заказ --}}
                @if ($value->subdomain->name !== SubdomainEnvironment::currentSubdomain())
                    <span class="label label-primary">
                            @if ($value->status->id < 6 || $value->status->id == 500)
                            {{-- 500 это статус "заказ сформирован" --}}
                            Заказ формируется
                        @else
                            @if ($value->statusPartner)
                                {{ $value->statusPartner->name }}
                            @else
                                Ожидает оплаты
                            @endif
                        @endif
                        </span><br>

                    @if ($value->confirm_image_partner && ! in_array($value->status_partner, [6, 7, 8]))
                        <a href="{{ url($value->confirm_image_partner) }}" target="_blank" class="label label-default">
                            Платёжное поручение
                        </a>
                    @endif

                    <div class="logs" style="font-size: 12px;margin-top:10px;">
                        @foreach ($value->logs as $record)
                            @if (! $record->subdomain && in_array($record->status, [6, 7]))
                                <div class="text-muted">{{ $record->comments }}</div>
                            @endif
                        @endforeach
                    </div>
                    <!--/td-->
                @else
                    <span class="label label-warning"
                          style="background-color: {{ "#".$value->status->color }} !important;">{{ $value->status->name }}
                        </span>

                    @if (in_array($value->status->id, [1, 8]) && $value->payment_status)
                        <br>
                        <a href="#" data-order-id="{{$value->id}}" class="label label-default checkGuarantee"
                           target="_blank">
                            {{ $value->payment_status }}
                        </a>
                    @endif

                    <div class="logs" style="font-size: 12px;margin-top:10px;">
                        @foreach ($value->logs as $record)
                            @if (! is_null($record->subdomain_id) && in_array($record->status, [6, 7]))
                                <div class="text-muted">{{ $record->comments }}</div>
                            @endif
                        @endforeach
                    </div>
                @endif
            @else
                <!-- Подсчёт отмененных заказов -->
                @if ($value->orders->where('status', 1010)->count() == $value->orders->count())
                    <span class="label label-warning"
                      style="background-color: #ca195a !important;">Отменён</span>
                @else
                    <span class="label label-warning"
                          style="background-color: {{ "#".$value->status->color }} !important;">
                        {{ $value->status->name }}
                    </span>
                @endif

                <? $price_expenses = ($value->price_expenses < $value->price) ? $value->price_expenses : $value->price; ?>
                @if ($value->pay > $price_expenses)
                    <br><span class="label label-warning">Переплачен</span>
                @endif

                @if (in_array($value->status->id, [1, 8]) && $value->payment_status)
                    <br>
                    <a href="#" data-order-id="{{$value->id}}" class="label label-default checkGuarantee"
                       target="_blank">
                        {{ $value->payment_status }}
                    </a>
                @endif

                @if ($value->repeat_send > 0 && $value->status->id == status('order_formed', 'id'))
                    <br>
                    <span class="label label-info">Отправлено напоминание</span><br>
                @endif

                <div class="logs" style="font-size: 12px;margin-top:10px;">
                    @foreach ($value->logs as $record)
                        @if (in_array($record->status, [6, 7]))
                            <div class="text-muted">{{ $record->comments }}</div>
                        @endif
                    @endforeach
                </div>

                @if (isset($value->return_defect))
                    <div class="logs" style="font-size: 12px;margin-top:2px;">
                        {{ "-".$value->return_defect." руб. (возврат за брак)" }}
                    </div>
                @endif

                @if (isset($value->return_discount))
                    <div class="logs" style="font-size: 12px;margin-top:2px;">
                        {{ "-".$value->return_discount." руб. (скидка за брак)" }}
                    </div>
                @endif

            @endif
        </td>

        {{-- Действия --}}
        @if ($value->subdomain && $value->subdomain->name !== SubdomainEnvironment::currentSubdomain() )
            <td class="td_do">
                @if ($value->resend)
                    <a href="/order/reviwer/send/repeated/{{ $value->id }}">
                        <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">
                            Напомнить о заявке
                        </button>
                    </a>
                @elseif ($value->confirm_image_partner && (is_null($value->status_partner) || $value->status_partner != 6))
                    <button class="do_show_invoice btn btn-block m-partpay btn-xs" data-id="{{ $value->id }}" data-clientstatus="{{ $value->client_status }}" style="margin-top: 5px;">
                        Частичная предоплата
                    </button>
                    <button class="do_show_invoice_full btn btn-block m-fullpay btn-xs" data-id="{{ $value->id }}" style="margin: 5px 0px 5px 0px">
                        Оплачен полностью
                    </button>
                    </a>
                @endif
            </td>
        @else
            <td class="td_do">
                @if ($value->resend)
                    <a href="/order/reviwer/send/repeated/{{ $value->id }}">
                        <button style="margin-bottom: 5px"
                                class="btn btn-block btn-success btn-xs">
                            Напомнить о заявке
                        </button>
                    </a>
                @else
                    @if ($value->status->id == 1)
                        <a id="no_money_{{$value->id}}" @if (($value->client_status == 1) || (($value->payment_status == 'Гарантийное письмо') && $value->check_guarantee))
                        href="/invoicenomoney/add/{{ $value->id }}"
                           @else
                           @if (($value->payment_status == 'Гарантийное письмо') && !$value->check_guarantee)
                           onclick="alert('Необходимо проверить гарантийное письмо!');"
                           @else
                           onclick="alert('Для этого клиента недоступно');"
                                @endif
                                @endif>
                            <button class="btn btn-block m-noprepay btn-xs">Печать без предоплаты</button>
                        </a>
                    @endif

                    {{-- Подтвердить оплату для типографии от партнёра. Только если частично или полностью оплачен --}}
                    @if (! is_null($value->subdomain_id) && ! $partnerGroupOrderShowingForTypography && in_array($value->status->id, [6, 7, 8]) && ! $value->confirm_image_partner)
                        <button class="btn btn-primary btn-block btn-xs confirmPaymentBtn" data-group-order-id="{{ $value->id }}">
                            Подтвердить оплату
                        </button>
                    @endif

                    @if (in_array($value->status->id, [1, 7, 8]))
                        <button class="do_show_invoice_full btn btn-block m-fullpay btn-xs" data-id="{{ $value->id }}" style="margin: 5px 0px 5px 0px">
                            Оплачен полностью
                        </button>
                    @endif

                    @if (in_array($value->status->id, [1, 7, 8]))
                        <button class="do_show_invoice btn btn-block m-partpay btn-xs" data-id="{{ $value->id }}" data-clientstatus="{{ $value->client_status }}" style="margin-top: 5px;">Частичная
                            предоплата
                        </button>
                    @endif
                @endif

                @if ($value->issued_orders_count() > 0)
                    <button style="margin-bottom: 5px" class="btn btn-success btn-block btn-xs" onclick="$('.closing_documents_{{ $value->id }}').show()">
                        Выдать документы
                    </button>
                @elseif ($value->status->id == 52)
                    <button style="margin-bottom: 5px" class="btn btn-block btn-close btn-xs" onclick="$('.closing_group_documents_{{ $value->id }}').show()">
                        Закрыть
                    </button>
                @endif
                <button style="@if ($value->send_to_issue_with_control_quality_count() == 0) display:none; @endif
                        margin-bottom: 5px" class="btn btn-block btn-xs send-delivery btnSendToIssue"
                    data-groupid="{{ $value->id }}"
                    >Отправить на выдачу</button>
            </td>
        @endif
    </tr>
    @if ($value->issued_orders_count() > 0)
        <tr class="closing_documents_{{ $value->id }} closing_documents_form" style="display: none">
            <td colspan="9" class="left-border rigth-border">
                <div class="panel box box-success with-border">
                    <div class="panel-collapse collapse in">
                        <div class="box-header">
                            <h3 class="box-title">Закрывающие документы</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-xs-4">
                                <div class="btn-actions">
                                    <a id="get_invoice" href="{{ (is_null($value->issued_act)) ? $value->getInvoiceLink() : '' }}" style="display:none"></a>
                                    <a id="get_act" href="" style="display:none"></a>
                                    <button class="btnPrintClosingDocuments btn btn-primary" data-group_order_id="{{ $value->id }}" data-status="{{ $value->status->id }}">
                                        Распечатать
                                    </button>
                                    <button onclick="$('.closing_documents_form').hide();" type="button" class="btn btn-primary" style="border-color: #000;background-color: #fff;color: #000;">
                                        Отменить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endif
    @if ($value->status->id == 52)
        <tr class="closing_group_documents_{{ $value->id }} closing_group_documents_form" style="display: none">
            <td colspan="9" class="left-border rigth-border">
                <div class="panel box box-success with-border">
                    <div class="panel-collapse collapse in">
                        <div class="box-header">
                            <h4 class="box-title" id="closeDocLoadModalLabel">
                                Загрузите закрывающие документы
                            </h4>
                        </div>
                        <div class="box-body" style="padding-bottom:15px;">
                            @for ($i = 0; $i < $value->issued_act - (isset($value->close_docs) ? count($value->close_docs) : 0); $i++)
                                <div class="form-group">
                                    <input type="file" name="images[]" class="form-control confirm_close_doc_image_selected"
                                           accept=".jpg, .jpeg, .png, .pdf"/>
                                </div>
                            @endfor
                            <div class="help-block">Поддерживаемые форматы файлов: jpg, png, pdf.</div>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-4">
                                <div class="btn-actions">
                                    <button class="btn btn-primary upload_close_doc_image"
                                            data-group-order-id="{{ $value->id }}">
                                        Загрузить
                                    </button>
                                    <button onclick="$('.closing_group_documents_form').hide();" type="button"
                                            class="btn btn-primary" style="border-color: #000;background-color: #fff;color: #000;">
                                        Отменить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endif
    <tr class="not_invoce_full{{ $value->id }} invoice_form_full row-border-left-right" style="display: none">
        <td colspan="9">
            <div class="panel box box-success">
                <div class="box-header with-border">
                    <h4 class="box-title">Полная оплата по счету #{{ $value->id }}</h4>
                </div>

                <div class="panel-collapse collapse in">
                    <div class="box-body">
                        <form method="POST"
                              action="{{ $partnerGroupOrderShowingForTypography ? '/invoice/addFullPartner/' . $value->id : '/invoice/close/' . $value->id }}"
                              onsubmit="return validate_form_pay_type_full ( this );"
                        >
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label>Способ оплаты:</label>
                                <div class="radio">
                                    <label><input type="radio" name="pay_type" value="2">На счёт</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="pay_type" value="1">В кассу</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="pay_type" value="3">E-money</label>
                                </div>
                            </div>
                            <div class="btn-actions">
                                <button id="btnSubmitFullPay" class="btn btn-primary pull-left">
                                    <i class="fa fa-download"></i>
                                    Зарегистрировать оплату
                                </button>
                                <button onclick="$('.invoice_form_full').hide();"
                                        type="button"
                                        class="btn btn-primary btn-cancel"
                                >
                                    Отменить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </td>
    </tr>

    <tr class="not_invoce_{{ $value->id }} invoice_form" style="display: none">
        <td colspan="9" class="left-border rigth-border">
            <div class="panel box box-success">
                <div class="box-header with-border">
                    <h4 class="box-title">Частичная оплата по счету #{{ $value->id }}</h4>
                </div>

                <div class="panel-collapse collapse in">
                    <div class="box-body">
                        <form method="POST"
                              action="{{ $partnerGroupOrderShowingForTypography ? '/invoice/addNotFullPartner/' . $value->id : '/invoice/addNotFull/' . $value->id }}"
                              enctype="multipart/form-data"
                              autocomplete="off"
                              onsubmit="return validate_form_pay_type ( this );"
                        >
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Сумма, руб</label>
                                            <input class="form-control" style="width: 200px" type="number"
                                                   name="price" value="{{ $partnerGroupOrderShowingForTypography ? $value->price_partner - $value->pay_partner :$value->price - $value->pay }}">
                                        </div>

                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label>Дата окончательного платежа</label>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="close_date"
                                                       class="form-control datepicker"
                                                       style="width: 162px;float: left">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Способ оплаты:</label>
                                            <div class="radio">
                                                <label><input type="radio" name="pay_type" value="2">На счёт</label>
                                            </div>
                                            <div class="radio">
                                                <label><input type="radio" name="pay_type" value="1">В кассу</label>
                                            </div>
                                            <div class="radio">
                                                <label><input type="radio" name="pay_type" value="3">E-money</label>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-actions">
                                <button type="submit" class="btn btn-primary pull-left">
                                    <i class="fa fa-download"></i>
                                    Зарегистрировать оплату
                                </button>
                                <button onclick="$('.invoice_form').hide();" type="button" class="btn btn-primary btn-cancel pull-left">
                                    Отменить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </td>
    </tr>

    <tr class="orders_list_{{ $value->id }} orders_list_item"
        style="@if (($value->id != $selected_group_order_id) && !$opened) display: none; @endif background-color: #dfe2e4;">
        <td class="left-border"><b>#</b></td>
        <td><b>Наименование</b></td>
        <td><b>Тираж</b></td>
        <td><b>Превью</b></td>
        <td><b>Дата выдачи</b></td>
        <td><b>Цена</b></td>
        <td><b>Статус</b></td>
        <td class="rigth-border"><b>Действия</b></td>
    </tr>

    <?php $i = 0; ?>
    @foreach ($value->orders as $order)
        <?php $i++; ?>
        <tr class="orders_list_{{ $value->id }} orders_list_item row-border-left-right" style="@if (($value->id != $selected_group_order_id) && !$opened) display: none; @endif border-bottom:1px solid#f4f4f4 !important">
            {{-- ID --}}
            <td>
                <a href="/order/tz/{{ $order->id }}">
                    {{ $order->group_id }}.{{ $order->position }}
                </a>
            </td>

            {{-- Наименование --}}
            <td>
                {{ $order->product_name . ' ' . $order->price_details['selected_size'] }}
                @if ($order->printler_type == 3),
                <br>{{ $order->price_details['selected_product'] . ' стр.'}}@if ($order->price_details['selected_cover'] != 'Без обложки')
                    +обложка @else Без обложки @endif
                @elseif ($order->printler_type == 1),
                <br>{{ $order->price_details['selected_chromacity'] }}
                @elseif ($order->printler_type == 2),
                <br>({{ $order->price_details['selected_type'] }})
                @endif
            </td>

            {{-- Тираж --}}
            <td>
                {{ $order->draw }} шт.
                @if (is_null($order->parent_id) && $order->issue_troubles && ($order->issue_troubles['issued'] > 0))
                    @if (!isset($order->issue_troubles['discount']))
                        <br>к выдаче {{ $order->draw - $order->issue_troubles['issued'] }} шт.
                    @else
                        <br>выдано {{ $order->issue_troubles['issued'] }} шт.
                    @endif
                @endif
            </td>
            {{-- Превью --}}
            <td>
                @if (in_array($order->status, [2000, 2010]))
                    <p style="text-align: left">Макет еще не выбран</p>
                @elseif ($order->status == 10)
                    <p style="text-align: left">Макет еще не загружен</p>
                @elseif ($order->status == 300 || $order->status == 301)
                    <p style="text-align: left">Макет готовит дизайнер</p>
                @else
                    @if ($order->img)
                        @if (filter_var($order->img, FILTER_VALIDATE_URL))
                            <img src="{{ $order->img }}" class="max-preview-img-size">
                        @else
                            @if (file_exists('lib/frx/'.$order->img))
                                <img style="text-align: left;" class="max-preview-img-size"
                                     src="/lib/frx/{{ $order->img }}" alt="Превью"/>
                            @else
                                <i class="fa fa-refresh fa-spin"></i>
                            @endif
                        @endif
                    @endif
                @endif
            </td>

            {{-- Дата выдачи --}}
            <td>{{ $order->price_details['close_date'] }}</td>

            {{-- Цена --}}
            <td>
                @if ($order->isRework())
                &mdash;
                @else
                    @if (isset($order->stopped_reasons['expenses']) && (($order->stopped_reasons['expenses']) >= 0))
                        @if ($order->status_info->id == 1010)
                            <strike>{{ $order->price }} руб.</strike><br>
                            Понесенные затраты<br> {{ $order->stopped_reasons['expenses'] }} руб.
                        @endif
                        @if ($order->status_info->id == 57)
                            <strike>{{ $order->price }} руб.</strike><br>
                            С возвратом за брак<br>
                            0 руб.
                        @endif
                        @if ($order->status_info->id == 58)
                            <strike>{{ $order->price }} руб.</strike><br>
                            Со скидкой за брак<br>
                            {{ $order->stopped_reasons['expenses']." руб." }}
                        @endif
                    @else
                        @if ($order->issue_troubles['discount'])
                            <strike>{{ $order->price }} руб.</strike><br>
                            Со скидкой за тираж<br>
                            {{ $order->price - $order->issue_troubles['discount'] }} руб.
                        @else
                            {{ $order->_price() }} руб.
                        @endif
                    @endif
                @endif
            </td>

            {{-- Статус --}}
            @if ($value->subdomain && $value->subdomain->name !== SubdomainEnvironment::currentSubdomain() && $order->status < 25)
                <td>
                    @if ($value->status->id <= 8 || $value->status->id == 500)
                        <div class="label label-primary">
                            Готовится к запуску
                        </div>
                    @endif
                </td>
            @else
                <td>
                    @if ($order->isRework())
                        <span class="label label-default">
                            Допечатка тиража
                        </span><br>
                    @endif
                    @if (! is_null($order->designer_working_starttime) && in_array($order->status, status(['self_designer', 'exclusive_design'], 'id')->toArray()))
                        <span class="label label-work">
                            {{ $order->designer_working ? 'В работе' : 'Приостановлено' }}
                        </span>
                        <br>
                    @endif
                    @if (!$order->isRework() && $order->reworked)
                        <span class="label l-reworked">Доработан</span><br>
                    @endif

                    @if (isset($order->rework_status_info))
                        <span class="label label-warning" style="background-color: {{ "#".$order->rework_status_info->color }} !important;">
                            {{ $order->rework_status_info->name }}
                        </span><br>
                    @endif
                    @if ($value->subdomain)
                        @if ($value->subdomain->name == SubdomainEnvironment::currentSubdomain())
                            @if ($order->status_info->id == 52)
                                <span class="label label-success">На выдаче</span><br>
                            @elseif ($order->status_info->id == 53)
                                <span class="label label-success">Выдан</span><br>
                            @else
                                <span class="label label-warning" style="background-color: {{ "#".$order->status_info->color }} !important;">{{ $order->status_info->name }}</span><br>
                            @endif
                        @else
                            @if ($value->status->id == 51)
                                <span class="label label-warning">Выдан</span><br>
                            @else
                                @if ($order->status_info->id == 51 || $order->status_info->id == 53)
                                    <span class="label label-warning">Выдан</span><br>
                                @elseif ($order->status == 45)
                                    <div>
                                        <span class="label label-warning control_quality" style="background-color: {{ "#".$order->status_info->color }} !important;"
                                        >Контроль качества @if (isset($order->control_quality)) пройден @endif</span>
                                    </div>
                                @else
                                    <span class="label label-warning" style="background-color: {{ "#".$order->status_info->color }} !important;">{{ $order->status_info->name }}</span><br>
                                @endif
                            @endif
                        @endif
                    @else
                        @if ($value->status->id == 51)
                            <span class="label label-warning">Выдан</span><br>
                        @else
                            @if ($order->with_contractor)
                                <div class="status-box">
                                    <span class="label label-warning" style="background-color: {{ "#".$order->status_info->color }} !important;">
                                        {{ $order->status_info->name }}
                                    </span>
                                </div><br>
                            @else
                                @if (($order->status == 56) && is_null($order->parent_id) && $order->issue_troubles && ($order->issue_troubles['issued'] > 0))
                                    <div>
                                        <span class="label label-warning" style="background-color: {{ "#".$order->status_info->color }} !important;">Выдан частично</span><br>
                                        {{ $order->issue_troubles['issued'] }} шт.
                                    </div>
                                @elseif ($order->status == 45)
                                    <div>
                                        <span class="label control_quality" style="background-color: {{ (isset($order->control_quality)) ? '#ef00d2' : "#".$order->status_info->color }} !important;"
                                        >Контроль качества @if (isset($order->control_quality)) пройден @endif</span>
                                    </div>
                                @else
                                    <div>
                                        <span class="label label-warning" style="background-color: {{ "#".$order->status_info->color }} !important;">
                                            {{ $order->status_info->name }}
                                        </span>
                                    </div>
                                @endif
                            @endif
                        @endif
                    @endif

                    @if ($order->client->email == $order->manager->email)
                        <span class="label label-warning" style="background-color: {{ "#".status('self_created', 'color') }} !important;">
                            {{ status('self_created', 'name') }}
                        </span><br>
                    @endif

                    {{ $order->payment_status }}

                    @if ($order->use_specification)
                        <?php $exclusiveDesignStatus = isset($exclusiveDesignStatus) ? $exclusiveDesignStatus : status('exclusive_design', 'id'); ?>
                        @if ($order->status == $exclusiveDesignStatus)
                            @if ($order->edit_specification)
                                <span class="label label-danger">ТЗ отредактировано</span><br>
                            @else
                                <span class="label tz-formed">ТЗ сформировано</span><br>
                            @endif
                        @endif
                    @endif
                </td>
            @endif

            @if ($value->subdomain && $value->subdomain->name !== SubdomainEnvironment::currentSubdomain() && $order->status < 45)
                <td></td>
            @else
                <td>
                    @if ($order->isRework())
                        @if ($order->status == status('ready')->id)
                            <form method="post" action="{{ route('rework.apply', $order->id) }}">
                                {{ method_field('patch') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary btn-block btn-xs">
                                    Принять допечатку
                                </button>
                            </form>
                        @endif
                    @else
                        @if (in_array($order->status_info->id, [2000, 2010]))
                            <a href="/orders/post-step2/{{$order->id}}">
                                <button style="margin-bottom: 5px"
                                        class="btn btn-block btn-info btn-xs">
                                    @if ($order->price_details['selected_prepress'] == 1)
                                        Выбрать шаблон
                                    @elseif ($order->price_details['selected_prepress'] == 5)
                                        Выбрать из архива
                                    @endif
                                </button>
                            </a>
                        @endif

                        @if ($order->status_info->id == 10)
                            <a href="/loading/{{random_int(1000, 9999).$order->id.random_int(1000, 9999)}}">
                                <button style="margin-bottom: 5px"
                                        class="btn btn-block btn-info btn-xs">
                                    Загрузить макет
                                </button>
                            </a>
                        @endif

                        @if ($order->status_info->id == 4)
                            <a href="/order/reviwer/0/{{ $order->id }}">
                                <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">
                                    Согласовать
                                </button>
                            </a>
                        @endif

                        @if (in_array($order->status_info->id, [5, 9]))
                            <?php /*<a href="/order/reviwer/send/repeated/{{ str_replace('.frx', '', $order->file) }}">
                                <button style="margin-bottom: 5px"
                                        class="btn btn-block btn-success btn-xs">
                                    Отправить повторно
                                </button>
                            </a> */?>

                            @if ($order->file)
                                <a href="/order/reviwer/0/{{ $order->id }}">
                                    <button style="margin-bottom: 5px"
                                            class="btn btn-block btn-success btn-xs">
                                        Утвердить на месте
                                    </button>
                                </a>
                            @else
                                <a href="/order/reviwer/0/{{ $order->id }}?editable=false">
                                    <button style="margin-bottom: 5px"
                                            class="btn btn-block btn-success btn-xs">
                                        Утвердить на месте
                                    </button>
                                </a>
                                @endif
                                @endif

                                        <!-- если только утвержден -->
                                @if (in_array($order->status_info->id, [2, 12, 102, 112]))
                                    @if (in_array($value->status->id, [6, 7, 8]))
                                        <button class="do_inwork btn btn-block btn-success btn-xs" data-id="{{ $value->id }}" data-orderid="{{ $order->id }}" style="margin-top: 5px;">
                                            Отправить в работу
                                        </button>
                                        <button class="do_stopped btn btn-block btn-danger btn-xs"
                                                data-id="{{ $value->id }}"
                                                data-orderid="{{ $order->id }}"
                                                data-loading-text="Подождите..."
                                                style="margin-top: 5px;"
                                        >
                                            Приостановить
                                        </button>
                                    @endif
                                @endif
                                @if (($order->status == 30) && ($order->with_contractor))
                                    <a href="/order/status/{{ $order->id }}/45">
                                        <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">
                                            Готов
                                        </button>
                                    </a>
                                @endif
                                @if (in_array($order->status_info->id, [25, 30, 35, 40]) || (($order->sort_order > 55) && ($order->status_info->id == 56)))
                                    @if ($value->client_status == 1 || ($value->client_status == 3 && $value->pay > 0) || ($value->client_status == 2 && ( $value->pay == $value->price)))
                                        <button class="do_stopped btn btn-block btn-danger btn-xs"
                                                data-id="{{ $value->id }}"
                                                data-orderid="{{ $order->id }}"
                                                data-loading-text="Подождите..."
                                                style="margin-top: 5px;"
                                        >
                                            Приостановить
                                        </button>
                                    @endif
                                @endif

                                @if ($order->status_info->id == 300)
                                    <a href="{{ route('order.specification.create', $order->id) }}">
                                        <button class="btn btn-block btn-info btn-xs" style="margin-bottom: 5px">
                                            {{ $order->use_specification ? 'Редактировать ТЗ' : 'Сформировать ТЗ' }}
                                        </button>
                                    </a>
                                @endif

                                @if ($order->status_info->id == 1000)
                                    @if ((!$order->template_changed && ($order->stopped_user_id && ($order->stopped_user_id == auth()->user()->id)))
                                        || ($order->template_changed && ($order->template_changed_user_id == auth()->user()->id))
                                        || ($order->template_changed && is_null($order->template_changed_user_id) && ($order->stopped_user_id == auth()->user()->id)))
                                        <button class="do_restore btn btn-block btn-resume btn-xs"
                                                data-id="{{ $value->id }}"
                                                data-orderid="{{ $order->id }}"
                                                style="margin-bottom: 5px;"
                                                @if ($order->stopped_user_id && $order->stopped_from != \App\Orders::STOPPED_FROM_HISTORY)
                                                    disabled="disabled"
                                                @endif
                                        >Возобновить</button>
                                        <button class="do_cancel btn btn-block btn-cancel2 btn-xs" data-id="{{ $value->id }}" data-orderid="{{ $order->id }}"
                                                style="margin-bottom: 5px"
                                        >Отменить</button>
                                    @endif
                                @endif

                                @if ($value->subdomain)
                                    @if ($value->subdomain->name === SubdomainEnvironment::currentSubdomain())
                                        @if ($order->status_info->id == 52)
                                            {{--<a href="/order/status/{{ $order->id }}/51">--}}
                                            {{--<button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs">--}}
                                            {{--Отправить на выдачу--}}
                                            {{--</button>--}}
                                            {{--</a>--}}
                                        @endif
                                    @endif
                                @else
                                    @if (($order->status_info->id == 45) && (is_null($order->control_quality)))
                                        <button style="margin-bottom: 5px;" class="btn btn-block send-delivery btn-xs btnSetCheckControlQuality"
                                                data-groupid="{{ $value->id }}" data-orderid="{{ $order->id }}"
                                            >Готов к отгрузке</button>
                                        <button style="margin-bottom: 5px" class="btn_rework btn btn-block send-rework btn-xs" data-id="{{ $value->id }}" data-orderid="{{ $order->id }}">
                                            Запустить на доработку
                                        </button>
                                    @endif
                                @endif

                                <? $btn_issue = false; ?>
                                @if ($value->subdomain)
                                    @if ($value->subdomain->name !== SubdomainEnvironment::currentSubdomain())
                                        @if ($order->status_info->id == 45)  <? $btn_issue = true; ?> @endif
                                    @else
                                        @if ($order->status_info->id == 51) <? $btn_issue = true; ?> @endif
                                    @endif
                                @else
                                    @if ($order->status_info->id == 51) <? $btn_issue = true; ?> @endif
                                @endif

                                @if ($btn_issue)
                                    <button style="margin-bottom: 5px" class="btn btn-block btn-success btn-xs btn_issue" data-guarantee-image="{{ $value->guarantee_image }}"
                                            data-group-status="{{ $value->status->id }}" data-group-order-id="{{ $value->id }}" data-order-id="{{ $order->id }}" data-status="52">Выдать</button>
                                @endif

                                @if ((!$value->subdomain) && ($order->status_info->id == status('sent_to_issue_point', 'id')))
                                    <a class="btn btn-block btn-success btn-xs btnSetCheckControlQuality"
                                       data-groupid="{{ $value->id }}" data-orderid="{{ $order->id }}"
                                       style="margin-bottom: 5px;">
                                        Доставлен на точку выдачи
                                    </a>
                                @endif


                                @if ($order->status_info->id == 52 && $value->status->id != 100)
                                    <a href="javascript: void(0);"
                                       onclick="$('.defect_return_form').hide(); $('.defect_return_form_{{ $order->id }}').slideDown();">
                                        <button style="margin-bottom: 5px" class="btn btn-block btn-danger btn-xs">
                                            Возврат по браку
                                        </button>
                                    </a>
                                @endif

                                @if ($order->status_info->id == 55)
                                    <button style="margin-bottom: 5px" class="btn_rework btn btn-block btn-success btn-xs" data-id="{{ $value->id }}" data-orderid="{{ $order->id }}">
                                        Запустить на доработку
                                    </button>
                                    <a href="javascript: void(0);"
                                       onclick="$('.defect_close_with_discount_form').hide(); $('.defect_close_with_discount_form_{{ $order->id }}').slideDown();">
                                        <button style="margin-bottom: 5px" class="btn btn-block btn-warning btn-xs">
                                            Закрыть со скидкой
                                        </button>
                                    </a>
                                    <a href="/order/status/{{ $order->id }}/57">
                                        <button style="margin-bottom: 5px" class="btn btn-block btn-danger btn-xs">
                                            Закрыть с возвратом денег
                                        </button>
                                    </a>
                                @endif
                            @endif
                </td>
            @endif
        </tr>
        @if (!$value->subdomain && in_array($order->status_info->id, [45, 55]))
            <tr class="rework_{{ $order->id }} rework_form row-border-left-right" style="display: none;">
                <td colspan="8">
                    <div class="panel box box-success">
                        <div class="panel-collapse collapse in">
                            <div class="box-body">
                                {!! Form::open(['url' => '/order/status_rework/'.$order->id, 'method' => 'post', 'autocomplete' => 'off']) !!}
                                <div class="col-xs-12">
                                    <div class="form-group" style="margin-bottom: 10px;">
                                        <label>Отправить на доработку в</label>
                                        <div class="radio">
                                            <label style="width: 100px">
                                                <input type="radio" name="rework_type" data-orderid="{{ $order->id }}" value="1" required>
                                                дизайн
                                            </label>
                                            <label style="width: 100px">
                                                <input type="radio" name="rework_type" data-orderid="{{ $order->id }}" value="3">
                                                печать
                                            </label>
                                            <label style="width: 100px">
                                                <input type="radio" name="rework_type" data-orderid="{{ $order->id }}" value="4">
                                                постпресс
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group rework_count" style="display:none; width:320px;">
                                        Перепечатать/допечатать <input type="text" class="form-control" name="rework_reason_count" min="1" max="{{ $order->draw }}"required style="width:120px; display:inline-block;"> шт.
                                    </div>
                                    <div class="form-group" style="dusplay:inline-block;">
                                        <input type="text" class="form-control" name="rework_reason" placeholder="комментарий" required style="display: inline-block">
                                    </div>
                                    {!! Form::submit('Отправить', ['class' => "btn btn-primary pull-left", 'style' => "margin-right: 5px;"]) !!}
                                    <button type="button"
                                            class="btn btn-default"
                                            data-action="hide-form"
                                            data-target=".rework_{{ $order->id }}"
                                    >
                                        Отмена
                                    </button>
                                    <br>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        @if ($btn_issue)
            <tr class="issue_draw_{{ $order->id }} issue_draw_form row-border-left-right" style="display: none;">
                <td colspan="8">
                    <div class="panel box box-success">
                        <div class="panel-collapse collapse in">
                            <div class="box-body">
                                {!! Form::open(['url' => '/order/status_issue', 'method' => 'post', 'autocomplete' => 'off']) !!}
                                <div class="col-xs-12">
                                    <div class="form-group" style="margin-bottom: 10px;">
                                        <input type="number" name="order_id" value="{{ $order->id }}" style="display:none;">
                                        <label style="width: 250px; font-weight: normal;">
                                            Выдано&nbsp
                                            <?php $need_to_issue = (isset($order->issue_troubles) && $order->issue_troubles['issued']) ? ($order->draw - $order->issue_troubles['issued']) : $order->draw; ?>
                                            <input type="number" name="issue_draw" data-temp-value="{{ $need_to_issue }}" value="{{ $need_to_issue }}" style="width:80px; text-align:center;">
                                            &nbspэкземпляров
                                        </label>
                                        <label class="issue_draw_trouble" style="width: 230px; font-weight: normal; display:none;">
                                            <input type="checkbox" name="issue_draw_to_print" value="1" style="vertical-align: top;">
                                            &nbspНеобходимо допечатать тираж
                                        </label>
                                        <div class="col-xs-5 input-group" style="display:none; margin-top:5px;">
                                            <div class="input-group-addon">Комментарий</div>
                                            <input class="form-control" type="text" name="issue_draw_commentary">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5 no-padding" style="margin-top:10px;">
                                                <div class="div_issue_discount_type" style="display:none;">
                                                    <div class="col-xs-4 radio" style="display: inline-block; margin:7px 0;">
                                                        <label><input type="radio" name="issue_discount_type" checked
                                                               style="margin-top:4px;" value="0"/>Пересчитать тираж</label>
                                                    </div>
                                                    <div class="col-xs-4 radio" style="display: inline-block; margin:7px 0; padding-left:0;">
                                                        <label><input type="radio" name="issue_discount_type"
                                                               style="margin-top:4px;" value="1"/>Предложить скидку</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4" style="display: inline-block; padding-left:0; padding-right:0;">
                                                    <div class="input-group" style="display:none;">
                                                        <input class="form-control" type="number" name="issue_draw_discount"
                                                               placeholder="">
                                                        <span class="input-group-addon">руб.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12" style="padding-left:0;">
                                        {!! Form::submit('Подтвердить', ['class' => "btn btn-primary pull-left", 'style' => "margin-right: 5px;"]) !!}
                                        <button type="button"
                                                class="btn btn-default"
                                                data-action="hide-form"
                                                data-target=".issue_draw_{{ $order->id }}"
                                        >
                                            Отмена
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        <tr class="inwork_{{ $order->id }} inwork_form row-border-left-right" style="display: none;">
            <td colspan="8">
                <div class="panel box box-success">
                    <div class="panel-collapse collapse in">
                        <div class="box-body">
                            {!! Form::open(['url' => '/order/inwork/'.$order->id, 'method' => 'post', 'autocomplete' => 'off']) !!}
                            <div class="col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <div class="radio">
                                        <label style="width: 180px">
                                            <input type="radio" name="hwodo" data-orderid="{{ $order->id }}" id="optionsRadios1" value="1" checked="">
                                            Своё производство
                                        </label>
                                        <label style="width: 210px">
                                            <input type="radio" name="hwodo" data-orderid="{{ $order->id }}" id="optionsRadios2" value="2">
                                            Производство подрядчика
                                        </label>
                                    </div>
                                </div>

                                <div class="row inwork_not_my_{{ $order->id }}" style="margin: 25px -15px 5px -15px; display: none">
                                    <div class="col-xs-9 no-padding">
                                        <div class="col-xs-5">
                                            Согласованная стоимость работ
                                            {!! Form::number('summ_worker', '', ['class' => 'form-control', 'placeholder' => '']) !!}
                                        </div>
                                        <div class="col-xs-4">
                                            Дата исполнения подряда
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('close_date', null, ['class' => 'form-control datepicker', 'style' => 'width: 162px;float: left']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="margin-top:15px;">
                                        Выберите или добавьте подрядчика
                                        <div class="input-group">
                                            {!! Form::select('contractor_id', $contractorsSelect, 1, ['class' => 'form-control', 'style' => 'width:100%']) !!}
                                            <a href="#" class="input-group-btn addContractorBtn" data-order-id="{{ $order->id }}">
                                                {!! Form::button('<i class="fa fa-plus"></i>', ['class' => 'btn btn-primary btn-md']) !!}
                                            </a>
                                        </div><!-- /input-group -->
                                    </div>
                                </div>

                                <br>
                                {!! Form::submit('Отправить в работу', ['class' => "btn btn-primary pull-left", 'style' => "margin-right: 5px;"]) !!}
                                <button type="button"
                                        class="btn btn-default"
                                        data-action="hide-form"
                                        data-target=".inwork_{{ $order->id }}"
                                >
                                    Отмена
                                </button>
                                <br><br>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        @if (in_array($order->status_info->id, [2, 12, 25, 30, 35, 40, 56, 102, 112, 1000, 1010]))
            <tr class="stopped_form_{{ $order->id }} stopped_form row-border-left-right" style="display: none;">
                <td colspan="10">
                    <div class="panel box box-success">
                        <div class="box-header with-border stopped-form-commentary-title">
                            <h4 class="box-title">Комментарий</h4>
                        </div>

                        <div class="panel-collapse collapse in">
                            <div class="box-body">
                                <form method="POST" action="/order/stopped/{{ $order->id }}/0"
                                      onsubmit="return validate_form_reason ( this );"
                                      enctype="multipart/form-data" autocomplete="off">
                                    {!! csrf_field() !!}
                                    <input type="hidden" class="form-control"
                                           id="stopped_state_{{ $order->id }}"
                                           name="stopped_state_{{ $order->id }}"
                                           value=""
                                    >

                                    <input type="hidden"
                                           name="stopped_from"
                                           value="{{ \App\Orders::STOPPED_FROM_HISTORY }}"
                                    >

                                    <div class="form-group" style="margin-bottom: 5px;">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="pay-message form-group" style="display: none;">
                                                    {{ $order->totalExpense() }}
                                                </div>
                                                <div class="form-group stopped-form-commentary" >
                                                    <input type="text" class="form-control" name="stopped_reason" required>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="accept-message btn btn-primary">
                                                        Подтвердить
                                                    </button>

                                                    <button type="button"
                                                            class="cancel-message btn btn-default"
                                                            data-action="hide-form"
                                                            data-target=".stopped_form_{{ $order->id }}">
                                                        Отмена
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endif

        @if ($order->status_info->id == 52 && $value->status->id != 100)
            <tr class="defect_return_form_{{ $order->id }} defect_return_form row-border-left-right"
                style="display: none;">
                <td colspan="9">
                    <div class="panel box box-success">
                        <div class="box-header with-border">
                            <h4 class="box-title">Комментарий</h4>
                        </div>

                        <div class="panel-collapse collapse in">
                            <div class="box-body">
                                <form method="POST"
                                      action="/order/status_defect_return/{{ $order->id }}"
                                      onsubmit="return validate_form_reason ( this );"
                                      enctype="multipart/form-data" autocomplete="off">
                                    {!! csrf_field() !!}

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="defect_return_reason" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"
                                        >Подтвердить</button>

                                        <button type="button"
                                                class="btn btn-default"
                                                data-action="hide-form"
                                                data-target=".defect_return_form_{{ $order->id }}"
                                        >Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endif

        @if ($order->status_info->id == 55)
            <tr class="defect_close_with_discount_form_{{ $order->id }} defect_close_with_discount_form row-border-left-right"
                style="display: none;">
                <td colspan="9">
                    <div class="panel box box-success">
                        <div class="box-header with-border">
                            <h4 class="box-title">Закрыть со скидкой</h4>
                        </div>

                        <div class="panel-collapse collapse in">
                            <div class="box-body">
                                <form method="POST" action="/order/status_defect_close_with_discount/{{ $order->id }}"
                                      onsubmit="return validate_form_close_with_discount ( this );"
                                      enctype="multipart/form-data" autocomplete="off">
                                    {!! csrf_field() !!}
                                    <div class="col-xs-12">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                            <div class="col-xs-5">
                                                <div class="col-xs-12 input-group">
                                                    <div class="input-group-addon">Согласована скидка</div>
                                                    <input type="number" class="form-control" name="defect_close_with_discount_price"
                                                           placeholder="">
                                                    <div class="input-group-addon">руб.</div>
                                                </div>
                                                <div class="col-xs-12 input-group" style="margin-top: 10px;">
                                                    <div class="input-group-addon" style="width:156px;">Комментарий</div>
                                                    <input type="text" class="form-control" name="defect_close_with_discount_comment"
                                                           placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-xs-3" style="margin-top:44px;">
                                                <button type="submit" class="btn btn-primary pull-left"
                                                        style="margin-right: 5px;">
                                                    Подтвердить
                                                </button>

                                                <button type="button" class="btn btn-default" data-action="hide-form" data-target=".defect_close_with_discount_form_{{ $order->id }}">
                                                    Отмена
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endif
    @endforeach
    <tr class="orders_list_{{ $value->id }} orders_list_item"
        style="@if (($value->id != $selected_group_order_id) && !$opened) display: none; @endif border-bottom:1px solid#f4f4f4 !important">
        <td colspan="9" style="border-top: 1px black solid;padding-top: 0px;padding-bottom: 0px;"></td>
    </tr>
@endforeach