@extends('app')

@section('content')
    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th width="150">Название</th>
                            <th width="150">Цвет</th>
                            <th width="150">Коммент</th>
                        </tr>
                        <?php $i = 0;?>
                        @foreach ($order_statuses as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->color }}</td>
                                <td>{{ $value->text }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! (new Landish\Pagination\Pagination($order_statuses))->render() !!}

                </div>
            </div>
        </div>
    </div>
    </section>
@stop