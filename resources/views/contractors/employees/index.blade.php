@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg @if (!$errors->has()) collapsed @endif"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse" required
                        data-target="#collapsed_container">Добавить нового сотрудника
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>

                    @if ($errors->has())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    @include("contractors.employees.create_form")
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Подразделение</th>
                    <th>Должность</th>
                    <th>Аватар</th>
                    <th>ФИО</th>
                    <th>Контакты</th>
                    <th>Дата регистрации</th>
                    <th>Действия</th>
                </tr>
                </thead>

                <tbody>
                @foreach($employees as $person)
                    <tr>

                        <td>{{ $person->id }}</td>

                        {{-- Подразделение --}}
                        <td>{{ $person->group->name }}</td>

                        {{-- Должность --}}
                        <td>{{ $person->position }}</td>

                        {{-- Аватар --}}
                        <td>
                            @if ($person->photo)
                                <img src="{{ $person->photo }}" style="vertical-align: top; max-width: 90px">
                            @endif
                        </td>

                        {{-- ФИО --}}
                        <td>{{ $person->contact->fullName }}</td>

                        {{-- Контакты --}}
                        <td>
                            @if ($person->contact)
                                <div>{{ $person->contact->phone }}</div>
                                <div>{{ $person->contact->email }}</div>
                            @endif
                        </td>

                        {{-- Дата регистрации --}}
                        <td>{{ $person->created_at->format('d.m.Y H:i') }}</td>

                        {{-- Действия --}}
                        <td>
                            <a href="{{ route('employee.salary', $person) }}" class="btn btn-default btn-block btn-xs">
                                Зарплата
                            </a>

                            <a href="#" class="btn btn-primary btn-block btn-xs">
                                Карточка сотрудника
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            {!! (new Landish\Pagination\Pagination($employees))->render() !!}
        </div>
    </div>
@endsection
