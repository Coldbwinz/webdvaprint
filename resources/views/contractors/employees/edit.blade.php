@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body">
            <employee-edit-form :employee-id="{{ $employee->id }}"></employee-edit-form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection

