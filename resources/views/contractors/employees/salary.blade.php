@extends('app')

@section('content')
    <div id="root">
        <div class="row">
            <div class="col-md-7">
                <employee-add-bonus-form :employee-id="{{ $employee->id }}"></employee-add-bonus-form>
            </div>
        </div>

        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-4">
                        <salary-filter-form/>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Событие</th>
                        <th>Наименование</th>
                        <th>Клиент</th>
                        <th>Стоимость</th>
                        <th>Оплачено</th>
                        <th>Тип оплаты</th>
                        <th>Остаток</th>
                        <th>Расчёт до</th>
                        <th>Начислено</th>
                        <th>К получению</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($operations as $operation)
                        @if ($operation->is_header)
                            <tr class="info">
                                <td colspan="12">{!! $operation->action !!}</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{ $operation->created_at }}</td>
                                <td>{{ $operation->action }}</td>
                                <td>{{ $operation->name }}</td>
                                <td>
                                    @if ($operation->client)
                                        {{ $operation->client->type->name }}
                                    @endif
                                </td>
                                <td>{{ $operation->value }}</td>
                                <td>{{ $operation->paid_sum }}</td>
                                <td>{{ $operation->payment_type }}</td>
                                <td>{{ $operation->balance }}</td>
                                <td>
                                    @if ($operation->calculation_to)
                                        {{ $operation->calculation_to->format('d.m.Y') }}
                                    @endif
                                </td>
                                <td>{{ $operation->accrued }}</td>
                                <td>{{ $operation->receivable }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>

                {!! $operations->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
