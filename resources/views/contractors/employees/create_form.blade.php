{!! Form::open(['route' => 'contractors.employees.store', 'method' => 'post']) !!}

    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::text('lastname', old('lastname'), ['placeholder' => 'Фамилия', 'class' => 'form-control', 'required' => true]) !!}
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    {!! Form::text('name', old('name'), ['placeholder' => 'Имя (Отчество)', 'class' => 'form-control', 'required' => true]) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select name="id_group" class="form-control" required>
                        <option disabled selected hidden value="">Подразделение</option>
                        @foreach ($userGroups as $key => $group)
                            <option value="{{ $key }}">{{ $group }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    {!! Form::text('position', old('position'), ['placeholder' => 'Должность', 'class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group form-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    {!! Form::email('email', old('email'), ['placeholder' => 'E-mail', 'class' => 'form-control', 'required' => true]) !!}
                </div>
                <div class="input-group form-group">
                    <span required class="input-group-addon"><i class="fa fa-phone"></i></span>
                    {!! Form::text('phone', old('phone'), ['placeholder' => 'Телефон', 'class' => 'form-control', 'id' => 'c-phone', 'required']) !!}
                </div>
                <div class="input-group form-group">
                    <span required class="input-group-addon"><i class="fa fa-lock"></i></span>
                    {!! Form::password('password', ['placeholder' => 'Пароль', 'class' => 'form-control', 'required' => true, 'tabindex' => 100]) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::textarea('comment', old('comment'), ['placeholder' => 'Комментарий', 'class' => 'form-control comments']) !!}
                </div>
                <div class="input-group form-group">
                    <span required class="input-group-addon"><i class="fa fa-lock"></i></span>
                    {!! Form::password('confirm_password', ['placeholder' => 'Повтор пароля', 'class' => 'form-control', 'required' => true, 'tabindex' => 110]) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}
