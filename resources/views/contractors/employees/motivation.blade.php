@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body">
            <edit-motivation-form :group-id="{{ \App\UserGroup::MANAGER }}"></edit-motivation-form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
