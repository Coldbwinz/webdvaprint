@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="box">
                <button type="button" class="btn btn-lg @if (!$errors->has()) collapsed @endif"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse" required
                        data-target="#collapsed_container"> Добавить нового партнёра
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>

                    @if ($errors->count())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    {!! Form::open(['route' => 'partner.store', 'class' => 'client-form']) !!}
                        <div class="box-body">
                            @include("contractors.partners.create-form")
                        </div>

                        <div class="box-footer">
                            <button type="submit" name="action" value="create" class="btn btn-primary">
                                Создать
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Аватар</th>
                        <th>Название</th>
                        <th>Поддомен</th>
                        <th>Контактное лицо</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Дата регистрации</th>
                        <th>Средний чек</th>
                        @if (Auth::user()->accessLevel() > 2) <th>Менеджер</th> @endif
                    </tr>
                </thead>

                <tbody>
                @foreach($partners as $partner)
                    <tr>
                        <td>{{ $partner->id }}</td>

                        <td>
                            <img src="{{ asset($partner->picture) }}" height="80"/>
                        </td>

                        {{-- Название компании --}}
                        <td>{{ $partner->tm_name }}</td>

                        {{-- Поддомен --}}
                        <td>
                            @if ($partner->account && $partner->account->subdomain)
                                {{ $partner->account->subdomain->name }}.w2p.me
                            @endif
                        </td>

                        {{-- Контакты --}}
                        <td>
                            @if ($partner->contacts->count())
                                <p>
                                    @foreach($partner->contacts as $person)
                                        {{ $person->fullName }}
                                    @endforeach
                                </p>
                            @endif
                        </td>

                        {{-- phones --}}
                        <td>
                            @if ($partner->contacts)
                                @foreach($partner->contacts as $contact)
                                    <div><a class="contact-phone">{{ $contact->phone }}</a></div>
                                @endforeach
                            @endif
                        </td>

                        {{-- email --}}
                        <td>
                            @if ($partner->contacts)
                                @foreach($partner->contacts as $contact)
                                    <div><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></div>
                                @endforeach
                            @endif
                        </td>

                        {{-- Дата создания --}}
                        <td>{{ $partner->created_at->format('d.m.Y H:i') }}</td>

                        {{-- Средний чек --}}
                        <td>0 руб.</td>

                        {{-- Менеджер --}}
                        @if (Auth::user()->accessLevel() > 2)
                            <td>
                                @if ($partner->manager)
                                    <a href="{{ route('contact.show', [$partner->contacts->first()->id, $partner->id]) }}">
                                        {{ $partner->manager->fullName }}
                                    </a>
                                @endif
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            {!! $partners->render() !!}
        </div>
    </div>
@endsection
