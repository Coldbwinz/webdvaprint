<div class="row">
    <div class="col-md-4 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
    </div>
    <div class="col-md-8 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя (Отчество)']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <label>
            <input type="radio" name="is_company" class="minimal" value="0" checked="" disabled>
            &nbsp;&nbsp;Частное лицо
        </label>

        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
            <?php
                $position = '';
                if (isset($client) && $client->contacts->count()) {
                    $position = $client->contacts->first()->getInformationFieldForClient($client->id, 'position');
                }
            ?>
            <input class="form-control"
                   type="text"
                   name="position"
                   placeholder="Должность"
                   value="{{ old('position', $position) }}"
            />
        </div>
    </div>

    <div class="col-md-8">
        <label>
            <input type="radio" name="is_company" class="minimal" value="1" checked>
            &nbsp;&nbsp;Юридическое лицо
        </label>

        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
            <input class="form-control"
                   type="text"
                   name="company_name"
                   placeholder="Компания"
                   value="{{ old('company_name', isset($client) ? $client->tm_name : '') }}"
            />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
        <div class="form-group">
            <div class="input-group{{ $errors->has('subdomain') ? ' has-error' : '' }}">
                <input type="text"
                       class="form-control text-right"
                       placeholder="Поддомен"
                       name="subdomain"
                       value="{{ old('subdomain') }}"
                />
                <span class="input-group-addon">.w2p.me</span>
            </div>
        </div>

        <div class="input-group form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <span required class="input-group-addon"><i class="fa fa-phone"></i></span>

            <input name="phone"
                   type="text"
                   class="form-control"
                   id="c-phone"
                   value="{{ old('phone', isset($client) ? $client->contacts->first()->phone : '') }}"
                   placeholder="Телефон"
                   required
            />
        </div>

        <div class="input-group form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input required
                   class="form-control"
                   type="email"
                   name="email"
                   value="{{ old('email', isset($client) ? $client->contacts->first()->email : '') }}"
                   placeholder="E-mail"
            />
        </div>
    </div>

    <div class="col-md-5">
        <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                    <textarea class="form-control"
                              placeholder="Комментарий"
                              name="comment"
                              style="height: 132px;"
                    >{{ old('comment', isset($client) ? $client->description : '') }}</textarea>
        </div>
    </div>
</div>
