@extends('app')

@section('content')
    <div class="box">
        <div class="box-body">
            @if ($errors->count())
                <div class="alert alert-danger">
                    <p>При отправке формы возникли следующие проблемы:</p>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('intermediary.store') }}" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="control-label">Фамилия</label>
                            <input type="text" id="last_name" name="last_name" value="{{ old('last_name') }}" class="form-control"/>

                            @if ($errors->has('last_name'))
                                <div class="help-block">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Имя Отчество</label>
                            <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control"/>

                            @if ($errors->has('name'))
                                <div class="help-block">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('subdomain') ? ' has-error' : '' }}">
                    <label for="subdomain" class="control-label">Поддомен</label>
                    <input type="text" id="subdomain" name="subdomain" value="{{ old('subdomain') }}" class="form-control"/>
                    @if ($errors->has('subdomain'))
                        <div class="help-block">{{ $errors->first('subdomain') }}</div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">Логин (e-mail)</label>
                            <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control"/>
                            @if ($errors->has('email'))
                                <div class="help-block">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="control-label">Номер телефона</label>
                            <input type="text" id="phone" name="phone" value="{{ old('phone') }}" class="form-control"/>
                            @if ($errors->has('phone'))
                                <div class="help-block">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Пароль</label>
                    <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control"/>
                    @if ($errors->has('password'))
                        <div class="help-block">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password_confirmation" class="control-label">Повтор пароля</label>
                    <input type="password" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control"/>
                    @if ($errors->has('password_confirmation'))
                        <div class="help-block">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="control-label">Комментарий</label>
                    <textarea name="description" id="description" class="form-control" rows="5">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <div class="help-block">{{ $errors->first('description') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <button class="btn bnt-primary btn-lg" type="submit">
                        Создать
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
