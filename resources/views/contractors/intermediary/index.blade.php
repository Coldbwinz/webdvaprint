@extends('app')

@section('content')
    <div class="box">
        <div class="box-header">
            <a href="{{ route('intermediary.create') }}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Добавить посредника
            </a>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Пользователь</th>
                        <th>Контакт</th>
                        <th>Поддомен</th>
                        <th>Дата создания</th>
                        <th>Действия</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($intermediaries as $intermediary)
                        <tr>
                            <td>{{ $intermediary->id }}</td>
                            <td>
                                <a href="{{ route('user.edit', $intermediary->user->id) }}" target="_blank">
                                    {{ $intermediary->user->email }}
                                </a>
                            </td>
                            <td>
                                <a href="#" target="_blank">
                                    {{ $intermediary->contact->fullName }}
                                </a>
                            </td>
                            <td>{{ $intermediary->subdomain }}</td>
                            <td>{{ $intermediary->created_at->format('d.m.Y H:i') }}</td>
                            <td>
                                <a href="{{ route('intermediary.edit', $intermediary->id) }}"
                                   class="btn btn-default btn-sm"
                                   title="Изменить запись"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            {!! $intermediaries->render() !!}
        </div>
    </div>
@endsection
