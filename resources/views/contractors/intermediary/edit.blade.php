@extends('app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h2>Редактирование посредника #{{ $intermediary->id }}</h2>
        </div>

        <div class="box-body">
            @if ($errors->count())
                <div class="alert alert-danger">
                    <p>При отправке формы возникли следующие проблемы:</p>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('intermediary.update', $intermediary->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('patch') }}

                <div class="form-group">
                    <label class="control-label">Пользователь</label>
                    <p class="form-control-static">
                        <a href="{{ route('user.edit', $intermediary->contact->user->id) }}">
                            {{ $intermediary->contact->user->email }}
                        </a>
                    </p>
                </div>

                <div class="form-group">
                    <label class="control-label">Контакт</label>
                    <p class="form-control-static">
                        <a href="#">
                            {{ $intermediary->contact->fullName }}
                        </a>
                    </p>
                </div>

                <div class="form-group{{ $errors->has('subdomain') ? ' has-error' : '' }}">
                    <label for="subdomain" class="control-label">Поддомен</label>
                    <input type="text" id="subdomain" name="subdomain" value="{{ old('subdomain', $intermediary->subdomain) }}" class="form-control"/>
                    @if ($errors->has('subdomain'))
                        <div class="help-block">{{ $errors->first('subdomain') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <button class="btn bnt-primary btn-lg" type="submit">
                        Сохранить
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
