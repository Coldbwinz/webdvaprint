@extends('pdfviewer')

@section('content')
    <div class="edit-content">
        <img src="/lib/frx/{{ $order->img }}">
        <!--div class="pdfviewer" id="PDFViewer"></div-->
    </div>
    <div class="aside-right contractor-order">
        <h3>Информация о заказе</h3>

        <div class="row">
            <div class="col-sm-4"><label>No:</label></div>
            <div class="col-sm-8">{{ $order->group_id }}.{{ $order->position }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Продукт:</label></div>
            <div class="col-sm-8">{{ $order->productType->name }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Цвет:</label></div>
            <div class="col-sm-8">{{ $order->price_details['selected_chromacity'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Формат:</label></div>
            <div class="col-sm-8">{{ $order->price_details['selected_size'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Материал:</label></div>
            <div class="col-sm-8">{{ $order->price_details['selected_product'] }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Тираж:</label></div>
            <div class="col-sm-8">{{ $order->draw }} шт.</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Дата выдачи:</label></div>
            <div class="col-sm-8">{{ $job->deadline->format('d.m.y') }}</div>
        </div>
        <div class="row">
            <div class="col-sm-4"><label>Стоимость:</label></div>
            <div class="col-sm-8">{{ intval($job->price) }} руб.</div>
        </div>
        <div class="row text-center">
            <div class="col-sm-12">
                <a href="{{ route('contractor.order.download', ['token' => $token->token]) }}" class="btn btn-primary">Скачать макет</a>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>PDFObject.embed("/lib/frx/{{ $order->pdf }}", "#PDFViewer", {

    });</script>
@endsection
