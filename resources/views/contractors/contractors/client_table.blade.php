@foreach($clients as $client)
    <tr>
        <td>{{ $client->id }}</td>

        <td>
            @if ($client->is_company)
                <img src="{{ asset($client->picture) }}" height="80"/>
            @else
                @if ($client->contacts->count())
                    <img src="{{ asset($client->contacts->first()->avatar) }}"
                         class="image-rounded"
                         onerror="this.src='/images/nophoto.png'"
                         width="80"
                    />
                @endif
            @endif
        </td>

        <td>
            @if ($client->isCompany())
                <div>{{ $client->tm_name }}</div>
            @else
                <div>Частное лицо</div>
            @endif

            @if ($client->type)
                <span class="label label-default">
                                    {{ $client->type->name }}
                                </span>
            @endif
        </td>
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <div style="padding-right: 10px;min-width: 200px;">
                        <a class="contact-link"
                           href="{{ route('contact.show', [$contact->id, $client->id]) }}">
                            {{ $contact->full_name }}
                        </a>
                    </div>
                @endforeach
            @endif
        </td>

        {{-- phones --}}
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <div><a class="contact-phone">{{ $contact->phone }}</a></div>
                @endforeach
            @endif
        </td>

        {{-- email --}}
        <td>
            @if ($client->contacts)
                @foreach($client->contacts as $contact)
                    <div><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></div>
                @endforeach
            @endif
        </td>

        {{-- Дата регистрации --}}
        <td>{{ $client->created_at->format('d.m.Y H:i') }}</td>

        {{-- Средний чек --}}
        <td>0 руб.</td>

        {{-- Менеджер --}}
        @if (Auth::user()->accessLevel() > 2)
            <td>
                @if ($client->manager)
                    <a href="{{ route('contact.show', [$client->contacts->first()->id, $client->id]) }}">
                        {{ $client->manager->fullName }}
                    </a>
                @endif
            </td>
        @endif
    </tr>
@endforeach