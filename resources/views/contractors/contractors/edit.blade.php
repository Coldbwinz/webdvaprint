@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование подрядчика</h3>
                </div><!-- /.box-header -->                
                <div class="box-body">

                    @if ($errors->has())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    @include("contractors.contractors._form")
                </div>
            </div>
        </div>
    </div>
@endsection
