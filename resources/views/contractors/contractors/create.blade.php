@extends('app')

@section('content')
    <div class="box">
        <div class="box-body">
            <form action="{{ route('contractors.contractors.store') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="control-label">ФИО</label>
                    <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control"/>

                    @if ($errors->has('name'))
                        <div class="help-block">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                @include('contractors.contractors.partials.contacts_select', ['selected' => []])

                <div class="form-group">
                    <button class="btn bnt-primary btn-lg" type="submit">
                        Создать
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
