@if (empty($contractor))
    {!! Form::open(['route' => 'contractors.contractors.store', 'method' => 'POST', 'class' => 'client-form', 'autocomplete' => 'off']) !!}
@else
    {!! Form::model($contractor, ['action' => array('Contractors\ContractorsController@update', $contractor->id), 'method' => 'PATCH', 'class' => 'client-form', 'autocomplete' => 'off']) !!}
@endif
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 form-group">
                {!! Form::text('last_name',
                        (!empty($contact) ? $contact->last_name : null),
                        ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
            </div>
            <div class="col-md-8 form-group">
                {!! Form::text('name',
                        (!empty($contact) ? $contact->name : null),
                        ['class' => 'form-control', 'placeholder' => 'Имя (Отчество)', 'required' => true]) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="is_person_value">
                    {!! Form::radio('is_company', 0,
                            (!empty($contact) && false == $contact->is_company ? true : old('is_company') == 0) ,
                            ['class' => 'minimal', 'id' => 'is_person_value',
                             'onclick' => "$('.company_block').hide();"]) !!}
                    &nbsp;
                    Частное лицо
                </label>
                <div class="form-group company_block display-none" @if (old('is_company') == 1 || (!empty($contact) && $contact->is_company)) style="display: block" @endif>
                    {!! Form::text('position',
                            (!empty($contact) ? $contact->position : null),
                            ['class' => 'form-control', 'placeholder' => 'Должность']) !!}
                </div>
            </div>
            <div class="col-md-8">
                <label for="is_company_value">
                    {!! Form::radio('is_company', 1,
                            (!empty($contact) && true == $contact->is_company ? true : old('is_company') == 1),
                            ['class' => 'minimal', 'id' => 'is_company_value',
                             'onclick' => "$('.company_block').show();"]) !!}

                    &nbsp;
                    Юридическое лицо
                </label>
                <div class="form-group company_block display-none" @if (old('is_company') == 1 || (!empty($contact) && $contact->is_company)) style="display: block" @endif>
                    @if (old('is_company') == 0)
                        {!! Form::text('company_name',
                                (!empty($contractor) ? $contractor->company_name : null),
                                ['class' => 'form-control', 'placeholder' => 'Компания']) !!}
                    @else
                        {!! Form::text('company_name',
                                (!empty($contractor) ? $contractor->company_name : null),
                                ['class' => 'form-control', 'placeholder' => 'Компания', 'required' => true]) !!}
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="input-group form-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    {!! Form::text('phone',
                            (!empty($contact) ? $contact->phone : null),
                            ['class' => 'form-control', 'placeholder' => 'Телефон', 'required' => true]) !!}
                </div>

                <div class="input-group form-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    {!! Form::email('email',
                            (!empty($contact) ? $contact->email : null),
                            ['class' => 'form-control', 'placeholder' => 'E-mail', 'required' => true]) !!}
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    {!! Form::textarea('description',
                            (!empty($contact) ? $contact->description : null),
                            ['class' => 'form-control', 'placeholder' => 'Комментарий']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        @if (!is_null($order) && empty($contractor))
            {!! Form::submit('Создать заказ', ['class' => 'btn btn-primary']) !!}
            {!! Form::hidden('order_id', $order->id) !!}
            {!! Form::hidden('summ_worker', $summ_worker) !!}
            {!! Form::hidden('close_date', $close_date) !!}
        @endif
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}

<script type="text/javascript">
    $('input[name=is_company]').click(function() {
        var vis = $(this).val() == 1;
        $('input[name=company_name]').prop('required', vis);
    });
</script>
