<div class="form-group{{ $errors->has('contacts') ? ' has-error' : '' }}">
    <label for="contacts" class="control-label">Контакты</label>

    <select name="contacts[]" id="contacts" class="form-control" multiple>
        @foreach($contacts as $contact)
            <option value="{{ $contact->id }}"{{ in_array($contact->id, old('contacts', $selected)) ? ' selected' : '' }}>
                {{ $contact->name }}
            </option>
        @endforeach
    </select>

    @if ($errors->has('contacts'))
        <div class="help-block">{{ $errors->first('contacts') }}</div>
    @endif
</div>
