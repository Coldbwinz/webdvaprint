@extends('app')

@section('styles')
<style>
    .btn.btn-lg:after {
        font-family: "Glyphicons Halflings";
        content: "\e114";
        float: right;
        margin-left: 15px;
    }

    .btn.btn-lg.collapsed:after {
        content: "\e080";
    }
</style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg @if (!$errors->has()) collapsed @endif"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse" required
                        data-target="#collapsed_container">Добавить нового подрядчика
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>

                    @if ($errors->has())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    @include("contractors.contractors._form")
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Компания</th>
                        <th>Контактное лицо</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Выполнено заказов</th>
                        <th>Заказы с браком</th>
                        <th>В работе</th>
                        <th>Балланс</th>
                        <th>Действия</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($contractors as $contractor)
                        <tr>
                            <td>{{ $contractor->id }}</td>
                            <td>{{ $contractor->is_company ? $contractor->company_name : 'Частное лицо' }}</td>
                            <td>
                                @if ($contractor->contacts)
                                    @foreach($contractor->contacts as $contact)
                                        <div>{{ $contact->name }} {{ $contact->last_name }}</div>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                @if ($contractor->contacts)
                                    @foreach($contractor->contacts as $contact)
                                        <div>{{ $contact->phone }}</div>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                @if ($contractor->contacts)
                                    @foreach($contractor->contacts as $contact)
                                        <div>{{ $contact->email }}</div>
                                    @endforeach
                                @endif
                            </td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td class="text-center">0</td>
                            <td>
                                <a href="{{ route('contractors.contractors.edit', $contractor->id) }}"
                                   class="btn btn-default btn-sm"
                                   title="Изменить запись"
                                >
                                    <i class="fa fa-pencil"> Редактировать</i>
                                </a>
                                <?php /*
                                <a href="#" class="btn btn-danger btn-sm" title="Удалить запись" onclick="if(confirm('Действительно удалить эту запись?'))document.getElementById('delete-contractor-form-{{ $contractor->id }}').submit();return false;">
                                    <i class="fa fa-trash"></i>
                                </a>

                                <form id="delete-contractor-form-{{ $contractor->id }}"
                                      action="{{ route('contractors.contractors.destroy', $contractor->id) }}"
                                      method="post"
                                      class="hidden"
                                >
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form> */?>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            {!! $contractors->render() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @if ($open_form)
        <script>
            $(document).ready(function() {
                $('#collapsed_container').collapse('show');
            })
        </script>
    @endif
@endsection
