<form method="POST" action="{{ route('client.new') }}" autocomplete="off" enctype="multipart/form-data" class="client-form" id="newClientForm">
    {!! csrf_field() !!}
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 form-group">
                <input class="form-control" type="text" name="lastname" placeholder="Фамилия" value="{{ old('lastname') }}"/>
            </div>
            <div class="col-md-8 form-group">
                <input class="form-control" type="text" required name="name" placeholder="Имя (Отчество)"
                       value="{{ old('name') }}">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="is_person_value">
                    <input type="radio" name="is_company" onclick="$('.company_block').hide();"
                           class="minimal" value="0" checked="" id="is_person_value">
                    &nbsp;&nbsp;Частное лицо
                </label>
                <div class="form-group company_block"
                    @if (old('is_company') == 0) style="display: none" @endif>
                    <input class="form-control" type="text" name="position" placeholder="Должность"
                            value="{{ old('position') }}">
                </div>
            </div>
            <div class="col-md-8">
                <label for="is_company_value">
                    <input type="radio" name="is_company" onclick="$('.company_block').show();"
                           class="minimal" value="1" id="is_company_value">
                    &nbsp;&nbsp;Юридическое лицо
                </label>
                <div class="form-group company_block"
                    @if (old('is_company') == 0) style="display: none" @endif>
                    <input class="form-control" type="text" name="company_name" placeholder="Компания"
                            value="{{ old('company_name') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="input-group form-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                    <input name="phone"
                           type="text"
                           class="form-control"
                           id="c-phone"
                           value="{{ (Request::input('phone') === null) ? old('phone') : Request::input('phone') }}"
                           placeholder="Телефон"
                    />
                </div>

                <div class="input-group form-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input required
                           class="form-control"
                           type="email"
                           name="email"
                           value="{{ old('email') }}"
                           placeholder="E-mail"
                    />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <textarea class="form-control"
                              placeholder="Комментарий"
                              name="comment"
                    >{{ old('comment') }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select name="client_type" class="client_type form-control" id="client_type" required>
                        <option value="" disabled hidden @if (!in_array(old('client_type'), [1, 3])) selected @endif >Тип клиента</option>
                        <option value="1" @if (old('client_type') == 1) selected @endif>Заказчик</option>
                        <option value="3" @if (old('client_type') == 3) selected @endif>Посредник</option>
                    </select>
                </div>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" name="action" value="save_and_order" class="btn btn-primary">Создать заказ</button>
        <button type="submit" name="action" value="save_and_show" class="btn btn-default">Перейти в карточку клиента</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        // validate client_type
        var field = document.getElementById('client_type');
        field.oninvalid = function(e) {
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Выберите тип клиента");
            }
        };
        $('#client_type').change(function() {
            this.setCustomValidity('');
        });
    });
</script>
