@extends('app')

@section('styles')
<style>
    .btn.btn-lg:after {
        font-family: "Glyphicons Halflings";
        content: "\e114";
        float: right;
        margin-left: 15px;
    }

    .btn.btn-lg.collapsed:after {
        content: "\e080";
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-7">
        <div class="box" style="border-top: none;">
            <button type="button" class="btn btn-lg @if (!$errors->has()) collapsed @endif"
                    style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                    data-toggle="collapse" required
                    data-target="#collapsed_container">Добавить нового клиента
            </button>
            <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>

                @if ($errors->has())
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @include("contractors.clients.new_client_form")
            </div>
        </div>
    </div>
</div>
    <div class="box">
        <div class="box-header">
            <form id="seach_form" action="{{ route('contractors.clients') }}" method="get">
                <div class="input-group">
                    <input type="text"
                           name="search"
                           class="form-control input-sm pull-right"
                           placeholder="Введите фразу для поиска..."
                           value="{{ $search }}"
                    />

                    <div class="input-group-btn">
                        <button id="btnSearch" class="btn btn-sm btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover client_table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Аватар</th>
                        <th>Клиент</th>
                        <th>Контактное лицо</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Дата регистрации</th>
                        <th>Средний чек</th>
                        @if (Auth::user()->accessLevel() > 2) <th>Менеджер</th> @endif
                    </tr>
                </thead>

                <tbody>
                    @include('contractors.contractors.client_table')
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            {!! $clients->render() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    var count_timout = 0;
    $(document).ready(function () {
        $('input[name = "search"]').keyup(function () {
            count_timout++;
            setTimeout(function() {
                count_timout--;
                if (count_timout == 0) {
                    filterActivate();
                }
            }, 500);
        });

        $("#btnSearch").click(function () {
            filterActivate();
        });

        function filterActivate() {
            var frm = $('#seach_form');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: frm.serialize(),
                method: "POST",
                url: "/contractors/clients",
                error: function (xhr, status, error) {
                    $('.future_field_msg').css('color', 'red').text("Error! Contact admin ...");
                },
                success: function (data) {
                    $('.client_table tbody').html(data);
                }
            });
        }
    });
</script>
@endsection
