@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавить контактное лицо</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="POST" action="/client/new_contact" autocomplete="off" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                <h4><i class="icon fa fa-ban"></i> Форма не отправлена!</h4>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input id="last_name"
                                           class="form-control"
                                           type="text"
                                           name="last_name"
                                           value="{{ old('last_name') }}"
                                           placeholder="Фамилия"
                                    />
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <input class="form-control"
                                           type="text"
                                           name="name"
                                           value="{{ old('name') }}"
                                           placeholder="Имя (Отчество)"
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="client_id">Компания</label>
                            {!! Form::select('client_id', $companies->pluck('tm_name', 'id'), $company->id, ['class' => 'form-control']) !!}
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text"
                                           name="position"
                                           value="{{ old('position') }}"
                                           placeholder="Должность"
                                           class="form-control"
                                    />
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input class="form-control"
                                               type="email"
                                               name="email"
                                               value="{{ old('email') }}"
                                               placeholder="Электронная почта"
                                        />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input name="phone"
                                               type="text"
                                               class="form-control"
                                               id="c-phone"
                                               value="{{ old('phone') }}"
                                               placeholder="Телефон"
                                        />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control"
                                              name="description"
                                              placeholder="Комментарий"
                                              style="height: 133px;"
                                    >{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>        
@stop