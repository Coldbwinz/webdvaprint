@extends('app')

@section('content')
    <script>
        window.CLIENT_ID = {{ $client->id }};
        console.log( {{ \Request::input('phone') }} )
    </script>
    <input type="hidden" id="contact_id" value="{{ $contact->id }}"/>
    <div id="card" class="client-card">
        <div class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        @foreach($contacts as $person)
                            <a href="/contact/show/{{ $person->id }}/{{ $client->id }}" title="{{ $person->full_name }}">
                                <img src="{{ asset($person->avatar) }}"
                                     alt="{{ $person->full_name }} avatar"
                                     class="img-circle img-bordered-sm"
                                     width="50"
                                     onerror="this.src='/images/nophoto.png'"
                                />
                            </a>
                        @endforeach
                    </div>
                    <div class="col-md-5 text-right">
                        @if($contactClients)
                            @foreach($contactClients as $personClient)
                                <a href="{{ url('contact/show/' . $contact->id . '/' . $personClient->id) }}"
                                   title="{{ $personClient->tm_name }}"
                                >
                                    <img src="{{ asset($personClient->picture) }}"
                                         alt="{{ $personClient->name }}"
                                         class="img-bordered-sm"
                                         height="50"
                                         onerror="this.src='/images/nophoto.png'"
                                    />
                                </a>
                            @endforeach
                        @endif
                        @if ($client->type_id != \App\ClientTypes::PARTNER)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> Добавить <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('client/new_client/'.$contact->id.'/'.$client->id) }}">Компанию</a></li>

                                @if ($client->isCompany())
                                    <li><a href="{{ url('client/new_contact/'.$client->id) }}">Контакт</a></li>
                                @endif
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="col-md-4">
                        @if (! Auth::user()->isClient())
                            <select class="form-control" name="manager">
                                <option value="0" @if ($client->manager_id == 0) selected @endif>нет
                                    менеджера
                                </option>
                                @foreach ( $managers as $manager )
                                    <option value="{{ $manager->id }}"
                                            @if ($client->manager_id == $manager->id) selected @endif>  {{ $manager->name }}  {{ $manager->lastname }} </option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="box box-primary">
                    <form role="form" method="post">
                        {!! csrf_field() !!}
                        <div class="hidden">
                            <input id="upload" name="photo" type="file" value=""/>
                            <input type="hidden" name="MAX_FILE_SIZE" value="8192000"/>
                            <input type="hidden" id="imagebase64" name="imagebase64"/>
                        </div>
                        <div class="box-body clearfix">
                            <div class="contact-profile">
                                <div class="contact-picture-wrapper">

                                    <logo name="contact-avatar"
                                          default="{{ asset('images/nophoto.png') }}"
                                          class-list="image-rounded"
                                          src="{{ asset($contact->avatar) }}"
                                          handler="{{ url('api/contact/' . $contact->id . '/avatar') }}"
                                          width="128"
                                          height="128"
                                    ></logo>

                                </div>

                                <div class="contact-information">
                                    <a href="#"
                                       data-editable
                                       data-type="text"
                                       data-pk="{{ $contact->id }}"
                                       data-name="last_name"
                                       data-url="{{ route('api.contact.update', $contact->id) }}"
                                       data-title="Фамилия"
                                       class="profile-username"
                                       data-emptytext="Фамилия не указана"
                                    >
                                        {{ $contact->last_name }}
                                    </a>

                                    <br>

                                    <a href="#"
                                       data-editable
                                       data-type="text"
                                       data-pk="{{ $contact->id }}"
                                       data-name="name"
                                       data-url="{{ route('api.contact.update', $contact->id) }}"
                                       data-title="Имя Отчество"
                                       class="profile-username"
                                       data-emptytext="Имя Отчество не указано"
                                    >
                                        {{ $contact->name }}
                                    </a>

                                    <br>

                                    <a href="#"
                                       data-editable
                                       data-type="text"
                                       data-pk="{{ $contact->id }}"
                                       data-name="position"
                                       data-url="{{ route('api.contact.client.update', [$contact->id, $client->id]) }}"
                                       data-title="Должность"
                                       data-emptytext="Должность не установлена"
                                    >
                                        {{ $contact->getInformationFieldForClient($client->id, 'position') }}
                                    </a>

                                    <br>

                                    <a href="#"
                                       data-editable
                                       data-type="text"
                                       data-name="email"
                                       data-pk="{{ $contact->id }}"
                                       data-url="{{ route('api.contact.update', $contact->id) }}"
                                       data-title="e-mail"
                                       data-emptytext="e-mail не указан"
                                    >
                                        {{ $contact->email }}
                                    </a>

                                    <br>

                                    <a href="#"
                                       data-editable
                                       data-type="text"
                                       data-name="phone"
                                       data-pk="{{ $contact->id }}"
                                       data-url="{{ route('api.contact.update', $contact->id) }}"
                                       data-title="Телефон"
                                       data-emptytext="Телефон не указан"
                                    >
                                        {{ $contact->phone }}
                                    </a>
                                </div>
                            </div>


                            <div class="form-group">
                                <a href="/orders/new-step1/{{ $client->id }}?contact_id={{ $contact->id }}" class="btn btn-danger btn-full-width">
                                    Оформить новый заказ
                                </a>
                            </div>

                            @include('contractors.clients.partials.orders')
                            @include('contractors.clients.partials.1s-settings')
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-9">
                @include('contractors.clients.form.company-info-form')
                @include('contractors.clients.form.private-person-info-form')
            </div>
        </div>
    </div>
    <div class="modal" id="avatar-crop-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Редактировать изображение</h4>
                </div>
                <div class="modal-body">
                    <div id="uploaded-image"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-croppie">Применить</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/cropperjs/cropper.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('plugins/cropperjs/cropper.min.js') }}"></script>
    <script src="{{ asset('plugins/croppie/croppie.js') }}"></script>
    <script src="{{ asset('js/components/logo.js') }}"></script>

    <script type="text/x-template" id="logoComponentTempalate">
        <div class="picture">
            <img :src="imageSrc"
                 :class="classList"
                 @click="selectFileTrigger"
                 title="Загрузить другое изображение"
                 style="cursor:pointer;"
                 :onError="onErrorImage"
                 :width="width"
                 :height="height"
            />

            <input :id="identificator"
                   type="file"
                   class="form-control hidden"
                   @change="selectLogo"
            />

            <input type="hidden" :name="name" :value="imageSrc"/>

            <div class="modal" :id="modalId">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Редактировать изображение</h4>
                        </div>

                        <div class="modal-body">
                            <div style="margin-bottom: 20px;">
                                <canvas :id="cropperAreaId"></canvas>
                            </div>

                            <div class="cropper-controls">
                                <div class="btn-group">
                                    <button type="button" :class="{'btn': true, 'btn-primary': true, 'active': dragMode === 'move'}" title="Режим перемещения изображения" @click="dragMode = 'move'">
                                        <i class="fa fa-arrows"></i>
                                    </button>
                                    <button type="button" :class="{'btn': true, 'btn-primary': true, 'active': dragMode === 'crop'}" title="Режим выбора области" @click="dragMode = 'crop'">
                                        <i class="fa fa-crop"></i>
                                    </button>
                                </div>

                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" title="Увеличить изображение" @click="zoom(0.1)">
                                        <i class="fa fa-search-plus"></i>
                                    </button>
                                    <button type="button" class="btn btn-primary" title="Уменьшить изображение" @click="zoom(-0.1)">
                                        <i class="fa fa-search-minus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-primary"
                                    @click.prevent="cropImage"
                                    :disabled="isBusy"
                            >Применить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script>
        var client = new Vue({
            el: '#card',
            data: {
                clientId: {{ $client->id }},
                pictureUploadUrl: '{{ route('api.client.picture', $client->id) }}',
                pictureSrc: '{{ asset($client->picture) }}'
            },

            mounted: function () {
                // Bootstrap editable initialization
                $.fn.editable.defaults.ajaxOptions = {type: "patch"};
                $('a[data-editable]').editable({
                    error: function (response, newValue) {
                        if (response.status === 500) {
                            return 'Service unavailable. Please try later.';
                        }

                        if (response.status === 422) {
                            var message = '';
                            $.each(response.responseJSON, function (key, value) {
                                message += key + ': ' + value;
                            })

                            return message;
                        }
                    }
                });
            }
        });
    </script>

    <script src="/js/client_controller.js"></script>
    <script>
        $('select[name="manager"]').on('change', function () {
            var $selector = $(this),
                    managerId = $selector.val(),
                    clientId = '{{ $client->id }}';

            $selector.prop('disabled', true);

            $.get(url('client/change_manager_ajax/' + clientId + '/' + managerId))
                    .always(function () {
                        $selector.prop('disabled', false);
                    });
        });
    </script>
@endsection
