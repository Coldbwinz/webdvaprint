@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Создание партнёра</h3>
                </div>

                @if ($errors->count())
                    <div class="alert alert-danger">
                        <p><strong>При создании партнёра возникили следующие ошибки:</strong></p>
                        <ul>
                            @foreach($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif

                <div class="box-body">
                    {!! Form::model($client, ['url' => "client/{$client->id}/{$contact->id}/partner"]) !!}
                        @include('contractors.partners.create-form')

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Сделать партнёром
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
