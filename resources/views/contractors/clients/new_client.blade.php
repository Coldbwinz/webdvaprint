@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавить компанию</h3>
                </div>

                <form method="POST" action="/client/new_client" autocomplete="off" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <input type="hidden" name="client_id" value="{{ $client->id }}">

                    <div class="box-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                <h4><i class="icon fa fa-ban"></i> Форма не отправлена!</h4>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{-- Компания --}}
                        <div class="form-group">
                            <input class="form-control"
                                   type="text"
                                   name="tm_name"
                                   value="{{ old('tm_name') }}"
                                   placeholder="Компания"
                            />
                        </div>

                        {{-- Контактное лицо --}}
                        <div class="form-group">
                            <label for="contact_id">Контактное лицо</label>
                            {!! Form::select('contact_id', $client->contacts->lists('fullName', 'id'), $contact->id, ['class' => 'form-control']) !!}
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {{-- Должность --}}
                                <div class="form-group">
                                    <input type="text"
                                           name="position"
                                           value="{{ old('position') }}"
                                           placeholder="Должность"
                                           class="form-control"
                                    />
                                </div>

                                {{-- Рабочий телефон --}}
                                <div class="form-group">
                                    <input name="phone"
                                           type="text"
                                           class="form-control"
                                           value="{{ old('phone') }}"
                                           placeholder="Рабочий телефон"
                                    />
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- Комментарий --}}
                                <div class="form-group">
                                    <textarea class="form-control"
                                              name="description"
                                              placeholder="Комментарий"
                                              style="height: 83px;"
                                    >{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
