<div id="private-person-wrapper" class="{{ $client->isCompany() ? ' hidden' : '' }}">
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="private-person-area" class="control-label">&nbsp;</label>
                    <input name="area"
                           type="text"
                           class="form-control text-right"
                           id="private-person-area"
                           placeholder="Отрасль"
                           value="{{ $client->area }}"
                           data-quick-edit
                           data-name="area"
                           data-url="{{ route('api.client.update', $client->id) }}"
                    />
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="c-sector" class="control-label">Статус</label>
                            <select class="form-control"
                                    name="client_type"
                                    data-quick-edit
                                    data-url="{{ route('api.client.update', $client->id) }}"
                            >
                                @foreach ( $client_types as $client_type )
                                    @if ($client->subdomain_id == $client_type->subdomain_id)
                                        <option value="{{ $client_type->id }}"
                                            @if ($client->type_id == $client_type->id) selected @endif>
                                            {{ $client_type->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="c-sector" class="control-label">Запуск в работу</label>
                            <select class="form-control"
                                    name="client_status"
                                    data-quick-edit
                                    data-url="{{ route('api.client.update', $client->id) }}"
                            >
                                @foreach ( $client_statuses as $client_status )
                                    <option value="{{ $client_status->id }}"
                                            @if ($client->status_id == $client_status->id) selected @endif>
                                        {{ $client_status->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#person_tab_1" data-toggle="tab">Общая информация</a></li>
            <li><a href="#person_tab_2" data-toggle="tab">Активность</a></li>
            <li><a href="#person_tab_3" data-toggle="tab">Реквизиты</a></li>
            <li><a href="#person_tab_4" data-toggle="tab">Документы</a></li>
            @if($waitingOrder)
                <li class="has-debt">
                    <button class="btn btn-warning"
                            onclick="document.location.href='/order/info/{{ $waitingOrder->id }}'; return false;">
                        <i class="fa fa-warning"></i>Ожидает оплаты
                    </button>
                </li>
            @endif
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="person_tab_1">
                <form id="client-form">
                    <div class="box-body">
                        <div id="private-person-info-form">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ltd-client-type" class="control-label">Тип клиента</label>
                                        <select id="ltd-client-type"
                                                class="form-control"
                                                data-url="{{ route('api.client.update', $client->id) }}"
                                        >
                                            <option value="company">Компания</option>
                                            <option value="private-person" selected="selected">Частное лицо</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="person-last-name" class="control-label">&nbsp;</label>
                                        <input type="text"
                                               name="last_name"
                                               id="person-last-name"
                                               class="form-control"
                                               value="{{ $client->last_name }}"
                                               placeholder="Фамилия"
                                               data-quick-edit
                                               data-url="{{ route('api.client.update', $client->id) }}"
                                        />
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="ltd-name" class="control-label">&nbsp;</label>
                                        <input id="ltd-name"
                                               type="text"
                                               name="name"
                                               class="form-control"
                                               placeholder="Имя (Отчество)"
                                               value="{{ $client->name }}"
                                               data-quick-edit
                                               data-url="{{ route('api.client.update', $client->id) }}"
                                        />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="text"
                                       name="address"
                                       value="{{ $client->address }}"
                                       class="form-control
           text-right"
                                       placeholder="Фактический адрес (используется для доставки)"
                                       data-quick-edit
                                       data-url="{{ route('api.client.update', $client->id) }}"
                                />
                            </div>

                            @include('contractors.clients.form.fields.connections')

                            <div class="form-group">
                                <textarea class="form-control"
                                          placeholder="Комментарии и краткая информация о клиенте"
                                          rows="5"
                                          data-quick-edit
                                          name="description"
                                          data-url="{{ route('api.client.update', $client->id) }}"
                                >{{ $client->description }}</textarea>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="person_tab_2">
                <ul class="timeline timeline-inverse">
                    <?php $buffer_date = ''; ?>

                    @foreach ($order_logs as $value)
                        @if ($buffer_date != date('d.m.Y', strtotime($value->created_at)))
                            <li class="time-label">
                                    <span class="bg-red">
                                        {{ date('d.m.Y', strtotime($value->created_at)) }}
                                    </span>
                            </li>
                        @endif
                    <!-- timeline item -->
                        <li>
                            <i class="fa fa-file-text-o bg-blue"></i>
                            <div class="timeline-item">
                                            <span class="time"><i
                                                        class="fa fa-clock-o"></i> {{ date('H:i', strtotime($value->created_at)) }}</span>
                                <h3 class="timeline-header"><a href="#">Заказ №{{ $value->order_id }}</a>
                                    сменил статус</h3>
                                <div class="timeline-body">
                                    Статус: <b>
                                        @if ($value->status == 1)
                                            Черновик
                                        @endif

                                        @if ($value->status == 2)
                                            Ждет
                                        @endif

                                        @if ($value->status == 3)
                                            Отправлен
                                        @endif

                                        @if ($value->status == 4)
                                            Макет утвержден
                                        @endif

                                        @if ($value->status == 5)
                                            Ожидает оплаты
                                        @endif

                                        @if ($value->status == 6)
                                            Полностью оплачен
                                        @endif

                                        @if ($value->status == 7)
                                            Частично оплачен
                                        @endif

                                        @if ($value->status == 8)
                                            Печать без предоплаты
                                        @endif

                                        @if ($value->status == 9)
                                            Запущен в работу
                                        @endif

                                        @if ($value->status == 10)
                                            Пошел в печать
                                        @endif

                                        @if ($value->status == 11)
                                            Готов
                                        @endif

                                        @if ($value->status == 12)
                                            На выдаче
                                        @endif
                                    </b>
                                </div>
                                <div class="timeline-footer">
                                    @if ($value->status == 1)<a
                                            class="btn btn-danger btn-xs">Удалить</a>@endif
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <?php $buffer_date = date('d.m.Y', strtotime($value->created_at)); ?>
                    @endforeach
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            </div>
            @if ($client->isCompany())
                <div class="tab-pane" id="person_tab_3">
                    @include('contractors.clients.form.details_form')
                </div>
            @endif
            <div class="tab-pane" id="person_tab_4">
                <form method="POST" action="/client/uploadFile" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="contact_id" value="{{ $contact->id }}"/>
                    <div class="form-group">
                        <button class="btn btn-flat btn-primary pull-right" id="file-upload-trigger">Загрузить
                            файл
                        </button>
                        <input type="file" name="document" id="file-upload" style="display: none;"/>
                        <div class="clearfix"></div>
                    </div>
                    <input type="submit" style="display: none;"/>
                </form>
                <table class="table">
                    <tr>
                        <th>Счёт №</th>
                        <th>Обновлен</th>
                        <th class="actions text-right">Действие</th>
                    </tr>
                    @foreach($order_pdfs as $invoice_pdf)
                        <tr>
                            <td>{{ $invoice_pdf->id }}</td>
                            <td>{{ date('d.m.Y H:i', strtotime($invoice_pdf->updated_at)) }}</td>
                            <td class="text-right">
                                @if ($invoice_pdf->download)
                                    <a href="
                                        @if ($invoice_pdf->need_new_invoice)
                                            orders_pdf/order_n_{{$invoice_pdf->id}}_{{$invoice_pdf->pdf}}.pdf
                                        @else
                                            /orders/history/calculation/0/0/{{$invoice_pdf->id}}/1
                                        @endif
                                            ">Скачать</a>
                                @else
                                    <a href="#">Счёт незаполнен</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    {{--
                    @foreach($files as $file)
                    <tr id="doc-{{ $file->id }}">
                        <td><a href="{{ $file->path }}">{{ $file->name }}</a></td>
                        <td class="actions text-right">
                            <button class="btn btn-flat btn-danger remove-file" data-id="{{ $file->id }}" title="Удалить"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                    --}}
                </table>
            </div>
        </div>
    </div>
</div>
