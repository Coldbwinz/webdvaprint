<div class="row connections-field">
    <div class="col-sm-3 connection-item connection-item-primary">
        <div class="form-group">
            <input type="text"
                   class="form-control"
                   placeholder="e-mail"
                   value="{{ $client->email }}"
                   name="email"
                   data-quick-edit
                   data-url="{{ route('api.client.update', $client->id) }}"
            />
        </div>
    </div>

    <div class="col-sm-3 connection-item connection-item-primary">
        <div class="form-group">
            <input type="text"
                   class="form-control"
                   placeholder="Мобильный телефон"
                   value="{{ $client->phone }}"
                   name="phone"
                   data-quick-edit
                   data-url="{{ route('api.client.update', $client->id) }}"
            />
        </div>
    </div>

    @if ($client->connections)
        @foreach($client->connections as $connection)
            <div class="col-sm-3 connection-item connection-additional">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-close><i class="fa fa-close"></i></button>
                        </span>

                        <input type="text"
                               class="form-control"
                               value="{{ $connection['value'] }}"
                               data-name="{{ $connection['name'] }}"
                               placeholder="{{ isset($connection['title']) ? $connection['title'] : '' }}"
                        />
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    <div class="col-sm-3 connection-item connection-item-primary">
        <div class="form-group">
            <div class="btn-group connection-add-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Добавить средство связи <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" data-name="phone">Номер телефона</a></li>
                    <li><a href="#" data-name="email">E-mail</a></li>
                    <li><a href="#" data-name="social">Ссылка в соц. сетях</a></li>
                    <li><a href="#" data-name="skype">Skype</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>