@if ($client->isCompany() && (!$client->company_details['all_ok']))
    @include('contractors.clients.form.client-parameters-form')
@endif
<script>
    $(document).ready(function() {
        $('#sameAsAddress').click(function(e){
            if (e.currentTarget.checked) {
                $('#c-post-address').val($('#c-address').val()).change();
            } else {
                $('#c-post-address').val('').change();
            }
            $('input[name="address"]').val($('#c-post-address').val());
        });
    });
</script>
    <form id="details-form" class="form-horizontal"
          @if ($client->isCompany() && empty($client->company_details['name']) &&
          empty($client->company_details['bank_name'])) style="display:none" @endif>
    <div class="box" id="client_company_details" @if ($client->isCompany() && empty($client->company_details['name'])) style="display:none" @endif>
        <div class="box-header">
            <h3 class="box-title">
                @if (isset($client->company_details['type']) && ($client->company_details['type'] == 'INDIVIDUAL'))
                    Реквизиты предпринимателя
                @else
                    Реквизиты компании
                @endif
            </h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="c-inn" class="col-sm-12 text-left">Полное наименование</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="c-full-name" placeholder=""
                        name="details_full_name"
                        details-quick-edit
                        data-url="{{ route('client.save.details', $client->id) }}"
                        @if (isset($client->company_details['full_name']))
                        value="{{ $client->company_details['full_name'] }}"
                        @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-inn" class="col-sm-4 control-label">Краткое наименование</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-name" placeholder=""
                           name="details_name"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['name'])) value="{{ $client->company_details['name'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-inn" class="col-sm-4 control-label">ФИО руководителя</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-director" placeholder=""
                           name="details_director"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['director'])) value="{{ $client->company_details['director'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-inn" class="col-sm-4 control-label">Должность руководителя</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-director-position" placeholder=""
                           name="details_director_position"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['director_position'])) value="{{ $client->company_details['director_position'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-inn" class="col-sm-4 control-label">ИНН</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-inn" placeholder=""
                           name="details_inn"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['inn'])) value="{{ $client->company_details['inn'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-inn" class="col-sm-4 control-label">КПП</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-kpp" placeholder=""
                           name="details_kpp"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['kpp'])) value="{{ $client->company_details['kpp'] }}" @endif/>
                </div>
            </div>
            <div class="form-group" style="margin-bottom: 0;">
                <label for="c-ogrn" class="col-sm-4 control-label">ОГРН</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-ogrn" placeholder=""
                           name="details_ogrn"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['ogrn'])) value="{{ $client->company_details['ogrn'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-address" class="col-sm-12 text-left">Юридический адрес</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="c-address" placeholder=""
                           name="details_address"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['address'])) value="{{ $client->company_details['address'] }}" @endif/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 text-left" style="margin-right: 15px;">Фактический адрес <input id="sameAsAddress"
                    @if ((isset($client->company_details['address']) &&
                    isset($client->company_details['post_address'])) &&
                    (strlen($client->company_details['address'] > 0)) &&
                    ($client->company_details['address'] == $client->company_details['post_address'])) checked @endif
                    type="checkbox" value="" style="margin-left:20px; height:16px; vertical-align: text-bottom;">
                    <font style="font-weight: normal;"> Соответствует юридическому</font></label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="c-post-address" placeholder=""
                           name="details_post_address"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['post_address'])) value="{{ $client->company_details['post_address'] }}" @endif/>
                </div>
            </div>
        </div>
    </div>
    <div class="box" id="client_bank_details" @if ($client->isCompany() && empty($client->company_details['bank_name'])) style="display:none" @endif>
        <div class="box-header">
            <h3 class="box-title">Банковские реквизиты</h3>
        </div>
        <div class="box-body">
            <div class="form-group" style="margin-bottom: 0;">
                <label for="c-bank-rs" class="col-sm-4 control-label">Расчётный счёт</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-bank-rs" placeholder=""
                           name="details_bank_rs"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['bank_rs'])) value="{{ $client->company_details['bank_rs'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-bank-name" class="col-sm-12 text-left">Наименование банка</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="c-bank-name" placeholder=""
                           name="details_bank_name"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['bank_name'])) value="{{ $client->company_details['bank_name'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-bank-ks" class="col-sm-4 control-label">Корреспондентский счёт</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-bank-ks" placeholder=""
                           name="details_bank_ks"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['bank_ks'])) value="{{ $client->company_details['bank_ks'] }}" @endif/>
                </div>
            </div>
            <div class="form-group">
                <label for="c-bank-bik" class="col-sm-4 control-label">БИК</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="c-bank-bik" placeholder=""
                           name="details_bank_bik"
                           details-quick-edit
                           data-url="{{ route('client.save.details', $client->id) }}"
                           @if (isset($client->company_details['bank_bik'])) value="{{ $client->company_details['bank_bik'] }}" @endif/>
                </div>
            </div>
        </div>
    </div>
</form>