<div class="row" id="client_parameters_by_three_fields">
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>
    <script>
        var getDataButtonPressed = false;
        $(document).ready( function() {
            $('.add_inn').click( function() {
                if (!getDataButtonPressed) {
                    getDataButtonPressed = true;
                    var params = {
                        inn: $('#company_inn').val(),
                    };
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                        },
                        url: '/options/set_all_data/{{$client->id}}',
                        data: params,
                        type: 'post',
                        success: function (response) {
                            $('#details-form').show();
                            $('#client_company_details').show();
                            getDataButtonPressed = false;
                            var data = JSON.parse(response);
                            if (response.length > 2) {
                                $('input[name="details_full_name"]').val(data.full_name);
                                $('input[name="details_name"]').val(data.name);
                                $('input[name="details_inn"]').val(data.inn);
                                $('input[name="details_ogrn"]').val(data.ogrn);
                                $('input[name="details_kpp"]').val(data.kpp);
                                if (data.type == "INDIVIDUAL") {
                                    $('input[name="details_director"]').val(data.director);
                                    $('input[name="details_director_position"]').val(data.director_position);
                                    $('input[name="details_address"]').val('');
                                    $('#client_company_details').find('h3').html('Реквизиты предпринимателя');
                                } else {
                                    $('input[name="details_director"]').val(data.director);
                                    $('input[name="details_director_position"]').val(data.director_position);
                                    $('input[name="details_address"]').val(data.address);
                                    $('#client_company_details').find('h3').html('Реквизиты компании');
                                }
                                $('input[name="details_post_address"]').val('');
                                $('#legal_name').val(data.legal_name);
                                $('#company-ownership-type').val(data.ownership_type);
                                $('input[name="address"]').val(data.address);
                            }
                            checkToHide();
                        },
                        failed: function (data) {
                            getDataButtonPressed = false;
                            console.log("ajax error");
                        }
                    });
                }
            });

            $('.add_bik').click( function() {
                if (!getDataButtonPressed) {
                    getDataButtonPressed = true;
                    var params = {
                        bik: $('#bank_bik').val(),
                    };
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                        },
                        url: '/options/set_all_data/{{$client->id}}',
                        data: params,
                        type: 'post',
                        success: function (response) {
                            $('#details-form').show();
                            $('#client_bank_details').show();
                            getDataButtonPressed = false;
                            var data = JSON.parse(response);
                            if (response.length > 2) {
                                $('#c-bank-name').val(data.bank_name);
                                $('#c-bank-bik').val(data.bank_bik);
                                $('#c-bank-ks').val(data.bank_ks);
                            }
                            checkToHide();
                        },
                        failed: function (data) {
                            getDataButtonPressed = false;
                            console.log("ajax error");
                        }
                    });
                }
            });

            $('#company_rs').on('input', function() {
                $('#c-bank-rs').val($('#company_rs').val()).change();
                checkToHide();
            });

            $('.add_inn, .add_bik').on('click', function () {
                if (!getDataButtonPressed) {
                    checkToHide();
                }
            });

            function checkToHide() {
                if (($('#c-full-name').val().length > 0) && ($('#c-bank-bik').val().length > 0) &&
                        ($('#c-bank-rs').val().length > 0)) {
                    $('#client_parameters_by_three_fields').hide();
                    $('#client_bank_details').show();
                    $('#client_company_details').show();
                }
            }
        });
    </script>
    <div class="col-md-6">
        <div class="box" style="border-top: none;">
            {!! csrf_field() !!}
            <div class="box-body">
                <div class="box-header no-padding">
                    <h3 class="box-title">Заполните только три поля, всё остальное добавит система</h3>
                </div>
                <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                    <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">ИНН</label>
                    <input class="form-control" style="display: table-cell;" type="number" id="company_inn"
                    @if (isset($client->company_details['inn']))
                        value="{{ $client->company_details['inn'] }}"@endif/>
                    <span class="input-group-addon btn btn-default add_inn">+ Добавить</span>
                </div>
                <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                    <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">Расчётный счёт</label>
                    <input class="form-control" style="display: table-cell;" type="number" id="company_rs"
                    @if (isset($client->company_details['bank_rs']))
                        value="{{ $client->company_details['bank_rs'] }}"@endif/>
                </div>
                <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                    <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">БИК</label>
                    <input class="form-control" style="display: table-cell;" type="number" id="bank_bik"
                    @if (isset($client->company_details['bank_bik']))
                        value="{{ $client->company_details['bank_bik'] }}"@endif/>
                    <span class="input-group-addon btn btn-default add_bik">+ Добавить</span>
                </div>
            </div>
        </div>
    </div>
</div>