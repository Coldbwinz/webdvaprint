<table class="table table-condensed table-hover">
    <caption>Заказы</caption>
    <thead>
        <tr>
            <th width="70%"></th>
            <th width="10%">Свои</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td><b>На согласовании</b></td>
            <td class="text-center">
                <a href="{{ route('orders.history', ['status' => 2]) }}?client_id={{ $client->id }}&contact_id={{ $contact->id }}" title="Смотреть">
                    {{ $stats['on_agreement_count'] }}
                </a>
            </td>
        </tr>

        <tr>
            <td><b>Требуют внимания!</b></td>
            <td class="text-center">
                <a href="{{ route('orders.history', ['status' => 1]) }}?client_id={{ $client->id }}&contact_id={{ $contact->id }}" title="Смотреть">
                    {{ $stats['need_attention_count'] }}
                </a>
            </td>
        </tr>

        <tr>
            <td><b>В работе</b></td>
            <td class="text-center">
                <a href="{{ route('orders.history', ['status' => 5]) }}?client_id={{ $client->id }}&contact_id={{ $contact->id }}" title="Смотреть">
                    {{ $stats['in_work_count'] }}
                </a>
            </td>
        </tr>

        <tr>
            <td><b>Готовы к выдаче</b></td>
            <td class="text-center">
                <a href="{{ route('orders.history', ['status' => 6]) }}?client_id={{ $client->id }}&contact_id={{ $contact->id }}" title="Смотреть">
                    {{ $stats['ready_count'] }}
                </a>
            </td>
        </tr>

        <tr class="danger">
            <td><b>Ожидают оплаты</b></td>
            <td class="text-center">
                <a href="{{ route('orders.history', ['status' => 3]) }}?client_id={{ $client->id }}&contact_id={{ $contact->id }}" title="Смотреть">
                    {{ $stats['waiting_payment_count'] }}
                </a>
            </td>
        </tr>
    </tbody>
</table>
