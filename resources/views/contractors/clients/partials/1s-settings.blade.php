@if (settings('1c_sync'))
    <div class="settings-1s">
        <div class="form-group">
            <label for="contact_1s_id">Идентификатор контакта</label>
            <input type="text"
                   id="contact_1s_id"
                   name="contact_1s_id"
                   class="form-control"
                   value="{{ $contact->id_1s }}"
                   data-quick-edit
                   data-name="id_1s"
                   data-url="{{ route('api.contact.update', $contact->id) }}"
            />
        </div>

        <div class="form-group">
            <label for="client_1s_id">Идентификатор клиента</label>
            <input type="text"
                   id="client_1s_id"
                   name="client_1s_id"
                   class="form-control"
                   value="{{ $client->id_1s }}"
                   data-quick-edit
                   data-name="id_1s"
                   data-url="{{ route('api.client.update', $client->id) }}"
            />
        </div>
    </div>
@endif