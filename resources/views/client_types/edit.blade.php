@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать продукт</h3>
                </div>
                <form method="POST" action="/client_types/edit/{{ $client_info->id }}" enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @if ($errors->has())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" type="text" name="name" value="{{ ( old('name') ? old('name') : $client_info->name) }}"/>
                            </div>
                            <div class="form-group">
                                <label>Скидка</label>
                                <input class="form-control" type="number" name="discount"
                                       value="{{ ( old('name') ? old('name') : $client_info->discount) }}"/>
                            </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
@stop