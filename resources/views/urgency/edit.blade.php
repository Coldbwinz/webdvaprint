@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать срочность</h3>
                </div>
                <form method="POST" action="/urgency/edit/{{ $urgency->id }}" enctype="multipart/form-data"
                      autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @if ($errors->has())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" type="text" name="name" value="{{ ( old('name') ? old('name') : $urgency->name) }}"/>
                            </div>

                            @if (settings('1c_sync'))
                                <div class="form-group">
                                    <label for="key_1s">Название для 1С</label>
                                    <input type="text"
                                           id="key_1s"
                                           class="form-control"
                                           name="key_1s"
                                           value="{{ old('key_1s', $urgency->key_1s) }}"
                                    />
                                </div>
                            @endif

                            <div class="form-group">
                                <label>Скидка</label>
                                <input class="form-control" type="number" name="discount"
                                       value="{{ ( old('name') ? old('name') : $urgency->discount) }}"/>
                            </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
@stop