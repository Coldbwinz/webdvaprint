@extends('out_loading')

@section('content')
    <script>
        var global_response = JSON.parse('<? echo $response; ?>');
    </script>
    @if (json_decode($response)->client_for_close > 0)
        <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#modalContinueOrder').show();
            })
        </script>
        @include('orders.continue_order_for_group')
    @elseif ((json_decode($response)->client_for_close == -1) && ($company_details > 0))
        <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#modalGetCompanyData').show();
            })
        </script>
        @include('orders.modals.get_company_data_form')
    @else
        <script>
            @if (Auth::user()->accessLevel() == 1)
                window.location.href = '/orders/need_attention';
            @else
                window.location.href = '/orders/history';
            @endif
        </script>
    @endif
@stop

@section('scripts_bottom')
    @parent
@stop