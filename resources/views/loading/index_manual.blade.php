@extends('out_loading')

@section('scripts_bottom')
    @parent
@stop

@section('content')
    <br><br><br><br><br>
    @foreach ($print_structure as $key => $value)
        <div style="text-align: center;">
            <a href="#" style="font-size: 20px" class="btn red">
                <?php
                    if ($key == 'cover') echo 'Загрузить обложку';
                    if ($key == 'pad') echo 'Загрузить подложку';
                    if ($key == 'body') echo 'Загрузить страницы';
                ?>
            </a>
        </div>
    @endforeach
@stop
