@extends('out_loading')

@section('scripts_bottom')
    @parent
@stop
<div id="div_overlay_top"
     style="display: none; height:101px; left: 0; top:0; padding-right:16px; background: #DEE4EE; position: fixed; z-index: 9999 !important;">
    <h1 style="font-size:2em; font-weight: bold; color: #333c4b; text-align:center; "></h1></div>

<div id="div_overlay"
     style="display: none; height:96px; left: 0; bottom:0; padding-right:16px; background: #DEE4EE; position: fixed; z-index: 9999 !important;"></div>

@section('content')
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script>
        $(document).ready(function (e) {
            $('.wrapper').hide();
            var id_order = '<? echo $id_order ?>';
            var order_status = '<? echo $order_status ?>';
            var printler_type = '<? echo $printler_type ?>';
            var selected_type = '<? echo $selected_type ?>';
            CheckingSeassion();
            function CheckingSeassion() {
                var checkstatus_url = '/order/check_status/' + id_order + '/' + order_status;
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: checkstatus_url,
                    data: {},
                    success: function (result) {
                        if (result) {
                            location.reload();
                        } else {
                            $('.wrapper').show();
                        }
                    }
                });
            }

            var approve = false;
            var data_info_id = '';
            var iframe;
            PrintlerMaquetteWidget.on("maquetteApprove", function (data) {
                $("#div_overlay").hide();
                $("#div_overlay_top").hide();
                data_info_id = data.info.id;
                approve = true;
            });

            PrintlerMaquetteWidget.on("close", function (data) {
                var type = 1;
                if (data.id == 'maquette-id-cover') {
                    type = 2;
                } else if (data.id == 'maquette-id-pad') {
                    type = 3;
                }
                if (approve) {
                    window.location.href = '/order/template_agree/' + id_order + '/' + data_info_id + '/' + type + '/' + '<?php echo $new; ?>';
                }
            });

            PrintlerMaquetteWidget.on("open", function (data) {
                iframe = $("#printler-maquette-widget");
                iframe.css("z-index", 1);
                $(".main-header").css("z-index", 0);
                var obj = $(this[1].arguments[1]);
                var width = obj.attr('printler-width');
                var height = obj.attr('printler-height');
                var type = obj.attr('printler-input-id').substr(12);
                var count = 0;
                var countText = '';
                var size = width + ' x ' + height + ' мм - ';
                if (printler_type == 3) {
                    if (obj.attr('printler-product-type') == 'brochurebifold') {
                        count = '';
                        type = 'Обложка в развёрнутом виде: ';
                        size = (width * 2) + ' x ' + height + ' мм - ';
                        countText = ((obj.attr('printler-back-chromaticity') == 0)) ? ' одна сторона' : ' две стороны';
                    } else if (obj.attr('printler-product-type') == 'singlesheet') {
                        count = (obj.attr('printler-back-chromaticity') == 0) ? 1 : 2;
                        countText = (count == 1) ? ' сторона' : ' стороны';
                        if (type == 'cover') {
                            type = 'Обложка ';
                        } else if (type == 'pad') {
                            type = 'Подложка '
                        }
                    } else if (type == 'body') {
                        type = 'Внутренний блок ';
                        count = parseInt(obj.attr('printler-count')) + 4;
                        countText = ' полос';
                    } else {
                        count = (obj.attr('printler-block-back-chromaticity') == 0) ? 1 : 2;
                        countText = (count == 1) ? ' сторона' : ' стороны';
                        if (type == 'cover') {
                            type = 'Обложка ';
                        } else if (type == 'pad') {
                            type = 'Подложка '
                        }
                    }
                    if ((count > 20) && (((count % 10) == 2) || ((count % 10) == 4))) {
                        countText += 'ы';
                    }
                } else {
                    count = (obj.attr('printler-back-chromaticity') == 0) ? 1 : 2;
                    if (printler_type == 2) {
                        count = '';
                        if (selected_type == 'один сгиб')  {
                            type = 'Фальцуемое изделие с одним сгибом: ';
                            size = size.substr(0, size.length - 3);
                        } else if (selected_type == 'два сгиба') {
                            type = 'Евробуклет';
                            countText = '';
                            size = '';
                        }
                    } else if (printler_type == 1) {
                        type = 'Листовое изделие: ';
                        countText = (count == 1) ? ' сторона' : ' стороны';
                    }
                }
                $('#div_overlay_top h1').html(type + ' ' + size + count + countText);
                overlayResize();
            });

        });
        $(window).resize(function (e) {
            overlayResize();
        });

        function overlayResize() {
            if ($("#printler-maquette-widget").is(':visible')) {
                if ($(window).width() >= 901) {
                    $("#div_overlay").css('width', $(window).width() * 0.66).show();
                    $("#div_overlay_top").css('width', $(window).width() * 0.66).show();
                } else {
                    $("#div_overlay").css('width', $(window).width() - 16).hide();
                    $("#div_overlay_top").css('width', $(window).width() * 1 - 16).show();
                }
            } else {
                $("#div_overlay").hide();
                $("#div_overlay_top").hide();
            }
        }
    </script>

    <br><br><br><br><br>
    @foreach ($print_structure as $key => $value)
        <div style="text-align: center;">
            <div style="text-align: center;"><a href="#" style="font-size: 20px" class="btn red printler-maquette-button"
                printler-input-id="maquette-id-{{$value['type']}}"
                printler-ok-text="Сохраняем макет..."
                printler-ok-class="green"
                printler-product-type="{{ $value['p_name'] }}"
                @if (($value['p_name'] == 'bookletwiredcoil') ||
                ($value['p_name'] == 'bookletsaddlestitch'))
                    printler-cover-front-chromaticity="{{($value['chromacity_front'] == 1) ? 4 : $value['chromacity_front']}}"
                    printler-cover-back-chromaticity="{{($value['chromacity_back'] == 1) ? 4 : $value['chromacity_back']}}"
                    printler-pad-front-chromaticity="{{($value['chromacity_front'] == 1) ? 4 : $value['chromacity_front']}}"
                    printler-pad-back-chromaticity="{{($value['chromacity_back'] == 1) ? 4 : $value['chromacity_back']}}"
                    printler-block-front-chromaticity="{{($value['chromacity_front'] == 1) ? 4 : $value['chromacity_front']}}"
                    printler-block-back-chromaticity="{{($value['chromacity_back'] == 1) ? 4 : $value['chromacity_back']}}"
                    printler-count="{{ $value['count'] - 4 }}"
                @else
                    @if ((($value['p_name'] == 'brochurebifold') || ($value['p_name'] == 'brochuretrifold')) && ($value['chromacity_back'] == 0))
                        printler-count="1"
                    @endif
                    printler-front-chromaticity="{{($value['chromacity_front'] == 1) ? 4 : $value['chromacity_front']}}"
                    printler-back-chromaticity="{{($value['chromacity_back'] == 1) ? 4 : $value['chromacity_back']}}"
                @endif
                printler-width="{{ $value['width'] }}"
                printler-height="{{ $value['height'] }}">
                @if ( $value['type'] == 'body')
                    @if (($value['p_name'] == 'bookletwiredcoil') || ($value['p_name'] == 'bookletsaddlestitch'))
                        {{ ($order_img == null) ? 'Загрузить страницы' : (($order_status == 12) ? 'Заменить страницы' : 'Страницы загружены') }}
                    @elseif (isset($value['same_pages']))
                        {{ ($order_img == null) ? 'Загрузить страницу' : (($order_status == 12) ? 'Заменить страницу' : 'Страница загружен') }}
                    @else
                        {{ ($order_img == null) ? 'Загрузить макет' : (($order_status == 12) ? 'Заменить макет' : 'Макет загружен') }}
                    @endif
                @elseif ( $value['type'] == 'cover' )
                    {{ ($template_cover_download_id == null) ? "Загрузить обложку" : (($order_status == 12) ? 'Заменить обложку' : 'Обложка загружена')}}
                @elseif ( $value['type'] == 'pad' )
                    {{ ($template_pad_download_id == null) ? "Загрузить подложку" : (($order_status == 12) ? 'Заменить подложку' : 'Подложка загружена')}}
                @endif
            </a></div>
        </div>
    @endforeach
    <script id="printler-api-script" src="//public.printler.pro/api.js"
            widget="28ba0df418994bf68fddb4a2e05fbbe5"></script>
@stop
