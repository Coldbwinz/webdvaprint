@extends('app')

@section('content')
    <div id="services">
        {!! Form::model($productType, ['route' => ['sync1s.update', $productType], 'method' => 'post']) !!}

            <div class="form-group{{ $errors->has('product_1c_key') ? ' has-error' : '' }}">
                {!! Form::label('product_1c_key', 'Идентификатор продукта в 1С') !!}
                {!! Form::text('product_1c_key', null, ['class' => 'form-control']) !!}
                @if ($errors->has('product_1c_key'))
                    <div class="help-block">{{ $errors->first('product_1c_key') }}</div>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
