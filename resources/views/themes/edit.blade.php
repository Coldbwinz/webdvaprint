@extends('app')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактировать тематику</h3>
                </div>

                <form method="POST" action="/products/types/edit/{{ $theme->id }}" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        {{-- Название --}}
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control" type="text" name="name" value="{{ $theme->name }}">
                        </div>

                        {{-- Теги --}}
                        <div class="form-group">
                            <label for="tags" class="control-label">Теги</label>
                            {!! Form::select('tags[]', \App\ThemeTag::all()->lists('name', 'id'), $theme->tags->lists('id')->toArray(), ['class' => 'form-control select2', 'multiple']) !!}
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if ($errors->has())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
@stop