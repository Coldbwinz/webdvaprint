@extends('app')

@section('content')
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>
    <div class="row">
        <div class="col-md-6">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg collapsed"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse"
                        data-target="#collapsed_container">Добавить новую тематику
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>
                    <form method="POST" action="/products/types/new" autocomplete="off" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            @if ($errors->has())
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">

                        <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th>Название</th>
                            <th>Теги</th>
                            <th width="120">Включен</th>
                            <th width="220">Действия</th>
                        </tr>

                        @foreach ($themes as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>

                                {{-- Теги --}}
                                <td>
                                    @foreach($value->tags as $tag)
                                        <span class="label label-default">{{ $tag->name }}</span>
                                    @endforeach
                                </td>

                                <td>
                                    @if ($value->activity == 1)
                                        <a href="/products/types/off/{{ $value->id }}">
                                            <button class="btn btn-danger btn-xs">выключить</button>
                                        </a>
                                    @else
                                        <a href="/products/types/on/{{ $value->id }}">
                                            <button class="btn btn-success btn-xs">включить</button>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="/products/types/edit/{{ $value->id }}">
                                        <button class="btn btn-success btn-xs">редактировать</button>
                                    </a>
                                    <a onclick="return confirmDelete();"
                                       href="/products/types/delete/{{ $value->id }}">
                                        <button class="btn btn-danger btn-xs">удалить</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop