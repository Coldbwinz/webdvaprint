@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body">
            <prepress-table></prepress-table>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
