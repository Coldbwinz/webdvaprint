@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body table-responsive">
            <queue-prepress-table></queue-prepress-table>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
