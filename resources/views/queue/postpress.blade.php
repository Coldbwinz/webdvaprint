@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body table-responsive">
            <queue-postpress-table></queue-postpress-table>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
