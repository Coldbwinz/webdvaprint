@extends('app')

@section('content')
    <div id="root" class="box">
        <div class="box-body table-responsive">
            <queue-design-table></queue-design-table>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
