<div class="content">
    <div class="title" style="font-family: 'Roboto', sans-serif;font-size: 40px; margin-top: 100px;text-align: center">Ой, кажется что-то сломалось</div>
    @unless(empty($sentryID))
        <!-- Sentry JS SDK 2.1.+ required -->
        <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

        <script>
        Raven.showReportDialog({
            eventId: '{{ $sentryID }}',

            // use the public DSN (dont include your secret!)
            dsn: 'https://c3a3702a8889412e88d5c60ae56cb8bb@sentry.io/112863'
        });
        </script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    @endunless
</div>