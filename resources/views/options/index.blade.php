@extends('app')

@section('content')
    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th width="50">Номер</th>
                                <th width="150">Название</th>
                                <th width="160">Действие</th>
                            </tr>

                            @foreach ($options as $value)
                                <tr>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        @if ($value->activity == 1)
                                            <a href="{{ route('options.off', $value->id) }}">
                                                <button class="btn btn-danger btn-xs">выключить</button>
                                            </a>
                                        @else
                                            <a href="{{ route('options.on', $value->id) }}">
                                                <button class="btn btn-success btn-xs">включить</button>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $options->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
