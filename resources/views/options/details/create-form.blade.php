<div class="row">
    <div class="col-md-6">
        <div class="box" style="border-top: none;">
            <button type="button" class="btn btn-lg collapsed"
                    style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;" data-toggle="collapse"
                    data-target="#collapsed_container">Добавить организацию
            </button>
            <div id="collapsed_container" class="collapse">
                <form method="POST" action="{{ route('details.store') }}" autocomplete="off" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Название организации</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label>Почтовый индекс</label>
                            <input class="form-control" type="text" name="post_address" value="{{ old('post_address') }}">
                        </div>
                        <div class="form-group">
                            <label>Адрес</label>
                            <input class="form-control" type="text" name="address" value="{{ old('address') }}">
                        </div>
                        <div class="form-group">
                            <label>Р/С</label>
                            <input class="form-control" type="text" name="bank_rs" value="{{ old('bank_rs') }}">
                        </div>
                        <div class="form-group">
                            <label>Название банка</label>
                            <input class="form-control" type="text" name="bank_name" value="{{ old('bank_name') }}">
                        </div>
                        <div class="form-group">
                            <label>БИК</label>
                            <input class="form-control" type="text" name="bank_bik" value="{{ old('bank_bik') }}">
                        </div>
                        <div class="form-group">
                            <label>К/С</label>
                            <input class="form-control" type="text" name="bank_ks" value="{{ old('bank_ks') }}">
                        </div>
                        <div class="form-group">
                            <label>ИНН</label>
                            <input class="form-control" type="text" name="bank_inn" value="{{ old('bank_inn') }}">
                        </div>
                        <div class="form-group">
                            <label>КПП</label>
                            <input class="form-control" type="text" name="bank_kpp" value="{{ old('bank_kpp') }}">
                        </div>
                        <div class="form-group">
                            <label>Директор</label>
                            <input class="form-control" type="text" name="director" value="{{ old('director') }}">
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>