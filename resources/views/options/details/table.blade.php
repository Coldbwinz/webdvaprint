<div class="row">
    <div class="col-xs-12">
        @if (Session::has('message.success'))
            <div class="alert alert-success">
                {{ Session::get('message.success') }}
            </div>
        @endif

        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th width="150">Название</th>
                            <th width="160">Директор</th>
                            <th width="140">Почтовый адрес</th>
                            <th width="140">Адрес</th>
                            <th width="140">Р/С</th>
                            <th width="140">ИНН</th>
                            <th width="140">Включен</th>
                            <th width="140">Действия</th>
                        </tr>

                        @foreach ($details as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->director }}</td>
                                <td>{{ $value->post_address }}</td>
                                <td>{{ $value->address }}</td>
                                <td>{{ $value->bank_rs }}</td>
                                <td>{{ $value->bank_inn }}</td>
                                <td>
                                    @if ($value->activity == 1)
                                        <a href="/options/details/off/{{ $value->id }}">
                                            <button class="btn btn-danger btn-xs">выключить</button>
                                        </a>
                                    @else
                                        <a href="/options/details/on/{{ $value->id }}">
                                            <button class="btn btn-success btn-xs">включить</button>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="/options/details/edit/{{ $value->id }}">
                                        <button class="btn btn-success btn-xs">редактировать</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $details->render() !!}
            </div>
        </div>
    </div>
</div>
