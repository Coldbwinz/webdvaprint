@extends('app')

@section('content')
  <div class="row">
            <div class="col-xs-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Редактировать организацию</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  <form method="POST" action="/options/details/edit/{{ $details->id }}" enctype="multipart/form-data" autocomplete="off">
                  {!! csrf_field() !!}
                    <div class="box-body">
                      @if ($errors->has())
                        <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                          @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                          @endforeach
                        </div>
                      @endif
                      <div class="form-group">
                        <label>Название организации</label>
                        <input class="form-control" type="text" name="name" value="{{ old('name') ? old('name') : $details->name }}">
                      </div>
                      <div class="form-group">
                        <label>Почтовый индекс</label>
                        <input class="form-control" type="text" name="post_address" value="{{ old('post_address') ? old('post_address') : $details->post_address }}">
                      </div>
                      <div class="form-group">
                        <label>Адрес</label>
                        <input class="form-control" type="text" name="address" value="{{ old('address') ? old('address') : $details->address }}">
                      </div>
                      <div class="form-group">
                        <label>Р/С</label>
                        <input class="form-control" type="text" name="bank_rs" value="{{ old('bank_rs') ? old('bank_rs') : $details->bank_rs }}">
                      </div>
                      <div class="form-group">
                        <label>Название банка</label>
                        <input class="form-control" type="text" name="bank_name" value="{{ old('bank_name') ? old('bank_name') : $details->bank_name }}">
                      </div>
                      <div class="form-group">
                        <label>БИК</label>
                        <input class="form-control" type="text" name="bank_bik" value="{{ old('bank_bik') ? old('bank_bik') : $details->bank_bik }}">
                      </div>
                      <div class="form-group">
                        <label>К/С</label>
                        <input class="form-control" type="text" name="bank_ks" value="{{ old('bank_ks') ? old('bank_ks') : $details->bank_ks }}">
                      </div>
                      <div class="form-group">
                        <label>ИНН</label>
                        <input class="form-control" type="text" name="bank_inn" value="{{ old('bank_inn') ? old('bank_inn') : $details->bank_inn }}">
                      </div>
                      <div class="form-group">
                        <label>КПП</label>
                        <input class="form-control" type="text" name="bank_kpp" value="{{ old('bank_kpp') ? old('bank_kpp') : $details->bank_kpp }}">
                      </div>
                      <div class="form-group">
                        <label>Директор</label>
                        <input class="form-control" type="text" name="director" value="{{ old('director') ? old('director') : $details->director }}">
                      </div>
                    </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


</section><!-- /.content -->
@stop