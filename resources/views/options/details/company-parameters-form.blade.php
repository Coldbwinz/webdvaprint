<div class="row">
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>
    <script>
        var getDataButtonPressed = false;
        $(document).ready( function() {
            $('.add_inn').click( function() {
                if (!getDataButtonPressed) {
                    getDataButtonPressed = true;
                    var params = {
                        inn: $('#company_inn').val(),
                    };
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                        },
                        url: '/options/get_inn_data',
                        data: params,
                        type: 'post',
                        success: function (response) {
                            getDataButtonPressed = false;
                            var data = JSON.parse(response);
                            $('#company_data').show();
                            if (response.length > 2) {
                                $('input[name="full_name"]').val(data.data.name['full_with_opf']);
                                $('input[name="name"]').val(data.data.name['short_with_opf']);
                                $('input[name="inn"]').val(data.data.inn);
                                $('input[name="ogrn"]').val(data.data.ogrn);
                                $('input[name="post_address"]').val('');
                                if (data.data.type == "INDIVIDUAL") {
                                    $('input[name="director"]').val(data.data.name.full);
                                    $('input[name="director_position"]').val(data.data.opf.full);
                                    $('input[name="address"]').val('');
                                    $('input[name="kpp"]').prop('required', false).val('').closest('div').hide();
                                    $('#company_data').find('h3').html('Реквизиты предпринимателя');
                                } else {
                                    $('input[name="director"]').val(data.data.management.name);
                                    $('input[name="director_position"]').val(data.data.management.post);
                                    $('input[name="address"]').val(data.address.data.postal_code + ', ' + data.address.data.country + ', ' + data.address.unrestrictedValue);
                                    $('input[name="kpp"]').prop('required', true).val(data.data.kpp).closest('div').css('display', 'table');
                                    $('#company_data').find('h3').html('Реквизиты компании');
                                }
                            }
                            closeIfFilled();
                        },
                        failed: function (data) {
                            getDataButtonPressed = false;
                            console.log("ajax error");
                        }
                    });
                }
            });

            $('.add_bik').click( function() {
                if (!getDataButtonPressed) {
                    getDataButtonPressed = true;
                    var params = {
                        bik: $('#bank_bik').val(),
                    };
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                        },
                        url: '/options/get_bik_data',
                        data: params,
                        type: 'post',
                        success: function (response) {
                            getDataButtonPressed = false;
                            $('#bank_data').show();
                            var data = JSON.parse(response);
                            if (response.length > 2) {
                                $('input[name="bank_name"]').val(data.full_data.name.full);
                                $('input[name="bank_bik"]').val(data.full_data.bic);
                                $('input[name="bank_ks"]').val(data.full_data.correspondentAccount);
                            }
                            closeIfFilled();
                        },
                        failed: function (data) {
                            getDataButtonPressed = false;
                            console.log("ajax error");
                        }
                    });
                }
            });

            $('#company_rs').on('input', function() {
                $('input[name="bank_rs"]').val($('#company_rs').val());
            });

            $('.add_inn, .add_bik').on('click', function () {
                if (!getDataButtonPressed) {
                    closeIfFilled();
                }
            });
            function closeIfFilled() {
                if (($('input[name="full_name"]').val().length > 0) && ($('input[name="bank_ks"]').val().length > 0) &&
                        ($('input[name="bank_rs"]').val().length > 0)) {
                    $('#collapsed_container').collapse('hide');
                }
            }
        });
    </script>
    <div class="col-md-6">
        <div class="box" style="border-top: none;">
            <button type="button" class="btn btn-lg collapsed"
                    style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                    data-toggle="collapse"
                    data-target="#collapsed_container">Добавить реквизиты компании
            </button>
            <div id="collapsed_container" class="collapse">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="box-header no-padding">
                        <h3 class="box-title">Заполните только три поля, всё остальное добавит система</h3>
                    </div>
                    <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                        <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">ИНН</label>
                        <input class="form-control" style="display: table-cell;" type="number" id="company_inn" value="{{ old('company_inn') }}"/>
                        <span class="input-group-addon btn btn-default add_inn">+ Добавить</span>
                    </div>
                    <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                        <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">Расчётный счёт</label>
                        <input class="form-control" style="display: table-cell;" type="number" id="company_rs" value="{{ old('company_rs') }}"/>
                    </div>
                    <div class="input-group col-md-12 no-padding" style="display: table; margin-top: 10px;">
                        <label style="font-size:12px; display: table-cell; vertical-align: middle; width:200px; text-align:right; padding-right: 10px;">БИК</label>
                        <input class="form-control" style="display: table-cell;" type="number" id="bank_bik" value="{{ old('bank_bik') }}"/>
                        <span class="input-group-addon btn btn-default add_bik">+ Добавить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>