@extends('app')

@section('content')
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>

    <div class="row">
        <div class="col-md-6">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg collapsed"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;" data-toggle="collapse"
                        data-target="#collapsed_container">Добавить организацию
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>
                    <form method="POST" action="{{ route('details.store') }}" autocomplete="off" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            @if ($errors->has())
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Название организации</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label>Почтовый индекс</label>
                                <input class="form-control" type="text" name="post_address" value="{{ old('post_address') }}">
                            </div>
                            <div class="form-group">
                                <label>Адрес</label>
                                <input class="form-control" type="text" name="address" value="{{ old('address') }}">
                            </div>
                            <div class="form-group">
                                <label>Р/С</label>
                                <input class="form-control" type="text" name="bank_rs" value="{{ old('bank_rs') }}">
                            </div>
                            <div class="form-group">
                                <label>Название банка</label>
                                <input class="form-control" type="text" name="bank_name" value="{{ old('bank_name') }}">
                            </div>
                            <div class="form-group">
                                <label>БИК</label>
                                <input class="form-control" type="text" name="bank_bik" value="{{ old('bank_bik') }}">
                            </div>
                            <div class="form-group">
                                <label>К/С</label>
                                <input class="form-control" type="text" name="bank_ks" value="{{ old('bank_ks') }}">
                            </div>
                            <div class="form-group">
                                <label>ИНН</label>
                                <input class="form-control" type="text" name="bank_inn" value="{{ old('bank_inn') }}">
                            </div>
                            <div class="form-group">
                                <label>КПП</label>
                                <input class="form-control" type="text" name="bank_kpp" value="{{ old('bank_kpp') }}">
                            </div>
                            <div class="form-group">
                                <label>Директор</label>
                                <input class="form-control" type="text" name="director" value="{{ old('director') }}">
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        th, td {
            text-align: center;
        }

        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">

            @if (Session::has('message.success'))
                <div class="alert alert-success">
                    {{ Session::get('message.success') }}
                </div>
            @endif

            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th width="50">Номер</th>
                            <th width="150">Название</th>
                            <th width="160">Директор</th>
                            <th width="140">Почтовый адрес</th>
                            <th width="140">Адрес</th>
                            <th width="140">Р/С</th>
                            <th width="140">ИНН</th>
                            <th width="140">Включен</th>
                            <th width="140">Действия</th>
                        </tr>

                        @foreach ($details as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->director }}</td>
                                <td>{{ $value->post_address }}</td>
                                <td>{{ $value->address }}</td>
                                <td>{{ $value->bank_rs }}</td>
                                <td>{{ $value->bank_inn }}</td>
                                <td>
                                    @if ($value->activity == 1)
                                        <a href="/options/details/off/{{ $value->id }}">
                                            <button class="btn btn-danger btn-xs">выключить</button>
                                        </a>
                                    @else
                                        <a href="/options/details/on/{{ $value->id }}">
                                            <button class="btn btn-success btn-xs">включить</button>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="/options/details/edit/{{ $value->id }}">
                                        <button class="btn btn-success btn-xs">редактировать</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {!! (new Landish\Pagination\Pagination($details))->render() !!}

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    </section><!-- /.content -->
@stop