<div id="bank_data" class="col-md-12" @if (!isset($details) || !isset($details->bank_name))  style="display: none;" @endif>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Банковские реквизиты</h3>
        </div>
        <div class="box-body">
            <div class="form-group col-md-12 no-padding" style="display: table; margin-bottom: 0;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">Расчётный счёт</label>
                <input class="form-control" style="display: table-cell;" type="text" name="bank_rs" required
                       value="{{ old('bank_rs', ($details) ? $details->bank_rs : '') }}"/>
            </div>
            <div class="form-group">
                <label style="font-size:12px;">Наименование банка</label>
                <input class="form-control" type="text" name="bank_name" required
                       value="{{ old('bank_name', ($details) ? $details->bank_name : '') }}"/>
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">Корреспондентский счёт</label>
                <input class="form-control" style="display: table-cell;" type="text" name="bank_ks" required
                       value="{{ old('bank_ks', ($details) ? $details->bank_ks : '') }}"/>
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">БИК</label>
                <input class="form-control" style="display: table-cell;" type="text" name="bank_bik" required
                       value="{{ old('bank_bik', ($details) ? $details->bank_bik : '') }}"/>
            </div>
        </div>
    </div>
</div>