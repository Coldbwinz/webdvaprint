<script>
    $(document).ready(function() {
        $('#sameAsAddress').click(function(e){
            var newaddress = $('input[name="address"]').val();
            tryToDeleteRealAddress();
            if (e.currentTarget.checked) {
                $('input[name="post_address"]').val(newaddress);
                addAddress();
            } else {
                $('input[name="post_address"]').val('');
            }
            showAdresses();
        });

        $('input[name="post_address"]').focusin(function() {
            tryToDeleteRealAddress();
        });

        $('input[name="post_address"]').focusout(function() {
            addAddress();
            showAdresses();
        });

        function tryToDeleteRealAddress() {
            var newaddress = $('input[name="post_address"]').val();
            if ($.inArray(newaddress, settings.issuingAddresses) != -1) {
                settings.issuingAddresses.splice($.inArray(newaddress, settings.issuingAddresses), 1);
            }
            if ($.inArray(newaddress, settings.printingAddresses) != -1) {
                settings.printingAddresses.splice($.inArray(newaddress, settings.printingAddresses), 1);
            }
            newaddress = newaddress.replace(/^[0-9]{6}[,][ ]/, '');
            if ($.inArray(newaddress, settings.issuingAddresses) != -1) {
                settings.issuingAddresses.splice($.inArray(newaddress, settings.issuingAddresses), 1);
            }
            if ($.inArray(newaddress, settings.printingAddresses) != -1) {
                settings.printingAddresses.splice($.inArray(newaddress, settings.printingAddresses), 1);
            }
        }
        function addAddress() {
            var newaddress = $('input[name="post_address"]').val();
            newaddress = newaddress.replace(/^[0-9]{6}[,][ ]/, '');
            if (newaddress.length > 0) {
                if ($.inArray(newaddress, settings.issuingAddresses) == -1) {
                    settings.issuingAddresses.push(newaddress);
                }
                if ($.inArray(newaddress, settings.printingAddresses) == -1) {
                    settings.printingAddresses.push(newaddress);
                }
            }
        }
    });
</script>

<div id="company_data" class="col-md-12" @if (!isset($details) || !isset($details->name))  style="display: none;" @endif>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Реквизиты компании</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label style="font-size:12px;">Полное наименование</label>
                <input class="form-control" type="text" name="full_name" required
                       value="{{ old('full_name', ($details) ? $details->full_name : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">Краткое наименование</label>
                <input class="form-control" style="display: table-cell;" type="text" name="name" required
                       value="{{ old('short_name', ($details) ? $details->name : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">ФИО руководителя</label>
                <input class="form-control" style="display: table-cell;" type="text" name="director" required
                       value="{{ old('director_fio', ($details) ? $details->director : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">Должность руководителя</label>
                <input class="form-control" style="display: table-cell;" type="text" name="director_position" required
                       value="{{ old('director_position', ($details) ? $details->director_position : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">ИНН</label>
                <input class="form-control" style="display: table-cell;" type="text" name="inn" required
                       value="{{ old('company_inn', ($details) ? $details->inn : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding"
                 style="display: @if ($details && $details->inn && (strlen($details->inn) != 12)) table @else none @endif;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">КПП</label>
                <input class="form-control" style="display: table-cell;" type="text" name="kpp" @if ($details && $details->inn && (strlen($details->inn) != 12)) require @endif
                       value="{{ old('company_kpp', ($details) ? $details->kpp : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding" style="display: table;">
                <label style="font-size:12px; display: table-cell; width:250px; text-align:right; padding-right: 10px;">ОГРН</label>
                <input class="form-control" style="display: table-cell;" type="text" name="ogrn" required
                       value="{{ old('company_ogrn', ($details) ? $details->ogrn : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding">
                <label style="font-size:12px;">Юридический адрес</label>
                <input class="form-control" style="font-size:12px;" type="text" name="address" required
                       value="{{ old('company_ur_adress', ($details) ? $details->address : '') }}">
            </div>
            <div class="form-group col-md-12 no-padding">
                <label style="font-size:12px; margin-right: 15px;">Фактический адрес <input id="sameAsAddress"
                   @if ($details && ($details->post_address == $details->address) && (strlen($details->address) > 0)) checked @endif
                    type="checkbox" value="" style="margin-left:5px; height:14px; vertical-align: text-bottom;">
                    <font style="font-weight: normal;"> соответствует юридическому</font></label>
                <input class="form-control" style="font-size:12px;" type="text" name="post_address" required
                       value="{{ old('company_post_address', ($details) ? $details->post_address : '') }}">
            </div>
        </div>
    </div>
</div>
