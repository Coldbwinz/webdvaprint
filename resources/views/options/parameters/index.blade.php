@extends('app')

@section('content')
    @if ($errors->count())
        <div class="alert alert-danger">
            <strong>Ошибка!</strong>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('options.details.company-parameters-form')

    <form action="{{ route('settings.update') }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div id="settings" class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header">
                            <h3 class="box-title">Основные настройки</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-4">
                                <label style="font-size:12px;" for="logo">Логотип компании</label>
                                <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
                                    <logo name="logo"
                                          src="{{ $settings->get('logo') }}"
                                          handler="{{ route('settings.logo') }}"
                                          width="250"
                                          height="195"
                                          class-list="img-responsive"></logo>

                                    @if ($errors->has('logo'))
                                        <div class="help-block">{{ $errors->first('logo') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-8 no-padding">
                                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                    <label style="font-size:12px;" for="company_name">Название сервиса</label>

                                    <input id="company_name"
                                           type="text"
                                           name="company_name"
                                           value="{{ old('company_name', $settings->get('company_name')) }}"
                                           class="form-control"/>

                                    @if ($errors->has('company_name'))
                                        <div class="help-block">{{ $errors->first('company_name') }}</div>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('color_primary') ? ' has-error' : '' }}"
                                     style="padding-left:0;">
                                    <label style="font-size:12px;">Фирменный цвет</label>
                                    <div class="input-group color_primary">
                                        <input id="color_primary"
                                               name="color_primary"
                                               value="{{ old('color_primary', $settings->get('color_primary')) }}"
                                               placeholder="#367FA9"
                                               type="text" class="form-control">

                                        <div class="input-group-addon" style="padding: 2px 2px;">
                                            <i style="width:28px; height:28px;"></i>
                                            <i class="fa fa-caret-down"
                                               style="position:absolute; width:12px; height:12px; margin-top: 15px; margin-left:-12px; background-color:#fff;"></i>
                                        </div>
                                    </div>
                                    @if ($errors->has('color_primary'))
                                        <div class="help-block">{{ $errors->first('color_primary') }}</div>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('color_secondary') ? ' has-error' : '' }}"
                                     style="padding-left:0; padding-right:0;">
                                    <label style="font-size:12px;">Доп. цвет</label>
                                    <div class="input-group color_secondary">
                                        <input id="color_secondary"
                                               name="color_secondary"
                                               value="{{ old('color_secondary', $settings->get('color_secondary')) }}"
                                               placeholder="#3C8DBC"
                                               type="text" class="form-control">

                                        <div class="input-group-addon" style="padding: 2px 2px;">
                                            <i style="width:28px; height:28px;"></i>
                                            <i class="fa fa-caret-down"
                                               style="position:absolute; width:12px; height:12px; margin-top: 15px; margin-left:-12px; background-color:#fff;"></i>
                                        </div>
                                    </div>
                                    @if ($errors->has('color_secondary'))
                                        <div class="help-block">{{ $errors->first('color_secondary') }}</div>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                                    <label style="font-size:12px;" for="meta_title">Заголовок вкладки</label>

                                    <input id="meta_title"
                                           type="text"
                                           name="meta_title"
                                           value="{{ old('meta_title', $settings->get('meta_title')) }}"
                                           class="form-control"
                                           maxlength="200"
                                    />

                                    @if ($errors->has('meta_title'))
                                        <div class="help-block">{{ $errors->first('meta_title') }}</div>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="form-group col-md-3 {{ $errors->has('country') ? ' has-error' : '' }}"
                                     style="padding-left:0;">
                                    <label style="font-size:12px;" for="country">Страна</label>

                                    <select name="country" id="country" class="form-control">
                                        <option value="russia"{{ old('country', $settings->get('country')) === 'russia' ? ' selected' : '' }}>
                                            Россия
                                        </option>
                                        <option value="byelorussia"{{ old('country', $settings->get('country')) === 'byelorussia' ? ' selected' : '' }}>
                                            Белоруссия
                                        </option>
                                    </select>

                                    @if ($errors->has('country'))
                                        <div class="help-block">{{ $errors->first('country') }}</div>
                                    @endif
                                </div>

                                <div class="form-group col-md-3 {{ $errors->has('user_timezone') ? ' has-error' : '' }}"
                                     style="padding-left:0;">
                                    <label style="font-size:12px;" for="user_timezone">Часовой пояс</label>
                                    <select name="user_timezone" id="user_timezone" class="form-control">
                                        @foreach(DateTimeZone::listIdentifiers() as $timezone)
                                            <option value="{{ $timezone }}"
                                                    {{ old('user_timezone', settings('user_timezone')) === $timezone ? ' selected' : '' }}>
                                                {{ $timezone }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('user_timezone'))
                                        <div class="help-block">{{ $errors->first('user_timezone') }}</div>
                                    @endif
                                </div>

                                <div class="form-group col-md-3" style="padding-left:0;">
                                    <label style="font-size:12px;" for="invoice_number">№ следующего счета</label>
                                    <input type="number"
                                           id="invoice_number"
                                           name="invoice_number"
                                           value="{{ old('invoice_number', $settings->get('invoice_number')) }}"
                                           class="form-control"
                                           min="{{ $settings->get('invoice_number') }}"
                                    />

                                    @if ($errors->has('invoice_number'))
                                        <div class="help-block">{{ $errors->first('invoice_number') }}</div>
                                    @endif
                                </div>

                                <div class="form-group col-md-3 no-padding">
                                    <label style="font-size:12px;" for="act_number">№ следующего акта</label>
                                    <input type="number"
                                           id="act_number"
                                           name="act_number"
                                           value="{{ old('act_number', $settings->get('act_number')) }}"
                                           class="form-control"
                                           min="{{ $settings->get('act_number') }}"
                                    />

                                    @if ($errors->has('act_number'))
                                        <div class="help-block">{{ $errors->first('act_number') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 no-padding{{ $errors->has('email_title') ? ' has-error' : '' }}">
                                <label style="font-size:12px;" for="email_title">Торговая марка (указывается в письмах
                                    клиенту)</label>

                                <input id="email_title"
                                       type="text"
                                       name="email_title"
                                       value="{{ old('email_title', $settings->get('email_title')) }}"
                                       class="form-control"/>

                                @if ($errors->has('email_title'))
                                    <div class="help-block">{{ $errors->first('email_title') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        @if (! \App\Subdomain::findCurrent())
                            <div id="printingAddresses">
                                <address-items title="Адреса типографии"
                                               selectsame='<div class="form-control">
                                            <input type="radio" style="height: 10px;" name="addressSame" value="0"> Совпадает с юридическим адресом</input>
                                            <input type="radio" style="height: 10px; margin-left: 20px;" name="addressSame" value="1"> Совпадает с почтовым адресом</input>
                                            </div>'
                                               name="printingAddresses[]"
                                               placeholder="Адрес типографии"
                                               errors="{{ $errors->has('printingAddresses') ? $errors->first('printingAddresses') : '' }}"
                                               :items="printingAddresses"
                                ></address-items>
                            </div>
                        @endif
                        <div id="issuingAddresses">
                            <address-items title="Адреса выдачи"
                                           name="issuingAddresses[]"
                                           placeholder="Адрес выдачи"
                                           errors="{{ $errors->has('issuingAddresses') ? $errors->first('issuingAddresses') : '' }}"
                                           :items="issuingAddresses"
                            ></address-items>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 no-padding">
                        @include('options.details.company_data_form')
                        @include('options.details.bank_data_form')
                </div>
        </div>
        <div class="box">
            <div class="box-body">
                <button type="submit" class="btn btn-primary btn-lg">
                    <i class="fa fa-save"></i> Сохранить
                </button>
            </div>
        </div>
    </form>
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/cropperjs/cropper.min.css') }}">
@endsection

@section('scripts')
    <script type="text/x-template" id="logoComponentTempalate">
        <div class="picture">
            <img :src="imageSrc"
                 :class="classList"
            @click="selectFileTrigger"
            title="Загрузить другое изображение"
            style="cursor: pointer
            ;"
            :onError="onErrorImage"
            :width="width"
            :height="height"
            />

            <input :id="identificator"
                   type="file"
                   class="form-control hidden"
            @change="selectLogo"
            />

            <input type="hidden" :name="name" :value="imageSrc"/>

            <div class="modal" :id="modalId">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Редактировать изображение</h4>
                        </div>

                        <div class="modal-body">
                            <div style="margin-bottom: 20px;">
                                <canvas :id="cropperAreaId"></canvas>
                            </div>

                            <div class="cropper-controls">
                                <div class="btn-group">
                                    <button type="button"
                                            :class="{'btn': true, 'btn-primary': true, 'active': dragMode === 'move'}"
                                            title="Режим перемещения изображения" @click="dragMode = 'move'">
                                    <i class="fa fa-arrows"></i>
                                    </button>
                                    <button type="button"
                                            :class="{'btn': true, 'btn-primary': true, 'active': dragMode === 'crop'}"
                                            title="Режим выбора области" @click="dragMode = 'crop'">
                                    <i class="fa fa-crop"></i>
                                    </button>
                                </div>

                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" title="Увеличить изображение" @click="
                                    zoom(0.1)">
                                    <i class="fa fa-search-plus"></i>
                                    </button>
                                    <button type="button" class="btn btn-primary" title="Уменьшить изображение" @click="
                                    zoom(-0.1)">
                                    <i class="fa fa-search-minus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-primary"
                                    @click.prevent="cropImage"
                                    :disabled="isBusy"
                            >Применить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="addressComponentTemplate">
        <div :class="{'box': true, 'box-default': hasErrors === 0, 'box-danger': hasErrors > 0}">
            <div class="box-header">
                <h3 class="box-title" v-text="title"></h3>
            </div>

            <div class="box-body">
                <div v-if="hasErrors" class="alert alert-danger" v-html="errors"></div>

                <div class="alert alert-info" v-show="addresses.length === 0">
                    Нет адресов.
                </div>

                <ul class="list-group">
                    <li v-for="(address, index) in addresses"
                        class="list-group-item"
                    >
                        <input type="hidden" :name="name" :value="address"/>
                        @{{ address }}

                        <button type="button"
                                class="btn btn-danger btn-xs pull-right"
                                title="Удалить адрес"
                                @click.prevent="deleteAddress(index)"
                        >
                            <i class="fa fa-trash"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class="box-footer">
                <div :class="{ 'input-group': true, 'has-error': hasErrors }">
                    <input type="text"
                           class="form-control"
                           :placeholder="placeholder"
                           v-model="newAddress"
                           @keypress.enter.prevent="addAddress"
                    />
                    <span class="input-group-btn">
                        <button class="btn btn-default"
                                type="button"
                                @click.prevent="addAddress"
                                :disabled="newAddress.length === 0"
                        >
                            <i class="fa fa-plus"></i> Добавить адрес
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </script>

    <script src="{{ asset('plugins/cropperjs/cropper.min.js') }}"></script>
    <script src="{{ asset('js/components/settings.js') }}"></script>
    <script src="{{ asset('js/components/colorpicker.js') }}"></script>
    <script src="{{ asset('js/components/logo.js') }}"></script>

    <script>
        var settings = new Vue({
            el: '#settings',

            data: {
                issuingAddresses: [],
                printingAddresses: []
            },

            mounted: function () {
                /* Передача JSON с ковычками json array double quotes */
                this.issuingAddresses = {!! json_encode($settings->get('issuingAddresses')) !!} || [];
                this.printingAddresses = {!! json_encode($settings->get('printingAddresses')) !!} || [];
            }
        });

        $(function () {
            $(".color_primary").colorpicker();
            $(".color_secondary").colorpicker();
        });

        $(document).ready(function () {
            showAdresses();

        });

        function showAdresses() {
            $('#issuingAddresses').toggle(settings.issuingAddresses.length > 0);
            $('#printingAddresses').toggle(settings.printingAddresses.length > 0);
        }
    </script>
@endsection
