<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">* {
            font-family: "times";
        }

        table tr td {
            border: 1px solid black;
        }

        .border0px {
            border: 0;
        }</style>
</head>
<body TEXT="#000000">
<div>{{ $detail->name }}<br><br></div>
<div>{{ "Юридический адрес:".$detail->address }}
    @if ($detail->address != $detail->post_address)
        <br>Почтовый адрес:{{ $detail->post_address }}
    @endif
    <br><br>
</div>
<div style="text-align:center;">Образец заполнения платежного поручения</div>
<table cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td>ИНН {{ $detail->bank_inn }}</td>
        <td>КПП {{ $detail->bank_kpp }}</td>
        <td rowspan="2" style="vertical-align:bottom;">Сч. №</td>
        <td rowspan="2" style="vertical-align:bottom;">{{ $detail->bank_rs }}</td>
    </tr>
    <tr>
        <td colspan="2">Получатель<br>{{ $detail->name }}</td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2">Банк получателя<br>{{ $detail->bank_name }}</td>
        <td>БИК</td>
        <td>{{ $detail->bank_bik }}</td>
    </tr>
    <tr>
        <td>Сч. №</td>
        <td>{{ $detail->bank_ks }}</td>
    </tr>
</table>
<?php
$months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
?>
<div style="text-align:center;"><br>СЧЕТ № {{ $invoice_number }}
    от {{ date("d") }} {{ $months[date("n") - 1] }} {{ date("Y") }} г.<br><br><br><br></div>
<div>Плательщик: {{ $payer }}<br><br></div>
<div>Грузополучатель: {{ $payer }}<br><br></div>
<table cellpadding="5" cellspacing="0" width="100%">
    <tr style="text-align:center;">
        <td width="20px">№</td>
        <td width="350px">Наименование<br>товара</td>
        <td width="50px">Коли-<br>чество</td>
        <td width="50px">Единица<br>изме-<br>рения</td>
        <td>Цена</td>
        <td>Сумма</td>
    </tr>
    <?php
    use App\Http\Controllers\GroupOrdersController;
    use App\ProductType;use App\Urgency;
    $summ = 0;
    $names_index = 0;
    $order_index = 0;
    ?>
    @foreach ( $group_orders as $order )
        <?php
            $order_index++;
            $opt_index = 1;
            $names = '';
            $names_options = '';
            $names_index++;

            $order_summ = 0;
            $approx_koef = 0;

            $user_count = $order->draw;
            if ($order->type_product != 999999) {
                $urgency_table = Urgency::where('subdomain_id', null)->get();
                $product_type = ProductType::find($order->type_product);

                $product_name = $product_type->name;
                $product_info = (new GroupOrdersController)->getPriceTableForProduct($order->type_product, $order->client_id);
                $group_selected_index = $order->price_details['price_index'];
                $service_info = $product_info->$group_selected_index;
                $service_info->counts = $product_info->counts;
                foreach ($urgency_table as $urgency) {
                    if ($urgency->name == $order->price_details['selected_urgency']) {
                        break;
                    }
                }

                $selectedPriceIndex = 0;
                $koef_price = 1;
                $counts = count($service_info->counts);
                $complects_discount_percent = (isset($order->price_details['complects_discount_percent'])) ? (100 - $order->price_details['complects_discount_percent']) / 100 : 1;
                if (!isset($order->price_details['price_change_reason']) && ($order->price_details['complects_calculation_type'] != 1)) {
                    $payment_koeff = ( 100 + $order->price_details['selected_payment_type']['discount'] ) / 100;
                    $need_approx = false;
                    if ($user_count <= $service_info->counts[0]) {
                        $koef_price = $service_info->counts[0] / $order->draw;
                        $user_count = $service_info->counts[0];
                        $selectedPriceIndex = 0;
                    } else if ($user_count >= $service_info->counts[$counts - 1]) {
                        $koef_price = 1;
                        $selectedPriceIndex = $counts - 1;
                    } else {
                        foreach ($service_info->counts as $i => $item) {
                            if ($user_count < $item) {
                                $selectedPriceIndex = $i - 1;
                                $need_approx = ($user_count > $service_info->counts[0]) && ($user_count < $service_info->counts[$counts - 1]);
                                if ($need_approx) {
                                    $count1 = $service_info->counts[$selectedPriceIndex];
                                    $count2 = $service_info->counts[$selectedPriceIndex + 1];
                                    $approx_koef = ($user_count - $count1) / ($count2 - $count1);
                                }
                                break;
                            }
                        }
                    }

                    foreach ($service_info->products as $index => $service) {
                        if ($service == $order->price_details['selected_product']) {
                            if ($need_approx) {
                                $price1 = $service_info->products_prices[$index * $counts + $selectedPriceIndex];
                                $price2 = $service_info->products_prices[$index * $counts + $selectedPriceIndex + 1];
                                $approxPrice = $price1 - ($price1 - $price2) * $approx_koef;
                            } else {
                                $approxPrice = $service_info->products_prices[$index * $counts + $selectedPriceIndex];
                            }
                            $approxPrice *= $complects_discount_percent;
                            $approxPrice *= $payment_koeff;
                            $summ_part = $approxPrice * $user_count;
                            $summ += $summ_part;
                            $order_summ = $summ_part;
                            break;
                        };
                    };
                    foreach ($service_info->options as $index => $service) {
                        foreach ($order->price_details['selected_options'] as $option_index => $option) {
                            if ($service == $option) {
                                if ($need_approx) {
                                    $price1 = $service_info->options_prices[$index * $counts + $selectedPriceIndex];
                                    $price2 = $service_info->options_prices[$index * $counts + $selectedPriceIndex + 1];
                                    $approxPrice = $price1 - ($price1 - $price2) * $approx_koef;
                                } else {
                                    $approxPrice = $service_info->options_prices[$index * $counts + $selectedPriceIndex];
                                }
                                $names_options .= ', ' . $option;
                                $approxPrice *= $complects_discount_percent;
                                $approxPrice *= $payment_koeff;
                                $summ_part = $approxPrice * $user_count;
                                $summ += $summ_part;
                                $order_summ += $summ_part;
                            };
                        }
                    };
                    if ($product_type->printler_type == 3) {
                        if (($order->price_details['selected_type'] == 'на скрепке') && ($order->price_details['selected_cover'] != 'Без обложки')) {
                            foreach ($service_info->covers as $index => $cover) {
                                if ($cover == $order->price_details['selected_cover']) {
                                    $selected_cover_chromacity = $order->price_details['selected_cover_chromacity'];
                                    $index = $index * $counts + $selectedPriceIndex;
                                    if ($need_approx) {
                                        $index1 = $index + 1;
                                        $price1 = $service_info->covers_prices->$selected_cover_chromacity->$index;
                                        $price2 = $service_info->covers_prices->$selected_cover_chromacity->$index1;
                                        $approxPrice = $price1 - ($price1 - $price2) * $approx_koef;
                                    } else {
                                        $approxPrice = $service_info->covers_prices->$selected_cover_chromacity->$index;
                                    }
                                    $approxPrice *= $complects_discount_percent;
                                    $approxPrice *= $payment_koeff;
                                    $summ_part = $approxPrice * $user_count;
                                    $summ += $summ_part;
                                    $order_summ += $summ_part;
                                    break;
                                };
                            };
                        }
                        if (($order->price_details['selected_type'] == 'на прукжине') && ($order->price_details['selected_pad'] != 'Без подложки')) {
                            foreach ($service_info->pads as $index => $pad) {
                                if ($pad == $order->price_details['selected_pad']) {
                                    $selected_pad_chromacity = $order->price_details['selected_pad_chromacity'];
                                    $index = $index * $counts + $selectedPriceIndex;
                                    if ($need_approx) {
                                        $index1 = $index + 1;
                                        $price1 = $service_info->pad_prices->$selected_pad_chromacity->$index;
                                        $price2 = $service_info->pad_prices->$selected_pad_chromacity->$index1;
                                        $approxPrice = $price1 - ($price1 - $price2) * $approx_koef;
                                    } else {
                                        $approxPrice = $service_info->pads_prices->$selected_pad_chromacity->$index;
                                    }
                                    $approxPrice *= $complects_discount_percent;
                                    $approxPrice *= $payment_koeff;
                                    $summ_part = $approxPrice * $user_count;
                                    $order_summ += $summ_part;
                                    $summ += $summ_part;
                                };
                            };
                        }
                    }
                } else {
                    $order_summ = round($order->price_details['price'] / (100 + $urgency->discount) * 100, 2);
                    $summ += $order_summ;
                }

                if ($product_type->printler_type == 1) {
                    $names .= $product_name . ' ' . $service_info->size . ', ' .
                            $order->price_details['selected_product'] . ', ' . $order->price_details['selected_chromacity'];
                } elseif ($product_type->printler_type == 2) {
                    $names .= $product_name . ' ' . $service_info->size . ', (' . $order->price_details['selected_type'] . '), ' .
                            $order->price_details['selected_product'] . ', ' . $order->price_details['selected_chromacity'];
                } else if ($product_type->printler_type == 3) {
                    $names .= $product_name . ' ' . $service_info->size . ', ' . $order->price_details['selected_product'] . 'стр. ';

                    $is_wire = ($order->price_details['selected_type'] == 'на пружине');
                    $no_cover = ($order->price_details['selected_cover'] == 'Без обложки');
                    $inside_block = '(Внутренний блок: ' . $service_info->material . ', ' . $service_info->chromacity;
                    if (!$is_wire) {
                        if ($no_cover) {
                            $names .= $order->price_details['selected_type'] . ' без обложки. ' . $inside_block . ')';
                        } else {
                            $names .= '+ обложка ' . $order->price_details['selected_cover'] .
                                    ', ' . $order->price_details['selected_cover_chromacity'] . '. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover']  .
                                    ', ' . $order->price_details['selected_cover_chromacity']. ')';
                        }
                    } else if ($is_wire) {
                        $no_pad = ($order->price_details['selected_pad'] == 'Без подложки');
                        if ($no_cover && $no_pad) {
                            $names .= $order->price_details['selected_type'] . ' без обложек. ' . $inside_block . ')';
                        } else if ($no_cover) {
                            $names .= '+ подложка ' . $order->price_details['selected_type'] . ' без обложки. ' . $inside_block .
                                    '; Подложка: ' . $order->price_details['selected_pad'] .
                                    ', ' . $order->price_details['selected_pad_chromacity'] . ')';
                        } else if ($no_pad) {
                            $names .= '+ обложка ' . $order->price_details['selected_type'] . ' без подложки. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover'] .
                                    ', ' . $order->price_details['selected_cover_chromacity'] . ')';
                        } else {
                            $names .= '+ обложка ' . $order->price_details['selected_type'] . '. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover'] .
                                    ', ' . $order->price_details['selected_cover_chromacity'] .
                                    '; Подложка: ' . $order->price_details['selected_pad'] .
                                    ', ' . $order->price_details['selected_pad_chromacity'] . ')';
                        }
                    }
                }
            } else {
                foreach ($order->price_details['selected_options'] as $option_index => $option) {
                    $names_options .= ', ' . $option;
                }

                $names .= $order->price_details['selected_product_type_name'] .
                        ' ' . $order->price_details['selected_size'] . ', ';

                if ($order->price_details['printler_type'] == 1) {
                    $names .= $order->price_details['selected_product'] . ', ' . $order->price_details['selected_chromacity'];
                } elseif ($order->price_details['printler_type'] == 2) {
                    $names .= '(' . $order->price_details['selected_type'] . '), ' . $order->price_details['selected_product'] . ', ' . $order->price_details['selected_chromacity'];
                } else if ($order->price_details['printler_type'] == 3) {
                    $names .= $order->price_details['selected_product'] . ' стр. ';

                    $is_wire = ($order->price_details['selected_type'] == 'на пружине');
                    $no_cover = ($order->price_details['selected_cover'] == 'Без обложки');
                    $inside_block = '(Внутренний блок: ' . $order->price_details['selected_material'] . ', ' . $order->price_details['selected_chromacity'];
                    if (!$is_wire) {
                        if ($no_cover) {
                            $names .= $order->price_details['selected_type'] . ' без обложки. ' . $inside_block . ')';
                        } else {
                            $names .= '+ обложка ' . $order->price_details['selected_type'] . '. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover'] . ')';
                        }
                    } else if ($is_wire) {
                        $no_pad = ($order->price_details['selected_pad'] == 'Без подложки');
                        if ($no_cover && $no_pad) {
                            $names .= $order->price_details['selected_type'] . ' без обложек. ' . $inside_block . ')';
                        } else if ($no_cover) {
                            $names .= '+ подложка ' . $order->price_details['selected_type'] . ' без обложки. ' . $inside_block .
                                    '; Подложка: ' . $order->price_details['selected_pad'] . ')';
                        } else if ($no_pad) {
                            $names .= '+ обложка ' . $order->price_details['selected_type'] . ' без подложки. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover'] . ')';
                        } else {
                            $names .= '+ обложка ' . $order->price_details['selected_type'] . '. ' . $inside_block .
                                    '; Обложка: ' . $order->price_details['selected_cover'] .
                                    '; Подложка: ' . $order->price_details['selected_pad'] . ')';
                        }
                    }
                }
                $order_summ = $order->price_details['price'] - $order->price_details['selected_delivery_type']['cost'];
                $summ += $order_summ;
            }
            $names .= $names_options;

            //Подготоваливкм данные для печати информации по препрессу и возможно включаем их в стоимость заказа.
            if (!$step2) {
                if (($order->creator > 0) && ($order->price_details['template_cost'] > 0)) {
                    if (isset($order->price_details['template_cost_hour'])) {
                        $creator_price = round($order->designer_working_time / 3600, 2) * $order->price_details['template_cost_hour'];
                        $price_for_one = $order->price_details['template_cost_hour'] * $complects_discount_percent;
                    } else {
                        $creator_price = $order->price_details['template_cost'];
                        $price_for_one = round($creator_price / $order->sidesOfTemplate() * $complects_discount_percent, 2);
                    }
                    $summ_part = round($creator_price * $complects_discount_percent, 2);
                    $designer_work = ($order->price_details['selected_prepress'] == 3) || ($order->price_details['selected_prepress'] == 4);

                    $td_prepress_index = $order_index.'.'.$opt_index++;

                    if ($order->creator == 1) {
                        $td_prepress_name = 'Шаблон макета';
                    } elseif ($order->creator == 2) {
                        $td_prepress_name = 'Разработка макета';
                    } elseif ($order->creator == 4) {
                        $td_prepress_name = 'Проверка макета';
                    } elseif ($designer_work) {
                        $td_prepress_name = 'Работа дизайнера';
                    }

                    if ($designer_work && isset($order->price_details['template_cost_hour'])) {
                        $td_prepress_count = round($order->designer_working_time / 3600, 2);
                    } else {
                        $td_prepress_count = $order->sidesOfTemplate();
                    }

                    if ($designer_work && isset($order->price_details['template_cost_hour'])) {
                        $td_prepress_type = 'час';
                    } else {
                        if ($order->price_details['printler_type'] == 3) {
                            $td_prepress_type = 'стр.';
                        } else {
                            if ($designer_work) {
                                $td_prepress_type = 'экз.';
                            } else {
                                $td_prepress_type = 'шт.';
                            }
                        }
                    }

                    if (!isset($order->price_details['price_change_reason'])) {
                        if ($order->price_details['prepress_in_one_line'] == 1) {
                            dump('here');
                            $summ += $summ_part;
                            $order_summ += $summ_part;
                        }
                    }
                }
            }
        ?>

        <tr>
            <td style="text-align:left;">{{ $order_index }}</td>
            <td>{{ $names }}</td>
            <td style="text-align:right;">{{ $order->draw }}</td>
            <td style="text-align:center;">шт</td>
            <td style="text-align:right;">{{ round($order_summ / $user_count, 2) }}</td>
            <td style="text-align:right;">{{ $order_summ }}</td>
        </tr>

        <!--Если макет не надо объединять - выводим отдкльной строкой-->
        @if (isset($td_prepress_name) && ($order->price_details['prepress_in_one_line'] == 0))
            <tr>
                <td style="text-align:left;">{{ $td_prepress_index }}</td>
                <td>{{ $td_prepress_name }}</td>
                <td style="text-align:right;">{{ $td_prepress_count }}</td>
                <td style="text-align:center;">{{ $td_prepress_type }}</td>
                <td style="text-align:right;">{{ $price_for_one }}</td>
                <td style="text-align:right;">{{ $summ_part }}</td>
            </tr>
            <?php
                $names_index++;
                if (!isset($order->price_details['price_change_reason'])) {
                    $summ += $summ_part;
                    $order_summ += $summ_part;
                }
            ?>
        @endif

        @if ($order->price_details['selected_delivery_type']['cost'] != 0)
            <?php
                $names_index++;
                $summ_part = $order->price_details['selected_delivery_type']['cost'];
                $summ += $summ_part;
                $order_summ += $summ_part;
            ?>
            <tr>
                <td style="text-align:left;">{{ $order_index }}.{{ $opt_index++ }}</td>
                <td>{{ $order->price_details['selected_delivery_type']['name'] }}</td>
                <td style="text-align:right;">1</td>
                <td style="text-align:center;">шт.</td>
                <td style="text-align:right;">{{ $summ_part }}</td>
                <td style="text-align:right;">{{ $summ_part }}</td>
            </tr>
        @endif

        @if (($order->type_product != 999999) && ($urgency->discount != 0))
            <?php
            $names_index++;
            $summ_discount = round($order_summ * $urgency->discount / 100, 2);
            ?>
            <tr>
                <td style="text-align:left;">{{ $order_index }}.{{ $opt_index++ }}</td>
                <td>
                    @if ($urgency->discount > 0)
                        Наценка
                    @else
                        Скидка
                    @endif
                    за срочность({{ $urgency->name }})
                </td>
                <td style="text-align:right;">{{ $urgency->discount }}</td>
                <td style="text-align:center;">%</td>
                <td style="text-align:right;">-</td>
                <td style="text-align:right;">{{ $summ_discount }}</td>
            </tr>
            <?php
            $summ += $summ_discount;
            ?>
        @endif
        <?php
        if (isset($order->price_details['price_change_reason'])) {
            $summ += $order->price_details['template_cost'];
        }
        ?>
    @endforeach
    <tr style="text-align:right;">
        <td colspan="5" class="border0px">Итого:</td>
        <td>{{ $summ }}</td>
    </tr>
    <tr style="text-align:right;">
        <td colspan="5" class="border0px">НДС:</td>
        <td>-</td>
    </tr>
    <tr style="text-align:right;">
        <td colspan="5" class="border0px">Всего к оплате:</td>
        <td>{{ $summ }}</td>
    </tr>
</table>
<br><br>
<div>Всего наименований {{ $names_index }}, на сумму {{ $summ }}</div>
<div>{{ num2str($summ) }}<br><br><br><br></div>
<div>Руководитель предприятия_____________________ ({{ $detail->director }})<br><br></div>
<div>Главный бухгалтер____________________________ ({{ $detail->director }})</div>
</body>
</html>