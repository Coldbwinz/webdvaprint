<script>
    $(document).ready(function () {
        $('#upload').on("change", function () {
            $('.modal').show();
        });

        $('.close').on("click", function () {
            $('.modal').hide();
        });
    });
</script>

<div class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Редактировать изображений</h4>
            </div>
            <div class="modal-body">
                <div id="uploaded-image"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-croppie">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div>
    <input type="hidden" id="imagebase64" name="imagebase64">
    <input type="hidden" name="MAX_FILE_SIZE" value="8192000"/>
    <input id="upload" name="photo" type="file" value=""/>
</div>