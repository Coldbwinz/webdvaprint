@extends('app')

@section('content')
    <link rel="stylesheet" href="/plugins/croppie/croppie.css"/>
    <script src="/plugins/croppie/croppie.js"></script>
    <script src="/plugins/croppie/croppie_code.js"></script>
    <style>
        .btn.btn-lg:after {
            font-family: "Glyphicons Halflings";
            content: "\e114";
            float: right;
            margin-left: 15px;
        }

        .btn.btn-lg.collapsed:after {
            content: "\e080";
        }
    </style>
    <div class="row">
        <div class="col-md-6">
            <div class="box" style="border-top: none;">
                <button type="button" class="btn btn-lg collapsed"
                        style="width: 100%; height:42px; background-color: #d2d6de; text-align: left;"
                        data-toggle="collapse"
                        data-target="#collapsed_container">Добавить нового пользователя
                </button>
                <div id="collapsed_container" @if (!$errors->has()) class="collapse" @endif>
                    <form id="userForm" method="POST" action="/users/new" autocomplete="off"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        <div class="box-body">
                            @if ($errors->has())
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Фото</label>
                                @include('users.croppie')
                            </div>

                            <div class="form-group">
                                <label>Имя</label>
                                <input class="form-control" type="text" name="name"
                                       required value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                <label>Фамилия</label>
                                <input class="form-control" name="lastname"
                                       required value="{{ old('lastname') }}"/>
                            </div>

                            <div class="form-group">
                                <label>Должность</label>
                                <input class="form-control" name="position"
                                       required value="{{ old('position') }}"/>
                            </div>

                            <div class="form-group">
                                <label>Эл. почта</label>
                                <input class="form-control" type="email" name="email"
                                       required value="{{ old('email') }}"/>
                            </div>

                            <div class="form-group">
                                <label>Телефон</label>
                                <input class="form-control" type="name" name="phone"
                                       required value="{{ old('phone') }}"/>
                            </div>

                            <div class="form-group">
                                <label>Права</label>
                                <select class="form-control" name="id_group">
                                    <?php $i = 0; ?>
                                    @foreach ($users_groups as $value)
                                        <?php
                                        $i++;
                                        if ($value->id !== 4) {
                                        ?>
                                        <option @if ( ( old('id_group') == $i) ) selected="selected"
                                                @endif value="{{ $value->id }}">{{ $value->name }}</option>
                                        <?php } ?>
                                    @endforeach
                                </select>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    </div>

    <style>
        td {
            vertical-align: middle !important;
        }
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">

                        <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Аватар</th>
                            <th>Наименование</th>
                            <th>Тип пользователя</th>
                            <th>Действия</th>
                        </tr>
                            @foreach ($users as $person)
                                <tr>
                                    <td>{{ $person->id }}</td>

                                    {{-- Аватар --}}
                                    <td>
                                        @if ($person->photo)
                                            <img src="{{ $person->photo }}" style="vertical-align: top; max-width: 90px">
                                        @endif
                                    </td>

                                    {{-- Наименование --}}
                                    <td>
                                        @if ($person->client)
                                            @if ($person->client->is_company)
                                                {{ $person->client->tm_name }}
                                            @else
                                                {{ (isset($person->contact)) ? $person->contact->fullName : '' }}
                                            @endif
                                        @else
                                            {{ $person->contact->fullName }}
                                        @endif
                                    </td>

                                    {{-- Тип пользователя --}}
                                    <td>{{ $person->group->name }}</td>

                                    {{-- Действия --}}
                                    <td>
                                        @if ($person->activity == 1)
                                            <a href="/users/off/{{ $person->id }}" class="btn btn-danger btn-xs btn-block">
                                                выключить
                                            </a>
                                        @else
                                            <a href="/users/on/{{ $person->id }}" class="btn btn-success btn-xs btn-block">
                                                включить
                                            </a>
                                        @endif

                                        <a href="/users/edit/{{ $person->id }}" class="btn btn-success btn-xs btn-block">
                                            редактировать
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $users->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop