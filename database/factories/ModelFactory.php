<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Contacts::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'position' => $faker->word,
        'description' => $faker->sentence,
        'avatar' => $faker->imageUrl(),
        'is_company' => false,
    ];
});

$factory->define(App\SheetFormat::class, function (Faker\Generator $faker) {
    return [
        'width' => $faker->numberBetween(50, 100),
        'height' => $faker->numberBetween(100, 300),
        'type' => $faker->randomElement(['initial', 'print'])
    ];
});

$factory->define(\App\Models\MaterialType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\App\Models\CoverType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\App\Models\Statistic\StatisticInvoice::class, function (Faker\Generator $faker) {
    $invoice = $faker->randomFloat(2, 100, 1000);
    $paidInvoice = $faker->randomFloat(2, 0, $invoice);

    return [
        'invoice_amount' => $invoice,
        'paid_invoice_amount' => $paidInvoice,
        'full_paid' => $faker->boolean(),
        'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
    ];
});

$factory->define(\App\Models\Statistic\StatisticClient::class, function (Faker\Generator $faker) {
    return [
        'new_client' => $faker->boolean(),
        'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
    ];
});

$factory->define(\App\Models\Statistic\StatisticOrder::class, function (Faker\Generator $faker) {
    return [
        'repeat' => $faker->boolean(),
        'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
    ];
});

$factory->define(\App\Models\SalaryOperation::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'action' => $faker->sentence,
        'name' => $faker->word,
        'client_id' => 1,
        'value' => $faker->numberBetween(100, 10000),
        'paid_sum' => $faker->numberBetween(100, 10000),
        'payment_type' => $faker->word,
        'balance' => $faker->numberBetween(100, 10000),
        'calculation_to' => $faker->date(),
        'accrued' => $faker->numberBetween(100, 10000),
        'receivable' => $faker->numberBetween(100, 10000),
        'is_header' => $faker->boolean(20),
        'created_at' => $faker->dateTimeBetween(),
    ];
});

$factory->define(\App\ProductType::class, function (Faker\Generator $faker) {
    $name = $faker->word;

    return [
        'key' => 'v',
        'name' => $name,
        'printler_type' => 1,
        'url' => $faker->imageUrl(),
        'activity' => true,
        'created_auto' => false,
        'services' => '',
        'services_id' => 0,
        'product_1c_key' => strtoupper(str_slug($name)).'_W2P',
    ];
});

$factory->define(\App\TemplateLibrary::class, function (Faker\Generator $faker) {
    $width = $faker->randomNumber(2);
    $height = $faker->randomNumber(2);

    return [
        'owner_id' => 1,
        'product_type_id' => 1,
        'status' => 'ready',
        'w2p' => '/path/to/frx/file',
        'psd' => '/path/to/psd/file',
        'url' => $faker->imageUrl(),
        'second_preview' => $faker->imageUrl(),
        'collection' => $faker->word,
        'type_key' => 'v',
        'type' => 0,
        'two_side' => false,
        'name' => '',
        'activity' => true,
        'width' => $width,
        'height' => $height,
        'ratio' => round($width / $height, 2),
        'folds' => 0,
    ];
});
