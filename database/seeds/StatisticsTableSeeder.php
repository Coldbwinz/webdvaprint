<?php

use Illuminate\Database\Seeder;

class StatisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Statistic\StatisticInvoice::truncate();
        \App\Models\Statistic\StatisticClient::truncate();
        \App\Models\Statistic\StatisticOrder::truncate();

        $managers = \App\User::managers()->get();

        foreach ($managers as $manager) {
            factory(\App\Models\Statistic\StatisticInvoice::class, 1000)->create([
                'manager_id' => $manager->id,
            ]);
        }
    }
}
