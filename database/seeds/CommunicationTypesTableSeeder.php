<?php

use App\CommunicationType;
use Illuminate\Database\Seeder;

class CommunicationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommunicationType::create(['name' => 'phone', 'display_name' => 'Телефон']);
        CommunicationType::create(['name' => 'email', 'display_name' => 'E-mail']);
        CommunicationType::create(['name' => 'skype', 'display_name' => 'Skype']);
        CommunicationType::create(['name' => 'vkontakte', 'display_name' => 'Вконтакте']);
    }
}
