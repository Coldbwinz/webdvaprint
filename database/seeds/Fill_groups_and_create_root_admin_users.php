<?php

use Illuminate\Database\Seeder;

class Fill_groups_and_create_root_admin_users extends Seeder
{

    public function run()
    {
        $id_admin = DB::table('users')->where('email', 'admin_admin@gmail.com')->get();

        if (count($id_admin) == 0) {
            DB::table('users')->insert([
                'id_group' => 3,
                'name' => "admin",
                'lastname' => "admin",
                'position' => "admin",
                'email' => 'admin_admin@gmail.com',
                'password' => bcrypt('admin'),
                'activity' => 1,
            ]);
        }

        $id_root = DB::table('users')->where('email', 'root_root@gmail.com')->get();

        if (count($id_root) == 0) {
            DB::table('users')->insert([
                'id_group' => 4,
                'name' => "root",
                'lastname' => "root",
                'position' => "root",
                'email' => 'root_root@gmail.com',
                'password' => bcrypt('root'),
                'activity' => 1,
            ]);
        }

        DB::table('users_groups')->truncate();

        DB::table('users_groups')->insert(
            [['name' => 'Пользователь'],
                ['name' => 'Менеджер'],
                ['name' => 'admin'],
                ['name' => 'root']]);
    }
}
