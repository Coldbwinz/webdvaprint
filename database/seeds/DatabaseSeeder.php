<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(Fill_groups_and_create_root_admin_users::class);
        $this->call(Fill_client_statuses::class);
        $this->call(Fill_client_types::class);
        $this->call(Fill_order_statuses::class);
        $this->call(Fill_parameters::class);
        $this->call(Fill_delivery_types::class);
        Model::reguard();
    }
}
