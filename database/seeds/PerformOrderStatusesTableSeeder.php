<?php

use App\PerformOrderStatus;
use Illuminate\Database\Seeder;

class PerformOrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PerformOrderStatus::truncate();

        PerformOrderStatus::create(['title' => 'Готовится к печати']);
        PerformOrderStatus::create(['title' => 'Готов к печати']);
        PerformOrderStatus::create(['title' => 'Пошёл в печать']);
        PerformOrderStatus::create(['title' => 'Отпечатан']);
    }
}
