<?php

use Illuminate\Database\Seeder;

class Fill_order_statuses extends Seeder
{
    public function run()
    {
        DB::table('order_statuses')->truncate();
        DB::table('order_statuses')->insert(
            [
                ['id' => 1, 'key' => 'invoiced', 'name' => 'Выставлен счет', 'color' => '001a35', 'sort_order' => 210],
                ['id' => 2, 'key' => 'layout_approved', 'name' => 'Макет утвержден', 'color' => '357ca5', 'sort_order' => 130],
                ['id' => 3, 'key' => 'on_agreeing', 'name' => 'На согласовании', 'color' => '5a6e82', 'sort_order' => 220],
                ['id' => 4, 'key' => 'shown_for_client', 'name' => 'Макет не утверждён', 'color' => '475577', 'sort_order' => 140],
                ['id' => 5, 'key' => 'sent_for_approval', 'name' => 'Отправлен на согласование', 'color' => '9365b8', 'sort_order' => 230],
                ['id' => 6, 'key' => 'fully_paid', 'name' => 'Полностью оплачен', 'color' => '008d4c', 'sort_order' => 240],
                ['id' => 7, 'key' => 'partially_paid', 'name' => 'Частично оплачен', 'color' => '605ca8', 'sort_order' => 250],
                ['id' => 8, 'key' => 'cash_on_delivery', 'name' => 'Оплата при получении', 'color' => '5da6c2', 'sort_order' => 260],
                ['id' => 9, 'key' => 'sent_again', 'name' => 'Отправлен повторно', 'color' => '00a65a', 'sort_order' => 270],
                ['id' => 10, 'key' => 'layout_prepares_client', 'name' => 'Макет готовит клиент', 'color' => 'b72e2e', 'sort_order' => 280],
                ['id' => 12, 'key' => 'layout_checked', 'name' => 'Макет проверен', 'color' => '2c82c9', 'sort_order' => 150],
                ['id' => 25, 'key' => 'in_work', 'name' => 'Запущен в работу', 'color' => 'ca195a', 'sort_order' => 290],

                ['id' => 30, 'key' => 'preparing_to_press', 'name' => 'Готовится к печати', 'color' => '9365b8', 'sort_order' => 300],
                ['id' => 35, 'key' => 'in_press', 'name' => 'Пошел в печать', 'color' => '5da6c2', 'sort_order' => 310],
                ['id' => 40, 'key' => 'in_postpress', 'name' => 'В постпрессе', 'color' => '2c82c9', 'sort_order' => 320],
                ['id' => 45, 'key' => 'ready', 'name' => 'Готов', 'color' => '00c0ef', 'sort_order' => 330],

                ['id' => 51, 'key' => 'on_issue', 'name' => 'На точке выдачи', 'color' => '433E0E', 'sort_order' => 340],
                ['id' => 52, 'key' => 'issued', 'name' => 'Выдан', 'color' => '547AA5', 'sort_order' => 350],
                ['id' => 53, 'key' => 'received', 'name' => 'Получен', 'color' => 'D2BF55', 'sort_order' => 360],

                ['id' => 55, 'key' => 'return_marriage', 'name' => 'Возврат по браку', 'color' => '003849', 'sort_order' => 370],
                ['id' => 56, 'key' => 'on_rework', 'name' => 'Отправлен на доработку', 'color' => '32322C', 'sort_order' => 380],
                ['id' => 57, 'key' => 'closed_money_back', 'name' => 'Закрыт с возвратом денег', 'color' => '', 'sort_order' => 390],
                ['id' => 58, 'key' => 'closed_with_discount', 'name' => 'Выдан со скидкой', 'color' => '', 'sort_order' => 400],

                ['id' => 100, 'key' => 'closed', 'name' => 'Закрыт', 'color' => '0FA3B1', 'sort_order' => 410],

                ['id' => 102, 'key' => 'layout_changed', 'name' => 'Макет изменен', 'color' => '2c82c9', 'sort_order' => 420],
                ['id' => 112, 'key' => 'layout_replaced', 'name' => 'Макет заменен', 'color' => '2c82c9', 'sort_order' => 430],
                ['id' => 125, 'key' => 'on_rework_search', 'name' => 'На доработке', 'color' => '2c82c9', 'sort_order' => 335],

                ['id' => 200, 'key' => 'order_forms', 'name' => 'Заказ формируется', 'color' => '', 'sort_order' => 440],

                ['id' => 201, 'key' => 'sent_to_issue_point', 'name' => 'Отправлен на точку выдачи', 'color' => '', 'sort_order' => 450],

                ['id' => 300, 'key' => 'exclusive_design', 'name' => 'Разработка макета по ТЗ', 'color' => '2c82c9', 'sort_order' => 100],
                ['id' => 301, 'key' => 'self_designer', 'name' => 'Совместная работа с дизайнером', 'color' => '2c82c9', 'sort_order' => 110],

                // Доп. статусы для карточки заказа
                ['id' => 500, 'key' => 'order_formed', 'name' => 'Заказ сформирован', 'color' => '', 'sort_order' => 480],
                ['id' => 501, 'key' => 'order_recalculated', 'name' => 'Заказ пересчитан', 'color' => '', 'sort_order' => 490],
                ['id' => 502, 'key' => 'payment_guaranteed', 'name' => 'Оплата гарантирована', 'color' => '', 'sort_order' => 500],
                ['id' => 503, 'key' => 'payment_confirmed', 'name' => 'Оплата подтверждена', 'color' => '', 'sort_order' => 510],
                ['id' => 504, 'key' => 'permitted_payment_delivery', 'name' => 'Разрешена оплата при получении', 'color' => '', 'sort_order' => 520],
                ['id' => 505, 'key' => 'agreed_discount', 'name' => 'Согласована скидка', 'color' => '', 'sort_order' => 530],
                ['id' => 506, 'key' => 'refund', 'name' => 'Возврат денег', 'color' => '', 'sort_order' => 540],
                ['id' => 507, 'key' => 'calculation_completed', 'name' => 'Произведён расчёт', 'color' => '', 'sort_order' => 550],
                ['id' => 508, 'key' => 'gived_discount', 'name' => 'Предоставлена скидка', 'color' => '', 'sort_order' => 560],

                ['id' => 776, 'key' => 'parsing', 'name' => 'Идёт процесс конвертации', 'color' => '', 'sort_order' => 570],
                ['id' => 777, 'key' => 'prepares_agreement', 'name' => 'Готовится к согласованию', 'color' => '', 'sort_order' => 120],

                ['id' => 999, 'key' => 'error', 'name' => 'Ошибка', 'color' => 'ff5252', 'sort_order' => 590],
                ['id' => 1000, 'key' => 'paused', 'name' => 'Приостановлен', 'color' => 'bc3a61', 'sort_order' => 600],
                ['id' => 1010, 'key' => 'canceled', 'name' => 'Отменён', 'color' => '', 'sort_order' => 610],

                ['id' => 2000, 'key' => 'not_fully_decorated', 'name' => 'Разработка макета', 'color' => '', 'sort_order' => 620],
                ['id' => 2010, 'key' => 'draft', 'name' => 'Разработка макета', 'color' => '', 'sort_order' => 630],
                ['id' => 2020, 'key' => 'reorder', 'name' => 'Повторный заказ', 'color' => '', 'sort_order' => 640],
                ['id' => 2030, 'key' => 'self_created', 'name' => 'Оформлен самостоятельно', 'color' => '', 'sort_order' => 650],
            ]);
    }
}
