<?php

use App\ProductType;
use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductType::truncate();

        ProductType::create([
            'key' => 'v',
            'name' => 'Визитка',
            'printler_type' => 1,
            'url' => '10.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => 'fb',
            'name' => 'Бланк',
            'printler_type' => 1,
            'url' => 'blank.png',
            'activity' => true,
            'created_auto' => false,
           'services' => '',
        ]);

        ProductType::create([
            'key' => '',
            'name' => 'Буклет',
            'printler_type' => 2,
            'url' => 'booklet.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => '',
            'name' => 'Блокнот',
            'printler_type' => 3,
            'url' => 'notepad.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => 'l',
            'name' => 'Листовка',
            'printler_type' => 1,
            'url' => 'leaflet.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => '',
            'name' => 'Плакат',
            'printler_type' => 1,
            'url' => 'poster.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => '',
            'name' => 'Конверты',
            'printler_type' => 1,
            'url' => 'envelope.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);

        ProductType::create([
            'key' => '',
            'name' => 'Кубарики',
            'printler_type' => 3,
            'url' => 'stickers.png',
            'activity' => true,
            'created_auto' => false,
            'services' => '',
        ]);
    }
}
