<?php

use Illuminate\Database\Seeder;

class UsersGroupsRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\UserGroup::where('id', 1)->update(['role' => 'client']);
        \App\UserGroup::where('id', 2)->update(['role' => 'manager']);
        \App\UserGroup::where('id', 3)->update(['role' => 'admin']);
        \App\UserGroup::where('id', 4)->update(['role' => 'root']);
        \App\UserGroup::where('id', 5)->update(['role' => 'partner']);
        \App\UserGroup::where('id', 6)->update(['role' => 'director']);
    }
}
