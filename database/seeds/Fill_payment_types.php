<?php

use Illuminate\Database\Seeder;

class Fill_payment_types extends Seeder
{

    public function run()
    {
        DB::table('payment_types')->truncate();
        DB::table('payment_types')->insert([
            ['name' => 'Полная предоплата', 'discount' => 0],
            ['name' => 'Частичная предоплата', 'discount' => 10],
            ['name' => 'Оплата при получении', 'discount' => 20],
            ['name' => 'Отсрочка платежа', 'discount' => 30],
        ]);
    }
}
