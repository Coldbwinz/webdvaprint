<?php

use App\Models\Equipment\EquipmentPrintType;
use Illuminate\Database\Seeder;

class EquipmentPrintTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EquipmentPrintType::truncate();
        EquipmentPrintType::create(['name' => 'Цифровая']);
        EquipmentPrintType::create(['name' => 'Широкоформатная']);
    }
}
