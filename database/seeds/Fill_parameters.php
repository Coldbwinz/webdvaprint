<?php

use Illuminate\Database\Seeder;

class Fill_parameters extends Seeder
{
    public function run()
    {
        DB::table('parameters')->truncate();
        DB::table('parameters')->insert(
            ['id' => 1, 'name' => 'Работа с 1С', 'activity' => 0]);
    }
}
