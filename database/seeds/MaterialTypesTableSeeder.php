<?php

use App\Models\MaterialType;
use Illuminate\Database\Seeder;

class MaterialTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MaterialType::truncate();

        factory(MaterialType::class, 10)->create();
    }
}
