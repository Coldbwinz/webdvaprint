<?php

use App\SheetFormat;
use Illuminate\Database\Seeder;

class SheetFormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SheetFormat::truncate();

        SheetFormat::create(['width' => 305, 'height' => 450, 'type' => 'print']);
        SheetFormat::create(['width' => 320, 'height' => 450, 'type' => 'print']);
        SheetFormat::create(['width' => 320, 'height' => 464, 'type' => 'print']);
        SheetFormat::create(['width' => 325, 'height' => 470, 'type' => 'print']);
        SheetFormat::create(['width' => 330, 'height' => 470, 'type' => 'print']);

        SheetFormat::create(['width' => 339, 'height' => 479, 'type' => 'initial']);
        SheetFormat::create(['width' => 156, 'height' => 200, 'type' => 'initial']);
    }
}
