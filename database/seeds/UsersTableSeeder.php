<?php

use App\User;
use App\UserGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        User::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        // Root
        $root = new User();
        $root->name = 'root';
        $root->email = 'root_root@gmail.com';
        $root->id_group = 4;
        $root->password = bcrypt('root');
        $root->save();

        // Another root
        $admin = new User();
        $admin->name = 'Administrator';
        $admin->email = 'admin@w2p.ru';
        $admin->id_group = 4;
        $admin->password = bcrypt('admin');
        $admin->save();

        $admin->contact()->create([
            'name' => 'Администратор Проекта',
            'last_name' => 'Фамилия',
            'email' => 'admin@w2p.ru',
            'phone' => '+79259036632',
            'description' => 'Админитсратор проекта. Создан через сидирование.',
        ]);

        // Manager
        $manager = new User();
        $manager->name = 'Manager';
        $manager->email = 'manager@w2p.dev';
        $manager->id_group = UserGroup::MANAGER;
        $manager->password = bcrypt('manager');
        $manager->save();

        $manager->contact()->create([
            'name' => 'Иннокентий Михайлович',
            'last_name' => 'Смоктуновский',
            'email' => 'manager@w2p.dev',
            'phone' => '+79253269575',
            'description' => 'менеджер проекта. Создан через сидирование.'
        ]);

        // Manager 2
        $manager = new User();
        $manager->name = 'Manager 2';
        $manager->email = 'manager2@w2p.dev';
        $manager->id_group = UserGroup::MANAGER;
        $manager->password = bcrypt('manager');
        $manager->save();

        $manager->contact()->create([
            'name' => 'Иван Иванович',
            'last_name' => 'Иванов',
            'email' => 'manager2@w2p.dev',
            'phone' => '+79253269575',
            'description' => 'Ещё один менеджер. Создан через сидирование.'
        ]);

        // Make some managers
        factory(User::class, 5)->create([
            'id_group' => UserGroup::MANAGER,
        ])->each(function (User $user) {
            factory(\App\Contacts::class)->create([
                'email' => $user->email,
                'user_id' => $user->id,
            ]);
        });
    }
}
