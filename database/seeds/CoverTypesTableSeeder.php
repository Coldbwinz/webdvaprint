<?php

use App\Models\CoverType;
use Illuminate\Database\Seeder;

class CoverTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CoverType::truncate();

        factory(CoverType::class, 10)->create();
    }
}
