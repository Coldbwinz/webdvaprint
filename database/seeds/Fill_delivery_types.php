<?php

use Illuminate\Database\Seeder;

class Fill_delivery_types extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_types')->truncate();
        DB::table('delivery_types')->insert([
            ['name' => 'Самовывоз', 'cost' => 0, 'days' => 0],
            ['name' => 'Доставка курьером', 'cost' => 100, 'days' => 1],
        ]);
    }
}
