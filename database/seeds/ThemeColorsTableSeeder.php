<?php

use Illuminate\Database\Seeder;

class ThemeColorsTableSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $colors = [
        'Красный',
        'Оранжевый',
        'Желтый',
        'Зеленый',
        'Голубой',
        'Синий',
        'Фиолетовый',
        'Белый',
        'Черный'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->colors as $color) {
            \App\ThemeColor::create([
                'color' => $color,
            ]);
        }
    }
}
