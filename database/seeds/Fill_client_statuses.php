<?php

use Illuminate\Database\Seeder;

class Fill_client_statuses extends Seeder
{
    public function run()
    {
        DB::table('client_statuses')->truncate();
        DB::table('client_statuses')->insert(
            [['name' => 'без предоплаты'],
                ['name' => 'при полной оплате'],
                ['name' => 'по частичной предоплате'],
            ]);
    }
}
