<?php

use App\Details;
use Illuminate\Database\Seeder;

class DetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $details = new Details();
        $details->activity = 1;
        $details->name = 'АртЛайнСити';
        $details->post_address = 224030;
        $details->address = 'г. Хабаровск ул. Волочаевская 15 оф. 30';
        $details->bank_rs = '40702810008010018335';
        $details->bank_name = 'РЕГИОБАНК - филиал ОАО Банка "ФК Открытие"';
        $details->bank_bik = '123456789';
        $details->bank_ks = '30101810508130000997';
        $details->bank_inn = '123456789123';
        $details->bank_kpp = '272301001';
        $details->director = 'Гуменюк П.Л.';
        $details->save();
    }
}
