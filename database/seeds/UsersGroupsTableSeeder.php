<?php

use Illuminate\Database\Seeder;

class UsersGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\UserGroup::truncate();
        \App\UserGroup::create(['name' => 'Пользователь', 'role' => 'client']);
        \App\UserGroup::create(['name' => 'Менеджмент', 'role' => 'manager']);
        \App\UserGroup::create(['name' => 'Администрация', 'role' => 'admin']);
        \App\UserGroup::create(['name' => 'root', 'role' => 'root']);
        \App\UserGroup::create(['name' => 'Партнёр', 'role' => 'partner']);
        \App\UserGroup::create(['name' => 'Производство', 'role' => 'director']);
        \App\UserGroup::create(['name' => 'Препресс', 'role' => 'designer']);
    }
}
