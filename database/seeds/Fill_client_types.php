<?php

use Illuminate\Database\Seeder;

class Fill_client_types extends Seeder
{
    public function run()
    {
        DB::table('client_types')->truncate();
        DB::table('client_types')->insert([
            ['name' => 'Новый клиент', 'discount' => 0],
            ['name' => 'Постоянный клиент', 'discount' => 10],
            ['name' => 'Посредник', 'discount' => 20],
            ['name' => 'Партнер', 'discount' => 30],
        ]);
    }
}
