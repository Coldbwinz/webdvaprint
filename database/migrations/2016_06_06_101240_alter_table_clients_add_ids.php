<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClientsAddIds extends Migration
{
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable()->after('id');            
        });
    }

    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {            
            $table->dropColumn('user_id');
        });
    }
}
