<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->index();
            $table->decimal('invoice_amount')->default(0);
            $table->decimal('paid_invoice_amount')->default(0);
            $table->boolean('full_paid')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statistic_invoices');
    }
}
