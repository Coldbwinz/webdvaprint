<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUrgency extends Migration
{
    public function up()
    {
        Schema::create('urgency', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('discount');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('urgency');
    }
}
