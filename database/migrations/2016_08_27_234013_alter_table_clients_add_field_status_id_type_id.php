<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClientsAddFieldStatusIdTypeId extends Migration
{
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('type_id')->unsigned()->nullable()->after('manager_id');
            $table->integer('status_id')->unsigned()->nullable()->after('type_id');
        });
    }

    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('status_id');
            $table->dropColumn('type_id');
        });
    }
}
