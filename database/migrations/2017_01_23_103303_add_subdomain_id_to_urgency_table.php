<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubdomainIdToUrgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urgency', function (Blueprint $table) {
            $table->integer('subdomain_id')
                ->unsigned()
                ->nullable()
                ->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('urgency', function (Blueprint $table) {
            $table->dropColumn('subdomain_id');
        });
    }
}
