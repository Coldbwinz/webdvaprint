<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subdomain_id')->unsigned()->index()->nullable();
            $table->integer('product_type_id')->unsigned()->index();
            $table->string('name');
            $table->string('chromacity');
            $table->string('alias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('export_settings');
    }
}
