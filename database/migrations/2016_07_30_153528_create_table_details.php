<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetails extends Migration
{
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('activity')->default(1);
            $table->string('name');
            $table->string('post_address');
            $table->string('address');
            $table->string('bank_rs');
            $table->string('bank_name');
            $table->string('bank_bik');
            $table->string('bank_ks');
            $table->string('bank_inn');
            $table->string('bank_kpp');
            $table->string('director');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('details');
    }
}
