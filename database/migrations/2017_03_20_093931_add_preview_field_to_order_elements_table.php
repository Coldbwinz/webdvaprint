<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreviewFieldToOrderElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_elements', function (Blueprint $table) {
            $table->string('preview')
                ->nullable()
                ->after('postpress');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_elements', function (Blueprint $table) {
            $table->dropColumn('preview');
        });
    }
}
