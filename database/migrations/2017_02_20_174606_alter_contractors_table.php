<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->integer('subdomain_id')->unsigned()->nullable()->after('id');
            $table->string('last_name')->after('name');
            $table->string('email')->after('last_name');
            $table->string('phone')->after('email');
            $table->boolean('is_company')->default(false)->after('last_name');
            $table->string('company_name')->after('is_company');
            $table->string('position')->after('company_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->dropColumn('subdomain_id');
            $table->dropColumn('last_name');
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropColumn('is_company');
            $table->dropColumn('company_name');
            $table->dropColumn('position');
        });
    }
}
