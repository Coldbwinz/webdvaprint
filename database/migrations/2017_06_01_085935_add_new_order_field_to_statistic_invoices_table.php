<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewOrderFieldToStatisticInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->boolean('new_order')->default(false)->after('full_paid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->dropColumn('new_order');
        });
    }
}
