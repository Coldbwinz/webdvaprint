<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductTypesAddWidthHeightPrintlerType extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->integer('width')->after('name')->default(40);
            $table->integer('height')->after('width')->default(40);
            $table->integer('printler_type')->after('height')->default(1);
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('printler_type');
        });
    }
}
