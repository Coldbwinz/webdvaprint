<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('action');
            $table->string('name')->nullable();
            $table->integer('client_id')->nullable()->index();
            $table->decimal('value')->nullable();
            $table->decimal('paid_sum')->nullable();
            $table->string('payment_type')->nullable();
            $table->decimal('balance')->nullable();
            $table->timestamp('calculation_to')->nullable();
            $table->decimal('accrued')->nullable();
            $table->decimal('receivable')->nullable();
            $table->boolean('is_header')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_operations');
    }
}
