<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTemplateDownloadAddFieldsPreviewDownloadedPdfDownload extends Migration
{
    public function up()
    {
        Schema::table('template_download', function (Blueprint $table) {
            $table->boolean('preview_downloaded')->default(false)->after('id_order');
            $table->boolean('pdf_download')->default(false)->after('preview_downloaded');
        });
    }

    public function down()
    {
        Schema::table('template_download', function (Blueprint $table) {
            $table->dropColumn('preview_downloaded');
            $table->dropColumn('pdf_download');
        });
    }
}
