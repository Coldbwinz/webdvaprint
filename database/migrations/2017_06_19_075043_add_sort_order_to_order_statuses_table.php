<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortOrderToOrderStatusesTable extends Migration
{
    public function up()
    {
        Schema::table('order_statuses', function (Blueprint $table) {
            $table->smallInteger('sort_order')->after('text');
        });
    }

    public function down()
    {
        Schema::table('order_statuses', function (Blueprint $table) {
            $table->dropColumn('sort_order');
        });
    }
}
