<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateUploadFieldToTemplateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->integer('owner_id')->nullable()->index()->after('id');
            $table->string('status')->nullable()->index()->after('owner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->dropColumn(['owner_id', 'status']);
        });
    }
}
