<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubdomainIdToClientTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_types', function (Blueprint $table) {
            $table->integer('subdomain_id')
                ->nullable()
                ->unsigned()
                ->index()
                ->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_types', function (Blueprint $table) {
            $table->dropColumn('subdomain_id');
        });
    }
}
