<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientTypes extends Migration
{
    public function up()
    {
        Schema::create('client_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('discount');
            $table->boolean('print_without_payment')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('client_types');
    }
}
