<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToThemeThemeTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('theme_theme_tag', function (Blueprint $table) {
            $table->foreign('theme_id')
                ->references('id')->on('themes')
                ->onDelete('cascade');

            $table->foreign('theme_tag_id')
                ->references('id')->on('theme_tags')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('theme_theme_tag', function (Blueprint $table) {
            $table->dropForeign('theme_theme_tag_theme_id_foreign');
            $table->dropForeign('theme_theme_tag_theme_tag_id_foreign');
        });
    }
}
