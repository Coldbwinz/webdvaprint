<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerSpecificFieldsToGroupOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->integer('pay_partner')->nullable()->after('source_id');
            $table->integer('pay_cash_partner')->nullable()->after('pay_partner');
            $table->integer('pay_rs_partner')->nullable()->after('pay_cash_partner');
            $table->integer('pay_eth_partner')->nullable()->after('pay_rs_partner');
            $table->string('close_date_partner')->nullable()->after('pay_eth_partner');
            $table->integer('status_partner')->nullable()->unsigned()->index()->after('close_date_partner');
            $table->integer('money_partner')->nullable()->after('status_partner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn([
                'pay_partner', 'pay_cash_partner', 'pay_rs_partner', 'pay_eth_partner',
                'close_date_partner', 'status_partner', 'money_partner'
            ]);
        });
    }
}
