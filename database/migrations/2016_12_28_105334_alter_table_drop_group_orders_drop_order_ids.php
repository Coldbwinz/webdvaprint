<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDropGroupOrdersDropOrderIds extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('order_ids');
        });
    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->text('order_ids')->after('subdomain_id');
        });
    }
}
