<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGroupOrdersAddFieldPriceExpenses extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->decimal('price_expenses')->nullable()->after('price_partner');
        });
    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('price_expenses');
        });
    }
}
