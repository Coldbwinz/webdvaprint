<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductTypesDropColumns extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('page_count');
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->integer('width')->after('name');
            $table->integer('height')->after('width');
            $table->integer('page_count')->after('height');
        });
    }
}
