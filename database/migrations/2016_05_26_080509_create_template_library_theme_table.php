<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateLibraryThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_library_theme', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_library_id')->unsigned();
            $table->integer('theme_id')->unsigned();            
            
            $table->foreign('template_library_id')
                    ->references('id')->on('template_library')
                    ->onDelete('cascade');
            
            $table->foreign('theme_id')
                    ->references('id')->on('themes')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_library_theme');
    }
}
