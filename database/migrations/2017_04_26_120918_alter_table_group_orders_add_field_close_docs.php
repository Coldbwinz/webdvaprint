<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGroupOrdersAddFieldCloseDocs extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->text('close_docs')
                ->nullable()
                ->after('guarantee_image');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('close_docs');
        });
    }
}
