<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentTypes extends Migration
{
    public function up()
    {
        Schema::create('payment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subdomain_id')->unsigned()->nullable();
            $table->string('name');
            $table->integer('discount');
            $table->string('key_1s')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('payment_types');
    }
}
