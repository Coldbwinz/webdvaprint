<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->unsignedSmallInteger('cover_type_id')->index()->after('id');
            $table->unsignedSmallInteger('material_type_id')->index()->after('cover_type_id');
            $table->decimal('price_ton')->after('name');
            $table->unsignedSmallInteger('initial_sheet_width')->after('price_ton');
            $table->unsignedSmallInteger('initial_sheet_height')->after('initial_sheet_width');
            $table->unsignedSmallInteger('print_sheet_width')->after('initial_sheet_height');
            $table->unsignedSmallInteger('print_sheet_height')->after('print_sheet_width');
            $table->unsignedSmallInteger('print_sheet_number')->after('print_sheet_height');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn([
                'cover_type_id', 'material_type_id', 'price_ton', 'initial_sheet_width',
                'initial_sheet_height', 'print_sheet_width', 'print_sheet_height', 'print_sheet_number',
            ]);
        });
    }
}
