<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryMotivationSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_motivation_schemes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_group_id')->index();
            $table->integer('percent')->index();
            $table->string('salary');
            $table->string('premium');
            $table->string('bonus');
            $table->float('bonus_coefficient');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_motivation_schemes');
    }
}
