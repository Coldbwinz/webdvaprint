<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_group_id')->index();
            $table->integer('product_type_id')->index();
            $table->integer('client_type_id')->index();
            $table->integer('value');
            $table->string('dimension');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_bonuses');
    }
}
