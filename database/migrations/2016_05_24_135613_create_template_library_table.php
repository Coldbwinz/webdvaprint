<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_library', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('w2p');
            $table->string('psd');
            $table->string('url');                       
            $table->integer('type');
            $table->string('themes');
            $table->string('name');
            $table->integer('activity')->default(1);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_library');
    }
}
