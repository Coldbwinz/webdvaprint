<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeKeyFieldToTemplateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->string('type_key')->nullable()->index()->after('collection');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->dropColumn('type_key');
        });
    }
}
