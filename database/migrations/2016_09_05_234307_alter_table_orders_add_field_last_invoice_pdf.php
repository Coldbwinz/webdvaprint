<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrdersAddFieldLastInvoicePdf extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('last_invoice_pdf')->after('pdf');
            $table->boolean('need_new_invoice')->after('last_invoice_pdf');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('need_new_invoice');
            $table->dropColumn('last_invoice_pdf');
        });
    }
}
