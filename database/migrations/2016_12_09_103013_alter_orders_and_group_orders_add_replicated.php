<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersAndGroupOrdersAddReplicated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->boolean('replicated')->default(false)->after('status');
            $table->integer('source_id')->nullable()->after('replicated');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('replicated')->default(false)->after('status');
            $table->integer('source_id')->nullable()->after('replicated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('source_id');
            $table->dropColumn('replicated');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('source_id');
            $table->dropColumn('replicated');
        });
    }
}
