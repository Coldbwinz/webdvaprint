<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductTypesAddPageCount extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->integer('page_count')->after('height')->default(1);
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('page_count');
        });
    }
}
