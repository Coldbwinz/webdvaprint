<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderLogAddFieldStatusFree extends Migration
{
    public function up()
    {
        Schema::table('order_logs', function (Blueprint $table) {
            $table->string('status_free')->nullable()->after('status');
        });
    }

    public function down()
    {
        Schema::table('order_logs', function (Blueprint $table) {
            $table->dropColumn('status_free');
        });
    }
}
