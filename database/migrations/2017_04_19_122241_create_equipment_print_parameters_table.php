<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentPrintParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_print_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('equipment_id')->index();
            $table->string('print_type');
            $table->unsignedInteger('print_width');
            $table->unsignedInteger('print_height');
            $table->decimal('cost_price');
            $table->text('properties');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_print_parameters');
    }
}
