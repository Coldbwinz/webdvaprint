<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableParameters extends Migration
{
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->integer('id')->unique();
            $table->string('name');
            $table->integer('value');
            $table->boolean('activity');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('parameters');
    }
}
