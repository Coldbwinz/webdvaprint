<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perform_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product');
            $table->string('chromacity');
            $table->string('print_sheet_format');
            $table->integer('draw');
            $table->string('material');
            $table->string('postpress_processes'); //
            $table->date('issue_at');
            $table->integer('perform_order_status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perform_orders');
    }
}
