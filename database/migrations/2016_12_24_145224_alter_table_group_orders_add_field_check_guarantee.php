<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGroupOrdersAddFieldCheckGuarantee extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->boolean('check_guarantee')->default(false)->after('payment_status');
        });
    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('check_guarantee');
        });
    }
}
