<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableDeliveryTypes extends Migration
{
    public function up()
    {
        Schema::create('delivery_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('cost');
            $table->integer('days');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('delivery_types');
    }
}
