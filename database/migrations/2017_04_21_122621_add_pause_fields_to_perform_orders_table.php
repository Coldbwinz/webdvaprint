<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPauseFieldsToPerformOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->boolean('paused')->default(false)->after('revision_comment');
            $table->string('paused_reason')->nullable()->after('paused');
            $table->string('resumed_reason')->nullable()->after('paused_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->dropColumn(['paused', 'paused_reason', 'resumed_reason']);
        });
    }
}
