<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDensityFieldsToMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('stock');

            $table->unsignedInteger('density')->after('price');
            $table->unsignedInteger('stock_availability')->after('density');
            $table->unsignedInteger('stock_balance')->after('stock_availability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->smallInteger('stock')->unsigned()->after('price');

            $table->dropColumn([
                'density', 'stock_availability', 'stock_balance'
            ]);
        });
    }
}
