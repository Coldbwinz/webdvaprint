<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPasswordResets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('password_resets', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('user_id')->unsigned()->after('id');
            $table->boolean('activate')->default(false)->after('token');
            $table->dateTime('activate_at')->after('activate');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('password_resets', function (Blueprint $table) {
            $table->dropForeign('password_resets_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('activate');
            $table->dropColumn('activate_at');
            $table->dropColumn('id');
        });
    }
}
