<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetChromacityNullableInExportSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('export_settings', function (Blueprint $table) {
            $table->string('chromacity')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('export_settings', function (Blueprint $table) {
            $table->string('chromacity')->nullable(false)->change();
        });
    }
}
