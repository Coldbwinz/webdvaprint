<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClientsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('tm_name')->nullable()->after('company_name');
            $table->string('address')->after('description');
            $table->boolean('vat')->default(false)->after('address');
            $table->boolean('is_company')->default(false)->after('vat');
            $table->string('ownership_type')->nullable()->after('is_company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['address', 'vat', 'is_company', 'ownership_type', 'tm_name']);
        });
    }
}
