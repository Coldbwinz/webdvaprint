<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductTypesAddFieldCreatedAuto extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->boolean('created_auto')->after('activity');
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('created_auto');
        });
    }
}
