<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTableServices extends Migration
{
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name');
            $table->string('count');
            $table->decimal('price');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('services');
    }
}
