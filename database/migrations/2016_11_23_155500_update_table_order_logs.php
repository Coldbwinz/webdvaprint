<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableOrderLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_logs', function (Blueprint $table) {
          $table->string('value')->after('status');
          $table->string('comments')->after('value');
          $table->dropColumn('pay');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_logs', function (Blueprint $table) {
          $table->dropColumn('value');
          $table->dropColumn('comments');
            $table->string('pay')->after('status');
      });
    }
}
