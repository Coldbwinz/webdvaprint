<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParsingSpecificFieldsToReadyUploadProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ready_upload_products', function (Blueprint $table) {
            $table->smallInteger('status_code')
                ->default(\App\ReadyUploadProduct::PARSE_NEW)
                ->after('position');

            $table->string('frx_path')
                ->nullable()
                ->after('status_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ready_upload_products', function (Blueprint $table) {
            $table->dropColumn(['status_code', 'frx_path']);
        });
    }
}
