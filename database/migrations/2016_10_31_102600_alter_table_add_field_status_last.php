<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddFieldStatusLast extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('status_last')->after('status');
            $table->json('stopped_reasons')->after('status_last');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('stopped_reasons');
            $table->dropColumn('status_last');
        });
    }
}
