<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIssuedActToGroupOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->integer('issued_act')
                ->nullable()
                ->after('invoice_number_partner');
        });
    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('issued_act');
        });
    }
}
