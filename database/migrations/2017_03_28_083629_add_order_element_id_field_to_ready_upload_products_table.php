<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderElementIdFieldToReadyUploadProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ready_upload_products', function (Blueprint $table) {
            $table->integer('order_element_id')
                ->unsigned()
                ->index()
                ->after('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ready_upload_products', function (Blueprint $table) {
            $table->dropColumn('order_element_id');
        });
    }
}
