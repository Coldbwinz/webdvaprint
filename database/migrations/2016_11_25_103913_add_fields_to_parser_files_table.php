<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToParserFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_files', function (Blueprint $table) {
            $table->string('vendor')->nullable();
            $table->string('number')->nullable();
            $table->string('type')->nullable();
            $table->string('side')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_files', function (Blueprint $table) {
            $table->dropColumn(['vendor', 'number', 'type', 'side']);
        });
    }
}
