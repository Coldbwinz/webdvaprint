<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKey1sToUrgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urgency', function (Blueprint $table) {
            $table->string('key_1s')
                ->nullable()
                ->after('discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('urgency', function (Blueprint $table) {
            $table->dropColumn('key_1s');
        });
    }
}
