<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableChangeFieldDesrciptionToServicesId extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->integer('services_id')->after('activity');
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('services_id');
            $table->text('description')->after('activity');
        });
    }
}
