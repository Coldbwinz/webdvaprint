<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('new_clients_count_plan')->default(0)->after('remember_token');
            $table->integer('repeated_orders_count_plan')->default(0)->after('new_clients_count_plan');
            $table->decimal('invoices_total_plan')->default(0)->after('repeated_orders_count_plan');
            $table->decimal('paid_invoices_total_plan')->default(0)->after('invoices_total_plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'new_clients_count_plan', 'repeated_orders_count_plan',
                'invoices_total_plan', 'paid_invoices_total_plan',
            ]);
        });
    }
}
