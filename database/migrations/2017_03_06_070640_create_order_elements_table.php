<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->index();
            $table->integer('perform_order_id')->nullable()->index();
            $table->integer('status_id')->index();
            $table->string('name');
            $table->string('chromacity');
            $table->string('format');
            $table->integer('draw');
            $table->integer('pages_number')->nullable();
            $table->integer('perform_draw')->nullable();
            $table->integer('perform_layout')->nullable();
            $table->string('material');
            $table->text('postpress')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_elements');
    }
}
