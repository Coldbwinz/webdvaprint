<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropEquipmentDigitalPrintPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('equipment_digital_print_properties');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('equipment_digital_print_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('equipment_id')->index();

            // Характеристики
            $table->integer('print_sheet_width');
            $table->integer('print_sheet_height');
            $table->integer('technological_fields');
            $table->integer('valve');
            $table->string('valve_side');
            $table->string('chromaticity');
            $table->integer('click_price');
            $table->integer('currency');
            $table->integer('performance');
            $table->integer('rebuilding');
            $table->integer('fitting_1');
            $table->integer('fitting_2');
            $table->integer('fitting_3');

            // Баннерная печать
            $table->integer('banner_print_sheet_width')->nullable();
            $table->integer('banner_print_sheet_height')->nullable();
            $table->integer('banner_technological_fields')->nullable();
            $table->integer('banner_valve')->nullable();
            $table->string('banner_valve_side')->nullable();
            $table->string('banner_chromaticity')->nullable();
            $table->integer('banner_click_price')->nullable();
            $table->integer('banner_currency')->nullable();
            $table->integer('banner_performance')->nullable();
            $table->integer('banner_rebuilding')->nullable();
            $table->integer('banner_fitting_1')->nullable();
            $table->integer('banner_fitting_2')->nullable();
            $table->integer('banner_fitting_3')->nullable();
            $table->timestamps();
        });
    }
}
