<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClientTypesDeleteFieldPrintWithoutPayment extends Migration
{
    public function up()
    {
        Schema::table('client_types', function (Blueprint $table) {
            $table->dropColumn('print_without_payment');
        });
    }

    public function down()
    {
        Schema::table('client_types', function (Blueprint $table) {
            $table->boolean('print_without_payment')->default(0);
        });
    }
}
