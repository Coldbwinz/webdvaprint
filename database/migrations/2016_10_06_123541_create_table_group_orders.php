<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGroupOrders extends Migration
{
    public function up()
    {
        Schema::create('group_orders', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('manager_id');
            $table->integer('client_id');
            $table->integer('detail_id');
            $table->json('order_ids');
            $table->tinyInteger('invoice_send');
            $table->integer('repeat_send');
            $table->integer('last_invoice_pdf');
            $table->tinyInteger('need_new_invoice');
            $table->tinyInteger('money');
            $table->integer('price');
            $table->string('close_date');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('group_orders');
    }
}
