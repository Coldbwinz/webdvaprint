<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSingleFieldToPerformOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->boolean('single')->default(false)->after('preview');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->dropColumn('single');
        });
    }
}
