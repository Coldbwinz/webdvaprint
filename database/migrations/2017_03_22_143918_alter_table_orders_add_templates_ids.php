<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrdersAddTemplatesIds extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('template_cover_download_id')->nullable()->after('template_download_id');
            $table->integer('template_pad_download_id')->nullable()->after('template_cover_download_id');
            $table->string('pdf_cover')->nullable()->after('pdf');
            $table->string('pdf_pad')->nullable()->after('pdf_cover');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('template_cover_download_id');
            $table->dropColumn('template_pad_download_id');
            $table->dropColumn('pdf_cover');
            $table->dropColumn('pdf_pad');
        });
    }
}
