<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPricePartnerFieldFromGroupOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('price_partner');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('price_partner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->integer('price_partner')->after('price');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('price_partner')->after('price');
        });
    }
}
