<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesignerWorkTimeToOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('designer_working_starttime')->nullable()->after('designer_working');
            $table->integer('designer_working_time')->nullable()->after('designer_working_starttime');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('designer_working_time');
            $table->dropColumn('designer_working_starttime');
        });
    }
}
