<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGroupOrdersAddFieldPayHistory extends Migration
{
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->decimal('pay_cash')->after('pay');
            $table->decimal('pay_rs')->after('pay_cash');
            $table->decimal('pay_eth')->after('pay_rs');
        });
    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('pay_eth');
            $table->dropColumn('pay_rs');
            $table->dropColumn('pay_cash');
        });
    }
}
