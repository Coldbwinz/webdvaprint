<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateLibraryThemeColorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_library_theme_color', function (Blueprint $table) {
            $table->integer('theme_color_id')->unsigned()->index();
            $table->integer('template_library_id')->unsigned()->index();

            $table->foreign('theme_color_id')
                ->references('id')->on('theme_colors')
                ->onDelete('cascade');

            $table->foreign('template_library_id')
                ->references('id')->on('template_library')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_library_theme_color');
    }
}
