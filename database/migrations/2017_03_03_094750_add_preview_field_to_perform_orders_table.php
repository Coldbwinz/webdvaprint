<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreviewFieldToPerformOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->string('preview')->nullable()->after('templates_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perform_orders', function (Blueprint $table) {
            $table->dropColumn('preview');
        });
    }
}
