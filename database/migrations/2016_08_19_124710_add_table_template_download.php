<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTemplateDownload extends Migration
{
    public function up()
    {
        Schema::create('template_download', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('id_order');
            $table->boolean('downloading');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('template_download');
    }
}
