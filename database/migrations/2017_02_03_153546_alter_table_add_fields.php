<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddFields extends Migration
{
    public function up()
    {
        Schema::table('details', function (Blueprint $table) {
            $table->string('full_name')->after('name');
            $table->string('director_position')->after('director');
            $table->string('inn')->after('address');
            $table->string('kpp')->after('inn');
            $table->string('ogrn')->after('kpp');
        });
    }
    
    public function down()
    {
        Schema::table('details', function (Blueprint $table) {
            $table->dropColumn('full_name');
            $table->dropColumn('director_position');
            $table->dropColumn('inn');
            $table->dropColumn('kpp');
            $table->dropColumn('ogrn');
        });
    }
}
