<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrdersGroupOrdersAddPricePartner extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('price_partner')->after('price');
        });
        Schema::table('group_orders', function (Blueprint $table) {
            $table->integer('price_partner')->after('price');
        });

    }

    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn('price_partner');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('price_partner');
        });
    }
}
