<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableParserFilesAddName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_files', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
            $table->string('preview')->nullable()->after('upload_folder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_files', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('preview');
        });
    }
}
