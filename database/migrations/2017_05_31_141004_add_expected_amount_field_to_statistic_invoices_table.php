<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpectedAmountFieldToStatisticInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->decimal('expected_amount')->after('paid_invoice_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->dropColumn('expected_amount');
        });
    }
}
