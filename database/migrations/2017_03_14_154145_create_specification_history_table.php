<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specification_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specification_id')->unsigned();
            $table->integer('position');
            $table->text('data');
            $table->timestamps();

            $table->foreign('specification_id')
                ->references('id')->on('specifications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specification_history');
    }
}
