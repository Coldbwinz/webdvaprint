<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContractorOrderJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractor_order_jobs', function (Blueprint $table) {
            $table->float('price')->default(0.00)->after('status_id');
            $table->timestamp('deadline')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractor_order_jobs', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('deadline');
        });
    }
}
