<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldsFromTemplateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->dropColumn(['number', 'type_key', 'side']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_library', function (Blueprint $table) {
            $table->string('number')->after('activity');
            $table->string('type_key')->after('number');
            $table->string('side')->after('type_key');
        });
    }
}
