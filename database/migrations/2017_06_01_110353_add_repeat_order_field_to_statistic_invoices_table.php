<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRepeatOrderFieldToStatisticInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->boolean('repeat_order')->default(false)->after('new_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistic_invoices', function (Blueprint $table) {
            $table->dropColumn('repeat_order');
        });
    }
}
