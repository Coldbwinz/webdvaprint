<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductTypesDropServicesIdAddServicesJson extends Migration
{
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->json('services')->after('activity');
        });
    }

    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('services');
        });
    }
}
