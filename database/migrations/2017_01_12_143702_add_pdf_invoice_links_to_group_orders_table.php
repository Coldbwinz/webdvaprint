<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPdfInvoiceLinksToGroupOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->string('invoice_pdf')->nullable()->after('money_partner');
            $table->string('invoice_partner_pdf')->nullable()->after('invoice_pdf');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_orders', function (Blueprint $table) {
            $table->dropColumn(['invoice_pdf', 'invoice_partner_pdf']);
        });
    }
}
