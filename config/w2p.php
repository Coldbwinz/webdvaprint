<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Настройки по умолчанию
    |--------------------------------------------------------------------------
    |
    | Если запрашиваемая настройка для поддомена отсутствует в базе,
    | значение будет подставлено из приведённых ниже данных
    |
    */
    'defaults' => [
        'user_timezone' => config('app.timezone'),
        'company_name' => 'WEB2PRINT',
        'company_short_name' => 'W2P',
        'email_title' => 'WEB2PRINT',
        'logo' => '/images/logo.png',
        'meta_title' => 'WEB2PRINT - система управления заказами',
        'meta_description' => 'WEB2PRINT - система управления заказами',
        'color_primary' => '#367fa9',
        'color_secondary' => '#3c8dbc',
        'country' => 'russia',
        'user_timezone' => 'Europe/Moscow',
        'invoice_number' => 1,
        'act_number' => 1,
        '1c_sync' => 0,
    ],

    'company_picture_default' => 'images/company-logo-default.png',

    'subdomain_env_path' => env('SUBDOMAIN_ENVIRONMENT_PATH', base_path('subdomains-env.yml')),

    'template_size_tolerance' => 8,

    'templates' => [
        'v' => [
            'dimension' => '94x54',
            'folds' =>  0,
        ],
        'fb' => [ // Бланк
            'dimension' => '216x303',
            'folds' =>  0,
        ],
        'l' => [
            'dimension' => '216x303', // a4, a5, a6
            'folds' =>  0,
        ],
        'fg' => [
            'dimension' => '216x106',
            'folds' =>  0,
        ],
        'fv' => [
            'dimension' => '106x216',
            'folds' =>  0,
        ],
        'pg' => [
            'dimension' => '600x846',
            'folds' =>  0,
        ],
        'pv' => [
            'dimension' => '846x600',
            'folds' =>  0,
        ],
        'b1' => [
            'dimension' => '210x297', // changed
            'folds' =>  1,
        ],
        'b2' => [
            'dimension' => '210x297',
            'folds' =>  2,
        ],
        'b' => [ // Обложка блокнота
            'dimension' => '216x303',
            'folds' =>  0,
        ],
        'kg' => [
            'dimension' => '107x76',
            'folds' =>  0,
        ],
        'kv' => [
            'dimension' => '76x107',
            'folds' =>  0,
        ],
        'ke' => [
            'dimension' => '210x110',
            'folds' =>  0,
        ],
        'k4' => [
            'dimension' => '324x229',
            'folds' =>  0,
        ],
        'c' => [ // кубарик
            'dimension' => '96x96',
            'folds' =>  0,
        ],
        'd' => [
            'dimension' => '216x303',
            'folds' =>  0,
        ],
        'o5' => [
            'dimension' => '303x216',
            'folds' =>  1,
        ],
        'o5s' => [
            'dimension' => '154x216',
            'folds' =>  0,
        ],
        'oe' => [
            'dimension' => '216x216',
            'folds' =>  1,
        ],
        'oes' => [
            'dimension' => '106x216',
            'folds' =>  0,
        ],
    ]
];
