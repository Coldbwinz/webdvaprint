<?php

return [

    /**
     * Path to library folder with all psd templates
     */
    'path' => env('LIBRARY_PATH', '/home/user/library/psd'),

    /**
     * URL to psd parser api
     */
    'psd_parser_url' => env('PSD_PARSER_URL', 'http://w2parser.tw1.ru/parse.php'),

];