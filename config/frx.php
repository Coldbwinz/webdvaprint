<?php

return [
    
    'path' => [
        
        // path to library
        'library' => env('FRX_LIBRARY', '/var/www/library/parser_psd'),
        
        // path to preview image
        'preview' => 'lib/psd_preview',
        
        // path to user templates
        'templates' => 'lib/frx/default',
        
    ],
    
];

