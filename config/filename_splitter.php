<?php

return  [
    'templates' => [
        '/[a-zA-Z]+_\d{3}_\w{1,2}(_\d{1,2})?$/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => $parts[0],
                'number' => $parts[1],
                'type' => $parts[2],
                'side' => isset($parts[3]) ? 'back' : 'front',
            ];
        },

        '/[a-zA-Z0-9]+_\d{3}\w{1,2}(_\d{1,2})?$/' => function ($basename) {
            $parts = explode('_', $basename);

            $number = substr($parts[1], 0, 3);

            return [
                'vendor' => $parts[0],
                'number' => $number,
                'type' => str_replace($number, '', $parts[1]),
                'side' => isset($parts[2]) ? 'back' : 'front',
            ];
        },

        // viz_43_1_oborot.psd, viz_43_1_lico.psd
        '/viz_\d+_\d+_(lico|oborot)/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => 'shutterstock',
                'number' => $parts[0] . '_' . $parts[1],
                'type' => 'V',
                'side' => $parts[2] === 'lico' ? 'front' : 'back',
            ];
        },

        // viz_1_lico.psd, viz_18_oborot.psd
        '/viz_\d+_(lico|oborot)/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => 'shutterstock',
                'number' => $parts[0] . '_' . $parts[1],
                'type' => 'V',
                'side' => $parts[2] === 'lico' ? 'front' : 'back',
            ];
        },

        // Vinnikova_Viz004, Vinnikova_Viz004_2
        '/Vinnikova_Viz\d{3}(_2)?/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => 'Vinnikova',
                'number' => $parts[1],
                'type' => 'V',
                'side' => isset($parts[2]) ? 'back' : 'front',
            ];
        },

        // Lesenka_072KK1, Lesenka_072KK2, Vinnikova_043KV1, Vinnikova_043KV2
        '/(Lesenka|Vinnikova)_\d{3}[A-Z]{2}\d/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => $parts[0],
                'number' => substr($parts[1], 0, 3),
                'type' => substr($parts[1], 3, 2),
                'side' => substr($parts[1], 5, 1) == 2 ? 'back' : 'front',
            ];
        },

        '/Lesenka\d{3}V_2/' => function ($basename) {
            $basename = str_replace('Lesenka', '', $basename);

            return [
                'vendor' => 'Lesenka',
                'number' => substr($basename, 0, 3),
                'type' => 'V',
                'side' => substr($basename, -2) == '_2' ? 'back' : 'front',
            ];
        },

        '/vizitka_shutterstock_\d+(_\d+)?_\d{2}kh\d{2}(_litso|_oborot)?/' => function ($basename) {
            $parts = explode('_', $basename);

            $last = array_pop($parts);

            return [
                'vendor' => 'shutterstock',
                'number' => $parts[2],
                'type' => 'v',
                'side' => $last === 'oborot' ? 'back' : 'front',
            ];
        },

        // lesenka_070kk2, lesenka_070kv1, vinnikova_050kv1, vinnikova_050kv2
        '/(lesenka|vinnikova)_\d{3}[a-z]{2}\d/' => function ($basename) {
            $parts = explode('_', $basename);

            return [
                'vendor' => $parts[0],
                'number' => substr($parts[1], 0, 3),
                'type' => substr($parts[1], 3, 2),
                'side' => substr($parts[1], -1) == 2 ? 'back' : 'front'
            ];
        },

        '/white(\d{3})?_[a-z0-9]+(_2)?/' => function ($basename) {
            $parts = explode('_', $basename);

            $number = str_replace('white', '', $parts[0]);

            return [
                'vendor' => 'white',
                'number' => $number ?: '000',
                'type' => $parts[1],
                'side' => isset($parts[2]) ? 'back' : 'front',
            ];
        },

        '/vizitka_\w+/' => function () {
            return [
                'vendor' => 'unnamed',
                'number' => null,
                'type' => 'v',
                'side' => 'front',
            ];
        },
    ]
];
