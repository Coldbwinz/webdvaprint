<?php

namespace App;

use App\Http\Controllers\GroupOrdersController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\Order\OrderWasPaused;
use App\Events\Order\OrderWasResumed;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Orders extends Model
{
    use LocalizedEloquentDates;

    const STOPPED_FROM_HISTORY = 1;
    const STOPPED_FROM_QUEUE = 2;

    protected $quarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'price_details' => 'array',
        'stopped_reasons' => 'array',
        'issue_troubles' => 'array',
        'spec_description' => 'array',
        'options' => 'array',
        'draw' => 'integer',
        'postpress_operations' => 'array',
        'designer_working' => 'bool',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['product_name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['sent_revision_at'];

    public function token() {
        return $this->hasOne(ContractorToken::class, 'order_id');
    }

    public function productType() {
        return $this->belongsTo(ProductType::class, 'type_product');
    }

    public function productTypeName() {
        if ($this->type_product != 999999) {
            return ProductType::find($this->type_product)->name;
        } else {
            return $this->price_details['selected_product_type_name'];
        }
    }

    public function statusInfo() {
        return $this->belongsTo(OrderStatuses::class, 'status');
    }

    public function statusLastInfo() {
        return $this->belongsTo(OrderStatuses::class, 'status_last');
    }

    public function group() {
        return $this->belongsTo(GroupOrders::class, 'group_id');
    }

    public function client() {
        return $this->belongsTo(Clients::class, 'client_id');
    }

    /**
     * Attachment files associate with this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments() {
        return $this->hasMany(AttachmentFile::class, 'order_id');
    }

    /**
     * Specification (Техзадание)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function specification() {
        return $this->hasOne(Specification::class, 'order_id');
    }

    /**
     * Ready upload products files associate with this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function readyProducts() {
        return $this->hasMany(ReadyUploadProduct::class, 'order_id');
    }

    /**
     * Perform order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function performOrder()
    {
        return $this->belongsTo(PerformOrder::class);
    }

    public function manager() {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function logs()
    {
        return $this->hasMany(Order_logs::class, 'order_id');
    }

    public function contractorJobs()
    {
        return $this->hasOne(ContractorOrderJob::class, 'order_id');
    }

    public function contractor()
    {
        return $this->belongsToMany(Contractor::class, 'contractor_order_jobs', 'order_id', 'contractor_id');
    }

    /**
     * Template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(TemplateLibrary::class, 'template_library_id');
    }

    public function clientTemplate()
    {
        return $this->belongsTo(Template::class, 'template_id');
    }

    /**
     * Set status (update status_last)
     *
     * @param integer $status
     * @param boolean $updateLast
     */
    public function setStatus($status, $updateLast = true)
    {
        if ($updateLast) {
            $this->status_last = $this->status;
        }
        $this->status = $status;
    }

    public function stopped($state, $reason, $save = false)
    {
        $this->setStopReson($state, $reason);
        if ($state == 1000) {
            $this->status_last = $this->status;
            $this->main_sort_order = -1;
            $this->status = 1000;
            $this->stopped_user_id = auth()->user()->id;
            addLog($this->id, $this->status, auth()->user()->id, '', null, 0, $reason);
            (new Contractor)->sendStopped($this);

            $this->elements()->has('performOrder')->get()->each(function ($element) use ($reason) {
                $element->performOrder->pause($reason, false, 'history');
            });

        } elseif ($state == 0) {
            $this->main_sort_order = 0;
            $this->status = $this->status_last;
            addLog($this->id, 'Возобновлён', auth()->user()->id, '', null, 0, $reason);
            $this->status_last = 0;
            $this->stopped_user_id = null;
            (new Contractor)->sendStart($this);

            $this->elements()->has('performOrder')->get()->each(function ($element) use ($reason) {
                $element->performOrder->resume($reason, false);
            });

        } elseif ($state == 1010) {
            $this->status = 1010;
            $comment = '';
            $expense = $this->totalExpenseNumber();
            if (strlen($expense) > 0) {
                $comment = 'Клиент готов погасить затраты';
                $stoppedReasons = $this->stopped_reasons;
                $stoppedReasons['expenses'] = $expense;
                $stoppedReasons['price'] = $this->price;
                $expense .= ' руб.';
                $this->stopped_reasons = $stoppedReasons;
                $this->save();
                (new GroupOrdersController)->recalculateGroupPrice($this->group->id);
            }
            $group = $this->group;
            if ($group->pay >= $group->price_expenses) {
                if ($group->orders->where('status', 1010)->count() == $group->orders->count()) {
                    $group->status = 100;
                    $group->save();
                    addLog(0, 100, auth()->user()->id, '', $group->id, 0, '');
                }
            }
            addLog($this->id, $this->status, $this->group->client->user_id, $expense, null, 0, $comment);
        }
        if ($save) {
            $this->save();
        }
    }

    public function workWithTemplate($start, $save = false) {
        if ($start == 1) {
            $this->template_changed_user_id = auth()->user()->id;
        } else {
            $this->template_changed_user_id = null;
        }
        if ($save) {
            $this->save();
        }
    }

    public function setStopReson($state, $reason) {
        $stoppedReasons = $this->stopped_reasons;
        $stoppedReasons[$state] = $reason;
        $this->stopped_reasons = $stoppedReasons;
    }

    public function _price() {
        return ($this->group()->first()->subdomain) ? ((\Auth::user()->accessLevel() != 5) ? $this->price_partner : $this->price) : $this->price;
    }

    public function log($comment = '', $reason = '') {
        addLog($this->id, $this->status, \Auth::user()->id, $comment, null, 0, $reason);
    }

    public function seleted_chromacity_text() {
        if ($this->price_details['selected_chromacity'][4] == 0) {
            return '(односторон.)';
        } else {
            return '(двусторон.)';
        }
    }

    function sides() {
        return $this->price_details['selected_sides'];
    }

    function sidesOfTemplate() {
        if ($this->price_details['printler_type'] < 3) {
            return 1;
        } else {
            return $this->price_details['sides'];
        }
    }

    /**
     * Scope orders with "in work" status
     *
     * @param $query
     * @return mixed
     */
    public function scopeInWork($query)
    {
        return $query->whereIn('status', status(['in_work'], 'id'));
    }

    /**
     * Заказы на согласовании
     *
     * @param $query
     */
    public function scopeOnAgreement($query)
    {
        $query->whereIn('status', status([
            'on_agreeing', 'sent_for_approval', 'sent_again', 'layout_prepares_client',
        ], 'id'));
    }

    /**
     * Заказы, требующие внимания
     *
     * @param $query
     */
    public function scopeRequireAttention($query)
    {
        $query->whereIn('status', status([
            'layout_approved', 'shown_for_client', 'sent_again', 'layout_checked', 'ready',
            'return_marriage', 'layout_changed', 'layout_replaced', 'paused',
        ], 'id'));
    }

    /**
     * Заказы, готовые к выдаче
     *
     * @param $query
     */
    public function scopeReady($query)
    {
        $query->whereIn('status', status([
            'on_issue'
        ], 'id'));
    }

    /**
     * Все позиции, где grouporder принадлежит типографии
     *
     * @param $query
     */
    public function scopeForTypography($query)
    {
        $query->whereHas('group', function ($q) {
            $q->whereNull('subdomain_id');
        });
    }

    /**
     * Filter query
     *
     * @param $query
     * @param Request $request
     * @throws \Exception
     */
    public function scopeFilter($query, Request $request)
    {
        if ($request->has('status_id')) {
            $statusId = $request->input('status_id');

            if (is_array($statusId)) {
                return $query->whereIn('status', $statusId);
            }

            return $query->where('status', $statusId);
        }

        if ($request->has('range')) {
            $period = explode(' - ', $request->input('range'));

            if (count($period) !== 2) {
                throw new \Exception('Invalid date');
            }

            $dateFrom = Carbon::parse($period[0])->setTimezone(settings('user_timezone'))->startOfDay();
            $dateTo = Carbon::parse($period[1])->setTimezone(settings('user_timezone'))->endOfDay();

            $query->whereBetween('close_date', [$dateFrom->timestamp, $dateTo->timestamp]);
        }
    }

    /**
     * Select template for order
     *
     * @param TemplateLibrary $template
     */
    public function selectTemplate(TemplateLibrary $template)
    {
        $folder = str_random();
        $destination = public_path('lib/frx/'.$folder);

        $source = storage_path('app/templates/'.pathinfo($template->w2p, PATHINFO_FILENAME));

        if (is_dir($source)) {
            recursive_copy_flatten($source, $destination);
        }

        $frxFile = public_path("lib/frx/".$folder.".frx");
        \App\Classes\Parser\Frx::copyFrx($template->w2p, $frxFile);

        $this->file = $folder.".frx";
        $this->template()->associate($template);
    }

    /**
     * Элементы заказа (например обложка, вн. блок и т.д.)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function elements()
    {
        return $this->hasMany(OrderElement::class, 'order_id');
    }

    /**
     * Родитель заказа, если заказ является доработкой
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Orders::class, 'parent_id');
    }

    /**
     * Проверяет, является ли заказ переделкой к другому
     *
     * @return bool
     */
    public function isRework()
    {
        return ! is_null($this->parent);
    }

    /**
     * Дизайнер начал работу над заказом
     *
     * @return $this
     */
    public function designerStartWorking()
    {
        $this->designerStopAllWorks();

        $this->designer_working = true;
        $this->designer_working_starttime = microtime(true);
        $this->save();

        addLog($this->id, 'Разработка макета', auth()->user()->id, ($this->status == 301) ? 'совместно с заказчиком' : '', null, 0, "Дизайнер начал выполнение");

        return $this;
    }

    /**
     * Дизайнер присотановил работу над заказом
     *
     * @return $this
     */
    public function designerStopWorking()
    {
        if ($this->designer_working) {
            $this->designer_working = false;
            $this->designer_working_time += microtime(true) - $this->designer_working_starttime;
            $this->save();
            addLog($this->id, 'Приостановлено', auth()->user()->id, 'Работа дизайнера: '.round($this->designer_working_time / 3600,2).' ч.' , null, 0, "Дизайнер приостановил выполнение");
        }

        return $this;
    }

    /**
     * Остановить все работы дизайнера
     *
     * @todo Потенциально большое кол-во запросов к базе. Придумать решение лучше.
     */
    public function designerStopAllWorks()
    {
        $orders = Orders::where('designer_working', 1)
            ->get();

        $orders->each(function (Orders $order) {
            $order->designerStopWorking();
        });
    }

    /**
     * [getPageType description]
     *
     * @param  Orders $order
     * @return string
     *//*
    public function getPageType()
    {
        if ($this->price_details['printler_type'] != 3) {
            return null;
        }
        if (isset($this->price_details['selecter_cover'])) {
            $count = $this->price_details['selected_product'];
            $type = 'cover';
        }
        elseif(isset($this->price_details['selected_pad'])) {
            $type = 'backcover'
        }
        elseif(isset($this->price_details['pages'])) {
            $count = $this->price_details['selected_product'];
            $type = 'pages'
        }
    }*/

    /**
     * Получает название продукта у заказа
     *
     * @return string
     */
    public function getProductNameAttribute()
    {
        if (is_null($this->productType)) {
            return isset($this->price_details['selected_product_type_name'])
                ? $this->price_details['selected_product_type_name']
                : 'unknown';
        }

        return $this->productType->name;
    }

    /**
     * Appends logs to the order
     *
     * @param Collection $logs
     * @return array|Collection
     */
    public function appendLogs(Collection $logs)
    {
        $logs = $logs->map(function ($log) {
            $log = $log->replicate();
            $log->order_id = null;
            $log->group_order_id = null;

            return $log;
        });

        return $this->logs()->saveMany($logs);
    }

    /**
     * Pause order processing
     *
     * @param string|null $reason
     */
    public function pause($reason = null)
    {
        $this->stopped(1000, $reason);
        $this->stopped_from = static::STOPPED_FROM_QUEUE;
        $this->save();

        event(new OrderWasPaused($this));
    }

    /**
     * Resume order processing
     *
     * @param string|null $reason
     */
    public function resume($reason = null)
    {
        $this->stopped(0, $reason);
        $this->stopped_from = null;
        $this->save();

        event(new OrderWasResumed($this));
    }

    public function complectOrder()
    {
        return $this->belongsTo(Orders::class, 'complect_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function complect()
    {
        return $this->hasMany(Orders::class, 'complect_id');
    }

    public function getExpense()
    {
        $template_cost = $this->getExpenseTemplateCost();
        if (($this->status_last >= 35 ) || ($this->sort_order >= 56)) {
            return round($this->price_details['price'] * 0.75 + $template_cost, 2);
        } else if ($this->status_last == 30 ) {
            return round($this->price_details['price'] * 0.3 + $template_cost, 2);
        }
        return '';
    }

    public function getExpenseTemplateCost()
    {
        if ($this->price_details['template_cost'] > 0 && !in_array($this->status,[3, 4, 5, 10])) {
            return $this->price_details['template_cost'];
        }
        return '';
    }

    public function totalExpenseNumber() {
        return $this->getExpense() + $this->getExpenseTemplateCost();
    }

    public function totalExpense() {
        $expense = $this->getExpense();
        $expenseTemplate = $this->getExpenseTemplateCost();
        if (strlen($expenseTemplate) > 0) {
            $expenseTemplate = (($expense > $expenseTemplate) ? ', в том числе ' : '') . 'разработка макета ' . $this->price_details['template_cost'] . ' руб.';
        }
        if ($expense > $expenseTemplate) {
            $expense  .= ' руб.';
        } else {
            $expense = '';
        }
        $total_expense = $expense . $expenseTemplate;
        return (strlen($total_expense) > 0) ? 'Фактически выполненные работы и понесённые затраты: ' . $total_expense : '';
    }

    /**
     * Номер заказа
     *
     * @return string
     */
    public function getNumberAttribute()
    {
        return $this->group->id . '.' . $this->position;
    }
}
