<?php

namespace App;

use App\Models\Salary\SalaryBonus;
use App\Models\Salary\SalaryMotivationScheme;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    const ROOT = 4;
    const ADMIN = 3;
    const MANAGER = 2;
    const CLIENT = 1;
    const PARTNER = 5;
    const DIRECTOR = 6;
    const DESIGNER = 7;

    /**
     * @var string
     */
    public $table = 'users_groups';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'role', 'new_clients_count_plan', 'repeated_orders_count_plan', 'invoices_total_plan',
        'paid_invoices_total_plan', 'substractions', 'salary',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'bonuses' => 'object',
        'coefficients' => 'object',
        'substractions' => 'object',
        'salary' => 'object',
    ];

    /**
     * Бонусы по зарплате для роли
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaryBonuses()
    {
        return $this->hasMany(SalaryBonus::class, 'user_group_id');
    }

    /**
     * Схемы мотивации
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaryMotivationSchemes()
    {
        return $this->hasMany(SalaryMotivationScheme::class);
    }
}
