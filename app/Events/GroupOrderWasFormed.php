<?php

namespace App\Events;

use App\Events\Event;
use App\GroupOrders;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GroupOrderWasFormed extends Event
{
    use SerializesModels;

    /**
     * @var GroupOrders
     */
    public $groupOrder;

    /**
     * Create a new event instance.
     *
     * @param GroupOrders $groupOrder
     */
    public function __construct(GroupOrders $groupOrder)
    {
        $this->groupOrder = $groupOrder;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
