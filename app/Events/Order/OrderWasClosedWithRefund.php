<?php

namespace App\Events\Order;

use App\Orders;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class OrderWasClosedWithRefund extends Event
{
    use SerializesModels;

    /**
     * @var Orders
     */
    public $order;

    /**
     * @var int
     */
    public $compensation;

    /**
     * Create a new event instance.
     *
     * @param Orders $order
     * @param int $compensation
     */
    public function __construct(Orders $order, $compensation)
    {
        $this->order = $order;
        $this->compensation = $compensation;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
