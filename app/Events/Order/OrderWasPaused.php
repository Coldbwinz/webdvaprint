<?php

namespace App\Events\Order;

use App\Events\Event;
use App\Orders;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderWasPaused extends Event
{
    use SerializesModels;

    /**
     * @var Orders
     */
    protected $order;

    /**
     * Create a new event instance.
     *
     * @param Orders $order
     */
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
