<?php

namespace App\Events\Order;

use App\Orders;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class OrderWasClosedWithDiscount extends Event
{
    use SerializesModels;

    /**
     * @var Orders
     */
    public $order;

    /**
     * @var integer
     */
    public $discountAmount;

    /**
     * @var string
     */
    public $comment;

    /**
     * Create a new event instance.
     *
     * @param Orders $order
     * @param integer $discountAmount
     * @param string $comment
     */
    public function __construct(Orders $order, $discountAmount, $comment)
    {
        $this->order = $order;
        $this->discountAmount = $discountAmount;
        $this->comment = $comment;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
