<?php

namespace App\Events\PerformOrder;

use App\Events\Event;
use App\PerformOrder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PerformOrderWasPaused extends Event
{
    use SerializesModels;

    /**
     * @var PerformOrder
     */
    public $performOrder;

    /**
     * Create a new event instance.
     *
     * @param PerformOrder $performOrder
     */
    public function __construct(PerformOrder $performOrder)
    {
        $this->performOrder = $performOrder;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
