<?php

namespace App\Events;

use App\GroupOrders;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PaymentWasAdded extends Event
{
    use SerializesModels;

    /**
     * @var GroupOrders
     */
    public $groupOrder;

    /**
     * @var
     */
    public $amount;

    /**
     * @var integer
     */
    public $payMethod;

    /**
     * Create a new event instance.
     *
     * @param GroupOrders $groupOrder
     * @param integer $amount
     * @param integer $payMethod
     */
    public function __construct(GroupOrders $groupOrder, $amount, $payMethod)
    {
        $this->groupOrder = $groupOrder;
        $this->amount = $amount;
        $this->payMethod = $payMethod;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
