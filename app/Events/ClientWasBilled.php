<?php

namespace App\Events;

use App\Clients;
use App\GroupOrders;
use Illuminate\Queue\SerializesModels;

class ClientWasBilled extends Event
{
    use SerializesModels;

    /**
     * @var Clients
     */
    public $client;

    /**
     * @var GroupOrders
     */
    public $groupOrder;

    /**
     * Create a new event instance.
     *
     * @param Clients $client
     * @param GroupOrders $groupOrder
     */
    public function __construct(Clients $client, GroupOrders $groupOrder)
    {
        $this->client = $client;
        $this->groupOrder = $groupOrder;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
