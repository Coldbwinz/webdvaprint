<?php

namespace App\Models\Equipment;

use App\Classes\CurrencyTranslator\CurrencyTranslator;
use App\Models\BaseModel;
use Illuminate\Support\Arr;

class Equipment extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'properties'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'properties' => 'object',
    ];

    /**
     * Equipment print type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function printType()
    {
        return $this->belongsTo(EquipmentPrintType::class, 'equipment_print_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function printParameters()
    {
        return $this->hasMany(EquipmentPrintParameter::class);
    }

    /**
     * Delete all print parameters from equipment
     *
     * @return mixed
     */
    public function unsetPrintParameters()
    {
        foreach ($this->printParameters as $parameter) {
            $parameter->delete();
        }
    }

    /**
     * @param $maxPrintSheetFormat
     * @param $technologicalFields
     * @param $valve
     * @param $printColorData
     * @param bool $bannerPrint
     */
    public function addPrintParameter($maxPrintSheetFormat, $technologicalFields, $valve, $printColorData, $bannerPrint = false)
    {
        $printType = $this->printType->name . ' ' . trans('dictionary.equipment.'.$printColorData['chromaticity']) . ($bannerPrint ? ' баннерная' : '');

        $priceValue = Arr::get($printColorData, 'click_price.value');
        $priceCurrency = Arr::get($printColorData, 'click_price.currency');

        $costPrice = $priceValue;
        if ($priceCurrency !== CurrencyTranslator::CURRENCY_RUB) {
            $costPrice = app(CurrencyTranslator::class)->convertToRub($priceValue, $priceCurrency);
        }

        $printSheetWidth = $maxPrintSheetFormat['width'];
        $printSheetHeight = $maxPrintSheetFormat['height'];

        if ($technologicalFields > 0) {
            $printSheetWidth -= $technologicalFields * 2;
            $printSheetHeight -= $technologicalFields * 2;
        }

        if ($valve['value'] > 0) {
            // По узкой стороне
            if ($valve['side'] === 'narrow') {
                if ($maxPrintSheetFormat['width'] >= $maxPrintSheetFormat['height']) {
                    $printSheetWidth -= $valve['value'];
                } else {
                    $printSheetHeight -= $valve['value'];
                }
            }

            // По широкой стороне
            if ($valve['side'] === 'wide') {
                if ($maxPrintSheetFormat['width'] >= $maxPrintSheetFormat['height']) {
                    $printSheetHeight -= $valve['value'];
                } else {
                    $printSheetWidth -= $valve['value'];
                }
            }
        }

        $this->printParameters()->create([
            'print_type' => $printType,
            'print_width' => $printSheetWidth,
            'print_height' => $printSheetHeight,
            'cost_price' => $costPrice,
            'properties' => [
                'max_sheet_width' => $maxPrintSheetFormat['width'],
                'max_sheet_height' => $maxPrintSheetFormat['height'],
                'technological_fields' => $technologicalFields,
                'valve' => $valve['value'],
                'valve_side' => $valve['side'],
                'chromaticity' => $printColorData['chromaticity'],
                'click_price' => Arr::get($printColorData, 'click_price.value'),
                'currency' => Arr::get($printColorData, 'click_price.currency'),
                'performance' => $printColorData['performance'],
                'rebuilding' => $printColorData['rebuilding'],
                'fitting_1' => $printColorData['fitting_1'],
                'fitting_2' => $printColorData['fitting_2'],
                'defect_norm' => $printColorData['defect_norm'],
                'banner_print' => $bannerPrint,
            ]
        ]);
    }
}
