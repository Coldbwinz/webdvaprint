<?php

namespace App\Models\Equipment;

use Illuminate\Database\Eloquent\Model;

class EquipmentPrintParameter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['print_type', 'print_width', 'print_height', 'cost_price', 'properties'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'properties' => 'object',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    /**
     * @return string
     */
    public function getMaxPrintFormatAttribute()
    {
        return $this->properties->max_sheet_width . 'x' . $this->properties->max_sheet_height;
    }

    /**
     * @return string
     */
    public function getPrintFormatAttribute()
    {
        return $this->print_width . 'x' . $this->print_height;
    }
}
