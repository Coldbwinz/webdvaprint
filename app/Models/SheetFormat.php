<?php

namespace App\Models;

use App\Traits\EloquentExtended;

class SheetFormat extends BaseModel
{
    use EloquentExtended;

    const SHEET_FORMAT_PRINT = 'print';
    const SHEET_FORMAT_INITIAL = 'initial';
    const SHEET_FORMAT_MAX = 'max';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['width', 'height', 'type'];
}
