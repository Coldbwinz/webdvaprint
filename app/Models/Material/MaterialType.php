<?php

namespace App\Models\Material;

use Illuminate\Database\Eloquent\Model;

class MaterialType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
