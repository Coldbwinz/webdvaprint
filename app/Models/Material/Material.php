<?php

namespace App\Models\Material;

use App\Classes\CurrencyTranslator\CurrencyTranslator;
use App\Models\Material\CoverType;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price_ton', 'price_ton_currency', 'initial_sheet_width', 'initial_sheet_height',
        'print_sheet_width', 'print_sheet_height', 'print_sheets_number', 'sheet_price', 'density',
        'stock_availability', 'stock_balance', 'lamination',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'lamination' => 'boolean',
    ];

    /**
     * Тип покрытия
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coverType()
    {
        return $this->belongsTo(CoverType::class);
    }

    /**
     * Тип материала
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }

    /**
     * Material format
     *
     * @return string
     */
    public function getFormatAttribute()
    {
        return $this->print_sheet_width . 'x' . $this->print_sheet_height;
    }

    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->materialType->name . ' ' . $this->coverType->name
            .' ('. $this->name .') '. $this->density .' г/м.кв';
    }

    /**
     * Считает цену за лист в рублях
     *
     * @param integer $initialSheetWeight
     * @param integer $printSheetsNumber
     * @param integer $priceTon
     * @param string $currency
     * @return float
     */
    public static function calculateSheetPrice($initialSheetWeight, $printSheetsNumber, $priceTon, $currency)
    {
        $printTonRub = $priceTon;

        if (strtoupper($currency) !== CurrencyTranslator::CURRENCY_RUB) {
            $printTonRub = app(CurrencyTranslator::class)->convertToRub($priceTon, $currency);
        }

        $initialSheetPrice = $initialSheetWeight * $printTonRub / 1000000;

        return $initialSheetPrice / $printSheetsNumber;
    }

    /**
     * Update material per sheet price
     *
     * @return bool
     */
    public function updateSheetPrice()
    {
        $initialSheetWeight = $this->initial_sheet_width * $this->initial_sheet_height / 1000000 * $this->density;

        $this->sheet_price = static::calculateSheetPrice(
            $initialSheetWeight, $this->print_sheets_number, $this->price_ton, $this->price_ton_currency
        );

        return $this->save();
    }
}
