<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    /**
     * Save a new model if not exists and return the instance.
     *
     * @param  array $attributes
     * @return static|boolean
     */
    public static function createIfNotExists(array $attributes = [])
    {
        if (! static::where($attributes)->exists()) {
            return static::create($attributes);
        }

        return false;
    }
}
