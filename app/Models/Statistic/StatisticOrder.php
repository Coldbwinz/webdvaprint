<?php

namespace App\Models\Statistic;

use DB;
use App\Traits\Managerable;
use App\Services\CarbonDatePeriod;
use Illuminate\Database\Eloquent\Model;

class StatisticOrder extends Model
{
    use Managerable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['repeat'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'repeat' => 'boolean',
    ];

    /**
     * Количество повторных заказов за период
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer $managerId
     *
     * @return int
     */
    public static function repeatedOrdersCount(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_orders')
            ->select(DB::raw('count(*) as repeated_orders_count'))
            ->where('repeat', true)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int) $records->value('repeated_orders_count');
    }
}
