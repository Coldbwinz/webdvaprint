<?php

namespace App\Models\Statistic;

use DB;
use Carbon\Carbon;
use App\Traits\Managerable;
use App\Services\CarbonDatePeriod;
use Illuminate\Database\Eloquent\Model;

class StatisticInvoice extends Model
{
    use Managerable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_amount', 'paid_invoice_amount', 'expected_amount', 'full_paid',
        'new_order', 'created_at', 'repeat_order',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'full_paid' => 'boolean',
        'new_order' => 'boolean',
    ];

    /**
     * Получает сгруппированные суммы выставленных и оплаченных счетов за период
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer|null $managerId
     * @return
     */
    public static function groupedForPeriod(CarbonDatePeriod $period, $managerId = null)
    {
        $diffInDays = $period->diffInDays();

        $groupByFunction = $diffInDays <= 31 ? 'DAY' : 'WEEK';

        $records = DB::table('statistic_invoices')
            ->select(DB::raw(
                'DATE(created_at) as date,
                SUM(invoice_amount) as invoices,
                SUM(paid_invoice_amount) as paid_invoices,
                SUM(expected_amount) as expected'
            ))
            ->whereBetween('created_at', $period->toArray())
            ->groupBy(DB::raw("{$groupByFunction}(created_at)"))
            ->orderBy('date', 'asc');

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return $records->get();
    }

    /**
     * Получает сумму выставленных счетов за период
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer|null $managerId
     * @return mixed
     */
    public static function invoicesSum(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('sum(invoice_amount) as invoices_total'))
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return $records->value('invoices_total');
    }

    /**
     * Получает сумму оплаченных счетов за период
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer|null $managerId
     * @return mixed
     */
    public static function paidInvoicesSum(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('sum(paid_invoice_amount) as paid_invoices_total'))
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int) $records->value('paid_invoices_total');
    }

    /**
     * Количество выставленных счетов для
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer|null $managerId
     * @return mixed
     */
    public static function invoicesCount(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('count(*) as invoices_count'))
            ->where('invoice_amount', '>', 0)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return $records->value('invoices_count');
    }

    /**
     * Количество полностью оплаченных счетов
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer|null $managerId
     * @return mixed
     */
    public static function paidInvoicesCount(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('count(*) as invoices_count'))
            ->where('full_paid', true)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return $records->value('invoices_count');
    }

    /**
     * Get total invoices for the new orders
     *
     * @param $period
     * @param $managerId
     * @return mixed
     */
    public static function newOrdersTotal($period, $managerId)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('sum(invoice_amount) as invoices_total'))
            ->where('new_order', true)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int)$records->value('invoices_total');
    }

    /**
     * Repeat order total
     *
     * @param $period
     * @param $managerId
     * @return int
     */
    public static function repeatOrdersTotal($period, $managerId)
    {
        $records = DB::table('statistic_invoices')
            ->select(DB::raw('sum(invoice_amount) as invoices_total'))
            ->where('repeat_order', true)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int)$records->value('invoices_total');
    }

    /**
     * Get expected amount sum
     *
     * @param null $managerId
     *
     * @return int
     */
    public static function expectedAmountSum($managerId = null)
    {
        $records = DB::table('group_orders')
            ->select(DB::raw('sum(price) - sum(pay) as expected_total'))
            ->where('close_date', '>=', Carbon::now());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int) $records->value('expected_total');
    }

    /**
     * Просроченная задолженность
     *
     * @param $managerId
     * @return int
     */
    public static function arrearsAmountSum($managerId = null)
    {
        $records = DB::table('group_orders')
            ->select(DB::raw('sum(price) - sum(pay) as expected_total'))
            ->where('close_date', '<', Carbon::now());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int) $records->value('expected_total');
    }
}
