<?php

namespace App\Models\Statistic;

use DB;
use App\Traits\Managerable;
use App\Services\CarbonDatePeriod;
use Illuminate\Database\Eloquent\Model;

class StatisticClient extends Model
{
    use Managerable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['new_client'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'new_client' => 'boolean',
    ];

    /**
     * Количество новых клиентов за месяц
     *
     * @param \App\Services\CarbonDatePeriod $period
     * @param integer $managerId
     *
     * @return int
     */
    public static function newClientsCount(CarbonDatePeriod $period, $managerId = null)
    {
        $records = DB::table('statistic_clients')
            ->select(DB::raw('count(*) as new_clients_count'))
            ->where('new_client', true)
            ->whereBetween('created_at', $period->toArray());

        if (! is_null($managerId)) {
            $records->where('manager_id', $managerId);
        }

        return (int) $records->value('new_clients_count');
    }
}
