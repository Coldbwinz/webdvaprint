<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;

class SalaryMotivationScheme extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_group_id', 'percent', 'salary', 'premium', 'bonus', 'bonus_coefficient',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'percent' => 'integer',
        'bonus_coefficient' => 'float',
    ];
}
