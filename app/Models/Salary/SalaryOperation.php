<?php

namespace App\Models\Salary;

use App\Clients;
use App\Services\CarbonDatePeriod;
use Illuminate\Database\Eloquent\Model;

class SalaryOperation extends Model
{
    const SALARY_BONUS = 'bonus';
    const SALARY_ACCRUAL = 'accrual';
    const SALARY_DEDUCTION = 'deduction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'action', 'name', 'client_id', 'value', 'paid_sum', 'payment_type',
        'balance', 'calculation_to', 'accrued', 'receivable', 'is_header',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['calculation_to'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_header' => 'boolean',
    ];

    /**
     * Идентификатор клиента, с заказом которого связана операция
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Clients::class);
    }

    /**
     * Filter by period on created_at
     *
     * @param $query
     * @param CarbonDatePeriod $period
     */
    public function scopeFilterByPeriod($query, CarbonDatePeriod $period)
    {
        $query->whereBetween('created_at', $period->toArray());
    }

    /**
     * Filter scope
     *
     * @param $query
     * @param array $filters
     */
    public function scopeFilter($query, array $filters)
    {
        if (! empty($filters['period'])) {
            $period = CarbonDatePeriod::makeFromString($filters['period'], ' - ');
            $query->filterByPeriod($period);
        }
    }
}
