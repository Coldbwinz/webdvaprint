<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExportSetting extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'alias', 'chromacity', 'subdomain_id'];

    /**
     * Get settings for current subdomain
     *
     * @param $query
     */
    public function scopeForCurrentSubdomain($query)
    {
        $query->where('subdomain_id', Subdomain::findCurrentId());
    }
}
