<?php

namespace App;

use App\Traits\SubdomainTrait;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;

class Order_logs extends Model
{
    use LocalizedEloquentDates, SubdomainTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statusInfo()
    {
        return $this->belongsTo(OrderStatuses::class, 'status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    public static function templateNotAgree($order_id)
    {
        $order = Order_logs::where('order_id', $order_id)->orderBy('id', 'desc')->first();
        if (count($order) > 0) {
            return $order->status_free;
        }
        return false;
    }
}
