<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    protected $table = 'password_resets';

    protected $fillable = ['user_id', 'email', 'token', 'activate', 'activate_at', 'created_at'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Create record with user
     *
     * @param \App\User $user
     * @return \App\PasswordReset::class
     */
    public static function createAuto(\App\User $user)
    {
        return self::create([
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => self::generateToken(),
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * Generate random token
     *
     * @return string
     */
    public static function generateToken()
    {
        return md5('password_resets_' . str_random(10).'_'.time());
    }

}
