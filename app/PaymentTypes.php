<?php

namespace App;

use App\Traits\SubdomainTrait;
use Illuminate\Database\Eloquent\Model;

class PaymentTypes extends Model
{
    use SubdomainTrait;

    protected $table = 'payment_types';

    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    public static function getDefaultPaymentTypes($subdomainId)
    {
        $result = [];
        $payment_types = PaymentTypes::where('subdomain_id',null)->get();
        foreach ($payment_types as $payment_type) {
            array_push($result, ['subdomain_id' => $subdomainId, 'name' => $payment_type->name, 'discount' => 0, 'name' => $payment_type->name]);
        }
        return $result;
    }

    public static function issetTable() {
        return PaymentTypes::where('discount', '!=', 0)->where('subdomain_id',Subdomain::findCurrentId())->count() > 0;
    }
}
