<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryTypes extends Model
{
    public $table = 'delivery_types';
}
