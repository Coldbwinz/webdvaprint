<?php

namespace App;

use App\Classes\Parser\Frx;
use App\Classes\Parser\RubyParser;
use App\Contracts\TemplateLibraryContract;
use App\Traits\TemplateLibraryFilters;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class TemplateLibrary extends Model implements TemplateLibraryContract
{
    use TemplateLibraryFilters;

    protected $table = 'template_library';

    protected $fillable = [
        'w2p', 'psd', 'url', 'type', 'two_side', 'themes', 'name', 'activity', 'width', 'height', 'folds',
        'collection', 'type_key', 'owner_id', 'status', 'ratio', 'product_type_id',
    ];

    /**
     * Themes, related to this template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function themes()
    {
        return $this->belongsToMany(Theme::class, 'template_library_theme');
    }

    /**
     * Colors, related to the template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function colors()
    {
        return $this->belongsToMany(ThemeColor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Orders with this template
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'template_library_id');
    }

    /**
     * @param ParserFiles $file
     * @param null $type
     * @param bool $twoSide
     * @return static
     */
    public static function makeFromParsedFile(ParserFiles $file, $type = null, $twoSide = false)
    {
        $ftype = str_replace('__2', '', $file->type);
        $dimension = config("w2p.templates.{$ftype}.dimension");
        $width = null;
        $height = null;

        if ($dimension) {
            $dimension = explode('x', $dimension);
            $width = $dimension[0];
            $height = $dimension[1];
        }

        $folds = config("w2p.templates.{$ftype}.folds", 0);

        return static::create([
            'w2p' => $file->getFrxPath(),
            'psd' => $file->path,
            'url' => $file->preview,
            'type' => $type,
            'two_side' => $twoSide,
            'name' => '',
            'activity' => true,
            'width' => $width,
            'height' => $height,
            'folds' => $folds,
            'collection' => $file->vendor.'_'.$file->number,
            'type_key' => $ftype,
        ]);
    }

    /**
     * Check for psd already added to library
     *
     * @param $psdFilePath
     * @return mixed
     */
    public static function hasPsd($psdFilePath)
    {
        return (bool) static::where('psd', $psdFilePath)->count();
    }

    /**
     * Scope active templates
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('activity', true);
    }

    /**
     * Фильтрует шаблоны по ratio
     *
     * @param $query
     * @param $ratioRange
     * @param null $ratioRangeInversed
     * @return mixed
     */
    public function scopeRatioInRange($query, $ratioRange, $ratioRangeInversed = null)
    {
        sort($ratioRange);
        sort($ratioRangeInversed);

        if (is_null($ratioRangeInversed)) {
            return $query->whereBetween('ratio', $ratioRange);
        }

        return $query->where(function ($query) use ($ratioRange, $ratioRangeInversed) {
            $query->whereBetween('ratio', $ratioRange)
                ->orWhereBetween('ratio', $ratioRangeInversed);
        });
    }

    public static function makeFromPsd($frontPsdPath, $backPsdPath = null)
    {
        if (! file_exists($frontPsdPath)) {
            throw new FileNotFoundException("File {$frontPsdPath} not exists.");
        }

        if (! is_null($backPsdPath) && ! file_exists($backPsdPath)) {
            throw new FileNotFoundException("File {$backPsdPath} not exists.");
        }

        // Односторонний макет
        if (is_null($backPsdPath)) {
            $pathInfo = pathinfo($frontPsdPath);
            $frxPath = $pathInfo['dirname'];
            $frxFilename = $pathInfo['filename'];

            // Распарсить первый шаблон
            if (! RubyParser::parse($frontPsdPath)) {
                throw new \Exception('Ошибка парсинга файла ' . $frontPsdPath);
            };

            $frxFilePath = $frxPath.'/'.$frxFilename.'.frx';
            $psdPreviewPath = '/lib/psd_preview/'.$frxFilename.'.png';

            // Copy preview
            copy($frxPath.'/'.$frxFilename.'.png', public_path($psdPreviewPath));

            // Создать новый шаблон в базе
            $template = new static();
            $template->w2p = $frxFilePath;
            $template->psd = $frontPsdPath;
            $template->url = $psdPreviewPath;
            $template->activity = true;
            $template->save();

            return $template;
        }

        // Двухсторонний макет
        $frontPathInfo = pathinfo($frontPsdPath);
        $frontFrxPath = $frontPathInfo['dirname'];
        $frontFrxFilename = $frontPathInfo['filename'];

        $backPathInfo = pathinfo($backPsdPath);
        $backFrxPath = $backPathInfo['dirname'];
        $backFrxFilename = $backPathInfo['filename'];

        // Распарсить первый шаблон
        if (! RubyParser::parse($frontPsdPath)) {
            throw new \Exception('Ошибка парсинга файла ' . $frontPsdPath);
        };

        // Распарсить второй шаблон
        if (! RubyParser::parse($backPsdPath)) {
            throw new \Exception('Ошибка парсинга файла ' . $backPsdPath);
        };

        $frontFrxFilePath = $frontFrxPath.'/'.$frontFrxFilename.'.frx';
        $backFrxFilePath = $backFrxPath.'/'.$backFrxFilename.'.frx';

        // Объединить frx шаблоны
        $combinedFrxFilename = str_random();
        $newFrxPath = storage_path('app/templates/'.$combinedFrxFilename);
        $combinedFrxFilePath = $newFrxPath.'.frx';

        mkdir($newFrxPath, 0777, true);

        Frx::combine($frontFrxFilePath, $backFrxFilePath, $combinedFrxFilePath);

        // Copy previews
        mkdir(public_path('lib/psd_preview/' . $combinedFrxFilename), 0777, true);
        $frontPreview = public_path('lib/psd_preview/'.$combinedFrxFilename.'/'.$combinedFrxFilename.'.png');
        $backPreview = public_path('lib/psd_preview/'.$combinedFrxFilename.'/'.$combinedFrxFilename.'_2.png');

        copy($frontFrxPath.'/'.$frontFrxFilename.'.png', $frontPreview);
        copy($backFrxPath.'/'.$backFrxFilename.'.png', $backPreview);

        // Создать новый шаблон в базе
        $template = new static();
        $template->w2p = $combinedFrxFilePath;
        $template->psd = $newFrxPath.'.psd';
        $template->url = $frontPreview;
        $template->second_preview = $backPreview;
        $template->activity = true;
        $template->save();

        return $template;
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusReadableName($status)
    {
        $names = [
            'new' => 'Новый',
            'parsing' => 'Обработка',
            'ready' => 'Готов',
            'error' => 'Ошибка',
        ];

        return Arr::get($names, $status, $status);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }
}
