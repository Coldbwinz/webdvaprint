<?php

namespace App;

use App\Facades\SubdomainEnvironment;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;

class Subdomain extends Model
{
    use LocalizedEloquentDates;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupOrders()
    {
        return $this->hasMany(GroupOrders::class);
    }

    /**
     * Find current subdomain
     *
     * @return Subdomain
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function findCurrent()
    {
        return static::where('name', SubdomainEnvironment::currentSubdomain())->first();
    }

    /**
     * Find current subdomain ID
     *
     * @return Subdomain
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function findCurrentId()
    {
        return static::findId(static::findCurrent());
    }

    /**
     * Find subdomain ID
     *
     * @return Subdomain
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function findId(\App\Subdomain $subdomain = null)
    {
        return is_null($subdomain) ? null : $subdomain->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    /**
     * Client types of this subdomain
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientTypes()
    {
        return $this->hasMany(ClientTypes::class);
    }
}
