<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Classes\SubdomainEnvironment\SubdomainEnvironment
 */
class SubdomainEnvironment extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'subdomain-environment';
    }
}
