<?php

namespace App\Jobs;

use App\Emails\SendInvoiceNotification;
use App\ReadyUploadProduct;
use App\Classes\Parser\Frx;
use App\Classes\Parser\RubyParser;
use App\Services\FrxToPdfConvertor;
use App\Services\Slack;
use App\Template;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Queue\SerializesModels;
use App\Emails\LayoutApprovalCustomer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class ParseDesignerPsd extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var ReadyUploadProduct
     */
    protected $product;

    /**
     * Create a new job instance.
     *
     * @param ReadyUploadProduct $product
     */
    public function __construct(ReadyUploadProduct $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Log $log)
    {
        if ($this->product->isParsed() || $this->product->mime_type !== 'image/vnd.adobe.photoshop') {
            return false;
        }

        try {
            $this->product->update(['status_code' => ReadyUploadProduct::PARSE_IN_PROCESS]);

            $psdPath = public_path($this->product->path);

            $frxPath = RubyParser::parse($psdPath);

            if (!$frxPath) {
                $this->product->update([
                    'status_code' => ReadyUploadProduct::PARSE_ERROR,
                ]);

                return false;
            }

            // Перенести файлы в папку с распарсенными шаблонами
            $frxDestFile = $this->moveParsed($frxPath);

            $this->product->update([
                'status_code' => ReadyUploadProduct::PARSE_COMPLETED,
                'frx_path' => $frxDestFile,
            ]);

            $order = $this->product->order;
            $element = $this->product->orderElement;

            if ($element->templates()->nonParsed()->count() === 0) {

                $parsed = $element
                    ->templates()
                    ->parsed()->get()
                    ->pluck('frx_path')->toArray();

                $mergedFrx = Frx::mergeMultiplyFiles($parsed);

                $pdf = FrxToPdfConvertor::convert($mergedFrx);
                rmkdir(public_path('upload/elements/pdf_templates'));
                if (file_exists($pdf)) {
                    copy($pdf, public_path('upload/elements/pdf_templates/'.basename($pdf)));
                }

                // assign with element
                $element->template = $mergedFrx;
                $element->pdf_path = '/upload/elements/pdf_templates/'.basename($pdf);
                $element->save();
            }

            $complect = $order->complectOrder->complect;

            foreach ($complect as $order) {
                // Если распарсены все шаблоны всех элементов
                if ($order->readyProducts()->nonParsed()->count() === 0) {

                    // Merge all frx to one
                    $files = $order->elements->pluck('template')
                        ->filter(function ($file) {
                            return $file;
                        })
                        ->toArray();

                    $files = array_values($files);

                    if (count($files) === 0) {
                        throw new \Exception('No template files');
                    }

                    $mainFrx = Frx::mergeMultiplyFiles($files);

                    $destFrxInPublic = public_path('lib/frx/' . basename($mainFrx));

                    Frx::move($mainFrx, $destFrxInPublic);

                    // Get preview
                    $sortingOrder = ['cover', 'body', 'pad'];
                    $sortedElements = $order->elements()->printable()->get()->sortBy(function ($element) use ($sortingOrder) {
                        return array_search($element->type, $sortingOrder);
                    });
                    $preview = $sortedElements->first()->preview;
                    $order->file = basename($destFrxInPublic);
                    $order->img = $preview;
                    $order->save();

                    // Создать пользовательский макет в архив
                    $templateSize = explode('x', $order->price_details['selected_size']);
                    $template = $order->client->templates()->create([
                        'parent_id' => null,
                        'w2p' => $order->file,
                        'psd' => null,
                        'url' => $order->img,
                        'width' => $templateSize[0],
                        'height' => $templateSize[1],
                        'editable' => true,
                        'folds' => isset($order->price_details['folds']) ? $order->price_details['folds'] : 0,
                        'two_side' => false,
                    ]);

                    $order->template_id = $template->id;
                    $order->save();

                    $this->orderTemplateParsingComplete($order);
                }
            }
        } catch (\Exception $exception) {
            $this->product->exception = $exception->getMessage();
            $this->product->save();
            throw $exception;
        }
    }

    /**
     * Move parsed files to the new location
     *
     * @param $frxPath
     * @return string
     */
    protected function moveParsed($frxPath)
    {
        $templatesFolder = storage_path('app/templates');

        if (!is_dir($templatesFolder)) {
            mkdir($templatesFolder, 0777, true);
        }

        $pathinfo = pathinfo($frxPath);
        $frxDestFile = $templatesFolder . '/' . $pathinfo['basename'];
        copy($frxPath, $frxDestFile);
        $frxSrcDir = $pathinfo['dirname'] . '/' . $pathinfo['filename'];
        $frxDestDir = $templatesFolder . '/' . $pathinfo['filename'];

        if (is_dir($frxSrcDir)) {
            if (!is_dir($frxDestDir)) {
                mkdir($frxDestDir);
            }
            rcopy($frxSrcDir, $frxDestDir);
        }
        rrmdir($frxSrcDir);
        unlink($frxPath);
        return $frxDestFile;
    }

    private function orderTemplateParsingComplete($order)
    {
        // Заказ вместе с дизайнером
        if ($order->status == status('parsing', 'id')) {
            $order->status = status('layout_approved', 'id');
            $order->save();
            (new SendInvoiceNotification)->send($order);
        }

        // Заказ по ТЗ дизайнеру
        if ($order->status == status('prepares_agreement', 'id')) {
            $order->status = status('sent_for_approval', 'id');
            $order->save();

            // Отправить письмо клиенту со ссылкой для утверждения макета
            app(LayoutApprovalCustomer::class)->send($order);

            $groupOrder = $order->group;
            $groupOrder->invoice_send = 1;
            $groupOrder->save();
        }
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        $this->product->update(['status_code' => ReadyUploadProduct::PARSE_ERROR]);
        $order = $this->product->order;
        $order->status = status('error', 'id');
        $order->save();

        slack('Error in queue while parsing product: ' . $this->product->id);
    }
}
