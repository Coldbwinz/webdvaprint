<?php

namespace App\Jobs;

use Log;
use Exception;
use App\Jobs\Job;
use App\ParserFiles;
use App\Classes\Parser\Frx;
use App\Classes\Parser\RubyParser;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class ParsePsd extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var ParserFiles
     */
    private $file;

    /**
     * Create a new job instance.
     *
     * @param ParserFiles $file
     */
    public function __construct(ParserFiles $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        if ($this->file->status !== 'new') {
            return false;
        }

        $this->file->update(['status' => 'parse']);

        $result = RubyParser::parse($this->file->path);

        if (! $result) {
            $this->file->update([
                'status' => 'error',
                'errors' => "[" . date('d.m.Y H:i:s') . "] Ruby parser error.",
            ]);

            return false;
        }

        if ($uploadFolder = $this->moveParsedFiles($this->file)) {
            $this->file->update([
                'status' => 'move',
                'upload_folder' => $uploadFolder,
            ]);

            return true;
        }

        $this->file->update([
            'status' => 'error',
            'errors' => "[" . date('d.m.Y H:i:s') . "] Error moving file: {$this->file->path}. Find information in logs.",
        ]);
    }

    /**
     * Move parsed files
     *
     * @param ParserFiles $file
     * @return string
     */
    protected function moveParsedFiles(ParserFiles $file)
    {
        $info = pathinfo($file->path);
        $path = $info['dirname'];
        $fileName = $info['filename'];

        // create upload folder
        $subFolderName = md5($fileName.time().rand(0,1000));
        $uploadFolder = Frx::getLibraryPath().'/'.$subFolderName;
        if (!file_exists($uploadFolder)) {
            Log::debug("Folder {$uploadFolder} not exists. Creating...");
            mkdir($uploadFolder, 0777, true);
        }

        // move psd folder
        $folder = $path.'/'.$fileName;
        if (file_exists($folder)) {
            $newFolder = $uploadFolder.'/files';
            Log::debug("Copy {$folder} to {$newFolder}.");
            rcopy($folder, $newFolder);
            try {
                rrmdir($folder);
            } catch (Exception $e) {
                Log::error("Can't remove folder {$folder}.", [$e]);
            }
        }

        // move frx
        $frx = $path.'/'.$fileName.'.frx';
        if (file_exists($frx)) {
            $newFile = $uploadFolder.'/'.$fileName.'.frx';

            Log::debug("Copy FRX file from {$frx} to {$newFile}");

            copy($frx, $newFile);
            try {
                unlink($frx);
            } catch (Exception $e) {
                Log::error("Can't remove frx file {$frx}", [$e]);
            }
        } else {
            Log::error("File {$frx} not exists.");
            return null;
        }

        // move png
        $png = $path.'/'.$fileName.'.png';
        if (file_exists($png)) {
            $newFile = $uploadFolder.'/'.$fileName.'.png';
            copy($png, $newFile);

            // make preview
            $small = $uploadFolder.'/'.$fileName.'_small.png';
            image_resize($png, $small, 250, 250, 70);

            // move preview
            $previewFolder = public_path('lib/psd_preview/'.$subFolderName);
            if (!file_exists($previewFolder)) {
                mkdir($previewFolder, 0777, true);
            }
            $preview = $previewFolder.'/'.$fileName.'_small.png';
            copy ($small, $preview);

            try {
                unlink($png);
            } catch (Exception $e) {
                Log::error("Can't remove pmg file {$png}", [$e]);
            }

            //save file
            $file->preview = '/lib/psd_preview/'.$subFolderName.'/'.$fileName.'_small.png';
        } else {
            Log::error("File {$png} not exists.");
            return null;
        }

        return $uploadFolder;
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        $this->file->update([
            'status' => 'error',
            'errors' => "[" . date('d.m.Y H:i:s') . "] Parsing error",
        ]);
    }
}
