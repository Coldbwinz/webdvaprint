<?php

namespace App\Jobs;

use App\Classes\Parser\Frx;
use App\Classes\Parser\RubyParser;
use App\Jobs\Job;
use App\TemplateLibrary;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class ParseLibraryTemplate extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var TemplateLibrary
     */
    protected $templateLibrary;

    /**
     * Create a new job instance.
     *
     * @param TemplateLibrary $templateLibrary
     */
    public function __construct(TemplateLibrary $templateLibrary)
    {
        $this->templateLibrary = $templateLibrary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->templateLibrary->status !== 'new') {
            return false;
        }

        $this->templateLibrary->update([
            'status' => 'parsing',
        ]);

        if ($frx = RubyParser::parse($this->templateLibrary->psd)) {

            $pathinfo = pathinfo($frx);

            $previewBasename = $pathinfo['filename'].'.png';

            // move preview to the public folder
            copy($pathinfo['dirname'].'/'.$previewBasename, public_path('lib/psd_preview/'.$previewBasename));
            unlink($pathinfo['dirname'].'/'.$previewBasename);

            // move frx files
            $newFrxName = str_random().'.frx';
            Frx::move($frx, public_path('lib/frx/'.$newFrxName));

            if (is_dir(pathinfo($frx, PATHINFO_DIRNAME).'/'.pathinfo($frx, PATHINFO_FILENAME))) {
                rrmdir(pathinfo($frx, PATHINFO_DIRNAME).'/'.pathinfo($frx, PATHINFO_FILENAME));
            }
            unlink($frx);

            $size = Frx::getSize(public_path('lib/frx/'.$newFrxName));

            $this->templateLibrary->update([
                'w2p' => public_path('lib/frx/'.$newFrxName),
                'url' => '/lib/psd_preview/'.$previewBasename,
                'width' => 94, //$size['width'],
                'height' => 54, //$size['height'],
                'ratio' => 1.74074, //$size['width'] / $size['height'],
                'status' => 'ready',
            ]);

            return true;
        }

        $this->templateLibrary->update([
            'status' => 'error',
        ]);
    }
}
