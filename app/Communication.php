<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'value'
    ];

    /**
     * Client of the communication
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Clients::class);
    }

    /**
     * Type of the communication
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(CommunicationType::class);
    }
}
