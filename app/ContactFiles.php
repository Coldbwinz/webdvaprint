<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactFiles extends Model
{
    protected $table = 'contact_files';
}
