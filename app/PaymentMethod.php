<?php

namespace App;

class PaymentMethod
{
    const PAYMENT_METHOD_CASH = 1; // В кассу
    const PAYMENT_METHOD_ACCOUNT = 2; // На счёт
    const PAYMENT_METHOD_EMONEY = 3; // e-money

    /**
     * Get payment method name
     *
     * @param $paymentMethod
     * @return string
     * @throws \Exception
     */
    public static function getPaymentName($paymentMethod)
    {
        if ($paymentMethod == 1) {
            return 'В кассу';
        } elseif ($paymentMethod == 2) {
            return 'На счёт';
        } elseif ($paymentMethod == 3) {
            return 'E-money';
        }

        \Log::error('Unknown payment method id: ' . $paymentMethod);
    }
}
