<?php

namespace App;

use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ClientContactInformation;

class Contacts extends Model
{
    use ClientContactInformation, LocalizedEloquentDates;

    /**
     * @var string
     */
    protected $table = 'contacts';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'last_name', 'email', 'phone', 'description', 'avatar', 'position', 'is_company',
        'id_1s',
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Clients::class, 'contacts_clients_relation', 'contact_id', 'client_id');
    }

    /**
     * User of this contact
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRelationContacts()
    {
        $client_id = ContactsClientsRelation::find($this->id)->client_id;
        $relation_contact_ids = ContactsClientsRelation::where('client_id', $client_id)->get();
        $contacts = [];
        foreach ($relation_contact_ids as $relation_contact_id) {
            array_push($contacts, Clients::find($relation_contact_id->client_id));
        }
        return (object) $contacts;
    }

    /**
     * @param $value
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        if (! $value) {
            return asset('images/nophoto.png');
        }

        return $value;
    }
}
