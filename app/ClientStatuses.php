<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientStatuses extends Model
{
    protected $table = 'client_statuses';
}
