<?php

namespace App;

use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;

class Intermediary extends Model
{
    use LocalizedEloquentDates;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['subdomain'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Clients::class);
    }

    /**
     * Get user, associated with this intermediary
     *
     * @return mixed
     */
    public function user()
    {
        return $this->contact()->getResults()->belongsTo(User::class);
    }
}
