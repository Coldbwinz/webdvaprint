<?php
/*
 * Helper functions
 */

/**
 * Make preview from PNG
 *
 * @param string $infile
 * @param string $outfile
 * @param int $w
 * @param int $h
 * @param int $quality
 */
if (! function_exists('make_png_preview'))
{
    function make_png_preview($infile, $outfile, $w, $h, $quality = 9)
    {
        if (file_exists($infile)) {
            $img = imagecreatefrompng($infile);
            $width = imagesx($img);
            $height = imagesy($img);
            if ($width > $height) {
                $scaling = $w / $width;
                $h = floor($height * $scaling);
            }
            else {
                $scaling = $h / $height;
                $w = floor($width * $scaling);
            }
            //create
            $newImg = imagecreatetruecolor($w, $h);
            //alpha
            $black = imagecolorallocate($newImg, 0, 0, 0);
            imagecolortransparent($newImg, $black);
            imagealphablending($newImg, false);
            //resize
            imagecopyresampled($newImg, $img, 0, 0, 0, 0, $w, $h, $width, $height);
            imagepng($newImg, $outfile, $quality);
            //destroy
            imagedestroy($img);
            imagedestroy($newImg);
        }
    }
}

/**
 * Resize image
 *
 * @param string $src
 * @param string $dst
 * @param int $width
 * @param int $height
 * @return void
 */
if (! function_exists('image_resize'))
{
    function image_resize($src, $dst, $width, $height, $quality = 90)
    {
        $imageClass = app('Image');
        $img = $imageClass::make($src);
        $img->resize($width, $height, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($dst, $quality);
    }
}

/**
 * Remove directory with files, recursive
 *
 * @param str $dir
 * @return void
 */
if (!function_exists('rrmdir'))
{
    function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file)
            if ($file != "." && $file != "..") rrmdir("$dir/$file");
            rmdir($dir);
        }
        else if (file_exists($dir)) unlink($dir);
    }
}

/**
 * Copy directory with files, recursice
 *
 * @param str $src
 * @param str $dst
 * @return void
 */
if (!function_exists('rcopy'))
{
    function rcopy($src, $dst)
    {
        if (file_exists($dst)) rrmdir($dst);
        if (is_dir($src)) {
            mkdir($dst);
            $files = scandir($src);
            foreach ($files as $file)
            if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file");
        }
        else if (file_exists($src)) copy($src, $dst);
    }
}
/**
 * Recursive mkdir
 *
 * @param string $dir
 * @param integer $mode
 */
if (! function_exists('rmkdir'))
{
    function rmkdir($dir, $mode = 0777)
    {
        if (! is_dir($dir)) {
            mkdir($dir, $mode, true);
        }
    }
}

if (! function_exists('recursive_copy_flatten')) {
    /**
     * Recursive copy all the files in a folder to another folder without structure.
     * Files with the same names are overwritten.
     * Destination path created automatically, if it not exists.
     *
     * @param string $source absolute path to source folder
     * @param string $destination absolute path to destination folder
     * @return array copied files
     */
    function recursive_copy_flatten($source, $destination)
    {
        $files = [];
        $directory = new \RecursiveDirectoryIterator($source);
        $iterator = new \RecursiveIteratorIterator($directory);

        if (! file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        foreach ($iterator as $file) {
            if (! $file->isFile()) {
                continue;
            }

            copy($file->getPathname(), $destination.'/'.$file->getFilename());
            $files[] = $destination.'/'.$file->getFilename();
        }

        return $files;
    }
}

if (! file_exists('mime_to_extension')) {
    /**
     * Get extension by mime type
     *
     * @param string $mime
     * @return string|null
     */
    function mime_to_extension($mime)
    {
        $map = [
            'image/png' => 'png',
            'image/jpg' => 'jpg',
            'image/bmp' => 'bmp',
            'image/jpeg' => 'jpg',
        ];

        return isset($map[$mime]) ? $map[$mime] : null;
    }
}

if (!function_exists('glue_url')) {
    /**
     * @link http://php.net/manual/en/function.parse-url.php#77053
     * @param $parsed
     * @return bool|string
     */
    function glue_url($parsed)
    {
        if (! is_array($parsed)) return false;
        $uri = isset($parsed['scheme']) ? $parsed['scheme'].':'.((strtolower($parsed['scheme']) == 'mailto') ? '':'//'): '';
        $uri .= isset($parsed['user']) ? $parsed['user'].($parsed['pass']? ':'.$parsed['pass']:'').'@':'';
        $uri .= isset($parsed['host']) ? $parsed['host'] : '';
        $uri .= isset($parsed['port']) ? ':'.$parsed['port'] : '';
        if(isset($parsed['path']))
        {
            $uri .= (substr($parsed['path'],0,1) == '/')?$parsed['path']:'/'.$parsed['path'];
        }
        $uri .= isset($parsed['query']) ? '?'.$parsed['query'] : '';
        $uri .= isset($parsed['fragment']) ? '#'.$parsed['fragment'] : '';
        return $uri;
    }
}

if (! function_exists('replaceSubdomain')) {
    /**
     * Replace third level subdomain
     *
     * @param $url
     * @param $newSubdomain
     * @return bool|string
     */
    function replaceSubdomain($url, $newSubdomain)
    {
        $parsed = parse_url($url);

        $exploded = explode('.', $parsed['host']);
        if (count($exploded) === 3) {
            $exploded[0] = $newSubdomain;
            $parsed['host'] = implode('.', $exploded);
        }

        if (count($exploded) < 3) {
            array_unshift($exploded, $newSubdomain);
            $parsed['host'] = implode('.', $exploded);
        }

        return glue_url($parsed);
    }
}

if (! function_exists('settings')) {
    /**
     * Get / set the specified configuration value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function settings($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('subdomain-settings');
        }

        if (is_array($key)) {
            return app('subdomain-settings')->set($key);
        }

        return app('subdomain-settings')->get($key, $default);
    }
}

if (! function_exists('subdomain')) {
    /**
     * Get current subdomain from request
     *
     * @return string
     */
    function subdomain()
    {
        return app('subdomain-environment')->currentSubdomain();
    }
}

if (! function_exists('addLog')) {
    /**
     * Функция добавления записи в логи заказов
     *
     * @param int $order_id
     * @param int $status
     * @param int $client_id
     * @param string $value
     * @param null $groupOrderId
     * @param int $payed
     * @param string $comment
     * @return bool
     */
    function addLog($order_id, $status, $user_id = null, $value = '', $groupOrderId = null, $payed = 0, $comment = '')
    {
        // Делаем запись в лог заказов
        $logs = new \App\Order_logs();
        $logs->order_id = $order_id;
        if (! is_numeric($status)) {
            $logs->status_free = $status;
            $status = \App\Orders::find($order_id)->status;
        }

        $user_id = $user_id ?: auth()->user()->id;

        $logs->status = $status;
        $logs->user_id = $user_id;
        $logs->value = $value;
        $logs->group_order_id = $groupOrderId;
        $logs->pay = $payed;
        $logs->comments = $comment;
        return $logs->save();
    }
}

if (! function_exists('morph')) {
    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }
}

if (! function_exists('num2str')) {
    /**
     * перевод числа в строку
     *
     * @param int $num
     * @return string
     */

    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array(
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );

        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) {
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1;
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            }
        } else $out[] = $nul;
        $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]);
        $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]);
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }
}

if (! function_exists('status')) {
    /**
     * Get status helper
     *
     * status('layout_approved'),
     * status('layout_approved', 'name'),
     * status(['layout_approved', 'shown_for_client']),
     * status(['layout_approved', 'shown_for_client'], 'name')
     *
     * @param string|array $key
     * @param string $field
     * @return OrderStatuses|Collection|string|null
     */
    function status($key, $field = null)
    {
        if ($field) {
            return app('order-statuses')->fetchFieldByKey($key, $field);
        }

        return app('order-statuses')->findByKey($key);
    }
}

if (! function_exists('mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @return HtmlString
     *
     * @throws \Exception
     */
    function mix($path, $manifestDirectory = '')
    {
        static $manifest;

        if (! starts_with($path, '/')) {
            $path = "/{$path}";
        }

        if ($manifestDirectory && ! starts_with($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }

        if (file_exists(public_path($manifestDirectory.'/hot'))) {
            return new HtmlString("http://localhost:8080{$path}");
        }

        if (! $manifest) {
            if (! file_exists($manifestPath = public_path($manifestDirectory.'/mix-manifest.json'))) {
                throw new Exception('The Mix manifest does not exist.');
            }

            $manifest = json_decode(file_get_contents($manifestPath), true);
        }

        if (! array_key_exists($path, $manifest)) {
            throw new Exception(
                "Unable to locate Mix file: {$path}. Please check your ".
                'webpack.mix.js output paths and try again.'
            );
        }

        return new \App\Helpers\HtmlString($manifestDirectory.$manifest[$path]);
    }
}

/**
 * Sends message to the 2up's #dev channel
 *
 * @param $message
 */
function slack($message)
{
    return \App\Services\Slack::notification($message);
}