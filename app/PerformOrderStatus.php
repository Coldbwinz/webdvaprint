<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerformOrderStatus extends Model
{
    const PREPARING_TO_PRINT = 1;
    const READY_TO_PRINT = 2;
    const SENT_TO_PRINT = 3;
    const PRINTED = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Perform orders with this status
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function performOrders()
    {
        return $this->hasMany(PerformOrder::class);
    }
}
