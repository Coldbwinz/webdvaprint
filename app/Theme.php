<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'activity', 'count'];

    /**
     * Tags, related to this theme
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->belongsToMany(ThemeTag::class);
    }

    /**
     * Templates, accociated with this theme
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function templates()
    {
        return $this->belongsToMany(TemplateLibrary::class);
    }

    /**
     * Scope active themes
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('activity', true);
    }
}
