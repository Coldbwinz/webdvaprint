<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    private $sentryID;

    public function report(Exception $e)
    {
        $debug = config('app.sentry', false);
        if ($debug) {
            
            if ($this->shouldReport($e)) {
                // bind the event ID for Feedback
                $this->sentryID = app('sentry')->captureException($e);
            }
            parent::report($e); 
            
        } else {
            return parent::report($e);
        }
    }

    // ...
    public function render($request, Exception $e)
    {
        $debug = config('app.sentry', false);
        
        if ($debug) {
            return response()->view('errors.500', [
                'sentryID' => $this->sentryID,
            ], 500);
        } else {
            if ($e instanceof ModelNotFoundException) {
                $e = new NotFoundHttpException($e->getMessage(), $e);
            }

            return parent::render($request, $e);
        }

    }
}
