<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractorOrderJob extends Model
{

    protected $table = 'contractor_order_jobs';

    protected $fillable = ['order_id', 'contractor_id', 'order_status_id', 'status_id', 'deadline', 'price'];

    protected $dates = ['deadline'];

    /**
     * Order join
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id');
    }

    /**
     * Contractor join
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractor()
    {
        return $this->belongsTo(Contractor::class, 'contractor_id');
    }

    /**
     * Status join
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(OrderStatuses::class, 'status_id');
    }

}
