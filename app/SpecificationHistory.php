<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationHistory extends Model
{

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'specification_history';

    /**
     * [$fillable description]
     * @var array
     */
    protected $fillable = ['specification_id', 'data', 'position'];

    /**
     * Specification parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specification()
    {
        return $this->belongsTo(Specification::class, 'specification_id');
    }

    public function latest($query)
    {
        return $query->order_by('created_at', 'desc');
    }

}
