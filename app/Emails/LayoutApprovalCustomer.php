<?php

namespace App\Emails;

use App\Orders;
use Illuminate\Contracts\Mail\Mailer;

class LayoutApprovalCustomer
{
    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Orders $order
     */
    public function send(Orders $order)
    {
        $groupOrder = $order->group;
        $client = $groupOrder->client;
        $user = $client->account;
        $manager = $client->manager;
        $companyName = settings('email_title', settings('company_name'));
        $data = [
            'subject' => 'Информация по заказу от ' . $companyName,
            'to' => $client->email,
            'name' => $client->name,
            'group_order' => $groupOrder,
            'manager_contacts' => $manager->contact ? $manager->contact : $manager,
            'login_token' => $user->login_token,
            'userOrdersCount' => 0,
            'libOrdersCount'  => 1,
            'client' => $client,
            'agree_template_id' => $order->id,
            'choise_template_id' => null,
            'printler_id' => null,
            'designer_id' => null,
        ];

        $this->mailer->send('emails.group_only_invoice', $data, function ($message) use ($data) {
            $message->subject($data['subject'])
                ->to($data['to']);
        });
    }
}
