<?php
namespace App\Emails\Params;

use BadMethodCallException;

class MailMessageParams
{

    /**
     * Parameters storage
     *
     * @var array
     */
    private $params;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->params = $this->prepareParams();
    }

    /**
     * Get all parameters
     *
     * @return array
     */
    public function all()
    {
        return $this->params;
    }

    /**
     * Set parameter
     *
     * @param string $key
     * @param string $value
     * @return string
     */
    public function set($key, $value)
    {
        return $this->params[$key] = $value;
    }

    /**
     * Get parameter
     *
     * @param string $key
     * @return string
     */
    public function get($key)
    {
        return array_key_exists($key, $this->params) ? $this->params[$key] : null;
    }

    /**
     * Prepare params by default
     *
     * @return array
     */
    private function prepareParams()
    {
        return array_fill_keys(array_values($this->getAllowedKeys()), null);
    }

    /**
     * Get allowed keys for $this->params and magic methods
     *
     * @return array
     */
    private function getAllowedKeys()
    {
        return ['to', 'from', 'sender', 'cc', 'bcc', 'replyTo', 'subject', 'priority', 'name', 'data', 'view'];
    }

    /**
     * Check magic method name
     *
     * @param array $split
     * @return boolean
     */
    private function isMagicMethod($split)
    {
        return in_array($split[0], ['set', 'get'])
            && in_array($split[1], $this->getAllowedKeys());
    }

    /**
     * Split $method name to two-values array
     *  - first value is action
     *  - sectond value is parameter name
     *
     * @param string $method
     * @return array
     */
    private function splitMagic($method)
    {
        return [
            substr($method, 0, 3),
            strtolower(substr($method, 3, strlen($method) - 1))
        ];
    }

    /**
     * Handle dynamic calls into macros or pass missing methods to the store.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @throws \BadMethodCallException
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $split = $this->splitMagic($method);
        if ($this->isMagicMethod($split)) {
            if ($split[0] == 'get') {
                return $this->get($split[1]);
            }
            elseif ($split[0] == 'set') {
                if (isset($parameters[0]) &&
                        ($split[1] !== 'data' && (is_null($parameters[0]) || is_string($parameters[0]) || is_numeric($parameters))) ||
                        ($split[1] == 'data' && is_array($parameters[0]))
                    )
                    return $this->set($split[1], $parameters[0]);
                else
                    throw new \Exception("Invalid arguments in method [{$method}].");
            }
        }
        else {
            throw new BadMethodCallException("Method [{$method}] does not exist.");
        }
    }

}
