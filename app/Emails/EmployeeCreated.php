<?php

namespace App\Emails;

use App\User;
use Illuminate\Contracts\Mail\Mailer;

class EmployeeCreated
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param User $employee
     * @param string $password password of the user
     */
    public function send(User $employee, $password)
    {
        $this->mailer->send(
            'emails.new_employee_notification',
            compact('employee', 'password'),
            function ($message) use ($employee) {
                $message->to($employee->email)
                    ->subject('Создание аккаунта на сайте w2p.me');
            }
        );
    }
}
