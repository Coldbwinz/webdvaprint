<?php

namespace App\Emails;

use App\Orders;
use App\Events\ClientWasBilled;
use App\Http\Controllers\GroupOrdersController;

class SendInvoiceNotification
{

    /**
     * Отправить счет
     *
     * @param Orders $order
     * @return
     */
    public function send(Orders $order)
    {
        $client = $order->client()->first();
        $manager = $client->manager()->first();

        $group = $order->group;

        if (!in_array($group->status, [6, 7, 8])) {
            if ($group->orders()->whereIn('status', [3, 4, 5, 9, 300, 301, 2000, 2010])->count() == 0) {
                $pdf_link = substr((new GroupOrdersController)->getGroupOrderPDF($group->id, null), 1);
                $group->status = 1; // Выставлен счёт
            }
            (new GroupOrdersController)->recalculateGroupPrice($group->id);
            $group->save();
        }

        if ($group->status == 1) {
            event(new ClientWasBilled($client, $group));

            // Добавление "Выставлен счёт" в логи
            $company_name = settings('email_title') ? settings('email_title') : settings('company_name');
            $data = array(
                'subject' => 'Счет на оплату от ' . $company_name,
                'to' => $client->email,
                'email' => $manager->contact ? $manager->contact->email : $manager->email,
                'client' => $client,
                'url' => '',
                'pdf_link' => $pdf_link,
                'order_id' => $order->id,
                'manager' => $manager,
                'login_token' => $client->account->login_token,
            );

            $mail_send = \Mail::send('emails.new_invoice', $data, function ($message) use ($data) {
                $message->attach(public_path($data['pdf_link']), array('as' => 'invoice_' . $data['order_id'] . '.pdf', 'mime' => 'application/pdf'));
                $message->subject($data['subject'])
                    ->to($data['to'])
                    ->from($data['email'], url() . ' - Ваш счет');

            });

            if (count(\Mail::failures()) == 0) {
                addLog(0, 1, $group->manager->id, $client->email,
                    $group->id, 0, "Отправлен на почту заказчика");
                return $mail_send;
            }
        }
    }
}
