<?php
namespace App\Emails;

use App\Orders;
use App\User;
use App\Emails\SendEmail;

class SendLayoutEmail extends SendEmail
{

    protected $view;

    public function __construct($view)
    {
        $this->view = $view;
        parent::__construct();
    }

    protected function setParameters()
    {
        $this->parameters->setView($this->view);
    }

    /**
     * Send email with include template
     *
     * @param \App\Orders $user
     * @return Mailer
     */
     public function send(Orders $order)
     {
         $client = $order->client()->first();
         $manager = $client->manager()->first();
         $contact = $client->contacts()->first();

         $user = User::find($order->client->user_id);
         $company_name = settings('email_title') ? settings('email_title') : settings('company_name');

         $data = array(
             'subject' => 'Макет на согласование от ' . $company_name,
             'to' => $contact->email,
             'email' => $manager->contact ? $manager->contact->email : $manager->email,
             'name' => $order->client->name,
             'manager' => $manager,
             'login_token' => $user->login_token,
             'client' => $client,
             'order_id' => $order->id,
         );

         \Mail::send($this->parameters->getView(), $data, function ($message) use ($data) {
             $message->subject($data['subject'])
                 ->to($data['to'])
                 ->from($data['email'], $_SERVER["SERVER_NAME"] . ' - Ваш макет');
         });
     }

}
