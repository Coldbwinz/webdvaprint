<?php
namespace App\Emails;

use Illuminate\Support\Facades\Mail;
use App\Emails\Params\MailMessageParams;

abstract class SendEmail
{

    /**
     * Parameters storage
     *
     * @var App\Emails\Params\MailMessageParams
     */
    protected $parameters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parameters = new MailMessageParams;
        $this->setParameters();
    }

    abstract protected function setParameters();

    /**
     * Handle dynamic calls into macros or pass missing methods to the store.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->parameters, $method], $arguments);
    }


}
