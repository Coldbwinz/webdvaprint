<?php

namespace App\Emails;

use Mail;
use App\Contractor;
use App\Orders;
use App\ContractorToken;

class ContractorJobEmail
{

    /**
     * Send email to contractor
     *
     * @param  Contractor      $contractor
     * @param  Orders          $order
     * @param  ContractorToken $token
     * @return void
     */
    public function send(Contractor $contractor, Orders $order, ContractorToken $token)
    {
        $user = auth()->user();
        $contact = $contractor->contacts()->first();

        $email = !is_null($contact) ? $contact->email : $contractor->email;
        $from = $user->contact ? $user->contact->email : $user->email;

        $manager = auth()->user();

        $data = compact('contractor', 'order', 'manager', 'token');

        Mail::send('emails.contractor.send_layout', $data, function ($message) use ($email, $from) {
            $message
                ->to($email)
                ->from($from)
                ->subject('Макет в работу');
        });
    }
}
