<?php
namespace App\Emails;

use App\User;
use App\PasswordReset;
use App\Emails\SendEmail;

class CreatePasswordEmail extends SendEmail
{

    protected function setParameters()
    {
        $this->parameters->setSubject('W2P Завершение регистрации');
        $this->parameters->setFrom('info@'.$_SERVER["SERVER_NAME"]);
    }

    /**
     * Send email with include template
     *
     * @param \App\User $user
     * @param $data
     * @return Mailer
     */
    public function send(User $user, $data)
    {
        // set email to send
        $this->parameters->setTo($user->email);

        return \Mail::send('emails.password.create', $data, function($message) {
            $message->subject( $this->parameters->getSubject() );
            $message->from( $this->parameters->getFrom() );
            $message->to( $this->parameters->getTo() );
        });
    }

}
