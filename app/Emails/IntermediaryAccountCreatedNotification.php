<?php

namespace App\Emails;

use App\Intermediary;

class IntermediaryAccountCreatedNotification
{
    /**
     * @var Intermediary
     */
    private $intermediary;
    private $password;

    /**
     * @param Intermediary $intermediary
     * @param $password
     */
    public function __construct(Intermediary $intermediary, $password)
    {
        $this->intermediary = $intermediary;
        $this->password = $password;
    }

    /**
     * Send email
     */
    public function send()
    {
        $intermediary = $this->intermediary;
        $link = replaceSubdomain(url('auth/login'), $this->intermediary->subdomain);
        $user = $this->intermediary->user;
        $password = $this->password;
        $company_name = settings('email_title') ? settings('email_title') : settings('company_name');

        return \Mail::send(
            'emails.intermediary.create',
            compact('intermediary', 'link', 'user', 'password'),
            function ($message) use ($user, $company_name) {
                $message->to($user->email);
                $message->subject($company_name . ": Создание аккаунта");
            }
        );
    }
}
