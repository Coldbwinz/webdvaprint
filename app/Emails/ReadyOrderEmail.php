<?php
namespace App\Emails;

use App\Orders;
use App\GroupOrders;
use App\PasswordReset;
use App\Emails\SendEmail;

class ReadyOrderEmail
{

    /**
     * Send email with include template
     *
     * @param \App\Orders $user
     * @return Mailer
     */
    public function send(GroupOrders $group_order)
    {
        $ordersInGroup = $group_order->orders;
        $order = $ordersInGroup->first();
        $readyOrders = Orders::with('productType')
            ->where('group_id', $order->group_id)
            ->where(function ($query) {
                $query->where('status', 51)->whereNotNull('control_quality');
            })
            ->get();

        $all_delivery = [];
        foreach ($readyOrders as $order) {
            $order->control_quality = null;
            $order->save();
            $delivery_type = $order->price_details['selected_delivery_type']['name'];
            $delivery_type = mb_strtolower(mb_substr($delivery_type, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($delivery_type, 1, null, 'UTF-8');
            if (!isset($all_delivery[$delivery_type])) {
                $all_delivery[$delivery_type] = [];
            }
            if (!isset($all_delivery[$delivery_type][$order->price_details['selected_address']])) {
                $all_delivery[$delivery_type][$order->price_details['selected_address']] = [];
            }
            $order_name = $order->getProductNameAttribute().' '.$order->price_details['selected_size'].' '.$order->draw;
            if (!isset($all_delivery[$delivery_type][$order->price_details['selected_address']][$order_name])) {
                $all_delivery[$delivery_type][$order->price_details['selected_address']][$order_name] = 0;
            }
            $all_delivery[$delivery_type][$order->price_details['selected_address']][$order_name]++;
        }

        $delivery_string = '<br>';
        $index = 0;
        if (count($all_delivery) > 0) {
            foreach ($all_delivery as $key1 => $delivery1) {
                if (count($delivery1) > 0) {
                    foreach ($delivery1 as $key2 => $delivery2) {
                        if (count($delivery2) > 0) {
                            foreach ($delivery2 as $key3 => $delivery3) {
                                $index++;
                                $delivery_string .= ((count($all_delivery) != 1) ? $index.'. ' : '').'Тип доставки: '.$key1.' адрес: '.$key2.' продукт('.$delivery3.' шт.): '.$key3.' шт.<br>';
                            }
                        }
                    }
                }
            }
        }

        $client = $group_order->client()->first();

        $full = ($readyOrders->count() == $ordersInGroup->count()) ? 'full' : 'part';
        $payment = ($group_order->money == 1) ? 'pay' : 'nopay';

        $products = '';
        $lastDate = '';
        
        if ($full == 'part') {
            $lastDate = date('d.m.Y', max($ordersInGroup->pluck('close_date')->all()));

            $productTypes = $readyOrders->pluck('draw', 'productType.name');
            $index = 0;
            foreach ($productTypes as $name => $draw) {
                if ($index++ > 0) {
                    $products .= ($index == $productTypes->count() - 1) ? " и " : ", ";
                }
                $products .= $name . ' - ' . $draw . ' шт';
            }
        }

        $debt = 0;
        if ($payment == 'nopay') {
            if ($group_order->price > $group_order->pay) {
                $debt = $group_order->price - $group_order->pay;
            }
        }
        
        $data = array(
            'subject' => 'Информация о вашем заказе',
            'to' => $client->email,
            'email' => \Auth::user()->contact ? \Auth::user()->contact->email : \Auth::user()->email,
            'manager' => $group_order->manager()->first(),
            'client' => $client,
            'address' => '',
            'products' => $products,
            'debt' => $debt,
            'last_date' => $lastDate,
            'group_order_pay' => $group_order->pay,
            'full' => $full,
            'payment' => $payment,
            'delivery_string' => $delivery_string,
        );

        return \Mail::send('emails.orders.ready_order', $data, function ($message) use ($data) {
            $message->subject($data['subject'])
                ->to($data['to'])
                ->from('sales@w2p.me', $_SERVER["SERVER_NAME"] . ' - Статус заказа');
        });
    }

}
