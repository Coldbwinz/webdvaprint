<?php

namespace App;

use App\Traits\SubdomainTrait;
use Illuminate\Database\Eloquent\Model;

class Urgency extends Model
{
    use SubdomainTrait;
    
    protected $table = 'urgency';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    /**
     * Стандартные типы клиентов для партнёров.
     *
     * @param int $subdomainId
     * @return array
     */
    public static function getDefaultUrgency($subdomainId)
    {
        $result = [];
        $urgency_main = Urgency::where('subdomain_id',null)->get();
        foreach ($urgency_main as $urgency) {
            array_push($result, ['subdomain_id' => $subdomainId, 'name' => $urgency->name, 'discount' => 0, 'name' => $urgency->name]);
        }
        return $result;
    }

    public static function issetTable() {
        return Urgency::where('discount', '!=', 0)->where('subdomain_id',Subdomain::findCurrentId())->count() > 0;
    }
}
