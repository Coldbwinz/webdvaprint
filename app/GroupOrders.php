<?php

namespace App;

use App\Traits\SubdomainTrait;
use App\Traits\GroupOrderSearchTrait;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;

class GroupOrders extends Model
{
    use LocalizedEloquentDates, SubdomainTrait, GroupOrderSearchTrait;

    /**
     * @var string
     */
    protected $table = 'group_orders';

    /**
     * Set the fillable attributes for the model.
     *
     * @param  array  $fillable
     * @return $this
     */
    protected $fillable = [
        'invoice_number', 'invoice_number_partner'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'partner_data' => 'object',
        'close_docs' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager() {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client() {
        return $this->belongsTo(Clients::class, 'client_id');
    }

    /**
     * Contact, who created this group order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contacts::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function details() {
        return $this->belongsTo(Details::class, 'detail_id');
    }

    public function partnerAsClient() {
        return $this->manager()->first()->client()->first();
    }

    /**
     * Orders og the group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'group_id');
    }

    public function issued_orders_count() {
        return $this->orders()->where('parent_id','=',null)->where('issued','>',0)->count();
    }

    public function send_to_issue_with_control_quality_count() {
        return $this->orders()->where('parent_id','=',null)->where('status','=',45)->whereNotNull('control_quality')->count();
    }

    /**
     * Заказы, отправленные на переделку
     *
     * @return mixed
     */
    public function revisionedOrders()
    {
        return $this->hasMany(Orders::class, 'group_id')
            ->whereNotNull('sent_revision_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Order_logs::class, 'group_order_id');
    }

    /**
     * Status of the group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statusInfo()
    {
        return $this->belongsTo(OrderStatuses::class, 'status');
    }

    /**
     * Статус партнёрского заказа
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statusPartner()
    {
        return $this->belongsTo(OrderStatuses::class, 'status_partner');
    }

    /**
     * Domain of the group order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    public function _price() {
        return ($this->subdomain) ? ((\Auth::user()->accessLevel() != 5) ? $this->price_partner : $this->price) : $this->price;
    }

    public function log($comment = '', $reason = '') {
        addLog(0, $this->status, \Auth::user()->id, $comment, $this->id, 0, $reason);
    }

    /**
     * Возвращает ссылку на pdf-счёт клиета
     *
     * @return string
     */
    public function getInvoiceLink()
    {
        $path = "orders_pdf/{$this->invoice_pdf}";

        if (is_file(public_path($path))) {
            return url($path);
        }

        return url('/404');
    }

    /**
     * Возвращает ссылку на pdf-счёт для партнёра от производства
     *
     * @return string
     */
    public function getPartnerInvoiceLink()
    {
        $path = "orders_pdf/{$this->invoice_partner_pdf}";

        if (is_file(public_path($path))) {
            return url($path);
        }

        return url('/404');
    }

    public function checkGroupOrdersForPrintlerDownloading() {
        $changed = null;
        foreach ($this->orders as $order) {
            $template_download = TemplateDownload::where('id_order', $order->id)->first();
            if (!empty($template_download)) {
                //TODO::Убрать для скачнивания макета с принтлера, раскомментировать для скачивания макета
                //$template_download->pdf_download = $this->money;
                //template_download->save();
                if (!$changed) {
                    $changed = $template_download;
                }
            }
        }
        if ($changed) {
            $changed->startDownloadTemplates();
        }
    }

    /**
     * Scope group orders with "in work" status
     *
     * @param $query
     * @return mixed
     */
    public function scopeInWork($query)
    {
        return $query->whereNotIn('status', [100]);
    }

    /**
     * Scope orders for client
     *
     * @param $query
     * @param Clients $client
     * @return mixed
     */
    public function scopeForClient($query, Clients $client)
    {
        return $query->where('client_id', $client->id);
    }

    /**
     * Scope orders for client
     *
     * @param $query
     * @param Contacts $contact
     * @return mixed
     */
    public function scopeForContact($query, Contacts $contact)
    {
        return $query->where('contact_id', $contact->id);
    }

    /**
     * Заказы, ожидающие оплаты для типографии
     *
     * @param $query
     * @return mixed
     */
    public function scopeTypographyWaitingPayment($query)
    {
        $query->where('status', '!=', status('invoiced', 'id'))
            ->where('money', 0)
            ->where(function ($query) {
                // Партнёрские заказы со статусом Ожидает оплаты
                $query->where(function ($q) {
                    $q->whereNotNull('subdomain_id')
                        ->whereIn('status', status([
                            'partially_paid', 'fully_paid', 'cash_on_delivery',
                        ], 'id'));
                })
                // Заказы от клиента со статусами Оплата при получении и Частично оплачен
                ->orWhere(function ($q) {
                    $q->whereNull('subdomain_id')
                        ->whereIn('status', status([
                            'partially_paid', 'cash_on_delivery',
                        ], 'id'));
                });
        });
    }

    /**
     * Заказы, ожидающие оплаты для партнёра
     *
     * @param $query
     */
    public function scopePartnerWaitingPayment($query)
    {
        $query->whereHas('subdomain', function ($q) {
            $q->where('name', subdomain());
        })
            ->whereIn('group_orders.status', status([
            'partially_paid', 'cash_on_delivery'
        ], 'id'));
    }

    /**
     * Check for group order is fully paid
     *
     * @return bool
     */
    public function isFullyPayed()
    {
        return $this->price == $this->pay;
    }
}
