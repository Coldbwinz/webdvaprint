<?php

namespace App;

use App\Traits\SubdomainTrait;
use Illuminate\Database\Eloquent\Model;

class Details extends Model
{

    use SubdomainTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'full_name', 'post_address', 'address', 'inn', 'kpp', 'ogrn', 'bank_rs', 'bank_name', 'bank_bik', 'bank_ks',
        'bank_inn', 'bank_kpp', 'director', 'director_position'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

}
