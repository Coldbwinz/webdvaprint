<?php

namespace App;

use App\Traits\SubdomainTrait;
use Illuminate\Database\Eloquent\Model;

class ClientTypes extends Model
{

    use SubdomainTrait;

    const NEW_СLIENT = 1;
    const REGULAR = 2;
    const INTERMEDIARY = 3;
    const PARTNER = 4;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'discount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    /**
     * Стандартные типы клиентов для партнёров.
     *
     * @param int $subdomainId
     * @return array
     */
    public static function getDefaultTypes($subdomainId)
    {
        return [
            [
                'name' => 'Новый клиент',
                'discount' => 0,
                'subdomain_id' => $subdomainId,
            ],
            [
                'name' => 'Постоянный клиент',
                'discount' => 0,
                'subdomain_id' => $subdomainId,
            ],
            [
                'name' => 'Посредник',
                'discount' => 0,
                'subdomain_id' => $subdomainId,
            ]
        ];
    }

    public static function getPartnerDiscount() {
        return ClientTypes::find(ClientTypes::PARTNER)->discount;
    }

    public static function issetTable() {
        return ClientTypes::where('discount', '!=', 0)->where('subdomain_id',Subdomain::findCurrentId())->count() > 0;
    }
}
