<?php

namespace App;

use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;

class Contractor extends Model
{
    use SoftDeletes, LocalizedEloquentDates;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['name', 'last_name', 'is_company', 'company_name', 'subdomain_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Contacts of the contractor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts()
    {
        return $this->belongsToMany(Contacts::class, 'contact_contractor', 'contractor_id', 'contact_id');
    }

    /**
     * Contractor jobs join
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function jobs()
    {
        return $this->hasMany(ContractorOrderJob::class, 'contractor_id');
    }

    /**
     * Job orders for this contractor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jobOrders()
    {
        return $this->belongsToMany(Orders::class, 'contractor_order_jobs', 'contractor_id', 'order_id');
    }

    public function fullName()
    {
        return $this->name.' '.$this->last_name;
    }

    public function displayedName()
    {

        return $this->is_company ? $this->company_name : $this->fullName();
    }
    
    public function sendStopped($order) {
        $this->sendMessage($order, 'contractor.stopped', 'Приостановка заказа');
    }

    public function sendStart($order) {
        $this->sendMessage($order, 'contractor.start', 'Возобновление заказа');
    }

    public function sendMessage($order, $template, $title) {
        if ($order->with_contractor) {
            $user = auth()->user();
            $contractor = $order->contractor()->first();
            $contact = $contractor->contacts()->first();
            $manager = $order->manager();
            $token = $order->token()->first();
            $email = !is_null($contact) ? $contact->email : $contractor->email;
            $from = $order->manager ? $order->manager->email : $user->email;

            $data = compact('contractor', 'order', 'manager', 'token');
            Mail::send('emails.' . $template, $data, function ($message) use ($email, $from, $title) {
                $message->to($email)->from($from)->subject($title);
            });
        }
    }
}
