<?php

namespace App\Classes\FilenameSplitter;

class Splitter
{
    /**
     * @var array
     */
    private $templates;

    /**
     * @param array $templates
     */
    public function __construct(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param $filename
     * @param string $suffix
     * @return array
     */
    public function split($filename, $suffix = '.psd')
    {
        $basename = basename($filename, $suffix);

        foreach ($this->templates as $regexp => $extractor) {
            if (preg_match($regexp, $basename)) {
                list($vendor, $number, $type, $side) = array_values(
                    $extractor($basename, $filename, $regexp)
                );

                return [
                    'vendor' => mb_strtolower($vendor),
                    'number' => $number,
                    'type' => mb_strtolower($type),
                    'side' => mb_strtolower($side),
                ];
            }
        }

        $this->exceptionIfNotSplitted($filename);
    }

    /**
     * @param string $filename
     * @throws FilenameSplitterException
     */
    protected function exceptionIfNotSplitted($filename)
    {
        throw new FilenameSplitterException("Can not split this filename \"{$filename}\"");
    }
}
