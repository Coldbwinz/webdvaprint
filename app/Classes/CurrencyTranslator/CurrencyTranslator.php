<?php

namespace App\Classes\CurrencyTranslator;

interface CurrencyTranslator
{
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_RUB = 'RUB';

    /**
     * Конвертирует сумму с указанной валютой в рубли
     *
     * @param integer $value сумма для конвертирования
     * @param string $currency валюта суммы
     * @return integer сумма в рублях
     */
    public function convertToRub($value, $currency);
}
