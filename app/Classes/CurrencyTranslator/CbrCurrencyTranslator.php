<?php

namespace App\Classes\CurrencyTranslator;
use Carbon\Carbon;
use Illuminate\Contracts\Cache\Repository;

/**
 * Class CbrCurrencyTranslator
 * @package App\Classes\CurrencyTranslator
 */
class CbrCurrencyTranslator implements CurrencyTranslator
{
    /**
     * @var Repository
     */
    protected $cache;

    /**
     * @var Carbon
     */
    protected $carbon;

    /**
     * CbrCurrencyTranslator constructor.
     * @param Repository $cache
     * @param Carbon $carbon
     */
    public function __construct(Repository $cache, Carbon $carbon)
    {
        $this->cache = $cache;
        $this->carbon = $carbon;
    }

    /**
     * @var string
     */
    protected $serviceUrl = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * Load currencies from http://www.cbr.ru
     *
     * @return \SimpleXMLElement
     */
    protected function loadCurrencies()
    {
        $date = $this->carbon->now()->format('d.m.Y');

        $content = $this->cache->remember('currencies_'.$date, 60, function () {
            return file_get_contents($this->serviceUrl);
        });

        return simplexml_load_string($content);
    }

    /**
     * Конвертирует сумму с указанной валютой в рубли
     *
     * @param integer $value сумма для конвертирования
     * @param string $currency валюта суммы
     * @return int сумма в рублях
     * @throws \Exception
     */
    public function convertToRub($value, $currency)
    {
        $currencies = $this->loadCurrencies();

        foreach ($currencies as $item) {
            if (strtoupper($item->CharCode) === strtoupper($currency)) {
                return $item->Value * $value;
            }
        }

        throw new \Exception('Cannot convert value to the currency.');
    }
}
