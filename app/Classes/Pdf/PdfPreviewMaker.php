<?php

namespace App\Classes\Pdf;

class PdfPreviewMaker
{
    public static function make($source, $target)
    {
        $im = new \Imagick($source."[0]"); // 0-first page, 1-second page
        $im->setCompression(\Imagick::COMPRESSION_JPEG);
        $im->setCompressionQuality(100);
        $im->setimageformat("jpeg");
        $im->writeimage($target);
        $im->clear();
        $im->destroy();
    }
}
