<?php

namespace App\Classes\Archiver;

use Exception;
use ZipArchive;

class ZipArchiver implements ArchiverContract
{
    /**
     * @var \ZipArchive
     */
    protected $zip;

    /**
     * @var string
     */
    protected $zipArchiveFilename;

    /**
     * Create new archive
     *
     * @param string $filename path to archive file
     *
     * @return $this
     * @throws \Exception
     */
    public function create($filename)
    {
        $this->zipArchiveFilename = $filename;

        $this->zip = new ZipArchive();

        if ($this->zip->open($this->zipArchiveFilename, ZipArchive::CREATE) !== true) {
            throw new Exception("Cannot create zip {$this->zipArchiveFilename}");
        }

        return $this;
    }

    /**
     * Add single file to the archive
     *
     * @param string $file path to the file to add
     * @param string $localname custom file name in archive
     *
     * @return $this
     * @throws Exception
     */
    public function addFile($file, $localname = null)
    {
        if (! is_file($file)) {
            throw new Exception("File {$file} does not exists.");
        }

        $this->zip->addFile($file, $localname);

        return $this;
    }

    /**
     * Save archive and close it
     *
     * @return string path to created archive
     */
    public function save()
    {
        $this->zip->close();

        return $this->zipArchiveFilename;
    }
}
