<?php

namespace App\Classes\Archiver;

interface ArchiverContract
{
    /**
     * Create new archive
     *
     * @param string $filename path to archive file
     *
     * @return $this
     */
    public function create($filename);

    /**
     * Add single file to the archive
     *
     * @param string $file path to the file to add
     * @param string $localname custom file name in archive
     *
     * @return $this
     */
    public function addFile($file, $localname = null);

    /**
     * Save arhive and close it
     *
     * @return string path to created archive
     */
    public function save();
}
