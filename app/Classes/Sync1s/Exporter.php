<?php

namespace App\Classes\Sync1s;

use App\GroupOrders;
use App\ProductType;
use App\Subdomain;
use App\Urgency;
use Illuminate\Contracts\Filesystem\Filesystem;

class Exporter
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param \Illuminate\Contracts\Filesystem\Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Export group order data to file
     *
     * @param \App\GroupOrders $groupOrder
     * @return bool
     */
    public function export(GroupOrders $groupOrder)
    {
        if (!settings('1c_sync')) {
            return false;
        }

        $serverName = request()->server('SERVER_NAME');

        $filename = 'app/exports/' . $serverName . '/' . $this->getExportFileName($groupOrder);

        if (!is_dir(storage_path('app/exports/' . $serverName))) {
            mkdir(storage_path('app/exports/' . $serverName), 0777, true);
        }

        $groupOrder->load(['orders.productType', 'client']);

        $titles = [
            'ID заказа', 'Номер счёта', 'ID продукта', 'Дата заказа', 'Крайний срок',
            'ID детали', 'ID клиента', 'Реквизиты', 'ФИО Контакта', 'Телефон контакта',
            'email контакта', 'Наименование', 'Формат', 'Красочность',
            'Материал', 'Полосы', 'Тираж', 'Опции', 'Стоимость'
        ];

        $data[] = $titles;

        foreach ($groupOrder->orders as $order) {
            $price = $order->price_details;
            $productId = null;

            if ($order->productType) {
                $productId = $order->productType->product_1c_key;
            } elseif (isset($price['type_product'])) {
                try {
                    $productId = ProductType::findOrFail($price['type_product'])->product_1c_key;
                } catch (\Exception $exception) {
                    $productId = null;
                }
            }

            if (! $productId) {
                continue;
            }

            $productChromacity = null; // Цветность продукта
            $productFormat = $price['selected_size']; // Формат (размер) продукта
            $productMaterial = null; // Материал продукта
            $productOptions = ''; // Опции продукта

            if ($price['printler_type'] == 1 || $price['printler_type'] == 2) {
                $productChromacity = $price['selected_chromacity'];
                $productMaterial = $price['selected_product'];

                $options = [];

                if ($order->productType) {
                    $services = json_decode($order->productType->services);
                    $index = $price['selected_size'] . '_' . $price['selected_type'] . '_' . $price['selected_chromacity'] . '_' . $price['selected_material'];

                    if (isset($services->{$index})) {
                        $options = $services->{$index}->options;
                    }

                    foreach ($options as $option) {
                        if (in_array($option, $price['selected_options'])) {
                            $productOptions .= $option.':1;';
                        } else {
                            $productOptions .= $option.':0;';
                        }
                    }
                } else {
                    foreach ($price['selected_options'] as $option) {
                        $productOptions .= $option.':1;';
                    }
                }
            }

            // Название продукта
            $productName = $order->productTypeName();
            if ($price['selected_type'] !== '###') {
                $productName .= ' - ' . $price['selected_type'];
            }

            $requisites = '';
            if ($order->client->isCompany()) {
                $clientId = 'ИНН:'.$order->client->company_details['inn'].';КПП:'.$order->client->company_details['kpp'];
                if (is_array($order->client->company_details)) {
                    foreach ($order->client->company_details as $key => $value) {
                        if (in_array($key, ['all_ok', 'all_ok_info'])) {
                            continue;
                        }

                        $value = $value ?: 0;

                        $requisites .= $key.':'.$value.';';
                    }
                }
            } else {
                $clientId = $order->client->fullName;
            }

            // Главная строка
            $data[] = [
                'id' => $order->id,
                'invoice_number' => $groupOrder->invoice_number,
                'product_id' => $productId,
                'created_at' => $order->created_at->format('d.m.Y'),
                'close_date' => $price['close_date'],
                'detail_id' => null,
                'client_id' => $clientId,
                'requisites' => $requisites,
                'client_name' => $order->client->fullName,
                'client_phone' => $order->client->phone,
                'client_email' => $order->client->email,
                'product_name' => $productName,
                'product_format' => $productFormat,
                'product_chromacity' => $productChromacity,
                'product_material' => $productMaterial,
                'product_pages' => null,
                'draw' => $order->draw,
                'product_options' => $productOptions,
                'price' => $order->price,
            ];

            // Для многостраничного заказа выводить его элементы
            if ($price['printler_type'] == 3) {
                foreach ($order->elements as $elementIndex => $element) {
                    $data[] = [
                        'id' => $order->id,
                        'product_id' => $productId,
                        'created_at' => $order->created_at->format('d.m.Y'),
                        'close_date' => $price['close_date'],
                        'detail_id' => $order->id.'.'.($elementIndex + 1),
                        'client_id' => null,
                        'requisites' => null,
                        'client_name' => null,
                        'client_phone' => null,
                        'client_email' => null,
                        'product_name' => $element->name,
                        'product_format' => $element->format,
                        'product_chromacity' => $element->chromacity,
                        'product_material' => $element->material,
                        'product_pages' => $element->pages_number,
                        'draw' => $order->draw,
                        'product_options' => $productOptions,
                        'price' => null,
                    ];
                }
            }
        }

        if (count($data) > 1) {
            $fp = fopen(storage_path($filename), 'w');

            foreach ($data as $row) {
                fputcsv($fp, $row);
            }

            fclose($fp);

            chmod(storage_path($filename), 0777);
        }
    }

    /**
     * @param \App\GroupOrders $groupOrder
     * @return string
     */
    protected function getExportFileName(GroupOrders $groupOrder)
    {
        return '/group_order_' . $groupOrder->invoice_number . '_' . $groupOrder->id . '_export_' . time() . '.csv';
    }
}
