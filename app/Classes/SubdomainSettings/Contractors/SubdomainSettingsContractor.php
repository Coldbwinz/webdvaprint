<?php

namespace App\Classes\SubdomainSettings\Contractors;

interface SubdomainSettingsContractor
{
    /**
     * Save settings to the storage
     *
     * @return mixed
     */
    public function save();

    /**
     * Fill settings storage
     *
     * @param array $settings
     * @return $this
     */
    public function fill(array $settings);
}
