<?php

namespace App\Classes\SubdomainSettings\Contractors;

interface SubdomainSettingsStorage
{
    /**
     * Get all settings for the subdomain
     *
     * @param string $subdomain
     * @return array
     */
    public function all($subdomain);

    /**
     * Store all settings for the subdomain
     *
     * @param array $settings
     * @param string $subdomain
     * @return bool
     */
    public function store($settings, $subdomain);
}
