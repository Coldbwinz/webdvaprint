<?php

namespace App\Classes\SubdomainSettings;

use Illuminate\Support\Facades\Facade;

/**
 * @see \DoubleUp\SubdomainSettings\SubdomainSettings
 */
class SubdomainSettingsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'subdomain-settings';
    }
}
