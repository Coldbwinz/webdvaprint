<?php

namespace App\Classes\SubdomainSettings;

use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepositoryContract;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsContractor;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsStorage as StorageContract;

/**
 * Class SubdomainSettings
 *
 * Implements a set of methods for managing multidomain settings.
 *
 * @package App\Classes\SubdomainSettings
 */
class SubdomainSettings extends ConfigRepository implements ConfigRepositoryContract, SubdomainSettingsContractor
{
    /**
     * @var string
     */
    private $subdomain;

    /**
     * @var StorageContract
     */
    private $storage;

    /**
     * Create a new configuration repository.
     *
     * @param StorageContract $storage
     * @param string $subdomain
     */
    public function __construct(StorageContract $storage, $subdomain)
    {
        $this->storage = $storage;

        $this->setSubdomain($subdomain);
    }

    /**
     * Save settings into database
     *
     * @return mixed
     */
    public function save()
    {
        return $this->storage->store($this->items, $this->subdomain);
    }

    /**
     * Fill settings storage
     *
     * @param array $settings
     * @return $this
     */
    public function fill(array $settings)
    {
        array_walk($settings, function ($value, $key) {
            $this->set($key, $value);
        });

        return $this;
    }

    /**
     * Copy settings from current subdomain to the $subdomain
     *
     * @param string $subdomain
     */
    public function replicateTo($subdomain)
    {
        // Settings keys to exclude from replication
        $excludedSettings = ['settings_correct', 'invoice_number', 'act_number', 'issuingAddresses', '1c_sync'];

        $settingsForReplicate = $this->all();

        foreach ($settingsForReplicate as $key => $item) {
            if (in_array($key, $excludedSettings)) {
                unset($settingsForReplicate[$key]);
            }
        }

        $this->storage->store($settingsForReplicate, $subdomain);
    }

    /**
     * @param string $subdomain
     * @return $this
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;

        $this->items = array_merge(config('w2p.defaults'), $this->storage->all($subdomain));

        return $this;
    }

    /**
     * Get settings for the subdomain
     *
     * @param string $subdomain
     * @return mixed
     */
    public function from($subdomain)
    {
        return app('subdomain-settings')->setSubdomain($subdomain);
    }

    /**
     * Получает настройки типографии
     *
     * @return mixed
     */
    public function fromTypography()
    {
        $key = $this->storage->getTypographyKey();

        return $this->from($key);
    }
}
