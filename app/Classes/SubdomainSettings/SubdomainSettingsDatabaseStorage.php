<?php

namespace App\Classes\SubdomainSettings;

use DB;
use Exception;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsStorage;

class SubdomainSettingsDatabaseStorage implements SubdomainSettingsStorage
{
    /**
     * Get all settings for the subdomain
     *
     * @param $subdomain
     * @return array
     */
    public function all($subdomain)
    {
        try {
            $settings = DB::table('settings')
                ->where('subdomain', $subdomain)
                ->value('settings_json');
        } catch (Exception $exception) {
            return [];
        }

        return $settings ? json_decode($settings, true) : [];
    }

    /**
     * Store all settings for the subdomain
     *
     * @param array $settings
     * @param string $subdomain
     * @return bool
     */
    public function store($settings, $subdomain)
    {
        $record = DB::table('settings')
            ->where('subdomain', $subdomain)
            ->first();

        if ($record) {
            return (bool) DB::table('settings')
                ->where('subdomain', $subdomain)
                ->update([
                    'settings_json' => json_encode($settings),
                ]);
        }

        return (bool) DB::table('settings')
            ->insert([
                'subdomain' => $subdomain,
                'settings_json' => json_encode($settings),
            ]);
    }

    /**
     * Название ключа настроек для типографии
     *
     * @return mixed
     */
    public function getTypographyKey()
    {
        return DB::table('settings')
            ->where('default', true)
            ->value('subdomain');
    }
}
