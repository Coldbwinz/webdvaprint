<?php
namespace App\Classes\FPDI;

use \FPDI;

/*
 | -----------------------------------------------------------------------------
 | CLASS USAGE:
 | -----------------------------------------------------------------------------
 |
 | -------------------------------
 | concat
 | -------------------------------
 | $pdf = new FPDIx();
 | $pdf->concat($files);
 | $pdf->Output("bulk.pdf", "F");    // save on server
 |
 | -------------------------------
 | split
 | -------------------------------
 | $pdf = new FPDIx();
 | $pages = $pdf->splitPages($filename); // return array of instance FPDIx
 | foreach ($pages as $index => $page) {
 |     $output = "split_{$index}.pdf";
 |     $page->Output($output, "D");
 | }
 | $pdf->close();
 |
 | -------------------------------
 | save
 | -------------------------------
 | // save on server
 | $pdf->Output($filename, "F");
 |
 | // download response
 | $pdf->Output($filename, "D");
 |
 | -----------------------------------------------------------------------------
 */

class FPDIx extends FPDI
{

    /**
     * Объединяет несколько PDF файлов в один
     *
     * @param array|string $files
     * @return FPDIx
     */
    public function concat($files)
    {
        // $this->setPrintHeader(false);
        // $this->setPrintFooter(false);

        // mixed value
        $files = (is_array($files)) ? $files : [$files];

        foreach($files AS $file)
        {
            $pagecount = $this->setSourceFile($file);

            for($i = 1; $i <= $pagecount; $i++)
            {
                $tplidx = $this->ImportPage($i);
                $size = $this->getTemplateSize($tplidx);

                $width = $size['w'];
                $height = $size['h'];
                $orientation = $width > $height ? 'Landscape' : 'Portrait';

                $this->AddPage($orientation, [$width, $height]);

                $this->useTemplate($tplidx);
            }
        }

        return $this;
    }


    /**
     * [splitPages description]
     *
     * @return FPDIx
     */
    public function splitPages($filename)
    {
    	$pagecount = $this->setSourceFile($filename); // How many pages?

        $objects = [];

    	// Split each page into a new PDF
    	for ($i = 1; $i <= $pagecount; $i++) {
    		$new_pdf = new FPDIx();
    		$new_pdf->AddPage();
    		$new_pdf->setSourceFile($this->currentFilename);
    		$new_pdf->useTemplate($new_pdf->importPage($i));

            $objects[] = $new_pdf;
    	}

        return $objects;
    }

}
