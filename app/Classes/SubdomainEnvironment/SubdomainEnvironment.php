<?php

namespace App\Classes\SubdomainEnvironment;

use Symfony\Component\Yaml\Yaml;

class SubdomainEnvironment
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $environmentFile;

    /**
     * @var array
     */
    private $environment;

    /**
     * @var array
     */
    private $blacklist;

    /**
     * SubdomainEnvironment constructor.
     * @param string $host
     * @param string $environmentFile
     */
    public function __construct($host, $environmentFile)
    {
        $this->host = strtolower($host);
        $this->environmentFile = $environmentFile;
        $this->environment = [];
        $this->blacklisted = [];

        if (file_exists($environmentFile)) {
            $this->environment = $this->parseEnvironmentFile($environmentFile);
            $this->blacklist = isset($this->environment['blacklist']) ? $this->environment['blacklist'] : [];
        }
    }

    /**
     * Get current third level subdomain
     *
     * @return string|null null if subdomain not exists
     */
    public function currentSubdomain()
    {
        return $this->extractSubdomain($this->host);
    }

    /**
     * Extract subdomain name from string
     *
     * @param string $host
     * @param string $default
     * @return null|string
     */
    public function extractSubdomain($host, $default = null)
    {
        $host = str_replace(['http://', 'https://'], '', $host);
        $parts = explode('.', $host);

        if (count($parts) === 3) {
            return $parts[0];
        }

        return $default;
    }

    /**
     * Parse environment yaml file
     *
     * @param $yamlFile
     * @return mixed
     */
    public function parseEnvironmentFile($yamlFile)
    {
        return Yaml::parse(file_get_contents($yamlFile));
    }

    /**
     * Has config for subdomain
     *
     * @param string $subdomain
     * @return bool
     */
    public function hasConfig($subdomain)
    {
        return isset($this->environment[$subdomain]);
    }

    /**
     * Get config for subdomain
     *
     * @param string|null $subdomain
     * @return array
     */
    public function getConfig($subdomain = null)
    {
        $subdomain = $subdomain ?: $this->currentSubdomain();

        if ($this->hasConfig($subdomain)) {
            return $this->environment[$subdomain];
        }

        return [];
    }

    /**
     * Set config for the subdomain and write it to the environment file
     *
     * @param $subdomain
     * @param array $config
     * @return bool
     */
    public function writeConfig($subdomain, array $config)
    {
        $this->environment[$subdomain] = $config;

        return file_put_contents($this->environmentFile, Yaml::dump($this->environment, 5)) !== false;
    }

    /**
     * Remove config for the subdomain
     *
     * @param string $subdomain
     * @return bool
     */
    public function removeConfig($subdomain)
    {
        unset($this->environment[$subdomain]);

        return file_put_contents($this->environmentFile, Yaml::dump($this->environment, 5)) !== false;
    }

    /**
     * Check for subdomain in blacklist
     *
     * @param $subdomain
     * @return bool
     */
    public function blacklisted($subdomain)
    {
        if (! $this->blacklist) {
            return false;
        }

        return in_array($subdomain, $this->blacklist);
    }

    /**
     * @return string
     */
    public function getAll()
    {
        return $this->environment;
    }
}
