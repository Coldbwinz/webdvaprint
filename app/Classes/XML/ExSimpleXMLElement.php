<?php
namespace App\Classes\XML;

class ExSimpleXMLElement extends \SimpleXMLElement
{
    
    /**
     * Append child
     * 
     * @param \App\Classes\XML\ExSimpleXMLElement $root
     * @param \App\Classes\XML\ExSimpleXMLElement $new
     */
    
    public static function appendChild($root, $new) 
    {
        $node = $root->addChild($new->getName(), (string) $new);
        foreach($new->attributes() as $attr => $value) {
            $node->addAttribute($attr, $value);
        }
        foreach($new->children() as $ch) {
            self::appendChild($node, $ch);
        }
    }
    
}

