<?php

namespace App\Classes\OrderStatuses;

use App\OrderStatuses;
use Illuminate\Database\Eloquent\Collection;

class OrderStatusesCollection
{
    /**
     * @var Collection
     */
    protected $statuses;

    /**
     * Название поля, в котором хранится название статуса
     *
     * @var string
     */
    protected $keyAttributeName = 'key';

    /**
     * @param Collection $statuses
     */
    public function __construct(Collection $statuses)
    {
        $this->statuses = $statuses;
    }

    /**
     * Get all statuses
     *
     * @return Collection
     */
    public function all()
    {
        return $this->statuses;
    }

    /**
     * Find status by key
     *
     * findByKey('fully_paid')
     * findByKey(['fully_paid', 'partially_paid'])
     *
     * @param string|array $key
     * @return OrderStatuses|Collection|null
     */
    public function findByKey($key)
    {
        if (! is_array($key)) {
            return $this->statuses->where($this->keyAttributeName, $key)->first();
        }

        return $this->statuses->filter(function ($item) use ($key) {
            return in_array(data_get($item, $this->keyAttributeName), $key, false);
        });
    }

    /**
     * Fetch status field by key
     *
     * fetchFieldByKey('fully_paid', 'name') => 'Полностью оплачен'
     * fetchFieldByKey(['fully_paid', 'partially_paid'], 'name') => ['Полностью оплачен', 'Частично оплачен']
     *
     * @param string|array $key
     * @param string $field
     * @return string|Collection|null
     */
    public function fetchFieldByKey($key, $field)
    {
        $status = $this->findByKey($key);

        if ($status instanceof Collection) {
            return $status->pluck($field);
        }

        if ($status) {
            return $status->getAttribute($field);
        }

        return null;
    }
}
