<?php

namespace App\Classes\Preview;

use Intervention\Image\ImageManager;
use Intervention\Image\Exception\NotReadableException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class Preview
{
    /**
     * @var \Intervention\Image\ImageManager
     */
    protected $image;

    public function __construct(ImageManager $image)
    {
        $this->image = $image;
    }

    /**
     * Generate thumbnail for file
     *
     * @param string $path путь к исходному файлу на сервере
     * @param null|string $destination путь для созранения файла превью
     * @param array $options массив дополнительных опций, см. код.
     * @return mixed
     */
    public function generate($path, $destination = null, $options = [])
    {
        $path = realpath($path);

        if (! file_exists($path)) {
            throw new FileNotFoundException("File {$path} not exists.");
        }

        $options = array_merge([
            'width' => null,
            'height' => 300,
        ], $options);

        if (is_null($destination)) {
            $pathinfo = pathinfo($path);
            $destination = $pathinfo['dirname'].'/'.$pathinfo['basename'].'_preview.png';
        }

        try {
            $image = $this->image->make($path);
        } catch (NotReadableException $exception) {
            $image = $this->image->make(public_path('images/thumb.png'));
        }

        $image->resize($options['width'], $options['height'], function ($constraint) {
            $constraint->aspectRatio();
        })
            ->save($destination);

        return str_replace(public_path(), '', $destination);
    }
}
