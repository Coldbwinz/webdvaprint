<?php
namespace App\Classes\Library;

use Auth;
use App\Orders;
use App\GroupOrders;
use App\OrderStatuses;

class DashboardPanelInfo
{

    /**
     * Append child
     *
     * @param \App\Classes\XML\ExSimpleXMLElement $root
     * @param \App\Classes\XML\ExSimpleXMLElement $new
     */

    public static function userCabinetParams()
    {
        $user = Auth::user();
        if ($user) {
            $clients = $user->contact->clients()->get();
            $client = $user->client()->first();
            if ($clients->count() > 0) {
                $clientIds = $clients->pluck('id')->toArray();

                $statuses = OrderStatuses::$cabinet_group;

                $info['need_attention'] = Orders::whereIn('client_id', $clientIds)
                    ->where(function ($q) use ($statuses) {
                        $q->whereIn('status', $statuses['need_attention'])
                            ->orWhere(function ($q) {
                                // Приостановленная в производстве позиция не должна менять статус в ЛК и не должна переноситься у клиента в раздел Требуют внимания
                                $q->where('status', 1000)
                                    ->where('stopped_user_id', \Auth::user()->id);
                            });
                    })->count();

                $info['user_orders'] = Orders::whereIn('client_id', $clientIds)
                    ->where(function ($query) use ($statuses) {
                        $query->whereIn('status', $statuses['user_orders'])
                            ->orWhereHas('statusInfo', function ($q) {
                                $q->where('id', 1000)
                                    ->where('stopped_user_id', '!=', Auth::user()->id);
                            });
                    })
                    ->count();

                $payQuery = GroupOrders::whereIn('client_id', $clientIds)
                                        ->whereHas('statusInfo', function ($query) use ($statuses) {
                                            $query->whereIn('id', $statuses['payments']);
                                        });
                $info['payments'] = $payQuery->count();

                $waitingPayment = $payQuery->where('money', 0)->get();
                $info['waiting_payment_total'] = $waitingPayment->map(function ($groupOrder) {
                    return $groupOrder->price - $groupOrder->pay;
                })
                ->sum();

                return $info;
            }
        }
        return false;
    }

}
