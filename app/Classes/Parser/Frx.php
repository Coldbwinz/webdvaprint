<?php

namespace App\Classes\Parser;

use Carbon\Carbon;
use File;
use Config;
use App\Classes\XML\ExSimpleXMLElement;

class Frx
{
    /**
     * Copy FRX files
     * 
     * @param string $src
     * @param string $dst
     * @throws \Exception
     */    
    
    public static function copyFrx($src, $dst)
    {
        if (file_exists($src)) {
            // read $src
            $handle = fopen($src, "rb");
            $contents = fread($handle, filesize($src));
            fclose($handle);

            // replace paths
            $dir_name = pathinfo($src, PATHINFO_FILENAME);
            $name = pathinfo($dst, PATHINFO_FILENAME);
            $contents = str_replace($dir_name, $name, $contents);

            // save $dst
            $fpp = fopen($dst, "w");
            fwrite($fpp, $contents);        
            fclose($fpp);
        }
        else {
            throw new \Exception('File not found: '.$src);
        }
    } 
    
    /**
     * Move all files associated with FRX
     * 
     * @param string $file
     * @param string $new
     * @param boolean $delete
     * @throws \Exception
     */
    
    public static function move($file, $new, $delete = false)
    {
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        } 
        
        $pinfo = pathinfo($file);
        $dir1 = $pinfo['dirname'];
        $filename1 = $pinfo['basename'];
        $name1 = $pinfo['filename'];
        
        $pinfo = pathinfo($new);
        $dir2 = $pinfo['dirname'];
        $filename2 = $pinfo['basename'];
        $name2 = $pinfo['filename'];
        
        // make directory
        rmkdir($dir2);
        
        // copy frx
        self::copyFrx($file, $new);
        if ($delete) {
            unlink($file);
        }           
        // copy psd
        $psd = $dir1.'/'.$name1.'.psd';
        if (file_exists($psd)) {
            $newFile = $dir2.'/'.$name2.'.psd';
            copy($psd, $newFile);
            if ($delete) {
                unlink($psd);
            }
        }        
        // copy png
        $png = $dir1.'/'.$name1.'.png';
        if (file_exists($png)) {
            $newFile = $dir2.'/'.$name2.'.png';
            copy($png, $newFile);
            if ($delete) {
                unlink($png);
            }
        }        
        // copy small
        $small = $dir1.'/'.$name1.'_small.png';
        if (file_exists($small)) {
            $newFile = $dir2.'/'.$name2.'_small.png';
            copy($small, $newFile);
            if ($delete) {
                unlink($png);
            }
        }        
        // copy folder
        self::moveAndReplaceFrxFolder($file, $new);
        if ($delete && is_dir($dir1.'/'.$name1)) {
            rrmdir($dir1.'/'.$name1);
        }                     
    }
    
    /**
     * 
     * @param type $file
     * @throws \Exception
     */
    
    public static function delete($file)
    {
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        }        
        
        $pinfo = pathinfo($file);
        $dir = $pinfo['dirname'];
        $name = $pinfo['filename'];
        
        if ($pinfo['extension'] != 'frx') {
            throw new \Exception('FRX file not found: '.$file);
        }
        
        $frx = $file;
        $psd = $dir.'/'.$name.'.psd';
        $png = $dir.'/'.$name.'.png';
        $small = $dir.'/'.$name.'_small.png';
        $folder = $dir.'/'.$name;
        
        if (file_exists($frx)) {
            File::delete($frx);
        }        
        if (file_exists($psd)) {
            File::delete($psd);
        }        
        if (file_exists($png)) {
            File::delete($png);
        }        
        if (file_exists($small)) {
            File::delete($small);
        }
        if (file_exists($folder)) {
            File::deleteDirectory($folder);
        }
    }
    
    /**
     * Parse FRX file to XML (Object ExSimpleXMLElement)
     * 
     * @param string $file
     * @return App\Classes\XML\ExSimpleXMLElement
     * @throws Exception
     */
    
    public static function parseFrx($file)
    {
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        }       
        
        $handle = fopen($file, 'r');
        $body = fread($handle, filesize($file));
        fclose($handle);
        
        return new ExSimpleXMLElement($body);        
    }
    
    /**
     * Combine two FRX files
     * Result save to $new
     * 
     * @param type $file1
     * @param type $file2
     * @param mixed $prefix
     * @return void
     */
    
    public static function combine($file1, $file2, $new, $prefix = 2)
    {
        $prefix = $prefix.'_';
        // move files
        self::move($file1, $new);        
        self::moveAndReplaceFrxFolder($file2, $new, $prefix);

        // make new frx
        $content = self::combineXml($new, $file2, $prefix);        
        file_put_contents($new, $content);        
    }
    
    /**
     * Merge FRX file2 to file1
     * 
     * @param string $file1
     * @param string $file2
     * @param mixed $prefix
     * @return void
     */
    
    public static function merge($file1, $file2, $prefix = 2)
    {
        $prefix = $prefix.'_';
        // move file
        self::moveAndReplaceFrxFolder($file2, $file1, $prefix);

        // make new frx
        $content = self::combineXml($file1, $file2, $prefix);

        pathinfo($file1, PATHINFO_DIRNAME);
        file_put_contents($file1, $content);
    }
    
    /**
     * Parse XML and merge two FRX files
     * Return XML string
     * 
     * @param string $file1
     * @param string $file2
     * @param array $pictures
     * @return string
     */
    
    public static function combineXml($file1, $file2, $prefix = null)
    {
        $xml = self::parseFrx($file1);        
        $xmlSecond = self::parseFrx($file2);        
        $page2 = $xmlSecond->ReportPage;
        
        if (!empty($page2)) {
            // change pictures paths
            if (!empty($prefix)) {                
                if (!empty($page2->DataBand)) {
                    $newDirName = pathinfo($file1, PATHINFO_FILENAME);
                    foreach ($page2->DataBand->PictureObject as $key => $pictureObject) {                        
                        $location = $pictureObject->attributes()->ImageLocation;
                        //replace
                        $dirname = dirname($location);
                        $name = pathinfo($location, PATHINFO_FILENAME);                         
                        $replace = str_replace($dirname, $newDirName, $location);
                        $replace = str_replace($name, $prefix.$name, $replace);
                        // save new location
                        $pictureObject->attributes()->ImageLocation = $replace;
                    }
                }
            }
            
            // add new Page
            $page2->attributes()->Name = 'Page2';
            $xml->appendChild($xml, $page2);
        }        
        return $xml->asXML();
    }
    
    /**
     * Move folder associates with FRX
     * 
     * @param string $file
     * @param string $new
     * @param string $prefix = false
     * @throws \Exception
     */
    
    public static function moveAndReplaceFrxFolder($file, $new, $prefix = '')
    {
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        } 
        
        $pinfo = pathinfo($file);
        $dir1 = $pinfo['dirname'];
        $filename1 = $pinfo['basename'];
        $name1 = $pinfo['filename'];
        
        $pinfo = pathinfo($new);
        $dir2 = $pinfo['dirname'];
        $filename2 = $pinfo['basename'];
        $name2 = $pinfo['filename'];
    
        // copy folder
        $folder = $dir1.'/'.$name1;
        if (file_exists($folder)) {
            $newFolder = $dir2.'/'.$name2;
            rmkdir($newFolder);
            
            $files = scandir($folder);
            foreach ($files as $f) {
                if (!in_array($f, ['.', '..'])) {
                    $src = $folder.'/'.$f;
                    $dst = $newFolder.'/'.$prefix.$f;
                    copy($src, $dst);
                }
            }            
        }
    }
    
    /**
     * Get config 
     * config/frx.php
     * 
     * @param string $name
     * @return mixed
     */
    
    public static function getConfig($name = null)
    {
        if (!is_null($name)) {
            return Config::get('frx.'.$name);
        }
        else {
            return Config::get('frx');
        }
    }
    
    /**
     * Get path to library folder
     * @return string
     */
    
    public static function getLibraryPath()
    {
        return rtrim(self::getConfig('path.library'), '/');
    }
    
    /**
     * Get path to user templates
     * @return string
     */
    
    public static function getTemplatesPath()
    {
        return rtrim(self::getConfig('path.templates'), '/');
    }
    
    /**
     * Get path to app preview folder
     * @return string
     */
    
    public static function getPreviewPath()
    {
        return rtrim(self::getConfig('path.preview'), '/');
    }
    
    /**
     * Get path to FRX preview file (*_small.png)
     * 
     * @param string $file
     * @throws \Exception
     */
    
    public static function getPreviewFile($file)
    {
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        }         
        $pinfo = pathinfo($file);        
        return $pinfo['dirname'].'/'.$pinfo['filename'].'_small.png';        
    }

    /**
     * Merge multiply frx files to the new one
     *
     * @param array $files array of frx files
     * @param string|null $combinedFile path to the combined file
     * @return string full path to the combined frx file
     * @throws \Exception
     */
    public static function mergeMultiplyFiles(array $files, $combinedFile = null)
    {
        foreach ($files as $file) {
            if (! file_exists($file)) {
                throw new \Exception('File ' . $file . ' not found!');
            }
        }

        $combinedFile = $combinedFile ?: pathinfo($files[0], PATHINFO_DIRNAME).'/'.str_random().'.frx';
        $combinedFilename = pathinfo($combinedFile, PATHINFO_FILENAME);
        $combinedFolder = pathinfo($combinedFile, PATHINFO_FILENAME);
        $combinedFilePath = pathinfo($combinedFile, PATHINFO_DIRNAME);

        $combinedFrx = new \SimpleXMLElement('<Report/>');
        $combinedFrx->addAttribute('ScriptLanguage', 'CSharp');
        $combinedFrx->addAttribute('ReportInfo.Created', Carbon::now()->format('d/m/Y H:i:s'));
        $combinedFrx->addAttribute('ReportInfo.Modified', Carbon::now()->format('d/m/Y H:i:s'));
        $combinedFrx->addAttribute('ReportInfo.CreatorVersion', '2015.3.12.0');

        foreach ($files as $index => $file) {
            $frxFolder = pathinfo($file, PATHINFO_DIRNAME);

            $frx = simplexml_load_file($file);
            foreach ($frx->ReportPage as $page) {
                $combinedPage = $combinedFrx->addChild($page->getName());
                foreach ($page->attributes() as $key => $value) {
                    $combinedPage->addAttribute($key, $value);
                }

                if ($page->children()->count()) {
                    foreach ($page->children() as $dataBand) {
                        $combinedDataBand = $combinedPage->addChild($dataBand->getName());
                        foreach ($dataBand->attributes() as $key => $value) {
                            $combinedDataBand->addAttribute($key, $value);
                        }

                        if ($dataBand->children()->count()) {
                            foreach ($dataBand->children() as $object) {
                                $combinedObject = $combinedDataBand->addChild($object->getName());
                                foreach ($object->attributes() as $key => $value) {
                                    $combinedObject->addAttribute($key, $value);
                                }

                                if ($object->getName() === 'PictureObject') {
                                    $imageLocation = (string) $object->attributes()->ImageLocation;
                                    $newFilename = $index.'_'.basename($object->attributes()->ImageLocation);
                                    $combinedObject->attributes()->ImageLocation = $combinedFolder.'/'.$newFilename;

                                    if (! is_dir($combinedFilePath.'/'.$combinedFolder)) {
                                        mkdir($combinedFilePath.'/'.$combinedFolder);
                                    }

                                    // Copy images
                                    $objectImagePath = $frxFolder.'/'.$imageLocation;
                                    if (file_exists($objectImagePath)) {
                                        copy($objectImagePath, $combinedFilePath.'/'.$combinedFolder.'/'.$newFilename);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $combinedFrx->saveXML($combinedFile);

        return $combinedFile;
    }

    /**
     * Get size from frx
     *
     * @param $frxFile
     * @return array
     */
    public static function getSize($frxFile)
    {
        $frx = simplexml_load_file($frxFile);

        $attributes = $frx->ReportPage->DataBand->attributes();

        return [
            'width' => (int) $attributes->Width,
            'height' => (int) $attributes->Height,
        ];
    }
}

