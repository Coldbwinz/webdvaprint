<?php
namespace App\Classes\Parser;

use Log;

class RubyParser
{
    /**
     * Parse file with Ruby Parser
     * 
     * @param string $filepath
     * @return bool
     */
    
    public static function parse($filepath)
    {    
        Log::info('Start parsing: '.$filepath);

        $url = config('library.psd_parser_url')."?".http_build_query([
                'path' => $filepath,
            ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);

        $pathinfo = pathinfo($filepath);
        $frx = $pathinfo['dirname'].'/'.$pathinfo['filename'].'.frx';
        $png = $pathinfo['dirname'].'/'.$pathinfo['filename'].'.png';

        if (file_exists($frx) && file_exists($png)) {
            return $frx;
        }

        return false;
    }
}
