<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsRelations extends Model
{
    protected $table = 'contacts_relations';
    public $timestamps = false;
    
    public function relation(){
        return $this->belongsTo('App\Contacts', 'related_id', 'id');
    }
}
