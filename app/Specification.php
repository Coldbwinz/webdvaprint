<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * (Техзадание)
 */
class Specification extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'specifications';

    /**
     * [$fillable description]
     * @var array
     */
    protected $fillable = ['order_id', 'user_id', 'description', 'links', 'attachment_ids'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'viewed' => 'bool',
    ];

    /**
     * User who owns this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Order who associate with this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id');
    }

    /**
     * Specification parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function history()
    {
        return $this->hasMany(SpecificationHistory::class, 'specification_id');
    }

    /**
     * Attachment files associate with this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments() {
        return $this->hasMany(AttachmentFile::class, 'specification_id');
    }

    /**
     * Создаёт задание дизайнеру на основе заказа. Да я понимаю на сколько это глупо, но че делать.
     *
     * @param Orders $order
     * @param string $description
     */
    public static function makeFromOrder(Orders $order, $description = null)
    {
        // заказ шаблона для дизайнера
        $order->creator = 5; //Заказ дизайнеру
        $order->status = 300;
        $order->use_specification = true;
        $order->save();

        // @todo создать ТЗ
        $workTemplateFile = '/lib/frx/'.$order->pdf;
        $thumbnailTemplateFile = '/lib/frx/'.basename($order->img);

        $basename = str_random();
        $uploadPath = 'upload/technical-task-files/'.substr(strtolower($basename), 0, 2);
        $newFileName = $basename.'.'.pathinfo($workTemplateFile, PATHINFO_EXTENSION);
        $relativePath = '/'. $uploadPath.'/'.$newFileName;
        $previewPath = '/'.$uploadPath.'/thumb_'.$basename.'.jpg';

        if (! is_dir(public_path($uploadPath))) {
            rmkdir(public_path($uploadPath));
        }

        if ($order->pdf) { //Если присутствует PDF, что в случае нередактируемого шаблона не факт
            copy(public_path($workTemplateFile), public_path($relativePath)); // copy template
        }
        copy(public_path($thumbnailTemplateFile), public_path($previewPath)); // copy preview

        \DB::beginTransaction();

        $attachment = AttachmentFile::create([
            'user_id' => $order->client->account->id,
            'order_id' => $order->id,
            'type' => null, // @todo
            'filename' => $newFileName,
            'path' => $relativePath,
            'url' => $relativePath,
            'preview' => asset($previewPath),
            'original_name' => pathinfo($workTemplateFile, PATHINFO_BASENAME),
            'mime_type' => mime_content_type(public_path($workTemplateFile)),
            'size' => filesize(public_path($workTemplateFile)),
            'title' => basename($order->pdf),
        ]);

        $description = $description ?: "Создать шаблон по предоставленному образцу.";
        $specification = Specification::create([
            'order_id' => $order->id,
            'user_id' => auth()->user()->id,
            'description' => $description,
            'links' => json_encode([]),
            'attachment_ids' => json_encode([
                $attachment->id
            ]),
        ]);

        $specification->attachments()->save($attachment);

        $position = 1;

        // save version
        $version = SpecificationHistory::create([
            'specification_id' => $specification->id,
            'data' => serialize($specification),
            'position' => $position,
        ]);

        // add log
        $value = '<a href="'.route('order.specification.show', [$order->id, $version->id]).'">ТЗ сформировано</a>';
        addLog($order->id, 'Заказан редактируемый шаблон', auth()->user()->id, $value, null, 0, 'по предоствленному образцу');

        \DB::commit();
    }
}
