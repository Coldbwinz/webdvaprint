<?php

namespace App\Listeners;

use App\Events\AccountWasCreated;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAccountDetailsEmail
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AccountWasCreated  $event
     * @return void
     */
    public function handle(AccountWasCreated $event)
    {
        $account = $event->user;
        $password = $event->password;

        $this->mailer->send(
        'emails.account.new-account-details',
            compact('account'),
            function ($message) use ($account) {
                $message->subject('Добро пожаловать на '.config('app.name'))
                    ->to($account->email);
            }
        );
    }
}
