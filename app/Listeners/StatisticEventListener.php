<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Events\ClientWasBilled;
use App\Events\PaymentWasAdded;
use App\Events\GroupOrderWasFormed;
use Illuminate\Contracts\Events\Dispatcher;

class StatisticEventListener
{
    /**
     * @param \App\Events\GroupOrderWasFormed $event
     */
    public function onGroupOrderFormed(GroupOrderWasFormed $event)
    {
        $groupOrder = $event->groupOrder;
        $client = $groupOrder->client;
        $manager = $groupOrder->manager;
        $clientGroupOrderCount = $client->groupOrders->count();

        // Первый заказ клиента - клиент новый
        if ($clientGroupOrderCount === 1) {
            $manager->statisticClients()->create([
                'new_client' => true,
            ]);
        }

        // Повторный заказ клиента
        $repeatedOrderCount = 2;
        if ($clientGroupOrderCount > 1 && $clientGroupOrderCount <= $repeatedOrderCount) {
            $manager->statisticOrders()->create([
                'repeat' => true,
            ]);
        }

        // Ожидаемое поступление
        $manager->statisticInvoices()->create([
            'expected_amount' => $event->groupOrder->price,
            'created_at' => Carbon::parse($event->groupOrder->close_date),
        ]);
    }

    /**
     * Выставлен счёт клиенту
     *
     * @param \App\Events\ClientWasBilled $event
     */
    public function onClientWasBilled(ClientWasBilled $event)
    {
        $manager = $event->groupOrder->manager;

        $groupOrder = $event->groupOrder;
        $client = $groupOrder->client;
        $clientGroupOrderCount = $client->groupOrders->count();
        $isNewOrder = $clientGroupOrderCount === 1;
        $isRepeated = $clientGroupOrderCount === 2;

        $manager->statisticInvoices()->create([
            'invoice_amount' => $groupOrder->price,
            'new_order' => $isNewOrder,
            'repeat_order' => $isRepeated,
        ]);
    }

    /**
     * Добавлена оплата
     *
     * @param \App\Events\PaymentWasAdded $event
     */
    public function onPaymentWasAdded(PaymentWasAdded $event)
    {
        $groupOrder = $event->groupOrder;
        $manager = $groupOrder->manager;

        $manager->statisticInvoices()->create([
            'paid_invoice_amount' => $event->amount,
            'full_paid' => $groupOrder->pay >= $groupOrder->price,
        ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\GroupOrderWasFormed',
            'App\Listeners\StatisticEventListener@onGroupOrderFormed'
        );

        $events->listen(
            'App\Events\ClientWasBilled',
            'App\Listeners\StatisticEventListener@onClientWasBilled'
        );

        $events->listen(
            'App\Events\PaymentWasAdded',
            'App\Listeners\StatisticEventListener@onPaymentWasAdded'
        );
    }
}
