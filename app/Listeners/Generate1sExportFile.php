<?php

namespace App\Listeners;

use App\Classes\Sync1s\Exporter;
use App\Events\GroupOrderWasFormed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Generate1sExportFile
{
    /**
     * @var \App\Classes\Sync1s\Exporter
     */
    private $exporter;

    /**
     * Create the event listener.
     *
     * @param \App\Classes\Sync1s\Exporter $exporter
     */
    public function __construct(Exporter $exporter)
    {
        $this->exporter = $exporter;
    }

    /**
     * Handle the event.
     *
     * @param  GroupOrderWasFormed  $event
     * @return void
     */
    public function handle(GroupOrderWasFormed $event)
    {
        $this->exporter->export($event->groupOrder);
    }
}
