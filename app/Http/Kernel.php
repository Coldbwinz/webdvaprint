<?php

namespace App\Http;

use App\Http\Middleware\RedirectIfSettingsIncorrect;
use App\Http\Middleware\RedirectIfSubdomainNotExists;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        RedirectIfSubdomainNotExists::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'is.user' => \App\Http\Middleware\IsUserAccount::class,
        'is.manager' => \App\Http\Middleware\IsManagerAccount::class,
        'is.admin' => \App\Http\Middleware\IsAdminAccount::class,
        'is.root' => \App\Http\Middleware\IsRootAccount::class,
        'only.user' => \App\Http\Middleware\OnlyUserAccount::class,
        'only.user' => \App\Http\Middleware\OnlyUserAccount::class,
        'only.partner' => \App\Http\Middleware\OnlyPartnerAccount::class,
        'only.manager' => \App\Http\Middleware\OnlyManagerAccount::class,
        'only.admin' => \App\Http\Middleware\OnlyAdminAccount::class,
        'only.root' => \App\Http\Middleware\OnlyRootAccount::class,
        'settings.correct' => RedirectIfSettingsIncorrect::class,
    ];
}
