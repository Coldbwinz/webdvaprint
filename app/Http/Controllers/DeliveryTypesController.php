<?php

namespace App\Http\Controllers;

use App\Services\SettingsValidator;
use App\DeliveryTypes;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Validator;

class DeliveryTypesController extends Controller
{
    public function index(Request $request)
    {
        $delyvery_types_table = DeliveryTypes::paginate(20);

        $breadcrumbs[] = 'Типы доставки';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'delivery_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('delivery_types.index', [
            'page' => $page,
            'delivery_types_table' => $delyvery_types_table,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:delivery_types,name',
        ]);
        if ($validator->fails()) {
            return redirect('/delivery_types')->withErrors($validator)->withInput();
        }

        $delivery_type = new DeliveryTypes();
        $delivery_type->name = $request->input('name');
        $delivery_type->cost = $request->input('cost');
        $delivery_type->days = $request->input('days');
        $delivery_type->save();

        return redirect('/delivery_types');
    }

    public function edit($id)
    {
        $delivery_type = DeliveryTypes::find($id);

        $breadcrumbs[] = 'Типы доставки';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'delivery_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('delivery_types.edit', [
            'page' => $page,
            'delivery_type' => $delivery_type
        ]);
    }

    public function update($id, Request $request)
    {
        $delivery_type = DeliveryTypes::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:delivery_types,name,'.$id.',id'
        ]);
        if ($validator->fails()) {
            return redirect('/delivery_type/edit/'.$id)->withErrors($validator)->withInput();
        }

        $delivery_type->name = $request->input('name');
        $delivery_type->cost = $request->input('cost');
        $delivery_type->days = $request->input('days');
        $delivery_type->save();

        SettingsValidator::updateValidationFlag();

        return redirect('/delivery_types');
    }
}
