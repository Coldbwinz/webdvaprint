<?php

namespace App\Http\Controllers;

use App\Orders;
use App\TemplateDownload;
use Carbon\Carbon;
use App\GroupOrders;
use Illuminate\Http\Request;

class PartnerPaymentController extends Controller
{
    /**
     * Payment types
     *
     * @var array
     */
    public static $types = [
        2 => 'по счёту',
        1 => 'в кассу',
        3 => 'e-money',
    ];

    /**
     * Add pay for partner's group order
     *
     * @param int $groupOrderId
     * @param Request $request
     * @return mixed
     */
    public function pay($groupOrderId, Request $request)
    {
        $this->validate($request, [
            'price' => 'required',
            'pay_type' => 'required',
        ]);

        $groupOrder = GroupOrders::findOrFail($groupOrderId);

        $price = $request->input('price');
        $payType = $request->input('pay_type');

        $groupOrder->pay_partner += $price;

        switch ($payType) {
            case 1:
                $groupOrder->pay_cash_partner += $price;
                break;
            case 2:
                $groupOrder->pay_rs_partner += $price;
                break;
            case 3:
                $groupOrder->pay_eth_partner += $price;
                break;
        }

        if (! empty($request->input('close_date'))) {
            $groupOrder->close_date_partner = $request->input('close_date');
        } else {
            $groupOrder->close_date_partner = Carbon::createFromTimestamp(
                $groupOrder->orders->min('close_date')
            )->format('d.m.Y');
        }

        $groupOrder->status_partner = 7;

        if ($groupOrder->pay_partner >= $groupOrder->price_partner) {
            $groupOrder->money_partner = 1;
            $groupOrder->status_partner = 6;
        }

        $groupOrder->save();

        // Add to logs
        $payType = isset(static::$types[$payType]) ? static::$types[$payType] : $payType;
        $comment = "{$price} руб. {$payType}";

        addLog(0, 7, auth()->user()->id, "{$price} руб.", $groupOrder->id, 1, "{$comment}");

        return back()->with('selected_group_order_id', $groupOrder->id);
    }

    /*
     * Полная оплата счёта
     */
    public function payFull($groupOrderId, Request $request)
    {
        $group_order = GroupOrders::findOrFail($groupOrderId);

        $price = $group_order->price_partner - $group_order->pay_partner;

        switch ($request->pay_type) {
            case 1:
                $group_order->pay_cash_partner += $price;
                break;
            case 2:
                $group_order->pay_rs_partner += $price;
                break;
            case 3:
                $group_order->pay_eth_partner += $price;
                break;
        }
        $group_order->pay_partner = $group_order->price_partner;
        $group_order->status_partner = 6;
        $group_order->money_partner = 1;
        $group_order->save();

        $this->check_order_for_closing($group_order->id, 6);

        $group_order->checkGroupOrdersForPrintlerDownloading();

        $pay_type = $request->input('pay_type');

        $types = [
            2 => 'по счёту',
            1 => 'в кассу',
            3 => 'e-money',
        ];

        $pay_type = isset($types[$pay_type]) ? $types[$pay_type] : $pay_type;
        $comment = "{$price} руб. {$pay_type}";

        addLog(0, 6, auth()->user()->id, "{$price} руб.", $group_order->id, 1, "{$comment}");

        return back()->with('selected_group_order_id', $group_order->id);
    }

    /**
     * @param $group_id
     * @param $status
     */
    public function check_order_for_closing($group_id, $status)
    {
        if (in_array($status, [6, 52, 57, 1010])) {
            $group_orders = Orders::where('group_id', $group_id)->get(['status']);
            $count_closed = 0;
            foreach ($group_orders as $item) {
                if (in_array($item->status, [6, 52, 57, 1010])) {
                    if ($item->price == $item->pay) {
                        $count_closed++;
                    }
                }
            }
            if ($count_closed == count($group_orders)) {
                $group_order = GroupOrders::find($group_id);
                if ($group_order->price_partner == $group_order->pay_partner) {
                    $group_order->status_partner = 52;
                    $group_order->save();
                }
            }
        }
    }
}
