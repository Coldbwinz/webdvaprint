<?php

namespace App\Http\Controllers\Contractors;

use App\User;
use App\Contacts;
use App\UserGroup;
use App\Intermediary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\IntermediaryWasCreated;
use App\Emails\IntermediaryAccountCreatedNotification;

class IntermediaryController extends Controller
{
    /**
     * @var Intermediary
     */
    private $intermediary;

    /**
     * @var SubdomainSettingsContractor
     */
    private $settings;

    /**
     * @var Contacts
     */
    private $contact;

    /**
     * @param Contacts $contact
     * @param Intermediary $intermediary
     */
    public function __construct(
        Contacts $contact,
        Intermediary $intermediary
    ) {
        $this->intermediary = $intermediary;
        $this->contact = $contact;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $intermediaries = $this->intermediary
            ->with('contact.user')
            ->latest()
            ->paginate(20);

        $page = [
            'menu' => 'contractors',
            'submenu' => 'intermediaries',
            'breadcrumbs' => [
                'Контрагенты',
                'Посредники'
            ]
        ];

        return view('contractors.intermediary.index', compact('intermediaries', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'intermediaries',
            'breadcrumbs' => [
                'Контрагенты',
                'Посредники'
            ]
        ];

        return view('contractors.intermediary.create', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';
        
        $this->validate($request, [
            'name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'subdomain' => 'required|alpha_num|max:50|unique:intermediaries',
            'email' => 'required|email|unique:users,email,NULL,id,subdomain_id,' . $subdomain_id,
            //'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|min:5|confirmed',
        ]);


        // Make user
        $password = $request->input('password');
        $user = User::create([
            'email' => $request->input('email'),
            'password' => bcrypt($password),
            'id_group' => UserGroup::MANAGER,
        ]);

        // Create new contact...
        $contact = $user->contact()->create(
            $request->only(['name', 'last_name', 'phone', 'email', 'description'])
        );

        // Create intermediary for the contact
        $intermediary = $contact->intermediary()->create(
            $request->only('subdomain')
        );

        // add new environment for subdomain
        $subdomain = $request->input('subdomain');
        $env = app('subdomain-environment');
        $env->writeConfig($subdomain, $env->getConfig());

        // copy settings from parent
        \DB::table('settings')->insert([
            'subdomain' => $subdomain,
            'settings_json' => json_encode(settings()->all()),
        ]);

        // Send account information to the intermediary's email
        (new IntermediaryAccountCreatedNotification($intermediary, $request->input('password')))->send();

        return redirect()->route('intermediary.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $intermediary = $this->intermediary->findOrFail($id);

        $page = [
            'menu' => 'contractors',
            'submenu' => 'intermediaries',
            'breadcrumbs' => [
                'Контрагенты',
                'Посредники'
            ]
        ];

        return view('contractors.intermediary.edit', compact('intermediary', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $intermediary = $this->intermediary->findOrFail($id);

        $this->validate($request, [
            'subdomain' => 'required|alpha_num|max:50|unique:intermediaries,subdomain,' . $intermediary->id,
        ]);

        $subdomain = $request->input('subdomain');

        // update subdomain environment
        if ($intermediary->subdomain !== $subdomain) {
            $env = app('subdomain-environment');
            $env->removeConfig($intermediary->subdomain);
            $env->writeConfig($subdomain, $env->getConfig());
        }

        $intermediary->update(
            $request->all()
        );

        return redirect()->route('intermediary.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
