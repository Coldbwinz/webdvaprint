<?php

namespace App\Http\Controllers\Contractors;

use App\Orders;
use App\Contacts;
use App\Contractor;
use App\ContractorToken;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContractorRequest;

class ContractorsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'contractors',
            'breadcrumbs' => [
                'Контрагенты',
                'Подрядчики'
            ]
        ];

        // block - все что ниже работает для перехода по кнопке добавления из orders.history
        $open_form = $request->has('op') ? $request->op : 0;
        $summ_worker = $request->has('summ_worker') ? $request->summ_worker : 0;
        $close_date = $request->has('close_date') ? $request->close_date : null;

        $order = null;
        if ($request->has('id')) {
            $order = Orders::find($request->id);
        }
        //end block

        $contractors = Contractor::with('contacts')->latest()->paginate(20);

        return view('contractors.contractors.index', compact('contractors', 'page', 'open_form', 'order', 'summ_worker', 'close_date'));
    }

    /**
     * Create new contractor page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    /*public function create()
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'contractors',
            'breadcrumbs' => [
                'Контрагенты',
                'Создать'
            ]
        ];

        return view('contractors.contractors.create', compact('page'));
    }*/

    /**
     * Store new contractor
     *
     * @param ContractorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContractorRequest $request)
    {
        $contractor = Contractor::create($request->except(['summ_worker', 'order_id', 'close_date']));

        $contact = Contacts::create($request->all());
        $this->syncWithContacts($contractor, [$contact->id]);

        //$this->syncWithContacts($contractor, $request->input('contacts'));

        // @todo show success flash message, e.g. laracasts/flash
        if ($request->has('order_id')) {
            return redirect()->route('order.inwork', array_merge($request->except(['id']), [
                'id' => $request->order_id,
                'contractor_id' => $contractor->id,
            ]));
        }

        return redirect()->route('contractors.contractors.index');
    }

    /**
     * Edit contractor page
     *
     * @param Contractor $contractor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Contractor $contractor)
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'contractors',
            'breadcrumbs' => [
                'Контрагенты',
                'Подрядчики',
                'Редактирование',
            ]
        ];

        $contact = $contractor->contacts()->first();

        return view('contractors.contractors.edit', compact('contractor', 'contact', 'page'));
    }

    /**
     * @param Contractor $contractor
     * @param ContractorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Contractor $contractor, ContractorRequest $request)
    {
        $contractor->update($request->all());

        // update contact
        $contact = $contractor->contacts()->first();
        if (!$contact) {
            $contact = Contacts::create($request->all());
        }
        else {
            $contact->update($request->all());
        }

        return redirect()->route('contractors.contractors.index');
    }

    /**
     * Delete contractor
     *
     * @param Contractor $contractor
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function destroy(Contractor $contractor)
    {
        $contractor->delete();

        // @todo show success flash message, e.g. laracasts/flash

        return redirect()->route('contractors.contractors.index');
    }*/

    /**
     * Order page
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function orderPage($token)
    {
        $token = ContractorToken::where('token', $token)->first();
        if (!$token) {
            abort(404);
        }
        $token->activate = true;
        $token->save();

        $contractor = $token->contractor()->first();
        if (!$contractor) {
            abort(404);
        }

        $order = $token->order()->first();
        if (!$order) {
            return view('contractors.contractors.pages.not_found');
        }

        $filename = public_path('lib/frx/').$order->pdf;
        if (!file_exists($filename)) {
            return view('contractors.contractors.pages.not_found');
        }

        $job = $contractor->jobs()->where('order_id', $order->id)->first();
        addLog($order->id, 'Принят в работу', $order->manager->id,
            'Дата выдачи: ' . $job->deadline->format('m.d.Y'), null, 0,
            'Срочность: ' . $order->price_details['selected_urgency']
        );

        return view('contractors.contractors.pages.order', compact('order', 'job', 'token'));
    }

    /**
     * Download Pdf
     *
     * @param  string $token
     * @return \Illuminate\Http\Response
     */
    public function downloadPdf($token)
    {
        $token = ContractorToken::where('token', $token)->first();
        if (!$token) {
            abort(404);
        }

        $contractor = $token->contractor()->first();
        if (!$contractor) {
            abort(404);
        }

        $order = $token->order()->first();
        if (!$order || !$order->pdf) {
            return view('contractors.contractors.pages.not_found');
        }

        if (in_array($order->status, [1000, 1010])) {
            return view('contractors.contractors.pages.cant_download');
        }

        $filename = public_path('lib/frx/').$order->pdf;
        if (!file_exists($filename)) {
            return view('contractors.contractors.pages.not_found');
        }

        $order->setStatus(status('preparing_to_press', 'id'));
        $order->save();

        addLog($order->id, 'Макет скачан', $order->manager->id, '' , null, 0, '');

        return response()->download($filename);
    }



    /**
     * Sync contractor with contacts
     *
     * @param $contactIds
     * @param $contractor
     * @internal param ContractorRequest $request
     */
    protected function syncWithContacts($contractor, $contactIds)
    {
        return $contractor->contacts()->sync($contactIds);
    }

}
