<?php

namespace App\Http\Controllers\Contractors;

use App\Emails\EmployeeCreated;
use App\Models\Salary\SalaryOperation;
use App\Services\CarbonDatePeriod;
use Carbon\Carbon;
use Validator;
use App\User;
use App\Contacts;
use App\UserGroup;
use Illuminate\Http\Request;
use App\Events\EmployeeWasCreated;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard as Auth;

class EmployeeController extends Controller
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * @param User $user
     * @param Auth $auth
     */
    public function __construct(User $user, Auth $auth)
    {
        $this->user = $user;
        $this->auth = $auth;

        $this->middleware('is.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userGroups = $this->getUserGroups();

        $subdomain = \App\Subdomain::findCurrent();
        if ($subdomain) {
            $query = $this->user
                ->with('group')
                ->whereHas('group', function ($query) {
                    $query->where('role', '!=', 'root')
                        ->where('role', '!=', 'client')
                        ->where('role', '!=', 'partner');
                })
                ->where('subdomain_id', $subdomain->id)
                ->latest();
        }
        else {
            $query = $this->user
                ->with('group')
                ->whereHas('group', function ($query) {
                    $query->where('role', '!=', 'root')
                        ->where('role', '!=', 'client')
                        ->where('role', '!=', 'partner');
                })
                ->latest();
        }
        $employees = $query->paginate(20);

        $page = [
            'menu' => 'contractors',
            'submenu' => 'employees',
            'breadcrumbs' => [
                'Контрагенты',
                'Сотрудники'
            ]
        ];

        return view('contractors.employees.index', compact('employees', 'page', 'userGroups'));
    }

    /**
     * Save new employee
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,subdomain_id,' . $subdomain_id,
            //'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
            'id_group' => 'in:'.$this->ruleUserGroupArray(),
        ]);
        $validator->setAttributeNames($this->fieldNames());
        if ($validator->fails()) {
            return redirect()->back()->withError($validator);
        }

        // make fields - user
        $fields = $request->all();
        $fields['password'] = bcrypt($request->password);
        $user = User::create($fields);

        // make fields - contact
        $fields['user_id'] = $user->id;
        $fields['is_company'] = 0;
        $fields['last_name'] = $request->lastname;
        $fields['description'] = $request->comment;
        $contact = Contacts::create($fields);

        // Send email to created user
        app(EmployeeCreated::class)->send($user, $request->input('password'));

        return redirect()->back();
    }

    /**
     * Get russian field names
     * @return array
     */
    protected function fieldNames()
    {
        return [
            'name' => 'Имя (Отчество)',
            'lastname' => 'Фамилия',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'confirm_password' => 'Повтор пароля',
            'position' => 'Должность',
            'id_group' => 'Подразделение',
        ];
    }

    /**
     * Get user groups for employees
     * array Key = user_groups.id
     *
     * @return array
     */

    protected function getUserGroups()
    {
        $perms = [];

        if (auth()->user()->isNot(['manager', 'client'])) {
            $perms[UserGroup::ADMIN] = 'Администрация';
        }

        $managerPerms = [
            UserGroup::MANAGER => 'Менеджмент',
            UserGroup::DIRECTOR => 'Производство',
            UserGroup::DESIGNER => 'Препресс',
        ];
        return $perms + $managerPerms;
    }

    protected function ruleUserGroupArray()
    {
        $str = implode(',', array_keys($this->getUserGroups()));
        return $str;
    }

    /**
     * Карточка сотрудника
     *
     * @param User $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $employee)
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'employees',
            'breadcrumbs' => [
                'Контрагенты',
                'Сотрудники'
            ]
        ];

        return view('contractors.employees.edit', compact('employee', 'page'));
    }

    /**
     * Страница с логом операциями по зарплате сотрудника
     *
     * @param User $employee
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function salary(User $employee, Request $request)
    {
        $operations = $employee
            ->salaryOperations()
            ->with('client.type')
            ->filter($request->all())
            ->where(function ($query) {
                $query->whereNull('type')
                    ->orWhere('type', '!=', SalaryOperation::SALARY_ACCRUAL);
            })
            ->oldest('created_at')
            ->paginate(50);

        $page = [
            'menu' => 'contractors',
            'submenu' => 'employees',
            'breadcrumbs' => [
                'Контрагенты',
                'Сотрудники'
            ]
        ];

        return view('contractors.employees.salary', compact('page', 'operations', 'employee'));
    }

    public function motivation()
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'employees',
            'breadcrumbs' => [
                'Контрагенты',
                'Сотрудники'
            ]
        ];

        return view('contractors.employees.motivation', compact('page'));
    }
}
