<?php

namespace App\Http\Controllers\Contractors;

use App\GroupOrders;
use App\Services\SettingsValidator;
use App\Subdomain;
use App\UserGroup;
use Image;
use Validator;
use File;
use Illuminate\Support\Facades\Auth;
use App\Clients;
use App\Orders;
use App\ClientStatuses;
use App\ClientTypes;
use App\Intermediary;
use App\Services\ClientPicture;
use App\User;
use App\Contacts;
use App\Order_logs;
use App\ContactFiles;
use GuzzleHttp\Query;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class ClientController extends Controller
{
    /**
     * @var Clients
     */
    private $clients;

    /**
     * @var Auth
     */
    private $auth;

    /**
     * ClientController constructor.
     * @param Clients $clients
     * @param Auth $auth
     */
    public function __construct(Clients $clients, Guard $auth)
    {
        $this->clients = $clients;
        $this->auth = $auth;
    }

    /**
     * Show list of clients
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = '';

        $query = $this->clients->with([
            'manager', 'account', 'type', 'contacts', 'groupOrders', 'ordersActive',
        ]);

        if ($request->has('search')) {
            $search = trim($request->input('search'));

            // @todo не делать таким образом на поиск продакшене, лучше заюзать Algolia
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%")
                    ->orWhere('tm_name', 'like', "%{$search}%")
                    ->orWhereHas('contacts', function ($query) use ($search) {
                        $query->where(function ($query) use ($search) {
                            $query->where('contacts.email', 'like', "%{$search}%")
                                ->orWhere('contacts.phone', 'like', "%{$search}%")
                                ->orWhere('contacts.name', 'like', "%{$search}%")
                                ->orWhere('contacts.last_name', 'like', "%{$search}%");
                        });
                    })
                    ->orWhereHas('type', function ($query) use ($search) {
                        $query->where(function ($query) use ($search) {
                            $query->where('client_types.name', 'like', "%{$search}%");
                        });
                    });
            });
        }

        // filter clients by subdomain
        $query->forSubdomain(Subdomain::findCurrent());

        if ($this->auth->user()->isManager()) {
            $query->where('manager_id', $this->auth->user()->id);
        }

        $query->whereNull('new_partner_id');

        $clients = $query->latest()->paginate(20);

        $page = [
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => [
                'Контрагенты',
                'Клиенты'
            ]
        ];

        if (\Request::ajax()) {
            return view('contractors.contractors.client_table', compact('clients'))->render();
        } else {
            return view('contractors.clients.index', compact('clients', 'page', 'search'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $page = array(
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => [
                'Контрагенты',
                'Клиенты'
            ]
        );
        return view('contractors.clients.new', ['page' => $page]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:clients,email,NULL,id,subdomain_id,' . $subdomain_id,
            'company_name' => 'required_if:is_company,1',
            'client_type' => 'required',
        ]);

        // Создаем клиента
        $client = new Clients();

        $client->tm_name = $request->input('company_name');
        $client->name = $request->input('name');
        $client->last_name = $request->input('lastname');

        if ($request->input('is_company')) {
            $client->is_company = true;
        }

        $clientTypeId = $request->client_type;

        // Посредник создаёт клиента
        if ($currentSubdomain = Subdomain::findCurrent()) {
            // Название типа клиента по умолчанию
            $clientTypeName = 'Новый клиент';

            if ($request->input('client_type') == 3) {
                $clientTypeName = 'Посредник';
            }

            // Т.к. у посредника свои типы клиентов, ищем нужный среди его типов
            $clientType = ClientTypes::where('name', $clientTypeName)
                ->where('subdomain_id', $currentSubdomain->id)
                ->first();

            $clientTypeId = $clientType->id;
        }

        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->type_id = $clientTypeId;
        $client->status_id = 3;
        $client->manager_id = Auth::user()->id;

        // Заказчик и посредник имеют роль "Пользователь"
        $group_id = UserGroup::CLIENT;
        $password_temp = str_random(6);
        $user = \App\Http\Controllers\UsersController::createAuto($group_id, $client->manager_id, $client->name, '', $client->phone, $client->email, $password_temp, '');
        $client->user_id = $user->id;
        $client->save();
        $user->password_temp = $password_temp;
        $user->save();

        // Создаем контакт
        $contact = new Contacts;
        $contact->user_id = $user->id;
        $contact->name = $request->name;
        $contact->last_name = $request->input('lastname');
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->is_company = $request->is_company;
        $contact->save();

        $position = $request->input('position') ?: 'Частное лицо';
        $contact->setInformationFieldForClient($client->id, 'position', $position);

        // Создаем связь
        $relation = new \App\ContactsClientsRelation;
        $relation->contact_id = $contact->id;
        $relation->client_id = $client->id;
        $relation->save();

        if ($request->input('action') === 'save_and_order') {
            return redirect('/orders/new-step1/' . $client->id);
        }

        return redirect('/client/show/' . $client->id);
    }

    /**
     * Show contact from specified company
     *
     * @param Contacts $contact
     * @param int $company_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showContacts(Contacts $contact, $company_id = 0)
    {
        $client = $contact->clients()->where('client_id', $company_id)->first();
        if (\Request::input('phone')) {
            $temp_connection = $client->connections;
            if (is_null($temp_connection)) {
                $temp_connection = [];
            }
            $add_phone = true;
            foreach ($temp_connection as $connection) {
                if ($connection['name'] == 'phone') {
                    if ($connection['value'] == \Request::input('phone')) {
                        $add_phone = false;
                        break;
                    }
                }
            }
            if ($add_phone) {
                $new_phone = new \stdClass();
                $new_phone->name = 'phone';
                $new_phone->value = \Request::input('phone');
                $new_phone->title = '\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430';
                array_push($temp_connection, $new_phone);
                $client->connections = $temp_connection;
                $client->save();
            }
        }

        $contacts = $client->contacts()->where('contact_id', '!=', $contact->id)->get();

        $contactClients = $contact->clients()->where('client_id', '!=', $company_id)->get();

        $waitingOrder = Orders::where('status', 3)->where('client_id', $client->id)->first();
        $order_pdfs = Orders::where('status', '>', 0)->where('client_id', $client->id)->where('price_details', '<>', '')
            ->get(['id', 'price', 'draw', 'price_details', 'last_invoice_pdf', 'need_new_invoice', 'updated_at']);
        foreach ($order_pdfs as $order_pdf) {
            $order_pdf->download = ((strlen($order_pdf->price_details['selected_product']) > 0) && (strlen($order_pdf->price_details['close_date']) > 0) && ($order_pdf->draw > 0) && ($order_pdf->price > 0));
        }

        $order_logs = Order_logs::query()->where('user_id', $client->id)->orderBy('id', 'desc')->get();
        //$relations = \App\ContactsRelations::query()->where('client_id', $id)->get();
        $relations = \App\ContactsClientsRelation::query()->where('client_id', $company_id)->get();

        //echo "<pre>"; print_r($relations); echo "</pre>";
        //$clients = \App\ContactsClientsRelation::query()->where('contact_id', $contact->id)->get();
        $clients = '';
        //$files = \App\ContactFiles::query()->where('contact_id', $contact->id)->get();
        $files = '';

        $managers = User::query()->whereIn('id_group', [2, 3])->where('id', '!=', $client->id)->get(['id', 'name']);

        switch ($client->type_id) {
            case 1:
                $client_types = ClientTypes::whereIn('id', [1, 2])->get();
                break;
            case 3:
                $client_types = ClientTypes::where('id', 3)->get();
                break;
            case 4:
                $client_types = ClientTypes::where('id', 4)->get();
                break;
            default:
                $client_types = ClientTypes::all();
                break;
        }

        $client_statuses = ClientStatuses::all();

        $page = [
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => [
                'Контрагенты',
                'Клиент'
            ]
        ];

        return view('contractors.clients.show', array_merge([
            'page' => $page,
            'client' => $client,
            'contacts' => $contacts,
            'contactClients' => $contactClients,
            'contact' => $contact,
            'waitingOrder' => $waitingOrder,
            'order_logs' => $order_logs,
            'order_pdfs' => $order_pdfs,
            'relations' => $relations,
            'clients' => $clients,
            'files' => $files,
            'managers' => $managers,
            'client_types' => $client_types,
            'client_statuses' => $client_statuses,
            'stats' => $this->getStatsForContact($contact, $client),
        ],
            $this->getAdditionalInfoByClient($client->id, $relations)));
    }

    public function show(Clients $client)
    {
        // @todo От этого метода нужно избавляться, основной метод для вывода карточки клиента - showContacts

        $contactRelation = \App\ContactsClientsRelation::query()->where('client_id', $client->id)->first();

        if ($client->contacts->count() === 0) {
            abort(404, 'Контакт не найден');
        }

        $contacts = $client->contacts;
        $contact = $contacts->shift();
        $contactClients = $contact->clients()->where('client_id', '!=', $client->id)->get();

        $waitingOrder = Orders::query()->where('status', 3)->where('client_id', $client->id)->first();
        $order_logs = Order_logs::query()->where('user_id', $client->id)->orderBy('id', 'desc')->get();
        $order_pdfs = Orders::query()->where('status', '>', 0)->where('client_id', $client->id)->where('price_details', '<>', '')
            ->get(['id', 'price', 'draw', 'price_details', 'last_invoice_pdf', 'need_new_invoice', 'updated_at']);
        foreach ($order_pdfs as $order_pdf) {
            (isset($order_pdf->price_details['name'])) ? $name = $order_pdf->price_details['name'] : $name = '';
            (isset($order_pdf->price_details['close_date'])) ? $close_date = $order_pdf->price_details['close_date'] : $close_date = '';
            (isset($order_pdf->price_details['draw'])) ? $draw = $order_pdf->price_details['draw'] : $draw = 0;
            (isset($order_pdf->price_details['price'])) ? $price = $order_pdf->price_details['price'] : $price = 0;
            if (strlen($name) > 0) {
                $order_pdf->download = ((strlen($name) > 0) && (strlen($close_date) > 0) && ($draw > 0) && ($price > 0));
            }
        }

        $relations = \App\ContactsRelations::query()->where('contact_id', $client->id)->get();
        $clients = \App\ContactsClientsRelation::query()->where('contact_id', $contact->id)->get();
        $files = \App\ContactFiles::query()->where('contact_id', $contact->id)->get();

        $managers = User::query()
            ->whereIn('id_group', [UserGroup::MANAGER, UserGroup::ADMIN])
            ->where('id', '!=', $client->id)
            ->get(['id', 'name']);

        switch ($client->type_id) {
            case 1:
                $client_types = ClientTypes::whereIn('id', [1, 2])->get();
                break;
            case 3:
                $client_types = ClientTypes::where('id', 3)->get();
                break;
            case 4:
                $client_types = ClientTypes::where('id', 4)->get();
                break;
            default:
                $client_types = ClientTypes::all();
                break;
        }

        $client_statuses = ClientStatuses::all();

        $page = [
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => ['Контрагенты', 'Клиент']
        ];

        return view('contractors.clients.show', array_merge([
            'page' => $page,
            'client' => $client,
            'contact' => $contact,
            'contacts' => $contacts,
            'waitingOrder' => $waitingOrder,
            'order_logs' => $order_logs,
            'order_pdfs' => $order_pdfs,
            'relations' => $relations,
            'clients' => $clients,
            'files' => $files,
            'managers' => $managers,
            'client_types' => $client_types,
            'client_statuses' => $client_statuses,
            'contactClients' => $contactClients,
            'stats' => $this->getStatsForContact($contact, $client),
        ],
            $this->getAdditionalInfoByClient($client->id, $relations)));
    }

    /**
     * Получает кол-во заказов клиента для контакта
     *
     * @param Contacts $contact
     * @param Clients $client
     * @return array
     */
    protected function getStatsForContact(Contacts $contact, Clients $client)
    {
        $stats = [];

        // На согласовании
        $stats['on_agreement_count'] = Orders::onAgreement()
            ->where('client_id', $client->id)
            ->whereHas('group', function ($query) use ($contact) {
                $query->where('contact_id', $contact->id);
            })
            ->count();

        // Требуют внимания!
        $stats['need_attention_count'] = Orders::requireAttention()
            ->where('client_id', $client->id)
            ->whereHas('group', function ($query) use ($contact) {
                $query->where('contact_id', $contact->id);
            })
            ->count();

        // В работе
        $stats['in_work_count'] = Orders::inWork()
            ->where('client_id', $client->id)
            ->whereHas('group', function ($query) use ($contact) {
                $query->where('contact_id', $contact->id);
            })
            ->count();

        // Готов к выдаче
        $stats['ready_count'] = Orders::ready()
            ->where('client_id', $client->id)
            ->whereHas('group', function ($query) use ($contact) {
                $query->where('contact_id', $contact->id);
            })
            ->count();

        // Ожидает оплаты
        $stats['waiting_payment_count'] = GroupOrders::typographyWaitingPayment()
            ->where('client_id', $client->id)
            ->where('contact_id', $contact->id)
            ->count();

        return $stats;
    }

    public function getAdditionalInfoByClient($id)
    {
        $orders_info = Orders::query()->where('status', 3)->where('client_id', $id)->get(['id', 'draw', 'price', 'pay', 'status', 'price_details']);
        return [
            'orders_info' => $orders_info,
        ];
    }

    public function getClient(Request $request, Clients $client)
    {
        $response = array('status' => 'fail');
        if (is_object($client)) {
            $response = array('status' => 'ok', 'data' => array(
                'name' => $client->name,
                'email' => $client->email,
                'phone' => $client->phone,
                'area' => $client->area,
                'comment' => $client->description
            ));
        }
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

    }

    public function saveContact(Request $request, Contacts $contact)
    {
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:contacts,email,' . $contact->id . ',id,subdomain_id,' . $subdomain_id,
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error'
            ]);
        }

        $contact->update($request->all());

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');

            $name = md5(uniqid());

            $avatar = $avatar->move(public_path('upload/users'), $name . '.' . $avatar->getClientOriginalExtension());

            $contact->avatar = "/upload/users" . $avatar->getFilename();
            $contact->save();
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function saveClient(Request $request, Clients $client)
    {
        $response = array("status" => "fail");
        if (is_object($client)) {
            $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:clients,email,' . $client->id . ',id,subdomain_id,' . $subdomain_id,
            ]);
            if (!$validator->fails()) {
                $client->name = isset($request->name) ? $request->name : $client->name;
                $client->email = isset($request->email) ? $request->email : $client->email;
                $client->phone = isset($request->phone) ? $request->phone : $client->phone;
                $client->area = isset($request->area) ? $request->area : $client->area;
                $client->type_id = $request->client_type;
                $client->status_id = $request->client_status;
                $client->description = isset($request->comment) ? $request->comment : $client->description;
                if ($client->save()) {
                    $response = array('status' => 'ok');
                }
            }
        }
        return response()->json($response);
    }

    public function saveDetails(Request $request, Clients $client)
    {
        $response = array("status" => "fail");
        if ($client->company_details) {
            $correctData = $client->company_details;
        } else {
            $correctData = [];
        }
        $substr_name = substr($request['name'],8); //Отрезаем details_ - 8 символов
        //Разбиваем значения на первое слово и второе слово
        if ($substr_name == 'name') {
            $client->ownership_type = substr($request['value'],0, strpos($request['value'],' '));
            $client->legal_name = substr($request['value'],strpos($request['value'],' '));
        }
        if ($substr_name == 'post_address') {
            $client->address = $request['value'];
        }
        $correctData[$substr_name] = $request['value'];
        $correctData['all_ok'] = (isset($correctData['name']) && isset($correctData['bank_name']) && isset($correctData['bank_rs']));
        $client->company_details = $correctData;
        if ($client->save()) {
            $response = array('status' => 'ok', 'parameter' => $substr_name);
        }
        return response()->json($response);

    }

    public function resizeImageAndSave($file, $upload_folder)
    {
        if (!file_exists($upload_folder)) {
            mkdir($upload_folder, 0777, true);
        }

        list($type, $file) = explode(';', $file);
        list(, $file) = explode(',', $file);

        $file = base64_decode($file);

        $name = md5(time() . rand(1000, 9999));
        $previewName = $name . '.png';
        $preview = $upload_folder . $previewName;
        file_put_contents($preview, $file);

        File::delete($file);

        return $previewName;
    }

    /**
     * Форма создания нового контакта для клиента
     *
     * @param Request $request
     * @param Clients $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function newContact(Request $request, Clients $client)
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => [
                'Контрагенты',
                'Новый контакт'
            ]
        ];

        if (!$client->isCompany()) {
            // @todo Add flash messages

            return redirect()->back();
        }

        $companies = $client->contacts->first()->clients()->where('is_company', true)->get();

        return view('contractors.clients.new_contact', [
            'page' => $page,
            'company' => $client,
            'companies' => $companies,
        ]);
    }

    /**
     * Add contact to client
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeContact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'client_id' => 'required|exists:clients,id',
            'email' => 'required|email',
            //'email' => 'required|email|unique:contacts,email',
            'phone' => 'required',
        ]);

        $client = Clients::findOrFail($request->input('client_id'));

        $newContact = $client->contacts()->create(
            $request->all()
        );

        if ($request->input('position')) {
            $newContact->setInformationFieldForClient($client->id, 'position', $request->input('position'));
        }

        return redirect('/contact/show/' . $newContact->id . '/' . $client->id);
    }

    /**
     * Create new client-company for the contact page
     *
     * @param Contacts $contact
     * @param Clients $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newClient(Contacts $contact, Clients $client)
    {
        $page = [
            'menu' => 'contractors',
            'submenu' => 'clients',
            'breadcrumbs' => [
                'Контрагенты',
                'Новая компания'
            ]
        ];

        return view('contractors.clients.new_client', compact('page', 'contafuct', 'client', 'contact'));
    }

    /**
     * Store new company for the contact
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeClient(Request $request)
    {
        $this->validate($request, [
            'tm_name' => 'required|min:3|max:50',
            'contact_id' => 'required|exists:contacts,id',
            'client_id' => 'required|exists:clients,id',
            'phone' => 'required',
        ]);

        // find contact
        $contact = Contacts::findOrFail($request->input('contact_id'));

        // find client
        $contactClient = Clients::findOrFail($request->input('client_id'));

        // create new client-company
        $client = new Clients();
        $client->user_id = $contact->user_id;
        $client->email = $contact->email;
        $client->tm_name = $request->input('tm_name');
        $client->name = $contact->name;
        $client->last_name = $contact->last_name;
        $client->is_company = true;
        $client->phone = $contact->phone;
        $client->type_id = $contactClient->type_id;
        $client->status_id = $contactClient->status_id;
        $client->manager_id = auth()->user()->id;
        $client->save();

        // Attach new client to contact
        $contact->clients()->attach($client->id);

        // position for contact in client-company
        if ($request->has('position')) {
            $contact->setInformationFieldForClient($client->id, 'position', $request->input('position'));
        }

        return redirect('/contact/show/' . $contact->id . '/' . $client->id);
    }

    public function uploadFile(Request $request)
    {
        if (isset($_FILES['document']) && $_FILES['document']) {
            $filename = $_FILES['document']['name'];
            $arr = explode(".", $filename);
            $cryptedFileName = md5(time() . $filename) . "." . end($arr);
            $uploadto = $_SERVER['DOCUMENT_ROOT'] . '/upload/documents/' . $cryptedFileName;
            if (move_uploaded_file($_FILES['document']['tmp_name'], $uploadto)) {
                $file = newContactFiles();
                $file->contact_id = $request->contact_id;
                $file->path = $uploadto;
                $file->name = $filename;
                $file->save();
            }
        }
        return redirect('/client/show/' . $request->contact_id);
    }

    public function deleteFile(Request $request, ContactFiles $file)
    {
        $response = array('status' => 'fail');
        if (is_object($file)) {
            $contact = Contacts::find($file->contact_id);
            if (is_object($contact) && Auth::user()->id == $contact->user_id) {
                if ($file->delete()) {
                    unlink($file->path);
                    $response = array('status' => 'ok');
                }
            }
        }
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Clients $client
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Clients $client)
    {
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:clients,email,' . $client->id . ',id,subdomain_id,' . $subdomain_id,
            //'email' => 'required|email|unique:clients,email,' . $client->id,
        ]);
        if ($validator->fails()) {
            return redirect('/client/show/' . $client->id)->withErrors($validator)->withInput();
        }

        $client->name = $request->name;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->save();

        SettingsValidator::updateValidationFlag();

        return redirect('/client/show/' . $client->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Clients $client
     * @return \Illuminate\Http\Response
     */

    public function destroy(Clients $client)
    {
        //
    }

    /**
     * fastSave description
     *
     * @param Request $request
     * @param Clients $client
     */

    public function saveDescription(Request $request, Clients $client)
    {
        if ($client && $request->input('name_filed') == 'description-field') {
            $client->description = $request->input('description-field');
            $client->save();
        }
    }

    public function changeManagerAjax(Clients $client, $manager_id)
    {
        if ($client->manager_id != $manager_id) {
            $client->manager_id = $manager_id;
            $client->save();
        }
    }

}
