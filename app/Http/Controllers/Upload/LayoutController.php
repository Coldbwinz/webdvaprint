<?php

namespace App\Http\Controllers\Upload;

use App\Classes\FPDI\FPDIx;
use App\Emails\LayoutApprovalCustomer;
use App\Emails\SendInvoiceNotification;
use App\Http\Controllers\GroupOrdersController;
use App\Jobs\ParseDesignerPsd;
use App\OrderElement;
use Illuminate\Support\Arr;
use PDF;
use DB;
use Auth;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Orders;
use App\ReadyUploadProduct;
use App\Specification;
use App\SpecificationHistory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Classes\Pdf\PdfPreviewMaker;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class LayoutController extends Controller
{

    /**
     * [start description]
     *
     * @param  integer $order_id
     * @return Illuminate\Http\Response
     */
    public function start($order_id)
    {
        $order = Orders::find($order_id);

        $complect = $order->complect;

        return view('upload.ready_layout.start', [
            'page' => 1,
            'complect' => $complect,
            'order' => $order,
            'manager' => $order->manager(),
            'id_order' => $order_id,
        ]);
    }

    /**
     * Страница формирования технического задания
     *
     * @param $order_id
     * @return Illuminate\Http\Response
     */
    public function index($order_id, $type)
    {
        $user = Auth::user();
        $order = Orders::findOrFail($order_id);

        if ($user->isClient() && $order->client->account->id !== $user->id) {
            abort(404, 'Permission denied');
        }

        $readyProducts = $order->readyProducts()->where('type', $type)->orderBy('position')->get();

        $acceptedFileTypes = '.pdf,.psd';

        return view('upload.ready_layout.index', [
            'order' => $order,
            'type' => $type,
            'readyProducts' => $readyProducts,
            'filesCount' => Arr::get($order->price_details, "print_structure.{$type}.count", 1),
            'acceptedFileTypes' => $acceptedFileTypes,
        ]);
    }

    /**
     * [sort description]
     *
     * @param  Request $request
     * @param  integer  $order_id
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request, $order_id, $type)
    {
        foreach ($request->sort as $position => $id) {
            $file = ReadyUploadProduct::find($id);
            if ($file) {
                $file->update(['position' => $position]);
            }
        }

        return response()->json(['success' => true]);
    }

    /**
     * [delete description]
     *
     * @param  integer $order_id
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete($order_id, $type, $id)
    {
        $order = Orders::findOrFail($order_id);

        $file = ReadyUploadProduct::find($id);
        if ($file) {
            // delete file
            $filePath = public_path($file->path);
            if (file_exists($filePath)) {
                File::delete($filePath);
            }

            // delete preview
            $previewName = 'thumb_'.pathinfo($file->filename, PATHINFO_FILENAME).'jpg';
            $previewPath = public_path(str_replace($file->filename, $previewName, $file->path));
            if (file_exists($previewPath)) {
                File::delete($previewPath);
            }

            // delete record
            $file->delete();

            // Пересчёт позиций продуктов
            $products = $order->readyProducts()->where('type', $type)->orderBy('position')->get();
            foreach ($products as $key => $product) {
                $product->position = $key;
                $product->save();
            }
        }

        return response()->json(['success' => true]);
    }

    /**
     * Загрузка файлов
     *
     * @param Request $request
     * @param Orders $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request, $order_id, $type)
    {
        $order = Orders::findOrFail($order_id);

        if (! $request->hasFile('file')) {
            return response()->json(['files' => []]);
        }

        $file = $request->file('file');

        $firstMimeType = $order->readyProducts()->orderBy('position')->value('mime_type');

        if ($firstMimeType && $file->getMimeType() !== $firstMimeType) {
            return response()->json([
                'files' => [],
                'error' => 'Выберите файлы одного формата: ' . $firstMimeType,
            ]);
        }

        // PDF format (upload and split if has many pages)
        if ($file->getMimeType() == 'application/pdf') {
            $filename = $file->getRealPath();

            // parse PDF
            $pdf = new FPDIx();
            $pagecount = $pdf->setSourceFile($filename); // How many pages?

            // if many pages
            if ($pagecount > 1) {
            	$files = $this->uploadPDF($pagecount, $file, $order, $type);

                // return response many files
                return response()->json(compact('files'));
            }
        }

        // Upload other formats (PSD this / One page PDF)
        $files = [
            $this->uploadFile($file, $order, $type)
        ];

        return response()->json(compact('files'));
    }

    /**
     * Upload PDF (split by pages)
     *
     * @param  integer  $pagecount
     * @param  File     $file
     * @param  Orders   $order [description]
     * @param  string   $type
     * @return array
     */
    protected function uploadPDF($pagecount, $file, $order, $type)
    {
        $files = [];

        $filename = $file->getRealPath();
        $originalName = $file->getClientOriginalName();

        // get last position
        $position = $this->getLastPosition($order, $type);

        // Split each page into a new PDF
        for ($i = 1; $i <= $pagecount; $i++) {
            // set params
            $basename = str_random();
            $uploadPath = 'upload/ready-products/'.substr(strtolower($basename), 0, 2);
            $newFileName = $basename.'.'.$file->getClientOriginalExtension();
            $relativePath = '/'. $uploadPath.'/'.$newFileName;
            $previewPath = $uploadPath.'/thumb_'.$basename.'.jpg';
            rmkdir(public_path($uploadPath));

            // create PDF object
            $new_pdf = new FPDIx();
            $new_pdf->setSourceFile($filename);

            $tplidx = $new_pdf->ImportPage($i);
            $size = $new_pdf->getTemplateSize($tplidx);
            $width = $size['w'];
            $height = $size['h'];
            $orientation = $width > $height ? 'Landscape' : 'Portrait';
            $new_pdf->AddPage($orientation, [$width, $height]);
            $new_pdf->useTemplate($tplidx);

            // save new pdf
            $output = public_path(trim($relativePath, '/'));
            $new_pdf->Output($output, "F");

            // make thumb
            $thumbnailImage = $this->makePreview($relativePath, $previewPath);

            $readyProduct = ReadyUploadProduct::create([
                'user_id' => $order->client->account->id,
                'order_id' => $order->id,
                'order_element_id' => $order->elements()->where('type', $type)->firstOrFail()->id,
                'type' => $type,
                'position' => $position,
                'filename' => $newFileName,
                'path' => $relativePath,
                'url' => $relativePath,
                'preview' => $thumbnailImage,
                'original_name' => $originalName,
                'mime_type' => $file->getMimeType(),
                'size' => $file->getSize(),
            ]);

            // increment position
            $position++;

            // add files struct
            $files[] = $this->getFileStruct($readyProduct, $order, $type);
        }

        return $files;
    }

    /**
     * Upload other formats (PSD this)
     *
     * @param  UploadedFile $file
     * @param  Orders $order
     * @param  string $type
     * @return array
     */
    protected function uploadFile($file, $order, $type)
    {
        $basename = str_random();
        $originalName = $file->getClientOriginalName();
        $uploadPath = 'upload/ready-products/'.substr(strtolower($basename), 0, 2);
        $newFileName = $basename.'.'.$file->getClientOriginalExtension();
        $relativePath = '/'. $uploadPath.'/'.$newFileName;
        $previewPath = $uploadPath.'/thumb_'.$basename.'.jpg';
        rmkdir(public_path($uploadPath));

        $file = $file->move(public_path($uploadPath), $newFileName);

        // make thumb
        $thumbnailImage = $this->makePreview($relativePath, $previewPath);

        // get last position
        $position = $this->getLastPosition($order, $type);

        $readyProduct = ReadyUploadProduct::create([
            'user_id' => $order->client->account->id,
            'order_id' => $order->id,
            'order_element_id' => $order->elements()->where('type', $type)->firstOrFail()->id,
            'type' => $type,
            'position' => $position,
            'filename' => $newFileName,
            'path' => $relativePath,
            'url' => $relativePath,
            'preview' => $thumbnailImage,
            'original_name' => $originalName,
            'mime_type' => $file->getMimeType(),
            'size' => $file->getSize(),
        ]);

        return $this->getFileStruct($readyProduct, $order, $type);
    }

    /**
     * [getFileStruct description]
     *
     * @param  ReadyUploadProduct $readyProduct
     * @param  Orders             $order
     * @param  string             $type
     * @return array
     */
    protected function getFileStruct(ReadyUploadProduct $readyProduct, Orders $order, $type)
    {
        return [
            'id' => $readyProduct->id,
            'url' => $readyProduct->url,
            'thumbnail_url' => $readyProduct->preview,
            'name' => $readyProduct->filename,
            'type' => $readyProduct->mime_type,
            'size' => $readyProduct->size,
            'delete_url' => route('order.upload.layout.files.delete', ['order_id' => $order->id, 'type' => $type, 'id' => $readyProduct->id]),
            'delete_type' => 'POST',
        ];
    }

    /**
     * [makePreview description]
     *
     * @return string
     */
    protected function makePreview($src, $dest)
    {
        // Make thumb
        //try{
            PdfPreviewMaker::make(public_path(trim($src, DIRECTORY_SEPARATOR)), public_path(trim($dest, DIRECTORY_SEPARATOR)));
            $preview = asset($dest);
        // }
        // catch(\Exception $e) {
        //     $preview = '';
        // }
        return $preview;
    }

    /**
     * [getLastPosition description]
     *
     * @param  Orders $order
     * @return integer
     */
    protected function getLastPosition(Orders $order, $type)
    {
        $last = $order->readyProducts()->where('type', $type)->orderBy('position', 'desc')->first();
        return !is_null($last) ? $last->position + 1 : 1;
    }

    /**
     * [splitPdf description]
     *
     * @param  string $filename
     * @param  string $outputPath
     * @return string
     */
    protected function splitPdf($filename, $outputPath, $destFilename)
    {
        $pdf = new FPDIx();
        $pages = $pdf->splitPages($filename);
        foreach ($pages as $index => $page) {
            $basename = str_random();
            $uploadPath = 'upload/ready-products/'.substr(strtolower($basename), 0, 2);
            $newFileName = $basename.'.'.$file->getClientOriginalExtension();
            $output = public_path(trim($outputPath, '/') . DIRECTORY_SEPARATOR . $destFilename);
            $page->Output($output, "F");

            $result[] = $output;
        }
        $pdf->close();

        return $result;
    }

    /**
     * Check for elements without uploaded templates
     *
     * @param int $orderId
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkReady($orderId)
    {
        $order = Orders::findOrFail($orderId);

        $complectOrder = $order->complectOrder;

        if ($complectOrder) {
            foreach ($complectOrder->complect as $order) {
                $elements = $order->elements()->where('printable', true)->get();

                foreach ($elements as $element) {
                    $needCount = $order->price_details['print_structure'][$element->type]['count'];

                    if ($needCount != $element->templates->count()) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Загружены не все шаблоны.',
                            'complect_id' => $order->complect_id,
                        ]);
                    }
                }
            }
        }

        $elementsWithoutTemplates = $order->elements()
            ->printable()
            ->doesntHave('templates')
            ->get();

        return response()->json([
            'elements' => $elementsWithoutTemplates,
        ]);
    }

    public function finish($orderId)
    {
        $complect = Orders::findOrFail($orderId)->complectOrder->complect;

        foreach ($complect as $order) {
            // Превью для заказа
            try {
                $sortingOrder = ['cover', 'body', 'pad'];
                $product = $order->readyProducts()->orderBy('position')->get()->sortBy(function ($product) use ($sortingOrder) {
                    return array_search($product->type, $sortingOrder);
                })->first();

                $order->img = $product->preview;
                $order->pdf = $product->path;
                $order->save();

                // Превью для элементов
                foreach ($order->elements as $element) {
                    $preview = $order->readyProducts()->where('type', $element->type)->orderBy('position')->first()->preview;
                    $element->preview = $preview;
                    $element->save();
                }
            } catch (\Exception $exception) {}

            $order->designerStopWorking();

            // Совместная работа с дизайнером
            if ($order->status == status('self_designer', 'id')) {
                
                $temp_price_details = $order->price_details;
                $temp_price_details['template_cost'] += round(($order->designer_working_time / 3600) *
                    (isset($temp_price_details['template_cost_hour']) ? $temp_price_details['template_cost_hour'] : 0), 0);
                $order->price_details = $temp_price_details;
                $order->price += $order->price_details['template_cost'];

                $elements = $order->elements;
                $elementTemplateMimeType = $elements->first()->templates->first()->mime_type;

                if ($elementTemplateMimeType === 'application/pdf') {
                    foreach ($elements as $element) {
                        $pdf = new FPDIx();
                        $elementPdfFiles = $element->templates->map(function ($template) {
                            return public_path($template->path);
                        });
                        $pdf->concat($elementPdfFiles->toArray());
                        $pdfFile = "element-{$element->id}-".str_random(10).'.pdf';
                        $uploadPath = 'upload/elements/pdf_templates';
                        $uploadFullPath = public_path($uploadPath);
                        rmkdir($uploadFullPath);
                        $outputPdf = $uploadFullPath . DIRECTORY_SEPARATOR . $pdfFile;
                        $pdf->Output($outputPdf, "F");
                        $element->pdf_path = $uploadPath . DIRECTORY_SEPARATOR . $pdfFile;
                        $element->save();
                    }

                    // Generate pdf for order
                    $pdf = new FPDIx();
                    $pdfFiles = $elements->map(function ($element) {
                        return public_path($element->pdf_path);
                    });
                    $pdf->concat($pdfFiles->toArray());
                    $pdfFile = "order-{$order->id}-".str_random(10).'.pdf';
                    $pdf->Output(public_path('lib/frx/'.$pdfFile), "F");
                    $order->pdf = $pdfFile;
                    $order->status = status('layout_approved', 'id');
                    $order->save();
                    if ($order->group->next == 0) {
                        if ($order->group->orders()->whereIn('status', [3, 4, 5, 9, 301])->where('id', '<>', $order->id)->count() == 0) {
                            if (auth()->user()->accessLevel() == 1 || $order->group->client()->first()->getDetailsOK() == 0) {
                                (new SendInvoiceNotification)->send($order);
                            }
                        }
                    }

                    \App\Template::makeFromOrder($order);

                } elseif ($elementTemplateMimeType === 'image/vnd.adobe.photoshop') {
                    // Запуск всех загруженных макетов на парсинг
                    $order->status = status('parsing', 'id');
                    $order->save();
                    $order->readyProducts->each(function (ReadyUploadProduct $product) {
                        $job = (new ParseDesignerPsd($product));
                        $this->dispatch($job);
                    });
                }
                (new GroupOrdersController)->recalculateGroupPrice($order->group->id);

                addLog($order->id, 'Макет утверждён', $order->group->client->user_id, 'Работа дизайнера: '.round($order->designer_working_time / 3600,2).' ч.', null, 0, "в присутствии дизайнера");
            }

            // Разработка макета то ТЗ
            if ($order->status == status('exclusive_design', 'id')) {

                $elementTemplateMimeType = $order->readyProducts->first()->mime_type;

                if ($elementTemplateMimeType === 'application/pdf') {

                    foreach ($order->elements as $element) {
                        $pdf = new FPDIx();
                        $elementPdfFiles = $element->templates->map(function ($template) {
                            return public_path($template->path);
                        });
                        $pdf->concat($elementPdfFiles->toArray());
                        $pdfFile = "element-{$element->id}-".str_random(10).'.pdf';
                        $uploadPath = 'upload/elements/pdf_templates';
                        $uploadFullPath = public_path($uploadPath);
                        rmkdir($uploadFullPath);
                        $outputPdf = $uploadFullPath . DIRECTORY_SEPARATOR . $pdfFile;
                        $pdf->Output($outputPdf, "F");
                        $element->pdf_path = $uploadPath . DIRECTORY_SEPARATOR . $pdfFile;
                        $element->save();
                    }

                    // Generate pdf for order
                    $pdf = new FPDIx();
                    $pdfFiles = $order->elements->map(function ($element) {
                        return public_path($element->pdf_path);
                    });
                    $pdf->concat($pdfFiles->toArray());
                    $pdfFile = "order-{$order->id}-".str_random(10).'.pdf';
                    $pdf->Output(public_path('lib/frx/'.$pdfFile), "F");
                    $order->pdf = $pdfFile;
                    $order->save();

                    $order->status = status('sent_for_approval', 'id');
                    $order->save();
                    // Отправить письмо клиенту со ссылкой для утверждения макета
                    app(LayoutApprovalCustomer::class)->send($order);

                    $groupOrder = $order->group;
                    $groupOrder->invoice_send = 1;
                    $groupOrder->save();

                    \App\Template::makeFromOrder($order);

                } elseif ($elementTemplateMimeType === 'image/vnd.adobe.photoshop') {
                    // Запуск всех загруженных макетов на парсинг
                    $order->status = status('prepares_agreement', 'id');
                    $order->save();
                    $order->readyProducts->each(function (ReadyUploadProduct $product) {
                        $job = (new ParseDesignerPsd($product));
                        $this->dispatch($job);
                    });
                }
            }
        }

        return response()->json([
            'redirect' => '/orders/history',
        ]);
    }
}
