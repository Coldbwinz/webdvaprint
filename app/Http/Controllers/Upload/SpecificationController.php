<?php

namespace App\Http\Controllers\Upload;

use DB;
use Auth;
use App\Orders;
use App\AttachmentFile;
use App\Specification;
use App\SpecificationHistory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Classes\Pdf\PdfPreviewMaker;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class SpecificationController extends Controller
{

    /**
     * Страница формирования технического задания
     *
     * @param $order_id
     * @return Illuminate\Http\Response
     */
    public function index($order_id)
    {
        $user = Auth::user();
        $order = Orders::findOrFail($order_id);

        if ($user->isClient() && $order->client->account->id !== $user->id) {
            abort(404, 'Permission denied');
        }

        $specification = $order->specification()->first();
        if ($specification) {
            $action = 'edit';
            $attachments = $specification->attachments()->get();
            $links = !empty($specification->links) ? json_decode($specification->links) : [];
            $description = $specification->description;
        }
        else {
            $action = 'create';
            $attachments = [];
            $links = [];
            $description = null;
        }

        return view('upload.specification.index', [
            'action' => $action,
            'order' => $order,
            'attachments' => $attachments,
            'links' => $links,
            'description' => $description,
            'company_id' => $order->group->client->id,
            'company_details' => $order->group->client()->first()->getDetailsOK(),
            'complects_count' => ($order->complect_id) ? Orders::where('complect_id', $order->complect_id)->count() : null,
        ]);
    }

    /**
     * [show description]
     *
     * @param  $order_id
     * @param  $version
     * @return Illuminate\Http\Response
     */
    public function show($order_id, $version_id = null)
    {
        $user = Auth::user();
        $order = Orders::findOrFail($order_id);
        if (!is_null($version_id)) {
            $version = SpecificationHistory::findOrFail($version_id);
        }
        elseif ($order->specification) {
            $version = $order->specification->history()->latest()->take(1)->get()->first();
        }
        else {
            abort(404);
        }

        $specification = unserialize($version->data);

        $links = json_decode($specification->links, true);
        $attachmentIds = json_decode($specification->attachment_ids, true);
        $attachments = $specification->attachments()->whereIn('id', $attachmentIds)->get();

        // Set specification view flag
        $order->specification->viewed = true;
        $order->specification->save();

        return view('upload.specification.show', [
            'order' => $order,
            'attachments' => $attachments,
            'links' => $links,
            'description' => $specification->description,
            'company_id' => $order->group->client->id,
            'company_details' => $order->group->client()->first()->getDetailsOK(),
            'complects_count' => ($order->complect_id) ? Orders::where('complect_id', $order->complect_id)->count() : null,
        ]);
    }

    /**
     * [store description]
     *
     * @param  Request $request
     * @param  integer  $order_id
     * @return Illuminate\Http\Response
     */
    public function store(Request $request, $order_id, $assign_to_one)
    {
        $this->validate($request, [
            'description' => 'required',
        ]);

        $user = Auth::user();

        // links
        $links = [];
        if ($request->has('links')) {
            foreach ($request->links as $link) {
                if (!empty($link)) {
                    $links[] = $link;
                }
            }
        }

        // uploaded_files
        $attachmentIds = $request->has('uploaded_files') ? $request->uploaded_files : [];
        $attachments = AttachmentFile::whereIn('id', $request->uploaded_files)->get();

        // specification
        $order = Orders::findOrFail($order_id);
        DB::beginTransaction();
        if ($order->complect_id && ($assign_to_one == 0)) {
            $complect_orders = Orders::where('complect_id', $order->complect_id)->get();
        } else {
            $complect_orders = Orders::where('id', $order_id)->get();
        }
        foreach ($complect_orders as $order) {
            if ($assign_to_one == 1) {
                $order->complect_id = $order->id;
            }
            $specification = $order->specification()->first();
            if ($order->id != $order_id) {
                if ($specification) {
                    $specification->attachments()->delete();
                }
                $newAttachmentsIds = [];
                foreach ($attachments as $file) {
                    $file = $file->replicate();
                    $file->order_id = $order->id;
                    $file->save();
                    array_push($newAttachmentsIds, $file->id);
                }
                $newAttachments = AttachmentFile::where('order_id', $order->id)->get();
            } else {
                $newAttachments = $attachments;
                $newAttachmentsIds = $attachmentIds;
            }
            // CREATE
            if (!$specification) {
                $specification = Specification::create([
                    'order_id' => $order->id,
                    'user_id' => $user->id,
                    'description' => $request->description,
                    'links' => json_encode($links),
                    'attachment_ids' => json_encode($newAttachmentsIds),
                ]);

                $specification->attachments()->saveMany($newAttachments);

                // set order flag
                $order->use_specification = true;
                $order->save();

                $position = 1;

                $this->saveHistory('ТЗ сформировано', $order, $specification, $position);
            } else { // UPDATE
                $specification->attachments()->saveMany($newAttachments);

                $linksJSON = json_encode($links);
                $attachmentIdsJSON = json_encode($newAttachmentsIds);

                // make comment for history
                $fixes = [];
                if ($request->description !== $specification->description) {
                    $fixes[] = 'текст ТЗ';
                }
                if ($attachmentIdsJSON !== $specification->attachment_ids) {
                    $fixes[] = 'файлы';
                }
                if ($linksJSON !== $specification->links) {
                    $fixes[] = 'ссылки';
                }
                $commentary = 'Изменения: ' . implode(', ', $fixes);

                // update
                if (!empty($fixes)) {
                    $specification->update([
                        'description' => $request->description,
                        'links' => $linksJSON,
                        'attachment_ids' => $attachmentIdsJSON,
                    ]);

                    $lastVersion = $specification->history()->latest()->take(1)->get()->first();
                    $position = $lastVersion->position + 1;

                    $this->saveHistory('ТЗ отредактировано', $order, $specification, $position, $commentary);
                }

                // set order flag
                $order->edit_specification = true;
                $order->save();

                // specification designer viewed flag
                $specification->viewed = false;
                $specification->save();
            }
        }
        DB::commit();

        // redirect
        if ($order->group->next == 1) {
            return 1;
        }
        else if ($user->accessLevel() == 1) {
            $redirect = '/orders/need_attention';
        }
        else {
            $redirect = '/orders/history/';
        }

        return response()->json(compact('redirect'));
    }

    /**
     * [saveHistory description]
     *
     * @param  string        $caption
     * @param  Orders        $order
     * @param  Specification $specification
     * @param  integer       $position
     * @return void
     */
    protected function saveHistory($caption, Orders $order, Specification $specification, $position, $commentary = '')
    {
        // save version
        $version = SpecificationHistory::create([
            'specification_id' => $specification->id,
            'data' => serialize($specification),
            'position' => $position,
        ]);

        // add log
        $value = '<a href="'.route('order.specification.show', [$order->id, $version->id]).'">ТЗ сформировано</a>';
        addLog($order->id, 'Заказан макет дизайнеру', auth()->user()->id, $value, null, 0, $commentary);
    }

    /**
     * Загрузка файлов
     *
     * @param Request $request
     * @param Orders $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request, $order_id)
    {
        $order = Orders::findOrFail($order_id);

        if (!$request->hasFile('file')) {
            return response()->json(['files' => []]);
        }

        $file = $request->file('file');

        $originalName = $file->getClientOriginalName();

        $basename = str_random();
        $uploadPath = 'upload/technical-task-files/'.substr(strtolower($basename), 0, 2);
        $newFileName = $basename.'.'.$file->getClientOriginalExtension();
        $relativePath = '/'. $uploadPath.'/'.$newFileName;
        $previewPath = $uploadPath.'/thumb_'.$basename.'.jpg';

        $file = $file->move(public_path($uploadPath), $newFileName);

        // Make thumb
        try{
            PdfPreviewMaker::make(public_path($relativePath), public_path($previewPath));
            $thumbnailImage = asset($previewPath);
        }
        catch(\Exception $e) {
            $thumbnailImage = '';
        }

        $attachment = AttachmentFile::create([
            'user_id' => $order->client->account->id,
            'order_id' => $order->id,
            'type' => null, // @todo
            'filename' => $newFileName,
            'path' => $relativePath,
            'url' => $relativePath,
            'preview' => $thumbnailImage,
            'original_name' => $originalName,
            'mime_type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'title' => $file->getFileName(),
        ]);

        return response()->json([
            'files' => [
                [
                    'id' => $attachment->id,
                    'url' => asset($uploadPath.'/'.$newFileName),
                    'thumbnail_url' => $thumbnailImage,
                    'name' => $newFileName,
                    'type' => $file->getMimeType(),
                    'size' => $file->getSize(),
                    'delete_url' => route('order.specification.files.delete', ['order_id' => $order->id, 'id' => $attachment->id]),
                    'delete_type' => 'POST',
                ],
            ]
        ]);
    }

    /**
     * [delete description]
     *
     * @param  integer $order_id
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function delete($order_id, $id)
    {
        $order = Orders::findOrFail($order_id);

        $file = AttachmentFile::find($id);
        if ($file) {
            $file->delete();
        }

        return response()->json(['success' => true]);
    }

    /**
     * Создание тех задания для заказа
     *
     * @param $orderId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function makeForOrder($orderId)
    {
        $order = Orders::findOrFail($orderId);

        Specification::makeFromOrder($order);

        if ($order->group->next == 1) {
            return 1;
        } else {
            return 0;
        }
    }
}
