<?php

namespace App\Http\Controllers;

use App\ParserFiles;
use App\TemplateLibrary;
use Illuminate\Http\Request;
use Response;
use DB;
use Auth;
use File;
use Validator;
use App\Template;
use App\ProductType;
use App\Clients;
use App\Theme;
use App\Classes\Parser\RubyParser;
use App\Classes\Parser\Frx;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Index
        $templates = Template::where('activity',true)
                                ->where(function($query) {
                                    return $query->where('status','=','parsed')->orWhere('status','=',null);
                                })
                                ->paginate(20);

        $product_types = DB::table('product_types')->where('activity', 1)->get();
        $themes = DB::table('themes')->where('activity', 1)->get();
        foreach ($templates as $key => $value) {
            $type_info = \DB::table('product_types')->find($value->type);
            if ($type_info) {
                $templates[$key]->type_name = $type_info->name;
            }
        }

        $breadcrumbs[] = 'Справочник';
        $breadcrumbs[] = 'Шаблоны';
        $page = array(
            'menu' => 'catalogs',
            'submenu' => 'template',
            'breadcrumbs' => $breadcrumbs
        );

        return view('templates.index', [
            'page' => $page,
            'templates' => $templates,
            'product_types' => $product_types,
            'themes' => $themes,
            'errors' => isset($errors) ? $errors : [],
        ]);
    }

    /**
     * Route /template/new/{client_id}
     *
     * @param Request $request
     * @param type $client_id
     * @return \Illuminate\Http\Response
     */

    public function create($client_id, $type_id)
    {
        $client = Clients::find($client_id);
        if (!$client) {
            abort(404, 'Клиент не найден');
        }

        $type = ProductType::find($type_id);
        if (!$client) {
            abort(404, 'Тип продукции не найден');
        }

        $breadcrumbs[] = 'Шаблоны';
        $breadcrumbs[] = 'Добавить';
        $page = array(
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => $breadcrumbs
        );

        return view('templates.new', [
            'page' => $page,
            'client_id' => $client_id,
            'type_id' => $type_id,
        ]);
    }

    /**
     * Route /template/new/{client_id}
     *
     * @param Request $request
     * @param int $client_id
     * @param int $type_id
     * @return redirect
     */

    public function store(Request $request, $client_id, $type_id)
    {
        $MAX_FILE_SIZE = 256 * 1024 * 1024;
        // validation
        $validator = Validator::make($request->all(), [
            'psd' => 'required|max:'.$MAX_FILE_SIZE,
            'psd2' => 'sometimes|max:'.$MAX_FILE_SIZE,
        ]);
        if ($validator->fails()) {
            return redirect('/template/new/'.$client_id.'/'.$type_id)->withErrors($validator)->withInput();
        }

        $client = Clients::find($client_id);
        if (!$client) {
            abort(404, 'Клиент не найден');
        }
        $theme = Theme::find($type_id);
        if (!$client) {
            abort(404, 'Тип продукции не найден');
        }

        // mkdir
        $uploadFolder = $_SERVER['DOCUMENT_ROOT'].'/lib/frx/default';
        rmkdir($uploadFolder);

        // psd
        $file = $request->file('psd');        
        if ($file) {
            $target = $this->uploadTemplate($file, $uploadFolder);
            // save to db
            $template = new Template;
            $template->psd = str_replace($_SERVER['DOCUMENT_ROOT'] , '', $target);
            $template->type = $type_id;
            $template->client_id = $client->id;
            $template->status = 'upload';
            $template->save();

            // psd2
            $file = $request->file('psd2');
            if ($file) {
                $target = $this->uploadTemplate($file, $uploadFolder);
                // save to db
                $template2 = new Template;
                $template2->psd = str_replace($_SERVER['DOCUMENT_ROOT'] , '', $target);
                $template2->type = $type_id;
                $template2->client_id = $client->id;
                $template2->status = 'upload';
                $template2->parent_id = $template->id;
                $template2->activity = 0;
                $template2->save();
            }
        }

        return redirect('/template/upload/'.$template->id)
                ->with('client_id', $client_id)
                ->with('type_id', $type_id);
    }

    /**
     * Upload page
     *
     * @param Request $request
     * @param int $id
     * @return type
     */

    public function upload($id)
    {
        $template = Template::find($id);
        if (!$template) {
            abort(404, 'Шаблон не найден');
        }

        $templates[] = $template;

        // add related temnplates
        $relatedTemplates = Template::where('parent_id', $template->id)->get();
        foreach ($relatedTemplates as $item) {
            $templates[] = $item;
        }

        $breadcrumbs[] = 'Справочник';
        $breadcrumbs[] = 'Шаблоны';
        $breadcrumbs[] = 'Загрузка';
        $page = array(
            'menu' => 'catalogs',
            'submenu' => 'template',
            'breadcrumbs' => $breadcrumbs
        );

        return view('templates.upload', [
            'page' => $page,
            'template_id' => $template->id,
            'templates' => $templates,
        ]);
    }

    /**
     * Parse Psd with Ruby parser
     * Ajax
     *
     * @param int $id
     * @return json
     */

    public function parse($id)
    {
        $template = Template::find($id);
        if (!$template) {
            abort(404, 'Шаблон не найден');
        }

        // related templates
        $relatedTemplates = Template::where('parent_id', $template->id)->get();

        $error = false;

        // parse
        if ($this->parsePsd($template)) {
            // parse related templates
            foreach ($relatedTemplates as $tpl) {
                if (!$this->parsePsd($tpl)) {
                    $error = true;
                    break;
                }
            }
        }
        else {
            $error = true;
        }

        // make result
        if (!$error) {
            $index = 2;
            $file1 = $_SERVER['DOCUMENT_ROOT'].$template->w2p;
            foreach ($relatedTemplates as $tpl) {
                $file2 = $_SERVER['DOCUMENT_ROOT'].$tpl->w2p;
                Frx::merge($file1, $file2);
                $index++;
            }

            $result['success'] = true;
        }
        else {
            $result['success'] = false;
            $result['error'] = 'Ошибка парсинга';
        }

        return Response::json($result);
    }

    /**
     * Parse PSD
     *
     * @param \App\Templates $template
     * @return boolean
     */

    public function parsePsd(Templates $template)
    {
        if (!$template->psd) {
            return false;
        }

        $fullpath = $_SERVER['DOCUMENT_ROOT'].$template->psd;
        if (!file_exists($fullpath)) {
            $template->delete();
            $result['success'] = false;
            $result['error'] = 'Файл не найден';
            return Response::json($result);
        }

        //parse
        if (!RubyParser::parse($fullpath)) {
            return false;
        }

        $pinfo = pathinfo($fullpath);
        $name = $pinfo['filename'];
        $path = $pinfo['dirname'];

        $frx = $path.'/'.$name.'.frx';
        $png = $path.'/'.$name.'.png';
        $preview = $path.'/'.$name.'_small.png';
        if (file_exists($frx) && file_exists($png)) {
            // make preview
            image_resize($png, $preview, 250, 250, 70);
            // save template
            $template->w2p = str_replace($_SERVER['DOCUMENT_ROOT'] ,'', $frx);
            $template->url = str_replace($_SERVER['DOCUMENT_ROOT'] ,'', $preview);
            $template->status = 'parsed';
            $template->save();
        }
        else {
            return false;
        }

        return true;
    }

    /**
     * Set activity to On
     *
     * @param int $id
     * @return redirect
     */

    public function on($id)
    {
       DB::update('UPDATE templates SET activity = "1" WHERE id = ?', [$id]);
       return redirect('/template');
    }

    /**
     * Set activity to Off
     *
     * @param int $id
     * @return redirect
     */

    public function off($id)
    {
       DB::update('UPDATE templates SET activity = "0" WHERE id = ?', [$id]);
       return redirect('/template');
    }

    /**
     * Delete
     *
     * @param int $id
     * @return json
     */

    public function delete($id)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];

        $template = Template::find($id);
        if ($template) {
            try {
                // delete from disk
                Frx::delete($root.$template->w2p);
            }
            catch(\Exception $e) {}

            // delete from db
            $template->delete();

            return Response::json(['success' => true]);
        }
        else {
            return Response::json(['success' => false]);
        }
    }

    /**
     *
     * @param UploadedFile $file
     * @param type $uploadFolder
     * @return type
     */

    protected function uploadTemplate(UploadedFile $file, $uploadFolder)
    {
        // make prefix
        $lastId = \DB::table('templates')->max('id');
        $prefix = str_pad($lastId + 1, 5, '0', STR_PAD_LEFT);

        // make new name
        $newFileName = str_replace([' '], '_', $file->getClientOriginalName());
        $fileName = $prefix.'_'.$newFileName;

        // move file
        return $file->move($uploadFolder, $fileName);
    }

    public function designer($id)
    {
        $user = Auth::user();

        $template = Template::find($id);

        if (!$template) {
            return redirect('/home');
        }

        $uuid = str_replace('/lib/frx', '',  $template->w2p);
        $uuid = str_replace('.frx', '', $uuid);

        if (!$user) {
            return redirect('/home');
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );
        return view('templates.designer', [
            'page' => $page,
            'user' => $user,
            'uuid' => $uuid
        ]);
    }

    /**
     * Страница раскладывания шаблонов на тематики
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function organize()
    {
        $templates = TemplateLibrary::with(['themes.tags', 'colors'])
            ->where('type_key', 'v')
            ->groupBy('collection')
            ->paginate(20);

        $themes = Theme::active()->get();

        $page = [
            'menu' => 'control',
            'submenu' => 'themes',
            'breadcrumbs' => ['Парсер', 'Тематики']
        ];

        return view('templates.organize', compact('page', 'templates', 'themes'));
    }

    /**
     * Update templates
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function patchOrganize(Request $request)
    {
        $templatesData = $request->input('templates');

        if (! $templatesData) {
            session()->flash('message.information', 'Данных для сохранения не выбрано');

            return redirect()->back();
        }

        foreach ($templatesData as $collection => $data) {
            $templates = TemplateLibrary::where('collection', $collection)->get();

            if ($templates->count() === 0) {
                continue;
            }

            $themeIds = isset($data['themes']) ? $data['themes'] : [];
            $colorIds = isset($data['colors']) ? $data['colors'] : [];

            foreach ($templates as $template) {
                $template->themes()->sync($themeIds);
                $template->colors()->sync($colorIds);
            }
        }

        session()->flash('message.success', 'Успешно выполнено.');

        return redirect()->back();
    }

    /**
     * Disable/Enable all templates in collection
     *
     * @param Request $request
     * @return mixed
     */
    public function collectionActivity(Request $request)
    {
        $this->validate($request, [
            'collection' => 'required',
            'action' => 'required'
        ]);

        $templates = TemplateLibrary::where('collection', $request->input('collection'))->get();

        foreach ($templates as $template) {
            $template->activity = $request->input('action') == 'enable';
            $template->save();
        }

        return response()->json([
            'status' => 'success',
        ]);
    }
}
