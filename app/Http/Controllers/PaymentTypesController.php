<?php

namespace App\Http\Controllers;

use App\PaymentTypes;
use App\Subdomain;
use Illuminate\Http\Request;
use App\Services\SettingsValidator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Validator;

class PaymentTypesController extends Controller
{
    /**
     * @var PaymentTypes
     */
    private $payment_types;

    /**
     * @param PaymentTypes $payment_types
     */
    public function __construct(PaymentTypes $payment_types)
    {
        $this->payment_types = $payment_types;
    }

    public function index(Request $request)
    {
        $payment_types_table = $this->payment_types
            ->forSubdomain(Subdomain::findCurrent())
            ->paginate(20);

        $breadcrumbs[] = 'Типы оплаты';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'payment_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('payment_types.index', [
            'page' => $page,
            'payment_types_table' => $payment_types_table,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:payment_types,name',
        ]);
        if ($validator->fails()) {
            return redirect('/payment_types')->withErrors($validator)->withInput();
        }

        $payment_types = new PaymentTypes();
        $payment_types->subdomain_id = Subdomain::findCurrentId();
        $payment_types->name = $request->input('name');
        $payment_types->discount = $request->input('discount');
        $payment_types->key_1s = $request->input('key_1s');
        $payment_types->save();

        return redirect('/payment_types');
    }

    public function edit($id)
    {
        $payment_type = \DB::table('payment_types')->find($id);

        $breadcrumbs[] = 'Типы оплаты';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'payment_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('payment_types.edit', [
            'page' => $page,
            'payment_type' => $payment_type,
        ]);
    }

    public function update($id, Request $request)
    {
        $payment_type = PaymentTypes::find($id);
        $subdomain_id = is_null($payment_type->subdomain_id) ? 'NULL' : $payment_type->subdomain_id;

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:payment_types,name,'.$id.',id,subdomain_id,'.$subdomain_id,
        ]);
        if ($validator->fails()) {
            return redirect('/payment_types/edit/'.$id)->withErrors($validator)->withInput();
        }

        $payment_type->name = $request->input('name');
        $payment_type->discount = $request->input('discount');
        $payment_type->key_1s = $request->input('key_1s');
        $payment_type->save();

        SettingsValidator::updateValidationFlag();

        return redirect('/payment_types');
    }
}
