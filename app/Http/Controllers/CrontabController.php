<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Orders;
use Carbon\Carbon;
use App\Order_logs;
use App\ParserFiles;
use App\Classes\Parser\Frx;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use App\Classes\Parser\RubyParser;
use Illuminate\Contracts\Logging\Log;

class CrontabController extends Controller
{
    /**
     * @var Log
     */
    private $logger;

    /**
     * CrontabController constructor.
     * @param Log $logger
     */
    public function __construct(Log $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Sand FRX to server
     * 
     * @return void
     */
    public function ordersMakePdf()
    {
        $start = time();
        $frxPath = 'lib/frx/';
        
        $orders = Orders::where('status','>=',3)->get();
        foreach ($orders as $order) {
            if (file_exists($frxPath . $order->file)) {
                if (!file_exists($frxPath . $order->img) || !file_exists($frxPath . $order->pdf)) {
                    echo $frxPath . $order->file."\n";
                    $this->makePdf($frxPath . $order->file);
                }
                //limit time
                if (time() - $start > 300) {
                    break;
                }
            }
            else {
                DB::beginTransaction();
                $order->delete();
                Order_logs::where('order_id', $order->id)->delete();
                DB::commit();
            }
        } 
        
        // Удалить черновики не редактируемый 3 дня подряд
        $carbon = Carbon::now();
        $carbon->subDays(3);
        
        $orders = Orders::where('status','=',1)->where('updated_at','<=',$carbon)->get();
        foreach ($orders as $order) {
            $file = $frxPath . $order->file;
            $dir = pathinfo($file, PATHINFO_FILENAME);
            if (file_exists($file)) {
                unlink($file);
            }
            if (file_exists($dir)) {
                rrmdir($dir);
            }
            
            DB::beginTransaction();
            $order->delete();
            Order_logs::where('order_id', $order->id)->delete();
            DB::commit();
        }
    }

    /**
     * Make PDF from order FRX
     *
     * @param $frx
     * @return bool
     * @throws Exception
     */
    public function makePdf($frx)
    {
        /*
        208.76.175.70 на тот, что работает в Azure 40.68.164.231
        */
        
        $dir = pathinfo($frx, PATHINFO_DIRNAME);
        $name = pathinfo($frx, PATHINFO_FILENAME);
        $file_name = pathinfo($frx, PATHINFO_BASENAME);
        $file_dir = $name;
        $zipFileName = $file_name . '.zip';
        
        // go to directory
        $currentDir = getcwd();
        chdir($dir);

        // make zip
        $zip = new \ZipArchive;        
        if ($zip->open($file_name . '.zip', \ZipArchive::CREATE) === true){
            $zip->addFile($file_name, $file_name);
            
            $dirHandle = opendir($file_dir);
            while (false !== ($file = readdir($dirHandle))) {
                if (strlen($file) > 3) {
                    $zip->addFile($file_dir.'/'.$file, $file_dir.'/'.$file);
                }
            }            
            $zip->close();
        }
        else{
            throw new Exception('Не могу создать архив: '.$zipFileName);
        }
        
        // Загружаем файл
        $filename = $file_name . '.zip';
        $pdf = str_replace('.zip','.pdf', $filename);
        $jpg = str_replace('.zip','.jpg', $filename);

        $myCurl = curl_init();
        curl_setopt_array($myCurl, [
            CURLOPT_URL => 'http://40.68.164.231/service/ReportService.svc/putzip/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_USERPWD => "frservice:jhkja882klo",
            CURLOPT_HTTPHEADER => ['Content-type: multipart/form-data'],
            CURLOPT_POSTFIELDS => ['preparedReport' => new \CurlFile($filename)]
        ]);
        $uuid = curl_exec($myCurl); // юид картинки
        curl_close($myCurl);

        $uuid = str_replace('"', '', $uuid);        

        if ($uuid) {
            //  get PDF file
            $myCurl = curl_init();
            curl_setopt_array($myCurl, [
                CURLOPT_URL => 'http://40.68.164.231/service/ReportService.svc/getpdf/' . $uuid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD => "frservice:jhkja882klo"
            ]);
            //открываем файловый дескриптор (куда сохранять файл)
            $fp = fopen($pdf,'w');
            //сохраняем файл
            curl_setopt($myCurl, CURLOPT_FILE, $fp);
            $response1 = curl_exec($myCurl);
            curl_close($myCurl);
            fclose($fp);

            // get JPG file
            $myCurl = curl_init();
            curl_setopt_array($myCurl, [
                CURLOPT_URL => 'http://40.68.164.231/service/ReportService.svc/getlogo/' . $uuid . '/500/500',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD => "frservice:jhkja882klo"
            ]);
            //открываем файловый дескриптор (куда сохранять файл)
            $fp = fopen($jpg,'w');
            //сохраняем файл
            curl_setopt($myCurl, CURLOPT_FILE, $fp);
            $response2 = curl_exec($myCurl);
            curl_close($myCurl);
            fclose($fp);
            
            // return
            chdir($currentDir);
            return true;
        }
        else {
            unlink($zipFileName);
            chdir($currentDir);
            return false;
        }
    }
}
