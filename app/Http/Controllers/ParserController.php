<?php

namespace App\Http\Controllers;

use App\Http\Requests\ParserFileRequest;
use Illuminate\Http\Request;
use Response;
use App\ParserFiles;
use App\Classes\Parser\RubyParser;
use App\TemplateLibrary;
use DB;
use Validator;
use App\Classes\Parser\Frx;
use App\Http\Controllers\Controller;
use Illuminate\Http\Exception\HttpResponseException;

class ParserController extends Controller
{
    /**
     * Route /parser
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ParserFiles::oldest('path');

        $filters = ['status', 'vendor', 'number', 'type', 'side'];

        foreach ($filters as $name) {
            if ($request->input($name)) {
                $query->where($name, $request->input($name));
            }
        }

        $files = $query->paginate(50);

        $total = ParserFiles::count();
        $parsedCount = ParserFiles::where('status', 'move')->count();
        $newCount = ParserFiles::where('status', 'new')->count();
        $errorCount = ParserFiles::where('status', 'error')->count();
        $processCount = ParserFiles::where('status', 'parse')->count();

        $breadcrumbs[] = 'Парсер';
        $breadcrumbs[] = 'Файлы PSD';
        $page = array(
            'menu' => 'control',
            'submenu' => 'parser',
            'breadcrumbs' => $breadcrumbs
        );
        return view('parser.index', compact('page', 'files', 'total', 'parsedCount', 'newCount', 'errorCount', 'processCount'));
    }

    /**
     * Route /parser/import
     * 
     * @return \Illuminate\Http\Response
     */
    
    public function import()
    {
        $files = ParserFiles::where('status','=','move')->where('hidden', false)->get();
        $product_types = DB::table('product_types')->where('activity', 1)->get();
        $themes = DB::table('themes')->where('activity', 1)->get();
        
        $breadcrumbs[] = 'Импорт PSD';
        $breadcrumbs[] = 'Файлы';
        $page = array(
            'menu' => 'control',
            'submenu' => 'import_psd',
            'breadcrumbs' => $breadcrumbs
        );
        return view('parser.import', [
            'page' => $page, 
            'files' => $files,
            'product_types' => $product_types,
            'themes' => $themes,
        ]);
    }
    
    /**
     * Merge 2 Frx to new library template
     * 
     * Route /parser/library/merge
     * 
     * @param Request $request
     * @return redirect
     */
    
    public function merge(Request $request)
    {
        $validator = Validator::make($request->all(), [            
            'first_id' => 'required|integer|exists:parser_files,id',
            'second_id' => 'required|integer|exists:parser_files,id',
            'type' => 'required|integer',
            'themes' => 'required|array',
        ]);
        if ($validator->fails()) {            
            return redirect('/parser/import?page='.$request->input('page'))                    
                    ->withErrors($validator)
                    ->withInput();
        }
        
        $first = ParserFiles::where('id','=',$request->input('first_id'))->where('status','=','move')->first();
        $second = ParserFiles::where('id','=',$request->input('second_id'))->where('status','=','move')->first();
        
        if ($first && $second) {
            $frx1 = $first->upload_folder.'/'.$first->name.'.frx';
            $frx2 = $second->upload_folder.'/'.$second->name.'.frx';            
            
            $name = md5(time().rand(1000,9999));
            $folder = md5($frx1.$frx2.rand(0,100));
            $newFrx = Frx::getLibraryPath().'/'.$folder.'/'.$name.'.frx';
            
            Frx::combine($frx1, $frx2, $newFrx);
            
            // move preview
            $previewPath = Frx::getPreviewPath().'/'.$folder;
            rmkdir($previewPath);            
            $small = Frx::getPreviewFile($newFrx);            
            $preview = $previewPath.'/'.$name.'_small.png';                        
            copy($small, $preview);            
            
            // Save template library
            $template = new TemplateLibrary;
            $template->w2p = $newFrx;            
            $template->url = '/'.$preview;
            $template->type = $request->input('type');
            $template->two_side = true;
            $template->save();
            
            // save template themes            
            $template->themes()->sync($request->input('themes'));
        }
        
        return redirect('/parser/import?page='.$request->input('page'))->with('merge_success', true);
    }
    
    /**
     * Move template to user library folder
     * Ajax
     * 
     * Route /parser/library/move/{id}
     * 
     * @param Request $request
     * @param type $id
     * @return \Illuminate\Http\Response::json
     */
    
    public function move(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [            
            'type' => 'required|integer',
            'themes' => 'required|array',            
        ]);
        $errors = $validator->errors()->all();        
        
        if (!$errors) {                    
            $file = ParserFiles::where('id','=',$id)->where('status','=','move')->first();
            if (!$file) {
                $result['success'] = false;
                $result['errors'][] = 'Страница не найдена';
                return Response::json($result);
            }
            
            // save to lib
            if ($this->saveToLibrary($file, $request)) {                
                $result['success'] = true;
            }
            else {                
                $result['success'] = false;
                $result['errors'][] = 'Файлы не найдены';                
                $file->status = 'error';
                $file->save();
            }        
        }
        else {
            $result['success'] = false;
            $result['errors'] = $errors;
        }
        
        return Response::json($result);
    }
    
    /**
     * Mass movement templates to user library
     * 
     * Route /parser/library/massmove
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response::json
     */
    
    public function massMove(Request $request)
    {
         // validation
        $validator = Validator::make($request->all(), [
            'ids' => 'required|array',
            'type' => 'required|integer',
            'themes' => 'required|array',            
        ]);
        $errors = $validator->errors()->all();
        
        if (!$errors) {        
            $parserFiles = ParserFiles::whereIn('id', $request->input('ids'))->where('status','=','move')->get();
            foreach ($parserFiles as $file) {
                if (! $this->saveToLibrary($file, $request)) {                
                    $result['errors'][] = 'Невозможно перенести файл: '.$file->filename;                    
                    $result['error_code'] = 2;
                    
                    $file->status = 'error';
                    $file->save();
                }
            }            
            //if not errors success = true
            $result['success'] = (!isset($result['errors']) || count($result['errors']) == 0);            
        }
        else {
            $result['success'] = false;            
            $result['errors'] = $errors;
            $result['error_code'] = 1;
        }
        
        return Response::json($result);
    }
    
    /**
     * Move template files to libary function
     * 
     * @param ParserFiles $parserFile
     * @param Request $request
     * @return boolean
     */
    
    protected function saveToLibrary(ParserFiles $file, Request $request)
    {
        // paths
        $libraryFolder = $file->upload_folder.'/files';
        $libraryFrx = $file->upload_folder.'/'.$file->name.'.frx';
        $libraryPsd = $file->path;
        $libraryPng = $file->upload_folder.'/'.$file->name.'.png';
        $libraryPreview = $file->upload_folder.'/'.$file->name.'_small.png';
        
        if (file_exists($libraryFolder) && file_exists($libraryFrx) && file_exists($libraryPng) && file_exists($libraryPreview)) {
            // Save template library
            $template = new TemplateLibrary;
            $template->w2p = $libraryFrx;
            $template->psd = $libraryPsd;
            $template->url = $file->preview;
            $template->type = $request->input('type');            
            $template->save();
            
            // save template themes            
            $template->themes()->sync($request->input('themes'));
            
            // save file status
            $file->status = 'library';
            $file->save();
            
            return true;
        }  
        else {
            // save file status
            $file->status = 'error';
            $file->save();
            return false;
        } 
    }
    
    public function hide($id)
    {
        $file = ParserFiles::find($id);
        if ($file) {
            $file->hidden = true;
            $file->save();
            return Response::json([
                'success' => true,
                'id' => $id
            ]);
        }
        else {
            return Response::json(['success' => false]);
        }
    }

    /**
     * Update status to new
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setStatusNew($id)
    {
        $file = ParserFiles::findOrFail($id);
        $file->status = 'new';
        $file->save();

        return redirect()->back();
    }

    /**
     * Get errors
     *
     * @param $id
     * @return Response
     */
    public function errors($id)
    {
        $file = ParserFiles::findOrFail($id);

        $errors = $file->errors ?: 'Нет данных.';

        return response($errors);
    }

    /**
     * @param $id
     * @param ParserFileRequest $request
     * @return Response
     */
    public function update($id, ParserFileRequest $request)
    {
        $parserFile = ParserFiles::findOrFail($id);

        $result = $parserFile->update([
            $request->input('name') => mb_strtolower($request->input('value'))
        ]);

        if ($result) {
            return response('Сохранено');
        }

        return response('Ошибка при редактировании.', 422);
    }
}
