<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientStatusesController extends Controller
{
    public function index()
    {
        $client_statuses = \DB::table('client_statuses')->paginate(20);

        $breadcrumbs[] = 'Статусы клиентов';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'client_statuses',
            'breadcrumbs' => $breadcrumbs
        );
        return view('client_statuses.index', [
            'page' => $page,
            'client_statuses' => $client_statuses,
        ]);
    }
}
