<?php

namespace App\Http\Controllers\Dictionary;

use App\Models\Equipment\Equipment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'menu' => 'catalogs',
            'submenu' => 'equipment',
            'breadcrumbs' => ['CRM', 'Оборудование'],
        ];

        $equipments = Equipment::with('printParameters')->get();

        return view('dictionary.equipment.index', compact('page', 'equipments'));
    }

    /**
     * @param Equipment $equipment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Equipment $equipment)
    {
        $page = [
            'menu' => 'catalogs',
            'submenu' => 'materials',
            'breadcrumbs' => ['CRM', 'Оборудование'],
        ];

        return view('dictionary.equipment.edit', compact('page', 'equipment'));
    }
}
