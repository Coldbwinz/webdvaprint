<?php

namespace App\Http\Controllers\Dictionary;

use App\Models\Material\Material;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = [
            'menu' => 'catalogs',
            'submenu' => 'materials',
            'breadcrumbs' => ['CRM', 'Материалы'],
        ];

        $materials = Material::all();

        return view('dictionary.material.index', compact('page', 'materials'));
    }

    /**
     * @param Material $material
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Material $material)
    {
        $page = [
            'menu' => 'catalogs',
            'submenu' => 'materials',
            'breadcrumbs' => ['CRM', 'Материалы'],
        ];

        return view('dictionary.material.edit', compact('material', 'page'));
    }
}
