<?php

namespace App\Http\Controllers;

use App\Services\SettingsValidator;
use App\Urgency;
use App\Subdomain;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Validator;

class UrgencyController extends Controller
{
    /**
     * @var Urgency
     */
    private $urgency;

    /**
     * @param Urgency $urgency
     */
    public function __construct(Urgency $urgency)
    {
        $this->urgency = $urgency;
    }

    public function index(Request $request)
    {
        $urgency_table = $this->urgency
            ->forSubdomain(Subdomain::findCurrent())
            ->paginate(20);

        $breadcrumbs[] = 'Типы срочности';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'urgency',
            'breadcrumbs' => $breadcrumbs
        );
        return view('urgency.index', [
            'page' => $page,
            'urgency_table' => $urgency_table,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:urgency,name',
        ]);
        if ($validator->fails()) {
            return redirect('/urgency')->withErrors($validator)->withInput();
        }

        $urgency = new Urgency();
        $urgency->subdomain_id = Subdomain::findCurrentId();
        $urgency->name = $request->input('name');
        $urgency->discount = $request->input('discount');
        $urgency->key_1s = $request->input('key_1s');
        $urgency->save();

        return redirect('/urgency');
    }

    public function edit($id)
    {
        $urgency = \DB::table('urgency')->find($id);

        $breadcrumbs[] = 'Типы срочности';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'urgency',
            'breadcrumbs' => $breadcrumbs
        );
        return view('urgency.edit', [
            'page' => $page,
            'urgency' => $urgency
        ]);
    }

    public function update($id, Request $request)
    {
        $urgency = Urgency::find($id);
        $subdomain_id = is_null($urgency->subdomain_id) ? 'NULL' : $urgency->subdomain_id;

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:urgency,name,'.$id.',id,subdomain_id,'.$subdomain_id,
        ]);
        if ($validator->fails()) {
            return redirect('/urgency/edit/'.$id)->withErrors($validator)->withInput();
        }

        $urgency->name = $request->input('name');
        $urgency->discount = $request->input('discount');
        $urgency->key_1s = $request->input('key_1s');
        $urgency->save();

        SettingsValidator::updateValidationFlag();

        return redirect('/urgency');
    }
}
