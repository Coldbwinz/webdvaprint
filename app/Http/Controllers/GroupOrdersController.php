<?php

namespace App\Http\Controllers;

use App\Clients;
use App\ClientTypes;
use App\Details;
use App\Emails\CreatePasswordEmail;
use App\Emails\ReadyOrderEmail;
use App\Events\GroupOrderWasFormed;
use App\GroupOrders;
use App\Http\Controllers\Personal\PaymentController;
use App\Orders;
use App\OrderStatuses;
use App\PasswordReset;
use App\ProductType;
use App\Contractor;

use finfo;
use Illuminate\Http\Request;
use App\Subdomain;
use App\Urgency;
use App\User;
use App\UserGroup;
use App\Emails\SendInvoiceNotification;
use App\Emails\SendLayoutEmail;

use Carbon\Carbon;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Landish\Pagination\Pagination;
use phpDocumentor\Reflection\Types\This;
use Response;

class GroupOrdersController extends Controller
{
public $dashBoardStatuses = ['2' => [3, 5, 9, 10], '1' => [2, 4, 9, 12, 45, 55, 102, 112, 1000, 201, 300, 301], '6' => [51], '3' => [/*1*/], 5 => [25]];

    public function getGroupOrders(Request $request, $status = 0)
    {
        $titles = [
            0 => 'История',
            1 => 'Требуют внимания',
            2 => 'На согласовании',
            3 => 'Ожидают оплаты',
            6 => 'Готовы к выдаче',
        ];

        // Если поддомена нет, то это типография
        $isTypography = is_null(Subdomain::findCurrent());

        $query = GroupOrders::query()->leftjoin('clients as c', function ($query) use ($request) {
            if (strlen($request->search) > 1) {
                $search_string = "%" . $request->search . "%";
                $query->on('c.id', '=', 'client_id')
                    ->where('c.name', 'like', $search_string)
                    ->orWhere('c.phone', 'like', $search_string)
                    ->orWhere('c.email', 'like', $search_string);
            } else {
                $query->on('c.id', '=', 'client_id');
            }
        })
            ->with(['client.contacts', 'statusPartner'])
            ->select('group_orders.*', 'c.name as client_name', 'c.area as area',
                'c.phone as contacts_phone', 'c.email as contacts_email', 'c.status_id as client_status')
            ->orderBy('group_orders.updated_at', 'desc');

        //Для быстрого поиска заказа с нужным номером
        //$query->where('group_orders.id', 135);

        if ($request->status_pay_id > 0) {
            $query->where('group_orders.status', $request->status_pay_id);
        }

        if ($request->status_pay_id == -1) {
            $date = Carbon::now();
            $query->whereIn('status', [1, 7, 8]);
            $query->where('close_date','<',$date);
        }

        // Заказы со статусом Выставлен счет не должны присутствовать в блоке Ожидают оплаты
        if ($request->$status == 3) {
            $query->where('status', '!=', 1);
            // Просматривает типография
            if ($isTypography) {
                $query->where(function ($query) {
                    $query->where(function ($q) {
                        // Партнёрские заказы со статусом Ожидает оплаты
                        $q->whereNotNull('group_orders.subdomain_id')
                            ->whereIn('group_orders.status', status(['partially_paid', 'fully_paid', 'cash_on_delivery'], 'id'));
                    })
                        // заказы от клиента со статусами Оплата при получении  и Частично оплачен
                        ->orWhere(function ($q) {
                            $q->whereNull('group_orders.subdomain_id')
                                ->whereIn('group_orders.status', status(['partially_paid', 'cash_on_delivery'], 'id'));
                        });
                });
            } else {
                // Просматривает партнёр
                $query->whereIn('group_orders.status', status(['partially_paid', 'cash_on_delivery'], 'id'));
            }
        }
        // Для партнёра выводить только заказы его поддомена
        $env = app('subdomain-environment');
        if (auth()->user()->isPartner() && $env->currentSubdomain()) {
            $subdomain = Subdomain::findCurrent();
            if ($subdomain) {
                $query->where('group_orders.subdomain_id', $subdomain->id);
            }
        }

        // Для менеджера выводить только его заказы
        if (auth()->user()->isManager()) {
            $query->where('group_orders.manager_id', auth()->user()->id);
        }

        // Фильтрация по клиенту
        if ($request->client_id) {
            $query->where('group_orders.client_id', $request->client_id);
        }

        // Фильтрация по контакту
        if ($request->contact_id) {
            $query->where('group_orders.contact_id', $request->contact_id);
        }

        if ($request->related_contact_id) {
            $query->where('contact_id', '!=', $request->related_contact_id);
        }

        // Поиск по дате создания
        if ($request->created_at) {
            $range = explode(' - ', $request->created_at);

            try {
                $startDate = Carbon::parse($range[0], settings('user_timezone'))->timezone(config('app.timezone'));
                $endDate = Carbon::parse($range[1], settings('user_timezone'))->timezone(config('app.timezone'));

                $query->whereBetween('group_orders.created_at', [$startDate, $endDate]);
            } catch (\Exception $exception) {
                session()->flash('message.danger', 'Неверный формат даты.');
            }
        }

        $product_types = [];
        $statuses = [];
        $rework = false;
        foreach ($query->get() as $item) {
            $new_product_types = (array)$item->orders()->pluck('type_product');
            $product_types = array_merge($product_types, $new_product_types);
            $statuses = array_merge($statuses, [$item->status]);
            $group_orders_statuses = (array) $item->orders()->pluck('status');
            $statuses = array_merge($statuses, $group_orders_statuses);

            if (!$rework) {
                $group_orders_sort_order = (array) $item->orders()->pluck('sort_order');
                if (in_array(56, $group_orders_sort_order)) {
                    if (count(array_intersect([25,30,35,40],$group_orders_statuses)) > 0) {
                        $statuses = array_merge($statuses,[125]);
                        $rework = true;
                    }
                }
            }
        }
        $product_types = ProductType::whereIn('id', $product_types)->get(['id', 'name']);
        $statuses = array_unique($statuses);
        $statuses = array_diff($statuses, [1, 6, 7, 8, 500]);
        $statuses = OrderStatuses::whereIn('id', $statuses)->orderby('sort_order','asc')->get(['id', 'name']);

        // Поиск по типу продукции
        if ($request->product_type_id) {
            $query->whereHas('orders', function ($q) use ($request) {
                $q->where('type_product', $request->product_type_id);
            });
        }

        // Поиск по статусу
        if ($request->status_id) {
            if ($request->status_id != 125) {
                $query->where('status', $request->status_id)->orWhereHas('orders', function ($q) use ($request) {
                    $q->where('status', $request->status_id);
                });
            } else { //Спец селект для позиций на доработке
                $query->whereHas('orders', function ($q) use ($request) {
                    $q->whereIn('status', [25, 30, 35, 40])->where('sort_order', '>', 55);
                });
            }
        }

        // Поиск по клиентам и контактам
        if ($request->search) {
            $isTypography
                ? $query->searchForTypography($request->search)
                : $query->searchForPartner($request->search);
        }

        $group_orders = $query->get();

        foreach ($group_orders as $key => $group_item) {
            $resendOrdersCount = $group_item->orders()->whereIn('status', [5, 9])->count();
            $group_orders[$key]->resend = $resendOrdersCount > 0;

            $ordersStatus57 = $group_item->orders()->where('status', 57)->get(['price']);
            if (count($ordersStatus57) > 0) {
                foreach ($ordersStatus57 as $orderStatus57) {
                    $group_orders[$key]->return_defect += $orderStatus57->price;
                }
            }
            $ordersStatus58 = $group_item->orders()->where('status', 58)->get(['defect_closed_discount']);
            if (count($ordersStatus58) > 0) {
                foreach ($ordersStatus58 as $orderStatus58) {
                    $group_orders[$key]->return_discount += json_decode($orderStatus58->defect_closed_discount)->price;
                }
            }

            $status_unset = false;
            if ($status == 3) {
                if (($group_item->money == 1) || ($group_item->status == 6)) {
                    $status_unset = true;
                }
                if (isset($group_item->subdomain_id) && $group_item->status_partner != 6) {
                    $status_unset = false;
                }
            }
            if ($status == 6) {
                if (empty(Subdomain::findCurrent()) && (isset($group_item->subdomain_id))) {
                    $status_unset = true;
                }
            }
            if ($status_unset == false) {
                if (in_array($status, [0, 3])) {
                    $orders_list = Orders::where('group_id', $group_item->id)
                        ->get();
                } else {
                    if ($group_item->subdomain_id && $status == 6) {
                        $orders_list = Orders::where('group_id', $group_item->id)
                            ->whereIn('status', $this->dashBoardStatuses[$status])
                            ->where('status', '!=', 45)
                            ->get();
                    } else {
                        if ($isTypography) {
                            $orders_list = Orders::where('group_id', $group_item->id)
                                ->forTypography()
                                ->whereIn('status', $this->dashBoardStatuses[$status])
                                ->get();
                        } else {
                            $orders_list = Orders::where('group_id', $group_item->id)
                                ->whereIn('status', $this->dashBoardStatuses[$status])
                                ->get();
                        }
                    }
                }
                if (count($orders_list) > 0) {
                    /* информация по статусам */
                    foreach ($orders_list as $key_order => $order_list_item) {
                        if (($order_list_item->status >= 25) && $order_list_item->status <= 40) {
                            if ($order_list_item->sort_order > 55) {
                                $order_list_item->rework_status_info = DB::table('order_statuses')->find(56);
                            }
                        }
                        $order_list_item->status_info = DB::table('order_statuses')->find($order_list_item->status);
                        if (empty($order_list_item->status_info)) {
                            $order_list_item->status_info = DB::table('order_statuses')->find(999);
                        }
                    }
                    $group_item->orders_total_count = Orders::where('group_id', $group_item->id)->count();
                    $group_item->orders = $orders_list;
                    if ($request->type_id > 0) {
                        $type_unset = true;
                    }
                    foreach ($group_item->orders as $order_item) {
                        if ($request->type_id > 0) {
                            if ($order_item->type_product == $request->type_id) {
                                $type_unset = false;
                            }
                        }
                        if ($order_item->type_product != 999999) {
                            $order_item->product_name = ProductType::find($order_item->type_product)->name;
                            $order_item->printler_type = $order_item->productType->printler_type;
                        } else {
                            $order_item->product_name = $order_item->price_details['selected_product_type_name'];
                            $order_item->printler_type = $order_item->price_details['printler_type'];
                        }
                        $order_item->product_services = $order_item->price_details['selected_product'];
                    }
                } else {
                    $status_unset = true;
                }
            }
            if ((($request->type_id > 0) && isset($tpe_unset) && $type_unset) || $status_unset || (isset($orders_list) && (count($orders_list) == 0))) {
                unset($group_orders[$key]);
            }
        }

        foreach ($group_orders as $key => $value) {
            /* Информация о коде статуса */
            $status_info = DB::table('order_statuses')->find($value->status);

            if (empty($status_info)) {
                $status_info = DB::table('order_statuses')->find(1);
            }

            $group_orders[$key]->status = $status_info;

            if ($value->pay == $value->price) {
                $value->status_text = '<span class="label label-info" style="background-color: #00a65a !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Оплачен полностью</span>';
            } else if ($value->pay < $value->price) {
                if ($value->pay > 0) {
                    $value->status_text = '<span class="label label-info" style="background-color: #1abc9c !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Частично оплачен</span>';
                } else {
                    $value->status_text = '<span class="label label-info" style="background-color: #5da6c2 !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Печать без предоплаты</span>';
                }
            } else {
                $value->status_text = '<span class="label label-info" style="background-color: #00a65a !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Клиент переплатил</span>';
            }
        }

        //Перенастраиваем пагинацию
        $paginator = [];
        foreach ($group_orders as $item) {
            $paginator[] = $item;
        }
        $perPage = 10;
        $current_page = $request->page;
        //dump($request->all());
        if (!isset($request->page) || ($request->change == 1)) {
            $current_page = 1;
            $request->page = 1;
        }
        $offset = ($current_page * $perPage) - $perPage;
        $articles = array_slice($paginator, $offset, $perPage, true);
        $group_orders = new LengthAwarePaginator($articles, count($paginator), $perPage, $current_page,
            ['path' => Paginator::resolveCurrentPath()]);
        if ($status && count($group_orders) === 0) {
            return redirect('/home');
        }

        // contractors
        $contractorsSelect = [];
        $contractors = Contractor::with('contacts')->get();
        foreach ($contractors as $contractor) {
            $contractorsSelect[$contractor->id] = $contractor->displayedName();
        }

        // page
        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = isset($titles[$status]) ? $titles[$status] : 'История';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );
        $selected_group_order_id = \Session::get('selected_group_order_id');
        $opened = (($status > 0) && ($status != 3));

        $data = [
            'request_page' => $request->input['page'],
            'request_temp_page' => $request->input['temp_page'],
            'temp_count' => count($paginator),
            'page' => $page,
            'current_page' => $current_page,
            'group_orders' => $group_orders,
            'product_types' => $product_types,
            'statuses' => $statuses,
            'status' => $status,
            'selected_group_order_id' => $selected_group_order_id,
            'opened' => $opened,
            'contractorsSelect' => $contractorsSelect,
            'pagination' => $group_orders->render(),
        ];
        if (\Request::ajax()) {
            $data['html'] =  view('orders.history_table_data', $data)->render();
            return \Response::json($data);
        } else {
            return \Response::view('orders.history', $data)
                ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        }
    }

    /**
     * Отправляем клиенту на согласование повторно
     */
    public function reviewerSendRepeated($group_id)
    {
        $groupOrder = GroupOrders::find($group_id);
        if (!$groupOrder) {
            return redirect('/home');
        }

        $ordersQuery = $groupOrder->orders()->whereIn('status', [5, 9]);
        $order = $ordersQuery->first();
        $order->client_info = Clients::find($order->client_id);

        // смена статуса
        $groupOrder->repeat_send += 1;
        $groupOrder->save();

        // send email
        (new SendLayoutEmail('emails.reviwer_repeat'))
            ->send($order);

        return redirect()->back()->with('selected_group_order_id', $group_id);
    }

    public function getGroupOrderPDF($group_order_id, $download_sign = null)
    {
        $new_pdf_sign = 1; //TODO: сделать проверку на необходимость нового расчёта
        if ($new_pdf_sign == 1) {
            $group_order = GroupOrders::find($group_order_id);
            $group_orders = $group_order->orders()->get();
            $step2 = (isset($group_order->subdomain_id)) ? true : false;

            if ($step2) {
                $client = $group_order->partnerAsClient();
            } else {
                $client = $group_order->client()->first();
            }
            $payer = $client->representative();
            $invoice_number = $this->getInvoiceNumber($step2, $group_order);
            $time_stamp = time();

            if ($step2) {
                $detail = Details::where('subdomain_id','=',null)->where('activity',1)->first();
            } else {
                $detail = Details::find($group_order->detail_id);
            }

            if ($detail) {
                $pdf = App::make('dompdf.wrapper');

                $html_doc = view('documents.invoice', [
                    'detail' => $detail,
                    'company_details' => $client->company_details,
                    'invoice_number' => $invoice_number,
                    'payer' => $payer,
                    'group_orders' => $group_orders,
                    'step2' => $step2,
                ])->render();

                $pdf->loadHTML($html_doc, 'UTF-8');

                $pdfPartnerFilename = 'order_partner_n_' . $invoice_number . '_' . $time_stamp . '.pdf';
                $pdfFilename = 'order_n_' . $invoice_number . '_' . $time_stamp . '.pdf';

                if ($step2) {
                    $path = "orders_pdf/{$pdfPartnerFilename}";
                    $group_order->invoice_partner_pdf = $pdfPartnerFilename;
                } else {
                    $path = "orders_pdf/{$pdfFilename}";
                    $group_order->invoice_pdf = $pdfFilename;
                }
                $pdf->save(public_path($path));

                $group_order->last_invoice_pdf = $time_stamp;
                $group_order->save();
                if (isset($download_sign)) {
                    return response()->download($path);
                } else {
                    return '/' . $path;
                }
            } else {
                return redirect('/options/details/');
            }
        } else {
            return '';
        }
    }

    public function getPriceTableForProduct($product_id, $client_id, $send_invoice_button = null)
    {
        if ($product_id != 999999) {
            $product_info = ProductType::find($product_id);
            $prices = json_decode($product_info->services);
            $price_discount = $this->getDiscount($client_id);
            if (!is_object($prices)) {
                $prices = json_decode('{}');
            }
            foreach ($prices as $group_key => $group) {
                if ($group_key != 'counts') {
                    if ($product_info->printler_type == 3) {
                        if (isset($group->covers_prices)) {
                            foreach ($group->covers_prices as $price_item_key => $price_item) {
                                foreach ($price_item as $price_jtem_key => $price_jtem) {
                                    $price_jtem = str_replace(',', '.', $price_jtem);
                                    $prices->$group_key->covers_prices->$price_item_key->$price_jtem_key = round($price_jtem * $price_discount, 2);
                                }
                            }
                        }
                        if ($group->type == 'на пружине') {
                            if (isset($group->pads_prices)) {
                                foreach ($group->pads_prices as $price_item_key => $price_item) {
                                    foreach ($price_item as $price_jtem_key => $price_jtem) {
                                        $price_jtem = str_replace(',', '.', $price_jtem);
                                        $prices->$group_key->pads_prices->$price_item_key->$price_jtem_key = round($price_jtem * $price_discount, 2);
                                    }
                                }
                            }
                        }
                    }
                    foreach ($group->products_prices as $price_item_key => $price_item) {
                        $prices->$group_key->products_prices[$price_item_key] = round($price_item * $price_discount, 2);
                    }
                    foreach ($group->options_prices as $price_item_key => $price_item) {
                        $prices->$group_key->options_prices[$price_item_key] = round($price_item * $price_discount, 2);
                    }
                }
            }
            $prices->send_invoice = $send_invoice_button;
            $prices->printler_type = $product_info->printler_typel;
            if ($send_invoice_button == 1) {
                return view('orders.priceForm_new', [
                    'type_id' => $product_id,
                    'company_id' => $client_id,
                    'prices' => $prices,
                    'selected_size' => '',
                    'selected_type' => '',
                    'selected_material' => '',
                    'selected_pad' => '',
                    'selected_pad_chromacity' => '',
                    'selected_cover' => '',
                    'selected_cover_chromacity' => '',
                    'selected_options' => [],
                    'price_count' => '',
                    'close_date' => '',
                    'step1' => '1',
                ])->render();
            } else {
                return $prices;
            }
        }
    }


    public function getDiscount($client_id)
    {
        if ($client_id) {
            $discount = Clients::find($client_id)->getDiscount();
            if ($discount > 100) {
                $discount = $discount / 100;
            }
            return (100 - $discount) / 100;
        }
        return 0;
    }

    function status($id, $status)
    {
        $group_order = GroupOrders::find($id);
        $group_order->status = $status;
        $group_order->save();

        // Закрыть заказ
        if ($status == 100) {
            foreach ($group_order->close_docs as $key => $close_doc) {
                addLog(0, 100, auth()->user()->id, link_to($close_doc, 'Документ '.($key + 1)), $group_order->id, 0, 'Подписаны закрывающие документы');
            }
        }

        return redirect('/orders/history');
    }

    /**
     * @param $company_id
     * @return mixed
     */
    public function closeGroupOrder($company_id)
    {
        $group_order = GroupOrders::query()->where('next', '=', 1)->where('manager_id', '=', \Auth::user()->id)->first();
        if (count($group_order) > 0) {
            //Сразу задаём номера счетов, чтобы они попали в логи.
            $this->getInvoiceNumber(null, $group_order);
            if (isset($group_order->subdomain_id)) {
                $this->getInvoiceNumber(true, $group_order);
            }
            $this->recalculateGroupPrice($group_order->id);
            if ($group_order->orders()->whereIn('status', [3, 4, 5, 9, 301])->count() == 0) {
                $this->closeGroupOrderSendInvoice($group_order);
                (new SendInvoiceNotification)->send($group_order->orders()->first());
            } else if ($group_order->orders()->whereIn('status', [5, 10, 301])->count() > 0) {
                $this->closeGroupOrderSendInvoice($group_order);
            } else if ($group_order->orders()->where('status', 301)->count() == 0) {
                (new SendInvoiceNotification)->send($group_order->orders()->first());
            }
            $group_order->next = 0;
            $group_order->save();

            event(new GroupOrderWasFormed($group_order));
        }
        if (\Auth::user()->accessLevel() > 1) {
            return redirect('/orders/history/');
        } else {
            return redirect('/orders/need_attention');
        }
    }

    public function recalculateGroupPrice($group_id)
    {
        $group_order = GroupOrders::find($group_id);
        $price_summ = 0;
        $price_partner_summ = 0;
        $price_expenses = 0;
        $max_payment_date = 0;
        foreach ($group_order->orders as $groupOrder) {
            $price_summ += $groupOrder->price;
            $price_partner_summ += $groupOrder->price_partner;
            $price_expenses += (isset($groupOrder->stopped_reasons['expenses'])) ? $groupOrder->stopped_reasons['expenses'] :
                (isset($groupOrder->issue_troubles['discount']) ? ($groupOrder->price - $groupOrder->issue_troubles['discount']) : $groupOrder->price);
            if ($max_payment_date < $groupOrder->close_date + $groupOrder->price_details['selected_payment_type']['days_for_wait'] * 86400) {
                $max_payment_date = $groupOrder->close_date + $groupOrder->price_details['selected_payment_type']['days_for_wait'] * 86400;
            }
        }
        $group_order->close_date = Carbon::createFromTimestampUTC($max_payment_date)->format('d.m.Y');
        $group_order->price = $price_summ;
        $group_order->price_partner = $price_partner_summ;
        $group_order->price_expenses = $price_expenses;
        if ($group_order->pay >= $group_order->price_expenses) {
            $group_order->status = 6;
        }
        $group_order->save();
    }

    public function closeGroupOrderSendInvoice($group_order)
    {
        $time = time() . "";
        $leftTime = substr($time, 0, 4);
        $rightTime = substr($time, 4, 4);

        $client_info = $group_order->client()->first(['email', 'name', 'last_name', 'user_id']);
        $user = User::find($client_info->user_id);
        $manager = $this->getManager($group_order->manager_id);

        $company_name = settings('email_title') ? settings('email_title') : settings('company_name');
        $agree_template = $group_order->orders()->where('status', 5)->first();
        $choise_template = $group_order->orders()->whereIn('status', [2000, 2010])->first();
        $first_printler = $group_order->orders()->where('status', 10)->first();
        $first_designer = $group_order->orders()->where('status', 300)->first();

        $designer_together_count = $group_order->orders()->where('status', 301)->count();

        if ($group_order->client()->first()->groupOrders()->count() == 1) {
            $data = [
                'user' => $user,
                'token' => PasswordReset::createAuto($user)->token,
            ];

            (new CreatePasswordEmail)->send($user, $data);
        };

        $data = array(
            'subject' => 'Информация по заказу от ' . $company_name,
            'to' => $client_info->email,
            'email' => \Auth::user()->email,
            'name' => $client_info->name,
            'url' => $leftTime . $group_order->id . $rightTime,
            'group_order' => $group_order,
            'manager_contacts' => $manager->contact ? $manager->contact : $manager,
            'login_token' => $user->login_token,
            'agree_template_id' => ($agree_template) ? $agree_template->id : null,
            'choise_template_id' => ($choise_template) ? $choise_template->id : null,
            'printler_id' => ($first_printler) ? $first_printler->id : null,
            'designer_id' => ($first_designer) ? $first_designer->id : null,
            'client' => $client_info,
        );

        //TODO::почему-то слетает manager - поставил проверку на наличие $manager в письме
        if ((\Auth::user()->accessLevel() > 1) && ($designer_together_count != $group_order->orders()->count())) {
            $send = \Mail::send('emails.group_only_invoice', $data, function ($message) use ($data) {
                try {
                    $message->subject($data['subject'])
                        ->to($data['to'])
                        ->from($data['email'], $_SERVER["SERVER_NAME"] . ' - Ваш расчёт');
                } catch (Exception $e) {
                }
            });
        }
        if ((count(\Mail::failures()) == 0) || (\Auth::user()->accessLevel() == 1)) {
            $group_order->invoice_send = 1;
            $group_order->save();
            addLog(0, 500, auth()->user()->id, "Сумма: {$group_order->price}руб.", $group_order->id, 0, "Позиций: {$group_order->orders->count()}");
        }
    }

    public function getManager($user_id)
    {
        $manager = User::query()->where('id', $user_id)->get(['id', 'name', 'lastname', 'photo', 'email', 'phone'])[0];

        if (count($manager) == 0) {
            $manager = json_decode("{}");
            $manager->id = null;
            $manager->name = null;
            $manager->lastname = null;
            $manager->email = null;
            $manager->photo = null;
        }
        return $manager;
    }

    public function checkGuarantee($group_order_id) {
        $group_order = GroupOrders::find($group_order_id);
        if ($group_order->check_guarantee == 0) {
            $group_order->check_guarantee = 1;
            $group_order->save();
        }
        if ($group_order->confirm_image) {
            return $group_order->confirm_image;
        }
        return $group_order->guarantee_image;
    }

    /**
     *  Страница заказа
     */
    public function info($id, $detail_id = 0)
    {
        $user = auth()->user();

        $groupOrder = GroupOrders::with(['logs' => function ($query) {
            $query->latest();
        }])->findOrFail($id);

//        if ($detail_id > 0) {
//            $order->detail_id = $detail_id;
//            $order->save();
//            return redirect('/order/info/' . $id);
//        }

        // $order->client_info = Clients::find($order->client_id);

        //$templates_info = \DB::table('product_types')->find($order->type_product);
        //$order->type_name = $templates_info->name;
        $details = \App\Details::all(['id', 'name']);

        $breadcrumbs[] = 'Заказ';
        $breadcrumbs[] = 'Информация о заказе';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );
        return view('orders.info', [
            'page' => $page,
            'user' => $user,
            'groupOrder' => $groupOrder,
            'details' => $details
        ]);
    }

    /**
     * Страница OrderGroup
     *
     * @param int $orderGroupId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function info2($orderGroupId)
    {
        $groupOrder = GroupOrders::with([
            'client', 'manager', 'logs', 'logs.statusInfo', 'logs.user'
        ])->findOrFail($orderGroupId);

        $page = [
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => ['Заказ', 'Техническое задание']
        ];

        return view('orders.info', compact('page', 'groupOrder'));
    }

    /**
     * Получает номер счёта
     *
     * @param $step2
     * @param $group_order
     * @return mixed
     */
    protected function getInvoiceNumber($step2, $group_order)
    {
        // Заказ создаётся типографией
        if (is_null(Subdomain::findCurrent())) {
            // Получить номер счёта типографии
            if ($group_order->invoice_number) {
                return $group_order->invoice_number;
            }

            $invoiceNumber = settings('invoice_number');
            $group_order->update(['invoice_number' => $invoiceNumber]);
            settings()->set('invoice_number', $invoiceNumber + 1);
            settings()->save();

            return $invoiceNumber;
        }

        // Заказ создаётся партнёром или клиентом партнёра

        if ($step2) {
            if ($group_order->invoice_number_partner) {
                return $group_order->invoice_number_partner;
            }

            $typographySettings = settings()->fromTypography();

            $invoiceNumber = $typographySettings->get('invoice_number');
            $group_order->update(['invoice_number_partner' => $invoiceNumber]);
            $typographySettings->set('invoice_number', $invoiceNumber + 1);
            $typographySettings->save();

            return $invoiceNumber;
        }

        if ($group_order->invoice_number) {
            return $group_order->invoice_number;
        }

        $invoiceNumber = settings()->get('invoice_number');
        $group_order->update(['invoice_number' => $invoiceNumber]);
        settings()->set('invoice_number', $invoiceNumber + 1);
        settings()->save();

        return $invoiceNumber;
    }

    /**
     * Получает номер акта
     *
     * @param $step2
     * @param $group_order
     * @return mixed
     */
    protected function getActNumber($step2, $group_order)
    {
        if ($step2) {
            $typographySettings = settings()->fromTypography();
            $actNumber = $typographySettings->get('act_number');
            $group_order->update(['invoice_number_partner' => $actNumber]);
            $typographySettings->set('act_number', $actNumber + 1);
            $typographySettings->save();
        } else {
            $actNumber = settings('act_number');
            settings()->set('act_number', $actNumber + 1);
            settings()->save();
        }
        return $actNumber;
    }

    /**
     * Confirm payment
     *
     * @param int $groupOrderId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function closeDocLoad($groupOrderId, Request $request)
    {
        foreach ($request->images as $key => $image) {
            $finfo = new finfo(FILEINFO_MIME);
            $mimeType  = explode(';', $finfo->file($image))[0];
            if (!in_array($mimeType, ['image/jpeg','application/pdf','image/png'])) {
                return 'Один из файлов имеет неподдерживаемый формат';
            }
        }
        $groupOrder = GroupOrders::find($groupOrderId);
        $close_docs = $groupOrder->close_docs;
        if (!isset($close_docs)) {
            $close_docs = [];
        }

        foreach ($request->images as $key => $image) {
            $imagePath = $this->makeImageFromRequest($request, $image);

            if (!$imagePath) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Ошибка загрузки файла',
                ]);
            }
            array_push($close_docs, $imagePath);
        }
        $groupOrder->close_docs = $close_docs;
        $groupOrder->save();

        if (count($groupOrder->close_docs) == $groupOrder->issued_act) {
            $this->status($groupOrder->id, 100);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Изображение(-я) успешно загружено(-ы)',
            'all' => (count($groupOrder->close_docs) == $groupOrder->issued_act),
        ]);
    }

    public function closeDocActPrint($group_order_id) {
        $group_order = GroupOrders::find($group_order_id);
        $group_orders = $group_order->orders()
            ->whereNull('parent_id')->where('issued', '>', 0)->where('defect_reason','=','')->get();
        if (count($group_orders) > 0) {
            $step2 = (isset($group_order->subdomain_id)) ? true : false;

            if ($step2) {
                $client = $group_order->partnerAsClient();
            } else {
                $client = $group_order->client()->first();
            }
            $payer = $client->representative();

            $act_number = $this->getActNumber($step2, $group_order);
            $invoice_number = $this->getInvoiceNumber($step2, $group_order);
            $time_stamp = time();

            $detail = Details::find($group_order->detail_id);
            $pdf = App::make('dompdf.wrapper');
            $html_doc = view('documents.act', [
                'detail' => $detail,
                'company_details' => $client->company_details,
                'invoice_number' => $invoice_number,
                'act_number' => $act_number,
                'payer' => $payer,
                'group_orders' => $group_orders,
                'issued_act' => $group_order->issued_act,
                'step2' => $step2,
            ])->render();


            $pdf->loadHTML($html_doc, 'UTF-8');
            $path = 'orders_pdf/order_n_' . $invoice_number . '_act_' . $time_stamp . '.pdf';
            $pdf->save(public_path($path));
            $group_order->issued_act = $group_order->issued_act + 1;
            $group_order->save();
            addLog(0, 1, $group_order->manager->id, link_to($path, 'Акт'),
                $group_order->id, 0, "Выдан акт");

            if (is_file(public_path($path))) {
                return url($path);
            }
        }
        return '';
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function makeImageFromRequest(Request $request, $file = null)
    {
        if ($file) {
            $image = $file;
        } else {
            $image = $request->file('image');
        }

        $uploadDir = "/upload/group-orders-files/";
        $filename = str_random();
        $extension = $image->getClientOriginalExtension();

        if (!file_exists(public_path($uploadDir))) {
            mkdir(public_path($uploadDir), 0777, true);
        }

        if ($image->move(public_path($uploadDir), "{$filename}.{$extension}")) {
            return $uploadDir.$filename.".".$extension;
        }

        return false;
    }

    public function sendToIssue(Request $request)
    {
        $group_order = GroupOrders::find($request->group_id);
        foreach ($group_order->orders as $order) {
            if (isset($order->control_quality)) {
                if (is_null($order->selected_issue_address) && (strlen($order->price_details['selected_address']) > 0) && ($order->price_details['selected_address'] != settings('printingAddresses')[0])) {
                    $order->selected_issue_address = $order->price_details['selected_address'];
                    $order->status = 201;
                    addLog($order->id, 201, auth()->user()->id, 'В пути', null, 0, $order->price_details['selected_address']);
                } else {
                    if (strlen($order->price_details['selected_address']) == 0) {
                        $order->selected_issue_address = settings('printingAddresses')[0];
                    }
                    $order->status = 51;
                    addLog($order->id, 51, auth()->user()->id, '', null, 0, '');
                }
                $order->save();
            }
        }
        (new ReadyOrderEmail)->send($group_order);
        return Redirect::back()->with('selected_group_order_id', $request->group_id);
    }

}
