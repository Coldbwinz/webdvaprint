<?php

namespace App\Http\Controllers\Designer;

use App\Jobs\ParseLibraryTemplate;
use App\Theme;
use App\ThemeColor;
use App\ProductType;
use App\TemplateLibrary;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    /**
     * @var TemplateLibrary
     */
    protected $templateLibrary;

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @param TemplateLibrary $templateLibrary
     * @param Guard $auth
     */
    public function __construct(TemplateLibrary $templateLibrary, Guard $auth)
    {
        $this->templateLibrary = $templateLibrary;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = $this->templateLibrary
            ->with(['themes', 'colors'])
            ->has('owner')->get();

        $page = [
            'menu' => 'options',
            'submenu' => 'client_types',
            'breadcrumbs' => ['Шаблоны', '']
        ];

        return view('designer.template.index', compact('page', 'templates'));
    }

    /**
     * Upload new template page
     *
     * @param Theme $theme
     * @param ThemeColor $themeColor
     * @param ProductType $productType
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Theme $theme, ThemeColor $themeColor, ProductType $productType)
    {
        $themes = $theme->active()->get();
        $themeColors = $themeColor->all();
        $products = $productType->all();

        $page = [
            'menu' => 'templates',
            'submenu' => 'create',
            'breadcrumbs' => ['Шаблоны', 'Загрузить']
        ];

        return view('designer.template.create', compact('page', 'themes', 'themeColors', 'products'));
    }

    /**
     * Store uploaded template
     *
     * @param Request $request
     * @param ProductType $productType
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, ProductType $productType)
    {
        $this->validate($request, [
            'psd_file' => 'required|mimes:psd',
            'product_type_id' => 'required|exists:product_types,id',
        ]);

        $user = $this->auth->user();

        $productType = $productType->find($request->input('product_type_id'));

        $collection = str_slug($user->name);

        $psdFile = $request->file('psd_file');
        $psdFile = $psdFile->move(storage_path("app/psd_library/{$collection}"), $psdFile->getClientOriginalName());

        $template = $user->templates()->create([
            'status' => 'new',
            'psd' => $psdFile->getPathname(),
            'collection' => $collection,
            'type_key' => $productType->key,
            'activity' => false,
        ]);

        if ($request->has('themes')) {
            $template->themes()->sync($request->input('themes'));
        }

        if ($request->has('colors')) {
            $template->colors()->sync($request->input('colors'));
        }

        $this->dispatch(new ParseLibraryTemplate($template));

        return redirect()->route('templates');
    }

    /**
     * Template approve page
     *
     * @param TemplateLibrary $template
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview(TemplateLibrary $template)
    {
        $page = [
            'menu' => 'templates',
            'submenu' => 'create',
            'breadcrumbs' => ['Шаблоны', 'Утвердить']
        ];

        return view('designer.template.approve', compact('page', 'template'));
    }

    /**
     * Утверждение шаблона
     *
     * @param TemplateLibrary $template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(TemplateLibrary $template)
    {
        if ($template->activity) {
            return redirect()->route('templates');
        }

        $template->update([
            'activity' => true
        ]);

        session()->flash('message.success', 'Шаблон утверждён');

        return redirect()->route('templates');
    }

    /**
     * @param TemplateLibrary $template
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(TemplateLibrary $template)
    {
        if ($template->orders->count() === 0) {
            $template->delete();

            session()->flash('message.success', 'Шаблон удалён.');

            return redirect()->route('templates');
        }

        session()->flash('message.danger', 'Невозможно удалить шаблон, т.к. к нему привязаны заказы.');

        return redirect()->route('templates');
    }
}
