<?php

namespace App\Http\Controllers;

use App\Emails\CreatePasswordEmail;
use App\Orders;
use App\PasswordReset;
use File;
use Input;
use App\User;
use Validator;
use App\UserGroup;
use Illuminate\Http\Request;
use App\Events\AccountWasCreated;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::with(['group', 'contact'])
            ->whereHas('group', function ($query) {
                $query->where('id', '!=', UserGroup::ROOT);
            })
            ->latest()
            ->paginate(20);

        $users_groups = DB::table('users_groups')->get();

        $breadcrumbs[] = 'Справочник';
        $breadcrumbs[] = 'Пользователи';
        $page = [
            'menu' => 'contractors',
            'submenu' => 'users',
            'breadcrumbs' => $breadcrumbs,
        ];
        return view('users.index', [
            'page' => $page,
            'users' => $users,
            'users_groups' => $users_groups,
        ]);
    }

    public function create(Request $request)
    {
        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';
        $validator = Validator::make($request->all(), [
            'id_group' => 'required',
            'name' => 'required',
            'lastname' => 'required',
            'position' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,subdomain_id,' . $subdomain_id,
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/users')->withInput()->withErrors($validator);
        }

        $password = str_random();
        $user = $this->createAuto($request->input('id_group'), Auth::user()->id, $request->name, $request->lastname, $request->phone, $request->email, $password, $request->position);

        // Create contact
        $user->contact()->create([
            'name' => $request->input('name'),
            'last_name' => $request->input('lastname'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'description' => '',
        ]);

        $password = $request->input('password');

        if (isset($request->photo)) {
            if (strlen($request->imagebase64) > 0) {
                $user->photo = $this->resizeImageAndSave($request->imagebase64, $_SERVER['DOCUMENT_ROOT'] . '/upload/users/');
            }
        }

        $user->save();

        // Создание записи на сброс пароля
        PasswordReset::createAuto($user);

        event(new AccountWasCreated($user, $password));

        return redirect('/users');
    }

    /**
     * @param $group_id
     * @param $manager_id
     * @param $name
     * @param $lastname
     * @param $phone
     * @param $email
     * @param $password
     * @param $position
     * @return User
     */
    public static function createAuto($group_id, $manager_id, $name, $lastname, $phone, $email, $password, $position)
    {
        $user = new User();
        $user->id_group = $group_id;
        $user->id_manager = $manager_id;
        $user->name = $name;
        $user->lastname = $lastname;
        $user->phone = $phone;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->position = $position;
        $user->login_token = str_random(60);
        $user->login_token_created_at = microtime(true);
        $user->save();
        return $user;
    }

    public function edit($id)
    {
        $user = User::find($id);
        $users_groups = DB::table('users_groups')->get();

        $breadcrumbs[] = 'Справочник';
        $breadcrumbs[] = 'Пользователи';
        $breadcrumbs[] = 'Редактирование';
        $page = array(
            'menu' => 'contractors',
            'submenu' => 'users',
            'breadcrumbs' => $breadcrumbs,
        );
        return view('users.edit', [
            'page' => $page,
            'user' => $user,
            'users_groups' => $users_groups,
        ]);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        if (!$user) {
            abort(404, 'User not found');
        }

        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';

        $validator = Validator::make($request->all(), [
            'id_group' => 'required',
            'name' => 'required',
            'lastname' => 'required',
            'position' => 'required',
            'email' => 'required|email|unique:users,email,' . $id . ',id,subdomain_id,' . $subdomain_id,
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        if (isset($request->photo)) {
            if (strlen($request->imagebase64) > 0) {
                $user->photo = $this->resizeImageAndSave($request->imagebase64, $_SERVER['DOCUMENT_ROOT'] . '/upload/users/');
            }
        }

        $user->id_group = $request->id_group;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->position = $request->position;
        $user->email = $request->email;
        $user->phone = $request->phone;

        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        if (strlen($password) >= 6) {
            if ($password == $password_confirmation) {
                $user->password = bcrypt($password);
            }
        }

        $user->save();

        return redirect('/users');
    }

    public function on($id)
    {
        DB::update('UPDATE users SET activity = "1" WHERE id = ?', [$id]);
        return redirect('/users');
    }

    public function off($id)
    {
        DB::update('UPDATE users SET activity = "0" WHERE id = ?', [$id]);
        return redirect('/users');
    }

    public function resizeImageAndSave($file, $upload_folder)
    {
        if (!file_exists($upload_folder)) {
            mkdir($upload_folder, 0777, true);
        }

        list($type, $file) = explode(';', $file);
        list(, $file) = explode(',', $file);

        $file = base64_decode($file);

        $name = md5(time() . rand(1000, 9999));
        $previewName = $name . '.png';
        $preview = $upload_folder . $previewName;
        file_put_contents($preview, $file);

        File::delete($file);

        return $previewName;
    }

    public function loginByToken($email, $token)
    {
        $user = User::where('login_token', '=', $token)->where('email', '=', $email)->first();
        if (!empty($user)) {
            Auth::login($user);
            return Redirect::to($this->checkFirstNeedToAccepted());
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function loginByTokenApprove($email, $token, $order_id)
    {
        $user = User::where('login_token', '=', $token)->where('email', '=', $email)->first();
        if (!empty($user)) {
            Auth::login($user);
            return Redirect::to($this->checkFirstNeedToAccepted($order_id));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function loginByTokenToPayments($email, $token) {
        $user = User::where('login_token', '=', $token)->where('email', '=', $email)->first();
        if (!empty($user)) {
            Auth::login($user);
            return Redirect::to('/orders/payments');
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    function checkFirstNeedToAccepted($order_id) {
        $clients = \Auth::user()->contact->clients()->get();
        $clientIds = $clients->pluck('id')->toArray();

        if (isset($order_id)) {
            $orders = Orders::where('id',$order_id)->get();
            $url = '/orders/need_attention_with_message';
        } else {
            $orders = Orders::select(['orders.id', 'orders.status', 'orders.file', 'orders.use_specification'])
                ->join('clients as c', function ($query) use ($clientIds) {
                    $query->on('c.id', '=', 'client_id')
                        ->whereIn('c.id', $clientIds);
                })
                ->where(function ($query) {
                    $query->whereIn('status', [4, 5, 9, 10, 300, 2000, 2010]);
                })->get();
            $url = '/orders/need_attention';
        }
        foreach ($orders as $order) {
            if (in_array($order->status, [4, 5, 9])) {
                if ($order->file) {
                    return '/order/reviwer/0/' . $order->id;
                } else {
                    return '/order/reviwer/0/' . $order->id . '?editable=false';
                }
                break;
            }
        }
        foreach ($orders as $order) {
            if (in_array($order->status, [2000, 2010])) {
                return '/orders/post-step2/' . $order->id;
                break;
            }
        }

        foreach ($orders as $order) {
            if ($order->status == 10) {
                return '/loading/' . random_int(1000, 9999) . $order->id . random_int(1000, 9999);
                break;
            }
        }
        foreach ($orders as $order) {
            if ($order->status == 300) {
                return 'order/' . $order->id . '/specification/create';
                break;
            }
        }
        return $url;
    }
}
