<?php

namespace App\Http\Controllers;

use Validator;
use App\Details;
use App\Subdomain;
use Illuminate\Http\Request;
use App\Services\SettingsValidator;
use App\Http\Requests\DetailRequest;
use App\Facades\SubdomainEnvironment;

class DetailsController extends Controller
{
    /**
     * @var Details
     */
    private $detail;

    /**
     * @var Subdomain
     */
    private $subdomain;

    /**
     * @param Details $detail
     * @param Subdomain $subdomain
     */
    public function __construct(Details $detail, Subdomain $subdomain)
    {
        $this->detail = $detail;
        $this->subdomain = $subdomain;
    }

    /**
     * Store new detail in the storage
     *
     * @param DetailRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DetailRequest $request)
    {
        $detail = $this->detail->create($request->all());
        $subdomain = Subdomain::findCurrent();

        if ($subdomain) {
            $detail->subdomain()->associate($subdomain)->save();
        }

        SettingsValidator::updateValidationFlag();

        session()->flash('message.success', "Реквизиты для {$detail->name} добавлены.");

        return redirect()->back();
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $details = $this->detail
            ->forSubdomain(Subdomain::findCurrent())
            ->where('id', $id)
            ->firstOrFail();

        $page = [
            'menu' => 'options',
            'submenu' => 'details',
            'breadcrumbs' => ['Настройки', 'Реквизиты']
        ];

        return view('options.details.edit', compact('page', 'details'));
    }

    /**
     * @param $id
     * @param DetailRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, DetailRequest $request)
    {
        $detail = $this->detail
            ->forSubdomain(Subdomain::findCurrent())
            ->where('id', $id)
            ->firstOrFail();

        $detail->update($request->all());

        SettingsValidator::updateValidationFlag();

        session()->flash('message.success', "Реквизиты для {$detail->name} обновлены.");

        return redirect('/options/parameters');
    }

    public function on($id)
    {
        $details = Details::find($id);
        $details->activity = 1;
        $details->save();

        return redirect('/options/parameters');
    }

    public function off($id)
    {
        $details = Details::find($id);
        $details->activity = 0;
        $details->save();

        return redirect('/options/parameters');
    }
}
