<?php

namespace App\Http\Controllers;

use App\Orders;

class ReworkOrdersController extends Controller
{
    /**
     * Применяет дочерний заказ-доработку к основному заказу
     * и удаляет дочерний заказ
     *
     * @param Orders $reworkOrder
     * @return \Illuminate\Http\RedirectResponse
     */
    public function apply(Orders $reworkOrder)
    {
        $parentOrder = $reworkOrder->parent;

        // Добавить логи к заказу-родителю
        $parentOrder->appendLogs($reworkOrder->logs);

        // Заказ-родитель получает статус "Готов" и метку "Переделан"
        $parentOrder->status = status('ready')->id;
        $parentOrder->reworked = true;
        $parentOrder->save();

        // Удалить дочерний заказ, его элементы и логи.
        $reworkOrder->logs()->delete();
        $reworkOrder->elements()->delete();
        $reworkOrder->delete();

        return back();
    }
}
