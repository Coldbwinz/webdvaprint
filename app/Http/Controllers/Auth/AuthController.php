<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Hash;
use App\User;
use App\Subdomain;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers;

    private $subdomain;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);

        $this->subdomain = Subdomain::findCurrent();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, $this->rules(), $this->messages(), $this->customAttributes());

        $throttles = false;

        $field = $this->loginUsername();

        $user = User::where($field, $request->get($field))
                    ->forSubdomain($this->subdomain)
                    ->first();

        if ($user && Hash::check($request->get('password'), $user->password)) {
            Auth::login($user, $request->has('remember'));
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // return with auth failed
        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return 'email';
    }

    /**
     * Get current subdomain_id
     *
     * @return integer
     */
    protected function getSubdomainId()
    {
        return $this->subdomain_id;
    }

    /**
     * Validation rules
     *
     * @return array
     */
    protected function rules()
    {
        //$subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        return [
            //'email' => 'required|email|exists:users,' . $this->loginUsername() .',subdomain_id,' . $subdomain_id,
            'password' => 'required',
        ];
    }

    /**
     * Validation field attributes (display names)
     *
     * @return array
     */
    protected function customAttributes()
    {
        return [
            'email' => 'Электронная почта',
            'password' => 'Пароль',
        ];
    }

    /**
     * Validation error messages
     *
     * @return array
     */
    protected function messages()
    {
        return [
            'email.exists' => 'Ошибка авторизации.<br>Такой пользователь не зарегистрирован',
        ];
    }

}
