<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Validator;
use Carbon\Carbon;
use App\User;
use App\Subdomain;
use App\PasswordReset;
use App\Emails\RestorePasswordEmail;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\ForgotPasswordSendRequest;
use App\Http\Requests\Auth\ForgotPasswordStoreRequest;

use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{

    protected $subdomain;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->subdomain = Subdomain::findCurrent();
    }

    /**
     * Start restore password
     *
     * @return Illuminate\Http\Response
     */
    public function restore()
    {
        return view('auth.password.restore');
    }

    /**
     * Send email with link to restore password
     *
     * @param ForgotPasswordSendRequest $request
     * @return Illuminate\Http\Response
     */
    public function send(ForgotPasswordSendRequest $request)
    {
        $user = User::where('email', $request->email)
                    ->forSubdomain($this->subdomain)
                    ->first();

        if (!$user) {
            return redirect()->route('auth.password.restore')->withInput();
        }

        // create token
        $reset = PasswordReset::createAuto($user);

        $data = [
            'user' => $user,
            'token' => $reset->token,
        ];

        // send mail
        (new RestorePasswordEmail)->send($user, $data);

        return redirect()->route('auth.password.info', $reset->token);
    }

    /**
     * Страница подтверждения об отправке
     *
     * @return Illuminate\Http\Response
     */
    public function info($token)
    {
        $reset = PasswordReset::where('token', $token)->first();
        if (!$reset) {
            abort(404);
        }

        $user = $reset->user()->forSubdomain($this->subdomain)->first();
        if (!$user) {
            abort(404);
        }

        return view('auth.password.info', compact('user', 'token'));
    }

    /**
     * Send new password
     *
     * @param string $token
     * @return Illuminate\Http\Response
     */
    public function newPassword($token)
    {
        $reset = PasswordReset::where('token', $token)->first();
        if (!$reset) {
            abort(404);
        }
        $user = $reset->user()->first();
        if (!$user) {
            abort(404);
        }

        return view('auth.password.new', [
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * [createPassword description]
     *
     * @param  string $token
     * @return Illuminate\Http\Response
     */
    public function createPassword($token)
    {
        $reset = PasswordReset::where('token', $token)->first();
        if (!$reset) {
            abort(404);
        }
        $user = $reset->user()->first();
        if (!$user) {
            abort(404);
        }

        return view('auth.password.create', [
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * Save new password
     *
     * @param ForgotPasswordStoreRequest $request
     * @param string $token
     * @return Illuminate\Http\Response
     */
    public function store(ForgotPasswordStoreRequest $request, $token)
    {
        $reset = PasswordReset::where('token', $token)->first();
        if (!$reset) {
            abort(404);
        }
        $user = $reset->user()->first();
        if (!$user) {
            abort(404);
        }

        // save password
        $user->password = bcrypt($request->password);
        $user->save();

        // save token
        $reset->activate = true;
        $reset->activate_at = Carbon::now()->toDateTimeString();
        $reset->save();

        Auth::login($user);

        return redirect()->route('home');
    }

}
