<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\UserGroup;
use App\Clients;
use App\Contacts;
use App\Subdomain;
use App\PasswordReset;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Emails\CreatePasswordEmail;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration  Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    | TODO: Class-заготовка под самостоятельную регистрацию юзера.
    | Если понадобиться открытая регистрация.
    |
    */

    use RegistersUsers;

    protected $subdomain;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);

        $this->subdomain = Subdomain::findCurrent();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $username = $this->loginUsername();
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,' . $username.',NULL,id,subdomain_id,' . $subdomain_id,
            //'phone' => 'required|min:10',
            'company_name' => 'required_if:is_company,1',
            'client_type' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        // Создаем клиента
        $client = new Clients();

        $client->tm_name = $data['company_name'];
        $client->name = $data['name'];
        $client->last_name = $data['last_name'];

        if ($data['is_company']) {
            $client->is_company = true;
        }

        $clientTypeId = $data['client_type'];

        $currentSubdomainId = null;

        // Посредник создаёт клиента
        if ($currentSubdomain = Subdomain::findCurrent()) {
            // Название типа клиента по умолчанию
            $clientTypeName = 'Новый клиент';

            if ($data['client_type'] == 3) {
                $clientTypeName = 'Посредник';
            }

            // Т.к. у посредника свои типы клиентов, ищем нужный среди его типов
            $clientType = ClientTypes::where('name', $clientTypeName)
                ->where('subdomain_id', $currentSubdomain->id)
                ->first();

            $clientTypeId = $clientType->id;

            $currentSubdomainId = $currentSubdomain->id;
        }

        $defaultManagerId = settings('widget_registration_manager_id');
        $manager_id = $defaultManagerId ?: User::whereIn('id_group', [2, 3])->where('subdomain_id', $currentSubdomainId)->first()->id;

        $client->email = $data['email'];
        $client->phone = $data['phone'];
        $client->type_id = $clientTypeId;
        $client->status_id = 3;
        $client->manager_id = $manager_id;
        $client->subdomain_id = $currentSubdomainId;

        // Заказчик и посредник имеют роль "Пользователь"
        $group_id = UserGroup::CLIENT;
        $password_temp = str_random(6);
        $user = \App\Http\Controllers\UsersController::createAuto($group_id, $client->manager_id, $client->name, '', $client->phone, $client->email, $password_temp, '');

        $client->user_id = $user->id;
        $client->save();

        $user->password_temp = $password_temp;
        $user->save();

        // Создаем контакт
        $contact = new Contacts;
        $contact->user_id = $user->id;
        $contact->name = $data['name'];
        $contact->last_name = $data['last_name'];
        $contact->email = $data['email'];
        $contact->phone = $data['phone'];
        $contact->is_company = $data['is_company'];
        $contact->save();

        $position = $data['position'] ?: 'Частное лицо';
        $contact->setInformationFieldForClient($client->id, 'position', $position);

        // Создаем связь
        $relation = new \App\ContactsClientsRelation;
        $relation->contact_id = $contact->id;
        $relation->client_id = $client->id;
        $relation->save();

        // create token
        $reset = PasswordReset::createAuto($user);

        $data = [
            'user' => $user,
            'token' => $reset->token,
        ];

        // send mail
        (new CreatePasswordEmail)->send($user, $data);

        return $user;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return 'email';
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }

}
