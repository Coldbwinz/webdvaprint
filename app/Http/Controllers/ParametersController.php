<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Emails\SendInvoiceNotification;
use App\Orders;
use Exception;
use App\Details;
use App\Subdomain;
use App\Services\LogoImage;
use Illuminate\Http\Request;
use App\Services\SettingsValidator;
use App\Http\Requests\SettingsUpdateRequest;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsContractor;

class ParametersController extends Controller
{
    /**
     * @var SubdomainSettingsContractor
     */
    private $settings;

    /**
     * @var Details
     */
    private $detail;

    /**
     * @param SubdomainSettingsContractor $settings
     * @param Details $detail
     */
    public function __construct(SubdomainSettingsContractor $settings, Details $detail)
    {
        $this->settings = $settings;
        $this->detail = $detail;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $details = $this->detail->forSubdomain(
            Subdomain::findCurrent()
        )->first();

        $page = [
            'menu' => 'options',
            'submenu' => 'parameters',
            'breadcrumbs' => ['Настройки', 'Параметры'],
        ];
        return view('options.parameters.index', [
            'page' => $page,
            'settings' => $this->settings,
            'details' => $details,
        ]);
    }

    /**
     * Update settings
     *
     * @param SettingsUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SettingsUpdateRequest $request)
    {
        $allOK = false;
        $availableSettings = [
            'company_name', 'company_short_name', 'user_timezone', 'country', 'color_primary', 'email_title',
            'color_secondary', 'issuingAddresses', 'printingAddresses', 'meta_title', 'meta_description',
            'invoice_number','act_number'
        ];

        foreach ($request->all() as $key => $value) {
            if (! in_array($key, $availableSettings)) {
                continue;
            }

            $this->settings->set($key, $value);
        }
        $this->settings->save();
        SettingsValidator::updateValidationFlag();

        $details = $this->detail->forSubdomain(
            Subdomain::findCurrent()
        )->first();

        if (!isset($details)) {
            $details = new Details();
            $details->subdomain_id = Subdomain::findCurrent()->id;
        }
        $details->name = $request->name;
        $details->full_name = $request->full_name;
        $details->director = $request->director;
        $details->director_position = $request->director_position;
        $details->inn = $request->inn;
        $details->kpp = $request->kpp;
        $details->ogrn = $request->ogrn;
        $details->address = $request->address;
        $details->post_address = $request->post_address;

        $details->bank_name = $request->bank_name;
        $details->bank_rs = $request->bank_rs;
        $details->bank_bik = $request->bank_bik;
        $details->bank_ks = $request->bank_ks;

        $details->save();
        //Проверить на заполнение
        $allOK = true;
        if ($allOK) {
            return redirect('/dashboard');
        } else {
            return redirect()->back()->withErrors();
        }
    }

    /**
     * Upload logo
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logo(Request $request)
    {
        try {
            $logo = LogoImage::makeFromRequest($request);

            $this->settings->set('logo', $logo);
            $this->settings->save();

            return response()->json([
                'status' => 'success',
                'image' => $logo,
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function getINNdata(Request $request, $encode = true) {
        $client = new \Dadata_Client(array('api_key' => '889aa977a268475ba6f0fc9e65ee4ecd6b00a4f0', 'content_type' => 'application/json'));
        $rest = new \Dadata_Service_Rest($client);
        $suggest = $rest->suggest->party(array('query' => $request->inn));
        $sendObj = new \stdClass();
        foreach($suggest->getSuggestions() as $party){
            $sendObj->data = $party->getData();
            $sendObj->address = $party->getData()->getAddress();
            $sendObj->address->data = $party->getData()->getAddress()->getData();
            break;
        }
        if ($encode) {
            return json_encode($sendObj);
        }
        return $sendObj;
    }

    public function getBIKdata(Request $request, $encode = true) {
        $client = new \Dadata_Client(array('api_key' => '889aa977a268475ba6f0fc9e65ee4ecd6b00a4f0', 'content_type' => 'application/json'));
        $rest = new \Dadata_Service_Rest($client);
        $suggest = $rest->suggest->bank(array('query' => $request->bik));
        $sendObj = new \stdClass();
        foreach($suggest->getSuggestions() as $bank){
            $sendObj->full_data = $bank->getData();
            break;
        }
        if ($encode) {
            return json_encode($sendObj);
        }
        return $sendObj;
    }
    
    public function setAllData($client_id, Request $request) {
        $client = Clients::find($client_id);
        $allData = new \stdClass();
        $allData->company = $this->getINNdata($request, false);
        $allData->bank = $this->getBIKdata($request, false);
        $allData->rs = $request->rs;

        if ($client->company_details) {
            $correctData = (object) $client->company_details;
        } else {
            $correctData = new \stdClass();
        }
        if (isset($allData->company->data)) {
            $correctData->name = $allData->company->data->name['short_with_opf'];
            $correctData->full_name = $allData->company->data->name['full_with_opf'];
            $correctData->ogrn = $allData->company->data->ogrn;
            $correctData->type = $allData->company->data->type;
            if ($correctData->type == "INDIVIDUAL") {
                $correctData->director = $allData->company->data->name['full'];
                $correctData->director_position = $allData->company->data->opf['full'];
                $correctData->address = '';
                $correctData->kpp = '0';
            } else {
                $correctData->director = $allData->company->data->management['name'];
                $correctData->director_position = $allData->company->data->management['post'];
                $correctData->address = $allData->company->data->address->data->postal_code.
                    ', '.$allData->company->data->address->data->country.
                    ', '.$allData->company->data->address->unrestrictedValue;
                $correctData->kpp = $allData->company->data->kpp;
            }
            $correctData->post_address = '';
            $client->legal_name = $allData->company->data->name['full'];
            $client->ownership_type = $allData->company->data->opf['short'];
            $correctData->legal_name = $allData->company->data->name['full'];
            $correctData->ownership_type = $allData->company->data->opf['short'];
            $client->address = '';
        }

        if (isset($allData->bank->full_data)) {
            $correctData->bank_name = $allData->bank->full_data->name['full'];
            $correctData->bank_ks =  $allData->bank->full_data->correspondentAccount;
        }

        if (isset($request->inn)) {
            $correctData->inn = $request->inn;
        }
        if (isset($request->bik)) {
            $correctData->bank_bik = $request->bik;
        }
        if (isset($request->rs)) {
            $correctData->bank_rs = $request->rs;
        }

        $correctData->all_ok = (isset($correctData->name) && isset($correctData->bank_name) && (strlen($correctData->bank_rs) > 0));
        $correctData->all_ok_info = json_encode((object)
        ['inn' => (isset($correctData->name)) ? 1 : 'Неправильно указан ИНН или не найден в базе',
         'rs' => (isset($correctData->bank_rs) && (strlen($correctData->bank_rs) > 0)) ? 1 : 'Неправильно указан Расчётный счёт',
         'bik' => (isset($correctData->bank_name)) ? 1 : 'Неправильно указан БИК или не найден в базе']);
        $client->company_details = $correctData;
        $client->save();

        if (isset($request->order_id)) {
            $order = Orders::find($request->order_id);
            (new SendInvoiceNotification)->send($order);
        }
        return json_encode($correctData);
    }

    //Памятка по работе с данными Dadata
    /*Получение данных об организации по ИНН
     $suggest = $rest->suggest->party(array('query' => '7604254988'));
     foreach($suggest->getSuggestions() as $party){
     $organization = $party->getData(); // получение данных об организации
     $name = $organization->getValue(); // Наименование организации одной строкой (для списка подсказок)
     $nameFull = $organization->getUnrestrictedValue(); // Наименование организации одной строкой (полное)
     $address = $organization->getAddress(); // получение адреса организации

     Получение адреса

     $suggest = $rest->suggest->address(array('query' => 'Ярославль Победы 38', 'count' => 10));
     foreach($suggest->getSuggestions() as $address){
     $value = $address->getValue(); // Адрес одной строкой (как показывается в списке подсказок)
     $unrestrictedValue = $address->getUnrestrictedValue(); // Адрес одной строкой (полный, от региона)
     $data = $address->getData(); // Получение подробных данных об адресе в виде объекта.

     Получение данных о банке по БИК

     $suggest = $rest->suggest->bank(array('query' => '046577964'));
     foreach($suggest->getSuggestions() as $bank){
     $bankName = $bank->getValue(); // Наименование банка одной строкой (для списка подсказок)
     $bankNameFull = $bank->getUnrestrictedValue(); // Наименование банка одной строкой (полное)
     $data = $item->getData(); // Подробные данные о банке.
     $bankAddress = $item->getData()->getAddress(); // Адрес банка
     $rkc = $item->getData()->getRkc(); // Расчетно-кассовый центр. Объект такой же структуры, как сам банк
     $rkcAddress = $item->getData()->getRkc()->getAddress(); // Адрес расчетно-кассового центра

     Определение геопозиции по IP

     $geo = $rest->geo->location(array('ip' => '128.74.215.229'));
     $location = $geo->getLocation();
     $address = $location->getValue(); // Получение адреса одной строкой (для списка подсказок)
     $addressFull = $location->getUnrestrictedValue(); // Получение адреса одной строкой (полного)
     $data = $location->getData(); // Получение подробных данных об адресе в виде объекта
     $coords = $data->getCoords(); // Получение точных координат в виде строки (lat,lon)
     */
}
