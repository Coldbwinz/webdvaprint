<?php

namespace App\Http\Controllers;

use App\GroupOrders;
use App\Subdomain;
use App\UserGroup;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Orders;
use App\Clients;
use App\Order_logs;

class CrmController extends Controller
{

    public function dashboard() {
        $user = Auth::user();
        
        // редарект для пользователей
        if ($user->id_group == 1) {
            return redirect('/orders/need_attention');
        }

        if ($user->hasGroup(UserGroup::DIRECTOR)) {
            return redirect('/orders/turn_orders');
        }

        //список заказов, последние 8
        $orders = Orders::orderBy('updated_at','DESC')->limit(8)->get();

        foreach ($orders as $key => $value) {
            $orders[$key]->client_info = Clients::find($value->client_id);

            $templates_info = \DB::table('product_types')->find($value->type_product);
            if (count($templates_info) == 1) {
                $orders[$key]['type_name'] = $templates_info->name;
            } else {
                $orders[$key]['type_name'] = $value->price_details['selected_product_type_name'];
            }
        }


        $breadcrumbs[] = 'Dashboard';
        $breadcrumbs[] = 'Контрольная панель';
        $page = array(
            'menu' => 'dashboard',
            'submenu' => '',
            'breadcrumbs' => $breadcrumbs
        );
        return view('dashboard.index', [
            'page' => $page,
            'user' => $user,
            'orders' => $orders
        ]);
    }

}
