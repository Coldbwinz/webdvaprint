<?php

namespace App\Http\Controllers;

use App\ProductType;
use App\Services;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\Parser\Frx;

use Auth;
use File;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Orders;
use App\Clients;
use App\Theme;
use App\Templates;
use App\TemplateLibrary;
use App\Order_logs;
use Log;

class iModuleController extends Controller
{
    public function firstStep($company_id)
    {
        $user = Auth::user();

        $product_types = \DB::table('product_types')->get();

        $price_tables = [];
        foreach ($product_types as $product_info) {
            if ($product_info->services_id > 0) {
                $newObject = (object)[];
                $serviceNames = [];
                $serviceNamesOpt = [];
                $serviceCounts = [];
                $servicePrices = [];
                $services = Services::where('id', $product_info->services_id)->groupBy('count')->get();
                foreach ($services as $service) {
                    array_push($serviceCounts, (object)array('count' => $service->count));
                }
                $services = Services::where('id', $product_info->services_id)->groupBy('name')->orderBy('option')->get();
                foreach ($services as $service) {
                    if (!$service->option) {
                        array_push($serviceNames, (object)array('name' => $service->name, 'option' => $service->option));
                    } else {
                        array_push($serviceNamesOpt, (object)array('name' => $service->name, 'option' => $service->option));
                    }
                }

                $services = Services::where('id', $product_info->services_id)->get();
                foreach ($services as $service) {
                    array_push($servicePrices, (object)array('price' => $service->price * 1));
                }
                $newObject = (object)array(
                    "serviceNames" => $serviceNames,
                    "serviceNamesOpt" => $serviceNamesOpt,
                    "serviceCounts" => $serviceCounts,
                    "servicePrices" => $servicePrices,
                );
                array_push($price_tables, array($product_info->id => $newObject));
            }
        }
        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => $breadcrumbs
        );
        return view('imodule.first',
            ['page' => $page,
                'user' => $user,
                'company_id' => $company_id,
                'product_types' => $product_types,
                'price_tables' => json_encode($price_tables),]);
    }

    /**
     * Step 2
     *
     * @param int $company_id
     * @param int $type_id
     * @param int $theme_id
     * @return \Illuminate\Http\Response
     */

    public function secondStep($company_id, $type_id, $theme_id = null, $fill_calculation = null)
    {
        // выбрираем все темы для данного $type_id
        // и считаем количество шаблонов
        $themes = \DB::select("SELECT t.*, c.count
                                FROM themes AS t
                                LEFT JOIN (SELECT theme_id, count(*) as count
                                           FROM template_library_theme AS tlt
                                           JOIN template_library AS lib ON lib.id=tlt.template_library_id
                                           WHERE lib.type = ?
                                           GROUP BY tlt.theme_id
                                ) AS c ON c.theme_id=t.id
                                WHERE c.count > 0
                            ", [$type_id]);

        $queryMy = \DB::table('templates')->where('client_id', '=', $company_id)->where('type', $type_id)->where('activity', 1);
        $queryLib = \DB::table('template_library')->where('type', $type_id)->where('activity', 1);

        // количество для пунктов Все и Мои
        $count_my = $queryMy->count();
        $count_all = $count_my + $queryLib->count();


        // получить шаблоны
        // all templates
        if (is_null($theme_id) || (is_numeric($theme_id) && $theme_id < 0)) {
            $category_title = 'Все';
            $myTemplates = $queryMy->get();
            $libTemplates = $queryLib->get();
            $templates = array_merge($myTemplates, $libTemplates);
        } // my templates
        elseif ($theme_id == 0) {
            $category_title = 'Клиентские';
            $templates = $queryMy->get();
        } // library templates
        elseif ($theme_id > 0) {

            $theme = Theme::find($theme_id);
            if (!$theme) {
                abort(404, 'Тематика не найдена');
            }
            $category_title = $theme->name;
            $templates = \DB::table('template_library_theme')
                ->leftJoin('template_library', 'template_library_id', '=', 'template_library.id')
                ->where('type', $type_id)
                ->where('activity', 1)
                ->where('template_library_theme.theme_id', '=', $theme->id)
                ->get();


            /*
            $templates = \DB::table('template_library')
                ->leftJoin('template_library_theme', 'template_library_id', '=', 'template_library.id')
                ->where('type', $type_id)
                ->where('activity', 1)
                ->where('template_library_theme.theme_id', '=', $theme->id)
                ->get();
            */
        }

        $breadcrumbs[] = 'Новый заказ';
        $breadcrumbs[] = 'Выбор шаблона';
        $page = array(
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => $breadcrumbs
        );
        
        $data = [
            'page' => $page,
            'category_title' => $category_title,
            'company_id' => $company_id,
            'type_id' => $type_id,
            'themes' => $themes,
            'templates' => $templates,
            'count_all' => $count_all,
            'count_my' => $count_my,
            'mini_menu' => true
        ];
        
        $data = array_merge($data, $this->getPriceInfo($type_id, $fill_calculation));

        return view('imodule.second', $data);
    }

    public function secondStepsavePrice($company_id, $type_id, Request $request)
    {
        $order = \DB::table('orders')->where('client_id', $company_id)->where('type_product', $type_id)->get();
        $newObject = json_decode('{}');
        $orderSelected = Orders::find($order[0]->id);
        $orderSelected->draw = $request->user_count;
        $orderSelected->price = $request->total_price;

        $newObject->options = [];
        for ($i = 0; $i < count($request->namesChecked); $i++) {
            if ($i == 0) {
                $newObject->name = $request->namesChecked[$i];
            } else {
                array_push($newObject->options, $request->namesChecked[$i]);
            }
        }

        $orderSelected->price_details = $newObject;
        $orderSelected->save();

        $newObject->draw = $request->user_count;
        $newObject->price = $request->total_price;
        $redirect_href = '/imodule/second/' . $company_id . '/' . $type_id . '/-1/' . json_encode($newObject);
        return $this->newStep2($company_id, $type_id, -1, $newObject);
    }

    public function getPriceInfo($type_id, $fill_calculation = null)
    {
        $product_info = ProductType::find($type_id);
        $serviceNames = [];
        $serviceNamesOpt = [];
        $serviceCounts = [];
        $servicePrices = [];
        $services = Services::where('id', $product_info->services_id)->groupBy('count')->get();
        foreach ($services as $service) {
            array_push($serviceCounts, (object)array('count' => $service->count));
        }
        $services = Services::where('id', $product_info->services_id)->groupBy('name')->orderBy('option')->get();
        foreach ($services as $service) {
            if (!$service->option) {
                array_push($serviceNames, (object)array('name' => $service->name, 'option' => $service->option));
            } else {
                array_push($serviceNamesOpt, (object)array('name' => $service->name, 'option' => $service->option));
            }
        }

        $services = Services::where('id', $product_info->services_id)->get();
        foreach ($services as $service) {
            array_push($servicePrices, (object)array('price' => $service->price * 1));
        }

        $nameSelected = "";
        $optionsSelected = [];
        $orderDraw = 0;
        $orderPrice = 0;
        if ($fill_calculation != null) {
            if (is_array($fill_calculation))
            {
                $fill_calculation = json_encode($fill_calculation);
            }
            if (!is_object($fill_calculation)) {
                $fill_calculation = json_decode($fill_calculation, false);
            }
            $orderDraw = $fill_calculation->draw;
            $orderPrice = $fill_calculation->price;
            $nameSelected = $fill_calculation->name;
            foreach ($fill_calculation->options as $option) {
                array_push($optionsSelected, $option);
            };
        }
        
        return [
            'services' => $serviceNames,
            'services_opt' => $serviceNamesOpt,
            'counts' => $serviceCounts,
            'prices' => $servicePrices,
            'price_count' => $orderDraw,
            'price_price' => $orderPrice,
            'name_selected' => $nameSelected,
            'options_selected' => $optionsSelected,
        ];
    }

    public function third($company_id, $type_id, $template_id, $lib = null, $fill_calculation = null)
    {

        if ($lib == 'lib') {
            $template = TemplateLibrary::find($template_id);
            if (!$template) {
                abort(404, 'Шаблон не найден');
            }

            $filename = $template->w2p;
            $template_type = 'template_library';
        } else {
            $template = Templates::find($template_id);
            if (!$template) {
                abort(404, 'Шаблон не найден');
            }
            $filename = substr($template->w2p, 1);
            $template_type = 'templates';
        }

        if (!$template) {
            abort(404, 'Шаблон не найден');
        }

        // новое имя для папки
        $name = md5(rand(1, 100000) * rand(1, 100000));

        if (file_exists($filename)) {
            $pinfo = pathinfo($filename);
            $src = $pinfo['dirname'] . '/' . $pinfo['filename'];
            $dst = 'lib/frx/' . $name;

            rcopy($src, $dst);

            $file_new = "lib/frx/" . $name . ".frx";
            Frx::copyFrx($filename, $file_new);
        } else {
            throw new \Exception('File not found: ' . $filename);
        }


        $uuid = $file_new;

        // Создать заказ в базу данных
        $orders = new Orders;
        $orders->status = 1;
        $orders->client_id = $company_id;
        $orders->type_product = $template->type;
        $orders->file = $name . '.frx';
        if ($template_type == 'template_library') {
            $orders->template_library_id = $template->id;
        } else {
            $orders->template_id = $template->id;
        }
        if ($fill_calculation != null) {
            if (!is_object($fill_calculation)) {
                $fill_calculation = json_decode($fill_calculation, false);
            }
            $orders->draw = $fill_calculation->draw;
            $orders->price = $fill_calculation->price;
            $price_details = json_decode('{}');
            $price_details->name = $fill_calculation->name;
            $price_details->options = $fill_calculation->options;
            $orders->price_details = $price_details;
        }
        //$orders->user_id = $user->id;
        $orders->save();

        // Делаем запись в лог заказов
        $logs = new Order_logs;
        $logs->status = 1;
        $logs->user_id = $company_id;
        $logs->order_id = $orders->id;
        $logs->save();

        return redirect('/imodule/edit/' . $orders->id);
    }

    /*
     * Редактор менеджера
     */
    public function edit($id)
    {
        $user = Auth::user();

        $order = Orders::find($id);

        if (!$order) {
            return redirect('/home');
        }

        if ($order->status > 3) {
            return redirect('/imodule/history');
        }

        if (!is_null($order->template_id)) {
            $template = Templates::find($order->template_id);
        } elseif (!is_null($order->template_library_id)) {
            $template = TemplateLibrary::find($order->template_library_id);
        }

        $uuid = str_replace('.frx', '', $order->file);

   
        $breadcrumbs[] = 'Оформление заказа';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );

        if ($user->isClient()) {
            return view('imodule.third_by_user', [
                'page' => $page,
                'user' => $user,
                'uuid' => $uuid,
                'template' => $template,
                'mini_menu' => true,
                'company_id' => $order->client_id
            ]);
        }

        return view('imodule.third', [
            'page' => $page,
            'user' => $user,
            'uuid' => $uuid,
            'template' => $template,
            'mini_menu' => true,
            'company_id' => $order->client_id,
            'order_id' => $order->id,
            'status' => $order->status
        ]);
    }

    /**
     *  Просмотр перед согласовнаие или оптравкой на ревью
     */
   /* public function reviwer($uuid)
    {

        $user = Auth::user();
        $order = Orders::query()->where('file', $uuid . '.frx')->get();
        $order = $order[0];

        if (!$order) {
            return redirect('/home');
        }

        if ($order->status >= 4) {
            return redirect('/imodule/history');
        }

        $templates_info = \DB::table('product_types')->find($order->type_product);
        $order->type_name = $templates_info->name;

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Страница согласования';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'user' => $user,
            'order' => $order,
            'company_id' => $order->company_id,
            'type_id' => $order->type_product,
        ];

        $fill_calculation = array_merge($order->price_details, ['draw' => $order->draw, 'price' => $order->price]);
        $data = array_merge($data, $this->getPriceInfo($order->type_product, $fill_calculation));

        return view('orders.reviwer', $data);
    }*/

    /*
    * прайс для подтверждения стоимости и условий
    */

    public function afterPrice($uuid)
    {
        $user = Auth::user();
        $order = Orders::query()->where('file', $uuid . '.frx')->get();
        $order = $order[0];       
            
        $templates_info = \DB::table('product_types')->find($order->type_product);
        $order->type_name = $templates_info->name;

        if (!$order) {
            return redirect('/imodule');
        }

        if ($order->status >= 4) {
            return redirect('/imodule/history');
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Страница согласования';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'user' => $user,
            'order' => $order,
            'company_id' => $order->company_id,
            'type_id' => $order->type_product,
        ];
       

       $fill_calculation = array_merge($order->price_details, ['draw' => $order->draw, 'price' => $order->price]);
       $data = array_merge($data, $this->getPriceInfo($order->type_product, $fill_calculation));

       Log::info($order->type_product);
       Log::info("very important message");

       return view('imodule.price_after',$data);


    }

    public function getOrder($uuid)
    {       
        $user = Auth::user();
        $order = Orders::query()->where('file', $uuid . '.frx')->get();
        $order = $order[0];
         
        if (!$order) {
            return redirect('/imodule');
        }

        if ($order->status >= 4) {
            return redirect('/imodule/history');
        }
    
        $templates_info = \DB::table('product_types')->find($order->type_product);
        $order->type_name = $templates_info->name;


        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Страница согласования';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'user' => $user,
            'order' => $order,
            'company_id' => $order->company_id,
            'type_id' => $order->type_product,
        ];
       

       $fill_calculation = array_merge($order->price_details, ['draw' => $order->draw, 'price' => $order->price]);
       $data = array_merge($data, $this->getPriceInfo($order->type_product, $fill_calculation));

       Log::info($order->type_product);

       return view('imodule.get_order',$data);
    }
}
