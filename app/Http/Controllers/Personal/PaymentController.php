<?php

namespace App\Http\Controllers\Personal;

use Session;
use App\GroupOrders;
use App\OrderStatuses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class PaymentController extends Controller
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Display a listing of the user's payments
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $this->auth->user();

        if (! $user->client) {
            abort(404, 'Current user has not related Client');
        }

        $current_page = $request->page;
        if (!isset($request->page) || ($request->change == 1)) {
            $current_page = 1;
            $request->page = 1;
        }

        $clients = \Auth::user()->contact->clients()->get();
        $clientIds = $clients->pluck('id')->toArray();
        $groupOrders = GroupOrders::whereIn('client_id', $clientIds)
                                ->whereHas('statusInfo', function ($query) {
                                    $query->whereIn('id', OrderStatuses::$cabinet_group['payments']);
                                })
                                ->with('orders')
                                ->orderBy('updated_at', 'desc')
                                ->paginate(10,['*'], 'page', $current_page);

        if (!$groupOrders->count() && (strlen($request->user_search) == 0)) {
            return redirect('/orders/user_orders');
        }

        $page = [
            'menu' => 'order',
            'submenu' => 'payments',
            'breadcrumbs' => ['Заказы', 'Мои платежи'],
        ];

        $data = [
            'page' => $page,
            'current_page' => $current_page,
            'pagination' => $groupOrders->render(),
            'groupOrders' => $groupOrders,
            'selected_group_order_id' => Session::get('selected_group_order_id'),
            'opened' => 0,
        ];

        if (\Request::ajax()) {
            $data['html'] = view('personal._table.orders_payment_table', $data)->render();
            return \Response::json($data);
        } else {
            return view('personal.payment.index', $data);
        }
    }

    /**
     * Confirm payment
     *
     * @param int $groupOrderId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm($groupOrderId, Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,png,pdf',
        ]);

        $groupOrder = GroupOrders::find($groupOrderId);

        $imagePath = $this->makeImageFromRequest($request);

        if (! $imagePath) {
            return response()->json([
                'status' => 'error',
                'message' => 'Ошибка загрузки файла',
            ]);
        }

        if ($request->has('partner') && $request->input('partner')) {
            $groupOrder->confirm_image_partner = $imagePath;
        } else {
            $groupOrder->confirm_image = $imagePath;
            $groupOrder->payment_status = 'Платёжное поручение';
        }

        $groupOrder->save();

        addLog(0, 503, auth()->user()->id, link_to($imagePath, 'Платёжное поручение'), $groupOrder->id, 0, "Платёжное поручение");

        return response()->json([
            'status' => 'success',
            'message' => 'Изображение успешно загружено',
        ]);
    }

    /**
     * Guarantee payment
     *
     * @param int $groupOrderId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function guarantee($groupOrderId, Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,png,pdf',
        ]);

        $groupOrder = GroupOrders::find($groupOrderId);

        $imagePath = $this->makeImageFromRequest($request);

        if (! $imagePath) {
            return response()->json([
                'status' => 'error',
                'message' => 'Ошибка загрузки файла',
            ]);
        }

        $groupOrder->guarantee_image = $imagePath;
        $groupOrder->payment_status = 'Гарантийное письмо';
        $groupOrder->save();

        addLog(0, 502, auth()->user()->id, link_to($imagePath, 'Гарантийное письмо'), $groupOrder->id, 0, "Гарантийное письмо");

        return response()->json([
            'status' => 'success',
            'message' => 'Изображение успешно загружено',
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function makeImageFromRequest(Request $request)
    {
        $image = $request->file('image');

        $uploadDir = "/upload/group-orders-files/";
        $filename = str_random();
        $extension = $image->getClientOriginalExtension();

        if (!file_exists(public_path($uploadDir))) {
            mkdir(public_path($uploadDir), 0777, true);
        }

        if ($image->move(public_path($uploadDir), "{$filename}.{$extension}")) {
            return $uploadDir.$filename.".".$extension;
        }

        return false;
    }
}
