<?php

namespace App\Http\Controllers\Personal;

use App\Orders;
use App\OrderStatuses;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserOrdersController extends Controller
{
    public function index(Request $request)
    {
        $clients = \Auth::user()->contact->clients()->get();
        $clientIds = $clients->pluck('id')->toArray();

        $current_page = $request->page;
        if (!isset($request->page) || ($request->change == 1)) {
            $current_page = 1;
            $request->page = 1;
        }

        $orders = Orders::select('orders.*')
            ->join('clients as c', function ($query) use ($request, $clientIds) {
                $query->on('c.id', '=', 'client_id')
                    ->whereIn('c.id', $clientIds);
            })
            ->with(['statusInfo', 'statusLastInfo'])
            ->where(function ($query) {
                $query->whereIn('status', OrderStatuses::$cabinet_group['user_orders'])
                        ->orWhere(function ($q) {
                            $q->where('status', status('exclusive_design', 'id'));
                            $q->where('use_specification', true);
                        })
                        ->orWhere(function ($q) {
                            // Приостановленная в производстве позиция не должна менять статус в ЛК и не должна переноситься у клиента в раздел Требуют внимания
                            $q->where('status', 1000)
                                ->where('stopped_user_id', '!=', \Auth::user()->id);
                        });
            })
            ->whereNull('parent_id')
            ->orderBy('updated_at', 'desc')
            ->paginate(10, ['*'], 'page', $current_page);

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Мои заказы';
        $page = array(
            'menu' => 'order',
            'submenu' => 'my_orders',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'orders' => $orders,
            'current_page' => $current_page,
            'pagination' => $orders->render(),
            'errorStatus' => OrderStatuses::find(999),
            'product_types' => \DB::table('product_types')->get(),
            'selected_group_order_id' => \Session::get('selected_group_order_id')
        ];

        if (\Request::ajax()) {
            $data['html'] = view('personal._table.orders_table', $data)->render();
            return \Response::json($data);
        } else {
            return view('personal.orders.index', $data);
        }
    }

}
