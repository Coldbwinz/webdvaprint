<?php

namespace App\Http\Controllers\Personal;

use App\Orders;
use App\OrderStatuses;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NeedAttentionController extends Controller
{

    public function index(Request $request)
    {

        $current_page = $request->page;
        if (!isset($request->page) || ($request->change == 1)) {
            $current_page = 1;
            $request->page = 1;
        }

        $clients = \Auth::user()->contact->clients()->get();
        $clientIds = $clients->pluck('id')->toArray();

        $orders = Orders::select('orders.*')
            ->join('clients as c', function ($query) use ($request, $clientIds) {
                $query->on('c.id', '=', 'client_id')
                    ->whereIn('c.id', $clientIds);
            })
            ->with('statusInfo')
            ->where(function ($query) {
                $query->whereIn('status', OrderStatuses::$cabinet_group['need_attention'])
                    ->orWhere(function ($q) {
                        $q->where('status', status('exclusive_design', 'id'));
                        $q->where('use_specification', false);
                    })
                    ->orWhere(function ($q) {
                        // Приостановленная в производстве позиция не должна менять статус в ЛК и не должна переноситься у клиента в раздел Требуют внимания
                        $q->where('status', 1000)
                            ->where('stopped_user_id', Auth::user()->id);
                    });
            })
            ->whereNull('parent_id')
            ->orderBy('updated_at', 'desc')
            ->paginate(10,['*'], 'page', $current_page);

        if (!$orders->count() && (strlen($request->user_search) == 0)) {
            return redirect('/orders/payments');
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Требующие внимания';
        $page = array(
            'menu' => 'order',
            'submenu' => 'need_attention',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'current_page' => $current_page,
            'pagination' => $orders->render(),
            'orders' => $orders,
            'user' => auth()->user(),
            'errorStatus' => OrderStatuses::find(999),
            'product_types' => \DB::table('product_types')->get(),
            'user_search' => $request->user_search,
            'selected_group_order_id' => \Session::get('selected_group_order_id'),
            'message' => (\Route::getCurrentRoute()->getPath() == 'orders/need_attention_with_message') ? true : false
        ];

        if (\Request::ajax()) {
            $data['html'] = view('personal._table.orders_table', $data)->render();
            return \Response::json($data);
        } else {
            return view('personal.need_attention.index', $data);
        }
    }

}
