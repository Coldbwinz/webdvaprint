<?php

namespace App\Http\Controllers\Personal;

use Illuminate\Http\Request;
use Auth;
use File;
use Validator;

use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function edit()
    {
        $user = Auth::user();

        $breadcrumbs[] = 'Профиль';
        $breadcrumbs[] = 'Редактировать профиль';
        $page = array(
            'menu' => '',
            'submenu' => '',
            'breadcrumbs' => $breadcrumbs,
        );
        return view('personal.profile.edit', [
            'page' => $page,
            'user' => $user,
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'position' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id . ',id,subdomain_id,' . $subdomain_id,
            'password' => 'sometimes|min:6|confirmed',
            'confirm_password' => 'sometimes|required|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (isset($request->photo)) {
            if (strlen($request->imagebase64) > 0) {
                $user->photo = $this->resizeImageAndSave($request->imagebase64, $_SERVER['DOCUMENT_ROOT'] . '/upload/users/');
            }
        }

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->position = $request->position;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('personal.orders.need_attention');
    }

    public function resizeImageAndSave($file, $upload_folder)
    {
        if (!file_exists($upload_folder)) {
            mkdir($upload_folder, 0777, true);
        }

        list($type, $file) = explode(';', $file);
        list(, $file) = explode(',', $file);

        $file = base64_decode($file);

        $name = md5(time() . rand(1000, 9999));
        $previewName = $name . '.png';
        $preview = $upload_folder . $previewName;
        file_put_contents($preview, $file);

        File::delete($file);

        return $previewName;
    }

}
