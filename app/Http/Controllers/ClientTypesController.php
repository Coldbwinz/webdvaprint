<?php

namespace App\Http\Controllers;

use App\ClientTypes;
use App\Subdomain;
use Illuminate\Http\Request;
use \Validator;

class ClientTypesController extends Controller
{
    /**
     * @var ClientTypes
     */
    private $clientType;

    /**
     * @param ClientTypes $clientType
     */
    public function __construct(ClientTypes $clientType)
    {
        $this->clientType = $clientType;
    }

    public function index(Request $request)
    {
        $client_types = $this->clientType
            ->forSubdomain(Subdomain::findCurrent())
            ->paginate(20);

        $breadcrumbs[] = 'Типы клиентов';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'client_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('client_types.index', [
            'page' => $page,
            'client_types' => $client_types,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:client_types,name',
        ]);
        if ($validator->fails()) {
            return redirect('/client_types')->withErrors($validator)->withInput();
        }

        $clientType = new ClientTypes();
        $clientType->subdomain_id = Subdomain::findCurrentId();
        $clientType->name = $request->input('name');
        $clientType->discount = $request->input('discount');
        $clientType->save();

        return redirect('/client_types');
    }

    public function edit($id)
    {
        $product_info = \DB::table('client_types')->find($id);

        $breadcrumbs[] = 'Типы клиентов';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'client_types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('client_types.edit', [
            'page' => $page,
            'client_info' => $product_info
        ]);
    }

    public function update($id, Request $request)
    {
        $clientType = ClientTypes::find($id);
        $subdomain_id = is_null($clientType->subdomain_id) ? 'NULL' : $clientType->subdomain_id;

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:client_types,name,'.$id.',id,subdomain_id,'.$subdomain_id,
        ]);
        if ($validator->fails()) {
            return redirect('/client_types/edit/'.$id)->withErrors($validator)->withInput();
        }

        $clientType->name = $request->input('name');
        $clientType->discount = $request->input('discount');
        $clientType->save();

        return redirect('/client_types');
    }
}
