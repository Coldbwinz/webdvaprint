<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Theme;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ThemesController extends Controller
{
    /**
     * @var Theme
     */
    private $theme;

    /**
     * @param Theme $theme
     */
    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    /**
     * Themes list
     *           
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $themes = $this->theme->with('tags')->get();

        $page = [
            'menu' => 'catalogs',
            'submenu' => 'types',
            'breadcrumbs' => ['Продукция', 'Тематики']
        ];

        return view('themes.index', compact('page', 'themes'));
    }
    
    /**
     * Route /products/types/new
     * 
     * @param Request $request
     * @return redirect
     */
    
    public function create(Request $request) 
    { 
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:themes,name|max:255',            
        ]);
        if ($validator->fails()) {
            return redirect('/products/types')->withErrors($validator);
        }
        
        $theme = new Theme;
        $theme->name = $request->input('name');   
        $theme->save();        

        return redirect('/products/types');
    }
    
    /**
     * Edit theme
     * 
     * Route GET /products/types/edit/{id}
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id) 
    {
        $theme = Theme::find($id);
        if (!$theme) {
            abort(404, 'Тематика не найдена');
        }

        $breadcrumbs[] = 'Продукция';
        $breadcrumbs[] = 'Тематики';
        $breadcrumbs[] = 'Редактировать';
        $page = array(
            'menu' => 'catalogs',
            'submenu' => 'types',
            'breadcrumbs' => $breadcrumbs
        );
        return view('themes.edit', [
            'page' => $page, 
            'theme' => $theme
        ]);
    }
    
    /**
     * Update theme
     * 
     * Route POST /products/types/edit/{id}
     * 
     * @param Request $request
     * @param int $id
     * @return redirect
     */
    
    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'name' => 'required|unique:themes,name,'.$id.'|max:255',
        ]);

        $theme = Theme::findOrFail($id);

        $theme->update($request->all());

        if ($request->has('tags')) {
            $theme->tags()->sync($request->input('tags'));
        }

        return redirect('/products/types');
    }
    
    /**
     * Route /products/types/delete/{id}
     *  
     * @param int $id    
     * @return \Illuminate\Http\Response
     */
    
    public function delete($id) 
    {
        $theme = Theme::find($id);
        if ($theme) {
            $theme->delete();
        }                    
        return redirect('/products/types');
    }
    
    /**
     * Set activity to on
     * 
     * @param int $id
     * @return redirect
     */

    public function on($id) {
        \DB::update('update themes set activity = "1" where id = ?', [$id]);
        return redirect('/products/types');
    }
    
    /**
     * Set activity to off
     * 
     * @param int $id
     * @return redirect
     */
    
    public function off($id) {
        \DB::update('update themes set activity = 0 where id = ?', [$id]);
        return redirect('/products/types');        
    }
}
