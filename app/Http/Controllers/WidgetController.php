<?php
namespace App\Http\Controllers;

use App\User;
use App\Subdomain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WidgetController extends Controller
{

    /**
     * [index description]
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'menu' => 'options',
            'submenu' => 'widgets',
            'breadcrumbs' => ['Настройка', 'Виджеты']
        ];

        $managers = User::whereIn('id_group', [2, 3])->where('subdomain_id', Subdomain::findCurrentId())->get();
        foreach ($managers as $manager) {
            $managerList[$manager->id] = $manager->displayedName();
        }

        $widgets = [
            'registration' => [
                'managerList' => $managerList,
            ],
        ];

        return view('widgets.index', compact ('page', 'widgets'));
    }

    /**
     * [saveRegistrationSettings description]
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function saveRegistrationSettings(Request $request)
    {
        settings()->set('widget_registration_manager_id', $request->manager_id);
        settings()->save();

        return redirect()->route('widgets')->withInput();
    }

}
