<?php

namespace App\Http\Controllers;

use App\Parameters;

class OptionController extends Controller
{
    /**
     * @var Parameters
     */
    private $parameters;

    /**
     * @param Parameters $parameters
     */
    public function __construct(Parameters $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * List all options
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $options = $this->parameters->paginate(20);

        $page = [
            'menu' => 'options',
            'submenu' => 'options',
            'breadcrumbs' => ['Настройки', 'Опции'],
        ];

        return view('options.index', compact('options', 'page'));
    }

    /**
     * Turn on option
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function on($id)
    {
        $parameter = $this->parameters->find($id);
        $parameter->activity = 1;
        $parameter->save();

        return redirect()->route('options');
    }

    /**
     * Turn off option
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function off($id)
    {
        $parameter = $this->parameters->find($id);
        $parameter->activity = 0;
        $parameter->save();

        return redirect()->route('options');
    }
}
