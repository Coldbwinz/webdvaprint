<?php

namespace App\Http\Controllers;

use App\Services;
use App\Urgency;
use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Input;
use Symfony\Component\EventDispatcher\Tests\Service;
use Validator;
use File;
use App\ProductType;

class ProductsController extends Controller
{

    public function index(Request $request)
    {
        $product_types = \DB::table('product_types')
            ->orderBy('activity','desc')
            ->paginate(20);

        $breadcrumbs[] = 'Продукция';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'index',
            'breadcrumbs' => $breadcrumbs
        );
        return view('products.index', [
            'page' => $page,
            'product_types' => $product_types,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_types,name',
            'photo' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/products')->withErrors($validator)->withInput();
        }

        $file = $request->file('photo');
        $uploadFolder = 'upload/products/';
        rmkdir($uploadFolder);
        $target = $file->move($uploadFolder, $file->getClientOriginalName());

        $name = md5(time() . rand(1000, 9999));
        $previewName = $name . '.' . $file->getClientOriginalExtension();
        $preview = $uploadFolder . $previewName;

        image_resize($target, $preview, 90, 90, 70);

        File::delete($target);

        // save
        $productType = new ProductType;
        $productType->name = $request->input('name');
        $productType->printler_type = $request->input('printler_type');
        $productType->url = $previewName;
        $productType->save();

        return redirect('/products');
    }

    public function edit($id)
    {
        $product_info = \DB::table('product_types')->find($id);

        $breadcrumbs[] = 'Продукция';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'index',
            'breadcrumbs' => $breadcrumbs
        );
        return view('products.edit', [
            'page' => $page,
            'product_info' => $product_info
        ]);
    }

    public function update($id, Request $request)
    {
        $productType = ProductType::find($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_types,name,' . $id,
        ]);
        if ($validator->fails()) {
            return redirect('/products/edit/' . $id)->withErrors($validator)->withInput();
        }

        if (Input::hasFile('photo')) {
            $name = Input::file('photo')->getClientOriginalName();
            $images = Input::file('photo')->move('upload/products', $name);
            $productType->url = $name;
        }
        $productType->name = $request->input('name');
        if ($request->input('printler_type') != $productType->printler_type) {
            $productType->printler_type = $request->input('printler_type');
            $productType->services = '';
        }
        $productType->save();

        return redirect('/products');
    }

    public function on($id)
    {
        \DB::update('update product_types set activity = "1" where id = ?', [$id]);
        return redirect('/products');
    }

    public function off($id)
    {
        \DB::update('update product_types set activity = 0 where id = ?', [$id]);
        return redirect('/products');
    }

    public function priceSave(Request $request)
    {
        $product_info = ProductType::find($request->product_id);
        $product_info->services = $request->prices;
        $product_info->save();
        return redirect('/products');
    }

    public function price($id)
    {
        $productType = ProductType::where('id', '=', $id)->first();
        $other_service = null;
        if (strlen($productType->services) == 0) {
            $productOfThisType = ProductType::where('printler_type', '=', $productType->printler_type)->get();
            foreach ($productOfThisType as $product) {
                if ($id != $product->id)
                    if (strlen($product->services) > 0) {
                        $other_service = json_decode($product->services);
                        foreach ($other_service as $key => $firstService) {
                            if ($key != 'counts') {
                                $other_service = json_encode($firstService->urgencies);
                                break;
                            }
                        }
                        break;
                    }
            }
        }
        $urgency_table = Urgency::where('subdomain_id',null)->get();
        $breadcrumbs[] = 'Продукция';
        $breadcrumbs[] = 'Стоимость';
        $page = array(
            'menu' => 'options',
            'submenu' => 'index',
            'breadcrumbs' => $breadcrumbs
        );
        return view('products.price', [
            'page_title' => $productType->name,
            'page' => $page,
            'product_id' => $productType->id,
            'printler_type' => $productType->printler_type,
            'product_services' => $productType->services,
            'urgency_table' => $urgency_table,
            'selected' => json_encode(session()->get('selected')),
            'other_service_urgency' => $other_service,
            'template_chromacity' => view('templates.chromacity'),
            'template_paper_clip_chromacity' => view('templates.chromacity_paper_clip'),
        ]);
    }

}
