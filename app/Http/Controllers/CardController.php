<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\Controller;

class CardController extends Controller
{
    /**
     * @author Denis Gorgul
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {        
        $breadcrumbs[] = 'CRM';
        $breadcrumbs[] = 'Карточка';  
        $page = array(
            'menu' => 'crm',
            'submenu' => 'all',
            'breadcrumbs' => $breadcrumbs
        );
        return view('card.index', ['page' => $page]);
    }
}
