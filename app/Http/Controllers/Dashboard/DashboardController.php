<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = [
            'menu' => 'new-dashboard',
            'submenu' => '',
            'breadcrumbs' => ['Dashboard', 'Контрольная панель']
        ];

        return view('dashboard.new-dashboard', compact('page'));
    }
}
