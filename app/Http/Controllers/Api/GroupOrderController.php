<?php

namespace App\Http\Controllers\Api;

use App\GroupOrders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupOrderController extends Controller
{
    /**
     * @var GroupOrders
     */
    private $groupOrder;

    /**
     * @param GroupOrders $groupOrder
     */
    public function __construct(GroupOrders $groupOrder)
    {
        $this->groupOrder = $groupOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->groupOrder
            ->with([
                'client.type', 'manager', 'contact', 'statusInfo', 'orders.productType'
            ])
            ->latest()
            ->paginate($request->input('paginate', 10));
    }
}
