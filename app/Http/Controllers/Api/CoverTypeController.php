<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Material\CoverType;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class CoverTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = CoverType::all();

        return response()->json($this->transformCollection($materials));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|unique:cover_types,name',
        ]);

        return $this->transform(CoverType::create($request->all()));
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $covers
     * @return array
     */
    public function transformCollection(Collection $covers)
    {
        return $covers->map(function ($cover) {
            return $this->transform($cover);
        });
    }

    /**
     * @param \App\Models\CoverType $coverType
     * @return array
     */
    protected function transform(CoverType $coverType)
    {
        return [
            'id' => $coverType->id,
            'name' => $coverType->name
        ];
    }
}
