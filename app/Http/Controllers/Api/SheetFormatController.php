<?php

namespace App\Http\Controllers\Api;

use App\Models\SheetFormat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SheetFormatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formats = SheetFormat::query();

        if ($request->has('type')) {
            $formats->where('type', $request->input('type'));
        }

        $formats = $formats->get();

        return $formats->map(function (SheetFormat $sheetFormat) {
            return $this->transform($sheetFormat);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'width' => 'required',
            'height' => 'required',
            'type' => 'required',
        ]);

        $input = $request->only(['width', 'height', 'type']);

        $format = SheetFormat::where($input)->first();

        if (! $format) {
            $sheetFormat = SheetFormat::create($request->all());

            return response()->json($this->transform($sheetFormat));
        }

        return response()->json($this->transform($format));
    }

    /**
     * Transform sheet format for api response
     *
     * @param SheetFormat $sheetFormat
     * @return array
     */
    protected function transform(SheetFormat $sheetFormat)
    {
        return [
            'width' => $sheetFormat->width,
            'height' => $sheetFormat->height,
            'type' => $sheetFormat->type,
        ];
    }
}
