<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Material\MaterialType;
use Illuminate\Database\Eloquent\Collection;

class MaterialTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = MaterialType::all();

        return response()->json($this->transformCollection($materials));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|unique:material_types,name',
        ]);

        return $this->transform(MaterialType::create($request->all()));
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $materials
     * @return array
     */
    public function transformCollection(Collection $materials)
    {
        return $materials->map(function ($material) {
            return $this->transform($material);
        });
    }

    /**
     * @param \App\Models\Material\MaterialType $materialType
     * @return array
     */
    protected function transform(MaterialType $materialType)
    {
        return [
            'id' => $materialType->id,
            'name' => $materialType->name
        ];
    }
}
