<?php

namespace App\Http\Controllers\Api\Contractors;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * @param User $employee
     * @param Request $request
     */
    public function addAccrual(User $employee, Request $request)
    {
        $this->validate($request, [
            'accrual_type' => 'required',
            'accrual_value' => 'required',
            'accrual_reason' => 'required',
        ], [], [
            'accrual_type' => 'Тип начисления',
            'accrual_value' => 'Сумма начисления',
            'accrual_reason' => 'Причина начисления',
        ]);

        $accrualValue = $request->input('accrual_value');

        $premiumAmount = $accrualValue['value'];

        $employee->addAccrual($request->input('accrual_type'), $premiumAmount, $request->input('accrual_reason'));

        return response()->json([
            'status' => 'redirect',
            'redirect' => route('employee.salary', $employee),
        ]);
    }
}
