<?php

namespace App\Http\Controllers\Api;

use App\Clients;
use App\ClientTypes;
use App\ClientStatuses;
use App\Services\ClientPicture;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Factory as Validator;

class ClientController extends Controller
{
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var ClientTypes
     */
    private $clientType;
    /**
     * @var ClientStatuses
     */
    private $clientStatuses;

    /**
     * @param Validator $validator
     * @param ClientTypes $clientType
     * @param ClientStatuses $clientStatuses
     */
    public function __construct(Validator $validator, ClientTypes $clientType, ClientStatuses $clientStatuses)
    {
        $this->validator = $validator;
        $this->clientType = $clientType;
        $this->clientStatuses = $clientStatuses;
    }

    /**
     * @param Request $request
     * @param Clients $client
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Clients $client)
    {
        $validator = $this->validator->make($request->all(), [
            'name' => 'required',
            'value' => 'required',
        ]);

        $attribute = $request->input('name');
        $value = $request->input('value');

        // Validate email
        if ($attribute === 'email') {
            $v = \Validator::make(['email' => $value], [
                'email' => 'email|unique:clients'
            ]);

            if ($v->fails()) {
                return response()->json([
                    'status' => 'fail',
                    'message' => implode(', ', $v->errors()->all()),
                ]);
            }
        }

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => implode(', ', $validator->errors()->all()),
            ]);
        }

        if ($attribute === 'client_type') {
            $client->type()->associate(
                $this->clientType->findOrFail($value)
            );
        }

        if ($attribute === 'client_status') {
            $client->status()->associate(
                $this->clientStatuses->findOrFail($value)
            );
        }

        $client->update([$attribute => $value]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @param Clients $client
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contacts(Clients $client, Request $request)
    {
        $connections = json_decode($request->input('connections'));

        $client->update([
            'connections' => $connections,
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * Change client picture
     *
     * @param Clients $client
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function picture(Clients $client, Request $request)
    {
        if (! $request->hasFile('picture')) {
            return response()->json([
                'status' => 'error',
                'message' => 'No picture selected',
            ]);
        }

        try {
            $picture = ClientPicture::makeFromRequest($request);

            $client->update(compact('picture'));

            return response()->json([
                'status' => 'success',
                'image' => $picture,
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
            ]);
        }
    }
}
