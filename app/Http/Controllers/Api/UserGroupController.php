<?php

namespace App\Http\Controllers\Api;

use App\UserGroup;
use App\Subdomain;
use App\ClientTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserGroupRequest;
use App\Http\Requests\UpdateUserRoleRequest;
use Illuminate\Support\Arr;

class UserGroupController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param UserGroup $userGroup
     * @return \Illuminate\Http\Response
     */
    public function show(UserGroup $userGroup)
    {
        $productTypes = [
            ['id' => 1, 'name' => 'Листовые изделия'],
            ['id' => 2, 'name' => 'Изделия со сгибом'],
            ['id' => 3, 'name' => 'Многостр. изделия']
        ];

        $items = [];

        $currentSubdomain = Subdomain::findCurrent();
        $clientTypes = ClientTypes::forSubdomain($currentSubdomain)->get();

        $defaultCoefficients = [
            [
                'percent' => null,
                'salary' => 'standart',
                'premium' => 'standart',
                'bonus' => 'standart',
                'bonus_coefficient' => null
            ]
        ];

        $defaultOwnProduction = [];
        foreach ($productTypes as $productType) {
            $defaultOwnProduction[] = [
                'value' => 0,
                'item' => 'turnover_percentage',
            ];
        }

        foreach ($clientTypes as $clientType) {
            $items[] = [
                'client_type' => [
                    'id' => $clientType->id,
                    'name' => $clientType->name,
                ],
                'own_production' => $defaultOwnProduction,
                'contract' => ['value' => 0, 'item' => 'turnover_percentage'],
            ];
        }

        $userGroupBonuses = [];
        foreach ($userGroup->salaryBonuses()
                     ->with(['productType', 'clientType'])
                     ->get() as $item) {
            $userGroupBonuses[$item->clientType->id][$item->productType->id] = [
                'value' => $item->value,
                'dimension' => $item->dimension ?: 'turnover_percentage',
            ];
        }

        $items = [];

        foreach ($clientTypes as $clientType) {
            $ownProduction = [];

            foreach ($productTypes as $productType) {
                $ownProduction[] = [
                    'value' => Arr::get($userGroupBonuses, "{$clientType->id}.{$productType['id']}.value", 0),
                    'item' => Arr::get($userGroupBonuses, "{$clientType->id}.{$productType['id']}.dimention", 'turnover_percentage'),
                ];
            }

            $items[] = [
                'client_type' => $clientType,
                'own_production' => $ownProduction,
                'contract' => [
                    'value' => 0,
                    'item' => 'turnover_percentage'
                ]
            ];
        }

        $bonuses = [
            'product_types' => $productTypes,
            'items' => $items,
        ];

        return response()->json([
            'new_clients_count_plan' => $userGroup->new_clients_count_plan,
            'repeated_orders_count_plan' => $userGroup->repeated_orders_count_plan,
            'invoices_total_plan' => $userGroup->invoices_total_plan,
            'paid_invoices_total_plan' => $userGroup->paid_invoices_total_plan,
            'salary' => $userGroup->salary ?: [
                'standard_salary' => 0,
                'increased_salary' => 0,
                'standard_premium' => 0,
                'increased_premium' => 0,
                'premium_calculation' => null,
            ],
            'bonuses' => $bonuses,
            'motivation_schemes' => $userGroup->salaryMotivationSchemes,
            'substractions' => $userGroup->substractions ?: [
                'overdue_receivable' => null,
                'discount' => null,
                'marriage_return' => null
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserGroup $userGroup
     * @param UpdateUserGroupRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserGroup $userGroup, UpdateUserGroupRequest $request)
    {
        $bonuses = $request->input('bonuses');

        foreach ($bonuses['items'] as $row) {
            foreach ($row['own_production'] as $index => $item) {

                $data = [
                    'product_type_id' => $bonuses['product_types'][$index]['id'],
                    'client_type_id' => $row['client_type']['id'],
                ];

                $exists = $userGroup->salaryBonuses()->where($data)->first();

                if ($exists) {
                    $exists->update([
                        'value' => $item['value'],
                        'dimension' => $item['item'],
                    ]);
                } else {
                    $userGroup->salaryBonuses()->create(array_merge($data, [
                        'value' => $item['value'],
                        'dimension' => $item['item'],
                    ]));
                }
            }
        }

        // save motivation schemes
        foreach ($request->input('motivation_schemes') as $scheme) {
            $data = [
                'percent' => $scheme['percent'],
                'salary' => $scheme['salary'],
                'premium' => $scheme['premium'],
                'bonus' => $scheme['bonus'],
                'bonus_coefficient' => $scheme['bonus_coefficient'],
            ];

            $exists = $userGroup->salaryMotivationSchemes()
                ->where('percent', $scheme['percent'])
                ->first();

            if ($exists) {
                $exists->update($data);
            } else {
                $userGroup->salaryMotivationSchemes()->create($scheme);
            }
        }

        $userGroup->update($request->all());

        session()->flash('message.success', 'Данные сотрудника обновлены.');

        return response()->json([
            'status' => 'redirect',
            'location' => url('/manager/motivation'),
        ]);
    }
}
