<?php

namespace App\Http\Controllers\Api\PrepressQueue;

use App\Orders;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class QueueController extends Controller
{
    /**
     * @var Orders
     */
    private $order;
    /**
     * @var Carbon
     */
    private $carbon;

    /**
     * @param Orders $order
     * @param Carbon $carbon
     */
    public function __construct(Orders $order, Carbon $carbon)
    {
        $this->order = $order;
        $this->carbon = $carbon;
    }

    /**
     * Получение всех видов постпресса у заказов в очереди
     *
     * @todo необходимо завести словать постпрессов, а не хранить jsonом у заказа
     *
     * @return array
     */
    public function postpress()
    {
        $postpresses = [];

        foreach ($this->order->all() as $order) {
            if ($order->price_details['selected_options']) {
                $postpresses = array_merge($postpresses, $order->price_details['selected_options']);
            }
        }

        // Перестройка индексов массива.
        // @todo как сделаем нормальное хранение данных заказа - это убрать
        $postpresses = array_values(array_unique($postpresses));

        return $postpresses;
    }

    /**
     * Получает все варианты цветности
     *
     * @return array
     */
    public function chromacity()
    {
        $items = [];

        foreach ($this->order->all() as $order) {
            if ($order->price_details['selected_chromacity']) {
                $items[] = $order->price_details['selected_chromacity'];
            }
        }

        // Перестройка индексов массива.
        // @todo как сделаем нормальное хранение данных заказа - это убрать
        return array_values(array_unique($items));
    }
}
