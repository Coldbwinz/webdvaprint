<?php

namespace App\Http\Controllers\Api;

use App\Clients;
use App\Communication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommunicationController extends Controller
{
    /**
     * @var Communication
     */
    private $communication;
    /**
     * @var Clients
     */
    private $client;

    /**
     * @param Communication $communication
     * @param Clients $client
     */
    public function __construct(Communication $communication, Clients $client)
    {
        $this->communication = $communication;
        $this->client = $client;
    }

    /**
     * Store new communication for client
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required|exists:clients,id',
            'name' => 'required|max:50',
            'value' => 'required|max:100',
        ]);

        $client = $this->client->findOrFail($request->input('client_id'));

        $communication = $client->communications()->create($request->all());

        return $communication;
    }
}
