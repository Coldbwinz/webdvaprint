<?php

namespace App\Http\Controllers\Api;

use App\OrderElement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderElementController extends Controller
{
    /**
     * @var OrderElement
     */
    private $orderElement;

    /**
     * @param OrderElement $orderElement
     */
    public function __construct(OrderElement $orderElement)
    {
        $this->orderElement = $orderElement;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        $elements = $this->orderElement
            ->with('order.statusInfo')
            ->whereHas('order', function ($query) {
                $query->whereIn('status', status(['in_work', 'preparing_to_press'], 'id'));
            })
            ->whereDoesntHave('performOrder');

        // Фильтрация по печатному листу
        if ($request->has('sheet')) {
            $elements->where('material', $request->input('sheet'));
        }

        // Фильтрация по цветности
        if ($request->has('chromacity')) {
            $elements->whereIn('chromacity', $request->input('chromacity'));
        }

        $elements->whereNull('pages_number');

        $elements = $elements->get();

        /**
         * Фильтрация по постпрессу
         * @todo переделать это гавно на нормальный запрос к БД
         */
        if ($request->has('postpress')) {
            $elements = $elements->filter(function ($element) use ($request) {
                return count(
                        array_diff($request->input('postpress'), $element->postpress)
                    ) === 0;
            });
        }

        // Перестройка индексов массива.
        $elements = $elements->values();

        return response()->json($elements);
    }

    public function chromacity()
    {
        return $this->orderElement->all()->pluck('chromacity');
    }
}
