<?php

namespace App\Http\Controllers\Api\Queue;

use App\Classes\Parser\Frx;
use App\Classes\Parser\RubyParser;
use App\Classes\Pdf\PdfPreviewMaker;
use App\Emails\SendLayoutEmail;
use App\GroupOrders;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GroupOrdersController;
use App\Http\Controllers\OrdersController;
use App\Http\Requests\UploadTemplateForApprovalRequest;
use App\Jobs\ParsePsd;
use App\Orders;
use App\ParserFiles;
use App\Template;
use App\TemplateLibrary;
use App\Templates;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Emails\SendInvoiceNotification;
use Log;

class DesignController extends Controller
{
    /**
     * @var GroupOrders
     */
    private $groupOrder;
    /**
     * @var Orders
     */
    private $order;

    /**
     * @param GroupOrders $groupOrder
     * @param Orders $order
     */
    public function __construct(GroupOrders $groupOrder, Orders $order)
    {
        $this->groupOrder = $groupOrder;
        $this->order = $order;
    }

    /**
     * Группы заказов для страницы очереди дизайна
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function groupOrders(Request $request)
    {
        $groupOrders = $this->groupOrder
            ->with([
                'orders' => function ($query) {
                    $query
                        ->groupBy('complect_id')
                        ->where(function ($query) {
                        $query->where('status', status('self_designer', 'id'))
                            ->orWhere(function ($query) {
                                $query->where('status', status('exclusive_design', 'id'))
                                    ->where('use_specification', true);
                            });
                    });
                },
                'revisionedOrders', 'orders.productType', 'client.type', 'manager', 'contact', 'statusInfo',
                'orders.statusInfo', 'orders.attachments', 'orders.group', 'orders.specification'
            ])
            ->whereHas('orders', function ($query) {
                $query->where(function ($query) {
                    $query->where('status', status('self_designer', 'id'))
                        ->orWhere(function ($query) {
                            $query->where('status', status('exclusive_design', 'id'))
                                ->where('use_specification', true);
                        });
                });
            })
            ->get();

        // @todo придумать сортировку по related table выборкой из базы, вместо пхп
        $groupOrders = $groupOrders->sortByDesc(function ($groupOrder) {
            return $groupOrder->revisionedOrders->count();
        });

        return new \Illuminate\Pagination\Paginator($groupOrders, request('per_page'));
    }

    /**
     * Working statuses
     *
     * @param $orderId
     * @param Request $request
     * @return
     */
    public function working($orderId, Request $request)
    {
        $this->validate($request, [
            'action' => 'required',
        ]);

        $order = $this->order->findOrFail($orderId);

        $action = $request->input('action');

        if ($action === 'start') {
            return $order->designerStartWorking();
        }

        // Stop
        return $order->designerStopWorking();
    }

    /**
     * Получает техническое задание для заказа
     *
     * @param $orderId
     * @return \Illuminate\Http\JsonResponse
     */
    public function technicalTask($orderId)
    {
        $order = $this->order->findOrFail($orderId);

        return response()->json([
            'content' => 'Техническое задание для ' . $order->id,
        ]);
    }

    /**
     * Загрузка и конвертация psd шаблона на проверку (
     *
     * @param $orderId
     * @param UploadTemplateForApprovalRequest $request
     * @return mixed
     */
    public function uploadForApproval($orderId, UploadTemplateForApprovalRequest $request)
    {
        $order = $this->order->findOrFail($orderId);

        $firstSideTemplate = $request->file('firstSide');
        $secondSideTemplate = $request->file('secondSide');

        $uploadPath = 'app/templates';

        // Сохранить шаблоны
        $filename = str_random().'.'.$firstSideTemplate->getClientOriginalExtension();
        $firstSideTemplate->move(storage_path($uploadPath), $filename);
        $firstPsdPath = storage_path($uploadPath.'/'.$filename);

        $secondPsdPath = null;
        if ($secondSideTemplate) {
            $filename = str_random().'.'.$secondSideTemplate->getClientOriginalExtension();
            $secondSideTemplate->move(storage_path($uploadPath), $filename);
            $secondPsdPath = storage_path($uploadPath.'/'.$filename);
        }

        try {
            $template = TemplateLibrary::makeFromPsd($firstPsdPath, $secondPsdPath);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
            ]);
        }

        // Associate template with client
        //$template->client()->associate($order->client);
        //$template->save();

        // Привязать шаблон к заказу
        $order->selectTemplate($template);
        $order->save();

        //addLog($order->id, 'Отправлен на согласование', auth()->user()->id, $order->client->email, null, 0, "на почту заказчика");

        // Сделать редирект на конструктор
        return response()->json([
            'status' => 'redirect',
            'url' => url('orders/edit/'.$order->id),
        ]);
    }

    public function uploadApproved($orderId, Request $request)
    {
        $this->validate($request, [
            'template' => 'required'
        ]);

        $order = $this->order->findOrFail($orderId);

        $template = $request->file('template');

        $filename = str_random().'.'.$template->getClientOriginalExtension();
        $path = 'lib/frx/';

        $template->move(public_path($path), $filename);

        PdfPreviewMaker::make(
            public_path($path.'/'.$filename),
            public_path('lib/frx/'.$filename.'.jpeg')
        );

        $order->status = status('layout_approved', 'id');
        $order->pdf = $filename;
        $order->img = $filename.'.jpeg';
        if ($order->designer_working) {
            $order->designer_working = false;
            $order->designer_working_time += microtime(true) - $order->designer_working_starttime;
        }
        $order->save();

        $order->elements->each(function ($element) use ($filename) {
            $element->preview = '/lib/frx/'.$filename.'.jpeg';
            $element->save();
        });

        Template::makeFromOrder($order);

        if ($order->group->next == 0) {
            if ($order->group->orders()->whereIn('status', [3, 4, 5, 9, 301])->where('id', '<>', $order->id)->count() == 0) {
                if (\Auth::user()->accessLevel() == 1) {
                    (new SendInvoiceNotification)->send($order);
                } else {
                    if ($order->group->client()->first()->getDetailsOK() == 0) {
                        (new SendInvoiceNotification)->send($order);
                    }
                }
            }
        }

        addLog($order->id, 'Макет утверждён', $order->group->client->user_id, 'Работа дизайнера: '.round($order->designer_working_time / 3600,2).' ч.', null, 0, "в присутствии дизайнера");

        return $order;
    }
}
