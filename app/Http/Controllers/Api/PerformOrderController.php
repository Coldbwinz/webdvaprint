<?php

namespace App\Http\Controllers\Api;

use App\Classes\Archiver\ArchiverContract;
use App\Http\Requests\UploadDescentRequest;
use App\Models\SheetFormat;
use App\OrderElement;
use App\Orders;
use App\PerformOrder;
use App\PerformOrderStatus;
use App\Services\Layouts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use ZipArchive;

class PerformOrderController extends Controller
{
    /**
     * @var PerformOrder
     */
    private $performOrder;

    /**
     * @var PerformOrderStatus
     */
    private $performOrderStatus;

    /**
     * @var Carbon
     */
    private $carbon;

    /**
     * @var Orders
     */
    private $order;

    /**
     * @var OrderElement
     */
    private $orderElement;

    /**
     * @param PerformOrder $performOrder
     * @param PerformOrderStatus $performOrderStatus
     * @param Carbon $carbon
     * @param Orders $order
     * @param OrderElement $orderElement
     */
    public function __construct(
        PerformOrder $performOrder,
        PerformOrderStatus $performOrderStatus,
        Carbon $carbon,
        Orders $order,
        OrderElement $orderElement
    ) {
        $this->order = $order;
        $this->carbon = $carbon;
        $this->performOrder = $performOrder;
        $this->orderElement = $orderElement;
        $this->performOrderStatus = $performOrderStatus;
    }

    /**
     * Get perform orders for prepress queue
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        $performOrders = $this->performOrder
            ->with([
                'elements.status',
                'elements.order.statusInfo',
                'elements.order.group',
                'performOrderStatus',
            ])
            ->filter($request)
            ->get();

        $sorted = $performOrders->sortByDesc(function ($performOrder) {
            $elementWeight = 0;
            foreach ($performOrder->elements as $element) {
                if (! is_null($element->order->sent_revision_at)) {
                    $elementWeight++;
                }
            }

            return $elementWeight;
        });

        $sorted = array_values($sorted->toArray());

        return $sorted;
    }

    /**
     * Store new perform order
     *
     * @param Request $request
     * @return static
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product' => 'required',
            'chromacity' => 'required',
            'print_sheet_format' => 'required',
            'draw' => 'required|numeric',
            'material' => 'required',
            'issue_at' => 'required',
            'elements' => 'required',
        ]);

        $input = $request->all();
        $input['issue_at'] = $this->carbon->parse(Arr::get($request->input('issue_at'), 'data'))
            ->timezone(settings('user_timezone'));
        $input['print_sheet_format'] = $request->input('print_sheet_format.width').'x'.$request->input('print_sheet_format.height');
        $input['postpress_processes'] = $request->input('postpress_processes');

        SheetFormat::createIfNotExists([
            'width' => $request->input('print_sheet_format.width'),
            'height' => $request->input('print_sheet_format.height'),
            'type' => 'print',
        ]);

        $performOrder = $this->performOrder->create($input);
        $status = $this->performOrderStatus->find(PerformOrderStatus::PREPARING_TO_PRINT);
        $performOrder->performOrderStatus()->associate($status);
        $performOrder->save();

        $elementsData = collect($request->input('elements'));
        $elementIds = $elementsData->pluck('id');
        $elements = $this->orderElement->whereIn('id', $elementIds)->get();

        $elements->each(function (OrderElement $element) use ($elementsData, $performOrder) {
            $data = $elementsData->where('id', $element->id)->first();
            $element->perform_order_id = $performOrder->id;
            $element->perform_draw = $data['perform_draw'];
            $element->perform_layout = $data['perform_layout'];
            $element->status_id = status('preparing_to_press', 'id');
            $element->save();
        });

        $performOrder->setOrderElementsStatus(status('preparing_to_press'));

        // Логи в карточку заказа
        $performOrder->elements->each(function ($element) {
            addLog($element->order->id, status('preparing_to_press')->id, null, '', null, 0, $element->name);
        });

        $performOrder->templates_path = $this->makeZip($performOrder);

        $performOrder->load([
            'elements.status',
            'elements.order.statusInfo',
            'elements.order.group',
            'performOrderStatus',
        ]);

        return $performOrder;
    }

    /**
     * @param $performOrder
     *
     * @return string
     * @throws \Exception
     */
    protected function makeZip($performOrder)
    {
        $storePath = public_path('perform-orders/templates');
        $zipArchive = $storePath . '/' . 'perform-order-' . $performOrder->id . '.zip';

        if (! is_dir($storePath)) {
            mkdir($storePath, 0777, true);
        }

        if (file_exists($zipArchive)) {
            unlink($zipArchive);
        }

        // Create archive with templates
        $archiver = app(ArchiverContract::class)->create($zipArchive);

        /** @var OrderElement $element */
        foreach ($performOrder->elements as $element) {
            try {
                $localname = "templates-{$element->id}-{$element->type}.pdf";
                $archiver->addFile(public_path($element->pdf_path), $localname);
            } catch (\Exception $exception) {
                //TODO:: Когда будут шаблоны на всё этото кусок не понадобится
                $localname = "templates-{$element->id}-{$element->type}.jpg";
                $archiver->addFile(public_path($element->preview), $localname);
            }
        }

        $archiver->save();

        return '/perform-orders/templates/perform-order-' . $performOrder->id . '.zip';
    }

    /**
     * Upload descent
     *
     * @param $id
     * @param UploadDescentRequest $request
     * @return array
     */
    public function uploadDescent($id, UploadDescentRequest $request)
    {
        $performOrder = PerformOrder::findOrFail($id);

        $performOrder->attachDescent($request);
        $performOrder->setOrderElementsStatus(status('in_press'));

        return [
            'success' => true,
            'perform_order' => $performOrder,
        ];
    }

    /**
     * @return null
     */
    public function getNextIndex()
    {
        // @todo
        return null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function downloadDescent($id)
    {
        $performOrder = PerformOrder::findOrFail($id);

        $performOrder->setStatus(PerformOrderStatus::SENT_TO_PRINT);
        $performOrder->setElementsStatus(status('in_press'));

        $performOrder->load(['elements.status', 'performOrderStatus', 'elements.order']);

        return $performOrder;
    }

    /**
     * Set printed status
     *
     * @param $id
     * @return mixed
     */
    public function printed($id)
    {
        $performOrder = PerformOrder::findOrFail($id);
        $performOrder->printed();

        return $performOrder;
    }

    /**
     * Download templates
     *
     * @param $id
     */
    public function downloadTemplates($id)
    {
        $performOrder = PerformOrder::findOrFail($id);

        // Update orders status
        $performOrder->setOrdersStatus(status('preparing_to_press'));

        $performOrder->load([
            'orders.productType', 'orders.statusInfo', 'performOrderStatus', 'orders.group'
        ]);

        return $performOrder;
    }

    /**
     * Выполнение постпресс операции
     *
     * @param $performOrderId
     * @param Request $request
     * @return array
     */
    public function postpressComplete($performOrderId, Request $request)
    {
        $this->validate($request, [
            'element_ids' => 'required',
            'postpress' => 'required',
        ]);

        $postpressProcess = [$request->input('postpress')];

        $performOrder = $this->performOrder
            ->with(['elements' => function ($query) use ($request) {
                $query->whereIn('id', $request->input('element_ids'));
            }, 'elements.status', 'elements.order', 'performOrderStatus'])
            ->where('id', $performOrderId)->firstOrFail();

        $performOrder->completePostpressOperations($postpressProcess);

        if ($performOrder->isForProduct()) {
            addLog($performOrder->collectOrder->id, implode(', ', $postpressProcess), null, '', null, 0, $performOrder->product);
        }

        $performOrder->load('elements.order', 'elements.status', 'performOrderStatus');

        return $performOrder;
    }

    /**
     * Процесс резки
     *
     * @param $performOrderId
     */
    public function cutProcess($performOrderId)
    {
        $performOrder = $this->performOrder
            ->with(['elements.status', 'elements.order', 'performOrderStatus'])
            ->where('id', $performOrderId)->firstOrFail();

        $collected = $performOrder->updateCollectedField();

        if ($performOrder->isForProduct() && $collected) {
            return $performOrder->complete($performOrder->collectOrder);
        }

        foreach ($performOrder->elements as $element) {
            // Элемент листового изделия
            if ($element->order->price_details['printler_type'] == 1) {
                if ($element->allPostpressPerformed()) {
                    return $performOrder->complete($element->order);
                }

                $this->performOrder->createForElement($element, [
                    'number' => $performOrder->id . '.' . $element->id,
                    'collected' => true,
                ]);
            }

            // Элемент часть многостраничного изделия
            if ($element->order->price_details['printler_type'] == 3) {
                if ($element->allPostpressPerformed()) {
                    $newPerformOrder = $this->performOrder->where('collect_order_id', $element->order->id)->first();

                    // Создание наряда на объединение элементов
                    if (! $newPerformOrder) {
                        $binding = 'переплёт ' . $element->order->price_details['selected_type'];

                        $newPerformOrder = $this->performOrder->create([
                            'product' => $element->order->price_details['selected_product_type_name'],
                            'chromacity' => null,
                            'print_sheet_format' => $element->format,
                            'draw' => $element->order->draw,
                            'meterial' => $element->material,
                            'postpress_processes' => [$binding],
                            'issue_at' => $this->carbon->timestamp($element->order->close_date),
                            'perform_order_status_id' => 4,
                            'collect_order_id' => $element->order->id,
                            'collected' => false,
                            'preview' => $performOrder->preview,
                        ]);
                    }

                    $newPerformOrder->associateElement($element);
                    $newPerformOrder->updateCollectedField();

                    addLog($element->order->id, status('ready')->id, null, '', null, 0, $element->name);
                } else {

                    // Если не все операции постпресса выполнены
                    // Создать наряд на выполнение
                    $this->performOrder->createForElement($element, [
                        'number' => $performOrder->id . '.' . $element->id,
                        'collected' => $collected,
                    ]);
                }
            }
        }

        return $performOrder->close();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function layout(Request $request)
    {
        $this->validate($request, [
            'width' => 'required',
            'height' => 'required',
            'format' => 'required',
        ]);

        $binding = $request->input('binding');
        $turnCount = $request->input('turnCount');

        if ($binding === 'на скрепке') {
            $decompositionSlew = explode('x', $request->input('decompositionSlew'));

            $tolerance = 4;

            $width = $decompositionSlew[0] + $tolerance;
            $height = $decompositionSlew[1] + $tolerance;

            $formatWidth = $request->input('width');
            $formatHeight = $request->input('height');

            $diffWidth = floor($formatWidth / $width);
            $diffWidthHeight = floor($formatWidth / $height);
            $maxDiffWidth = $diffWidth > $diffWidthHeight ? $diffWidth : $diffWidthHeight;

            $diffHeight = floor($formatHeight / $width);
            $diffHeightWidth = floor($formatHeight / $height);
            $maxDiffHeight = $diffHeight > $diffHeightWidth ? $diffHeight : $diffHeightWidth;

            $count = $maxDiffWidth * $maxDiffHeight;

            return [
                'layout' => $count,
                'preview' => asset('images/layouts/'.$count.'_turn.png')
            ];
        }

        $layout = new Layouts(
            $request->input('width'),
            $request->input('height'),
            $request->input('format')
        );

        return [
            'layout' => $layout->getLayout(),
            'preview' => $layout->getPreviewPath(),
        ];
    }

    /**
     * Pause perform order
     *
     * @param PerformOrder $performOrder
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pause(PerformOrder $performOrder, Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|max:255',
        ]);

        $performOrder->pause($request->input('reason'), true, 'queue');

        return response()->json([
            'status' => 'success',
            'perform_order' => $performOrder
        ]);
    }

    /**
     * Resume perform order
     *
     * @param PerformOrder $performOrder
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resume(PerformOrder $performOrder, Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|max:255',
        ]);

        $performOrder->resume($request->input('reason'));

        return response()->json([
            'status' => 'success',
            'perform_order' => $performOrder
        ]);
    }
}
