<?php

namespace App\Http\Controllers\Api;

use App\Clients;
use App\Contacts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Factory as Validator;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\File;

class ContactController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Contacts $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacts $contact)
    {
        $this->validate($request, [
            'name' => 'required',
            'value' => 'required',
        ]);

        // Validate email
        if ($request->input('name') === 'email') {
            $v = \Validator::make(['email' => $request->input('value')], [
                'email' => 'email|unique:contacts'
            ]);

            if ($v->fails()) {
                return response()->json($v->errors(), 422);
            }
        }

        $attribute = $request->input('name');
        $value = $request->input('value');

        if ($attribute === 'position') {
            $contact->setInformationFieldForClient('position', $value);
            $contact->save();
        }

        $contact->update([
            $attribute => $value
        ]);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * @param Request $request
     * @param Contacts $contact
     * @param Clients $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateClientInformation(Request $request, Contacts $contact, Clients $client)
    {
        $this->validate($request, [
            'name' => 'required',
            'value' => 'required_unless:name,position',
        ]);

        $attribute = $request->input('name');
        $value = $request->input('value');

        $contact->setInformationFieldForClient($client->id, $attribute, $value);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Change contact's avatar
     *
     * @param Contacts $contact
     * @param Request $request
     * @param ImageManager $imageManager
     * @return string
     */
    public function avatar(Contacts $contact, Request $request, ImageManager $imageManager)
    {
        $this->validate($request, [
            'contact-avatar' => 'required',
        ]);

        $avatar = $imageManager
            ->make($request->file('contact-avatar'))
            ->fit($request->input('width'), $request->input('height'));

        $name = str_random();
        $extension = mime_to_extension($avatar->mime());

        $filename = "{$name}.{$extension}";
        $folder = "/upload/users/";
        $path = $folder . $filename;

        if (! is_dir(public_path($folder))) {
            rmkdir(public_path($folder));
        }

        $avatar->save(public_path($path));

        $contact->avatar = $path;
        $contact->save();

        return asset($path);
    }

    public function checkWhoCall(Request $request) {
        $result = new \stdClass();
        $all_contacts = Contacts::all();

        foreach ($all_contacts as $contact) {
            $new_phone = $contact->phone;
            $new_phone = preg_replace('/\D/', '', $new_phone);
            if (strlen($new_phone) > 0) {
                if (strpos($new_phone, $request->phone_number) !== false) {
                    $result->clients = $contact->clients()->get();
                    $result->contact = $contact;
                    return  json_encode($result);
                }
            }
        }
        $result->clients_list = \DB::table('clients')
            ->join('contacts_clients_relation', 'clients.id', '=', 'contacts_clients_relation.client_id')
            ->leftjoin('contacts', 'contacts_clients_relation.contact_id', '=', 'contacts.id')->get();
        foreach ($result->clients_list as $client) {
            $new_phone = $client->phone;
            $new_phone = substr(preg_replace('/\D/', '', $new_phone), -10, 10);
            if ($new_phone == $request->phone_number) {
                $result = new \stdClass();
                $result->contact = $client;
                return  json_encode($result);
            }
        }

        foreach ($result->clients_list as $client) {
            if (!is_null($client->connections)) {
                foreach (json_decode($client->connections) as $connection) {
                    if ($connection->name == 'phone') {
                        $new_phone = $connection->value;
                        $new_phone = substr(preg_replace('/\D/', '', $new_phone), -10, 10);
                        if ($new_phone == $request->phone_number) {
                            $result = new \stdClass();
                            $result->contact = $client;
                            return  json_encode($result);
                        }
                    }
                }
            }
        }
        return json_encode($result);
    }
}
