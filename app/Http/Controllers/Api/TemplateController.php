<?php

namespace App\Http\Controllers\Api;

use App\Template;
use App\TemplateLibrary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    /**
     * @var \App\TemplateLibrary
     */
    protected $library;

    /**
     * @var \App\Template
     */
    protected $clientLibrary;

    /**
     * @param \App\TemplateLibrary $library
     * @param \App\Template $clientLibrary
     */
    public function __construct(TemplateLibrary $library, Template $clientLibrary)
    {
        $this->library = $library;
        $this->clientLibrary = $clientLibrary;
    }

    /**
     * Get templates from library
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'product_type_id' => 'exists:product_types,id',
            'two_side' => 'boolean',
            'folds' => 'numeric',
            'size' => 'regex:/\d+x\d+/',
        ]);

        $templates = $this->library->filter($request->all())->paginate(
            (int) $request->input('per_page', 20)
        );

        return response()->json($templates);
    }

    /**
     * Get library and client templates count
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCount(Request $request)
    {
        $this->validate($request, [
            'two_side' => 'boolean',
            'folds' => 'numeric',
            'size' => 'regex:/\d+x\d+/',
            'client_id' => 'exists:clients,id',
        ]);

        $libraryTemplatesCount = 0;

        if ($request->input('client_id') === null) {
            $libraryTemplatesCount = $this->library->filter($request->all())->count();
        }

        $clientTemplatesCount = $this->clientLibrary->filter($request->all())->count();

        $templatesCount = $libraryTemplatesCount + $clientTemplatesCount;

        return response()->json([
            'templates_count' => $templatesCount,
        ]);
    }
}
