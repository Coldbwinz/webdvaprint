<?php

namespace App\Http\Controllers\Api\Dictionary;

use App\Classes\CurrencyTranslator\CurrencyTranslator;
use App\Models\SheetFormat;
use Illuminate\Http\Request;
use App\Models\Material\Material;
use App\Models\Material\CoverType;
use App\Models\Material\MaterialType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dictionary\MaterialRequest;

class MaterialController extends Controller
{
    /**
     * @var CurrencyTranslator
     */
    protected $currencyTranslator;

    /**
     * @param CurrencyTranslator $currencyTranslator
     */
    public function __construct(CurrencyTranslator $currencyTranslator)
    {
        $this->currencyTranslator = $currencyTranslator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Dictionary\MaterialRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialRequest $request)
    {
        $materialType = MaterialType::findOrFail($request->input('material_type_id'));
        $coverType = CoverType::findOrFail($request->input('cover_type_id'));

        $printSheetFormat = $request->input('print_sheet_format');
        $initialSheetFormat = $request->input('initial_sheet_format');
        $printSheetsNumber = $request->input('print_sheets_number');

        SheetFormat::createIfNotExists([
            'width' => $printSheetFormat['width'],
            'height' => $printSheetFormat['height'],
            'type' => 'print',
        ]);

        SheetFormat::createIfNotExists([
            'width' => $initialSheetFormat['width'],
            'height' => $initialSheetFormat['height'],
            'type' => 'initial',
        ]);

        $dencities = $request->input('density_items');

        $newMaterials = [];

        foreach ($dencities as $dencity) {
            $initialSheetWeight = $initialSheetFormat['width'] * $initialSheetFormat['height'] / 1000000 * $dencity['density'];
            $priceTon = $request->input('price');

            $printSheetPrice = Material::calculateSheetPrice($initialSheetWeight, $printSheetsNumber, $priceTon['value'], $priceTon['currency']);

            $material = new Material();
            $material->fill($request->all());
            $material->coverType()->associate($coverType);
            $material->materialType()->associate($materialType);
            $material->print_sheet_width = $printSheetFormat['width'];
            $material->print_sheet_height = $printSheetFormat['height'];
            $material->initial_sheet_width = $initialSheetFormat['width'];
            $material->initial_sheet_height = $initialSheetFormat['height'];
            $material->price_ton = $priceTon['value'];
            $material->price_ton_currency = $priceTon['currency'];
            $material->name = request('name');
            $material->sheet_price = $printSheetPrice;
            $material->density = $dencity['density'];
            $material->stock_availability = $dencity['stock'];
            $material->stock_balance = $dencity['balance'];
            $newMaterials[] = $material->save();
        }

        return response()->json([
            'status' => 'success',
            'redirect' => route('dictionary.material'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $material = Material::findOrFail($id);

        return [
            'material_type_id' => $material->material_type_id,
            'cover_type_id' => $material->cover_type_id,
            'name' => $material->name,
            'price' => [
                'value' => $material->price_ton,
                'currency' => $material->price_ton_currency,
            ],
            'initial_sheet_format' => [
                'width' => $material->initial_sheet_width,
                'height' => $material->initial_sheet_height
            ],
            'print_sheet_format' => [
                'width' => $material->print_sheet_width,
                'height' => $material->print_sheet_height
            ],
            'lamination' => $material->lamination,
            'density_items' => [
                [
                    'density' => $material->density,
                    'stock' => $material->stock_availability,
                    'balance' => $material->stock_balance,
                ]
            ],
        ];
    }

    /**
     * Update material
     *
     * @param MaterialRequest $request
     * @param $materialId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MaterialRequest $request, $materialId)
    {
        $material = Material::findOrFail($materialId);
        $materialType = MaterialType::findOrFail($request->input('material_type_id'));
        $coverType = CoverType::findOrFail($request->input('cover_type_id'));

        $printSheetFormat = $request->input('print_sheet_format');
        $initialSheetFormat = $request->input('initial_sheet_format');
        $printSheetsNumber = $request->input('print_sheets_number');

        SheetFormat::createIfNotExists([
            'width' => $printSheetFormat['width'],
            'height' => $printSheetFormat['height'],
            'type' => 'print',
        ]);

        SheetFormat::createIfNotExists([
            'width' => $initialSheetFormat['width'],
            'height' => $initialSheetFormat['height'],
            'type' => 'initial',
        ]);

        $priceTon = $request->input('price');
        $dencity = $request->input('density_items')[0];
        $initialSheetWeight = $initialSheetFormat['width'] * $initialSheetFormat['height'] / 1000000 * $dencity['density'];

        $printSheetPrice = Material::calculateSheetPrice($initialSheetWeight, $printSheetsNumber, $priceTon['value'], $priceTon['currency']);

        $material->fill($request->all());
        $material->coverType()->associate($coverType);
        $material->materialType()->associate($materialType);
        $material->print_sheet_width = $printSheetFormat['width'];
        $material->print_sheet_height = $printSheetFormat['height'];
        $material->initial_sheet_width = $initialSheetFormat['width'];
        $material->initial_sheet_height = $initialSheetFormat['height'];
        $material->price_ton = $priceTon['value'];
        $material->price_ton_currency = $priceTon['currency'];
        $material->name = $request->input('name');
        $material->sheet_price = $printSheetPrice;
        $material->density = $dencity['density'];
        $material->stock_availability = $dencity['stock'];
        $material->stock_balance = $dencity['balance'];
        $material->save();

        return response()->json([
            'status' => 'success',
            'redirect' => route('dictionary.material'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
