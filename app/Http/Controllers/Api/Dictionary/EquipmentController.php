<?php

namespace App\Http\Controllers\Api\Dictionary;

use App\Models\SheetFormat;
use Illuminate\Support\Arr;
use App\Models\Equipment\Equipment;
use App\Http\Controllers\Controller;
use App\Models\Equipment\EquipmentPrintType;
use App\Http\Requests\Dictionary\EquipmentRequest;
use App\Models\Equipment\EquipmentDigitalPrintProperty;

class EquipmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  EquipmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EquipmentRequest $request)
    {
        $digitalPrintType = EquipmentPrintType::find(1);

        $equipment = $digitalPrintType->equipments()->create([
            'name' => $request->input('equipment_name'),
        ]);

        \DB::transaction(function () use ($request, $equipment) {

            $technologicalFields = $request->input('technological_fields');
            $bannerPrintEnabled = $request->input('banner_print_enabled');
            $bannerPrint = $request->input('banner_print_panel.data');
            $valve = $request->input('valve');

            $maxPrintSheetFormat = $request->input('max_print_sheet_format');
            SheetFormat::createIfNotExists([
                'width' => $maxPrintSheetFormat['width'],
                'height' => $maxPrintSheetFormat['height'],
                'type' => SheetFormat::SHEET_FORMAT_MAX,
            ]);

            if ($bannerPrintEnabled) {
                SheetFormat::createIfNotExists([
                    'width' => Arr::get($bannerPrint, 'max_print_sheet_format.width'),
                    'height' => Arr::get($bannerPrint, 'max_print_sheet_format.height'),
                    'type' => SheetFormat::SHEET_FORMAT_MAX,
                ]);
            }

            $equipment->name  = $request->input('equipment_name');
            $equipment->save();

            $equipment->unsetPrintParameters();

            $printColorPanels = $request->input('print_color_panels');

            foreach ($printColorPanels as $printColorPanel) {
                $equipment->addPrintParameter($maxPrintSheetFormat, $technologicalFields, $valve, $printColorPanel['data']);
            }

            if ($request->input('banner_print_enabled')) {
                $bannerPrintPanel = $request->input('banner_print_panel');
                $bannerPrint = $bannerPrintPanel['data'];

                $equipment->addPrintParameter(
                    $bannerPrint['max_print_sheet_format'],
                    $bannerPrint['technological_fields'],
                    $bannerPrint['valve'],
                    $bannerPrint['chromaticity'],
                    true
                );
            }
        });

        return response()->json([
            'status' => 'success',
            'redirect' => route('dictionary.equipment'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equipment = Equipment::findOrFail($id);

        $printColorPanels = [];
        $bannerPrintPanel = false;

        foreach ($equipment->printParameters as $index => $parameter) {
            if ($parameter->properties->banner_print === false) {
                $printColorPanels[] = [
                    'title' => trans('dictionary.equipment.'.$parameter->properties->chromaticity),
                    'collapsed' => false,
                    'data' => [
                        'chromaticity' => $parameter->properties->chromaticity,
                        'click_price' => [
                            'value' => $parameter->properties->click_price,
                            'currency' => $parameter->properties->currency,
                        ],
                        "performance" => $parameter->properties->performance,
                        "rebuilding" => $parameter->properties->rebuilding,
                        "fitting_1" => $parameter->properties->fitting_1,
                        "fitting_2" => $parameter->properties->fitting_2,
                        "defect_norm" => $parameter->properties->defect_norm,
                    ]
                ];
            } else {
                $bannerPrintPanel = [
                    'title' => 'Баннерная печать',
                    'collapsed' => false,
                    'data' => [
                        'max_print_sheet_format' => [
                            'width' => $parameter->properties->max_sheet_width,
                            'height' => $parameter->properties->max_sheet_height,
                        ],
                        'technological_fields' => $parameter->properties->technological_fields,
                        'valve' => [
                            'value' => $parameter->properties->valve,
                            'side' => $parameter->properties->valve_side,
                        ],
                        'chromaticity' => [
                            'chromaticity' => $parameter->properties->chromaticity,
                            'click_price' => [
                                'value' => $parameter->properties->click_price,
                                'currency' => $parameter->properties->currency,
                            ],
                            "performance" => $parameter->properties->performance,
                            "rebuilding" => $parameter->properties->rebuilding,
                            "fitting_1" => $parameter->properties->fitting_1,
                            "fitting_2" => $parameter->properties->fitting_2,
                            "defect_norm" => $parameter->properties->defect_norm,
                        ],
                    ]
                ];
            }
        }

        $firstParameter = $equipment->printParameters->first();

        return [
            'print_type_id' => $equipment->printType->id,
            'equipment_name' => $equipment->name,
            'max_print_sheet_format' => [
                'width' => $firstParameter->properties->max_sheet_width,
                'height' => $firstParameter->properties->max_sheet_height,
            ],
            'technological_fields' => $firstParameter->properties->technological_fields,
            'valve' => [
                'value' => $firstParameter->properties->valve,
                'side' => $firstParameter->properties->valve_side,
            ],
            'print_color_panels' => $printColorPanels,
            'banner_print_panel' => $bannerPrintPanel,
            'banner_print_enabled' => (bool) $bannerPrintPanel,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EquipmentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EquipmentRequest $request, $id)
    {
        $equipment = Equipment::findOrFail($id);

        \DB::transaction(function () use ($request, $equipment) {

            $technologicalFields = $request->input('technological_fields');
            $bannerPrintEnabled = $request->input('banner_print_enabled');
            $bannerPrint = $request->input('banner_print_panel.data');
            $valve = $request->input('valve');

            $maxPrintSheetFormat = $request->input('max_print_sheet_format');
            SheetFormat::createIfNotExists([
                'width' => $maxPrintSheetFormat['width'],
                'height' => $maxPrintSheetFormat['height'],
                'type' => SheetFormat::SHEET_FORMAT_MAX,
            ]);

            if ($bannerPrintEnabled) {
                SheetFormat::createIfNotExists([
                    'width' => Arr::get($bannerPrint, 'max_print_sheet_format.width'),
                    'height' => Arr::get($bannerPrint, 'max_print_sheet_format.height'),
                    'type' => SheetFormat::SHEET_FORMAT_MAX,
                ]);
            }

            $equipment->name  = $request->input('equipment_name');
            $equipment->save();

            $equipment->unsetPrintParameters();

            $printColorPanels = $request->input('print_color_panels');

            foreach ($printColorPanels as $printColorPanel) {
                $equipment->addPrintParameter($maxPrintSheetFormat, $technologicalFields, $valve, $printColorPanel['data']);
            }

            if ($request->input('banner_print_enabled')) {
                $bannerPrintPanel = $request->input('banner_print_panel');
                $bannerPrint = $bannerPrintPanel['data'];

                $equipment->addPrintParameter(
                    $bannerPrint['max_print_sheet_format'],
                    $bannerPrint['technological_fields'],
                    $bannerPrint['valve'],
                    $bannerPrint['chromaticity'],
                    true
                );
            }
        });

        return response()->json([
            'status' => 'success',
            'redirect' => route('dictionary.equipment'),
        ]);
    }
}
