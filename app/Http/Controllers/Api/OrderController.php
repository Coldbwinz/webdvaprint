<?php

namespace App\Http\Controllers\Api;

use App\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * @var Orders
     */
    private $order;
    /**
     * @var Carbon
     */
    private $carbon;

    /**
     * OrderController constructor.
     * @param Orders $order
     * @param Carbon $carbon
     */
    public function __construct(Orders $order, Carbon $carbon)
    {
        $this->order = $order;
        $this->carbon = $carbon;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        return $this->order
            ->whereHas('elements', function ($query) {
                $query->doesntHave('performOrder');
            })
            ->with(['statusInfo', 'productType', 'elements' => function ($query) {
                $query->doesntHave('performOrder')
                    ->where('chromacity', '!=', 'без печати');
            }, 'elements.status'])
            ->filter($request)
            ->latest('sent_revision_at')
            ->get()
            ->map(function ($order) {
                $order->product = $order->productType
                    ? $order->productType->name
                    : $order->price_details['selected_product_type_name'];

                $printlerType = $order->price_details['printler_type'];

                $material = $printlerType == 3
                    ? $order->price_details['selected_material']
                    : $order->price_details['selected_product'];

                $order->format = $order->price_details['selected_size'];
                $order->chromacity = $order->price_details['selected_chromacity'];
                $order->material = $material;
                $order->postpress_operations = $order->price_details['selected_options'];
                $order->issue_at = $this->carbon->timestamp($order->close_date);
                $order->preview = filter_var($order->img, FILTER_VALIDATE_URL) ?: '/lib/frx/' . $order->img;
                $order->multipage = $printlerType == 3;
                $order->binding = isset($order->price_details['selected_type']) && $order->price_details['selected_type'] !== '###'
                    ? $order->price_details['selected_type']
                    : null;

                $order->canCollect = $printlerType != 3 || ($printlerType == 3 && is_null($order->elements[0]->pages_number));
                $order->canDecompose = $printlerType == 3 && ! is_null($order->elements[0]->pages_number);

                return $order;
            });
    }

    /**
     * Можно ли приостановить заказ
     *
     * @param Orders $order
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function checkOrderCanPause(Orders $order)
    {
        $elements = $order->elements()->has('performOrder')->get();

        foreach ($elements as $element) {
            $elementsFromOtherOrdersCount = $element->performOrder->elements()
                ->where('order_id', '!=', $order->id)
                ->count();

            if ($elementsFromOtherOrdersCount) {
                return response()->json([
                    'can_pause' => false,
                ]);
            }
        }

        return response()->json([
            'can_pause' => true,
            'group_order_id' => $order->group->id,
            'position' => $order->position,
        ]);
    }
}
