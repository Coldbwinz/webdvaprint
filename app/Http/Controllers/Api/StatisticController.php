<?php

namespace App\Http\Controllers\Api;

use App\Models\Statistic\StatisticClient;
use App\Models\Statistic\StatisticOrder;
use App\Orders;
use App\UserGroup;
use DB;
use App\User;
use Carbon\Carbon;
use App\Http\Requests;
use App\Models\Statistic;
use Illuminate\Http\Request;
use App\Services\CarbonDatePeriod;
use App\Http\Controllers\Controller;
use App\Models\Statistic\StatisticInvoice;

class StatisticController extends Controller
{
    const CURRENT_MONTH = 'current_month';
    const PREVIOUS_MONTH = 'previous_month';
    const NEXT_MONTH = 'next_month';
    const CURRENT_WEEK = 'current_week';
    const PREVIOUS_WEEK = 'previous_week';
    const CURRENT_YEAR = 'current_year';
    const PREVIOUS_YEAR = 'previous_year';

    /**
     * @var Carbon
     */
    protected $carbon;

    /**
     * StatisticController constructor.
     * @param Carbon $carbon
     */
    public function __construct(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'period' => 'required',
        ]);

        $requestPeriod = $request->input('period');

        if ($requestPeriod === static::CURRENT_MONTH) {
            $period = new CarbonDatePeriod(Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth());
        } elseif ($requestPeriod === static::PREVIOUS_MONTH) {
            $period = new CarbonDatePeriod(Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->subMonths(2)->startOfMonth(), Carbon::now()->subMonths(2)->endOfMonth());
        } elseif ($requestPeriod === static::NEXT_MONTH) {
            $period = new CarbonDatePeriod(Carbon::now()->addMonth()->startOfMonth(), Carbon::now()->addMonth()->endOfMonth());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
        } elseif ($requestPeriod === static::CURRENT_WEEK) {
            $period = new CarbonDatePeriod(Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek());
        } elseif ($requestPeriod === static::PREVIOUS_WEEK) {
            $period = new CarbonDatePeriod(Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->startOfWeek()->subWeeks(2), Carbon::now()->endOfWeek()->subWeeks(2));
        } elseif ($requestPeriod === static::CURRENT_YEAR) {
            $period = new CarbonDatePeriod(Carbon::now()->startOfYear(), Carbon::now()->endOfYear());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()->subYear());
        } elseif ($requestPeriod === static::PREVIOUS_YEAR) {
            $period = new CarbonDatePeriod(Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()->subYear());
            $previousPeriod = new CarbonDatePeriod(Carbon::now()->startOfYear()->subYears(2), Carbon::now()->endOfYear()->subYears(2));
        }

        $managerId = $request->input('manager_id');

        DB::statement("SET sql_mode = '';");
        $groupedInvoiceStatistic = StatisticInvoice::groupedForPeriod($period, $managerId);
        $invoicesCount = StatisticInvoice::invoicesCount($period, $managerId);
        $paidInvoicesCount = StatisticInvoice::paidInvoicesCount($period, $managerId);

        $chartData = $this->getDataForChart($groupedInvoiceStatistic);

        $managerGroup = UserGroup::findOrFail(UserGroup::MANAGER);

        if ($managerId) {
            $newClientsCountMonthPlan = $managerGroup->new_clients_count_plan;
            $repeatedOrdersCountMonthPlan = $managerGroup->repeated_orders_count_plan;
            $invoicesTotalMonthPlan = $managerGroup->invoices_total_plan;
            $paidInvoicesTotalMonthPlan = $managerGroup->paid_invoices_total_plan;
        } else {
            $managersCount = User::managers()->count();
            $newClientsCountMonthPlan = $managerGroup->new_clients_count_plan * $managersCount;
            $repeatedOrdersCountMonthPlan = $managerGroup->repeated_orders_count_plan * $managersCount;
            $invoicesTotalMonthPlan = $managerGroup->invoices_total_plan * $managersCount;
            $paidInvoicesTotalMonthPlan = $managerGroup->paid_invoices_total_plan; //* $managersCount;
        }

        $newClientsCountPlan = $newClientsCountMonthPlan;
        $repeatedOrdersCountPlan = $repeatedOrdersCountMonthPlan;
        $invoicesTotalPlan = $invoicesTotalMonthPlan;
        $paidInvoicesTotalPlan = $paidInvoicesTotalMonthPlan;

        if ($requestPeriod === static::CURRENT_WEEK) {
            $newClientsCountPlan = round($newClientsCountMonthPlan / 4.2);
            $repeatedOrdersCountPlan = round($repeatedOrdersCountMonthPlan / 4.2);
            $invoicesTotalPlan = round($invoicesTotalMonthPlan / 4.2);
            $paidInvoicesTotalPlan = round($paidInvoicesTotalMonthPlan / 4.2);
        }

        if ($requestPeriod === static::CURRENT_YEAR) {
            $newClientsCountPlan = $newClientsCountMonthPlan * 12;
            $repeatedOrdersCountPlan = $repeatedOrdersCountMonthPlan * 12;
            $invoicesTotalPlan = $invoicesTotalMonthPlan * 12;
            $paidInvoicesTotalPlan = $paidInvoicesTotalMonthPlan * 12;
        }

        $data = [
            // Кнопки
            'new_clients_count' => StatisticClient::newClientsCount($period, $managerId),
            'repeated_orders_count' => StatisticOrder::repeatedOrdersCount($period, $managerId),
            'invoices_total' => StatisticInvoice::invoicesSum($period, $managerId),
            'paid_invoices_total' => StatisticInvoice::paidInvoicesSum($period, $managerId),

            // План менеджера или компании
            'new_clients_count_plan' => (int)$newClientsCountPlan,
            'repeated_orders_count_plan' => (int)$repeatedOrdersCountPlan,
            'invoices_total_plan' => (int)$invoicesTotalPlan,
            'paid_invoices_total_plan' => (int)$paidInvoicesTotalPlan,

            // Воронка
            'sent_business_suggestions_count' => 500,
            'negotiations_count' => 400,
            'calculated_applications_count' => 300,
            'invoices_count' => $invoicesCount,
            'paid_invoices_count' => $paidInvoicesCount,

            // График
            'invoices_chart' => $chartData,

            'new_orders_total' => StatisticInvoice::newOrdersTotal($period, $managerId),
            'repeated_orders_total' => StatisticInvoice::repeatOrdersTotal($period, $managerId),
            'expected_payments_total' => StatisticInvoice::expectedAmountSum($managerId),
            'arrears_total' => StatisticInvoice::arrearsAmountSum($managerId),

            // Данные за предыдущий период
            'previous_paid_invoices_total' => StatisticInvoice::paidInvoicesSum($previousPeriod, $managerId),
            'previous_new_orders_total' => StatisticInvoice::newOrdersTotal($previousPeriod, $managerId),
            'previous_repeated_orders_total' => StatisticInvoice::repeatOrdersTotal($previousPeriod, $managerId),
        ];

        return response()->json($data);
    }

    /**
     * Получает массив для графика оборота
     *
     * @param $records
     * @return array
     */
    protected function getDataForChart($records)
    {
        $labels = [];
        $invoicesData = [];
        $paidInvoicesData = [];
        $expectedData = [];

        foreach ($records as $record) {
            $labels[] = $this->carbon->parse($record->date)->format('d.m.Y');
            $invoicesData[] = $record->invoices;
            $paidInvoicesData[] = $record->paid_invoices;
            $expectedData[] = $record->expected;
        }

        return [
            'labels' => $labels,

            'datasets' => [
                [
                    'label' => 'Выставленные счета',
                    'data' => $invoicesData,
                    'borderColor' => 'rgb(210, 214, 222)',
                ],

                [
                    'label' => 'Оплаченные счета',
                    'data' => $paidInvoicesData,
                    'backgroundColor' => "rgba(60,141,188,0.9)",
                    'borderColor' => 'rgba(60,141,188,0.8)',
                ],

                [
                    'label' => 'Ожидаемые поступления',
                    'data' => $expectedData,
                    'backgroundColor' => "#FFE19C",
                    'borderColor' => '#ceb374',
                ],
            ]
        ];
    }

    public function orders()
    {
        $postpressCount = Orders::where('status', status('in_postpress', 'id'))->count();

        $printCount = Orders::whereIn('status', [status([
            'in_press',
        ], 'id')])->count();

        $prepareToPrintCount = Orders::whereIn('status', status([
            'in_work', 'preparing_to_press',
        ], 'id'))->count();

        $templateWorkCount = Orders::whereIn('status', status([
            'on_agreeing', 'shown_for_client', 'sent_for_approval', 'sent_again',
            'layout_prepares_client', 'exclusive_design', 'self_designer',
            'parsing', 'prepares_agreement', 'not_fully_decorated', 'draft',
        ], 'id'))->count();

        $pausedCount = Orders::whereIn('status', status(['paused'], 'id'))->count();

        $cancelCount = Orders::whereIn('status', [status(['canceled'], 'id')])->count();

        $waitingForProcessCount = Orders::whereIn('status', status([
            'layout_approved', 'layout_checked',
        ], 'id'))->count();

        $contractorsCount = Orders::where('with_contractor', true)->count();

        $qualityControlCount = Orders::whereIn('status', status([
            'ready',
        ], 'id'))->count();

        $issueOrdersCount = Orders::whereIn('status', status([
            'on_issue', 'sent_to_issue_point',
        ], 'id'))->count();

        $marriageCount = Orders::whereIn('status', status([
            'return_marriage', 'on_rework', 'closed_money_back', 'closed_with_discount',
        ], 'id'))->count();

        $data = [
            'labels' => [
                'Работа с макетом',
                'Ждёт запуска в работу',
                'Подготовка к печати',
                'В печати',
                'Послепечатная обработка',
                'У подрядчика',
                'Контроль качества',
                'На выдаче',
                'Приостановленные заказы',
                'Отмененные заказы',
                'Брак',
            ],

            'datasets' => [
                [
                    'label' => 'Orders',
                    'backgroundColor' => [
                        '#8263B5',
                        '#00a65a',
                        '#FF784F',
                        '#FFE19C',
                        '#922D50',
                        '#3c8dbc',
                        '#f39c12',
                        '#00c0ef',
                        '#d2d6de',
                        '#DB9D47',
                        '#584D3D',
                    ],
                    'data' => [
                        $templateWorkCount,
                        $waitingForProcessCount,
                        $prepareToPrintCount,
                        $printCount,
                        $postpressCount,
                        $contractorsCount,
                        $qualityControlCount,
                        $issueOrdersCount,
                        $pausedCount,
                        $cancelCount,
                        $marriageCount,
                    ],
                ]
            ]
        ];

        return response()->json($data);
    }
}
