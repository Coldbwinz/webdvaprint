<?php

namespace App\Http\Controllers;

use App\AttachmentFile;
use App\ClientTypes;
use App\DeliveryTypes;
use App\Details;
use App\Events\Order\OrderWasClosedWithDiscount;
use App\Events\Order\OrderWasClosedWithRefund;
use App\Events\PaymentWasAdded;
use App\GroupOrders;
use App\OrderElement;
use App\PaymentTypes;
use App\PerformOrder;
use App\PerformOrderStatus;
use App\ProductType;
use App\Specification;
use App\SpecificationHistory;
use App\Subdomain;
use App\Template;
use App\TemplateDownload;
use App\Urgency;
use App\User;
use App\Contractor;
use App\ContractorOrderJob;
use App\ContractorToken;
use App\DifferentOrderStatus;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Classes\Parser\Frx;

use DB;
use Auth;
use File;
use Illuminate\Support\Facades\App;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Orders;
use App\Clients;
use App\Theme;
use App\TemplateLibrary;
use App\Order_logs;
use App\OrderStatuses;
use App\Emails\SendInvoiceNotification;
use App\Emails\ReadyOrderEmail;
use App\Emails\SendLayoutEmail;
use App\Emails\ContractorJobEmail;


class OrdersController extends Controller
{
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var Clients
     */
    private $clients;

    /**
     * @param Guard $auth
     * @param Clients $clients
     */
    public function __construct(Guard $auth, Clients $clients)
    {
        $this->auth = $auth;
        $this->clients = $clients;

        $this->middleware('settings.correct', [
            'only' => ['newStep1']
        ]);
    }

    /**
     * Выбор клиента
     *
     * @param null $filter
     * @return \Illuminate\Contracts\View\Factory|Redirect|\Illuminate\View\View
     */
    public function newOrder(Request $request)
    {
        $this->checkNotClosedGroup(1);

        if (count(Details::where('activity', 1)->first()) == 0) {
            session()->flash('message.warning', 'Необходимо заполнить реквизиты!');
            return redirect()->route('settings.index');
        }

        $authUser = $this->auth->user();

        if ($authUser->id_group === 1) {
            // определяем правильный ID
            $client_info = Clients::query()->where('email', $authUser->email)->first();
            if (count($client_info) > 0) {
                return redirect('/orders/new-step1/' . $client_info->id);
            } else {
                return redirect('/orders/new-step1/0');
            }
        }

        $query = $this->clients->select(\DB::raw('clients.*'))
            ->with(['manager', 'account', 'type', 'contacts', 'orders', 'ordersActive'])
            ->leftJoin('orders', 'clients.id', '=', 'orders.client_id')
            ->groupBy('clients.id')
            ->orderBy('orders.created_at', 'desc');

        // Не показывать партнёров
        $query->where('type_id', '!=', ClientTypes::PARTNER);

        // Не показывать посредников, которые были преобразованы в партнёров
        $query->whereNull('new_partner_id');

        if (!$this->auth->user()->is(['admin', 'partner', 'root'])) {
            $query->where('clients.manager_id', $authUser->id);
        }

        // filter clients by subdomain
        $subdomain = Subdomain::findCurrent();
        if ($subdomain) {
            $query->where('clients.subdomain_id', $subdomain->id);
        } else {
            $query->whereNull('clients.subdomain_id');
        }

        if ($request->has('search')) {
            $search = trim($request->input('search'));

            // @todo don't do this in production, use e.g. Algolia instead
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%")
                    ->orWhere('tm_name', 'like', "%{$search}%")
                    ->orWhereHas('contacts', function ($query) use ($search) {
                        $query->where(function ($query) use ($search) {
                            $query->where('contacts.email', 'like', "%{$search}%")
                                ->orWhere('contacts.phone', 'like', "%{$search}%")
                                ->orWhere('contacts.name', 'like', "%{$search}%")
                                ->orWhere('contacts.last_name', 'like', "%{$search}%");
                        });
                    })
                    ->orWhereHas('type', function ($query) use ($search) {
                        $query->where(function ($query) use ($search) {
                            $query->where('client_types.name', 'like', "%{$search}%");
                        });
                    });
            });
        }

        $clients = $query->paginate(20);

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => $breadcrumbs
        );

        if (\Request::ajax()) {
            return view('orders.new_order_client_table', compact('clients'))->render();
        } else {
            return view('orders.new', compact('page', 'user', 'clients'));
        }
    }

    /**
     * Step 1
     *
     * @param int $company_id
     * @return \Illuminate\Http\Response
     */

    public function newStep1($company_id)
    {
        $user = Auth::user();

        $client = Clients::findOrFail($company_id);

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => $breadcrumbs
        );

        return view('orders.step1', ['page' => $page,
            'page_title' => 'Меню заказа',
            'user' => $user,
            'company_id' => $company_id,
            'company_details' => $client->getDetailsOK(),
            'product_types' => ProductType::getWorkProducts(),
            'prices' => [],
            'selected_size' => '',
            'selected_type' => '',
            'selected_page' => '',
            'selected_product' => '',
            'selected_options' => [],
            'urgency_table' => Urgency::all(),
            'price_count' => 0,
            'price_price' => 0,
            'template_cost' => 0,
            'close_date' => '',
            'type_id' => 0,
            //'dashboardPanelInfo' => $user->accessLevel() == 1 ?
        ]);
    }

    public function postStep2(Request $request, $order_id = null)
    {
        if (isset($order_id)) {
            $order = Orders::find($order_id);
            $order_details = $order->price_details;
            $request->company_id = $order->client_id;
            $request->type_id = $order->type_product;
            $request->order_id = $order_id;
            $request->fill_calculation = json_encode($order_details['fill_calculation']);
            $selected_prepress = $order_details['selected_prepress'];
            $selected_chromacity = $order_details['selected_chromacity'];
            $fill_calculation = (object) $order_details['fill_calculation'];
            if ($selected_prepress == 1) {
                $request->theme_id = -1;
            } else if ($selected_prepress == 5) {
                $request->theme_id = 0;
            }
            $folds = (isset($order_details['folds'])) ? $order_details['folds'] : 0;
        } else {
            $fill_calculation = (object) json_decode($request->fill_calculation);
            $selected_prepress = $fill_calculation->selected_prepress;
            $selected_chromacity = $fill_calculation->selected_chromacity;
            $folds = $fill_calculation->folds;
        }

        if ($request->type_id == 999999) {
            $productType = ProductType::where('name', $fill_calculation->selected_product_type_name)->first();
            if (is_null($productType)) {
                $printler_type = $fill_calculation->printler_type;
                $productType = ProductType::where('printler_type', $printler_type)->first();
            }
        } else {
            $productType = ProductType::find($request->type_id);
        }
        if (is_null($productType)) {
            $product_type_id = 999999;
        } else {
            $product_type_id = $productType->id;
        }
        $width = $fill_calculation->width;
        $height = $fill_calculation->height;
        $sides = $fill_calculation->sides;
        $size = $width.'x'.$height;
        $two_side = $sides > 1;

        $filters = compact('size', 'sides', 'folds', 'two_side', 'product_type_id');

        // Все тематики у которых есть шаблоны
        $themes = Theme::active()
            ->has('templates')
            ->with(['templates' => function ($query) use ($filters) {
                $query->active()->filter($filters);
            }])
            ->get();

        $client = Clients::findOrFail($request->company_id);

        $queryMy = Template::forClient($client)->filter($filters);
        $templatesQuery = TemplateLibrary::active()->filter($filters);

        $count_my = $queryMy->count();
        $count_all = $count_my + $templatesQuery->count();
        // @todo эту логику можно упростить!
        if (is_null($request->theme_id) || (is_numeric($request->theme_id) && $request->theme_id < 0)) {
            $category_title = 'Все';
            $myTemplates = $queryMy->get();
            $libTemplates = $templatesQuery->get();
            $templates = $myTemplates->merge($libTemplates);
        } elseif ($request->theme_id == 0) {
            $category_title = 'Клиентские';
            $templates = $queryMy->get();
        } elseif ($request->theme_id > 0) {
            $theme = Theme::find($request->theme_id);
            if (!$theme) {
                abort(404, 'Тематика не найдена');
            }

            $templatesQuery->whereHas('themes', function ($query) use ($theme) {
                $query->where('id', $theme->id);
            });

            $category_title = $theme->name;
            $templates = $templatesQuery->get();
        }

        $page = [
            'menu' => 'order',
            'submenu' => 'new',
            'breadcrumbs' => ['Заказы', 'Новый заказ']
        ];
        $data = [
            'page' => $page,
            'category_title' => $category_title,
            'order_id' => $request->order_id,
            'company_id' => $request->company_id,
            'type_id' => $request->type_id,
            'product_type' => ProductType::find($request->type_id),
            'themes' => $themes,
            'theme_id' => $request->theme_id,
            'templates' => $templates,
            'count_all' => $count_all,
            'count_my' => $count_my,
            'mini_menu' => true,
            'fill_calculation' => $request->fill_calculation,
            'selected_prepress' => $selected_prepress,
            'selected_chromacity' => $selected_chromacity,
            'company_details' => $client->getDetailsOK(),
        ];

        return view('orders.step2', $data);
    }

    public function postStep3(Request $request)
    {
        $client = Clients::findOrFail($request->input('company_id'));
        $contactId = $request->input('contact_id', $client->contacts->first()->id);
        $contact = $client->contacts()->findOrFail($contactId);
        $user = Auth::user();
        if (!$request->sendInvoice) {
            $name = uniqid(); // новое имя для папки
            $pdf = null;
            if ($request->lib == 'lib') {
                $template = TemplateLibrary::find($request->template_id);
                if (!$template) {
                    abort(404, 'Шаблон не найден');
                }

                $filename = $template->w2p;
                $template_type = 'template_library';

                $filename = $this->check_file($filename);

                if (file_exists($filename)) {
                    $src = pathinfo($filename)['dirname'];
                    $dst = public_path('lib/frx/' . $name);

                    recursive_copy_flatten($src, $dst);

                    $file_new = public_path("lib/frx/" . $name . ".frx");
                    Frx::copyFrx($filename, $file_new);
                } else {
                    throw new \Exception('File not found: ' . $filename);
                }

            } else {
                $template = Template::findOrFail($request->template_id);
                $template_type = 'templates';

                if ($template->w2p) {
                    $templateFile = public_path('lib/frx/' . $template->w2p);

                    if (!file_exists($templateFile)) {
                        throw new Exception('Template not found: ' . $templateFile);
                    }

                    if (!$template->editable) {
                        $name = null;
                        $pdf = $template->w2p;
                    }

                    $pathinfo = pathinfo($templateFile);

                    $newTemplateFile = $pathinfo['dirname'] . '/' . $name . '.' . $pathinfo['extension'];

                    copy($templateFile, $newTemplateFile);

                    $templateDir = $pathinfo['dirname'] . '/' . $pathinfo['filename'];

                    if (is_dir($templateDir)) {
                        $newDir = $pathinfo['dirname'] . '/' . $name;
                        rcopy($templateDir, $newDir);
                        Frx::copyFrx($templateFile, $newTemplateFile);
                    }
                } else {
                    $name = null;
                }
            }
        }

        // Создать заказ в базу данных
        if ($request->order_id) {
            $order = Orders::find($request->order_id);
            $group_order = $order->group;
        } else {
            $group_order = GroupOrders::query()->where('next', '=', 1)->where('manager_id', '=', Auth::user()->id)->first();
            if (count($group_order) == 0) {
                $group_order = new GroupOrders;
                $group_order->client_id = $client->id;
                $group_order->detail_id = Details::where('subdomain_id', '=', Clients::find($request->company_id)->subdomain_id)->where('activity', 1)->first()->id;
                $group_order->status = 500;
                $group_order->manager_id = Auth::user()->id;
                $group_order->next = 1;
                $group_order->contact_id = $contact->id;
                $group_order->save();
            }
            $order = new Orders;
            $order->group_id = $group_order->id;

            $order->client_id = $request->company_id;
            $order->manager_id = Auth::user()->id;
            if (Auth::user()->accessLevel() == 1) {
                $order->creator = 1; //Создал пользователь
            } else {
                $order->creator = 2; //Создал менеджер
            }
            $order->type_product = $request->type_id;
            if ($request->sendInvoice == 3) {
                $order->creator = 4; //На обработку принтлера
                $order->status = 10;
            } else if ($request->sendInvoice == 4) {
                $order->creator = 5; //Заказ дизайнеру
                $order->status = 300;
            } else if ($request->sendInvoice == 5) {
                $order->creator = 6; //Заказ дизайнера
                $order->status = 301;
            } else {
                if (auth()->user()->accessLevel() == 1) {
                    $order->status = 2000;
                } else {
                    $order->status = 2010;
                }
            }
            $order->save();
        }


        if ($request->sendInvoice == 0) {
            $order->file = $name ? $name . '.frx' : null;
            $order->pdf = $pdf;

            $preview = basename($template->url);
            if (filter_var($template->url, FILTER_VALIDATE_URL)) {
                $preview = str_random() . '.png';
                file_put_contents(public_path('lib/frx/' . $preview), fopen($template->url, 'r'));
            }

            $order->img = $preview;
            if ($template_type == 'template_library') {
                $order->template_library_id = $template->id;
            } else {
                $order->template_id = $template->id;
            }
        }

        $fillCalculation = json_decode($request->fill_calculation);
        $complects_count = (isset($fillCalculation->complects_count)) ? $fillCalculation->complects_count : 1;
        $fillCalculation->complects_count = 1;
        $this->saveCalculation($order, $request->fill_calculation);

        if (!$request->order_id) {
            $this->createOrderElements($order, $request->fill_calculation);
            addLog($order->id, 'Черновик', auth()->user()->id, '', null, 0, auth()->user()->isClient() ? 'в личном кабинете' : 'в CRM');
            $status = 'Разработка макета';
            $comment = '';
            $value = '';
            if ($order->template_library_id) {
                $comment = 'по шаблону';
            } else if ($order->template_id) {
                $comment = 'взят из архива заказчика';
                if ($order->clientTemplate->editable) {
                    $value = 'редактируемый';
                } else {
                    $value = 'нередактируемый';
                }
                $status = 'Готовый макет';
            }
            if (in_array($order->creator, [1, 2])) {
                addLog($order->id, $status, auth()->user()->id, $value, null, 0, $comment);
            } else if ($order->creator == 4) {
                addLog($order->id, 'Готовый макет', auth()->user()->id, 'загружен на проверку', null, 0, 'предоставлен заказчиком');
            } else if ($order->creator == 5) {
                $responsible = auth()->user()->isClient() ? auth()->user()->id : $order->group->client->user_id;
            }

            $order->complect_id = $order->id;
            $order->save();

            $this->createComplects($order, $request->fill_calculation, $complects_count, $fillCalculation->start);
        }

        (new GroupOrdersController)->recalculateGroupPrice($group_order->id);

        // addLog(0, 507, auth()->user()->id, "{$order->price}руб.", $group_order->id, 0, "{$order->productType->name} {$order->draw}шт.");
        if (!$request->sendInvoice) {

            // Если шаблон открывается из архива клиента
            if ($fillCalculation->selected_prepress == 5 || $fillCalculation->selected_prepress == 1) {

                if ($request->input('action') == 'agree_in_place') {
                    return redirect('/order/reviwer/1/' . $order->id . '?editable');
                }

                if ($request->input('action') == 'btn_invoice') {
                    $this->set_status_id($order->id, 5);
                    return redirect('/order/close_group_order/1');
                }

                if ($request->input('action') == 'btn_next') {
                    $this->set_status_id($order->id, 5);
                    return redirect('/orders/new-step1/1');
                }

                if ($fillCalculation->selected_prepress == 5) {
                    // Не открывает редактор
                    if ($order->clientTamplte && $order->clientTemplate->editable) {
                        // Шаблон редактируемый
                        // На страницу утверждения
                        return redirect('/order/reviwer/1/' . $order->id);
                    } else {
                        // Шаблон не редактируемый
                        // На страницу утверждения
                        return redirect('/order/reviwer/1/' . $order->id . '?editable');
                    }
                }
            }

            if ($fillCalculation->selected_prepress == 1 && $order->clientTemplate) {
                // Если шаблон открывается из библиотеки
                if (!$order->clientTemplate->editable) { // Шаблон не редактируемый

                    if (request('action') === "print_as_is") { // "Печатать как есть"
                        return redirect('/order/reviwer/1/' . $order->id . '?editable');
                    } elseif (request('action') === 'make_template') { // Заказать создание шаблона
                        Specification::makeFromOrder($order);

                        return redirect('/home');
                    }
                }
            }
        }


        if ($order->clientTemplate && !$order->clientTemplate->editable) {
            if ($fillCalculation->selected_prepress == 1 && !$request->sendInvoice && $request->lib == 'nolib') {
                return redirect('/order/reviwer/1/' . $order->id . '?editable');
            }
        }
        if ($request->sendInvoice == 0) {
            return redirect('/orders/edit/' . $order->id);
        } else if ($request->sendInvoice == 1) {
            $this->sendOnlyInvoice($order->id);
            return redirect('/orders/history/');
        } else if ($request->sendInvoice == 2) {
            return redirect('/loading/' . random_int(1000, 9999) . $order->id . random_int(1000, 9999));
        } else if ($request->sendInvoice == 3) {
            return '/loading/' . random_int(1000, 9999) . $order->id . random_int(1000, 9999);
        } else if ($request->sendInvoice == 4) {
            return '/order/' . $order->id . '/specification/create';
        } else if ($request->sendInvoice == 5) {
            return '/orders/history/';
        } else if ($request->sendInvoice == 6) {
            return '/orders/history/';
        }
    }

    public function createComplects($order, $fill_calculation, $count, $start)
    {
        if ($count > 1) {
            $new_position = $order->position;
            $order = Orders::find($order->id);
            $order_logs = $order->logs;
            while ($count > 1) {
                $orderCopy = $order->replicate();
                $orderCopy->position = ++$new_position;
                $new_name = uniqid();
                $old_name = pathinfo($order->file)['filename'];
                if ($order->file) {
                    $orderCopy->file = $new_name . '.frx';
                }
                if ($order->img) {
                    $orderCopy->img = $new_name . '.frx.jpg';
                }
                if ($order->pdf) {
                    $orderCopy->pdf = $new_name . '.frx.pdf';
                }
                $libpath = $_SERVER['DOCUMENT_ROOT'] . '/lib/frx/';
                mkdir($libpath . $new_name, 0777, true);
                $files = glob($libpath . $old_name . '/*.*');
                foreach ($files as $file) {
                    $file_to_go = str_replace($old_name, $new_name, $file);
                    copy($file, $file_to_go);
                }
                $orderCopy->save();

                $this->createOrderElements($orderCopy, $fill_calculation);

                foreach ($order_logs as $log) {
                    addLog($orderCopy->id, ($log->status_free) ? $log->status_free : $log->status, $log->user_id, $log->value, null, 0, $log->comments);
                }
                $count--;

                // Цифру 100 не менять!!! Код работает только с этой цифрой!!
                usleep(100);
            }
        }
    }

    /**
     * Create elements for order
     *
     * @param $order
     * @param $fillCalculation
     */
    public function createOrderElements($order, $fillCalculation)
    {
        $fillCalculation = json_decode($fillCalculation);

        // Создание элементов для заказа
        switch ($fillCalculation->printler_type) {
            case 1: // листовое
            case 2: // или со сгибом
                $name = $order->productType
                    ? $order->productType->name
                    : $fillCalculation->selected_product_type_name;

                $order->elements()->create([
                    'name' => $name,
                    'type' => 'body',
                    'chromacity' => $fillCalculation->selected_chromacity,
                    'format' => $fillCalculation->selected_size,
                    'draw' => $order->draw,
                    'material' => $fillCalculation->selected_product,
                    'postpress' => $fillCalculation->selected_options,
                    'status_id' => status('in_work', 'id'),
                    'preview' => '/lib/frx/' . basename($order->img),
                    'printable' => $fillCalculation->selected_chromacity !== 'без печати',
                ]);
                break;

            case 3: // многостраничное

                $issueAt = Carbon::createFromTimestamp($order->close_date);

                // Внутренний блок
                $innerElement = $order->elements()->create([
                    'name' => 'Внутренний блок',
                    'type' => 'body',
                    'chromacity' => $fillCalculation->selected_chromacity,
                    'format' => $fillCalculation->selected_size,
                    'draw' => $order->draw,
                    'material' => $fillCalculation->selected_material,
                    'postpress' => $fillCalculation->selected_options,
                    'pages_number' => $fillCalculation->selected_product,
                    'status_id' => status('in_work', 'id'),
                    'preview' => '/lib/frx/' . basename($order->img),
                    'printable' => $fillCalculation->selected_chromacity !== 'без печати',
                ]);

                // Если элемент без печати, то сразу создаётся наряд в постпресс
                if ($fillCalculation->selected_chromacity === 'без печати') {
                    $performOrder = PerformOrder::create([
                        'product' => $innerElement->name,
                        'chromacity' => $innerElement->chromacity,
                        'print_sheet_format' => $innerElement->format,
                        'draw' => $innerElement->draw,
                        'material' => $innerElement->material,
                        'postpress_processes' => $innerElement->postpress,
                        'issue_at' => $issueAt,
                        'perform_order_status_id' => 4,
                        'preview' => '/lib/frx/' . $innerElement->order->img,
                        'single' => true,
                    ]);
                    $innerElement->performOrder()->associate($performOrder);
                    $innerElement->save();
                }

                // Обложка
                if (isset($order->price_details['selected_cover'])) {
                    $coverElement = $order->elements()->create([
                        'name' => 'Обложка',
                        'type' => 'cover',
                        'chromacity' => $fillCalculation->selected_cover_chromacity,
                        'format' => $fillCalculation->selected_size,
                        'draw' => $order->draw,
                        'material' => $fillCalculation->selected_cover,
                        'postpress' => $fillCalculation->selected_options,
                        'status_id' => status('in_work', 'id'),
                        'preview' => '/lib/frx/' . basename($order->img),
                        'printable' => $fillCalculation->selected_cover_chromacity !== 'без печати',
                    ]);

                    // Если элемент без печати, то сразу создаётся наряд в постпресс
                    if ($fillCalculation->selected_cover_chromacity === 'без печати') {
                        $performOrder = PerformOrder::create([
                            'product' => $coverElement->name,
                            'chromacity' => $coverElement->selected_cover_chromacity,
                            'print_sheet_format' => $coverElement->format,
                            'draw' => $coverElement->draw,
                            'material' => $coverElement->material,
                            'postpress_processes' => $coverElement->postpress,
                            'issue_at' => $issueAt,
                            'perform_order_status_id' => 4,
                            'preview' => '/lib/frx/' . $coverElement->order->img,
                            'single' => true,
                        ]);
                        $coverElement->performOrder()->associate($performOrder);
                        $coverElement->save();
                    }
                }

                // Подложка
                if (isset($fillCalculation->selected_pad)) {
                    $padElement = $order->elements()->create([
                        'name' => 'Подложка',
                        'type' => 'pad',
                        'chromacity' => $fillCalculation->selected_pad_chromacity,
                        'format' => $fillCalculation->selected_size,
                        'draw' => $order->draw,
                        'material' => $fillCalculation->selected_pad,
                        'postpress' => $fillCalculation->selected_options,
                        'status_id' => status('in_work', 'id'),
                        'preview' => '/lib/frx/' . basename($order->img),
                        'printable' => $fillCalculation->selected_pad_chromacity !== 'без печати',
                    ]);

                    // Если подложка без печати, то сразу создаётся наряд в постпресс
                    if ($fillCalculation->selected_pad_chromacity === 'без печати') {
                        $performOrder = PerformOrder::create([
                            'product' => $padElement->name,
                            'chromacity' => $padElement->chromacity,
                            'print_sheet_format' => $padElement->format,
                            'draw' => $padElement->draw,
                            'material' => $padElement->material,
                            'postpress_processes' => $padElement->postpress,
                            'issue_at' => $issueAt,
                            'perform_order_status_id' => 4,
                            'preview' => '/lib/frx/' . $padElement->order->img,
                            'single' => true,
                        ]);
                        $padElement->performOrder()->associate($performOrder);
                        $padElement->save();
                    }
                }
                break;
        }
    }

    /*
     * TODO: Небольшое преообразование для поиска по подобному пути в public на windows (xampp)
     * для работы на windows из public/var и т.п.
     * */
    function check_file($path)
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $path)) {
            return $_SERVER['DOCUMENT_ROOT'] . $path;
        }
        return $path;
    }

    public function sendOnlyInvoice($order_id)
    {
        $order = Orders::find($order_id);
        $serviceInfo = $this->getPriceTableForProduct($order->type_product, $order->client_id);

        $history = Orders::find($order_id)->get();

        $history_orders = $this->getOrdersCalculationData($history, $order_id);
        $pdf_link = substr($this->getOrderPDF(1, $order_id, $serviceInfo, $history_orders, "/"), 1);

        $time = time() . "";
        $leftTime = substr($time, 0, 4);
        $rightTime = substr($time, 4, 4);

        $client_info = Clients::find($order->client_id);

        $company_name = settings('email_title') ? settings('email_title') : settings('company_name');
        $data = array(
            'subject' => 'Расчёт на согласование от ' . $company_name,
            'to' => $client_info->email,
            'email' => Auth::user()->email,
            'name' => $client_info->name,
            'url' => $leftTime . $order_id . $rightTime,
            'pdf_link' => $pdf_link,
            'order_id' => $order->id,
            'manager' => $this->getManager($order->manager_id),
            'client' => $client_info,
        );

        \Mail::send('emails.only_invoice', $data, function ($message) use ($data) {
            try {
                $message->attach($data['pdf_link'], array('as' => 'invoice_' . $data['order_id'] . '.pdf', 'mime' => 'application/pdf'));

                $message->subject($data['subject'])
                    ->to($data['to'])
                    ->from($data['email'], $_SERVER["SERVER_NAME"] . ' - Ваш расчёт');
            } catch (Exception $e) {
            }
        });

        if (count(\Mail::failures()) == 0) {
            $order->template_send = 1;
            $order->invoice_send = 1;
            $order->save();
        }
    }

    public function saveCalculation($order, $fill_calculation = null)
    {
        if ($fill_calculation != null) {
            $what_changed = '';
            if (is_array($fill_calculation)) {
                $fill_calculation = json_encode($fill_calculation);
            }
            if (!is_object($fill_calculation)) {
                $fill_calculation = json_decode($fill_calculation, false);
            }
            $price_details = json_decode('{}');
            if ($fill_calculation->price_index == 'manual') {
                $price_details->selected_product_type_name = $fill_calculation->selected_product_type_name;
                $price_details->type_product = (new ProductType)->createAuto($fill_calculation);
            }
            if ((isset($order->price_details['selected_product'])) && ($order->price_details['selected_product'] != $fill_calculation->selected_product)) {
                if ($fill_calculation->printler_type == 3) {
                    $what_changed = 'кол-во страниц ';
                } else {
                    $what_changed = 'материал ';
                }
                $what_changed .= "($fill_calculation->selected_product -> " . $order->price_details['selected_product'] . "), ";
            }
            $price_details->selected_product = $fill_calculation->selected_product;

            if ((isset($order->price_details['selected_options'])) && ($order->price_details['selected_options'] != $fill_calculation->selected_options)) {
                $what_changed .= "опции (" . implode(',', $order->price_details['selected_options']) . " -> " . implode(',', $fill_calculation->selected_options) . "), ";
            }

            $price_details->selected_options = $fill_calculation->selected_options;

            if ((isset($order->price_details['selected_size'])) && ($order->price_details['selected_size'] != $fill_calculation->selected_size)) {
                $what_changed .= "размер (" . $order->price_details['selected_size'] . " -> $fill_calculation->selected_size.";
            }
            $price_details->selected_size = $fill_calculation->selected_size;

            if ((isset($order->price_details['selected_type'])) && ($order->price_details['selected_type'] != $fill_calculation->selected_type)) {
                $what_changed .= "тип (" . $order->price_details['selected_type'] . ". -> $fill_calculation->selected_type), ";
            }
            $price_details->selected_type = $fill_calculation->selected_type;

            if ((isset($order->price_details['selected_chromacity'])) && ($order->price_details['selected_chromacity'] != $fill_calculation->selected_chromacity)) {
                $what_changed .= "цветность (" . $order->price_details['selected_chromacity'] . " -> $fill_calculation->selected_chromacity), ";
            }
            $price_details->selected_chromacity = $fill_calculation->selected_chromacity;

            if ((isset($order->price_details['selected_material'])) && ($order->price_details['selected_material'] != $fill_calculation->selected_material)) {
                $what_changed .= "материал (" . $order->price_details['selected_material'] . " -> $fill_calculation->selected_material), ";
            }
            $price_details->selected_material = $fill_calculation->selected_material;

            if (isset($fill_calculation->printler_type)) {
                $price_details->printler_type = $fill_calculation->printler_type;
            }
            $price_details->print_structure = $fill_calculation->print_structure;
            $price_details->price_index = $fill_calculation->price_index;
            if (isset($fill_calculation->selected_cover)) {
                if ((isset($order->price_details['selected_cover'])) && ($order->price_details['selected_cover'] != $fill_calculation->selected_cover)) {
                    $what_changed .= "обложка (" . $order->price_details['selected_cover'] . " -> $fill_calculation->selected_cover), ";
                }
                $price_details->selected_cover = $fill_calculation->selected_cover;
                $price_details->selected_cover_chromacity = $fill_calculation->selected_cover_chromacity;
            }
            if (isset($fill_calculation->selected_pad)) {
                if ((isset($order->price_details['selected_pad'])) && ($order->price_details['selected_pad'] != $fill_calculation->selected_pad)) {
                    $what_changed .= "подложка (" . $order->price_details['selected_pad'] . " -> $fill_calculation->selected_pad), ";
                }
                $price_details->selected_pad = $fill_calculation->selected_pad;
                $price_details->selected_pad_chromacity = $fill_calculation->selected_pad_chromacity;
            }

            $price_details->price = $fill_calculation->price;
            if (isset($fill_calculation->price_change_reason)) {
                $price_details->price_change_reason = $fill_calculation->price_change_reason;
            }
            $order->price = $fill_calculation->price + $fill_calculation->template_cost;
            $order->price_partner = $this->savePartnerPrice($order, $order->price);

            if ((isset($order->price_details['selected_urgency'])) && ($order->price_details['selected_urgency'] != $fill_calculation->selected_urgency)) {
                $what_changed .= "срочность (" . $order->price_details['selected_urgency'] . " -> $fill_calculation->selected_urgency), ";
            }
            $price_details->selected_urgency = $fill_calculation->selected_urgency;

            if ((isset($order->price_details['close_date'])) && ($order->price_details['close_date'] != $fill_calculation->close_date)) {
                $what_changed .= "дата закрытия (" . $order->price_details['close_date'] . " -> $fill_calculation->close_date), ";
            }
            $price_details->close_date = $fill_calculation->close_date;

            $price_details->template_cost = $fill_calculation->template_cost;
            $price_details->selected_prepress = $fill_calculation->selected_prepress;
            $price_details->fill_calculation = $fill_calculation;

            $price_details->selected_delivery_type = $fill_calculation->selected_delivery_type;
            if (isset($fill_calculation->template_cost_hour)) {
                $price_details->template_cost_hour = $fill_calculation->template_cost_hour;
            }
            if (isset($fill_calculation->complects_calculation_type)) {
                $price_details->complects_calculation_type = $fill_calculation->complects_calculation_type;
            }
            if (isset($fill_calculation->complects_discount_percent)) {
                $price_details->complects_discount_percent = $fill_calculation->complects_discount_percent;
            }
            $price_details->selected_address = $fill_calculation->selected_address;
            $price_details->selected_payment_type = $fill_calculation->selected_payment_type;
            $price_details->sides = $fill_calculation->sides;
            $price_details->prepress_in_one_line = $fill_calculation->prepress_in_one_line;
            if (($order->draw > 0) && ($order->draw != $fill_calculation->draw)) {
                $what_changed .= "тираж ($order->draw -> " . $fill_calculation->draw . "), ";
            }
            if (strlen($what_changed) > 0) {
                $what_changed = 'Изменения: ' . substr($what_changed, 0, strlen($what_changed) - 2) . '.';
            }
            $price_details->what_changed = $what_changed;
            $order->price_details = $price_details;
            $order->postpress_operations = $price_details->selected_options;
            $order->draw = $fill_calculation->draw;
            $order->close_date = strtotime($price_details->close_date . " 00:00:00");
            $order->selected_issue_address = $price_details->selected_address;
        }
        $order->save();
    }

    public function savePartnerPrice($order, $price)
    {
        $client_discount = $order->group->client->type->discount;
        $price = round($price / (100 - $client_discount) * 100, 2);
        $partner_discount = 0;
        if ($order->group()->first()->partnerAsClient()) {
            $partner_discount = ClientTypes::getPartnerDiscount();
            $price = round($price * ((100 - $partner_discount) / 100), 2);
        }
        $price = round($price * ((100 - $partner_discount) / 100), 2);
        return $price;
    }

    public function getPriceInfo($type_id, $fill_calculation = null, $client_id = null)
    {
        $data2 = [];
        if ($type_id != 999999) {
            $prices = $this->getPriceTableForProduct($type_id, $client_id);
        } else {
            $prices = null;
        }
        $selected_options = [];
        if ($fill_calculation != null) {
            if (is_array($fill_calculation)) {
                $fill_calculation = json_encode($fill_calculation);
            }
            if (!is_object($fill_calculation)) {
                $fill_calculation = json_decode($fill_calculation, false);
            }
            if (isset($fill_calculation->selected_material)) {
                $selected_material = $fill_calculation->selected_material;
            } else {
                $selected_material = '###';
            }
            $selected_cover = null;
            if (isset($fill_calculation->selected_cover)) {
                $selected_cover = $fill_calculation->selected_cover;
            }
            $selected_pad = null;
            if (isset($fill_calculation->selected_pad)) {
                $selected_pad = $fill_calculation->selected_pad;
            }
            foreach ($fill_calculation->selected_options as $option) {
                array_push($selected_options, $option);
            };
            if ($type_id == 999999) {
                if (!isset($fill_calculation->price)) {
                    $data2 = $this->getManualData();
                }
            }
            if (!isset($fill_calculation->template_cost)) {
                $fill_calculation->template_cost = 0;
            }
            if (!isset($fill_calculation->price_change_reason)) {
                $fill_calculation->price_change_reason = '';
            }
        }
        $data1 = [
            'selected_printler_type' => $fill_calculation->printler_type,
            'selected_size' => $fill_calculation->selected_size,
            'selected_type' => $fill_calculation->selected_type,
            'selected_chromacity' => $fill_calculation->selected_chromacity,
            'selected_material' => $selected_material,
            'selected_cover' => $selected_cover,
            'selected_cover_chromacity' => (isset($fill_calculation->selected_cover_chromacity)) ? $fill_calculation->selected_cover_chromacity : '',
            'selected_pad' => $selected_pad,
            'selected_pad_chromacity' => (isset($fill_calculation->selected_pad_chromacity)) ? $fill_calculation->selected_pad_chromacity : '',
            'selected_product' => $fill_calculation->selected_product,
            'selected_options' => $selected_options,
            'selected_urgency' => $fill_calculation->selected_urgency,
            'delivery_types' => DeliveryTypes::all(),
            'client_addresses' => Clients::find($client_id)->contacts()->first()->clients()->get()->pluck('address'),
            'payment_types' => PaymentTypes::get(['id','key_1s','name','discount']),
            'prepress_in_one_line' => $fill_calculation->prepress_in_one_line,
            'price_index' => $fill_calculation->price_index,
            'draw' => $fill_calculation->draw,
            'price' => $fill_calculation->price,
            'selected_prepress' => $fill_calculation->selected_prepress,
            'selected_delivery_type' => json_encode($fill_calculation->selected_delivery_type),
            'selected_payment_type' => json_encode($fill_calculation->selected_payment_type),
            'complects_calculation_type' => (isset($fill_calculation->complects_calculation_type)) ? $fill_calculation->complects_calculation_type : '',
            'complects_discount_percent' => (isset($fill_calculation->complects_discount_percent)) ? $fill_calculation->complects_discount_percent : '',
            'selected_address' => $fill_calculation->selected_address,
            'template_cost' => $fill_calculation->template_cost,
            'close_date' => $fill_calculation->close_date,
            'price_change_reason' => $fill_calculation->price_change_reason];
        return array_merge(['prices' => $prices], $data1, $data2);
    }


    /*
     * Редактор менеджера
     */
    public function editorOpen($order_id, $replicate = 0)
    {
        $user = Auth::user();

        $order = Orders::find($order_id);

        if (!$order) {
            return redirect('/home');
        }

        $order->template_edited = true;
        $order->save();

        $details = $order->price_details;
        $selectedSize = explode('x', $details['selected_size']);

        if ($order->template) {
            $templateWidth = $order->template->width;
            $templateHeight = $order->template->height;
        } elseif ($order->clientTemplate) {
            $templateWidth = $order->clientTemplate->width;
            $templateHeight = $order->clientTemplate->height;
        } else {
            throw new Exception('Unknown template');
        }

        $width = $selectedSize[0];
        $height = $selectedSize[1];

        $colors = [
            '4 + 0 (односторон.)' => 1,
            '4 + 4 (двухсторон.)' => 1,
            '4 + 1 (двухсторон.)' => 1,
            '1 + 0 (односторон.)' => 0,
        ];

        $workspaceColors = isset($colors[$details['selected_chromacity']]) ? $colors[$details['selected_chromacity']] : 1;

        if (!is_null($order->template_id)) {
            $template = Template::find($order->template_id);
        } elseif (!is_null($order->template_library_id)) {
            $template = TemplateLibrary::find($order->template_library_id);
        }

        $uuid = str_replace('.frx', '', $order->file);
        // если копируем заказ в новый
        if ($replicate == 1) {
            $newid = uniqid();
            $path = public_path('/lib/frx/');
            rcopy($path . $uuid, $path . $newid);
            copy($path . $uuid . '.frx', $path . $newid . '.frx');
            copy($path . $uuid . '.frx.jpg', $path . $newid . '.frx.jpg');
            copy($path . $uuid . '.frx.pdf', $path . $newid . '.frx.pdf');
            copy($path . $uuid . '.frx.zip', $path . $newid . '.frx.zip');

            if (file_exists($path . $newid . '.frx')) {
                $order->file = $newid . '.frx';
            }
            if (file_exists($path . $newid . '.frx.jpg')) {
                $order->img = $newid . '.frx.jpg';
            }
            if (file_exists($path . $newid . '.frx.pdf')) {
                $order->pdf = $newid . '.frx.pdf';
            }
            $uuid = $newid;
            $order->save();
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );

        (($order->status == 2000) || ($order->status == 2010)) ? $new_invoice = 1 : $new_invoice = 0;

        if ($user->isClient()) {
            return view('orders.step3_user', [
                'page' => $page,
                'user' => $user,
                'uuid' => $uuid,
                'template' => $template,
                'mini_menu' => true,
                'company_id' => $order->client_id,
                'company_details' => $order->group->client()->first()->getDetailsOK(),
                'order_id' => $order->id,
                'new_invoice' => $new_invoice,
                'status_last' => $order->status_last,
                'width' => $width,
                'height' => $height,
                'templateWidth' => $templateWidth,
                'templateHeight' => $templateHeight,
                'workspaceColors' => $workspaceColors,
            ]);
        }

        return view('orders.step3', [
            'page' => $page,
            'user' => $user,
            'uuid' => $uuid,
            'template' => $template,
            'mini_menu' => true,
            'company_id' => $order->client_id,
            'company_details' => $order->group->client()->first()->getDetailsOK(),
            'order_id' => $order->id,
            'status' => $order->status,
            'new_invoice' => $new_invoice,
            'status_last' => $order->status_last,
            'width' => $width,
            'height' => $height,
            'templateWidth' => $templateWidth,
            'templateHeight' => $templateHeight,
            'workspaceColors' => $workspaceColors,
        ]);
    }

    /*
     * Редактор пользоватлея
     */

    public function good($filename)
    {
        $order = Orders::query()->where('file', $filename)->first();
        if ($order->status == 1000) {
            if ($order->status_last < 25) {
                $order->stopped(0, 'Макет изменён');
                $order->status = 102;
            } else {
                if ($order->stopped_user_id == auth()->user()->id) {
                    $order->stopped(0, 'Макет изменён');
                } else {
                    $order->setStopReson(0, 'Макет изменён');
                    addLog($order->id, 'Макет изменён', auth()->user()->id, '', null, 0, '');
                }
            }
            $order->img = $filename . '.jpg';
            $order->pdf = $filename . '.pdf';
            $order->workWithTemplate(0);
            $order->template_changed = true;
        } else {
            if ($order->status != 2010) {
                addLog($order->id, 'Макет изменён', auth()->user()->id, '', null, 0, 'внесены правки');
            }
            $order->img = $filename . '.jpg';
            $order->pdf = $filename . '.pdf';
        }
        $order->save();

        // Для всех элементов заказа добавить сгенерированную PDF
        $order->elements->each(function (OrderElement $element) use ($filename) {
            $element->update([
                'preview' => '/lib/frx/' . $filename . '.jpg',
                'pdf_path' => '/lib/frx/' . $filename . '.pdf'
            ]);
        });
    }

    /**
     * Повторить заказ
     */
    public function reorder($id)
    {
        $user = Auth::user();
        $order = Orders::find($id);
        $group = $order->group()->first();
        if (!$order || !$group) {
            return redirect('/home');
        }

        // duplicate group_orders
        $groupCopy = new GroupOrders;
        $groupCopy->replicated = true;
        $groupCopy->manager_id = $group->manager_id;
        $groupCopy->client_id = $group->client_id;
        $groupCopy->detail_id = $group->detail_id;
        $groupCopy->subdomain_id = $group->subdomain_id;
        $groupCopy->contact_id = $group->contact_id;
        $groupCopy->next = 0;
        $groupCopy->status = 500;
        $groupCopy->source_id = $group->id;
        $groupCopy->save();

        // duplicate orders
        $orderCopy = new Orders;
        $orderCopy->replicated = true;
        $orderCopy->group_id = $groupCopy->id;
        $orderCopy->position = 1;
        $orderCopy->manager_id = $order->manager_id;
        $orderCopy->client_id = $order->client_id;
        $orderCopy->detail_id = $order->detail_id;
        $orderCopy->creator = 1;
        $orderCopy->template_id = $order->template_id;
        $orderCopy->template_library_id = $order->template_library_id;
        $orderCopy->template_download_id = $order->template_download_id;
        $orderCopy->type_product = $order->type_product;
        $orderCopy->status = 2000;

        $copyPriceDetails = $order->price_details;
        $copyPriceDetails['temp_template_cost'] = $order->price_details['template_cost'];
        $copyPriceDetails['price'] = 0;
        $copyPriceDetails['close_date'] = '';
        $copyPriceDetails['selected_urgency'] = '';
        $orderCopy->price_details = $copyPriceDetails;

        $new_name = uniqid();
        $old_name = pathinfo($order->file)['filename'];
        $orderCopy->file = $new_name . '.frx';
        $orderCopy->img = $new_name . '.frx.jpg';
        $orderCopy->pdf = $new_name . '.frx.pdf';
        $libpath = $_SERVER['DOCUMENT_ROOT'] . '/lib/frx/';
        mkdir($libpath . $new_name, 0777, true);
        $files = glob($libpath . $old_name . '/*.*');
        foreach ($files as $file) {
            $file_to_go = str_replace($old_name, $new_name, $file);
            copy($file, $file_to_go);
        }

        copy($libpath . $order->file, $libpath . $orderCopy->file);
        copy($libpath . $order->img, $libpath . $orderCopy->img);
        copy($libpath . $order->pdf, $libpath . $orderCopy->pdf);
        copy($libpath . $order->file . ".zip", $libpath . $orderCopy->file . '.zip');
        $orderCopy->save();
        $order->type_name = $order->productTypeName();

        addLog($orderCopy->id, 2020, auth()->user()->id, 'В личном кабинете', null, 0, 'Оформлен клиентом самостоятельно');
        addLog(0, 2020, auth()->user()->id, 'В личном кабинете', $groupCopy->id, 0, 'Оформлен клиентом самостоятельно');

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Повторный заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'page_title' => $order->productTypeName(),
            'user' => $user,
            'order' => $orderCopy,
            'company_details' => $order->group->client()->first()->getDetailsOK(),
            'company_id' => $orderCopy->client_id,
            'type_id' => $orderCopy->type_product,
            'new_invoice' => 1,
            'reviwer' => '1',
        ];
        $fill_calculation = array_merge($orderCopy->price_details, ['draw' => $orderCopy->draw, 'price' => $orderCopy->price]);
        $data = array_merge($data, $this->getPriceInfo($orderCopy->type_product, $fill_calculation, $orderCopy->client_id));
        return view('orders.reviwer', $data);
    }

    /**
     *  Просмотр перед согласовнаие или оптравкой на ревью
     */
    public function reviwer($new_invoice, $order_id)
    {
        $editable = request('editable', true);

        $user = Auth::user();

        $order = Orders::find($order_id);
        if ($editable && $editable !== 'false') {
            if (!$order) {
                return redirect('/home');
            }
        }

        if (($order->status == 2) || (($order->status > 5) && ($order->status != 9) && ($order->status < 2000))) {
            return Redirect::back()->with('selected_group_order_id', $order->group_id);
        }

        if ($order->type_product != 999999) {
            $templates_info = \DB::table('product_types')->find($order->type_product);
            $order->type_name = $templates_info->name;
        } else {
            $order->type_name = $order->price_details['selected_product_type_name'];
        }
        $responsible = auth()->user()->isClient() ? auth()->user()->id : $order->group->client->user_id;
        if (!Order_logs::templateNotAgree($order->id)) {
            addLog($order->id, 'Макет просмотрен', $responsible, '', null, 0, auth()->user()->isClient() ? 'в личном Кабинете' : 'в присутствии менеджера');
            addLog($order->id, 'Макет не утверждён', $responsible, '', null, 0, auth()->user()->isClient() ? 'в личном Кабинете' : 'в присутствии менеджера');
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Страница согласования';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );
        $data = [
            'page_title' => $order->type_name,
            'page' => $page,
            'user' => $user,
            'order' => $order,
            'company_details' => $order->group->client()->first()->getDetailsOK(),
            'company_id' => $order->client_id,
            'type_id' => $order->type_product,
            'new_invoice' => $new_invoice,
            'reviwer' => '1',
            'template_chromacity' => view('templates.chromacity'),
            'template_paper_clip_chromacity' => view('templates.chromacity_paper_clip')
        ];
        $fill_calculation = array_merge($order->price_details, ['draw' => $order->draw, 'price' => $order->price]);
        $data = array_merge($data, $this->getPriceInfo($order->type_product, $fill_calculation, $order->client_id));
        return view('orders.reviwer', $data);

    }

    public function reviwerSave(Request $request)
    {
        $order = Orders::find($request->order_id);
        $oldPrice = $order->price;
        if ($request->fill_calculation) {
            $this->saveCalculation($order, $request->fill_calculation);
        }
        (new GroupOrdersController)->recalculateGroupPrice($order->group_id);

        if (($order->group->next == 0) && ($oldPrice != $order->price)) {
            addLog(0, 501, auth()->user()->id, "№{$order->group->invoice_number}.{$order->position} c {$oldPrice}руб. на {$order->price}руб.", $order->group->id, 0, $order->price_details['what_changed']);
        }

        return redirect('/order/reviwer/' . $request->new_invoice . '/' . $request->order_id);
    }

    /**
     * Отправляем клиенту на согласование
     */
    public function reviwerSend($order_id)
    {
        $order = Orders::find($order_id);

        if (!$order) {
            return redirect('/home');
        }

        $order->client_info = Clients::find($order->client_id);

        (new SendLayoutEmail('emails.reviwer'))
            ->send($order);

        return redirect('/orders/history');
    }

    /**
     * Правка страницы согласования клиента
     */
    public function reviwerUserEdit($order_id)
    {
        $user = Auth::user();

        $order = Orders::find($order_id);

        if ($order->status > 3) {
            return redirect('/orders/history');
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );
        return view('orders.reviwerEdit', [
            'page' => $page,
            'user' => $user,
            'order_status' => $order->status,
            'order_id' => $order_id,
            'manager' => $this->getManager($order->manager_id),
        ]);
    }

    /**
     * Смена статуса
     */
    public function reviwerUserStatus($order_id, $status, $fill_calculation = null)
    {
        $user = Auth::user();

        $order = Orders::find($order_id);
        $id = $order[0]->id;
        $client = Clients::find($order[0]->client_id);
        $need_account = $this->checkEmailForNeedCreateAccount($client->email);

        $order = Orders::find($id);
        $order->status = $status;
        if ($fill_calculation) {
            $this->saveCalculation($order, $fill_calculation);
            return redirect('/user/reviwer/status/' . $order_id . '/4');
        }

        $order->save();
        $serviceInfo = $this->getPriceTableForProduct($order->type_product, $order->client_id);
        $history = Orders::find($order->id)->get();

        $history_orders = $this->getOrdersCalculationData($history, $order->id);
        $pdf_link = $this->getOrderPDF(1, $order->id, $serviceInfo, $history_orders, "");

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );

        $client_info = Clients::find($order->client_id);
        $manager = $this->getManager($order->manager_id);
        $company_name = settings('email_title') ? settings('email_title') : settings('company_name');

        $data = array(
            'subject' => 'Счет на оплату от ' . $company_name,
            'to' => $client_info->email,
            'email' => $manager->email,
            'name' => $client_info->name,
            'client' => $client_info,
            'url' => '',
            'pdf_link' => $pdf_link,
            'order_id' => $order->id,
            'manager' => $this->getManager($order->manager_id),
        );

        \Mail::send('emails.invoice', $data, function ($message) use ($data) {
            try {
                $message->attach($data['pdf_link'], array('as' => 'invoice_' . $data['order_id'] . '.pdf', 'mime' => 'application/pdf'));

                $message->subject($data['subject'])
                    ->to($data['to'])
                    ->from($data['email'], $_SERVER["SERVER_NAME"] . ' - Ваш счет');
            } catch (Exception $e) {
            }
        });

        return view('orders.reviwerYes', ['page' => $page,
            'user' => $user,
            'client' => $client,
            'need_account' => $need_account,
            'manager' => $this->getManager($order->manager_id),
            'order_status' => $order->status,
            'pdf_link' => $pdf_link,
        ]);
    }

    public function checkEmailForNeedCreateAccount($client_email)
    {
        if (count(User::where('email', $client_email)->get()) > 0) {
            return null;
        } else {
            return true;
        }
    }

    public function reviwerUserStatusCreateAccount($client_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $client = Clients::find($client_id);
        if (count(User::where('email', $client->email)->first()) == 0) {
            $user = UsersController::createAuto(1, Auth::user()->id, $client->name, '', $client->phone, $client->email, $request->password, '');
        }
        $client->user_id = $user->id;

        $data = array(
            'subject' => 'Регистрация в системе w2p',
            'to' => $client->email,
            'email' => $request->manager_email,
            'password' => $request->password,
            'name' => $client->name,
            'manager_name' => $request->manager_name,
            'manager_lastname' => $request->manager_lastname,
            'manager_phone' => $request->manager_phone,
            'login_token' => $user->login_token,
            'client' => $client,
        );

        \Mail::send('emails.new_user', $data, function ($message) use ($data) {
            $message->subject($data['subject'])
                ->to($data['to'])
                ->from($data['email']);
        });

        Auth::login($user, true);
        return redirect('/orders/history');
    }

    /**
     *
     */
    public function reviwerUserSee($order_id, $fill_calculation = null)
    {
        //$user = Auth::user();
        $order = Orders::query()->where('file', $order_id . '.frx')->first();

        if (empty($order)) {
            abort(404);
        }

        if ($order->status > 3) {
            return redirect('/user/reviwer/status/' . $order_id . '/4');
        }

        if ($fill_calculation) {
            $this->saveCalculation($order, $fill_calculation);
            return redirect('/user/reviwer/see/' . $order_id);
        }

        $templates_info = \DB::table('product_types')->find($order->type_product);
        $order->type_name = $templates_info->name;

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Новый заказ';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );

        $data = [
            'page' => $page,
            'order' => $order,
            'order_status' => $order->status,
            'company_id' => $order->client_id,
            'type_id' => $order->type_product,
            'manager' => $this->getManager($order->manager_id),
        ];

        $fill_calculation = array_merge($order->price_details, ['draw' => $order->draw, 'price' => $order->price]);
        $data = array_merge($data, $this->getPriceInfo($order->type_product, $fill_calculation, $order->client_id));
        return view('orders.reviwerSee', $data);
    }

    public function getManager($user_id)
    {
        $manager = User::query()->where('id', $user_id)->get(['id', 'name', 'lastname', 'photo', 'email', 'phone'])[0];

        if (count($manager) == 0) {
            $manager = json_decode("{}");
            $manager->id = null;
            $manager->name = null;
            $manager->lastname = null;
            $manager->email = null;
            $manager->photo = null;
        }
        return $manager;
    }

    /**
     * Route /orders/history
     *
     * @param int $status
     * @param int $user_id
     * @param int $id_order
     * @return \Illuminate\Http\Response
     */
    public function history($status = 0, $user_id = 0, $order_id = 0, $pdf_sign = 0, $filters = 0)
    {
        $user = Auth::user();
        $user_id = $user->id;
        if ($order_id > 0) {
            $order = Orders::find($order_id);
            $serviceInfo = $this->getPriceTableForProduct($order->type_product, $order->client_id);
        } else {
            $serviceInfo = json_decode('{}');
        }
        $product_types = \DB::table('product_types')->get();
        $userBlade = "";
        $query = \DB::table('orders');
        if ($user->id_group == 1) {
            $userBlade = '_user';
            $client_info = Clients::query()->where('email', $user->email)->first();
            $query = $query->where('client_id', '=', $client_info->id);
        } else if ($user->id_group == 2) {
            $query = $query->where('manager_id', '=', $user_id);
        }

        switch (intval($status)) {
            case 1: {
                $query = $query->whereIn('status', [2, 4, 102, 112]);
                break;
            } // Требуют внимания!
            case 2: {
                $query = $query->where('status', 3);
                break;
            } // На согласовании
            case 3: {
                $query = $query->where('money', 0)->where('status', '>', '4');
                break;
            }// Ожидает оплаты
            case 4: {
                $query = $query->where('status', 8);
                break;
            } // Печать без предоплаты
            case 5: {
                $query = $query->where('status', 9);
                break;
            }// В работе
            case 6: {
                $query = $query->whereIn('status', [12]);
                break;
            } // Готов к выдаче
        }

        if ($filters == 0) {
            $filters = json_decode('{"search_string" : "", "type_id" : null, "view" : null}');
        } else {
            $filters = json_decode($filters);
        }

        $history =
            $query->join('clients as c', function ($query) use ($filters) {
                if (strlen($filters->search_string) > 0) {
                    $search_string = "%" . $filters->search_string . "%";
                    $query->on('c.id', '=', 'client_id')
                        ->where('c.name', 'like', $search_string)
                        ->orWhere('c.phone', 'like', $search_string)
                        ->orWhere('c.email', 'like', $search_string);
                } else {
                    $query->on('c.id', '=', 'client_id');
                }
            })//TODO Если для поиска понадобятся контакты клиента
            /*$query->join('clients as c', 'orders.client_id', '=', 'c.id')
                ->join('contacts_clients_relation', 'contacts_clients_relation.client_id', '=', 'c.id')
                ->join('contacts', function ($query) use ($filters) {
                    if (strlen($filters->search_string) > 0) {
                        $search_string = "%" . $filters->search_string . "%";
                        $query->on('contacts.id', '=', 'contacts_clients_relation.contact_id')
                            ->where('c.name', 'like', $search_string)
                            ->orWhere('contacts.phone', 'like', $search_string)
                            ->orWhere('contacts.email', 'like', $search_string)
                            ->orWhere('contacts.name', 'like', $search_string);
                    } else {
                        $query->on('contacts.id', '=', 'contacts_clients_relation.contact_id');
                    }
                })*/
            ->join('product_types as p', function ($query) use ($filters) {
                if ($filters->type_id > 0) {
                    $query->on('orders.type_product', '=', 'p.id')
                        ->where('p.id', '=', ($filters->type_id));
                } else {
                    $query->on('orders.type_product', '=', 'p.id');
                }
            })
                ->select('orders.*', 'c.name as client_name', 'p.name as product_name', 'c.phone as contacts_phone', 'c.email as contacts_email')
                ->orderBy('orders.updated_at', 'desc')->paginate(25);

        foreach ($history as $key => $value) {
            /* Информация о коде статуса */
            $history[$key]->status = \DB::table('order_statuses')->find($value->status);

            if ($value->pay == $value->price) {
                $value->status_text = '<span class="label label-info" style="background-color: #00a65a !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Оплачен полностью</span>';
            } else if ($value->pay < $value->price) {
                if ($value->pay > 0) {
                    $value->status_text = '<span class="label label-info" style="background-color: #1abc9c !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Частично оплачен</span>';
                } else {
                    $value->status_text = '<span class="label label-info" style="background-color: #5da6c2 !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Печать без предоплаты</span>';
                }
            } else {
                $value->status_text = '<span class="label label-info" style="background-color: #00a65a !important;"><i style="font-size: 9px" class="fa fa-fw fa-ruble"></i> Клиент переплатил</span>';
            }
        }

        if ($filters->view) {
            if (!isset($_GET['page'])) {
                return view('orders.history' . $userBlade . '_table',
                    ['history' => $history])->render();
            }
        }

        /* Новая история */

        $history_orders = $this->getOrdersCalculationData($history, $order_id);
        $pdf_link = $this->getOrderPDF($pdf_sign, $order_id, $serviceInfo, $history_orders, "");
        if (strlen($pdf_link) > 0) {
            return response()->download($pdf_link, basename($pdf_link));
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'История';
        $page = array(
            'menu' => 'order',
            'submenu' => 'history',
            'breadcrumbs' => $breadcrumbs
        );
        return view('orders.history' . $userBlade, [
            'page' => $page,
            'history' => $history,
            'history_orders' => json_encode($history_orders),
            'service_info' => json_encode($serviceInfo),
            'pdf_sign' => $pdf_sign,
            'pdf_link' => $pdf_link,
            'product_types' => $product_types,
            'filters' => $filters,
            'filter_type_id' => $filters->type_id,
            'filter_search_string' => $filters->search_string,
            'order_id' => $order_id,
        ]);
    }

    public function getOrdersCalculationData($history, $id_order)
    {
        $history_orders = json_decode('{}');
        foreach ($history as $item) {
            if ($item->id == $id_order) {
                if ($item->price_details != null) {
                    $history_orders->draw = $item->draw;
                    $history_orders->price = $item->price;
                    $history_orders->price_details = $item->price_details;
                    return ($history_orders);
                }
            }
        }
        return $history_orders;
    }

    public function getOrderPDF($pdf_sign, $id_order, $serviceInfo, $calculation_data, $slash)
    {
        if ($pdf_sign == 1) {
            if (!isset($calculation_data->group_selected_index)) {
                $group_selected_index = 0;
            } else {
                $group_selected_index = $calculation_data->group_selected_index;
            }

            $serviceInfo = $serviceInfo->$group_selected_index;

            $order = Orders::find($id_order);
            $product_name = ProductType::find($order->type_product)->name;
            $client = Clients::where('id', $order->client_id)->get(['name'])[0];
            $detail = Details::find($order->detail_id);
            if ($detail) {
                $pdf = App::make('dompdf.wrapper');
                $months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
                $html =
                    '<html>' .
                    '<head>' .
                    '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                    '<style type="text/css">* {font-family: "times";} table tr td {border: 1px solid black;} .border0px {border: 0;}</style>' .
                    '</head>' .
                    '<body TEXT="#000000">' .
                    '<div>' . $detail->name . '<br><br></div>' .
                    '<div>' . $detail->post_address . ' ' . $detail->address . ' <br><br></div>' .
                    '<div style="text-align:center;">Образец заполнения платежного поручения</div>' .
                    '<table cellpadding="5" cellspacing="0" width="100%">' .
                    '<tr>' .
                    '<td>ИНН ' . $detail->bank_inn . '</td><td>КПП ' . $detail->bank_kpp . '</td><td rowspan="2" style="vertical-align:bottom;">Сч. №</td><td rowspan="2" style="vertical-align:bottom;">43</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td colspan="2">Получатель<br>' . $detail->name . '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td colspan="2" rowspan="2">Банк получателя<br>' . $detail->bank_name . '</td><td>БИК</td><td>' . $detail->bank_bik . '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td>Сч. №</td><td>30101810600000000602</td>' .
                    '</tr>' .
                    '</table>' .
                    '<div style="text-align:center;"><br>СЧЕТ № ' . $id_order . ' от ' . date("d") . ' ' . $months[date("n") - 1] . ' ' . date("Y") . ' г.<br><br><br><br></div>' .
                    '<div>Плательщик: ' . $client->name . '<br><br></div>' .
                    '<div>Грузополучатель: ' . $client->name . '<br><br></div>' .
                    '<table cellpadding="5" cellspacing="0" width="100%">' .
                    '<tr style="text-align:center;">' .
                    '<td width="20px">№</td><td width="350px">Наименование<br>товара</td><td width="50px">Единица<br>изме-<br>рения</td><td width="50px">Коли-<br>чество</td><td>Цена</td><td>Сумма</td>' .
                    '</tr>' .
                    '<tr><td style="text-align:right;">1</td><td>Создание макета' .
                    '</td><td style="text-align:center;">шт</td><td style="text-align:right;">1' .
                    '</td><td style="text-align:right;">200' .
                    '</td><td style="text-align:right;">200</td></tr>';
                $names = "";
                $summ = 200;
                $names_index = 0;

                $price_index = 0;
                $index = 0;
                if (!isset($calculation_data->draw)) {
                    $calculation_data->draw = 0;
                }
                foreach ($serviceInfo->counts as $count) {
                    if ($count > $calculation_data->draw) {
                        if ($index > 0) {
                            $price_index = $index - 1;
                        }
                        break;
                    }
                    $index++;
                }
                if ($index == count($serviceInfo->counts)) {
                    $price_index = $index - 1;
                }

                $draw = $calculation_data->draw;
                $countCounts = count($serviceInfo->counts);

                $index = 0;
                if (is_array($calculation_data->price_details)) {
                    $calculation_data->price_details = (object)$calculation_data->price_details;
                } else if (is_string($calculation_data->price_details)) {
                    $calculation_data->price_details = json_decode($calculation_data->price_details);
                }
                if (isset($calculation_data->price_details)) {
                    foreach ($serviceInfo->names as $service) {
                        if ($service == $calculation_data->price_details->name) {
                            $need_approx = ($draw > $serviceInfo->counts[0]) && ($draw < $serviceInfo->counts[$countCounts - 1]);
                            if ($need_approx) {
                                $price1 = $serviceInfo->prices[$index * $countCounts + $price_index];
                                $price2 = $serviceInfo->prices[$index * $countCounts + $price_index + 1];
                                $count1 = $serviceInfo->counts[$price_index];
                                $count2 = $serviceInfo->counts[$price_index + 1];
                                $approxPrice = $price1 + ($draw - $count1) / ($count2 - $count1) * ($price2 - $price1);
                            } else {
                                $approxPrice = $serviceInfo->prices[$index * $countCounts + $price_index];
                            }
                            $approxPrice = $approxPrice;
                            $summ_part = $approxPrice * $draw;
                            $names_index++;
                            $names .= '<tr><td style="text-align:right;">' . ($names_index + 1) . '</td><td>' . $product_name . ', ' . $service .
                                '</td><td style="text-align:center;">шт</td><td style="text-align:right;">' . $draw .
                                '</td><td style="text-align:right;">' . $approxPrice .
                                '</td><td style="text-align:right;">' . $summ_part . '</td></tr>';
                            $summ += $summ_part;

                        };
                        $index++;
                    };
                    $index = 0;
                    foreach ($serviceInfo->options as $service_options) {
                        foreach ($calculation_data->price_details->options as $option) {
                            if ($service_options == $option) {
                                $need_approx = ($draw > $serviceInfo->counts[0]) && ($draw < $serviceInfo->counts[$countCounts - 1]);
                                if ($need_approx) {
                                    $price1 = $serviceInfo->pricesOptions[$index * $countCounts + $price_index];
                                    $price2 = $serviceInfo->pricesOptions[$index * $countCounts + $price_index + 1];
                                    $count1 = $serviceInfo->counts[$price_index];
                                    $count2 = $serviceInfo->counts[$price_index + 1];
                                    $approxPrice = $price1 + ($draw - $count1) / ($count2 - $count1) * ($price2 - $price1);
                                } else {
                                    $approxPrice = $serviceInfo->pricesOptions[$index * $countCounts + $price_index];
                                }
                                $approxPrice = $approxPrice;
                                $summ_part = $approxPrice * $draw;

                                $names_index++;
                                $names .= '<tr><td style="text-align:right;">' . ($names_index + 1) . '</td><td>Опция: ' . $service_options .
                                    '</td><td style="text-align:center;">шт</td><td style="text-align:right;">' . $draw .
                                    '</td><td style="text-align:right;">' . $approxPrice .
                                    '</td><td style="text-align:right;">' . $summ_part . '</td></tr>';
                                $summ += $summ_part;
                            };
                        }
                        $index++;
                    };
                }

                $html .= $names;

                $html .= '<tr style="text-align:right;">' .
                    '<td colspan="5" class="border0px">Итого:</td><td>' . $summ . '</td>' .
                    '</tr>' .
                    '<tr style="text-align:right;">' .
                    '<td colspan="5" class="border0px">Без налога (НДС).</td><td>-</td>' .
                    '</tr>' .
                    '<tr style="text-align:right;">' .
                    '<td colspan="5" class="border0px">"Всего к оплате:</td><td>' . $summ . '</td>' .
                    '</tr>' .
                    '</table>' .
                    '<div>Всего наименований ' . ($names_index + 1) . ', на сумму ' . $summ . '</div>' .
                    '<div>' . $this->num2str($summ) . '<br><br><br><br></div>' .
                    '<div>Руководитель предприятия_____________________ (' . $detail->director . ')<br><br></div>' .
                    '<div>Главный бухгалтер____________________________ (' . $detail->director . ')</div>' .
                    '</body>' .
                    '</html>';
                $pdf->loadHTML($html);
                $time_stamp = time();
                $pdfFilename = 'order_n_' . $id_order . '_' . $time_stamp . '.pdf';
                $path = "orders_pdf/{$pdfFilename}";
                $pdf->save(public_path($path));
                $order->last_invoice_pdf = $time_stamp;
                $order->invoice_pdf = $pdfFilename;
                $order->save();
                return $slash . $path;
            } else {
                return '/options/details/';
            }
        } else {
            return '';
        }
    }

    public function historyDelete($id, Request $request)
    {
        $order = Orders::find($id);
        if ($order) {
            $order->delete();
        }
        if ($request->has('page')) {
            return redirect('/orders/history?page=' . $request->input('page'));
        }
        return redirect('/orders/history');
    }

    public function status($id, $status, $fill_calculation = null, $agree = null)
    {
        $order = Orders::find($id);
        if ($status == 45) {
            $order->main_sort_order = -2;
        }
        $order->status = $status;
        $order->save();
        if ($fill_calculation) {
            $this->saveCalculation($order, $fill_calculation);
        }

        $this->check_order_for_closing($order->group_id, $status);
        if ($status == 51) {
            // Редирект на метод роута (на всякий случай).
            // Для отправки на выдачу (status=51) использовать
            // Route::get('/order/status/send_to_issue/{id}/{?address_id}', 'OrdersController@sendToIssue');
            //
            return $this->sendToIssue($id, 0, $agree);
        }
        if ($status == 52) {
            addLog($order->id, 'Выдан', auth()->user()->id, '', null, 0, '');
        }

        // Закрыть с возвратом денег
        if ($status == 57) {
            $compensationAmount = $order->price;

            addLog($order->id, 'Возврат денег', auth()->user()->id, "Cумма компенсции: {$compensationAmount}", null, 0, '');
            addLog(0, 506, auth()->user()->id, "#{$order->group->invoice_number}.{$order->position}. Цена: {$order->price} руб.", $order->group->id, 0, 'Причина: ' . $order->defect_reason);

            $stoppedReasons = $order->stopped_reasons;
            $stoppedReasons['expenses'] = 0;
            $stoppedReasons['price'] = $order->price;
            $order->stopped_reasons = $stoppedReasons;
            $order->save();
            (new GroupOrdersController)->recalculateGroupPrice($order->group->id);

            event(new OrderWasClosedWithRefund($order, $compensationAmount));
        }

        if (isset($agree)) {
            return $order->group->next;
        } else {
            return Redirect::back()->with('selected_group_order_id', $order->group_id);
        }
    }

    /**
     * Заказ принят на выдаче
     *
     * @param $orderId
     */
    public function acceptedOnIssue($orderId)
    {
        $statusId = status('on_issue', 'id');

        $order = Orders::findOrFail($orderId);
        $order->status = $statusId;
        $order->save();

        $this->check_order_for_closing($order->group_id, $statusId);

        (new ReadyOrderEmail)->send($order, $order->selected_issue_address);
        addLog($order->id, 51, auth()->user()->id, 'Готов к выдаче', null, 0, 'Клиенту отправлено уведомление');

        return Redirect::back()->with('selected_group_order_id', $order->group_id);
    }

    public function postStatus(Request $request)
    {
        $order = Orders::find($request->id);
        if ($request->status == 45) {
            $order->main_sort_order = -2;
        }

        $order->status = $request->status;
        $responsible = auth()->user()->isClient() ? auth()->user()->id : $order->group->client->user_id;

        //TODO::убрать когда-нибудь позже. Отладка счёта
        addLog($order->id, 'Макет утверждён', $responsible, '', null, 0, auth()->user()->isClient() ? '' : 'в присутствии менеджера'); //Макет утверждён без изменений/с правками клиента (менеджера - если на месте)
        $oldPrice = $order->price;
        $order->save();
        if ($request->fill_calculation) {
            $this->saveCalculation($order, $request->fill_calculation);
        }
        (new GroupOrdersController)->recalculateGroupPrice($order->group_id);

        if (($order->group->next == 0) && ($oldPrice != $order->price)) {
            addLog(0, 501, auth()->user()->id, "№{$order->group->invoice_number}.{$order->position} c {$oldPrice}руб. на {$order->price}руб.", $order->group->id, 0, $order->price_details['what_changed']);
        }

        $this->check_order_for_closing($order->group_id, $request->status);

        if (isset($request->agree)) {
            $response = new \stdClass();
            $response->client_for_close = $this->checkNotClosedGroup(0);
            $response->count_not_closed_orders = $order->group->orders()->whereIn('status', [3, 4, 5, 9, 301])->where('id', '<>', $order->id)->count();
            if ($order->group->next == 0) {
                if ($response->count_not_closed_orders == 0) {
                    if (\Auth::user()->accessLevel() == 1) {
                        (new SendInvoiceNotification)->send($order);
                    } else {
                        if ($order->group->client()->first()->getDetailsOK() == 0) {
                            (new SendInvoiceNotification)->send($order);
                        } else {
                            $response->order_id = $order->id;
                        }
                    }
                }
            } else {
                if ($order->group->client()->first()->getDetailsOK() > 0) {
                    $response->order_id = $order->id;
                }
            }

            // Создаём шаблон в архив клиента только если у него нет такого
            if ($order->template_edited) {
                Template::makeFromOrder($order);
            }

            return json_encode($response);
        } else {
            return Redirect::back()->with('selected_group_order_id', $order->group_id);
        }
    }

    public function check_order_for_closing($group_id, $status)
    {
        if (in_array($status, [6, 52, 57, 58, 1010])) {
            $group_orders = Orders::where('group_id', $group_id)->get(['status']);
            $count_closed = 0;
            foreach ($group_orders as $item) {
                if (in_array($item->status, [6, 52, 57, 58, 1010])) {
                    if ($item->price == $item->pay) {
                        $count_closed++;
                    }
                }
            }
            if ($count_closed == count($group_orders)) {
                $group_order = GroupOrders::find($group_id);
                if ($group_order->price == $group_order->pay) {
                    $group_order->status = 52;
                    $group_order->save();
                }
            }
        }
    }

    function checkNotClosedGroup($close)
    {
        $group_order = GroupOrders::query()->where('next', '=', 1)->where('manager_id', '=', \Auth::user()->id)->first();
        if (count($group_order) > 0) {
            if ($close == 1) {
                $group_order->next = 0;
                $group_order->save();
                return 'OK';
            } else {
                return $group_order->client_id;
            }
        }
        return -1;
    }

    public function set_status_id($order_id, $status)
    {
        $order = Orders::find($order_id);
        if ($status == 5) {
            addLog($order->id, 'Отправлен на согласование', auth()->user()->id, $order->group->client->email, null, 0, 'на почту заказчика');
            return $this->status($order->id, $status, null, 1);
        }

        $result = $this->status($order_id, $status, null, 1);
        if ($status > 25) {
            if ($status == 45) {
                if ($order->group->subdomain_id) {
//                    $this->status($order_id, 51, null, 1);
//                    $result = new \stdClass();
//                    $result->status = 51;
//                    return json_encode($result);
                } else {
                    addLog($order_id, 'Контроль качества', $order->group->manager_id, '', null, 0, '');
                }
                if ($order->sort_order > 55) {
                    $order->sort_order = 55; // Это должно убрать статус "Запущен на переделку"
                    $order->reworked = true; // Помечает заказ как "Переделан"
                    $order->save();
                }

            } else {
                addLog($order_id, $status, auth()->user()->id, '', null, 0, '');
            }
            $result = new \stdClass();
            $result->status = $status;
            $order_status = OrderStatuses::find($status);
            $result->status_name = $order_status->name;
            $result->status_color = $order_status->color;
            $result = json_encode($result);
        }
        return $result;
    }

    
    public function setCheckControlQuality(Request $request) {
        $order = Orders::find($request->order_id);
        $order->control_quality = true;
        $order->save();
        addLog($request->order_id, 'Готов к отгрузке', auth()->user()->id, '', null, 0, '');
        return 'ok';
    }
    
    public function statusIssue(Request $request)
    {
        $order = Orders::find($request->order_id);
        $log_value = '';
        $order->issued += $request->issue_draw;
        if (strlen($request->issue_draw_commentary) > 0) {
            $issue_troubles = new \stdClass();
            $issue_troubles->issued = (isset($order->issue_troubles) && isset($order->issue_troubles['issued']) ? $order->issue_troubles['issued'] : 0) + $request->issue_draw;
            $issue_troubles->commentary = $request->issue_draw_commentary;
            $issue_troubles->discount_type = $request->issue_discount_type;
            $issue_troubles->discount = $request->issue_draw_discount;
            $issue_troubles->need_print = $request->issue_draw_to_print;
            if (!isset($request->issue_draw_to_print)) {
                if ($issue_troubles->discount_type == 0) {
                    $issue_troubles->discount = round($order->price_details['price'] * (($order->draw - $request->issue_draw) / $order->draw), 0);
                    $log_value = 'Выдано' .
                        (($issue_troubles->discount > 0) ? (' (с пересчётом ' . $issue_troubles->discount . ' руб.)') : '') .
                        ': ' . $request->issue_draw . ' из ' . $order->draw;
                } else {
                    $log_value = 'Выдано' .
                        (($issue_troubles->discount > 0) ? (' (со скидкой ' . $issue_troubles->discount . ' руб.)') : '') .
                        ': ' . $request->issue_draw . ' из ' . $order->draw;
                }
            }
            $order->issue_troubles = $issue_troubles;
        }
        if (!isset($request->issue_draw_to_print)) {
            $order->status = 52;
        }
        $order->save();
        if (isset($issue_troubles) && ($issue_troubles->discount > 0)) {
            (new GroupOrdersController)->recalculateGroupPrice($order->group->id);
        }
        if ($order->status == 52) {
            $this->check_order_for_closing($order->group_id, 52);
        }
        addLog($request->order_id, $request->status, auth()->user()->id, $log_value, null, 0, $request->issue_draw_commentary);
        if (isset($request->issue_draw_to_print)) {
            $request->rework_type = 3;
            $request->rework_reason_count = $order->draw - $issue_troubles->issued;
            $request->rework_reason = $request->issue_draw_commentary;
            return $this->statusRework($order->id, $request);
        } else {
            return redirect('/orders/history');
        }
    }

    public function statusRework($order_id, Request $request)
    {
        $order = Orders::find($order_id);
        $order->spec_description = (object)['count' => $request->rework_reason_count, 'reason' => $request->rework_reason];
        switch ($request->rework_type) {
            // Возврат дизайнеру
            case 1:
                Specification::makeFromOrder($order, $request->rework_reason);
                $order->sent_revision_at = Carbon::now();
                $order->status = status('exclusive_design', 'id');
                $order->save();

                addLog($order->id, 56, auth()->user()->id, 'в очередь дизайнера', null, 0, $request->rework_reason);
                break;

            // Возврат заказа в печать (пока в препресс очередь)
            case 3:
                $order->status = status('on_rework')->id;
                $order->sent_revision_at = Carbon::now();
                $order->save();

                // Клонирует заказ с новыми свойствами
                $reworkOrder = $order->replicate();
                $reworkOrder->parent_id = $order->id;
                $reworkOrder->draw = $request->rework_reason_count;
                $reworkOrder->status = status('in_work')->id;
                $reworkOrder->price = 0;
                $reworkOrder->save();

                // Клонирует элементы родителя и добавляет их дочернему элементу
                $order->elements->each(function ($element) use ($reworkOrder) {
                    $reworkedElement = $element->replicate();
                    $reworkedElement->draw = $reworkOrder->draw;
                    $reworkedElement->postpress = $reworkOrder->price_details['selected_options'];
                    $reworkedElement->order()->associate($reworkOrder);
                    $reworkedElement->save();
                });

                addLog($order->id, 56, auth()->user()->id, 'в печать', null, 0, $request->rework_reason);
                break;

            // Возврат заказа в постпресс
            case 4:
                $order->sent_revision_at = Carbon::now();
                $order->status = status('in_postpress', 'id');
                $order->save();

                addLog($order->id, 56, auth()->user()->id, 'в постпресс', null, 0, $request->rework_reason);

                $preview = '/lib/frx/' . $order->img;
                if (strpos($order->img, 'http') !== false) {
                    $preview = $order->img;
                }

                // Создать новый наряд на порезку
                $performOrder = PerformOrder::create([
                    'collect_order_id' => $order->id,
                    'product' => $order->productTypeName(),
                    'print_sheet_format' => $order->price_details['selected_size'],
                    'draw' => $order->draw,
                    'postpress_processes' => [],
                    'issue_at' => Carbon::createFromTimestamp($order->close_date),
                    'perform_order_status_id' => PerformOrderStatus::PRINTED,
                    'preview' => $preview,
                    'collected' => true,
                    'revisioned' => true,
                    'single' => true,
                    'revision_comment' => $request->rework_reason,
                ]);

                // Добавить элементы заказа в наряд
                $order->elements->each(function ($element) use ($performOrder) {
                    $element->postpress = [];
                    $element->performOrder()->associate($performOrder);
                    $element->save();
                });

                break;
        }
        $order->main_sort_order = 0;
        if ($order->sort_order < 56) {
            $order->sort_order = 56;
        } else {
            $order->sort_order += 1;
        }
        $order->save();
        return Redirect::back()->with('selected_group_order_id', $order->group_id);
    }

    public function statusDefectReturn($order_id, Request $request)
    {
        $order = Orders::find($order_id);
        $group_order = GroupOrders::find($order->group_id);
        $group_order->status = 6;
        $group_order->save();
        $order->status = 55;
        $order->defect_reason = $request->defect_return_reason;
        $order->save();
        addLog($order->id, 55, auth()->user()->id, '', null, 0, $request->defect_return_reason);
        return Redirect::back()->with('selected_group_order_id', $order->group_id);
    }

    public function statusDefectCloseDiscount($order_id, Request $request)
    {
        $order = Orders::find($order_id);

        $discountAmount = $request->input('defect_close_with_discount_price');
        $reason = $request->input('defect_close_with_discount_comment');

        $defect_closed_discount = new \stdClass();
        $defect_closed_discount->price = $discountAmount;
        $defect_closed_discount->comment = $reason;

        $stoppedReasons = $order->stopped_reasons;
        $stoppedReasons['expenses'] = $order->price - $discountAmount;
        $stoppedReasons['price'] = $order->price;
        $order->stopped_reasons = $stoppedReasons;

        $order->status = 58;
        $order->defect_closed_discount = json_encode($defect_closed_discount);
        $order->save();

        (new GroupOrdersController)->recalculateGroupPrice($order->group->id);

        $this->check_order_for_closing($order->group->id, 58);

        addLog($order->id, 58, auth()->user()->id, $discountAmount . ' руб.', null, 0, $reason);
        addLog(0, 508, auth()->user()->id, $defect_closed_discount->price . ' руб.', $order->group->id, 0, 'Причина: ' . $order->defect_reason);

        event(new OrderWasClosedWithDiscount($order, $discountAmount, $reason));

        return Redirect::back()->with('selected_group_order_id', $order->group_id);
    }

    public function addLog($order_id, $status, $client_id, $value = 0, $groupOrderId = null, $payed = 0, $comment = '')
    {
        // Делаем запись в лог заказов
        $logs = new Order_logs;
        $logs->status = $status;
        $logs->order_id = $order_id;
        $logs->group_order_id = $groupOrderId;
        $logs->value = $value;
        $logs->pay = $payed;
        $logs->user_id = $client_id;
        $logs->comments = $comment;
        $logs->save();
    }

    public function editorSave()
    {
        return true;
        exit();
    }

    /*
     * Оплачиваем счет
     */
    public function invoiceClose($id, Request $request)
    {
        $group_order = GroupOrders::find($id);

        $price = $group_order->price_expenses - $group_order->pay;
        if ($price > 0) {
            switch ($request->pay_type) {
                case 1:
                    $group_order->pay_cash += $price;
                    break;
                case 2:
                    $group_order->pay_rs += $price;
                    break;
                case 3:
                    $group_order->pay_eth += $price;
                    break;
            }
            $group_order->pay = $group_order->price_expenses;
            if ($group_order->status == 7 | $group_order->status == 1 | $group_order->status == 8) {
                $group_order->status = 6;
            }
            $group_order->money = 1;
            $group_order->save();

            $this->check_order_for_closing($group_order->id, 6);

            $group_order->checkGroupOrdersForPrintlerDownloading();

            $pay_type = $request->input('pay_type');

            $types = [
                1 => 'в кассу',
                2 => 'по счёту',
                3 => 'e-money',
            ];

            $pay_type = isset($types[$pay_type]) ? $types[$pay_type] : $pay_type;
            $comment = "{$price} руб. {$pay_type}";

            addLog(0, 6, auth()->user()->id, "{$price} руб.", $group_order->id, 1, "{$comment}");

            event(new PaymentWasAdded($group_order, $price, $pay_type));
        }
        return Redirect::back()->with('selected_group_order_id', $group_order->id);
    }

    /*
     * Частичная оплата
     */
    public function addNotFull($id, Request $request)
    {
        $groupOrder = GroupOrders::findOrFail($id);

        $groupOrder->pay += $request->price;

        switch ($request->pay_type) {
            case 1:
                $groupOrder->pay_cash += $request->price;
                break;
            case 2:
                $groupOrder->pay_rs += $request->price;
                break;
            case 3:
                $groupOrder->pay_eth += $request->price;
                break;
        }

        if (!empty($request->close_date)) {
            $groupOrder->close_date = $request->close_date;
        } else {
            (new GroupOrdersController)->recalculateGroupPrice($id);
        }

        $groupOrder->status = 7;

        if ($groupOrder->pay >= $groupOrder->price) {
            $groupOrder->money = 1;
            $groupOrder->status = 6;
            $groupOrder->save();
        }

        $groupOrder->checkGroupOrdersForPrintlerDownloading();

        $groupOrder->save();

        $price = $request->input('price');
        $pay_type = $request->input('pay_type');

        event(new PaymentWasAdded($groupOrder, $price, $pay_type));

        $types = [
            2 => 'по счёту',
            1 => 'в кассу',
            3 => 'e-money',
        ];

        $pay_type = isset($types[$pay_type]) ? $types[$pay_type] : $pay_type;
        $comment = "{$price} руб. {$pay_type}";

        addLog(0, $groupOrder->status, auth()->user()->id, "{$price} руб.", $groupOrder->id, 1, "{$comment}");

        return Redirect::back()->with('selected_group_order_id', $groupOrder->id);
    }

    /*
     * Запускаем без оплаты
     */
    public function invoiceNotMoney($id)
    {
        $order = Orders::find($id);
        $order->status = 8;
        $order->save();
        $order->log('Печать без предоплаты разрешена');
        return Redirect::back()->with('selected_group_order_id', $order->group_id);
    }

    /**
     * Карточка ТЗ
     */
    public function tz_card($id)
    {
        $order = Orders::find($id);
        if (!$order) {
            return redirect('/home');
        }

        $order->client_info = Clients::find($order->client_id);
        $order->maneger_info = User::find($order->manager_id);

        $order_logs = Order_logs::query()->where('order_id', $id)->orderBy('id', 'desc')->get();

        $agreeKey = -1000;
        $subdomain_id = $order_logs->last()->subdomain_id;
        $partner = (Subdomain::findCurrent() == null) && ($subdomain_id != null);
        $print = true;
        foreach ($order_logs as $key => $value) {
            if ($print) {
                if ($value->status) {
                    if (($value->status_free == 'Черновик') && (count($order_logs) > 1)) {
                        unset($order_logs[$key]);
                    } else if (($value->status_free == 'Макет не утверждён') && (($key - $agreeKey) == 1)) {
                        unset($order_logs[$key]);
                    } else {
                        $order_logs[$key]['who'] = User::find($value->user_id);
                        if (isset($value->status_free)) {
                            $order_logs[$key]['status_name'] = $value->status_free;
                        } else {
                            $status = OrderStatuses::find($value->status);
                            $order_logs[$key]['status_name'] = $status ? $status->name : '';
                        }
                        if ($order_logs[$key]['status_name'] == 'Макет утверждён') {
                            $agreeKey = $key;
                        }
                    }
                }
            } else {
                unset($order_logs[$key]);
            }
            if ($partner && $print) {
                if ($value->status == 25) {
                    $print = false;
                }
            }
        }
        $breadcrumbs[] = 'Заказ';
        $breadcrumbs[] = 'Техническое задание';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );
        return view('orders.tz_card', [
            'page' => $page,
            'order' => $order,
            'logs' => $order_logs
        ]);
    }

    /**
     * Show position info for queue item
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|Redirect|\Illuminate\View\View
     */
    public function tz_queue($id)
    {
        $order = Orders::find($id);
        if (!$order) {
            return redirect('/home');
        }

        $order->client_info = Clients::find($order->client_id);
        $order->maneger_info = User::find($order->manager_id);

        $order_logs = Order_logs::query()->where('order_id', $id)->orderBy('id', 'desc')->get();

        foreach ($order_logs as $key => $value) {
            $order_logs[$key]['manager_info'] = User::find($value->user_id);

            $status = OrderStatuses::find($value->status);
            $order_logs[$key]['status_name'] = $status ? $status->name : '';
        }

        $breadcrumbs[] = 'Заказ';
        $breadcrumbs[] = 'Техническое задание';
        $page = array(
            'menu' => 'order',
            'submenu' => 'reviwer',
            'breadcrumbs' => $breadcrumbs
        );

        return view('orders.tz_card', [
            'from_queue' => true,
            'page' => $page,
            'order' => $order,
            'logs' => $order_logs
        ]);
    }

    /*
     * Добавление счета
     */
    public function addInvoiceNoMoney($id)
    {
        $group_order = GroupOrders::find($id);
        $group_order->status = 8;
        (new GroupOrdersController)->recalculateGroupPrice($id);

        $group_order->save();

        $closeDate = Carbon::parse($group_order->close_date)->format('d.m.Y');

        $comment = $group_order->payment_status ? $group_order->payment_status : "условия договора";

        addLog(0, 504, auth()->user()->id, "{$group_order->price} руб. до {$closeDate}", $group_order->id, 0, "{$comment}");

        $group_order->checkGroupOrdersForPrintlerDownloading();

        return Redirect::back()->with('selected_group_order_id', $group_order->id);
    }

    /*
     * Отправка в работу
     */
    public function in_work(Request $request, $id)
    {
        $order = Orders::find($id);

        DB::beginTransaction();

        $order->setStatus(status('in_work', 'id'));
        $order->close_date = strtotime($order->price_details['close_date'] . " 00:" . random_int(10, 20) . ":" . random_int(10, 59));

        // to self
        if ($request->hwodo == 1) {
            addLog($order->id, 'Запущен в работу', auth()->user()->id, 'Дата выдачи: ' . $order->price_details['close_date'], null, 0, 'Срочность: ' . $order->price_details['selected_urgency']);
        } // to contractor
        else {
            $contractor = Contractor::find($request->contractor_id);
            if ($contractor) {
                $job = ContractorOrderJob::create([
                    'order_id' => $order->id,
                    'contractor_id' => $contractor->id,
                    'status_id' => $order->status,
                    'price' => floatval($request->get('summ_worker')),
                    'deadline' => date('Y-m-d', strtotime($request->get('close_date'))),
                ]);

                // mark order
                $order->with_contractor = true;

                // history
                addLog($order->id, 'Отправлен подрядчику', auth()->user()->id,
                    $contractor->contacts->first()->email, null, 0,
                    $contractor->displayedName());

                $token = ContractorToken::create([
                    'contractor_id' => $contractor->id,
                    'order_id' => $order->id,
                ]);

                $mailer = new ContractorJobEmail();
                $mailer->send($contractor, $order, $token);
            }
        }
        $order->save();
        DB::commit();

        return Redirect::route('orders.history')->with('selected_group_order_id', $order->group_id);
    }

    public function loading($id_order_with_time)
    {
        $id_order = substr($id_order_with_time, 4, strlen($id_order_with_time) - 8);
        $order = Orders::find($id_order);

        if (count($order->price_details['print_structure']) == 0) {
            $order->status = 12;
            $order->save();
            return redirect('/loading/loaded/' . $id_order_with_time);
        }

        return view('loading.index', [
            'page' => 1,
            'manager' => $this->getManager($order->manager_id),
            'order_status' => $order->status,
            'id_order' => $id_order,
            'new' => $order->status == 10 ? 1 : 0,
            'print_structure' => $order->price_details['print_structure'],
            'printler_type' => $order->price_details['printler_type'],
            'selected_type' => $order->price_details['selected_type'],
            'order_img' => $order->img,
            'template_cover_download_id' => $order->template_cover_download_id,
            'template_pad_download_id' => $order->template_pad_download_id,
        ]);
    }

    public function manual_loading($id_order_with_time)
    {
        $id_order = substr($id_order_with_time, 4, strlen($id_order_with_time) - 8);
        $order = Orders::find($id_order);
        return view('loading.index_manual', [
            'page' => 1,
            'manager' => $this->getManager($order->manager_id),
            'id_order' => $id_order,
            'print_structure' => $order->price_details['print_structure'],
        ]);
    }

    public function loaded($id_order_with_time)
    {
        $id_order = substr($id_order_with_time, 4, strlen($id_order_with_time) - 8);
        $order = Orders::find($id_order);
        $client = Clients::find($order->client_id);
        $need_account = $this->checkEmailForNeedCreateAccount($client->email);

        $response = new \stdClass();
        $response->client_for_close = $this->checkNotClosedGroup(0);
        $response->count_not_closed_orders = $order->group->orders()->whereIn('status', [3, 4, 5, 9, 301])->where('id', '<>', $order->id)->count();
        return view('loading.loaded', [
            'manager' => $this->getManager($order->manager_id),
            'company_details' => $client->getDetailsOK(),
            'response' => json_encode($response),
            'client' => $client,
            'company_id' => $order->client_id,
            'need_account' => $need_account,
        ]);
    }

    public function templateAgree($order_id, $template_id, $type, $new = 1, Request $request)
    {
        $order = Orders::find($order_id);
        $types = ['', 'body', 'cover', 'pad'];
        $typesIndex = $types[$type];
        if ($type == 1) {
            $order->template_download_id = $template_id;
        } else if ($type == 2) {
            $order->template_cover_download_id = $template_id;
        } else if ($type == 3) {
            $order->template_pad_download_id = $template_id;
        }
        if ($new == 1) {
            if ($order->price_details['printler_type'] != 3) {
                $order->status = 12;
            } else {
                $need_body = isset($order->price_details['print_structure']['body']);
                $need_cover = isset($order->price_details['print_structure']['cover']);
                $need_pad = isset($order->price_details['print_structure']['pad']);
                if (((($need_body == false) && ($order->template_download_id == 0)) || ($need_body && ($order->template_download_id > 0))) &&
                    ((($need_cover == false) && is_null($order->template_cover_download_id)) || ($need_cover && ($order->template_cover_download_id != null))) &&
                    ((($need_pad == false) && is_null($order->template_pad_download_id)) || ($need_pad && ($order->template_pad_download_id != null)))) {
                        $order->status = 12;
                }
            }
            $size = $order->price_details['print_structure'][$typesIndex]['width'] . 'x' . $order->price_details['print_structure'][$typesIndex]['height'];
            if ($type == 1) {
                if ($order->price_details['printler_type'] < 3) {
                    addLog($order->id, 'Макет проверен', $order->group->client->user_id, '(автоматическая проверка)', null, 0, '');
                } else {
                    addLog($order->id, 'Макет проверен', $order->group->client->user_id, '(автоматическая проверка)', null, 0,
                        'Вн. блок ' . $order->price_details['selected_product'] . ' стр. ' . $size . ', ' . $order->price_details['selected_chromacity']);
                }
            } else if ($type == 2) {
                addLog($order->id, 'Макет проверен', $order->group->client->user_id, '(автоматическая проверка)', null, 0,
                    'Обложка ' . $size . ', ' . $order->price_details['selected_cover_chromacity']);
            } else if ($type == 3) {
                addLog($order->id, 'Макет проверен', $order->group->client->user_id, '(автоматическая проверка)', null, 0,
                    'Подложка ' . $size . ', ' . $order->price_details['selected_pad_chromacity']);
            }
        } else {
            if ($order->status_last < 25) {
                $order->stopped(0, 'Загружен новый макет');
                $order->status = 112;
            } else {
                if ($order->stopped_user_id == auth()->user()->id) {
                    $order->stopped(0, 'Загружен новый макет');
                    $order->status = 25;
                } else {
                    $order->setStopReson(0, 'Загружен новый макет');
                }
            }
            $order->workWithTemplate(0);
            $order->template_changed = true;
        }
        $temp_print_structure = $order->price_details;
        $temp_print_structure['print_structure'][$typesIndex]['loaded'] = true;
        $order->price_details = $temp_print_structure;
        $order->save();

        TemplateDownload::where('id_order', $order->id)->delete();
        $template_download = new TemplateDownload;
        $template_download->id = $template_id;
        $template_download->id_order = $order_id;
        $groupOrder = GroupOrders::where('id', $order->group_id)->first();
        $template_download->pdf_download = ($groupOrder->pay == $groupOrder->price);
        $template_download->save();
        $template_download->startDownloadTemplates();

        if ($order->status == 12) {
            return redirect('/loading/loaded/' . random_int(1000, 9999) . $order_id . random_int(1000, 9999));
        } else {
            return redirect('/loading/' . random_int(1000, 9999) . $order->id . random_int(1000, 9999));
        }
    }

    public function checkStatus($order_id, $status)
    {
        $order_status = Orders::find($order_id)->status + 0;
        $status = $status + 0;
        echo($status != $order_status);
    }

    public function turnOrders($filters = null)
    {
        $turn_orders = Orders::where('with_contractor', false)->where('status', '>', 24)->where('status', '<', 46)->orWhere('status', 1000)->where('status_last', '>', 24)->where('status_last', '<', 46)
            ->join('order_statuses as os', function ($query) use ($filters) {
                if (($filters) && ($filters->status > 0)) {
                    $query->on('orders.status', '=', 'os.id')
                        ->where('os.status', '=', ($filters->status));
                } else {
                    $query->on('orders.status', '=', 'os.id');
                }
            })->select('orders.id', 'orders.group_id', 'orders.stopped_user_id', 'orders.template_library_id', 'orders.template_changed', 'orders.draw', 'orders.close_date as sort_close_date', 'orders.price_details', 'orders.file', 'orders.img', 'orders.pdf', 'orders.reworked',
                'orders.type_product', 'orders.status', 'orders.status_last', 'orders.main_sort_order', 'orders.sort_order','orders.designer_working', 'os.name as order_status_name', 'os.color as order_status_color')
            ->orderBy('orders.status', 'asc')->orderBy('orders.main_sort_order', 'desc')->orderBy('orders.sort_order', 'desc')->orderBy('orders.close_date')->paginate(10);

        foreach ($turn_orders as $item) {
            if ($item->type_product != 999999) {
                $product_type = ProductType::find($item->type_product);
            } else {
                $product_type = ProductType::where('name', $item->price_details['selected_product_type_name'])->first();
                $item->product_name = $product_type->name;
                $item->product_services = $product_type->services;
                $item->printler_type = $product_type->printler_type;
            }
            $item->sides_text = $item->seleted_chromacity_text();
            $item->selected_chromacity = $item->price_details['selected_chromacity'];
            $item->product_name = $product_type->name;
            $item->product_services = $product_type->services;
            $item->printler_type = $product_type->printler_type;
            $groupOrders = Orders::where('group_id', '=', $item->group_id)->get();
            $positionIndex = 1;
            foreach ($groupOrders as $groupOrder) {
                if ($groupOrder->id == $item->id) {
                    $item->position = $positionIndex;
                    break;
                }
                $positionIndex++;
            }

            //Подготовка к выдаче сообщений - макет изменён/заменён
            /*if ($item->template_changed) {
                if ($item->template_library_id == null) {
                    $status_template_change = OrderStatuses::find(112);
                } else {
                    $status_template_change = OrderStatuses::find(102);
                }
                $item->template_change_name = $status_template_change->name;
                $item->template_change_color = $status_template_change->color;

            }*/
            if ($item->sort_order > 55) {
                $status_rework = OrderStatuses::find(56);
                $item->order_status_rework_name = $status_rework->name;
                $item->order_status_rework_color = $status_rework->color;
            }
            if ($item->status_last > 0) {
                $status_last = OrderStatuses::find($item->status_last);
                $item->order_status_last_name = $status_last->name;
                $item->order_status_last_color = $status_last->color;
            }
            $item->selected_size = $item->price_details['selected_size'];
            if (isset($item->price_details['close_date'])) {
                $item->close_date = $item->price_details['close_date'];
            } else {
                $item->close_date = '';
            }
        }

        $breadcrumbs[] = 'Заказы';
        $breadcrumbs[] = 'Очередь заказов';
        $page = array(
            'menu' => 'order',
            'submenu' => 'turn_orders',
            'breadcrumbs' => $breadcrumbs
        );

        return view('orders.turn_orders', [
            'page' => $page,
            'turn_orders' => $turn_orders,
        ]);
    }

    public function changePosition($order_id, $delta)
    {
        $turn_orders = Orders::where('status', '>', 24)->where('status', '<', 46)->orWhere('status', 1000)->where('status_last', '>', 24)->where('status_last', '<', 46)->get();
        $order_item = Orders::find($order_id);
        $selected_item = null;
        foreach ($turn_orders as $item) {
            if ($delta < 0) {
                if ($item->close_date < $order_item->close_date) {
                    if (!isset($selected_item)) {
                        $selected_item = $item;
                    }
                    if ($item->close_date > $selected_item->close_date) {
                        $selected_item = $item;
                    }
                }
            } else {
                if ($item->close_date > $order_item->close_date) {
                    if (!isset($selected_item)) {
                        $selected_item = $item;
                    }
                    if ($item->close_date < $selected_item->close_date) {
                        $selected_item = $item;
                    }
                }
            }
        }

        if ($selected_item) {
            $temp_time = $selected_item->close_date;
            $order_item->close_date = $selected_item->close_date;
            $selected_item->close_date = $temp_time;
            $order_item->save();
            $selected_item->save();
        }
        return $this->turnOrders();
    }

    /**
     * @param $order_id
     * @param $is_user
     * @param Request $request
     * @return mixed
     */
    public function stopped($order_id, $is_user, Request $request)
    {
        $order = Orders::find($order_id);

        $elements = $order->elements()->has('performOrder')->get();
        foreach ($elements as $element) {
            // В perform order есть элементы из других заказов
            $elementsFromOtherOrdersCount = $element->performOrder->elements()
                ->where('order_id', '!=', $order->id)
                ->count();

            if ($elementsFromOtherOrdersCount) {
                session()->flash('message.danger', "Заказ №{$order->id} уже в работе вместе с другими заказами, приостановить его не возможно.");

                return redirect()->back();
            }
        }

        $stopped_state_id = 'stopped_state_' . $order->id;
        $order->stopped(intval($request->$stopped_state_id), $request->stopped_reason);
        $order->stopped_from = $request->input('stopped_from');
        $order->save();

        if ((Auth::user()->accessLevel() == 1) && ($request->$stopped_state_id == 1000)) {
            return Redirect::to('/orders/need_attention');
        } else {
            return Redirect::back()->with('selected_group_order_id', $order->group_id);
        }
    }

    public function changePsd($order_id, Request $request)
    {
        $order = Orders::find($order_id);
        if (!isset($order->stopped_user_id) || ($order->stopped_user_id == auth()->user()->id)) {
            if ($order->creator == 4) {
                $order->stopped(1000, 'Замена макета');
            } else {
                $order->stopped(1000, 'Изменение макета');
            }
        }
        $order->workWithTemplate(1, 1);
        if ($order->template_library_id == null) {
            return Redirect::to('/loading/' . rand(1000, 9999) . $order->id . rand(1000, 9999));
        } else {
            return Redirect::to('/orders/edit/' . $order->id);
        }
    }

    public function getPriceTableForProduct($product_id, $client_id, $send_invoice_button = null)
    {
        $price_discount = $this->getDiscount($client_id);
        $client_addresses = Clients::find($client_id)->contacts()->first()->clients()->get()->pluck('address');
        $issuing_addresses = settings('issuingAddresses');
        if ($product_id != 999999) {
            $product_info = ProductType::find($product_id);
            $prices = json_decode($product_info->services);
            if (!is_object($prices)) {
                $prices = json_decode('{}');
            }
            foreach ($prices as $group_key => $group) {
                if ($group_key != 'counts') {
                    if ($product_info->printler_type == 3) {
                        foreach ($group->covers_prices as $price_item_key => $price_item) {
                            foreach ($price_item as $price_jtem_key => $price_jtem) {
                                $price_jtem = str_replace(',', '.', $price_jtem);
                                $prices->$group_key->covers_prices->$price_item_key->$price_jtem_key = round($price_jtem * $price_discount, 2);
                            }
                        }
                        if ($group->type == 'на пружине') {
                            if (isset($group->pads_prices)) {
                                foreach ($group->pads_prices as $price_item_key => $price_item) {
                                    foreach ($price_item as $price_jtem_key => $price_jtem) {
                                        $price_jtem = str_replace(',', '.', $price_jtem);
                                        $prices->$group_key->pads_prices->$price_item_key->$price_jtem_key = round($price_jtem * $price_discount, 2);
                                    }
                                }
                            }
                        }
                    }
                    foreach ($group->products_prices as $price_item_key => $price_item) {
                        $price_item = str_replace(',', '.', $price_item);
                        $prices->$group_key->products_prices[$price_item_key] = round($price_item * $price_discount, 2);
                    }
                    foreach ($group->options_prices as $price_item_key => $price_item) {
                        $price_item = str_replace(',', '.', $price_item);
                        $prices->$group_key->options_prices[$price_item_key] = round($price_item * $price_discount, 2);
                    }
                }
            }
            $prices->client_id = $client_id;
            $prices->send_invoice = $send_invoice_button;
            $prices->printler_type = $product_info->printler_type;
            $prices->type_name = $product_info->name;
            $prices->urgency_table = Urgency::where('subdomain_id', null)->get();
            $prices->delivery_types = DeliveryTypes::all();
            $prices->payment_types = PaymentTypes::get(['id','key_1s','name','discount']);
            $prices->client_addresses = $client_addresses;
            $prices->issuing_addresses = $issuing_addresses;
            if ($send_invoice_button == 1) {
                return view('orders.priceForm_new', [
                    'type_id' => $product_id,
                    'company_id' => $client_id,
                    'prices' => $prices,
                    'selected_size' => '',
                    'selected_type' => '',
                    'selected_cover' => '',
                    'selected_cover_chromacity' => '',
                    'selected_pad' => '',
                    'selected_pad_chromacity' => '',
                    'selected_material' => '',
                    'selected_chromacity' => '',
                    'selected_product' => '',
                    'selected_options' => [],
                    'selected_urgency' => '',
                    'selected_prepress' => '',
                    'selected_delivery_type' => '{}',
                    'selected_payment_type' => '{}',
                    'complects_calculation_type' => 0,
                    'complects_discount_percent' => 0,
                    'selected_address' => '',
                    'price_index' => '',
                    'price_count' => '',
                    'price' => '',
                    'draw' => '',
                    'price' => '',
                    'template_cost' => '0',
                    'close_date' => '',
                    'step1' => '1',
                    'prepress_in_one_line' => 1,
                ])->render();
            } else {
                return ($prices);
            }
        } else {
            return view('orders.priceForm_manual', array_merge([
                'type_id' => $product_id,
                'company_id' => $client_id,
                'client_addresses' => $client_addresses,
                'issuing_addresses' => $issuing_addresses],
                $this->getManualData()))->render();
        }
    }

    function getManualData()
    {
        $products = ProductType::where('activity', 1)->get(['name', 'printler_type', 'services']);
        $product_type_names = new \stdClass();
        $product_sizes = new \stdClass();
        $product_names = new \stdClass();
        $product_materials = new \stdClass();
        $product_options = new \stdClass();
        $product_covers = new \stdClass();
        $product_pads = new \stdClass();
        foreach ($products as $product) {
            $printler_type = $product->printler_type;
            if (!isset($product_type_names->$printler_type)) {
                $product_type_names->$printler_type = [];
                $product_sizes->$printler_type = [];
                $product_names->$printler_type = [];
                $product_options->$printler_type = [];
                if ($printler_type == 3) {
                    $product_materials->$printler_type = [];
                    $product_covers->$printler_type = [];
                    $product_pads->$printler_type = [];
                }
            }
            array_push($product_type_names->$printler_type, $product->name);

            if (strlen($product->services) > 0) {
                $services = json_decode($product->services);
                foreach ($services as $key => $service_item) {
                    if ($key != 'counts') {
                        if (!in_array($service_item->size, $product_sizes->$printler_type)) {
                            array_push($product_sizes->$printler_type, $service_item->size);
                        }

                        foreach ($service_item->products as $product) {
                            if (!in_array($product, $product_names->$printler_type)) {
                                array_push($product_names->$printler_type, $product);
                            }
                        }

                        foreach ($service_item->options as $option) {
                            if (!in_array(trim($option), $product_options->$printler_type)) {
                                array_push($product_options->$printler_type, trim($option));
                            }
                        }

                        if ($printler_type == 3) {
                            if (!in_array($service_item->material, $product_materials->$printler_type)) {
                                array_push($product_materials->$printler_type, $service_item->material);
                            }
                            foreach ($service_item->covers as $cover) {
                                if ($cover != 'Без обложки') {
                                    if (!in_array($cover, $product_materials->$printler_type)) {
                                        array_push($product_materials->$printler_type, $cover);
                                    }
                                }
                            }
                            if (isset($service_item->pads)) {
                                foreach ($service_item->pads as $pad) {
                                    if ($pad != 'Без подложки') {
                                        if (!in_array($pad, $product_materials->$printler_type)) {
                                            array_push($product_materials->$printler_type, $pad);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $data = [
            'product_type_names' => $product_type_names,
            'product_sizes' => $product_sizes,
            'product_names' => $product_names,
            'product_options' => $product_options,
            'product_materials' => $product_materials,
            'product_covers' => $product_covers,
            'product_pads' => $product_pads,
            'urgency' => Urgency::where('subdomain_id', null)->first()->name,
            'delivery_types' => DeliveryTypes::all(),
            'payment_types' => PaymentTypes::get(['id','key_1s','name','discount']),
            'selected_printler_type' => 0,
            'selected_product_type_name' => '',
            'selected_size' => '',
            'selected_product' => '',
            'selected_material' => '',
            'selected_chromacity' => '',
            'selected_type' => '',
            'selected_cover' => '',
            'selected_cover_chromacity' => '',
            'selected_pad' => '',
            'selected_pad_chromacity' => '',
            'selected_options' => [],
            'selected_options_prices' => [],
            'selected_urgency' => '',
            'selected_prepress' => '',
            'selected_delivery_type' => '{}',
            'selected_payment_type' => '{}',
            'selected_address' => '',
            'price' => '',
            'template_cost' => '0',
            'draw' => '',
            'close_date' => '',
            'step1' => '1',
            'template_chromacity' => view('templates.chromacity'),
            'template_paper_clip_chromacity' => view('templates.chromacity_paper_clip'),
            'prepress_in_one_line' => 1,
        ];
        return $data;
    }


    public function getDiscount($company_id)
    {
        $discount = 0;
        if ($company_id) {
            $client_type = Clients::where('id', $company_id)->get(['type_id']);
            $client_type = $client_type[0]->type_id;
            $discount = ClientTypes::where('id', $client_type)->get(['discount'])[0]->discount;
            if ($discount > 100) {
                $discount = $discount / 100;
            }
        }
        return (100 - $discount) / 100;
    }

    /**
     * Отправить счет
     */
    public function sendInvoice(Orders $order, $fill_calculation = null)
    {
        $client = $order->client()->first();
        $manager = $client->manager()->first();

        $pdf_link = substr((new GroupOrdersController)->getGroupOrderPDF($order->group->id, null), 1);

        $group = $order->group;
        $group->status = 1; // Выставлен счёт
        $group->save();

        $company_name = settings('email_title') ? settings('email_title') : settings('company_name');
        $data = array(
            'subject' => 'Счет на оплату от ' . $company_name,
            'to' => $client->email,
            'email' => $manager->contact ? $manager->contact->email : $manager->email,
            'client' => $client,
            'url' => '',
            'pdf_link' => $pdf_link,
            'order_id' => $order->id,
            'manager' => $manager,
            'login_token' => $client->account->login_token,
        );

        return \Mail::send('emails.new_invoice', $data, function ($message) use ($data) {
            $message->attach($data['pdf_link'], array('as' => 'invoice_' . $data['order_id'] . '.pdf', 'mime' => 'application/pdf'));
            $message->subject($data['subject'])
                ->to($data['to'])
                ->from($data['email'], $_SERVER["SERVER_NAME"] . ' - Ваш счет');

        });
    }
}
