<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\Emails\CreatePasswordEmail;
use App\PasswordReset;
use App\Urgency;
use DB;
use Exception;
use Mail;
use App\Clients;
use App\ClientTypes;
use App\Subdomain;
use App\User;
use App\UserGroup;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $partners = Clients::with([
            'account.subdomain'
        ])->where('type_id', ClientTypes::PARTNER)->latest()->paginate(20);

        $page = array(
            'menu' => 'contractors',
            'submenu' => 'partners',
            'breadcrumbs' => ['CRM', 'Создание партнёра']
        );

        return view('contractors.partners.index', compact('partners', 'page'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $subdomain_id = \App\Subdomain::findCurrentId() ?: 'NULL';

        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'subdomain' => 'required|subdomain|unique:subdomains,name',
            'email' => 'required|email',
            'phone' => 'required',
            //'email' => 'required|unique:contacts,email|unique:users,email',
        ]);

        $env = app('subdomain-environment');

        try {
            DB::transaction(function () use ($request, $env) {
                $email = $request->input('email');
                $firstName = $request->input('name');
                $lastName = $request->input('last_name');
                $password = str_random(8);

                // Create new subdomain
                $subdomain = new Subdomain();
                $subdomain->name = $request->input('subdomain');
                $subdomain->save();

                // Create account for new user
                $user = new User();
                $user->email = $email;
                $user->password = bcrypt($password);
                $user->id_group = UserGroup::PARTNER;
                $user->save();

                // Associate user with new subdomain
                $user->subdomain()->associate($subdomain);
                $user->save();

                // Create contact for the user
                $contact = $user->contact()->create([
                    'name' => $firstName,
                    'last_name' => $lastName,
                    'email' => $email,
                    'phone' => $request->input('phone'),
                    'description' => $request->input('comment'),
                    'position' => $request->input('position'),
                ]);

                // make client-partner
                $partner = new Clients();
                $partner->name = $request->input('company_name');
                $partner->company_name = $request->input('company_name');
                $partner->tm_name = $request->input('company_name');
                $partner->manager_id = auth()->user()->id;
                $partner->user_id = $user->id;
                $partner->is_company = true;
                $partner->type_id = ClientTypes::PARTNER;
                $partner->status_id = 1; // без предоплаты
                $partner->email = $email;
                $partner->save();

                // Associate client with contact
                $contact->clients()->attach($partner->id);
                $contact->save();

                $contact->setInformationFieldForClient($partner->id, 'position', $request->input('position'));

                // add new environment for subdomain
                $env->writeConfig($request->input('subdomain'), $env->getConfig());

                // copy settings from parent and add to created subdomain
                settings()->replicateTo($request->input('subdomain'));

                // Add default client types for created subdomain
                ClientTypes::insert(ClientTypes::getDefaultTypes($subdomain->id));

                // Add default urgency for created subdomain
                Urgency::insert(Urgency::getDefaultUrgency($subdomain->id));

                // create token
                $reset = PasswordReset::createAuto($user);

                // Send account information to the client email
                $link = replaceSubdomain(route('auth.password.create', $reset->token), $request->input('subdomain'));
                $data = [
                    'user' => $user,
                    'token' => $reset->token,
                    'link' => $link,
                ];
                (new CreatePasswordEmail)->send($user, $data);
            });

        // @todo разобраться. Т.к. в исходном коде метода транзакции в ларавел обрабатывается ещё и новое исключение Trowable, которое было введено в php 7+, отработает ли \Exception
        } catch (\Exception $exception) {
            $env->removeConfig($request->input('subdomain'));
            session()->flash('message.danger', 'Ошибка при создании партнёра.');
        }

        return redirect()->route('partner.index');
    }

    /**
     * Show making partner page from client
     *
     * @param Clients $client
     * @param Contacts $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMakePartner(Clients $client, Contacts $contact)
    {
        $page = [
            'menu' => 'crm',
            'submenu' => 'all',
            'breadcrumbs' => ['CRM', 'Сделать партнёром']
        ];

        return view('contractors.clients.make-partner', compact('client', 'contact', 'page'));
    }

    /**
     * Преобразовывает посредника в партнёра
     *
     * @param Clients $client
     * @param Contacts $contact
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postMakePartner(Clients $client, Contacts $contact, Request $request)
    {
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'subdomain' => 'required|subdomain|unique:subdomains,name',
            'email' => 'required|unique:users,email,' . $client->account->id . ',id,subdomain_id,' . $subdomain_id,
            //'email' => 'required|unique:contacts,email,' . $client->contacts->first()->id . '|unique:users,email,' . $client->account->id
        ]);

        $email = $request->input('email');
        $firstName = $request->input('name');
        $lastName = $request->input('last_name');
        $password = str_random(8);
        $user = $client->account;

        // Create new subdomain
        $subdomain = new Subdomain();
        $subdomain->name = $request->input('subdomain');
        $subdomain->save();

        // Create new account for partner
        $newUser = $user->replicate();
        $newUser->id_group = UserGroup::PARTNER;
        $newUser->email = $email;
        $newUser->password = bcrypt($password);
        $newUser->save();

        // Associate user with new subdomain
        $newUser->subdomain()->associate($subdomain);
        $newUser->save();

        // Create new contact
        $contact = $newUser->contact()->create([
            'name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'phone' => $request->input('phone'),
            'description' => $request->input('comment'),
            'position' => $request->input('position'),
        ]);

        // make client-partner
        $partner = new Clients();
        $partner->name = $request->input('company_name');
        $partner->company_name = $request->input('company_name');
        $partner->tm_name = $request->input('company_name');
        $partner->manager_id = auth()->user()->id;
        $partner->user_id = $newUser->id;
        $partner->is_company = true;
        $partner->type_id = ClientTypes::PARTNER;
        $partner->status_id = 1; // без предоплаты
        $partner->email = $email;
        $partner->save();

        // Связь старого клиента посредника с новым клиентом партнёром
        $client->new_partner_id = $partner->id;
        $client->save();

        // Associate client with contact
        $contact->clients()->attach($partner->id);
        $contact->save();

        $newUser->contact->setInformationFieldForClient($partner->id, 'position', $request->input('position'));

        // add new environment for the subdomain
        $env = app('subdomain-environment');
        $env->writeConfig($request->input('subdomain'), $env->getConfig());

        // replicate settings for the new domain
        settings()->replicateTo($request->input('subdomain'));

        // Add default client types for created subdomain
        ClientTypes::insert(ClientTypes::getDefaultTypes($subdomain->id));

        // Add default urgency for created subdomain
        Urgency::insert(Urgency::getDefaultUrgency($subdomain->id));

        // Send account information to the client email
        $link = replaceSubdomain(url('auth/login'), $request->input('subdomain'));

        Mail::send(
            'emails.partner-account',
            compact('client', 'link', 'password', 'contact'),
            function ($message) use ($user) {
                $message->to($user->email);
                $message->subject(settings('company_name') . ": Создание аккаунта партнёра");
            }
        );

        session()->flash('error_msg', 'Клиент преобразован в партнёра');

        return redirect()->route('client.show', $partner->id);
    }
}
