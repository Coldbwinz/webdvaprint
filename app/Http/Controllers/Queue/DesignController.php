<?php

namespace App\Http\Controllers\Queue;

use App\Http\Controllers\Controller;

class DesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'menu' => 'design',
            'submenu' => null,
            'breadcrumbs' => ['Очередь дизайна', '']
        ];

        return view('queue.design', compact('page'));
    }
}
