<?php

namespace App\Http\Controllers\Queue;

use App\PerformOrder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    /**
     * @var PerformOrder
     */
    private $performOrder;

    /**
     * @param PerformOrder $performOrder
     */
    public function __construct(PerformOrder $performOrder)
    {
        $this->performOrder = $performOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'menu' => 'print',
            'submenu' => null,
            'breadcrumbs' => ['Очередь печати', '']
        ];

        return view('queue.print', compact('page'));
    }
}
