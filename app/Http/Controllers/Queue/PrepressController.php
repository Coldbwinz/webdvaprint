<?php

namespace App\Http\Controllers\Queue;

use App\Orders;
use App\Http\Controllers\Controller;

class PrepressController extends Controller
{
    /**
     * @var Orders
     */
    private $order;

    /**
     * @param Orders $order
     */
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = $this->order->all();

        $page = [
            'menu' => 'prepress',
            'submenu' => 'queue',
            'breadcrumbs' => ['Препресс - очередь ', '']
        ];

        return view('queue.prepress-multipage', compact('page', 'orders'));
    }
}
