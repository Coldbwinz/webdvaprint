<?php

namespace App\Http\Controllers\Queue;

use App\PerformOrder;
use App\PerformOrderStatus;
use App\Http\Controllers\Controller;

class PostpressController extends Controller
{
    /**
     * @var PerformOrder
     */
    private $performOrder;

    /**
     * @param PerformOrder $performOrder
     */
    public function __construct(PerformOrder $performOrder)
    {
        $this->performOrder = $performOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'menu' => 'postpress',
            'submenu' => null,
            'breadcrumbs' => ['Постпресс - очередь ', '']
        ];

        return view('queue.postpress', compact('page'));
    }
}
