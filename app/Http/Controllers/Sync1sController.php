<?php

namespace App\Http\Controllers;

use App\ExportSetting;
use App\ProductType;
use App\Http\Requests;
use App\Subdomain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class Sync1sController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $productTypeId
     * @return \Illuminate\Http\Response
     */
    public function edit($productTypeId)
    {
        $productType = ProductType::findOrFail($productTypeId);

        $page = [
            'menu' => 'options',
            'submenu' => 'index',
            'breadcrumbs' => ['Продукция', 'Настройки 1С'],
        ];

        return view('sync1s.edit', compact('productType', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $productTypeId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $productTypeId)
    {
        $this->validate($request, [
            'product_1c_key' => 'required|max:100',
        ]);

        $productType = ProductType::findOrFail($productTypeId);

        $productType->update($request->all());

        return redirect('/products');
    }
}
