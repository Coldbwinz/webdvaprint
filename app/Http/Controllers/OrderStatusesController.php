<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderStatusesController extends Controller
{
    public function index()
    {
        $order_statuses = \DB::table('order_statuses')->paginate(20);

        $breadcrumbs[] = 'Статусы заказов';
        $breadcrumbs[] = '';
        $page = array(
            'menu' => 'options',
            'submenu' => 'order_statuses',
            'breadcrumbs' => $breadcrumbs
        );
        return view('order_statuses.index', [
            'page' => $page,
            'order_statuses' => $order_statuses,
        ]);
    }
}
