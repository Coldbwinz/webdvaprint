<?php

namespace App\Http\ViewComposers;

use App\Orders;
use App\Subdomain;
use App\GroupOrders;
use Illuminate\View\View;

class DashboardPanelComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $subdomain = Subdomain::findCurrent();

        $user = \Auth::user();

        $orderManagerCondition = function ($q) use ($user) {
            if (! $user->isAdmin() && ! $user->isRoot()) {
                $q->where('manager_id', $user->id);
            }
        };

        $status = [];
        $status['debt'] = 0;

        if ($subdomain) {
            // На согласовании
            $status['approval'] = Orders::whereHas('group.subdomain', function ($query) use ($subdomain) {
                $query->where('id', $subdomain->id);
            })->whereIn('status', [3, 5, 9, 10])->count();

            // Требуют внимания!
            $status['need_attention'] = Orders::whereHas('group.subdomain', function ($query) use ($subdomain) {
                $query->where('id', $subdomain->id);
            })->whereIn('status', [2, 4, 9, 12, 45, 55, 102, 112, 1000])->count();

            // Готов к выдаче
            $status['ready'] = Orders::whereHas('group.subdomain', function ($query) use ($subdomain) {
                $query->where('id', $subdomain->id);
            })->whereIn('status', [51])->count();

            $waitingPayment = GroupOrders::partnerWaitingPayment()->get();
            $status['waiting_payment'] = $waitingPayment->count();  // Ожидает оплаты
            $status['waiting_payment_total'] = $waitingPayment->map(function ($order) {
                // return $order->status === 7 ? $order->price - $order->pay : 0;
                return $order->price - $order->pay;
            })->sum();
        } else {
            // На согласовании
            $status['approval'] = Orders::onAgreement()->whereHas('group', function ($q) {
                $q->whereNull('subdomain_id');
            })->where($orderManagerCondition)->count();

            // Требуют внимания!
            $status['need_attention'] = Orders::requireAttention()->where($orderManagerCondition)->count();
            // Готов к выдаче
            $status['ready'] = Orders::ready()->where($orderManagerCondition)->count();

            $waitingPayment = GroupOrders::typographyWaitingPayment()->where($orderManagerCondition)->get();

            $status['waiting_payment'] = $waitingPayment->count();  // Ожидает оплаты
            $status['waiting_payment_total'] = $waitingPayment->map(function ($order) {
                // return $order->status === 7 ? $order->price - $order->pay : 0;
                return $order->price - $order->pay;
            })->sum();
        }

        $status['print_without_payment']
            = count(Orders::query()->where('status', 8)->where($orderManagerCondition)->get());  // Печать без предоплаты
        $status['in_work']
            = count(Orders::query()->where('status', 9)->where($orderManagerCondition)->get());  // В работе

        $view->with('status', $status);
    }
}
