<?php
namespace App\Http\ViewComposers;

use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class GlobalComposer {

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::user()) {
            if (Auth::user()->id_manager > 0) {
                $view->with('manager', User::find(Auth::user()->id_manager));
            }
        }
    }

}