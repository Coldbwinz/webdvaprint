<?php

namespace App\Http\Requests;

use App\Subdomain;

class SettingsUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Если типография, то адрес выдачи обязателен
        $requirePrintingAddress = is_null(Subdomain::findCurrent());

        $printingAddressesValidationRules = [];

        if ($requirePrintingAddress) {
            $printingAddressesValidationRules[] = 'required';
        }

        return [
            'company_name' => 'required|min:3|max:50',
            'company_short_name' => 'min:2|max:50',
            'user_timezone' => 'required',
            'country' => 'required',
            'color_primary' => '',
            'color_secondary' => '',
            'issuingAddresses' => 'required_without:printingAddresses',
            'printingAddresses' => implode('|', $printingAddressesValidationRules),
            'logo' => 'required',
            'invoice_number' => 'required|numeric|min:' . settings('invoice_number'),
            'act_number' => 'required|numeric|min:' . settings('act_number')
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'company_name' => 'Название компании',
            'issuingAddresses' => 'Адреса выдачи',
            'printingAddresses' => 'Адреса типографии',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'issuingAddresses.required' => 'Необходимо ввести хотя бы один адрес.',
            'printingAddresses.required' => 'Необходимо ввести хотя бы один адрес.',
            'logo.required' => 'Вы не выбрали логотип',
        ];
    }
}
