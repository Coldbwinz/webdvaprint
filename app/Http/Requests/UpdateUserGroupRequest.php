<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_clients_count_plan' => 'required',
            'repeated_orders_count_plan' => 'required',
            'invoices_total_plan' => 'required',
            'paid_invoices_total_plan' => 'required',
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'new_clients_count_plan' => 'Новые клиенты',
            'repeated_orders_count_plan' => 'Повторные заказы',
            'invoices_total_plan' => 'Выставленые счета',
            'paid_invoices_total_plan' => 'Поступившие платежи',

            'standard_salary' => 'Стандартный оклад',
            'increased_salary' => 'Повышенный оклад',
            'standard_premium' => 'Стандартная премия',
            'increased_premium' => 'Повышенная премия',
        ];
    }
}
