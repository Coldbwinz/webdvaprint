<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadTemplateForApprovalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sides' => 'required',
            'firstSide' => 'required|mimes:psd',
            'secondSide' => 'required_if:sides,2|mimes:psd'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'firstSide' => 'PSD макет первой стороны',
            'secondSide' => 'PSD макет второй стороны',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstSide.required' => 'Необходимо загрузить :attribute',
            'secondSide.required_if' => 'Необходимо загрузить :attribute',
        ];
    }
}
