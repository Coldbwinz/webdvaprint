<?php

namespace App\Http\Requests\Auth;

use App\Subdomain;
use App\Http\Requests\Request;

class ForgotPasswordSendRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $subdomain_id = Subdomain::findCurrentId() ?: 'NULL';

        return [
            'email' => 'required|email|exists:users,email,subdomain_id,' . $subdomain_id,
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => 'Электронная почта',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.exists' => 'Такой пользователь не зарегистрирован',
        ];
    }

}
