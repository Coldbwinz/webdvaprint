<?php

namespace App\Http\Requests\Dictionary;

use App\Http\Requests\Request;

class EquipmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'print_type_id' => 'required',
            'equipment_name' => 'required',
            'max_print_sheet_format.width' => 'required|numeric',
            'max_print_sheet_format.height' => 'required|numeric',
            'technological_fields' => 'required|numeric',
            'valve.value' => 'required|numeric',
            'valve.side' => 'required|in:wide,narrow',
            'print_color_panels' => 'required',
            //'banner_print_panel' => 'required',
            'banner_print_enabled' => 'required',
        ];
    }
}
