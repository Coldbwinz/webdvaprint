<?php

namespace App\Http\Requests\Dictionary;

use App\Http\Requests\Request;

class MaterialRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'material_type_id' => 'required|exists:material_types,id',
            'cover_type_id' => 'required|exists:cover_types,id',
            'name' => 'required|max:255',
            'price' => 'required',
            'initial_sheet_format' => 'required',
            'print_sheet_format' => 'required',
            'print_sheets_number' => 'required|numeric|min:1',
            'density_items' => 'required',
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'material_type_id' => 'Тип материала',
            'cover_type_id' => 'Тип покрытия',
            'name' => 'Название',
            'print_sheet_number' => 'Раскрой',
            'density_items' => 'Плотность',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'density_items.required' => 'Необходимо добавить хотябы одну плотность'
        ];
    }
}
