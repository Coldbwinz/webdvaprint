<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response;

class ParserFileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pk' => 'required|exists:parser_files,id',
            'name' => 'required',
            'value' => 'required|min:3,max:50',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(new Response(
            $validator->getMessageBag()->first(), Response::HTTP_UNPROCESSABLE_ENTITY
        ));
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'value.required' => 'Введите значение',
        ];
    }
}
