<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DetailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $country = settings('country');

        // russia
        $innLength = 10;

        if ($country === 'byelorussia') {
            $innLength = 12;
        }

        return [
            'name' => 'required',
            'post_address' => 'required|digits:6',
            'address' => 'required',
            'bank_rs' => 'required|digits:20',
            'bank_name' => 'required',
            'bank_bik' => 'required|digits:9',
            'bank_ks' => 'required|digits:20',
            'bank_inn' => "required|digits:{$innLength}",
            'bank_kpp' => 'required|digits:9',
            'director' => 'required',
        ];
    }
}
