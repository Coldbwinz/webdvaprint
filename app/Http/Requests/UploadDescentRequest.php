<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadDescentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link' => 'required_without:file',
            'file' => 'required_without:link',
        ];
    }

    public function attributes()
    {
        return [
            'link' => 'Ссылка',
            'file' => 'Файл'
        ];
    }
}
