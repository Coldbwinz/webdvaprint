<?php

namespace App\Http\Requests;

use App\UserGroup;

class ContractorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->accessLevel() >= UserGroup::MANAGER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'phone' => 'required|max:30',
            'email' => 'required|email',
            'company_name' => 'required_if:is_company,1',
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'lastname' => 'Фамилия',
            'name' => 'Имя (отчество)',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'company_name' => 'Компания',
            'position' => 'Должность',
        ];
    }
}
