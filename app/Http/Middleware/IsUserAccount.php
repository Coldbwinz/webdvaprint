<?php

namespace App\Http\Middleware;

use App\Clients;
use App\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class IsUserAccount
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    protected $user;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->user = $auth->user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->user->accessLevel() < 1) {
            session()->flash('error_msg', 'This resource is restricted to Users!');
            return redirect('/home');
        }
        else {
            $client_info = Clients::query()->where('email', $this->user->email)->first();
            $manager = null;
            if ($this->user->id_manager > 0) {
                $manager = User::find($this->user->id_manager);
            }
            if (count($client_info) == 0) {
                return view("auth.not_found_client", ['manager' => $manager]);
            }
        }
        return $next($request);
    }
}
