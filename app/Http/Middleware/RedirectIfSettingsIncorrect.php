<?php

namespace App\Http\Middleware;

use App\ProductType;
use App\Services\SettingsValidator;
use Closure;
use App\Urgency;
use App\UserGroup;
use App\ClientTypes;

class RedirectIfSettingsIncorrect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! auth()->check()) {
            return $next($request);
        }

        // Для обычного пользователя-клиента не нужно запрашивать настройки
        if (auth()->user()->isClient()) {
            return $next($request);
        }

        if (SettingsValidator::hasCorrectSettings()) {
            if (auth()->user()->accessLevel() > 2) {
                $result = ProductType::issetTable();
                if (isset($result)) {
                    session()->flash('message.warning', 'Необходимо полностью заполнить параметры прайса данного продукта!');
                    return redirect($result->url)->with(['selected' => $result->selected]);
                }
            }
            return $next($request);
        }

        // TODO так делать не желательно. Переписать в соответствии с документацией.
        if (auth()->user()->accessLevel() > 2) {
            /* TODO:: убрано, потом понадобится (с) Андрей
             * if (! Urgency::issetTable()) {
                session()->flash('message.warning', 'Необходимо назначить наценки, скидки!');
                return redirect()->route('urgency.index');
            } */

            if (!ClientTypes::issetTable()) {
                session()->flash('message.warning', 'Необходимо назначить наценки, скидки!');
                return redirect()->route('client_types.index');
            }
        }

        session()->flash('message.warning', 'Необходимо заполнить реквизиты и добавить адрес выдачи!');

        return redirect()->route('settings.index');
    }
}
