<?php

namespace App\Http\Middleware;

use Closure;
use SubdomainEnvironment;

class RedirectIfSubdomainNotExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->environment() == 'local') {
            return $next($request);
        }

        $current = SubdomainEnvironment::currentSubdomain();

        if (SubdomainEnvironment::hasConfig($current) || SubdomainEnvironment::blacklisted($current)) {
            return $next($request);
        }

        return abort(404);
    }
}
