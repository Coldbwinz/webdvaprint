<?php
/**
 * @author Bogdan Odarchenko, cto@web2print.pro
 *
 */

/*
 * Авторизация и регистрация
 */
Route::get('/',                     'Auth\AuthController@getLogin')->name('home');
Route::get('auth/login/{email?}',   'Auth\AuthController@getLogin')->name('auth.login');
Route::post('auth/login',           'Auth\AuthController@postLogin')->name('auth.login');
Route::get('auth/logout/{email?}',  'Auth\AuthController@getLogout')->name('auth.logout');
Route::any('auth/login_token/{email}/{login_token}', 'UsersController@loginByToken')->name('auth.login_token');
Route::any('auth/login_token/{email}/{login_token}/approve/{order_id}', 'UsersController@loginByTokenApprove')->name('auth.login_token_approve');
Route::any('auth/login_token/{email}/{login_token}/1', 'UsersController@loginByTokenToPayments');

// Открытая регистрация ?
Route::get('auth/register',         'Auth\RegisterController@getRegister')->name('auth.register');;
Route::post('auth/register',        'Auth\RegisterController@postRegister')->name('auth.register');

Route::get('auth/password/restore',         'Auth\ForgotPasswordController@restore')->name('auth.password.restore');
Route::post('auth/password/restore',        'Auth\ForgotPasswordController@send')->name('auth.password.restore');
Route::get('auth/password/info/{token}',    'Auth\ForgotPasswordController@info')->name('auth.password.info');
Route::get('auth/password/create/{token}',     'Auth\ForgotPasswordController@createPassword')->name('auth.password.create');
Route::get('auth/password/new/{token}',     'Auth\ForgotPasswordController@newPassword')->name('auth.password.new');
Route::post('auth/password/new/{token}',    'Auth\ForgotPasswordController@store')->name('auth.password.store');

/*
 *  Cron
 */
Route::get('/cron/parser/search',   'CrontabController@parserSearch');
Route::get('/cron/parser/parse',    'CrontabController@parserParse');
Route::get('/cron/orders/make/pdf', 'CrontabController@ordersMakePdf');

/*
 * -----------------------------------------------
 *  AUTH USERS
 * -----------------------------------------------
 */

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', function() {
        if (\Auth::user()->accessLevel() == 1) {
            return redirect('/orders/user_orders/');
        } else {
            return redirect('/orders/history/');
        }
    });
    Route::get('/new-dashboard', 'Dashboard\DashboardController@index');

    /*
     * -----------------------------------------------
     *  ADMIN PANEL (root, admin, manager)
     * -----------------------------------------------
     */

    Route::group(['middleware' => ['is.manager']], function ()
    {
        // Dashboard
        Route::get('/dashboard',    'CrmController@dashboard');

        // Employees
        Route::get('contractors/employees', 'Contractors\EmployeeController@index')->name('contractors.employees');
        Route::post('contractors/employees/create', 'Contractors\EmployeeController@store')->name('contractors.employees.store');
        Route::get('contractors/employees/{user}/edit', 'Contractors\EmployeeController@edit')->name('contractors.employees.edit');
        
        Route::get('contractors/employee/{user}/salary', 'Contractors\EmployeeController@salary')->name('employee.salary');

        /*
         * Group orders
         */
        Route::any('/orders/history/{status?}', 'GroupOrdersController@getGroupOrders')->name('orders.history');

        /*
         * Справочник
         */
        Route::any('/template',                             'TemplateController@index');
        Route::get('/template/upload/{id}',                 'TemplateController@upload');
        Route::get('/template/parse/{id}',                  'TemplateController@parse');
        Route::get('/template/on/{id}',                     'TemplateController@on');
        Route::get('/template/off/{id}',                    'TemplateController@off');
        Route::get('/template/delete/{id}',                 'TemplateController@delete');
        Route::any('/template/new/{client_id}/{type_id}',   'TemplateController@create');
        Route::any('/template/store/{client_id}/{type_id}', 'TemplateController@store');
        Route::get('templates/organize', 'TemplateController@organize')->name('templates.organize');
        Route::patch('templates/organize', 'TemplateController@patchOrganize');
        Route::post('templates/organize/activity', 'TemplateController@collectionActivity');

        Route::any('/products',                         'ProductsController@index');
        Route::post('/products/new',                    'ProductsController@create');
        Route::get('/products/edit/{id}',               'ProductsController@edit');
        Route::post('/products/edit/{id}',              'ProductsController@update');
        Route::get('/products/price/{id}',              'ProductsController@price');
        Route::post('/products/price_save',             'ProductsController@priceSave');
        Route::get('/products/on/{id}',                 'ProductsController@on');
        Route::get('/products/off/{id}',                'ProductsController@off');

        Route::get('/products/types',               'ThemesController@index');
        Route::get('/products/types/delete/{id}',   'ThemesController@delete');
        Route::post('/products/types/new',          'ThemesController@create');
        Route::get('/products/types/edit/{id}',     'ThemesController@edit');
        Route::post('/products/types/edit/{id}',    'ThemesController@update');
        Route::get('/products/types/on/{id}',       'ThemesController@on');
        Route::get('/products/types/off/{id}',      'ThemesController@off');

        Route::get('/client_types',               'ClientTypesController@index')->name('client_types.index');
        Route::get('/client_types/delete/{id}',   'ClientTypesController@delete');
        Route::post('/client_types/new',          'ClientTypesController@create');
        Route::get('/client_types/edit/{id}',     'ClientTypesController@edit');
        Route::post('/client_types/edit/{id}',    'ClientTypesController@update');

        Route::get('/urgency',               'UrgencyController@index')->name('urgency.index');
        Route::get('/urgency/delete/{id}',   'UrgencyController@delete');
        Route::post('/urgency/new',          'UrgencyController@create');
        Route::get('/urgency/edit/{id}',     'UrgencyController@edit');
        Route::post('/urgency/edit/{id}',    'UrgencyController@update');

        Route::get('/payment_types',               'PaymentTypesController@index')->name('payment_types.index');
        Route::get('/payment_types/delete/{id}',   'PaymentTypesController@delete');
        Route::post('/payment_types/new',          'PaymentTypesController@create');
        Route::get('/payment_types/edit/{id}',     'PaymentTypesController@edit');
        Route::post('/payment_types/edit/{id}',    'PaymentTypesController@update');


        Route::get('/delivery_types',              'DeliveryTypesController@index')->name('delivery_types.index');
        Route::get('/delivery_type/delete/{id}',   'DeliveryTypesController@delete');
        Route::post('/delivery_type/new',          'DeliveryTypesController@create');
        Route::get('/delivery_type/edit/{id}',     'DeliveryTypesController@edit');
        Route::post('/delivery_type/edit/{id}',    'DeliveryTypesController@update');
        /*
         * options details
         */
        Route::post('/options/details/new',         'DetailsController@create');
        Route::get('/options/details/edit/{id}',    'DetailsController@edit');
        Route::post('/options/details/edit/{id}',   'DetailsController@update');
        Route::get('/options/details/on/{id}',      'DetailsController@on');
        Route::get('/options/details/off/{id}',     'DetailsController@off');
        Route::post('options/details', 'DetailsController@store')->name('details.store');

        /*
        * options parameters
        */
        Route::post('options/logo', 'ParametersController@logo')->name('settings.logo');
        Route::any('/options/get_inn_data', 'ParametersController@getINNdata');
        Route::any('/options/get_bik_data', 'ParametersController@getBIKdata');

        Route::get('/orders/turn_orders/{filters?}', 'OrdersController@turnOrders');

        /*
         * widgets
         */
        Route::get('/widgets/',                 'WidgetController@index')->name('widgets');
        Route::post('/widget/registration/save', 'WidgetController@saveRegistrationSettings')->name('widget.registration.save');

        // Contractors
        Route::any('contractors/clients', 'Contractors\ClientController@index')->name('contractors.clients');
        Route::resource('contractors', 'Contractors\ContractorsController', [
            'as' => 'contractors'
        ]);

        // intermediary
        Route::resource('intermediary', 'Contractors\IntermediaryController');

        /*
         * Clients
         */
        Route::get('/crm/{filters?}', 'Contractors\ClientController@index');
        Route::get('/crm_ajax/{filters?}', 'Contractors\ClientController@getFilteredClients');
        Route::get('/client/new', 'Contractors\ClientController@create')->name('client.new');
        Route::post('/client/new', 'Contractors\ClientController@store')->name('client.new');
        Route::get('/client/show/{client}', 'Contractors\ClientController@show')->name('client.show');
        Route::post('/client/save/{client}', 'Contractors\ClientController@update')->name('client.update');
        Route::post('/client/save/description/{client}', 'Contractors\ClientController@saveDescription')->name('client.save.description');
        Route::post('/client/saveContact/{contact}', 'Contractors\ClientController@saveContact')->name('client.save.contact');
        Route::post('/client/saveClient/{client}', 'Contractors\ClientController@saveClient')->name('client.save.client');
        Route::post('/client/saveDetails/{client}', 'Contractors\ClientController@saveDetails')->name('client.save.details');
        Route::get('/client/new_contact/{client}', 'Contractors\ClientController@newContact');
        Route::post('/client/new_contact', 'Contractors\ClientController@storeContact');
        Route::get('/client/new_client/{contact}/{client}', 'Contractors\ClientController@newClient');
        Route::post('/client/new_client', 'Contractors\ClientController@storeClient');
        Route::post('/client/getClient/{client}', 'Contractors\ClientController@getClient');
        Route::post('/client/uploadFile', 'Contractors\ClientController@uploadFile');
        Route::post('/client/deleteFile/{file}', 'Contractors\ClientController@deleteFile');
        Route::get('/client/change_manager_ajax/{client}/{manager_id}', 'Contractors\ClientController@changeManagerAjax');

        Route::any('/contact/show/{contact}/{company_id?}', 'Contractors\ClientController@showContacts')->name('contact.show');
        //Route::get('/contact/show/{contact}', 'Contractors\ClientController@showContacts');
    });

    /*
     * -----------------------------------------------
     *  IS ADMIN (admin, root)
     * -----------------------------------------------
     */

    Route::group(['middleware' => ['is.admin']], function () {
        /*
         * Пользователи
         */
        Route::post('/users/new', 'UsersController@create');
        Route::get('/users/edit/{id}', 'UsersController@edit')->name('user.edit');
        Route::post('/users/edit/{id}', 'UsersController@update');
        Route::get('/users/on/{id}', 'UsersController@on');
        Route::get('/users/off/{id}', 'UsersController@off');

        /*
         * Partners
         */
        Route::get('/client/{client}/{contact}/partner', 'PartnerController@getMakePartner');
        Route::post('/client/{client}/{contact}/partner', 'PartnerController@postMakePartner');

        Route::get('partners', 'PartnerController@index')->name('partner.index');
        Route::post('partners', 'PartnerController@store')->name('partner.store');

        /*
         * Turn Orders
         */
        Route::get('/orders/turn_orders/{filters?}', 'OrdersController@turnOrders');
        Route::get('/options/parameters', 'ParametersController@index')->name('settings.index');
        Route::patch('options/parameters', 'ParametersController@update')->name('settings.update');

        /**
         * Sync 1s
         */
        Route::get('products/sync1s/{id}', 'Sync1sController@edit')->name('sync1s.edit');
        Route::post('products/sync1s/{id}', 'Sync1sController@update')->name('sync1s.update');
    });

    /*
     * -----------------------------------------------
     *  ONLY ROOT
     * -----------------------------------------------
     */

    Route::group(['middleware' => ['only.root']], function () {

        Route::get('/client_statuses',            'ClientStatusesController@index');
        Route::get('/order_statuses',             'OrderStatusesController@index');

        // Options
        Route::get('options', 'OptionController@index')->name('options');
        Route::get('options/on/{id}', 'OptionController@on')->name('options.on');
        Route::get('options/off/{id}', 'OptionController@off')->name('options.off');

        Route::any('/users', 'UsersController@index');
    });

    /*
     * -----------------------------------------------
     *  USER CABINET
     * -----------------------------------------------
     */

    Route::group(['middleware' => ['only.user']], function () {

        Route::group(['namespace' => 'Personal', 'as' => 'personal.'], function () {
            Route::any('/orders/need_attention', 'NeedAttentionController@index')->name('orders.need_attention');
            Route::get('/orders/need_attention_with_message', 'NeedAttentionController@index');
            Route::any('/orders/user_orders', 'UserOrdersController@index')->name('orders.user_orders');
            Route::any('orders/payments', 'PaymentController@index')->name('orders.payments');
            Route::post('orders/{group_order_id}/guarantee', 'PaymentController@guarantee')->name('payment.guarantee');

            Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
            Route::post('/profile/edit', 'ProfileController@update')->name('profile.edit');
        });
    });

    /*
     * -----------------------------------------------
     *  For user and partner
     * -----------------------------------------------
     */
    Route::group(['middleware' => ['only.partner']], function () {
        Route::get('/orders/archive/{filters?}', 'Personal\UserOrdersController@index')->name('orders.archive');
    });

    /*
     * -----------------------------------------------
     *  FOR ALL AUTH USERS
     * -----------------------------------------------
     */

    /*
      Set Data about client
    */
    Route::any('/options/set_all_data/{client_id}', 'ParametersController@setAllData')->name('client.set_all_data');

    /*
     * Orders
     */
    Route::any('/orders/new', 'OrdersController@newOrder');
    Route::get('/orders/new-step1/{company_id}', 'OrdersController@newStep1');
    Route::get('/orders/edit/{id}/{replicate?}', 'OrdersController@editorOpen');
    Route::any('/orders/post-step2/{order_id?}', 'OrdersController@postStep2');
    Route::any('/orders/post-step3', 'OrdersController@postStep3');
    Route::get('/orders/thanks_for_order', 'OrdersController@thanksForOrder')->name('orders.thanks_for_order');
    //Route::get('/order/good/{file}',                'OrdersController@good');
    Route::get('/order/status/{id}/{status}/{calculation?}', 'OrdersController@status');
    Route::get('/order/status/{id}/{status}/{calculation?}/{agree?}', 'OrdersController@status');
    Route::post('/order/post_status', 'OrdersController@postStatus');
    Route::post('/order/status_issue', 'OrdersController@statusIssue');
    Route::post('/order/set_check_control_quality', 'OrdersController@setCheckControlQuality');
    Route::post('/order/status_rework/{id}', 'OrdersController@statusRework');
    Route::post('/order/status_defect_return/{id}', 'OrdersController@statusDefectReturn');
    Route::post('/order/status_defect_close_with_discount/{id}', 'OrdersController@statusDefectCloseDiscount');
    Route::get('/order/set_status_id/{id}/{status}', 'OrdersController@set_status_id');
    Route::get('/order/template_agree/{order_id}/{template_id}/{type}/{new?}', 'OrdersController@templateAgree');
    Route::get('/order/only_invoice_send/{order_id}','OrdersController@sendOnlyInvoice');
    Route::get('/order/get_price/{product_id}/{client_id}/{view?}','OrdersController@getPriceTableForProduct');
    Route::get('/order/check_status/{order_id}/{order_status}', 'OrdersController@checkStatus');
    Route::get('/order/change_position/{order_id}/{delta}', 'OrdersController@changePosition');
    Route::get('/order/close_group_order/{client_id}', 'GroupOrdersController@closeGroupOrder');
    Route::get('/group_order/status/{id}/{status}', 'GroupOrdersController@status');
    Route::get('/group_order/check_guarantee/{id}', 'GroupOrdersController@checkGuarantee');
    Route::post('/group_order/send_to_issue', 'GroupOrdersController@sendToIssue');

    Route::get('order/accepted-on-issue/{order_id}', 'OrdersController@acceptedOnIssue');

    //upload specification
    Route::get('order/{order_id}/specification/create', 'Upload\SpecificationController@index')->name('order.specification.create');
    Route::get('order/{order_id}/specification/show/{version_id?}', 'Upload\SpecificationController@show')->name('order.specification.show');
    Route::post('order/{order_id}/specification/store/{assign_to_one}', 'Upload\SpecificationController@store')->name('order.specification.store');
    Route::post('order/{order_id}/specification/files/upload', 'Upload\SpecificationController@upload')->name('order.specification.files.upload');
    Route::post('order/{order_id}/specification/files/delete/{id}', 'Upload\SpecificationController@delete')->name('order.specification.files.delete');

    // upload layout
    Route::get('order/{order_id}/upload/layout/start', 'Upload\LayoutController@start')->name('order.upload.layout.start');
    Route::get('order/{order_id}/upload/layout/type/{type}', 'Upload\LayoutController@index')->name('order.upload.layout.type');
    Route::post('order/{order_id}/upload/layout/type/{type}/files/add', 'Upload\LayoutController@add')->name('order.upload.layout.files.add');
    Route::post('order/{order_id}/upload/layout/type/{type}/files/sort', 'Upload\LayoutController@sort')->name('order.upload.layout.files.sort');
    Route::post('order/{order_id}/upload/layout/type/{type}/files/delete/{id}', 'Upload\LayoutController@delete')->name('order.upload.layout.files.delete');
    Route::get('order/{order_id}/upload/layout/check-ready', 'Upload\LayoutController@checkReady')->name('order.upload.layout.check-ready');
    Route::any('order/{order_id}/upload/layout/finish', 'Upload\LayoutController@finish')->name('order.upload.layout.finish');

    /* Карточка заказа */
    Route::get('/order/info/{id}/{detail_id?}', 'GroupOrdersController@info')->name('order-group.show');
    Route::get('/order/tz/{id}', 'OrdersController@tz_card')->name('order.show');
    Route::get('/queue/tz/{id}', 'OrdersController@tz_queue');

    /*
     * Group orders
     */
    Route::get('/orders/calculation_pdf/{group_order_id}/{download_sign?}', 'GroupOrdersController@getGroupOrderPDF')->name('calculation_pdf');
    Route::post('orders/{group_order_id}/confirm', 'Personal\PaymentController@confirm')->name('personal.payment.confirm');
    Route::post('/orders/close_doc_load/{group_order_id}', 'GroupOrdersController@closeDocLoad');
    Route::get('/orders/close_doc_act_print/{group_order_id}/{all_sign?}', 'GroupOrdersController@closeDocActPrint');
    /*
     * Редактор

    Route::post('/editor/save/lib/frx/{name}', 'OrdersController@editorSave');
    */
    Route::get('/designer/edit/{id}',     'TemplateController@designer');

    /*
     *  Отправка в работу
     */
    Route::any('/order/inwork/{id}', 'OrdersController@in_work')->name('order.inwork');

    /*
     *  Приостановить, возобновить, отменить
     */
    Route::any('/order/stopped/{id}/{is_user}', 'OrdersController@stopped');

    /*
     *  Загрузить с правками / изменить макет
     */
    Route::any('/order/change_psd/{id}', 'OrdersController@changePsd');

    /*
     * Согласование
     */
    Route::get('/order/reviwer/{new_invoice}/{order_id}',                 'OrdersController@reviwer');
    Route::post('/order/reviwer/calculation',           'OrdersController@reviwerSave');
    Route::get('/order/reviwer/send/{id}',              'OrdersController@reviwerSend');
    Route::get('/order/reviwer/send/repeated/{group_id}',     'GroupOrdersController@reviewerSendRepeated');

    Route::get('/order/reorder/{id}', 'OrdersController@reorder');

    /*
     * Счета
     */
    Route::any('/invoice/close/{id}',           'OrdersController@invoiceClose');
    Route::get('/invoice/close-not-full/{id}',  'OrdersController@addNotFull');
    Route::get('/invoice/close-not-money/{id}', 'OrdersController@invoiceNotMoney');

    Route::post('/invoice/addNotFull/{id}',  'OrdersController@addNotFull');

    Route::get('/invoicenomoney/add/{id}',  'OrdersController@addInvoiceNoMoney');

    Route::post('invoice/addNotFullPartner/{group_order_id}', 'PartnerPaymentController@pay');
    Route::post('invoice/addFullPartner/{group_order_id}', 'PartnerPaymentController@payFull');

    /*
     * Parser
     */
    Route::get('/parser', 'ParserController@index')->name('parser.index');
    Route::patch('parser/{id}', 'ParserController@update')->name('parser.update');
    Route::get('parser/{id}/errors', 'ParserController@errors');
    Route::get('/parser/import', 'ParserController@import');
    Route::get('/parser/{id}',                  'ParserController@index');
    Route::post('/parser/library/merge',        'ParserController@merge');
    Route::post('/parser/hide/{id}',        'ParserController@hide');
    // ajax
    Route::post('/parser/library/move/{id}',    'ParserController@move');
    Route::post('/parser/library/massmove',     'ParserController@massMove');

    /*
     * Control panel root
     */
    // Route::get('/parser',                       'ControlController@index');


    /*
     * Card
     */
    Route::get('/control/code', 'ControlController@code');

    Route::get('/specification/for-order/{order_id}', 'Upload\SpecificationController@makeForOrder');
});

/*
 * -----------------------------------------------
 *  API
 * -----------------------------------------------
 */
Route::group(['prefix' => 'api', 'namespace' => 'Api', 'as' => 'api.'], function () {
    Route::any('contact/{contact}/update', 'ContactController@update')->name('contact.update');
    Route::post('contact/{contact}/avatar', 'ContactController@avatar')->name('contact.avatar');
    Route::patch('contact/{contact}/client{client}/update', 'ContactController@updateClientInformation')->name('contact.client.update');
    Route::post('/client/check_who_call', 'ContactController@checkWhoCall');
    Route::post('client/{client}/update', 'ClientController@update')->name('client.update');
    Route::post('client/{client}/contacts', 'ClientController@contacts')->name('client.contacts');
    Route::post('client/{client}/picture', 'ClientController@picture')->name('client.picture');
});

/*
 * -----------------------------------------------
 *  PUBLIC
 * -----------------------------------------------
 */

// Contractors interface
Route::any('contractors/order/{token}', 'Contractors\ContractorsController@orderPage')->name('contractor.order');
Route::any('contractors/order/{token}/download', 'Contractors\ContractorsController@downloadPdf')->name('contractor.order.download');

/*
 *  Public links
 */

Route::get('/user/reviwer/see/{token}',             'OrdersController@reviwerUserSee');
Route::get('/user/reviwer/see/{token}/{calculation?}',             'OrdersController@reviwerUserSee');
Route::get('/user/reviwer/status/{token}/{status}/{calculation?}', 'OrdersController@reviwerUserStatus');
Route::post('/user/reviwer/status/{client_id}', 'OrdersController@reviwerUserStatusCreateAccount');
Route::get('/user/reviwer/edit/{token}',            'OrdersController@reviwerUserEdit');

// При сохранение изменения макета с старницы конструктора (Менеджер)
Route::get('/order/good/{filename}', 'OrdersController@good');

/*
 *  Loading maket
 */

Route::get('/loading/{order_id}', 'OrdersController@loading');
Route::get('/loading/loaded/{order_id}', 'OrdersController@loaded');

/*
* module frame
*/

Route::get('/imodule/{company_id}', 'iModuleController@firstStep');
Route::get('/imodule/second/{company_id}/{type_id}/{param?}', 'iModuleController@secondStep');
Route::get('/imodule/second/{company_id}/{type_id}/{param?}/{calculation?}', 'iModuleController@secondStep');
Route::post('/imodule/second/{company_id}/{type_id}/{param?}', 'iModuleController@secondStepsavePrice');
Route::get('/imodule/third/{company_id}/{type_id}/{template_id}/{lib?}/{calculation?}', 'iModuleController@third');
Route::get('/imodule/edit/{id}', 'iModuleController@edit');
Route::get('/imodule/review/{uuid}','iModuleController@afterPrice');
Route::get('/imodule/get_order/{uuid}','iModuleController@getOrder');

Route::get('queue/prepress', 'Queue\PrepressController@index')->name('queue.prepress');
Route::get('queue/print', 'Queue\PrintController@index')->name('queue.print');
Route::get('queue/postpress', 'Queue\PostpressController@index')->name('queue.postpress');
Route::get('queue/design', 'Queue\DesignController@index')->name('queue.design');

Route::group(['prefix' => 'dictionary', 'namespace' => 'Dictionary'], function () {
    Route::get('material', 'MaterialController@index')->name('dictionary.material');
    Route::get('material/{material}/edit', 'MaterialController@edit')->name('dictionary.material.edit');
    Route::get('equipment', 'EquipmentController@index')->name('dictionary.equipment');
    Route::get('equipment/{equipment}/edit', 'EquipmentController@edit')->name('dictionary.equipment.edit');
});

Route::patch('order/apply-rework/{rework_order_id}', 'ReworkOrdersController@apply')->name('rework.apply');
Route::get('manager/motivation', 'Contractors\EmployeeController@motivation')->name('employee.motivation');


Route::get('designer/template/{template}/preview', 'Designer\TemplateController@preview')->name('template.preview');
Route::patch('designer/template/{template}/approve', 'Designer\TemplateController@approve')->name('template.approve');
Route::get('designer/template', 'Designer\TemplateController@index')->name('templates');
Route::get('designer/template/create', 'Designer\TemplateController@create')->name('template.create');
Route::post('designer/template', 'Designer\TemplateController@store')->name('template.store');
Route::delete('designer/template/{template}', 'Designer\TemplateController@destroy')->name('template.delete');
