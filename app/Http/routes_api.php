<?php

/**
 * Api routes
 */

Route::get('order', 'OrderController@index');

Route::group(['prefix' => 'perform-order'], function () {
    Route::get('/', 'PerformOrderController@index');
    Route::post('/', 'PerformOrderController@store');
    Route::post('{id}/upload-descent', 'PerformOrderController@uploadDescent');
    Route::get('get-next-index', 'PerformOrderController@getNextIndex');
    Route::get('{id}/download-descent', 'PerformOrderController@downloadDescent');
    Route::get('{id}/printed', 'PerformOrderController@printed');
    Route::patch('{id}/download-templates', 'PerformOrderController@downloadTemplates');
    Route::patch('{id}/set-ready', 'PerformOrderController@setReady');
    Route::patch('{id}/complete-postpress', 'PerformOrderController@postpressComplete');
    Route::patch('{id}/cut', 'PerformOrderController@cutProcess');

    Route::patch('{perform_order}/pause', 'PerformOrderController@pause');
    Route::patch('{perform_order}/resume', 'PerformOrderController@resume');
});

Route::get('prepress-queue/postpress', 'PrepressQueue\QueueController@postpress');
Route::get('prepress-queue/chromacity', 'PrepressQueue\QueueController@chromacity');

Route::get('design/group-order', 'Queue\DesignController@groupOrders');
Route::patch('design/order/{id}/working', 'Queue\DesignController@working');
Route::get('design/order/{id}/technical-task', 'Queue\DesignController@technicalTask');
Route::post('design/order/{id}/upload-for-approval', 'Queue\DesignController@uploadForApproval');
Route::post('design/order/{id}/upload-approved', 'Queue\DesignController@uploadApproved');

Route::get('order-elements', 'OrderElementController@index');
Route::get('order-elements/chromacity', 'OrderElementController@chromacity');

Route::get('perform-order/layout', 'PerformOrderController@layout');

Route::resource('sheet-format', 'SheetFormatController', ['only' => ['index', 'store']]);
Route::resource('material-types', 'MaterialTypeController', ['only' => ['index', 'store']]);
Route::resource('cover-types', 'CoverTypeController', ['only' => ['index', 'store']]);

Route::resource('materials', 'Dictionary\MaterialController');
Route::resource('equipments', 'Dictionary\EquipmentController');

Route::get('user', 'UserController@index');
Route::get('statistic', 'StatisticController@index');
Route::get('statistic-orders', 'StatisticController@orders');

Route::resource('employee', 'Contractors\EmployeeController', [
    'only' => ['show', 'update']
]);

Route::get('client-type', 'Contractors\ClientTypeController@index');

Route::get('user-role/{group}', 'UserGroupController@show')->name('user-role.show');
Route::patch('user-role/{group}', 'UserGroupController@update')->name('user-role.update');

Route::post('accrual/{employee}', 'Contractors\EmployeeController@addAccrual');

Route::get('/order-can-pause/{order}', 'OrderController@checkOrderCanPause');

Route::get('templates', 'TemplateController@index');
Route::get('templates-count', 'TemplateController@getCount');
