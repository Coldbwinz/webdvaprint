<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunicationType extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'display_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communications()
    {
        return $this->hasMany(Communication::class);
    }
}
