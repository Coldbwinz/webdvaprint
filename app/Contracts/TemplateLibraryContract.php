<?php

namespace App\Contracts;

interface TemplateLibraryContract
{
    /**
     * Фильтрует шаблоны по размеру
     *
     * @param $query
     * @param array $widthRange массив [мин, макс]
     * @param array $heightRange массив размеров [мин, макс]
     */
    public function scopeSizeInRange($query, $widthRange, $heightRange);
}
