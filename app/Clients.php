<?php

namespace App;

use App\Traits\SubdomainTrait;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use LocalizedEloquentDates, SubdomainTrait;

    protected $table = 'clients';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'company_name', 'phone', 'area', 'description', 'picture', 'tm_name',
        'last_name', 'address', 'vat', 'is_company', 'ownership_type', 'connections', 'legal_name',
        'subdomain', 'id_1s'
    ];

    /**
     * Attribute casting
     *
     * @var array
     */
    protected $casts = [
        'connections' => 'array',
        'company_details' => 'array',
        'is_company' => 'bool',
    ];

    /**
     * Manager of the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * Account of the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Check for assigned to company
     *
     * @return bool
     */
    public function isCompany()
    {
        return $this->is_company == true;
    }

    /**
     * Type of the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ClientTypes::class, 'type_id');
    }

    /**
     * Contacts of the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contacts::class,
            'contacts_clients_relation',
            'client_id',
            'contact_id'
        );
    }

    /**
     * Check for client has contacts
     *
     * @return bool
     */
    public function hasContacts()
    {
        return $this->contacts->count() > 0;
    }

    /**
     * Orders of the clients
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'client_id');
    }

    /**
     * Active orders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersActive()
    {
        $instance = $this->hasMany(Orders::class, 'client_id');

        $instance->getQuery()->InWork();

        return $instance;
    }

    /**
     * Check for orders of client
     *
     * @return bool
     */
    public function hasOrders()
    {
        return $this->orders && $this->orders->count();
    }

    /**
     * Status of the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(ClientStatuses::class, 'status_id');
    }

    /**
     * Means of communication for the client
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communications()
    {
        return $this->hasMany(Communication::class);
    }

    /**
     * Scope all company clients
     *
     * @param $query
     * @return mixed
     */
    public function scopeCompanies($query)
    {
        return $query->where('is_company', true);
    }

    /**
     * @param $value
     * @return string
     */
    public function getPictureAttribute($value)
    {
        return $value ?: config('w2p.company_picture_default');
    }

    /**
     * Группы заказов клиента
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupOrders()
    {
        return $this->hasMany(GroupOrders::class, 'client_id');
    }

    /**
     * Check for group orders of client
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasGroupOrders()
    {
        return $this->groupOrders && $this->groupOrders->count();
    }

    /**
     * Active group Orders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupOrdersActive()
    {
        $instance = $this->hasMany(GroupOrders::class, 'client_id');

        $instance->getQuery()->InWork();

        return $instance;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    /**
     * Пользовательские шаблоны клиента
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates()
    {
        return $this->hasMany(Template::class, 'client_id');
    }

    /**
     * @return string Представитель
     */
    public function representative() {
        if ($this->isCompany()) {
            if ($this->company_details) {
                if (isset($this->company_details['full_name']) && isset($this->company_details['inn'])) {
                    return $this->company_details['full_name'].' ИНН: '.$this->company_details['inn'];
                } else {
                    return $this->comownership_type . ' ' . $this->legal_name;
                }
            } else {
                return $this->tm_name;
            }
        } else {
            return $this->contactName();
        }
    }

    /**
     * @return string Фамилия имя контакта
     */
    public function contactName() {
        return $this->contacts()->first()->getFullNameAttribute();
    }

    /**
     * Scope client-partners
     *
     * @param $query
     * @return mixed
     */
    public function scopePartners($query)
    {
        return $query->where('type_id', ClientTypes::PARTNER);
    }

    public function getRelationClients()
    {
        $contact_id = ContactsClientsRelation::where('client_id', $this->id)->first()->contact_id;
        $relation_client_ids = ContactsClientsRelation::where('contact_id', $contact_id)->get();
        $clients = [];
        foreach ($relation_client_ids as $relation_client_id) {
            $temp_client = Clients::find($relation_client_id->client_id);
            if ($temp_client->isCompany()) {
                array_push($clients, $temp_client);
            }
        }
        return (object) $clients;
    }

    function getDiscount() {
        return ClientTypes::find($this->type_id)->discount;
    }

    /**
     * @param $value
     * @return null
     */
    public function getContactAttribute($value)
    {
        if (is_null($value)) {
            return $this->contacts ? $this->contacts->first() : null;
        }

        return $value;
    }

    /**
     * В случае отсутствия реквизитов, возвращает id клиента
     * @return int|mixed
     */
    public function getDetailsOK() {
        if ($this->isCompany()) {
            if ($this->company_details && ($this->company_details['all_ok'] == '1')) {
                return 0;
            }
        } else {
            return 0;
        }
        return $this->id;
    }

    /**
     * Get full name of the client
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->name;
    }
}
