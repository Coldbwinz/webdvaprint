<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThemeTag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Themes, related with this tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function themes()
    {
        return $this->belongsToMany(Theme::class);
    }
}
