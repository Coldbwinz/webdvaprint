<?php

namespace App;

use App\Traits\TemplateLibraryFilters;
use Illuminate\Database\Eloquent\Model;
use App\Contracts\TemplateLibraryContract;

class Template extends Model implements TemplateLibraryContract
{
    use TemplateLibraryFilters;
    
    protected $table = 'templates';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'client_id', 'w2p', 'psd', 'url', 'type', 'name', 'themes',
        'activity', 'status', 'width', 'height', 'editable', 'folds', 'two_side',
        'template_edited', 'previews', 'product_type_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'editable' => 'bool',
        'template_edited' => 'bool',
        'previews' => 'array',
    ];
    
    public function client()
    {
        return $this->belongsTo('App\Clients');
    }

    /**
     * Создаёт макет клиента из заказа
     *
     * @param Orders $order
     */
    public static function makeFromOrder(Orders $order)
    {
        $templateSize = explode('x', $order->price_details['selected_size']);
        $editable = false;
        $w2p = null;

        if ($order->file) {
            $w2p = $order->file;
            $editable = true;
        } elseif ($order->pdf) {
            $w2p = $order->pdf;
        }

        $orderPreview = strpos($order->img, 'http://') !== false ? $order->img : '/lib/frx/'.basename($order->img);

        $previews = [];

        if ($order->readyProducts->count()) {
            foreach ($order->readyProducts as $product) {
                $previews[] = $product->preview;
            }
        } elseif ($order->elements->count()) {
            foreach ($order->elements as $element) {
                $previews[] = $element->preview;
            }
        } else {
            $previews = [$orderPreview];
        }

        $order_details = $order->price_details;
        $fill_calculation = $order_details['fill_calculation'];

        if ($order->type_product === 999999) {
            $productType = ProductType::where('name', $fill_calculation['selected_product_type_name'])->first();
        } else {
            $productType = ProductType::find($order->type_product);
        }

        return $order->client->templates()->create([
            'parent_id' => $order->template_id,
            'product_type_id' => $productType->id,
            'w2p' => $w2p,
            'psd' => null,
            'url' => $orderPreview,
            'previews' => $previews,
            'width' => $templateSize[0],
            'height' => $templateSize[1],
            'editable' => $editable,
            'folds' => isset($order->price_details['folds']) ? $order->price_details['folds'] : 0,
            'two_side' => false,
        ]);
    }

    /**
     * Scope templates for client
     *
     * @param $query
     * @param Clients $client
     */
    public function scopeForClient($query, Clients $client)
    {
        $query->where('client_id', $client->id);
    }
}
