<?php

namespace App;

use App\Template;
use Illuminate\Database\Eloquent\Model;

class TemplateDownload extends Model
{
    protected $table = 'template_download';

    public function startDownloadTemplates()
    {
        $templates_download = TemplateDownload::all();
        foreach ($templates_download as $downloadItem) {
            if (!$downloadItem->downloading) {
                if (!$downloadItem->preview_download || ($downloadItem->pdf_download)) {
                    $downloadItem->downloading = true;
                    $downloadItem->save();
                    $url = 'http://p.printler.pro/maquettes/' . $downloadItem->id . '/';
                    while (true) {
                        $myCurl = curl_init();
                        curl_setopt_array($myCurl, [
                            CURLOPT_URL => $url,
                            CURLOPT_HTTPHEADER => ['Authorization:Token JGLjLIJKPm4jcGMnq2FaaMWEHaJtI6FSwVZYqHxFl7bAbMz6QgT853T58AJINEyC',
                                'Accept:application/json; version=2.0'],
                            CURLOPT_RETURNTRANSFER => true,]);
                        $result = json_decode(curl_exec($myCurl), false);
                        curl_close($myCurl);
                        if (isset($result) && isset($result->revised_file) && (count($result->revised_file) > 0)) {
                            $result_revised_file = $result->revised_file;
                            break;
                        } else {
                            sleep(1);
                        }
                    }
                    if (!$downloadItem->preview_downloaded) {
                        $myCurl = curl_init();
                        curl_setopt_array($myCurl, array(
                            CURLOPT_URL => $result->preview_url,
                            CURLOPT_HTTPHEADER => ['Authorization:Token JGLjLIJKPm4jcGMnq2FaaMWEHaJtI6FSwVZYqHxFl7bAbMz6QgT853T58AJINEyC',
                                'Accept:application/json; version=2.0'],
                            CURLOPT_RETURNTRANSFER => true));
                        $preview_img_filename = "lib/frx/" . $downloadItem->id . ".frx.jpg";
                        $fp = fopen($preview_img_filename, 'w');
                        curl_setopt($myCurl, CURLOPT_FILE, $fp);
                        $response = curl_exec($myCurl);
                        curl_close($myCurl);
                        fclose($fp);
                        $downloadItem->preview_downloaded = true;
                        $order = Orders::find($downloadItem->id_order);
                        if ($downloadItem->id == $order->template_download_id) {
                            $order->img = $downloadItem->id . ".frx.jpg";
                        }

                        if ($downloadItem->id == $order->template_download_id) {
                            $bodyElement = $order->elements()->where('type', 'body')->first();
                            if ($bodyElement) {
                                $bodyElement->preview = '/lib/frx/'.$downloadItem->id . ".frx.jpg";
                                $bodyElement->save();
                            }
                        } else if ($downloadItem->id == $order->template_cover_download_id) {
                            $coverElement = $order->elements()->where('type', 'cover')->first();
                            if ($coverElement) {
                                $coverElement->preview = '/lib/frx/'.$downloadItem->id . ".frx.jpg";
                                $coverElement->save();
                            }
                        } else if ($downloadItem->id == $order->template_pad_download_id) {
                            $padElement = $order->elements()->where('type', 'pad')->first();
                            if ($padElement) {
                                $padElement->preview = '/lib/frx/'.$downloadItem->id . ".frx.jpg";
                                $padElement->save();
                            }
                        }

                        $order->save();

                        Template::makeFromOrder($order);
                    }
                    if ($downloadItem->pdf_download) {
                        while (true) {
                            sleep(1);
                            $myCurl = curl_init();
                            curl_setopt_array($myCurl, [
                                CURLOPT_URL => $result_revised_file,
                                CURLOPT_HTTPHEADER => ['Authorization:Token JGLjLIJKPm4jcGMnq2FaaMWEHaJtI6FSwVZYqHxFl7bAbMz6QgT853T58AJINEyC',
                                    'Accept:application/json; version=2.0'],
                                CURLOPT_RETURNTRANSFER => true]);
                            $result = json_decode(curl_exec($myCurl), false);
                            curl_close($myCurl);
                            if (count($result->location) > 0) {
                                break;
                            } else {
                                sleep(1);
                            }
                        }


                        $myCurl = curl_init();
                        curl_setopt_array($myCurl, array(
                            CURLOPT_URL => $result->location,
                            CURLOPT_RETURNTRANSFER => true));
                        $preview_pdf_filename = "lib/frx/" . $downloadItem->id . ".frx.pdf";
                        $fp = fopen($preview_pdf_filename, 'w');
                        curl_setopt($myCurl, CURLOPT_FILE, $fp);
                        $response = curl_exec($myCurl);
                        curl_close($myCurl);
                        fclose($fp);

                        $order = Orders::find($downloadItem->id_order);
                        if ($downloadItem->id == $order->template_download_id) {
                            $order->pdf = $downloadItem->id . ".frx.pdf";
                            $order->template_download_id = 0;
                        } else if ($downloadItem->id == $order->template_cover_download_id) {
                            $order->pdf_cover = $downloadItem->id . ".frx.pdf";
                            $order->template_cover_download_id = 0;
                        } else if ($downloadItem->id == $order->template_pad_download_id) {
                            $order->pdf_pad = $downloadItem->id . ".frx.pdf";
                            $order->template_pad_download_id = 0;
                        }
                        //$order->file = $template_id . ".frx";
                        //$order->status = 12; Пока не нужен
                        $order->save();
                        $downloadItem->delete();

                        Template::makeFromOrder($order);
                        
                    } else {
                        $downloadItem->downloading = false;
                        $downloadItem->save();
                    }
                }
            }
        }
    }
}
