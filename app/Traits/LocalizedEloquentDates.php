<?php

namespace App\Traits;

use Carbon\Carbon;

trait LocalizedEloquentDates
{
    /**
     * Created At
     *
     * @param $value
     * @return static
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone(settings('user_timezone'));
    }

    /**
     * Updated At
     *
     * @param $value
     * @return static
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone(settings('user_timezone'));
    }
}
