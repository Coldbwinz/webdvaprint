<?php

namespace App\Traits;

use App\Template;

trait TemplateLibraryFilters
{
    /**
     * Фильтрует шаблоны по размеру
     *
     * @param $query
     * @param array $widthRange массив [мин, макс]
     * @param array $heightRange массив размеров [мин, макс]
     * @return mixed
     */
    public function scopeSizeInRange($query, $widthRange = null, $heightRange = null)
    {
        if (! $widthRange && ! $heightRange) {
            return;
        }

        if ($widthRange && ! $heightRange) {
            return $query->widthInRange($widthRange);
        }

        if (! $widthRange && $heightRange) {
            return $query->heightInRange($heightRange);
        }

        return $query->where(function ($query) use ($widthRange, $heightRange) {
            $query->where(function ($q) use ($widthRange, $heightRange) {
                $q->widthInRange($widthRange);
                $q->heightInRange($heightRange);
            })
                ->orWhere(function ($q) use ($widthRange, $heightRange) {
                    $q->widthInRange($heightRange);
                    $q->heightInRange($widthRange);
                });
        });
    }

    /**
     * @param $query
     * @param array $widthRange
     */
    public function scopeWidthInRange($query, array $widthRange)
    {
        $query->whereBetween('width', $widthRange);
    }

    /**
     * @param $query
     * @param array $heightRange
     */
    public function scopeHeightInRange($query, array $heightRange)
    {
        $query->whereBetween('height', $heightRange);
    }

    /**
     * @param $query
     * @param int $width
     * @param int $height
     */
    public function scopeSize($query, $width, $height)
    {
        $sizeTolerance = config('w2p.template_size_tolerance');

        $widthRange = [$width - $sizeTolerance, $width + $sizeTolerance];
        $heightRange = [$height - $sizeTolerance, $height + $sizeTolerance];

        $query->sizeInRange($widthRange, $heightRange);
    }

    /**
     * @param $query
     * @param array $filters
     */
    public function scopeFilter($query, array $filters)
    {
        if (isset($filters['size'])) {
            list($width, $height) = explode('x', $filters['size']);
            $query->size($width, $height);
        }

        if (isset($filters['folds'])) {
            $query->whereFolds($filters['folds']);
        }

        if (isset($filters['two_side'])) {
            $query->where('two_side', $filters['two_side']);
        }

        if (isset($filters['product_type_id']) && ($filters['product_type_id'] != 999999)) {
            $query->where('product_type_id', $filters['product_type_id']);
        }

        // @todo пока только так ибо нет больше сил
        if ($this instanceof Template) {
            if (isset($filters['client_id'])) {
                $query->where('client_id', $filters['client_id']);
            }
        }
    }
}
