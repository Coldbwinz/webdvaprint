<?php

namespace App\Traits;

trait Managerable
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($record) {
            if (is_null($record->manager_id)) {
                $record->manager_id = auth()->user()->id;
            }
        });
    }

    /**
     * Scope records for the manager
     *
     * @param $query
     * @param integer|null $managerId
     */
    public function scopeForManager($query, $managerId = null)
    {
        if (! is_null($managerId)) {
            $query->where('manager_id', $managerId);
        }
    }
}
