<?php
namespace App\Traits;

use App\Subdomain;

/**
 * Трайт добавляется в модели с subdomaion_id
 */

trait SubdomainTrait
{

    /**
     * Scope subdomain
     *
     * @param $query
     * @param Subdomain|null $subdomain
     * @return mixed
     */
    public function scopeForSubdomain($query, $subdomain = null)
    {
        if (is_null($subdomain)) {
            return $query->whereNull('subdomain_id');
        }

        return $query->whereHas('subdomain', function ($query) use ($subdomain) {
            $query->where('id', $subdomain->id);
        });
    }

}
