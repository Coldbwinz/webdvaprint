<?php

namespace App\Traits;

/**
 * Предоставляет набор аксессоров и мутаторов, чтобы подменить записываемые и получаемые данные
 * для заказа, в зависимости от того, с какого поддомена запрашиваются эти данные
 *
 * @package App\Traits
 */
trait GroupOrdersPartnerDataTrait
{
    /**
     * Write data to json object
     *
     * @param $field
     * @param $value
     * @return \stdClass
     */
    public function writePartnerData($field, $value)
    {
        //if (($this->subdomain_id != null) || (\Auth::user()->accesLevel() == 5)) {
            return $this->attributes[$field] = $value;
        //}

        /*$data = $this->partner_data;

        if (is_null($data)) {
            $data = new \stdClass();
        }

        $data->{$field} = $value;

        return $this->partner_data = $data;*/
    }

    /**
     * @param $field
     * @param $value
     * @param null $default
     * @return null
     */
    public function getPartnerData($field, $value, $default = null)
    {
        /*if ($this->subdomain && $this->subdomain->name != subdomain()) {*/
            return $value;
        /*}

        if (! is_object($this->partner_data) || ! property_exists($this->partner_data, $field)) {
            return $default;
        }

        return $this->partner_data->{$field};*/
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPayAttribute($value)
    {
        return $this->getPartnerData('pay', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setPayAttribute($value)
    {
        return $this->writePartnerData('pay', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getMoneyAttribute($value)
    {
        return $this->getPartnerData('money', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setMoneyAttribute($value)
    {
        return $this->writePartnerData('money', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPriceAttribute($value)
    {
        return $this->getPartnerData('price', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setPriceAttribute($value)
    {
        return $this->writePartnerData('price', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getCloseDateAttribute($value)
    {
        return $this->getPartnerData('close_date', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setCloseDateAttribute($value)
    {
        return $this->writePartnerData('close_date', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPayCashAttribute($value)
    {
        return $this->getPartnerData('pay_cash', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setPayCashAttribute($value)
    {
        return $this->writePartnerData('pay_cash', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPayRsAttribute($value)
    {
        return $this->getPartnerData('pay_rs', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setPayRsAttribute($value)
    {
        return $this->writePartnerData('pay_rs', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPayEthAttribute($value)
    {
        return $this->getPartnerData('pay_eth', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setPayEthAttribute($value)
    {
        return $this->writePartnerData('pay_eth', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getStatusAttribute($value)
    {
        return $this->getPartnerData('status', $value);
    }

    /**
     * @param $value
     * @return \stdClass
     */
    public function setStatusAttribute($value)
    {
        return $this->writePartnerData('status', $value);
    }
}
