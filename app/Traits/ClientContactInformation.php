<?php

namespace App\Traits;

trait ClientContactInformation
{
    /**
     * Get specific contact information, related to the client
     *
     * @param int $clientId related client id
     * @return mixed
     */
    public function getInformationByClient($clientId)
    {
        return \DB::table('client_contact_informations')
            ->where('client_id', $clientId)
            ->where('contact_id', $this->id)
            ->first();
    }

    /**
     * @param $clientId
     * @param array $information
     * @return mixed
     */
    public function addInformationByClient($clientId, array $information)
    {
        $information = array_merge([
            'client_id' => $clientId,
            'contact_id' => $this->id,
        ], $information);

        return \DB::table('client_contact_informations')
            ->insert($information);
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function removeInformationByClient($clientId)
    {
        return \DB::table('client_contact_informations')
            ->where('client_id', $clientId)
            ->where('contact_id', $this->id)
            ->delete();
    }

    /**
     * @param $clientId
     * @param array $information
     * @return mixed
     */
    public function updateInformationByClient($clientId, array $information)
    {
        return \DB::table('client_contact_informations')
            ->where('client_id', $clientId)
            ->where('contact_id', $this->id)
            ->update($information);
    }

    /**
     * @param $clientId
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function setInformationFieldForClient($clientId, $attribute, $value)
    {
        if ($this->getInformationByClient($clientId)) {
            return $this->updateInformationByClient($clientId, [
                $attribute => $value,
            ]);
        }

        return $this->addInformationByClient($clientId, [
            $attribute => $value
        ]);
    }

    /**
     * @param $clientId
     * @param $attribute
     * @return null
     */
    public function getInformationFieldForClient($clientId, $attribute)
    {
        $information = $this->getInformationByClient($clientId);

        if (! $information) {
            return null;
        }

        return $information->{$attribute} ?: null;
    }
}
