<?php

namespace App\Traits;

trait EloquentExtended
{
    /**
     * Save a new model if not exists and return the instance.
     *
     * @param  array $attributes
     * @return static|boolean
     */
    public static function createIfNotExists(array $attributes = [])
    {
        if (! static::where($attributes)->exists()) {
            return static::create($attributes);
        }

        return false;
    }
}
