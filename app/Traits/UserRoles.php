<?php
namespace App\Traits;

trait UserRoles
{

    protected function getAllowedRoles()
    {
        $collection = \App\UserGroup::all();
        return $collection->pluck('role')->toArray();
    }

    /**
     * Handle dynamic calls into macros or pass missing methods to the store.
     * Автозамена по роли isManager(), isClient() etc.
     *
     * @param  string  $method
     * @param  array   $arguments
     * @throws \BadMethodCallException
     * @return mixed
     */
    protected function handleMagicRole($method, $arguments)
    {
        $action = substr($method, 0, 5);
        $role = strtolower(substr($method, 5));

        if (($action == 'isNot')) {
            if (in_array($role, $this->getAllowedRoles())) {
                return $this->isNot($role);
            }
            else {
                throw new \BadMethodCallException("Method [{$method}] does not exist.");
            }
        }

        $action = substr($method, 0, 2);
        $role = strtolower(substr($method, 2));
        if (($action == 'is')) {
            if (in_array($role, $this->getAllowedRoles())) {
                return $this->is($role);
            }
            else {
                throw new \BadMethodCallException("Method [{$method}] does not exist.");
            }
        }
        return null;
    }

}
