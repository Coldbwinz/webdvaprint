<?php

namespace App\Traits;

trait GroupOrderSearchTrait
{
    /**
     * Поиск для типографии
     *
     * @param $query
     * @param $keyword
     */
    public function scopeSearchForTypography($query, $keyword)
    {
        $keyword = '%' . trim($keyword) . '%';

        // Поиск по заказам самой типографии
        $query->where(function ($q) use ($keyword) {
            $q->whereNull('group_orders.subdomain_id')
                ->whereHas('client', function ($q) use ($keyword) {
                    $q->where(function ($q) use ($keyword) {
                        $q->where('name', 'like', $keyword)
                            ->orWhere('last_name', 'like', $keyword)
                            ->orWhere('email', 'like', $keyword)
                            ->orWhere('tm_name', 'like', $keyword)
                            ->orWhere('phone', 'like', $keyword);
                    });
                });
        })
            // Поиск по партнёрским заказам
            ->orWhere(function ($q) use ($keyword) {
                $q->whereNotNull('group_orders.subdomain_id')
                    ->whereHas('manager.contact', function ($q) use ($keyword) {
                        $q->where(function ($q) use ($keyword) {
                            $q->where('name', 'like', $keyword)
                                ->orWhere('last_name', 'like', $keyword)
                                ->orWhere('email', 'like', $keyword)
                                ->orWhere('phone', 'like', $keyword);
                        });
                    });
            });
    }

    /**
     * Поиск для партнёра
     *
     * @param $query
     * @param $keyword
     */
    public function scopeSearchForPartner($query, $keyword)
    {
        $keyword = '%' . trim($keyword) . '%';

        $query->where(function ($q) use ($keyword) {
            $q->whereNotNull('subdomain_id')
                ->whereHas('client', function ($q) use ($keyword) {
                    $q->where(function ($q) use ($keyword) {
                        $q->where('name', 'like', $keyword)
                            ->orWhere('last_name', 'like', $keyword)
                            ->orWhere('email', 'like', $keyword)
                            ->orWhere('tm_name', 'like', $keyword)
                            ->orWhere('phone', 'like', $keyword);
                    });
                });
        });
    }
}
