<?php

namespace App\Traits;

use App\Models\Salary\SalaryOperation;

trait UserSalary
{
    /**
     * Add premium to the user
     *
     * @param integer $amount
     * @param string $reason
     *
     * @return mixed
     */
    public function addPremium($amount, $reason = null)
    {
        return $this->salaryOperations()->create([
            'type' => SalaryOperation::SALARY_ACCRUAL,
            'action' => 'Единовременная премия',
            'name' => $reason,
            'accrued' => $amount,
        ]);
    }

    /**
     * Add a fine to the user
     *
     * @param integer $amount
     * @param string $reason
     *
     * @return mixed
     */
    public function addFine($amount, $reason = null)
    {
        $amount = - abs($amount);

        return $this->salaryOperations()->create([
            'type' => SalaryOperation::SALARY_ACCRUAL,
            'action' => 'Единовременный штраф',
            'name' => $reason,
            'accrued' => $amount,
        ]);
    }

    /**
     * Add accrual for the user
     *
     * @param string $accrualType
     * @param integer $amount
     * @param null $reason
     *
     * @return mixed
     */
    public function addAccrual($accrualType, $amount, $reason = null)
    {
        if ($accrualType === 'premium') {
            return $this->addPremium($amount, $reason);
        }

        if ($accrualType === 'fine') {
            return $this->addFine($amount, $reason);
        }
    }

    /**
     * Записи с операциями по зарплате сотрудника
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaryOperations()
    {
        return $this->hasMany(SalaryOperation::class);
    }

    /**
     * Add new salary operation for the user
     *
     * @param array $props
     * @return Model
     */
    public function addSalaryOperation(array $props)
    {
        return $this->salaryOperations()->create($props);
    }
}