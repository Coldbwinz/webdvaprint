<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadyUploadProduct extends Model
{
    const PARSE_NEW = 0;
    const PARSE_IN_PROCESS = 1;
    const PARSE_COMPLETED = 2;
    const PARSE_ERROR = -1;

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'ready_upload_products';

    /**
     * [$fillable description]
     * @var array
     */
    protected $fillable = [
        'user_id', 'order_id', 'type', 'filename', 'path', 'position',
        'original_name', 'mime_type', 'size', 'url', 'preview', 'order_element_id',
        'status_code', 'frx_path', 'exception',
    ];

    /**
     * The User who owns this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * The Order who associate with this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderElement()
    {
        return $this->belongsTo(OrderElement::class);
    }

    /**
     * Scope non parsed records
     *
     * @param $query
     */
    public function scopeNonParsed($query)
    {
        $query->where('status_code', '!=', static::PARSE_COMPLETED);
    }

    /**
     * Scope parsed records
     *
     * @param $query
     */
    public function scopeParsed($query)
    {
        $query->where('status_code', static::PARSE_COMPLETED);
    }

    /**
     * Check for parsing complete
     *
     * @return bool
     */
    public function isParsed()
    {
        return $this->status_code === static::PARSE_COMPLETED;
    }
}
