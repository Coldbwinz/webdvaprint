<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractorToken extends Model
{

    protected $table = 'contractor_tokens';

    protected $fillable = ['contractor_id', 'order_id', 'token', 'activate'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contractor()
    {
        return $this->belongsTo(Contractor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    /**
     * Save method overload
     *
     * @param  array $options
     * @return void
     */
    public function save(array $options = [])
    {
        if (empty($this->token)) {
            $this->generateToken();
        }
        parent::save($options);
    }

    /**
     * Generate token
     *
     * @return string
     */
    public function generateToken()
    {
        return $this->token = md5('contractor_token' . time() . uniqid());
    }



}
