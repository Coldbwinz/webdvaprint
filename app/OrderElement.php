<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class OrderElement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'chromacity', 'format', 'draw', 'material', 'postpress', 'status_id', 'pages_number',
        'preview', 'type', 'template', 'printable', 'pdf_path',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'postpress' => 'Array',
        'printable' => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    /**
     * Perform order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function performOrder()
    {
        return $this->belongsTo(PerformOrder::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(OrderStatuses::class, 'status_id');
    }

    /**
     * Set postpress to complete
     *
     * @param $postpress
     * @return bool
     */
    public function completePostpress($postpress)
    {
        $diff = array_diff($this->postpress, $postpress);

        if (count($diff) !== count($this->postpress)) {
            addLog($this->order->id, implode(', ', $postpress), null, '', null, 0, $this->name);
            $this->postpress = array_diff($this->postpress, $postpress);

            return $this->save();
        }

        return false;
    }

    /**
     * Проверяет, все ли операции постпресса выполнены у элемента
     *
     * @return bool
     */
    public function allPostpressPerformed()
    {
        return count($this->postpress) === 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates()
    {
        return $this->hasMany(ReadyUploadProduct::class, 'order_element_id');
    }

    /**
     * @param $query
     */
    public function scopePrintable($query)
    {
        $query->where('printable', true);
    }
}
