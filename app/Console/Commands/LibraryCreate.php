<?php

namespace App\Console\Commands;

use App\ParserFiles;
use App\ProductType;
use App\TemplateLibrary;
use App\Classes\Parser\Frx;
use Illuminate\Console\Command;

class LibraryCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:create {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create library from parser files with "move" status';
    /**
     * @var ParserFiles
     */
    private $parserFiles;

    /**
     * Create a new command instance.
     *
     * @param ParserFiles $parserFiles
     */
    public function __construct(ParserFiles $parserFiles)
    {
        parent::__construct();

        $this->parserFiles = $parserFiles;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parserFiles = $this->parserFiles->with('backside')
            ->where('status', 'move')
            ->where('side', 'front')
            ->get();

        $addedCount = 0;

        $bar = $this->output->createProgressBar(count($parserFiles));

        $force = $this->option('force');

        foreach ($parserFiles as $file) {
            $bar->advance();

            if ($force && TemplateLibrary::hasPsd($file->path)) {
                continue;
            }

            $type = 0;
//            $type = ProductType::where('key', $file->type)->first();
//            $type = $type ? $type->id : null;
//
//            if (is_null($type)) {
//                $this->line("Unknown product type: \"{$file->type}\"");
//                continue;
//            }

            TemplateLibrary::makeFromParsedFile($file, $type);

            // $this->line("File {$file->filename} added to library.");

            // Merge two sided
            if ($file->backside) {
                // merge templates and add to library
                $this->mergeTemplates($file, $file->backside);
                // $this->line("File {$file->filename} and {$file->backside->filename} was merged.");
            }

            $addedCount++;
        }

        $bar->finish();
        $this->line(PHP_EOL);
        $this->info("{$addedCount} file(s) added to library");

        if ($addedCount === 0) {
            $this->line("All psd added to library.");
        }
    }

    /**
     * @param ParserFiles $first
     * @param ParserFiles $second
     * @return bool
     */
    public function mergeTemplates(ParserFiles $first, ParserFiles $second)
    {
        if ($first->status !== 'move' || $second->status !== 'move') {
            return false;
        }

        $name = uniqid();
        $folder = uniqid();
        $newFrx = Frx::getLibraryPath().'/'.$folder.'/'.$name.'.frx';

//        $type = ProductType::where('key', $first->type)->first();
//        $type = $type ? $type->id : null;
        $type = 0;

        if (! file_exists(Frx::getLibraryPath().'/'.$folder)) {
            mkdir(Frx::getLibraryPath().'/'.$folder, 0777, true);
        }


        Frx::combine($first->getFrxPath(), $second->getFrxPath(), $newFrx);

        // copy files folder
        if (file_exists($first->upload_folder . '/files')) {
            rcopy($first->upload_folder . '/files', Frx::getLibraryPath().'/'.$folder.'/files');
        }
        $prefix = '2_';
        if (file_exists($second->upload_folder . '/files')) {
            $files = scandir($second->upload_folder . '/files');
            foreach ($files as $f) {
                if (!in_array($f, ['.', '..'])) {
                    $src = $second->upload_folder . '/files/'.$f;
                    $dst = Frx::getLibraryPath().'/'.$folder.'/files/'.$prefix.$f;
                    copy($src, $dst);
                }
            }
        }

        // move preview of the first file to the folder with new frx
        $previewSmall = $first->upload_folder . '/' . $first->filename . '_small.png';
        copy($previewSmall, Frx::getLibraryPath().'/'.$folder . '/' . $name . '_small.png');

        // move preview to the public folder
        $previewPath = Frx::getPreviewPath().'/'.$folder;
        if (! file_exists(public_path($previewPath))) {
            mkdir(public_path($previewPath), 0777, true);
        }
        $small = Frx::getPreviewFile($newFrx);
        $preview = $previewPath.'/'.$name.'_small.png';
        copy($small, public_path($preview));

        $secondPreview = $previewPath.'/'.$name.'_small_2.png';
        copy($second->upload_folder.'/'.$second->filename.'_small.png', public_path($secondPreview));

        $ftype = str_replace('__2', '', $first->type);
        $dimension = config("w2p.templates.{$ftype}.dimension");
        $width = null;
        $height = null;
        $folds = config("w2p.templates.{$ftype}.folds", 0);

        if ($dimension) {
            $dimension = explode('x', $dimension);
            $width = $dimension[0];
            $height = $dimension[1];
        }

        // Save template library
        $template = new TemplateLibrary();
        $template->w2p = $newFrx;
        $template->psd = $first->path;
        $template->url = '/'.$preview;
        $template->second_preview = '/'.$secondPreview;
        $template->type = $type;
        $template->two_side = true;
        $template->width = $width;
        $template->height = $height;
        $template->folds = $folds;
        $template->collection = $first->vendor.'_'.$first->number;
        $template->type_key = str_replace('__2', '', $first->type);
        $template->save();

        return $template;
    }
}
