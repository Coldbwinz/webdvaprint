<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ParserFiles;
use File;

class LibraryMoveFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:movefolder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = ParserFiles::where('status','=','move')->orWhere('status','=','library')->get();
        $count = $files->count();
        
        $bar = $this->output->createProgressBar($count);
        
        foreach ($files as $file) {            
            $src = $file->upload_folder.'/files';
            $dst = $file->upload_folder.'/'.$file->name;
            rcopy($src, $dst);
            
            $bar->advance();
        }
        
        $bar->finish();
                
        echo "\n\r".'Move '.$count.' folders'."\n\r";
        
        
    }
}
