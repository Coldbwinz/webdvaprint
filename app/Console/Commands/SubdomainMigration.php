<?php

namespace App\Console\Commands;

use Illuminate\Support\Arr;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class SubdomainMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subdomain:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate to all databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = \SubdomainEnvironment::getAll();

        $subdomains = array_filter($env, function ($item) {
            return Arr::has($item, 'database.connections.mysql.database');
        });

        $databases = array_map(function ($item) {
            return Arr::get($item, 'database.connections.mysql.database');
        }, $subdomains);

        $databases = array_values($databases);
        $databases = array_unique($databases);

        $databasesString = implode(', ', $databases);
        $count = count($databases);

        if ($count === 0) {
            $this->line('No databases for migrate.');
            return;
        }

        Artisan::call('down');
        $this->info(Artisan::output());

        if ($this->confirm("Found {$count} databases: {$databasesString}. Do you wish to migrate?")) {
            foreach ($databases as $databaseName) {
                $this->info("Starting migrate to: {$databaseName}.");

                $connectionName = "{$databaseName}SubdomainConnection";
                $connection = config('database.connections.mysql');
                $connection['database'] = $databaseName;

                app('config')->set("database.connections.{$connectionName}", $connection);

                Artisan::call('migrate', [
                    '--database' => $connectionName
                ]);

                $this->line(Artisan::output());
            }

            Artisan::call('up');

            $this->info(Artisan::output());

            return;
        }

        $this->info('Migration canceled by user.');
    }
}
