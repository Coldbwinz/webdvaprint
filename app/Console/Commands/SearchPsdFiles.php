<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use App\ParserFiles;
use App\Jobs\ParsePsd;
use FilesystemIterator;
use RecursiveIteratorIterator;
use Illuminate\Console\Command;
use RecursiveDirectoryIterator;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SearchPsdFiles extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'psd:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search new psd files for parsing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $uploadPath = config('library.path');

        $insertedCount = 0;
        $deletedCount = 0;

        // search
        $files = $this->findPsd($uploadPath);

        $totalCount = count($files);

        if ($totalCount === 0) {
            $this->line("Not find psd files in {$uploadPath}");

            return false;
        }

        // delete old files
        $allParserFiles = ParserFiles::where('status', 'new')->get();
        foreach ($allParserFiles as $parserFile) {
            if (! in_array($parserFile->path, $files)) {
                $parserFile->delete();
                $deletedCount++;
            }
        }

        // add new files
        foreach ($files as $file) {
            $parserFile = ParserFiles::where('path', $file)->first();

            if (! $parserFile) {
                $info = pathinfo($file);

                $new = ParserFiles::create([
                    'filename' => $info['filename'],
                    'name' => $info['filename'],
                    'path' => $file,
                    'status' => 'new',
                ]);

                $this->dispatch(new ParsePsd($new));

                $insertedCount++;
            }
        }

        $this->line("Founded {$totalCount} files");
        $this->line("Inserted {$insertedCount} files");
        $this->line("Deleted {$deletedCount} files");
    }

    /**
     * Find *.psd files in folder
     *
     * @param string $path
     * @return array
     * @throws Exception
     */
    protected function findPsd($path)
    {
        $psdFiles = [];

        if (! file_exists($path)) {
            throw new Exception("Can't find pds files. Folder does not exists: {$path}");
        }

        $directory = $it = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($directory);

        foreach ($iterator as $file) {
            if ($file->isFile() && strtolower($file->getExtension()) === 'psd') {
                $psdFiles[] = $file->getRealPath();
            }
        }

        return $psdFiles;
    }
}
