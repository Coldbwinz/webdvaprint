<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ClearDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear database from orders, users etc.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tablesForTruncate = [
            'attachment_files', 'clients', 'client_company', 'client_contact_informations', 'contacts',
            'contacts_clients_relation', 'contacts_relations', 'contact_contractor', 'contact_files',
            'contractors', 'contractor_order_jobs', 'contractor_tokens', 'group_orders', 'orders', 'order_elements',
            'order_logs', 'perform_orders', 'ready_upload_products', 'salary_bonuses', 'salary_motivation_schemes',
            'salary_operations', 'specifications', 'specification_history', 'statistic_clients', 'statistic_invoices',
            'statistic_orders', 'templates', 'template_download', 'users', 'users_groups',
        ];

        $seeds = [
            'Fill_client_types', 'Fill_order_statuses', 'UsersGroupsTableSeeder', 'UsersGroupsRoleSeeder',
            'UsersTableSeeder',
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tablesForTruncate as $table) {
            $this->line("Truncate <info>{$table}</info> table.");
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        // Migrations
        foreach ($seeds as $class) {
            Artisan::call('db:seed', [
                '--class' => $class
            ]);
            $this->line("<info>{$class}</info> seeded.");
        }

        $this->warn("Don't forget to edit subdomains-env.yml file!");
    }
}
