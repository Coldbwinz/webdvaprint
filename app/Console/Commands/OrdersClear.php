<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Orders;
use App\Order_logs;

class OrdersClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $frxPath = getcwd().'/public/lib/frx/';        
        
        $orders = Orders::all();
        $count = $orders->count();
        
        $index = 0;
        $bar = $this->output->createProgressBar($count);
        
        foreach ($orders as $order) {             
            if (!file_exists($frxPath . $order->file)) {                
                DB::beginTransaction();
                $order->delete();
                Order_logs::where('order_id', $order->id)->delete();
                DB::commit();
                
                $bar->advance();
                $index++;
            }
        }
        
        $bar->finish();
                
        echo "\n".'Delete '.$index.' records'."\n";
    }
    
}
