<?php

namespace App\Console\Commands;

use App\ParserFiles;
use App\Jobs\ParsePsd;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ParsePsdFiles extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'psd:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse new psd files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (ParserFiles::where('status', 'new')->get() as $file) {
            $this->dispatch(new ParsePsd($file));
        }
    }
}
