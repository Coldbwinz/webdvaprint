<?php

namespace App\Console\Commands;

use App\ParserFiles;
use Illuminate\Console\Command;
use DoubleUp\FilenameSplitter\FilenameSplitterException;

class PsdOrganize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'psd:organize {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Splits psd filenames by parts, eg. number, type, side etc.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ParserFiles $parserFiles
     * @return mixed
     */
    public function handle(ParserFiles $parserFiles)
    {
        $force = $this->option('force');

        $query = $parserFiles->parsed();

        if (! $force) {
            $query->whereNull('vendor')
                ->whereNull('number')
                ->whereNull('type')
                ->whereNull('side');
        }

        $files = $query->get();

        $total = $files->count();
        $splittedCount = 0;
        $unsortedCount = 0;
        $unsorted = [];

        if ($total === 0) {
            $this->error("There are no unorganized records about parsed files in 'parser_files' table");

            return false;
        }

        $splitter = app('filename-splitter');

        $this->info("Total records: {$total}");

        foreach ($files as $psdFile) {
            try {
                $data =  $splitter->split($psdFile->path);

                if ($psdFile->side === 'front') {
                    $backSide = $this->searchBackSide($psdFile);
                    $data['backside_id'] = $backSide ? $backSide->id : null;

                    if ($backSide) {
                        $data['type'] .= '__2';
                    }
                }

                $psdFile->update($data);
                $splittedCount++;
            } catch (FilenameSplitterException $exception) {
                $unsorted[] = $psdFile->filename;
                $unsortedCount++;
                $this->warn("Do not know how to split {$psdFile->filename} file.");
            }
        }

        $this->line("Splitted {$splittedCount} files.");
        $this->line("Unsorted {$unsortedCount} files.");
    }

    /**
     * Search backside template for $psdFile

     * @param $psdFile
     * @return null
     */
    private function searchBackSide($psdFile)
    {
        $backSide = ParserFiles::where('vendor', $psdFile->vendor)
            ->where('number', $psdFile->number)
            ->where('type', $psdFile->type)
            ->where('side', 'back')
            ->first();

        return $backSide;
    }
}
