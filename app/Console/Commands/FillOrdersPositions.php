<?php

namespace App\Console\Commands;

use App\GroupOrders;
use Illuminate\Console\Command;

class FillOrdersPositions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:update_positions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update positions for all orders. Warning! This is overwrite current orders positions!!!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groups = GroupOrders::with('orders')->get();

        $updated = 0;

        foreach ($groups as $group) {
            $orders = $group->orders;
            $index = 1;

            foreach ($orders as $order) {
                $order->position = $index;
                $order->save();
                $index++;
                $updated++;
            }
        }

        $this->line("Total orders updated: {$updated}");
    }
}
