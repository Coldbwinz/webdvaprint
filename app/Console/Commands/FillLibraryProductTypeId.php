<?php

namespace App\Console\Commands;

use App\ProductType;
use App\TemplateLibrary;
use Illuminate\Console\Command;

class FillLibraryProductTypeId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:producttype';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill product_type_id field for library templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $templates = TemplateLibrary::all();

        $productTypes = collect([
            'v' => 1, // визитка
            'fb' => 2, // бланк
            'b1' => 3, // бкулет
            'b2' => 3,
            'b' => 4, // блокнот
            'l' => 5, // листовка
            'pg' => 6, // плакат
            'pv' => 6,
            'ke' => 7, // конверт
            'k4' => 7,
            'c' => 8, // кубарики

            /*
            'd' => null, // дипломы
            'o5' => null, // открытки
            'o5f' => null,
            'oe' => null,
            'oef' => null,
            'kg' => null, // календарики
            'kv' => null, */
        ]);

        $bar = $this->output->createProgressBar(count($templates));

        foreach ($templates as $template) {
            $productTypeId = $productTypes->get($template->type_key);

            if ($productTypeId) {
                $template->product_type_id = $productTypeId;
                $template->save();
            }

            $bar->advance();
        }

        $bar->finish();
        $this->line(PHP_EOL);
        $this->info("Templates updated");
    }
}
