<?php

namespace App\Console\Commands;

use App\ParserFiles;
use App\TemplateLibrary;
use Exception;
use FilesystemIterator;
use Illuminate\Console\Command;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class LibraryClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It removes all the folders in the library folder that is not in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $libraryPath = config('frx.path.library');

        $templates = TemplateLibrary::all()->lists('w2p');

        $templates = $templates->map(function ($template) {
            return pathinfo($template, PATHINFO_DIRNAME);
        })->toArray();

        $parserFiles = ParserFiles::all()->pluck('upload_folder');
        foreach ($parserFiles as $file) {
            $templates[] = $file;
        }

        $templates = array_unique($templates);

        $templateFolders = $this->findTemplateFoldersOnDisk($libraryPath);

        $diff = array_diff($templateFolders, $templates);
        $diffCount = count($diff);

        foreach ($diff as $folderPath) {
            rrmdir($folderPath);
        }

        $this->info("{$diffCount} folder(s) deleted.");
    }

    /**
     * @param $path
     * @return array
     * @throws Exception
     */
    protected function findTemplateFoldersOnDisk($path)
    {
        if (! is_dir($path)) {
            throw new Exception("Can't find folders. Library folder does not exists: {$path}");
        }

        return glob($path . '/*', GLOB_ONLYDIR);
    }
}
