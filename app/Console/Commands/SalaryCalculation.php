<?php

namespace App\Console\Commands;

use App\Models\Salary\SalaryOperation;
use App\User;
use Carbon\Carbon;
use App\UserGroup;
use Illuminate\Console\Command;
use App\Services\CarbonDatePeriod;
use App\Models\Statistic\StatisticOrder;
use App\Models\Statistic\StatisticClient;
use App\Models\Statistic\StatisticInvoice;

class SalaryCalculation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Employees salary calculation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Получить всех сотрудников (менеджеров), которым необходимо подсчитать зарплату
        $managers = User::managers()->get();

        // Получить план продаж для менеджеров
        $managerRole = UserGroup::where('id', UserGroup::MANAGER)->first();
        $newClientsCountPlan = $managerRole->new_clients_count_plan;
        $repeatedOrdersCountPlan = $managerRole->repeated_orders_count_plan;
        $invoicesTotalPlan = $managerRole->invoices_total_plan;
        $paidInvoicesTotalPlan = $managerRole->paid_invoices_total_plan;

        // Для каждого сотрудника подсчитать выполнение плана продаж по усреднённому показателю
        $monthPeriod = new CarbonDatePeriod(Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());

        foreach ($managers as $manager) {
            $receivableTotal = 0;

            // Суммируем все начисленные бонусы вместе с вычетами
            $bonusesTotalSum = $manager->salaryOperations()
                ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                ->whereIn('type', [SalaryOperation::SALARY_BONUS, SalaryOperation::SALARY_DEDUCTION])
                ->sum('accrued');

            $newClientsCount = StatisticClient::newClientsCount($monthPeriod, $manager->id);
            $repeatedOrdersCount = StatisticOrder::repeatedOrdersCount($monthPeriod, $manager->id);
            $invoicesTotal = StatisticInvoice::invoicesSum($monthPeriod, $manager->id);
            $paidInvoicesTotal = StatisticInvoice::paidInvoicesSum($monthPeriod, $manager->id);

            $newClientsCountPlanImplementation = $newClientsCount * 100 / $newClientsCountPlan;
            $repeatedOrdersCountPlanImplementation = $repeatedOrdersCount * 100 / $repeatedOrdersCountPlan;
            $invoicesTotalPlanImplementation = $invoicesTotal * 100 / $invoicesTotalPlan;
            $paidInvoicesTotalPlanImplementation = $paidInvoicesTotal * 100 / $paidInvoicesTotalPlan;

            $planImplementaion = compact(
                'newClientsCountPlanImplementation',
                'repeatedOrdersCountPlanImplementation',
                'invoicesTotalPlanImplementation',
                'paidInvoicesTotalPlanImplementation'
            );

            if ($managerRole->salary->premium_calculation === 'averaged_index') {
                $monthPlanImplementation = array_sum($planImplementaion) / count($planImplementaion);
                $monthPlanImplementation = round($monthPlanImplementation);
            } else {
                $monthPlanImplementation = min($planImplementaion);
            }

            $manager->addSalaryOperation([
                'action' => "Месячный план выполнен на {$monthPlanImplementation}% (расчет по усредненному показателю)",
                'is_header' => true,
            ]);

            // get bonus coefficient
            $motivationScheme = $managerRole->salaryMotivationSchemes()
                ->where('percent', '>=', $monthPlanImplementation)
                ->orderBy('percent', 'asc')
                ->first();

            if (! $motivationScheme) {
                $motivationScheme = $managerRole->salaryMotivationSchemes()->orderBy('percent', 'desc')->first();
            }

            $bonusCoefficient = 1;

            if (in_array($motivationScheme->bonus, ['increased', 'decreased'])) {
                $bonusCoefficient = $motivationScheme->bonus_coefficient;
            }

            if ($motivationScheme->bonus === 'none') {
                $bonusCoefficient = 0;
            }

            $bonusesTotalSumWithCoefficient = $bonusesTotalSum * $bonusCoefficient;

            if ($bonusCoefficient < 1) {
                $action = "Расчитан пониженный коэффициент к бонусам";
            } elseif ($bonusCoefficient == 1) {
                $action = "Стандартный коэффициент к бонусам";
            } elseif ($bonusCoefficient > 1) {
                $action = "Расчитан повышенный коэффициент к бонусам";
            }

            $manager->addSalaryOperation([
                'action' => $action,
                'name' => $bonusCoefficient,
                'accrued' => $bonusesTotalSum,
                'receivable' => $bonusesTotalSumWithCoefficient,
            ]);

            $receivableTotal += $bonusesTotalSumWithCoefficient;

            $salary = 0;
            $salaryTitle = 'Без оклада';

            if ($motivationScheme->salary === 'standart') {
                $salary = $managerRole->salary->standard_salary;
                $salaryTitle = 'Стандартный оклад';
            } elseif ($motivationScheme->salary === 'increased') {
                $salary = $managerRole->salary->increased_salary;
                $salaryTitle = 'Увеличенный оклад';
            }

            $manager->addSalaryOperation([
                'action' => "Начислен оклад",
                'name' => $salaryTitle,
                'accrued' => $salary,
            ]);

            $receivableTotal += $salary;

            $premium = 0;
            $premiumTitle = 'Без премии';

            if ($motivationScheme->premium === 'standart') {
                $premium = $managerRole->salary->standard_premium;
                $premiumTitle = 'Стандартная премия';
            } elseif ($motivationScheme->premium === 'increased') {
                $premium = $managerRole->salary->increased_premium;
                $premiumTitle = 'Повышенная премия';
            }

            $manager->addSalaryOperation([
                'action' => "Начислена премия",
                'name' => $premiumTitle,
                'accrued' => $premium,
            ]);

            $receivableTotal += $premium;

            // Единовременные штрафы и премии
            $accruals = $manager->salaryOperations()
                ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                ->where('type', [SalaryOperation::SALARY_ACCRUAL])
                ->get();

            $accrualsTotal = $accruals->sum('accrued');

            foreach ($accruals as $accrual) {
                $manager->addSalaryOperation([
                    'action' => $accrual->accrued > 0
                        ? "Единовременное начисление (премия)"
                        : "Единовременное начисление (штраф)",
                    'name' => $accrual->name,
                    'accrued' => $accrual->accrued,
                ]);
            }

            $receivableTotal += $accrualsTotal;

            $manager->addSalaryOperation([
                'action' => "<div class=\"text-right\">Итого: <strong>{$receivableTotal} руб.</strong></div>",
                'is_header' => true,
            ]);
        }
    }
}
