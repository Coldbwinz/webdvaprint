<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ParserFiles;

class LibraryMakePreview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:makepreview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = ParserFiles::whereIn('status',['move', 'library'])->get();
        $count = $files->count();
        
        $bar = $this->output->createProgressBar($count);
        
        foreach ($files as $file) {
            try {
                $png = $file->upload_folder.'/'.$file->name.'.png';
                $small = $file->upload_folder.'/'.$file->name.'_small.png';

                image_resize($png, $small, 250, 250, 70);                

                // move preview
                $preview = __DIR__.'/../../../public'.$file->preview;
                $dir = dirname($preview);
                if (!file_exists($dir)) {
                    mkdir($dir);
                }
                copy ($small, $preview);
            }
            catch (\Exception $e) {
                echo $e->getMessage();                        
            }
            
            $bar->advance();
        }
        
        $bar->finish();
                
        echo "\n\r".'Resize '.$count.' pictures'."\n\r";
    }
}
