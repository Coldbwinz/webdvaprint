<?php

namespace App\Console\Commands;

use App\Clients;
use App\ClientTypes;
use Illuminate\Console\Command;

class UpdateClientTypesForPartners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client_types:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds client types by default to all partners who do not have them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $partners = Clients::partners()->get();

        $partnersWithoutClientTypes = $partners->filter(function (Clients $partner) {
            try {
                return ! $partner->account->subdomain->clientTypes
                    || ! $partner->account->subdomain->clientTypes->count();
            } catch (\Exception $exception) {
                return false;
            }
        });

        if ($partnersWithoutClientTypes->count() === 0) {
            $this->info('Nothing to update');
        }

        foreach ($partnersWithoutClientTypes as $partner) {
            $partner->account->subdomain->clientTypes()->insert(ClientTypes::getDefaultTypes(
                $partner->account->subdomain->id
            ));

            $this->info("Client types was added to {$partner->account->email}");
        }
    }
}
