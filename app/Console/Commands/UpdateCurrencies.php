<?php

namespace App\Console\Commands;

use App\Classes\CurrencyTranslator\CurrencyTranslator;
use App\Models\Equipment\Equipment;
use App\Models\Material\Material;
use Illuminate\Console\Command;

class UpdateCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update materials and equipments currencies';

    /**
     * @var CurrencyTranslator
     */
    protected $currencyTranslator;

    /**
     * Create a new command instance.
     *
     * @param CurrencyTranslator $currencyTranslator
     */
    public function __construct(CurrencyTranslator $currencyTranslator)
    {
        parent::__construct();

        $this->currencyTranslator = $currencyTranslator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Material::all()->each(function ($material) {
            $material->updateSheetPrice();
        });

        Equipment::where('equipment_print_type_id', 1)->with('printParameters')->get()->each(function ($equipment) {
            foreach ($equipment->printParameters as $parameter) {
                $currency = strtoupper($parameter->properties->currency);

                if ($currency !== CurrencyTranslator::CURRENCY_RUB) {
                    $clickPrice = $parameter->properties->click_price;
                    $parameter->cost_price = $this->currencyTranslator->convertToRub($clickPrice, $currency);
                    $parameter->save();
                }
            }
        });
    }
}
