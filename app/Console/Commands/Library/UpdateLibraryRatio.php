<?php

namespace App\Console\Commands\Library;

use App\TemplateLibrary;
use Illuminate\Console\Command;

class UpdateLibraryRatio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:ratio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update templates ratio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $templates = TemplateLibrary::all();

        $bar = $this->output->createProgressBar(count($templates));

        foreach ($templates as $template)
        {
            $width = $template->width;
            $height = $template->height;

            if (! $width || ! $height) {
                continue;
            }

            $ratio = round($width / $height, 5);

            $template->ratio = $ratio;
            $template->save();

            $bar->advance();
        }

        $bar->finish();

        $this->line(PHP_EOL);
        $this->info("Updating complete.");
    }
}
