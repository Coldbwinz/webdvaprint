<?php

namespace App\Console\Commands\Library;

use Illuminate\Console\Command;

class LibraryPreviewUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:preview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update second preview for two-sided templates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $templates = \App\TemplateLibrary::where('two_side', true)->get();

        foreach ($templates as $template) {
            // поиск psd шаблона
            $parserFile = \App\ParserFiles::where('path', $template->psd)->first();

            // Поиск psd второй стороны шаблона
            $secondParserFile = $parserFile->backside;

            // Скопировать превью второй стороны в папку с шаблоном
            $pathInfo = pathinfo($template->url);
            $from = $secondParserFile->upload_folder.'/'.$secondParserFile->filename.'_small.png';
            $secondPreview = $pathInfo['dirname'].'/'.$pathInfo['filename'].'_2.'.$pathInfo['extension'];

            if (! file_exists($from)) {
                $this->error("No file: {$from}");
                continue;
            }

            copy($from, public_path($secondPreview));

            // Записать путь ко 2й превью в двухсторонний шаблон
            $template->second_preview = $secondPreview;
            $template->save();

            $this->info('Updated: ' . $template->second_preview);
        }
    }
}
