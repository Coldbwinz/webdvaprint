<?php

namespace App\Console;

use App\Console\Commands\ClearDatabase;
use App\Console\Commands\FillLibraryProductTypeId;
use App\Console\Commands\FillOrdersPositions;
use App\Console\Commands\Library\LibraryPreviewUpdate;
use App\Console\Commands\LibraryClear;
use App\Console\Commands\OrdersFillPositions;
use App\Console\Commands\PsdOrganize;
use App\Console\Commands\ParsePsdFiles;
use App\Console\Commands\LibraryCreate;
use App\Console\Commands\SalaryCalculation;
use App\Console\Commands\SearchPsdFiles;
use App\Console\Commands\StartParsingPsd;
use App\Console\Commands\SubdomainMigration;
use App\Console\Commands\UpdateClientTypesForPartners;
use App\Console\Commands\Library\UpdateLibraryRatio;
use App\Console\Commands\UpdateCurrencies;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\OrdersClear::class,
        \App\Console\Commands\LibraryMoveFolder::class,
        \App\Console\Commands\LibraryMakePreview::class,
        SearchPsdFiles::class,
        ParsePsdFiles::class,
        PsdOrganize::class,
        LibraryCreate::class,
        FillOrdersPositions::class,
        UpdateClientTypesForPartners::class,
        SubdomainMigration::class,
        ClearDatabase::class,
        LibraryClear::class,
        LibraryPreviewUpdate::class,
        UpdateLibraryRatio::class,
        UpdateCurrencies::class,
        SalaryCalculation::class,
        FillLibraryProductTypeId::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('currency:update')
            ->dailyAt('00:00');
    }
}
