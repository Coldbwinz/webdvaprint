<?php

namespace App\Services;

use ZipArchive;

class FrxToPdfConvertor
{
    public static function convert($file_name)
    {
        $workingDir = pathinfo($file_name, PATHINFO_DIRNAME);

        chdir($workingDir);

        $file_name = basename($file_name);

        $file_dir = str_replace('.frx', '', $file_name);

        $dirHandle = opendir($file_dir);

        // Make zip
        $zip = new ZipArchive();
        if ($zip->open($file_name . '.zip', ZipArchive::CREATE) === true) {
            $zip->addFile($file_name, $file_name);

            while (false !== ($file = readdir($dirHandle))) {

                if (strlen($file) > 3) {
                    $zip->addFile( $file_dir . '/' . $file, $file_dir . '/' . $file);
                }
            }

            $zip->close();
        } else {
            echo 'Не могу создать архив!';
        }

        $filename = $file_name . '.zip';
        $pdf = str_replace('.zip','.pdf', $filename);

        $myCurl = curl_init();
        curl_setopt_array($myCurl, array(
            CURLOPT_URL => 'http://208.76.175.70/service/ReportService.svc/putzip/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_USERPWD => "frservice:jhkja882klo",
            CURLOPT_HTTPHEADER => array('Content-type: multipart/form-data'),
            CURLOPT_POSTFIELDS => array (
                'preparedReport' => curl_file_create($filename)
            )
        ));

        $uuid = curl_exec($myCurl); // юид картинки

        curl_close($myCurl);

        $uuid = str_replace('"', '', $uuid);

        if ($uuid) {

            // Get pdf
            $myCurl = curl_init();
            curl_setopt_array($myCurl, array(
                CURLOPT_URL => 'http://208.76.175.70/service/ReportService.svc/getpdf/' . $uuid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD => "frservice:jhkja882klo"
            ));

            //открываем файловый дескриптор (куда сохранять файл)
            $fp = fopen($pdf,'w');

            //сохраняем файл
            curl_setopt($myCurl, CURLOPT_FILE, $fp);

            $response = curl_exec($myCurl);

            curl_close($myCurl);

            fclose($fp);
        }

        return $workingDir . '/' . $pdf;
    }
}
