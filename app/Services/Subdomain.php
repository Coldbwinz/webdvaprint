<?php

namespace App\Services;

class Subdomain
{
    /**
     * Check for avaiable subdomain
     *
     * @param string $subdomain
     * @return bool
     */
    public static function avaiable($subdomain)
    {
        $env = app('subdomain-environment');

        return ! $env->blacklisted($subdomain) && ! $env->hasConfig($subdomain);
    }
}
