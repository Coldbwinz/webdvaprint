<?php

namespace App\Services;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class LogoImage
{
    /**
     * Make and store logo image from request
     *
     * @param Request $request
     * @return string
     */
    public static function makeFromRequest(Request $request)
    {
        $image = app(ImageManager::class)->make($request->file('logo'));
        $image->fit($request->input('width'), $request->input('height'));

        $name = str_random();
        $extension = mime_to_extension($image->mime());

        $filename = "{$name}.{$extension}";
        $folder = "/upload/logos";
        $path = "$folder/{$filename}";

        if (! file_exists(public_path($folder))) {
            mkdir(public_path($folder), 0777, true);
        }

        $image->save(public_path($path), 100);

        return $path;
    }
}
