<?php

namespace App\Services;

use DateInterval;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class CarbonDatePeriod implements Arrayable
{
    /**
     * @var \Carbon\Carbon
     */
    protected $start;

    /**
     * @var \Carbon\Carbon
     */
    protected $end;

    /**
     * CarbonDatePeriod constructor.
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function __construct(Carbon $start, Carbon $end)
    {
        $this->start = $start->startOfDay();
        $this->end = $end->endOfDay();
    }

    /**
     * Make period from string
     *
     * @param string $date date string
     * @param string $delimiter delimiter between two dates
     *
     * @return static
     * @throws \Exception
     */
    public static function makeFromString($date, $delimiter = ' - ')
    {
        $period = explode($delimiter, $date);

        if (count($period) !== 2) {
            throw new \Exception('Invalid date string');
        }

        return new static(Carbon::parse($period[0]), Carbon::parse($period[1]));
    }

    /**
     * Gets the end date
     *
     * @return Carbon
     */
    public function getEndDate()
    {
        return $this->end;
    }

    /**
     * Gets the start date
     *
     * @return Carbon
     */
    public function getStartDate()
    {
        return $this->start;
    }

    /**
     * Substraction from period
     *
     * @param \DateInterval $interval
     *
     * @return static
     */
    public function subInterval(DateInterval $interval)
    {
        $start = $this->start->sub($interval);
        $end = $this->end->sub($interval);

        return new static($start, $end);
    }

    /**
     * Get the difference in days
     *
     * @param bool $abs Get the absolute of the difference
     *
     * @return int
     */
    public function diffInDays($abs = true)
    {
        return (int)$this->end->diff($this->start, $abs)->format('%r%a');
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [$this->start, $this->end];
    }
}
