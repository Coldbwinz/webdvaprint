<?php

namespace App\Services;

use Image;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class ClientPicture
{
    /**
     * @param Request $request
     * @return string
     */
    public static function makeFromRequest(Request $request)
    {
        $image = app(ImageManager::class)->make($request->file('picture'));
        $image->fit($request->input('width'), $request->input('height'));

        $name = str_random();
        $extension = mime_to_extension($image->mime());

        $filename = "{$name}.{$extension}";
        $folder = "upload/clients";
        $path = "/$folder/{$filename}";

        if (! file_exists(public_path($folder))) {
            mkdir(public_path($folder), 0777, true);
        }

        $image->save(public_path($path), 100);

        return $path;
    }
}
