<?php

namespace App\Services;

use Maknz\Slack\Client;

class Slack
{
    public static function notification($message)
    {
        if (config('slack.notification_hook')) {
            $client = new Client(config('slack.notification_hook'));
            $client->send($message);
        }
    }
}
