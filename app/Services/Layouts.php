<?php

namespace App\Services;

class Layouts
{
    protected $width;
    protected $height;
    protected $format;
    protected $layoutItem;

    /**
     * @param $width
     * @param $height
     * @param $format
     */
    public function __construct($width, $height, $format)
    {
        $this->width = $width;
        $this->height = $height;
        $this->format = $format;

        $this->layoutItem = $this->findLayoutItem($width, $height, $format);
    }

    /**
     * @return array
     */
    public function layouts()
    {
        return [
            [
                'widthMin' => 305, 'widthMax' => 319,
                'heightMin' => 450, 'heightMax' => 475,

                'size' => [
                    '50x90' => 24,
                    '55x85' => 25,
                    '70x100' => 16,
                    '74x105' => 12,
                    '90x90' => 12,
                    '98x210' => 6,
                    '105x148' => 8,
                    '148x210' => 4,
                    '210x297' => 2,
                    '297x420' => 1
                ]
            ],

            [
                'widthMin' => 320, 'widthMax' => 330,
                'heightMin' => 450, 'heightMax' => 475,

                'size' => [
                    '50x90' => 24,
                    '55x85' => 25,
                    '70x100' => 18,
                    '74x105' => 16,
                    '90x90' => 12,
                    '98x210' => 6,
                    '105x148' => 8,
                    '148x210' => 4,
                    '210x297' => 2,
                    '297x420' => 1
                ]
            ],

            [
                'widthMin' => 330, 'widthMax' => 330,
                'heightMin' => 476, 'heightMax' => 500,

                'size' => [
                    '50x90' => 30,
                    '55x85' => 25,
                    '70x100' => 18,
                    '74x105' => 16,
                    '90x90' => 15,
                    '98x210' => 6,
                    '105x148' => 8,
                    '148x210' => 4,
                    '210x297' => 2,
                    '297x420' => 1
                ]
            ]
        ];
    }

    /**
     * @param $width
     * @param $height
     * @return null
     */
    public function findLayoutItem($width, $height)
    {
        $layouts = $this->layouts();

        foreach ($layouts as $layout) {
            if ($width >= $layout['widthMin'] && $width <= $layout['widthMax']
                && $height >= $layout['heightMin'] && $height <= $layout['heightMax']
            ) {
                return $layout;
            }
        }

        return null;
    }

    public function getLayout()
    {
        if (is_null($this->layoutItem)) {
            return 1;
        }

        if (isset($this->layoutItem['size'][$this->format])) {
            return $this->layoutItem['size'][$this->format];
        }

        return 1;
    }

    public function getPreviewPath()
    {
        if (is_null($this->layoutItem)) {
            return asset('images/layouts/305-319_450-475_297x420.png');
        }

        $filename = "{$this->layoutItem['widthMin']}-{$this->layoutItem['widthMax']}_{$this->layoutItem['heightMin']}-{$this->layoutItem['heightMax']}_{$this->format}.png";

        return asset('images/layouts/'.$filename);
    }
}
