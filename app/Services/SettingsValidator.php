<?php

namespace App\Services;

use App\ClientTypes;
use App\Details;
use App\Subdomain;
use App\Urgency;

/**
 * Class SettingsValidator
 * Проверяет обязательные настройки для аккаунта
 *
 * @package App\Services
 */
class SettingsValidator
{
    /**
     * Валидирует настройки и параметры системы
     *
     * @return bool
     */
    public static function validate()
    {
        $details = Details::forSubdomain(
            Subdomain::findCurrent()
        )->count();

        $issuingAddresses = settings()->has('issuingAddresses');

        return $details && $issuingAddresses && ClientTypes::issetTable(); // && Urgency::issetTable() на будущее
    }

    /**
     * Обновляет флаг валидности настроек в найтройках
     *
     * @return mixed
     */
    public static function updateValidationFlag()
    {
        $settings = app('subdomain-settings');
        $settings->set('settings_correct', static::validate());

        return $settings->save();
    }

    /**
     * Проверяет флаг валидности настроек
     *
     * @return mixed
     */
    public static function hasCorrectSettings()
    {
        return settings('settings_correct');
    }
}
