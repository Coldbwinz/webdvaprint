<?php

namespace App\Providers;

use App\OrderStatuses;
use Illuminate\Support\ServiceProvider;
use App\Classes\OrderStatuses\OrderStatusesCollection;

class OrderStatusesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('order-statuses', function ($app) {
            $allStatuses = OrderStatuses::all();

            return new OrderStatusesCollection($allStatuses);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['order-statuses'];
    }
}
