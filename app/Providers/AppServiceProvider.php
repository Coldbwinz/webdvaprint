<?php

namespace App\Providers;

use App\Services\Subdomain;
use App\Classes\Archiver\ZipArchiver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Classes\Archiver\ArchiverContract;
use App\Classes\CurrencyTranslator\CurrencyTranslator;
use App\Classes\CurrencyTranslator\CbrCurrencyTranslator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('subdomain', function ($attribute, $value, $parameters, $validator) {
            return Subdomain::avaiable($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ArchiverContract::class, ZipArchiver::class);

        $this->app->bind(CurrencyTranslator::class, CbrCurrencyTranslator::class);
    }
}
