<?php

namespace App\Providers;

use App\PerformOrder;
use App\User;
use Exception;
use App\Orders;
use App\Clients;
use App\Subdomain;
use App\Order_logs;
use App\GroupOrders;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\GroupOrderWasFormed' => [
            'App\Listeners\Generate1sExportFile',
        ],

        // Выслать письмо с данными акаунта пользователю
        'App\Events\AccountWasCreated' => [
            'App\Listeners\SendAccountDetailsEmail',
        ]
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\StatisticEventListener',
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        $this->registerModelEvents();
    }

    protected function registerModelEvents()
    {
        Orders::creating(function (Orders $order) {
            $index = $order->group->orders->max('position') + 1;
            $order->position = $index;
        });

        GroupOrders::creating(function (GroupOrders $groupOrder) {
            // associate current subdomain to the group
            try {
                $subdomain = Subdomain::findCurrent();
                if (!is_null($subdomain) && is_null($groupOrder->subdomain_id)) {
                    $groupOrder->subdomain()->associate($subdomain);
                }
            } catch (Exception $e) {
            };
        });

        Clients::creating(function (Clients $client) {
            // associate client to the current subdomain
            try {
                $subdomain = Subdomain::findCurrent();
                if (!is_null($subdomain) && is_null($client->subdomain_id)) {
                    $client->subdomain()->associate($subdomain);
                }
            } catch (Exception $e) {
            };
        });

        User::creating(function (User $user) {
            // associate user to the current subdomain
            try {
                $subdomain = Subdomain::findCurrent();
                if (!is_null($subdomain) && is_null($user->subdomain_id)) {
                    $user->subdomain()->associate($subdomain);
                }
            } catch (Exception $e) {
            };
        });

        Order_logs::creating(function (Order_logs $log) {
            // associate log to the current subdomain
            try {
                $subdomain = Subdomain::findCurrent();
                if (!is_null($subdomain) && is_null($log->subdomain_id)) {
                    $log->subdomain()->associate($subdomain);
                }
            } catch (Exception $e) {
            };
        });

        PerformOrder::created(function (PerformOrder $performOrder) {
            if (!$performOrder->number) {
                $performOrder->number = $performOrder->id;
                $performOrder->save();
            }
        });
    }
}
