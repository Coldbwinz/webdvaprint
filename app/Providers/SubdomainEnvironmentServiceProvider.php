<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\SubdomainEnvironment\SubdomainEnvironment;

class SubdomainEnvironmentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $envConfig = config('w2p.subdomain_env_path');

        $this->bindSubdomainEnvironment($envConfig);

        // Merge environment subdomain config with application's config
        $envConfig = $this->app['subdomain-environment']->getConfig();
        foreach ($envConfig as $key => $items) {
            $this->mixConfig($key, $envConfig[$key]);
        }

        // We rebind db.factory to the container, that it update
        // configuration for db connection
        $this->app->singleton('db.factory', function ($app) {
            return new \Illuminate\Database\Connectors\ConnectionFactory($app);
        });
    }

    /**
     * Mixes a given config array to the laravel configs
     *
     * @param $key
     * @param array $config
     */
    protected function mixConfig($key, array $config)
    {
        $baseConfig = $this->app['config']->get($key, []);

        $merged = array_replace_recursive($baseConfig, $config);

        $this->app['config']->set($key, $merged);
    }

    /**
     * @param $envConfig
     */
    protected function bindSubdomainEnvironment($envConfig)
    {
        $this->app->singleton('subdomain-environment', function ($app) use ($envConfig) {
            return new SubdomainEnvironment($app['request']->getHttpHost(), $envConfig);
        });
    }
}
