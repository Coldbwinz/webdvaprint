<?php

namespace App\Providers;

use App\Events\Order\OrderWasClosedWithDiscount;
use App\Events\Order\OrderWasClosedWithRefund;
use App\Models\Salary\SalaryOperation;
use App\Orders;
use App\User;
use Event;
use Carbon\Carbon;
use App\PaymentMethod;
use App\Events\ClientWasBilled;
use App\Events\PaymentWasAdded;
use Illuminate\Support\ServiceProvider;

class SalaryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindListeners();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function bindListeners()
    {
        Event::listen(ClientWasBilled::class, function (ClientWasBilled $event) {
            $client = $event->client;
            $groupOrder = $event->groupOrder;

            $manager = $groupOrder->manager;

            $manager->addSalaryOperation([
                'action' => "Выставлен счет №{$groupOrder->invoice_number} к заказу №{$groupOrder->id}",
                'name' => null,
                'client_id' => null,
                'value' => $groupOrder->price,
                'paid_sum' => null,
                'payment_type' => null,
                'balance' => $groupOrder->price,
                'calculation_to' => Carbon::parse($groupOrder->close_date),
                'accrued' => null,
                'receivable' => null,
            ]);
        });

        Event::listen(PaymentWasAdded::class, function (PaymentWasAdded $event) {
            $amount = $event->amount;
            $groupOrder = $event->groupOrder;
            $manager = $groupOrder->manager;

            $balance = $groupOrder->price - $groupOrder->pay;

            if ($groupOrder->isFullyPayed()) {
                $manager->addSalaryOperation([
                    'action' => "Полностью оплачен заказ №{$groupOrder->id}",
                    'name' => null,
                    'client_id' => null,
                    'value' => $groupOrder->price,
                    'paid_sum' => $amount,
                    'payment_type' => PaymentMethod::getPaymentName($event->payMethod),
                    'balance' => $balance,
                    'calculation_to' => Carbon::parse($groupOrder->close_date),
                    'accrued' => null,
                    'receivable' => null,
                ]);

                /** @var \App\Orders $order */
                foreach ($groupOrder->orders as $order) {

                    $accrued = $this->calculateManagerBonus($manager, $order, $order->price);

                    $manager->addSalaryOperation([
                        'type' => SalaryOperation::SALARY_BONUS,
                        'action' => "№{$groupOrder->id}.{$order->position}",
                        'name' => $order->productTypeName(),
                        'client_id' => $order->client->id,
                        'value' => $order->price,
                        'paid_sum' => null,
                        'payment_type' => PaymentMethod::getPaymentName($event->payMethod),
                        'balance' => $balance,
                        'calculation_to' => Carbon::parse($groupOrder->close_date),
                        'accrued' => $accrued,
                        'receivable' => null,
                    ]);
                }

            } else {
                $manager->addSalaryOperation([
                    'action' => "Частично оплачен заказ №{$groupOrder->id}",
                    'name' => null,
                    'client_id' => null,
                    'value' => $groupOrder->price,
                    'paid_sum' => $amount,
                    'payment_type' => PaymentMethod::getPaymentName($event->payMethod),
                    'balance' => $balance,
                    'calculation_to' => Carbon::parse($groupOrder->close_date),
                    'accrued' => null,
                    'receivable' => null,
                ]);
            }
        });

        // Закрытие заказа за скидкой
        Event::listen(OrderWasClosedWithDiscount::class, function (OrderWasClosedWithDiscount $event) {
            $order = $event->order;
            $manager = $order->manager;

            $accrued = $this->calculateManagerBonus($manager, $order, $event->discountAmount);
            $accrued = -$accrued;

            $manager->addSalaryOperation([
                'type' => SalaryOperation::SALARY_DEDUCTION,
                'action' => "Заказ №{$order->number} выдан со скидкой ({$event->discountAmount} руб.).",
                'name' => 'возврат по браку',
                'client_id' => $order->client->id,
                'value' => null,
                'accrued' => $accrued,
            ]);
        });

        Event::listen(OrderWasClosedWithRefund::class, function (OrderWasClosedWithRefund $event) {
            $order = $event->order;
            $manager = $order->manager;

            $accrued = $this->calculateManagerBonus($manager, $order, $order->price);
            $accrued = -$accrued;

            $manager->addSalaryOperation([
                'type' => SalaryOperation::SALARY_DEDUCTION,
                'action' => "Заказ №{$order->number} закрыт с возвратом денег ({$order->price} руб.)",
                'name' => 'возврат по браку',
                'client_id' => $order->client->id,
                'value' => $order->price,
                'accrued' => $accrued,
            ]);
        });
    }

    protected function calculateManagerBonus(User $manager, Orders $order, $price)
    {
        $bonus = 0;

        try {
            $bonus = $manager->group->salaryBonuses()
                ->where('product_type_id', $order->productType->id)
                ->where('client_type_id', $order->client->type->id)
                ->first()->value;
        } catch (\Exception $e) {}

        return $price * $bonus / 100;
    }
}
