<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\FilenameSplitter\Splitter;

class FilenameSplitterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('filename-splitter', function ($app) {
            $config = $app['config']['filename_splitter'];

            return new Splitter($config['templates']);
        });
    }
}
