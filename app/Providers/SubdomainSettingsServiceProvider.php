<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\SubdomainSettings\SubdomainSettings;
use App\Classes\SubdomainSettings\SubdomainSettingsDatabaseStorage;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsStorage;
use App\Classes\SubdomainSettings\Contractors\SubdomainSettingsContractor;

class SubdomainSettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerStorage();

        $this->registerSubdomainSettings();
    }

    /**
     * Get subdomain
     *
     * @param $host
     * @return bool|string
     */
    protected function getSubdomain($host)
    {
        $parts = explode('.', $host);

        if (count($parts) === 3) {
            return $parts[0];
        }

        return false;
    }

    /**
     * Register subdomain settings storage
     */
    public function registerSubdomainSettings()
    {
        $this->app->singleton(SubdomainSettingsContractor::class, function () {
            $storage = $this->app->make(SubdomainSettingsStorage::class);
            $host = $this->app->make('request')->getHttpHost();
            $subdomain = $this->getSubdomain($host) ?: 'default';

            return new SubdomainSettings($storage, $subdomain);
        });

        $this->app->alias(SubdomainSettingsContractor::class, 'subdomain-settings');
    }

    /**
     * Register storage for settings
     */
    public function registerStorage()
    {
        $this->app->singleton(SubdomainSettingsStorage::class, SubdomainSettingsDatabaseStorage::class);
    }
}
