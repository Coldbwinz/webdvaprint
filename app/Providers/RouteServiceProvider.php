<?php

namespace App\Providers;

use App\GroupOrders;
use App\Models\Equipment\Equipment;
use App\Models\Material\Material;
use App\OrderElement;
use App\Orders;
use App\PerformOrder;
use App\TemplateLibrary;
use App\User;
use App\UserGroup;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Request;
use App\Clients;
use App\Contacts;
use App\ContactFiles;
use App\Contractor;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('contractors', Contractor::class);
        $router->bind('client', function ($id) {
            $client = Clients::find($id);
            if ( $client === NULL && !Request::ajax()) {
                abort(404, 'Клиент не найден');
            }
            return $client;
        });

        $router->bind('contact', function ($id) {
            return Contacts::findOrFail($id);
        });

        $router->bind('file', function ($id) {
            $file = ContactFiles::find($id);
            if ( $file === NULL && !Request::ajax()) {
                abort(404, 'Файл не найден');
            }
            return $file;
        });

        $router->model('element_id', OrderElement::class);
        $router->model('rework_order_id', Orders::class);

        $router->model('material', Material::class);
        $router->model('equipment', Equipment::class);
        $router->model('perform_order', PerformOrder::class);
        $router->model('employee', User::class);
        $router->model('user', User::class);
        $router->model('group', UserGroup::class);
        $router->model('order', Orders::class);
        $router->model('template', TemplateLibrary::class);

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });

        $router->group(['namespace' => $this->namespace.'\Api', 'prefix' => 'api'], function ($router) {
            require app_path('Http/routes_api.php');
        });
    }
}
