<?php

namespace App\Providers;

use App\Contacts;
use Setting;
use App\Clients;
use Illuminate\Support\ServiceProvider;

class ViewComposeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('contractors.contractors.partials.contacts_select', function ($view) {
            $view->with('contacts', Contacts::all());
        });

        $settings = $this->app->make('subdomain-settings');

        view()->share('logo', $settings->get('logo'));
        view()->share('colorPrimary', $settings->get('color_primary'));
        view()->share('colorSecondary', $settings->get('color_secondary'));

        view()->composer('clients/new_contact', function ($view) {
            $clients = Clients::companies()->get();
            $view->with('clients', $clients);
        });

        view()->composer('clients/new_client', function ($view) {
            $contacts = Contacts::all();
            $view->with('contacts', $contacts);
        });

        view()->composer('dashboard/top-panel', 'App\Http\ViewComposers\DashboardPanelComposer');

        view()->composer('admin/menu/partner', function ($view) {
            $ordersHistoryCount = \App\Orders::where('client_id', auth()->user()->client->id)
                ->whereIn('status', \App\OrderStatuses::$cabinet_group['user_orders'])
                ->count();

            $view->with('ordersHistoryCount', $ordersHistoryCount);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
