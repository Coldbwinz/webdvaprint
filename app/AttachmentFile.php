<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AttachmentFile extends Model
{

    use SoftDeletes;

    /**
     * [$table description]
     * @var string
     */
    protected $table = 'attachment_files';

    /**
     * [$fillable description]
     * @var array
     */
    protected $fillable = ['user_id', 'order_id', 'specification_id', 'type', 'filename', 'path',
        'original_name', 'mime_type', 'size', 'width', 'height',
        'title', 'description', 'url', 'as_link', 'preview'
    ];

    /**
     * [$dates description]
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The User who owns this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * The Order who associate with this file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id');
    }

    /**
     * Specification parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specification()
    {
        return $this->belongsTo(Specification::class, 'specification_id');
    }

}
