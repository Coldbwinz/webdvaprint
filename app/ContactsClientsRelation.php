<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsClientsRelation extends Model
{
    protected $table = 'contacts_clients_relation';
    public $timestamps = false;
    
    public function client(){
        return $this->belongsTo('App\Clients', 'client_id', 'id');
    }
}
