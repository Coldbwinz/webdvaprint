<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;

class ProductType extends Model
{
    public $table = 'product_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'name', 'printler_type', 'url', 'activity', 'created_auto', 'services',
        'product_1c_key',
    ];

    /**
     * Настройки экспорта
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exportSettings()
    {
        return $this->hasMany(ExportSetting::class);
    }

    public function createAuto($fill_calculation) {
        $productType = ProductType::where('name',$fill_calculation->selected_product_type_name)->first();
        $selected_index = $fill_calculation->real_price_index;
        $prices = null;
        if (count($productType) > 0) {
            $prices = json_decode($productType->services);
            if (isset($prices->$selected_index)) {
                return $productType->id;
            }
        } else {
            $productType = new ProductType;
            $productType->name = $fill_calculation->selected_product_type_name;
            $productType->printler_type = $fill_calculation->printler_type;
            $productType->created_auto = 1;
            $productType->url = '';
            $prices = new \stdClass();
            $prices->counts = [$fill_calculation->draw];
            $prices->$selected_index = $this->saveCalculation($fill_calculation);
            $productType->services = json_encode($prices);
            $productType->save();
        }
        return $productType->id;
    }

    public function saveCalculation($fill_calculation) {
            $tempPrice = new \stdClass();
            $tempPrice->size = $fill_calculation->selected_size;
            $tempPrice->width = $fill_calculation->width;
            $tempPrice->height = $fill_calculation->height;
            $tempPrice->type = $fill_calculation->selected_type;
            $tempPrice->material = $fill_calculation->selected_material;
            $tempPrice->chromacity = $fill_calculation->selected_chromacity;
            $tempPrice->counts = [$fill_calculation->draw];
            $tempPrice->products = [$fill_calculation->selected_product];
            $tempPrice->options = $fill_calculation->selected_options;
            $tempPrice->products_prices = [0];
            $tempPrice->options_prices = [0];
            $tempPrice->temp_template_cost = $fill_calculation->template_cost;
            $tempPrice->temp_price = $fill_calculation->price;

            $tempPrice->price_template_employee = 0;
            $tempPrice->price_template_self = 0;
            $tempPrice->price_customer_template_employee = 0;
            $tempPrice->price_customer_template_auto = 0;
            $tempPrice->price_designer_together = 0;
            $tempPrice->price_designer_together_selected = 0;
            $tempPrice->price_designer_remote = 0;
            $tempPrice->days_designer_remote = 0;
            $tempPrice->weekends_sign = 0;

            if ($fill_calculation->printler_type == 3) {
                $tempPrice->covers_prices = new \stdClass();
                if ($fill_calculation->selected_cover != 'Без обложки') {
                    $tempPrice->covers = [$fill_calculation->selected_cover];
                    $cover_chromacity_index = $fill_calculation->selected_cover_chromacity;
                    $tempPrice->covers_prices->$cover_chromacity_index = [0];
                } else {
                    $tempPrice->covers = [];
                }

                $tempPrice->pads_prices = new \stdClass();
                if (($tempPrice->type == 'на пружине') && ($fill_calculation->selected_pad != 'Без подложки')) {
                    $tempPrice->pads = [$fill_calculation->selected_pad];
                    $pad_chromacity_index = $fill_calculation->selected_cover_chromacity;
                    $tempPrice->pads_prices->$pad_chromacity_index = [0];
                } else {
                    $tempPrice->pads = [];
                }

            }
            $tempPrice->active = 2; //создан автоматически
            return $tempPrice;
    }

    public static function issetTable()
    {
        $urgency_count = Urgency::where('subdomain_id', null)->count();
        $result = new \stdClass();
        $product_types = ProductType::all();
        foreach ($product_types as $product) {
            if (isset($product->services)) {
                $product_services = json_decode($product->services);
                if (count($product_services) > 0) {
                    foreach ($product_services as $key => $service) {
                        if ($key != 'counts') {
                            $good = true;
                            if (!isset($service->urgencies)) {
                                $good = false;
                            } else if (is_array($service->urgencies) && (count($service->urgencies) == 0)) {
                                $good = false;
                            } else if ($service->urgencies[1] && !is_array($service->urgencies[1])) {
                                $good = false;
                            } else if (!(isset($service->price_template_employee) && isset($service->price_template_self) &&
                                isset($service->price_customer_template_employee) && isset($service->price_customer_template_auto) &&
                                isset($service->price_designer_together) && isset($service->price_designer_remote))
                            ) {
                                $good = false;
                            } else {
                                $filled_urgency_count = 0;
                                if (isset($product_services->counts)) {
                                    $total_count = $urgency_count * count($product_services->counts);
                                    foreach ($service->urgencies as $urgency_line) {
                                        if ($good && is_array($urgency_line)) {
                                            foreach ($urgency_line as $urgency_item) {
                                                if (!(isset($urgency_item->dead_line) && isset($urgency_item->deploy_time))) {
                                                    $good = false;
                                                    break;
                                                }
                                                $filled_urgency_count++;
                                            }
                                        }
                                    }
                                    if ($total_count != $filled_urgency_count) {
                                        $good = false;
                                    }
                                } else {
                                    $good = false;
                                }
                            }
                            //active == 2 это созданный автоматически продукт, поэтому добавляем в исключения
                            if (!$good && $service->active != 2) {
                                $result->selected = new \stdClass();
                                $result->selected->size = $service->size;
                                $result->selected->type = $service->type;
                                $result->selected->chromacity = $service->chromacity;
                                $result->selected->material = $service->material;
                                $result->url = '/products/price/' . $product->id;
                                return $result;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public static function getWorkProducts() {
        if (settings('1c_sync')) {
            $product_types = ProductType::where('services', '!=', '')->where('activity', 1)->whereNotNull('product_1c_key')->get();
        } else {
            $product_types = ProductType::where('services', '!=', '')->where('activity', 1)->get();
        }
        foreach ($product_types as $key => $product) {
            $services = json_decode($product->services);
            $count_good = 0;
            foreach ($services as $key2 => $service_item) {
                if ($key2 != 'counts') {
                    if ($service_item->active == 1) {
                        $count_good++;
                    }
                }
            }
            if (($count_good == 0) || (strlen($product->url) == 0)) {
                unset($product_types[$key]);
            }
        }
        return $product_types;
    }

    public static function productTypeSameByCalculation($fill_calculation) {
        if (is_string($fill_calculation)) {
            $fill_calculation = json_decode($fill_calculation);
        } else {
            $fill_calculation = (object) $fill_calculation;
        }
        if ($fill_calculation->price_index == 'manual') {
            $product_types = ProductType::where('name', $fill_calculation->selected_product_type_name)->where('activity', 1)->get();
            if (count($product_types) > 0) {
                foreach ($product_types as $product) {
                    if (strlen($product->services) > 10) {
                        foreach (json_decode($product->services) as $product_services_item) {
                            if (is_object($product_services_item)) {
                                if (($product_services_item->active == 1) && ($product_services_item->price_designer_remote > 0)) {
                                    return $product_services_item;
                                }
                            }
                        }
                    }
                }
            }
            $product_types = ProductType::where('printler_type', $fill_calculation->printler_type)->where('activity', 1)->get();
            foreach ($product_types as $product) {
                if (strlen($product->services) > 10) {
                    foreach (json_decode($product->services) as $product_services_item) {
                        if (is_object($product_services_item)) {
                            if (($product_services_item->active == 1) && ($product_services_item->price_designer_remote > 0)) {
                                return $product_services_item;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
