<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesParams extends Model
{
    public $table = 'services_params';
}
