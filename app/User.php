<?php

namespace App;

use App\Models\Statistic\StatisticClient;
use App\Models\Statistic\StatisticOrder;
use App\Traits\UserRoles;
use App\Traits\UserSalary;
use App\Traits\UserAccruable;
use App\Traits\SubdomainTrait;
use Illuminate\Auth\Authenticatable;
use App\Models\Salary\SalaryOperation;
use App\Traits\LocalizedEloquentDates;
use Illuminate\Database\Eloquent\Model;
use App\Models\Statistic\StatisticInvoice;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, LocalizedEloquentDates,
        SubdomainTrait, UserRoles, UserSalary;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'id_group', 'subdomain_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @return mixed
     */
    public function accessLevel() {
        return $this->id_group;
    }

    /**
     * Get full name of the user
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->lastname;
    }

    /**
     * Group of the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(UserGroup::class, 'id_group');
    }

    /**
     * Check for user belongs to the group
     *
     * @param int|array $groupId
     * @return bool
     */
    public function hasGroup($groupId)
    {
        if (is_array($groupId)) {
            return in_array($this->group->id, $groupId);
        }

        return $this->group->id == $groupId;
    }

    /**
     * Check for user has not group
     *
     * @param $groupId
     * @return bool
     */
    public function hasNotGroup($groupId)
    {
        return ! $this->hasGroup($groupId);
    }

    /**
     * Check for user belongs to the group (by Role name)
     * Example:
     *      $user->is('admin')
     *      $user->is(['admin', 'root'])
     *
     * Magic:
     *      $user->isAdmin();
     *
     * @param int|array $role
     * @return bool
     */
    public function is($role)
    {
        if (is_array($role)) {
            return in_array($this->group->role, $role);
        }

        return $this->group->role == $role;
    }

    /**
     * Check for user is not belongs to the group (by Role name)
     * Example:
     *      $user->isNot('admin')
     *      $user->isNot(['admin', 'root'])
     *
     * Magic:
     *      $user->isNotAdmin();
     *
     * @param int|array $groupId
     * @return bool
     */
    public function isNot($role)
    {
        if (is_array($role)) {
            return ! in_array($this->group->role, $role);
        }

        return $this->group->role != $role;
    }

    /**
     * Contact, related to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contact()
    {
        return $this->hasOne(Contacts::class);
    }

    /**
     * Client, assigned to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(Clients::class);
    }

    /**
     * Attachment files by this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(AttachmentFile::class, 'user_id');
    }

    /**
     * Subdomain for user's account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdomain()
    {
        return $this->belongsTo(Subdomain::class);
    }

    /**
     * [displayedName description]
     *
     * @return string
     */
    public function displayedName()
    {
        $name = !is_null($this->contact) ? $this->contact->name : $this->name;
        $lastName = !is_null($this->contact) ? $this->contact->last_name : $this->last_name;

        return $name.' '.$lastName;
    }

    /**
     * Handle dynamic calls into macros or pass missing methods to the store.
     *
     * @param  string  $method
     * @param  array   $arguments
     * @return mixed
     */

    public function __call($method, $arguments)
    {
        $return = $this->handleMagicRole($method, $arguments);
        return (!is_null($return)) ? $return : parent::__call($method, $arguments);
    }

    /**
     * Get user login link
     *
     * @return string
     */
    public function loginLink()
    {
        return url("auth/login_token/{$this->email}/{$this->login_token}");
    }

    /**
     * Ссылка на установление нового пароля
     *
     * @return string
     */
    public function passwordResetLink()
    {
        $passwordReset = PasswordReset::where('email', $this->email)
            ->where('user_id', $this->id)
            ->first();

        return route('auth.password.create', $passwordReset->token);
    }

    /**
     * Scope manager users
     *
     * @param $query
     */
    public function scopeManagers($query)
    {
        $query->where('id_group', UserGroup::MANAGER);
    }

    /**
     * Статистика пользователя по счетам
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statisticInvoices()
    {
        return $this->hasMany(StatisticInvoice::class, 'manager_id');
    }

    /**
     * Статистика по клиентам
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statisticClients()
    {
        return $this->hasMany(StatisticClient::class, 'manager_id');
    }

    /**
     * Статистика по заказам
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statisticOrders()
    {
        return $this->hasMany(StatisticOrder::class, 'manager_id');
    }

    /**
     * Заказы пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'manager_id');
    }

    /**
     * Группы заказов пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupOrders()
    {
        return $this->hasMany(GroupOrders::class, 'manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates()
    {
        return $this->hasMany(TemplateLibrary::class, 'owner_id');
    }

    /**
     * @param string $value
     * @return string
     */
    public function getPhotoAttribute($value)
    {
        $default = asset('images/nophoto.png');

        return $value ? asset('upload/users/'.$value) : $default;
    }
}
