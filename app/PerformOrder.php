<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\UploadDescentRequest;
use App\Events\PerformOrder\PerformOrderWasPaused;
use App\Events\PerformOrder\PerformOrderWasResumed;

class PerformOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product', 'chromacity', 'print_sheet_format', 'draw', 'material', 'postpress_processes',
        'issue_at', 'descent_path', 'templates_path', 'perform_order_status_id', 'preview', 'number',
        'single', 'collect_order_id', 'collected', 'revisioned', 'revision_comment', 'paused', 'paused_reason',
        'resumed_reason', 'paused_from',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'postpress_processes' => 'array',
        'single' => 'boolean',
        'collected' => 'boolean',
        'revisioned' => 'boolean',
        'paused' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['issue_at'];

    /**
     * Perform order status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function performOrderStatus()
    {
        return $this->belongsTo(PerformOrderStatus::class);
    }

    /**
     * Order id для сборки разложенных элементов
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function collectOrder()
    {
        return $this->belongsTo(Orders::class, 'collect_order_id');
    }

    /**
     * Filter
     *
     * @param $query
     * @param Request $request
     * @return mixed
     */
    public function scopeFilter($query, Request $request)
    {
        if ($request->has('status_id')) {
            $statusId = $request->input('status_id');

            if (is_array($statusId)) {
                return $query->whereIn('perform_order_status_id', $statusId);
            }

            $query->where('perform_order_status_id', $statusId);
        }
    }

    /**
     * @param $query
     * @param PerformOrderStatus $status
     * @return mixed
     */
    public function scopeWithStatus($query, PerformOrderStatus $status)
    {
        return $query->whereHas('performOrderStatus', function ($query) use ($status) {
            $query->where('id', $status->id);
        });
    }

    /**
     * @param $performOrderStatusId
     */
    public function setStatus($performOrderStatusId)
    {
        $this->perform_order_status_id = $performOrderStatusId;
        $this->save();
    }

    /**
     * Set status to all related elements
     *
     * @param OrderStatuses $status
     * @return mixed
     */
    public function setElementsStatus(OrderStatuses $status)
    {
        return $this->elements->each(function (OrderElement $element) use ($status) {
            $element->status()->associate($status);
            $element->save();
        });
    }

    /**
     * Attach descent file
     *
     * @param UploadDescentRequest $request
     * @return string
     */
    public function attachDescent(UploadDescentRequest $request)
    {
        if ($request->has('link')) {
            $this->preview = $this->makeThumbnailWithText($this->preview, "{$this->product} №{$this->id}");
            $this->descent_path = $request->input('link');
            $this->perform_order_status_id = 2;
            $this->save();

            return $request->input('link');
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $destination = public_path('upload/descents/');
            $basename = str_random();
            $filename = $basename.'.'.$file->getClientOriginalExtension();

            if (! is_dir($destination)) {
                mkdir($destination, 0777, true);
            }

            $file->move($destination, $filename);

            // make preview of the file
            $preview = app(\App\Classes\Preview\Preview::class)
                ->generate('upload/descents/'.$filename, null, [
                    'width' => 500,
                    'height' => null
                ]);

            $this->preview = $this->makeThumbnailWithText(public_path($preview), "{$this->product} №{$this->id}");
            $this->descent_path = asset('upload/descents/'.$filename);
            $this->perform_order_status_id = 2;
            $this->save();
        }

        return asset('upload/descents/'.$filename);
    }

    /**
     * @param $value
     * @return static
     */
    public function getIssueAtAttribute($value)
    {
        return Carbon::parse($value);
    }

    /**
     * Close perform order
     */
    public function close()
    {
        $this->delete();
    }

    /**
     * @param $value
     * @return string
     */
    public function getPreviewAttribute($value)
    {
        if (is_null($value)) {
            return '/images/thumb.png';
        }

        return $value;
    }

    /**
     * Элементы
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function elements()
    {
        return $this->hasMany(OrderElement::class);
    }

    /**
     * Complete postpress operations in perform order.
     *
     * @param array $postpress
     * @return int count of the completed operations for elements
     */
    public function completePostpressOperations(array $postpress)
    {
        $this->postpress_processes = array_diff($this->postpress_processes, $postpress);
        $this->save();

        $elementCompletedCount = 0;

        foreach ($this->elements as $element) {
            $element->completePostpress($postpress) && $elementCompletedCount++;
        }

        return $elementCompletedCount;
    }

    /**
     * Создаёт наряд для одного элемента
     *
     * @param OrderElement $element
     * @param array $performOrderData
     * @return static
     */
    public static function createForElement(OrderElement $element, $performOrderData = [])
    {
        $performOrderData = array_merge([
            'product' => $element->name,
            'chromacity' => $element->chromacity,
            'print_sheet_format' => $element->format,
            'draw' => $element->draw,
            'material' => $element->material,
            'postpress_processes' => $element->postpress,
            'issue_at' => Carbon::createFromTimestamp($element->order->close_date),
            'perform_order_status_id' => PerformOrderStatus::PRINTED,
            'preview' => '/lib/frx/' . $element->order->img,
            'single' => true,
        ], $performOrderData);

        $performOrder = static::create($performOrderData);

        $element->performOrder()->associate($performOrder);
        $element->save();

        return $performOrder;
    }

    /**
     * Обновляет флаг целостности наряда. Если в наряде находятся все элементы
     * заказа, возвращает true. В противном случае возвращает false
     *
     * @return bool
     */
    public function updateCollectedField()
    {
        if (! $this->collectOrder) {
            return false;
        }

        $orderElementIds = $this->collectOrder->elements->pluck('id')->toArray();
        $performOrderElementIds = $this->elements->pluck('id')->toArray();
        $this->collected = count(array_diff($orderElementIds, $performOrderElementIds)) === 0;
        $this->save();

        return $this->collected;
    }

    public function associateElement(OrderElement $element)
    {
        $element->performOrder()->associate($this);
        $element->save();
    }

    /**
     * Устанавливает статус у заказа для всех элементов наряда
     *
     * @param OrderStatuses $status
     */
    public function setOrderElementsStatus(OrderStatuses $status)
    {
        foreach ($this->elements as $element) {
            $element->order->status = $status->id;
            $element->order->save();
        }
    }

    /**
     * Наряд отпечатан
     */
    public function printed()
    {
        $this->setStatus(PerformOrderStatus::PRINTED);
        $this->setElementsStatus(status('in_postpress'));
        $this->setOrderElementsStatus(status('in_postpress'));

        // Логи в карточку заказа
        // @todo вынести в event
        $this->elements->each(function ($element) {
            addLog($element->order->id, 'Отпечатан', null, '', null, 0, $element->name);
        });
    }

    /**
     * Возвращает true если наряд на элементы всего изделия и false,
     * если наряд на элементы, не связанные с изделием
     *
     * @return bool
     */
    public function isForProduct()
    {
        return ! is_null($this->collectOrder);
    }

    /**
     * Завершает наряд
     *
     * @param Orders $order
     */
    public function complete(Orders $order)
    {
        DB::transaction(function () use ($order) {
            addLog($order->id, 'Контроль качества', $order->group->manager_id, '', null, 0, '');
            foreach ($this->elements as $element) {
                $element->performOrder()->dissociate();
                $element->status_id = status('ready', 'id');
                $element->save();
            }

            $order->status = status('ready', 'id');
            $order->save();

            $this->close();
        });
    }

    /**
     * Pause perform order processing with its element's orders
     *
     * @param string|null $reason
     * @param bool $pauseOrder
     * @param null $pausedFrom
     */
    public function pause($reason = null, $pauseOrder = true, $pausedFrom = null)
    {
        $this->paused = true;
        $this->paused_reason = $reason;
        $this->save();

        event(new PerformOrderWasPaused($this));

        // pause element's orders
        if ($pauseOrder) {
            $this->elements->each(function (OrderElement $element) use ($reason) {
                $element->order->pause($reason);
            });
        }

        $this->paused_from = $pausedFrom;
        $this->save();
    }

    /**
     * Resume perform order processing with its element's orders
     *
     * @param string|null $reason
     * @param bool $resumeOrder
     */
    public function resume($reason = null, $resumeOrder = true)
    {
        $this->paused = false;
        $this->resumed_reason = $reason;
        $this->save();

        event(new PerformOrderWasResumed($this));

        // pause element's orders
        if ($resumeOrder) {
            $this->elements->each(function (OrderElement $element) use ($reason) {
                $element->order->resume($reason);
            });
        }
    }

    /**
     * Make thumbnail with text from image
     *
     * @param string $imagePath path to image
     * @param string $text text on image
     *
     * @return string relative path to the created thumbnail
     */
    public function makeThumbnailWithText($imagePath, $text)
    {
        $fontSize = 40;
        $fontFile = base_path('resources/assets/fonts/OpenSans.ttf');
        $rectangleHeight = $fontSize * 1.3;
        $image = \Image::make($imagePath);
        $image->rectangle(0, round($image->height() / 2 - $rectangleHeight / 2), $image->width(), round($image->height() / 2 - $rectangleHeight / 2) + $rectangleHeight, function ($draw) {
            $draw->background('#FFF');
        });
        $image->text($text, round($image->width() / 2), round($image->height() / 2), function($font) use ($fontFile, $fontSize) {
            $font->file($fontFile);
            $font->size($fontSize);
            $font->color('#000');
            $font->valign('center');
            $font->align('center');
        });

        $filename = strtolower(str_random()) . '.png';
        $path = "/upload/descents/{$filename}";

        $image->save(public_path($path));

        return $path;
    }
}
