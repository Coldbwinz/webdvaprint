<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParserFiles extends Model
{
    const STATUS_PARSED = 'move';

    /**
     * Fillable fields for mass assigment
     *
     * @var array
     */
    protected $fillable = [
        'name', 'filename', 'path', 'status', 'hidden', 'upload_folder',
        'preview', 'errors', 'vendor', 'number', 'type', 'side', 'backside_id'
    ];

    /**
     * Scope all parsed files
     *
     * @param $query
     * @return mixed
     */
    public function scopeParsed($query)
    {
        return $query->where('status', static::STATUS_PARSED);
    }

    /**
     * Backside of the template
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function backside()
    {
        return $this->hasOne(ParserFiles::class, 'id', 'backside_id');
    }

    /**
     * Get full path to frx file
     *
     * @return string
     */
    public function getFrxPath()
    {
        return $this->upload_folder.'/'.$this->filename . '.frx';
    }
}
