<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatuses extends Model
{
    protected $table = 'order_statuses';

    public static $cabinet_group = [
        'need_attention' => [5, 9, 10, 51, 55, 56, 201, 2000, 2010],
        'user_orders' => [2, 12, 25, 30, 35, 40, 45, 52, 53, 57, 58, 100, 102, 112, 301, 1010],
        'payments' => [1, 7, 8],
    ];

}
