@servers(['dev' => 'w2p@13.94.207.245'])

@task('deploy', ['on' => 'dev'])
    cd /datadrive/sites/www/dev
    sudo git pull
    sudo composer dumpautoload
    sudo php artisan migrate
    sudo php artisan db:seed --class=Fill_order_statuses
    sudo php artisan db:seed --class=EquipmentPrintTypesTableSeeder
    sudo composer install
    sudo npm install
    sudo npm run production
    sudo supervisorctl reload
    sudo chown -R www-data:www-data /datadrive/sites/www/dev
@endtask
