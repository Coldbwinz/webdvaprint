<?php

use Carbon\Carbon;
use App\Models\Statistic\StatisticInvoice;

class StatisticInvoicesTest extends TestCase
{
    protected $manager;

    public function setUp()
    {
        parent::setUp();

        $this->manager = $this->signIn();

        StatisticInvoice::truncate();
    }

    /** @test */
    function it_returns_right_invoices_company_total()
    {
        $otherManager = create('App\User');

        create(StatisticInvoice::class, ['invoice_amount' => 10, 'created_at' => Carbon::now()], 2);

        create(StatisticInvoice::class, [
            'invoice_amount' => 10,
            'created_at' => Carbon::now(),
            'manager_id' => $otherManager->id,
        ], 2);

        $period = new \App\Services\CarbonDatePeriod(Carbon::now()->subDay(), Carbon::now());

        $this->assertEquals(StatisticInvoice::invoicesSum($period), 40);
    }

    /** @test */
    function it_returns_right_invoices_manager_total()
    {
        $secondManager = create('App\User');

        create(StatisticInvoice::class, ['invoice_amount' => 10, 'created_at' => Carbon::now()], 2);

        create(StatisticInvoice::class, [
            'invoice_amount' => 30,
            'created_at' => Carbon::now(),
            'manager_id' => $secondManager->id,
        ], 2);

        $period = new \App\Services\CarbonDatePeriod(Carbon::now()->subDay(), Carbon::now());

        $this->assertEquals(StatisticInvoice::invoicesSum($period, $secondManager->id), 60);
    }

    /** @test */
    function it_returns_right_paid_invoices_count()
    {
        $secondManager = create('App\User');
        $period = new \App\Services\CarbonDatePeriod(Carbon::now()->subDay(), Carbon::now());
        $firstManagerPaidInvoicesCount = 2;
        $secondManagerPaidInvoicesCount = 3;

        create(StatisticInvoice::class, ['full_paid' => true, 'created_at' => Carbon::now()], 2);
        create(StatisticInvoice::class, [
            'full_paid' => true,
            'created_at' => Carbon::now(),
            'manager_id' => $secondManager->id,
        ], 3);

        $this->assertEquals($firstManagerPaidInvoicesCount, StatisticInvoice::paidInvoicesCount($period, $this->manager->id));
        $this->assertEquals($secondManagerPaidInvoicesCount, StatisticInvoice::paidInvoicesCount($period, $secondManager->id));
    }

    /** @test */
    function it_get_expected_amount_sum_for_company()
    {
        create(StatisticInvoice::class, [
            'expected_amount' => 10,
            'created_at' => Carbon::create(2017, 5, 1)
        ]);

        create(StatisticInvoice::class, [
            'expected_amount' => 10,
            'created_at' => Carbon::create(2017, 5, 2),
        ]);

        create(StatisticInvoice::class, [
            'expected_amount' => 10,
            'created_at' => Carbon::create(2017, 5, 5),
        ]);

        $firstPeriod = new \App\Services\CarbonDatePeriod(Carbon::create(2017, 5, 1), Carbon::create(2017, 5, 3));
        $secondPeriod = new \App\Services\CarbonDatePeriod(Carbon::create(2017, 5, 1), Carbon::create(2017, 5, 5));

        $this->assertEquals(StatisticInvoice::expectedAmountSum($firstPeriod), 20);
        $this->assertEquals(StatisticInvoice::expectedAmountSum($secondPeriod), 30);
    }
}
