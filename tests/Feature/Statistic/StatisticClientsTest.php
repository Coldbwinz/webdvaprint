<?php

use App\Services\CarbonDatePeriod;
use Carbon\Carbon;
use App\Models\Statistic\StatisticClient;

class StatisticClientsTest extends TestCase
{
    protected $manager;

    public function setUp()
    {
        parent::setUp();

        $this->manager = $this->signIn();

        StatisticClient::truncate();
    }

    /** @test */
    function it_returns_a_valid_new_clients_count()
    {
        create('App\Models\Statistic\StatisticClient', [
            'new_client' => true,
            'created_at' => Carbon::createFromDate(2017, 5, 20),
        ], 2);

        create('App\Models\Statistic\StatisticClient', [
            'new_client' => true,
            'created_at' => Carbon::createFromDate(2017, 5, 2),
        ], 2);

        $periodWeek = new CarbonDatePeriod(Carbon::createFromDate(2017, 5, 15), Carbon::createFromDate(2017, 5, 22));
        $this->assertEquals(2, StatisticClient::newClientsCount($periodWeek, $this->manager->id));

        $periodMonth = new CarbonDatePeriod(Carbon::createFromDate(2017, 5, 1), Carbon::createFromDate(2017, 5, 30));
        $this->assertEquals(4, StatisticClient::newClientsCount($periodMonth, $this->manager->id));
    }
}
