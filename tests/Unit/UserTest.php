<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /** @test */
    function it_has_a_statistic_invoices()
    {
        $user = create('App\User');
        create('App\Models\Statistic\StatisticInvoice', [
            'manager_id' => $user->id
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $user->statisticInvoices);
    }
}
