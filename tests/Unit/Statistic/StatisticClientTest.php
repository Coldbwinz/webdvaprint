<?php

use App\Models\Statistic\StatisticClient;

class StatisticClientTest extends TestCase
{
    /** @test */
    function it_has_a_current_authentificated_user_if_manager_not_defined()
    {
        $manager = $this->signIn();

        $record = create('App\Models\Statistic\StatisticClient', [
            'manager_id' => null,
        ]);

        $this->assertEquals($record->manager_id, $manager->id);
    }

    /** @test */
    function it_fetch_records_only_of_the_given_manager()
    {
        $this->signIn();

        $firstManager = create('App\User');
        $secondManager = create('App\User');

        StatisticClient::truncate();

        create('App\Models\Statistic\StatisticClient', [
            'manager_id' => $firstManager->id,
        ]);

        create('App\Models\Statistic\StatisticClient', [
            'manager_id' => $secondManager->id,
        ]);

        $records = StatisticClient::forManager($firstManager->id)->get();
        $this->assertEquals($records->count(), 1);

        $records = StatisticClient::all();
        $this->assertEquals($records->count(), 2);
    }
}
