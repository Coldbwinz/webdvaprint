<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CarbonDatePeriodTest extends TestCase
{
    /** @test */
    function it_returns_a_valid_start_and_end_dates()
    {
        $start = Carbon\Carbon::parse('01.05.2017 23:32:12');
        $end = Carbon\Carbon::parse('01.06.2017 15:05:23');

        $period = new \App\Services\CarbonDatePeriod($start, $end);

        $this->assertEquals('01.05.2017 00:00:00', $period->getStartDate()->format('d.m.Y H:i:s'));
        $this->assertEquals('01.06.2017 23:59:59', $period->getEndDate()->format('d.m.Y H:i:s'));
    }
}
