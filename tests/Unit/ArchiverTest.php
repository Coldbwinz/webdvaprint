<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArchiverTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->archiver = $this->app->make(\App\Classes\Archiver\ArchiverContract::class);
        $this->archiveFile = storage_path('testarchive.zip');
    }

    public function tearDown()
    {
        parent::tearDown();

        if (file_exists($this->archiveFile)) {
            unlink($this->archiveFile);
        }
    }

    /** @test */
    public function it_creates_new_archive()
    {
        $file = storage_path('app/testfile.txt');
        touch($file);

        $this->archiver->create($this->archiveFile);
        $this->archiver->addFile($file);
        $path = $this->archiver->save();

        $this->assertEquals($this->archiveFile, $path);

        unlink($file);
    }

    /** @test */
    public function it_throws_exception_if_file_not_exists()
    {
        $nonExistsFile = storage_path('/non_exists_file');

        $this->setExpectedException('\Exception');

        $this->archiver->create($this->archiveFile);

        $this->archiver->addFile($nonExistsFile);
    }
}
