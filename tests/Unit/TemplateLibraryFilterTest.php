<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TemplateLibraryFilterTest extends TestCase
{
    protected $sizeTolerance;

    public function setUp()
    {
        parent::setUp();

        $this->sizeTolerance = config('w2p.template_size_tolerance', 5);

        \App\TemplateLibrary::truncate();
    }

    /** @test */
    function it_filter_templates_by_width()
    {
        create('App\TemplateLibrary', ['width' => 100, 'height' => 100]);
        create('App\TemplateLibrary', ['width' => 50, 'height' => 90]);

        $templatesCount = \App\TemplateLibrary::widthInRange([50, 90])->count();

        $this->assertEquals(1, $templatesCount);
    }

    /** @test */
    function it_filter_templates_by_height()
    {
        create('App\TemplateLibrary', ['width' => 100, 'height' => 100]);
        create('App\TemplateLibrary', ['width' => 52, 'height' => 94]);

        $templatesCount = \App\TemplateLibrary::widthInRange([50, 90])->count();

        $this->assertEquals(1, $templatesCount);
    }

    /** @test */
    function it_filter_templates_size_in_range()
    {
        create('App\TemplateLibrary', ['width' => 100, 'height' => 100]);
        create('App\TemplateLibrary', ['width' => 52, 'height' => 94]);

        $widthRange = [50 - $this->sizeTolerance, 50 + $this->sizeTolerance];
        $heightRange = [90 - $this->sizeTolerance, 90 + $this->sizeTolerance];

        $templatesCount = \App\TemplateLibrary::sizeInRange($widthRange, $heightRange)->count();

        $this->assertEquals(1, $templatesCount);
    }

    /** @test */
    function it_filter_templates()
    {
        create('App\TemplateLibrary', ['width' => 100, 'height' => 100]);
        create('App\TemplateLibrary', ['width' => 52, 'height' => 94, 'folds' => 2, 'two_side' => true]);
        create('App\TemplateLibrary', ['width' => 52, 'height' => 94, 'folds' => 1, 'two_side' => true]);
        create('App\TemplateLibrary', ['width' => 52, 'height' => 94, 'folds' => 2, 'two_side' => false]);

        $filters = [
            'size' => '50x90',
            'folds' => 2,
            'two_side' => 1,
        ];

        $templatesCount = \App\TemplateLibrary::filter($filters)->count();

        $this->assertEquals(1, $templatesCount);
    }

    /** @test */
    function it_filter_by_product_type_id()
    {
        $nonExitingProductTypeId = 99999999;

        $productType = create('App\ProductType');

        create('App\TemplateLibrary', ['product_type_id' => $nonExitingProductTypeId]);
        create('App\TemplateLibrary', ['product_type_id' => $productType->id]);

        $templatesCount = \App\TemplateLibrary::filter(['product_type_id' => $productType->id])->count();

        $this->assertEquals(1, $templatesCount);
    }
}
