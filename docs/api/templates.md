# Получение шаблонов

```
GET /api/templates[?params]
```

Получает все шаблоны из базы.

Параметры:

* size (string) - размеры шаблонов, ширина и высота разделяется символом 'x'. Например 50x90
* folds (integer) - количество сгибов. Например: 2
* two_side (integer) - 0 - односторонний шаблон, 1 - двхсторонний
* product_type_id (integer) - идентификатор типа продукта, для которого получать шаблоны

http get http://dev.w2p.me/api/templates?size=50x90&two_side=1

```json
{
    "current_page": 1, 
    "data": [
        {
            "activity": 1, 
            "collection": "root", 
            "created_at": "2017-05-26 06:21:47", 
            "folds": 0, 
            "height": 54, 
            "id": 1, 
            "name": "", 
            "owner_id": 1, 
            "product_type_id": 1, 
            "psd": "/home/vagrant/Code/web2print/storage/app/psd_library/root/6_turnover.psd", 
            "ratio": 1.74074, 
            "second_preview": null, 
            "status": "ready", 
            "two_side": 1, 
            "type": 0, 
            "type_key": "v", 
            "updated_at": "2017-05-30 12:35:54", 
            "url": "/lib/psd_preview/6_turnover.png", 
            "w2p": "/home/vagrant/Code/web2print/public/lib/frx/scDw129oLMPgRWhf.frx", 
            "width": 94
        }
    ], 
    "from": 1, 
    "last_page": 1, 
    "next_page_url": null, 
    "per_page": 20, 
    "prev_page_url": null, 
    "to": 1, 
    "total": 1
}
```
