# Релиз на DEMO

! Обновить базу шаблонов, если нужно.

1. Сделать Pull Request master => demo
2. Подключиться по ssh к серверу

```
cd /datadrive/sites/www/demo
sudo git pull
sudo composer install
sudo php artisan migrate
sudo php artisan db:seed --class=SomeSeedClass
sudo npm install
sudo npm run production
cd ..; sudo chown -R www-data:www-data demo
```

# Релиз клиентам

1. Сделать backup всех баз клиентов (обычно названия БД начинаются на prod_*)
2. Выслать файл бэкапа на **cto@web2print.pro** с темой "Базы клиентов за d.m.Y H:i"
3. Сделать Pull Request master => production
4. Подключиться по ssh к серверу

```
cd /datadrive/sites/www/production
sudo git pull
sudo composer install
sudo php artisan subdomain:migrate
sudo php artisan db:seed --class=SomeSeedClass - для каждой базы. Изменять базу в .env
sudo npm install
sudo npm run production
cd ..; sudo chown -R www-data:www-data production
```