# События

### GroupOrderWasFormed
Вызывается когда заказ сформирован. 

### UserWasBilled
Пользователю был выставлен счёт

### PaymentWasAdded
Вызывается после частичной или полной оплаты заказа

### App\Events\Order\OrderWasClosedWithDiscount
Заказ закрыт со скидкой

### App\Events\Order\OrderWasClosedWithRefund
Заказ закрыт с возвратом денег
